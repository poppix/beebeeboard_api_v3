var Sequelize = require('sequelize');

var Token = sequelize.define('token', {
  token: {
    type: Sequelize.STRING
  },
  user_id: {
    type: Sequelize.BIGINT,

   references: {
     // This is a reference to another model
     model: Sequelize.Contact,

     // This is the column name of the referenced model
     key: 'id',

     // This declares when to check the foreign key constraint. PostgreSQL only.
     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
   }
  },
  expiration_date: {
  	type: Sequelize.DATE
  },
  client_id: {
	 type: Sequelize.BIGINT,

   references: {
     // This is a reference to another model
     model: Sequelize.Client,

     // This is the column name of the referenced model
     key: 'id',

     // This declares when to check the foreign key constraint. PostgreSQL only.
     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
   }
  },
  scope: {
  	type: Sequelize.STRING
  },
  redirecturi: {
  	type: Sequelize.STRING
  }
},{
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  underscored: true
});

global.Token = Token;

