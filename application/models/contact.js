
var Sequelize = require('sequelize');

var Contact = sequelize.define('contact', {
	id: {type: Sequelize.BIGINT,
		primaryKey: true
	},

	_tenant_id: { type: Sequelize.BIGINT, allowNull: false},
	organization: {type: Sequelize.STRING},
	organization_id: {type: Sequelize.BIGINT},
	supplier:  {type: Sequelize.BOOLEAN},
	name:  {type: Sequelize.STRING},
	mail:  {type: Sequelize.STRING},
	contact:  {type: Sequelize.STRING},
	telephone:  {type: Sequelize.STRING},
	mobile:  {type: Sequelize.STRING},
	fax:  {type: Sequelize.STRING},
	web_site:  {type: Sequelize.STRING},
	terms:  {type: Sequelize.STRING},
	piva:  {type: Sequelize.STRING},
	set_event_color:  {type: Sequelize.BOOLEAN},
	color:  {type: Sequelize.STRING},
	notes:  {type: Sequelize.STRING},
	thumbnail_name:  {type: Sequelize.STRING},
	thumbnail_url:  {type: Sequelize.STRING},
	thumbnail_etag: {type: Sequelize.STRING},
	thumbnail_type:  {type: Sequelize.STRING},
	role_id: {type: Sequelize.BIGINT}
	//product_ids: {type: Sequelize.ARRAY(Sequelize.BIGINT)}
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	underscored: true
});

global.Contact = Contact;


