var Sequelize = require('sequelize');

var User = sequelize.define('user', {
  id: {type: Sequelize.BIGINT,
    primaryKey: true
  },
  
 // instantiating will automatically set the flag to true if not set
 _tenant_id: { type: Sequelize.BIGINT, allowNull: false},
 organization: {type: Sequelize.STRING},
 organization_admin: {type: Sequelize.BOOLEAN},
 organization_id: {type: Sequelize.BIGINT},
 time_zone: {type: Sequelize.STRING},
 username: {type: Sequelize.STRING},
 password: {type: Sequelize.STRING},
 admin_id: { type: Sequelize.BIGINT,

   references: {
     // This is a reference to another model
     model: User,

     // This is the column name of the referenced model
     key: 'id',

     // This declares when to check the foreign key constraint. PostgreSQL only.
     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
   }},
   admin_name: {type: Sequelize.STRING},
   reset_password_token: {type: Sequelize.STRING},
   reset_password_expires: {type: Sequelize.DATE},
   create_user_token: {type: Sequelize.STRING},
   create_user_expires: {type: Sequelize.DATE},
   name: {type: Sequelize.STRING},
   mail: {type: Sequelize.STRING},
   telephone: {type: Sequelize.STRING},
   mobile: {type: Sequelize.STRING},
   fax: {type: Sequelize.STRING},
   web_site: {type: Sequelize.STRING},
   terms: {type: Sequelize.STRING},
   piva: {type: Sequelize.STRING},
   set_event_color: {type: Sequelize.BOOLEAN},
   color: {type: Sequelize.STRING},
   show_on_calendar: {type: Sequelize.BOOLEAN},
   calendar_rank: {type: Sequelize.INTEGER},
   from_mail_message: {type: Sequelize.STRING},
   mail_message: {type: Sequelize.STRING},
   cc_mail_message: {type: Sequelize.STRING},
   contact_visibility: {type: Sequelize.INTEGER},
   invoice_visibility: {type: Sequelize.INTEGER},
   estimate_visibility: {type: Sequelize.INTEGER},
   expense_visibility: {type: Sequelize.INTEGER},
   event_visibility: {type: Sequelize.INTEGER},
   product_visibility: {type: Sequelize.INTEGER},
   workhour_visibility: {type: Sequelize.INTEGER},
   calendar_step_minute:  {type: Sequelize.INTEGER},
   calendar_default_view:  {type: Sequelize.STRING},
   calendar_first_hour:  {type: Sequelize.INTEGER},
   calendar_last_hour:  {type: Sequelize.INTEGER},
   notes:  {type: Sequelize.STRING},
   piva_name: {type: Sequelize.STRING},
   start_yearmonth: {type: Sequelize.STRING},
   start_yearday: {type: Sequelize.STRING},
   invoice_from: {type: Sequelize.STRING},
   invoice_piva: {type: Sequelize.STRING},
   invoice_notes: {type: Sequelize.STRING},
   paypal: {type: Sequelize.STRING},
   invoice_style: {type: Sequelize.STRING},
   socket_room: {type: Sequelize.STRING},
   enable_trello: {type: Sequelize.BOOLEAN},
   trello_key: {type: Sequelize.STRING},
   trello_token: {type: Sequelize.STRING},
   redirect_url: {type: Sequelize.STRING}
 // It is possible to create foreign keys:
 /*contact_id: {
   type: Sequelize.BIGINT,

   references: {
     // This is a reference to another model
     model: Contact,

     // This is the column name of the referenced model
     key: 'id',

     // This declares when to check the foreign key constraint. PostgreSQL only.
     deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
   }
 }*/
},
{
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  underscored: true
});

global.User = User;


