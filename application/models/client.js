var Sequelize = require('sequelize');

var Client = sequelize.define('client', {
  name: {
    type: Sequelize.STRING
  },
  clientid: {
    type: Sequelize.STRING,
  },
  clientsecret: {
  	type: Sequelize.STRING
  }
},{
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  underscored: true
});

global.Client = Client;

