var Sequelize = require('sequelize');

var Organization = sequelize.define('organization', {
	id: {type: Sequelize.BIGINT,
      primaryKey: true
    },
	_tenant_id: { type: Sequelize.BIGINT, allowNull: false},
 	organization: {type: Sequelize.STRING},
 	name: {type: Sequelize.STRING},
 	sms_remained: {type: Sequelize.INTEGER},
 	contact_name: {type: Sequelize.STRING},
 	product_name: {type: Sequelize.STRING},
 	estimate_name: {type: Sequelize.STRING},
 	invoice_name: {type: Sequelize.STRING},
 	dashboard_name: {type: Sequelize.STRING},
 	expense_name: {type: Sequelize.STRING},
 	order_name: {type: Sequelize.STRING},
 	ddt_name: {type: Sequelize.STRING},
 	contract_name: {type: Sequelize.STRING},
 	calendar_name: {type: Sequelize.STRING},
 	tracker_name: {type: Sequelize.STRING},
 	cost_name: {type: Sequelize.STRING},
 	contacts_name: {type: Sequelize.STRING},
 	products_name: {type: Sequelize.STRING},
 	estimates_name: {type: Sequelize.STRING},
 	invoices_name: {type: Sequelize.STRING},
 	expenses_name: {type: Sequelize.STRING},
 	calendars_name: {type: Sequelize.STRING},
 	trackers_name: {type: Sequelize.STRING},
 	costs_name: {type: Sequelize.STRING},
 	orders_name: {type: Sequelize.STRING},
 	ddts_name: {type: Sequelize.STRING},
 	contracts_name: {type: Sequelize.STRING},
	//organization_custom_field_ids: {type: Sequelize.ARRAY(Sequelize.BIGINT)},
	//event_status :[],
	//event_status_ids: {type: Sequelize.ARRAY(Sequelize.BIGINT)}
},{
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  underscored: true
});

global.Organization = Organization;