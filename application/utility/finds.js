 var async = require('async');
 var Sequelize = require('sequelize');
 var utility = require('../utility/utility.js');

 exports.find_address = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT address_find_data(' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.params.id + ',\'asc\');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].address_find_data && datas[0].address_find_data != null) {
                         callback(null, datas[0].address_find_data[0]);
                     } else {
                         callback(null, []);
                     }

                 }).catch(function(err) {
                     callback(err, null);
                 });
         }
     }, function(err, results) {

         if (err) {
             return res.status(400).render('address_find: ' + err);
         }

         if (results.data.length == 0) {
             callback_(utility.error404('id', 'Nessun indirizzo trovato', 'Nessun indirizzo trovato', '404'), null);
         } else {
             var response = {
                 'data': results.data,
                 'included': []
             };

         }

         callback_(null, response);
     });
 };

 exports.find_contact = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT contact_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(contacts) {

                     if (contacts[0].contact_find_data_v2 && contacts[0].contact_find_data_v2 != null) {
                         callback(null, contacts[0].contact_find_data_v2[0]);
                     } else {
                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log('contacts:' + err);
                     callback(err, null);
                 });
         },
         included: function(callback) {
             sequelize.query('SELECT contact_find_included_v2(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(includeds) {

                     if (includeds[0].contact_find_included_v2 && includeds[0].contact_find_included_v2 != null) {
                         callback(null, includeds[0].contact_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log('includeds:' + err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {

         if (err) {
             callback_(err, null);
         }

         if (results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun contatto trovato', 'Nessun contatto trovato', '404'), null);
         } else {
             response = {
                 'data': utility.check_find_datas(results.data, 'contacts', ['addresses', 'user_calendars', 'reminder_settings', 'user_taxes', 'files', 'payments',
                     'products', 'projects', 'custom_fields', 'user_invoice_settings', 'contact_consensis', 'user_sezionales', 'assurances'
                 ]),
                 'included': utility.check_find_included(results.included, 'contacts', ['products', 'projects', 'addresses', 'payments', 'custom_fields', 'user_taxes', 'files',
                     'tags', 'organizations', 'event_states', 'contact_states', 'product_states', 'project_states',
                     'workhour_states', 'organization_custom_fields', 'categories', 'sub_categories', 'applications',
                     'custom_field_definitions', 'files', 'cumulable_taxes', 'user_calendars',
                     'roles', 'contacts', 'reminder_settings', 'events', 'related_contacts', 'invoice_styles','user_invoice_settings', 'contact_consensis', 'user_sezionales',
                     'assurances'
                 ])

             };

             callback_(null, response);
         }

     });
 };

 exports.find_custom_field = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT custom_field_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].custom_field_find_data_v2 && datas[0].custom_field_find_data_v2 != null) {
                         callback(null, datas[0].custom_field_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT custom_field_find_included_v2(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].custom_field_find_included_v2 && included[0].custom_field_find_included_v2 != null) {
                         callback(null, included[0].custom_field_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, result) {
         if (err) {
             return res.status(400).render('custom_field_find: ' + err);
         }

         if (result.data.length == 0 || !result.included) {

             callback_(utility.error404('id', 'Nessun campo personalizzato trovato', 'Nessun campo personalizzato trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(result.data, 'custom_fields', ['organization_custom_fields']),
                 'included': utility.check_find_included(result.included, 'custom_fields', ['organization_custom_fields', 'custom_field_definitions'])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_event = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT event_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].event_find_data_v2 && datas[0].event_find_data_v2 != null) {
                         callback(null, datas[0].event_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT event_find_included_v2(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].event_find_included_v2 && included[0].event_find_included_v2 != null) {
                         callback(null, included[0].event_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {
         if (err) {
             callback_(err, null);
         }

         if (results.data && results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun evento trovato', 'Nessun evento trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'events', ['custom_fields', 'reminders', 'payments', 'addresses', 'contacts', 'products', 'projects', 'files', 'invoices', 'expenses']),
                 'included': utility.check_find_included(results.included, 'events', ['products', 'projects', 'addresses', 'reminders', 'event_states', 'custom_fields', 'files',
                     'organization_custom_fields', 'payments', 'custom_field_definitions', 'contacts', 'contact_states', 'workhours', 'related_events', 'invoices', 'expenses', 'tags'
                 ])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_expense = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT expense_find_data_v3(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].expense_find_data_v3 && datas[0].expense_find_data_v3 != null) {
                         callback(null, datas[0].expense_find_data_v3[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         },
         included: function(callback) {
             sequelize.query('SELECT expense_find_included_v3(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].expense_find_included_v3 && included[0].expense_find_included_v3 != null) {
                         callback(null, included[0].expense_find_included_v3);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {
         if (err) {
             callback_(err, null);
         }
         if (results.data.length == 0 || !results.included) {

             callback_(utility.error404('id', 'Nessun acquisto trovato', 'Nessun acquisto trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'expenses', ['reminders', 'expire_dates', 'addresses', 'files', 'payments', 'expensearticles', 'invoices', 'related_expenses','invoicetaxes','expenses_notifiche_sdis']),
                 'included': utility.check_find_included(results.included, 'expenses', ['payments', 'files', 'addresses', 'contacts', 'products', 'projects', 'reminders',
                     'expensearticles', 'categories', 'sub_categories', 'expire_dates', 'tags', 'contact_states', 'events', 'invoices', 'related_expenses','invoicetaxes','expenses_notifiche_sdis'
                 ])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_invoice = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT invoice_find_data_v4(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].invoice_find_data_v4 && datas[0].invoice_find_data_v4 != null) {
                         callback(null, datas[0].invoice_find_data_v4[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT invoice_find_included_v4(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].invoice_find_included_v4 && included[0].invoice_find_included_v4 != null) {
                         callback(null, included[0].invoice_find_included_v4);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err && !results.data) {
             callback_(err, null);
         }
         else{
            if (results.data.length == 0 || !results.included) {

                 callback_(utility.error404('id', 'Nessuna fattura trovata', 'Nessuna fattura trovata', '404'), null);
             } else {

                 var response = {
                     'data': utility.check_find_datas(results.data, 'invoices', ['reminders', 'files', 'payments', 'invoicetaxes', 'invoiceproducts', 'expire_dates', 'invoicegroups', 'related_invoices', 'expenses','invoices_notifiche_sdis', 'workhours']),
                     'included': utility.check_find_included(results.included, 'invoices', ['payments', 'files', 'addresses', 'contacts', 'products', 'projects', 'reminders', 'tags', 'events', 'related_invoices', 'expenses',
                         'invoiceproducts', 'invoicegroups', 'invoicetaxes', 'expire_dates', 'invoiceproduct_taxes', 'contact_states', 'invoice_styles', 'user_taxes', 'invoices_notifiche_sdis', 'categories', 'sub_categories', 'workhours'
                     ])
                 };
               
                 callback_(null, response);
             }
         }
         


     });
 };

 exports.find_invoice_style = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT invoice_style_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].invoice_style_find_data_v2 && datas[0].invoice_style_find_data_v2 != null) {
                         callback(null, datas[0].invoice_style_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT invoice_style_find_included_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].invoice_style_find_included_v2 && included[0].invoice_style_find_included_v2 != null) {
                         callback(null, included[0].invoice_style_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err) {
             callback_(err, null);
         }
         if (results.data.length == 0 || !results.included) {

             callback_(utility.error404('id', 'Nessuno stile di fattura trovato', 'Nessuno stile di fattura trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'invoice_styles', ['files']),
                 'included': utility.check_find_included(results.included, 'invoice_styles', ['files', 'tags'])
             };

             callback_(null, response);

         }

     });
 };

 exports.find_invoiceproduct = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT invoiceproduct_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].invoiceproduct_find_data_v2 && datas[0].invoiceproduct_find_data_v2 != null) {
                         callback(null, datas[0].invoiceproduct_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT invoiceproduct_find_included_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].invoiceproduct_find_included_v2 && included[0].invoiceproduct_find_included_v2 != null) {
                         callback(null, included[0].invoiceproduct_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err) {
             callback_(err, null);
         }
         if (results.data.length == 0 || !results.included) {

             callback_(utility.error404('id', 'Nessun prodotto in fattura trovato', 'Nessun prodotto in fattura trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'invoiceproducts', ['invoiceproduct_taxes']),
                 'included': utility.check_find_included(results.included, 'invoiceproducts', ['invoiceproduct_taxes', 'cumulable_invoiceproduct_taxes', 'products', 'events', 'projects'])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_invoicegroup = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT invoicegroup_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].invoicegroup_find_data_v2 && datas[0].invoicegroup_find_data_v2 != null) {
                         callback(null, datas[0].invoicegroup_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT invoicegroup_find_included_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].invoicegroup_find_included_v2 && included[0].invoicegroup_find_included_v2 != null) {
                         callback(null, included[0].invoicegroup_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('invoicegroup_find: ' + err);
         }
         if (results.data.length == 0 || !results.included) {

             callback_(utility.error404('id', 'Nessun gruppo di prodotti in fattura trovato', 'Nessun gruppo di prodotti  in fattura trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'invoicegroups', ['invoiceproducts']),
                 'included': utility.check_find_included(results.included, 'invoicegroups', ['invoiceproducts', 'products', 'contacts', 'invoiceproduct_taxes', 'cumulable_invoiceproduct_taxes'])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_invoiceproduct_taxe = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT invoiceproduct_taxe_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].invoiceproduct_taxe_find_data_v2 && datas[0].invoiceproduct_taxe_find_data_v2 != null) {
                         callback(null, datas[0].invoiceproduct_taxe_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT invoiceproduct_taxe_find_included_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].invoiceproduct_taxe_find_included_v2 && included[0].invoiceproduct_taxefind_included_v2 != null) {
                         callback(null, included[0].invoiceproduct_taxe_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('invoiceproduct_taxe_find: ' + err);
         }
         if (results.data.length == 0 || !results.included) {

             callback_(utility.error404('id', 'Nessuna tassa di prodotto in fattura trovato', 'Nessuna tassa di prodotto in fattura trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'invoiceproduct_taxes', ['cumulable_invoiceproduct_taxes']),
                 'included': utility.check_find_included(results.included, 'invoiceproduct_taxes', ['cumulable_invoiceproduct_taxes'])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_invoicetaxe = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             var response = {};
             sequelize.query('SELECT invoicetaxe_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].invoicetaxe_find_data_v2 && datas[0].invoicetaxe_find_data_v2 != null) {
                         callback(null, datas[0].invoicetaxe_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('invoicetaxe_find: ' + err);
         }
         if (results.data.length == 0) {

             callback_(utility.error404('id', 'Nessuna tassa in fattura trovato', 'Nessuna tassa in fattura trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'invoicetaxes', []),
                 'included': []
             };

             callback_(null, response);
         }


     });
 };

 exports.find_payment = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT payments_find_data(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.role_id + ',' +
                     req.user.id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {
                     if (datas[0].payments_find_data && datas[0].payments_find_data != null) {
                         callback(null, datas[0].payments_find_data[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         },
         included: function(callback) {
             sequelize.query('SELECT payments_find_included(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.role_id + ',' +
                     req.user.id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].payments_find_included && included[0].payments_find_included != null) {
                         callback(null, included[0].payments_find_included);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }

     }, function(err, results) {
         if (err) {
             return res.status(400).render('payment_find: ' + err);
         }
         var inc = [];
         if (results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun pagamento trovato', 'Nessun pagamento trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'payments', []),
                 'included': utility.check_find_included(results.included, 'payments', ['products', 'projects', 'files', 'events', 'tags', 'contacts', 'invoices', 'expenses', 'contact_states'])
             };

             callback_(null, response);

         }

     });
 };

 exports.find_product = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT product_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].product_find_data_v2 && datas[0].product_find_data_v2 != null) {
                         callback(null, datas[0].product_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT product_find_included_v2(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {


                     if (included[0].product_find_included_v2 && included[0].product_find_included_v2 != null) {
                         callback(null, included[0].product_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('product_find: ' + err);
         }

         if (results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'products', ['files', 'payments', 'contacts', 'projects', 'custom_fields', 'todolists', 'product_categories']),
                 'included': utility.check_find_included(results.included, 'products', ['products', 'contacts', 'projects', 'files', 'payments', 'custom_fields', 'organization_custom_fields',
                     'todolists', 'reminders', 'addresses', 'product_states', 'tags', 'todos', 'tags', 'contact_states', 'events', 'related_products','product_categories'
                 ])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_project = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT project_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].project_find_data_v2 && datas[0].project_find_data_v2 != null) {
                         callback(null, datas[0].project_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT project_find_included_v2(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].project_find_included_v2 && included[0].project_find_included_v2 != null) {
                         callback(null, included[0].project_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('project_find: ' + err);
         }
         if (results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun progetto trovato', 'Nessun progetto trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'projects', ['files', 'payments', 'contacts', 'products', 'custom_fields', 'todolists', 'project_consensis']),
                 'included': utility.check_find_included(results.included, 'projects', ['products', 'contacts', 'files', 'payments', 'custom_fields', 'organization_custom_fields', 'addresses',
                     'todolists', 'reminders', 'project_states', 'tags', 'todos', 'tags', 'contact_states', 'events', 'related_projects', 'project_consensis'
                 ])
             };

             callback_(null, response);
         }


     });
 };

 exports.find_todolist = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT todolist_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].todolist_find_data_v2 && datas[0].todolist_find_data_v2 != null) {
                         callback(null, datas[0].todolist_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         },
         included: function(callback) {
             sequelize.query('SELECT todolist_find_included_v2(' +
                     req.params.id + ',' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].todolist_find_included_v2 && included[0].todolist_find_included_v2 != null) {
                         callback(null, included[0].todolist_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('todolist_find: ' + err);
         }
         if (results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun todolist trovato', 'Nessun todolist trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'todolists', ['todos','files']),
                 'included': utility.check_find_included(results.included, 'todolists', ['todos','files'])
             };

             callback_(null, response);
         }


     });
 };


 exports.find_todo = function(req, callback_) {

     async.parallel({
         data: function(callback) {
             sequelize.query('SELECT todo_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].todo_find_data_v2 && datas[0].todo_find_data_v2 != null) {
                         callback(null, datas[0].todo_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });

         }
     }, function(err, results) {
         if (err) {
             return res.status(400).render('todo_find: ' + err);
         }

         if (results.data.length == 0 ) {
             callback_(utility.error404('id', 'Nessun todo trovato', 'Nessun todo trovato', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'todos', []),
                 'included': []
             };

             callback_(null, response);
         }


     });
 };

 exports.find_workhour = function(req, callback_) {

     async.parallel({
         data: function(callback) {

             sequelize.query('SELECT workhour_find_data_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(datas) {

                     if (datas[0].workhour_find_data_v2 && datas[0].workhour_find_data_v2 != null) {
                         callback(null, datas[0].workhour_find_data_v2[0]);

                     } else {

                         callback(null, []);
                     }

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         },
         included: function(callback) {
             sequelize.query('SELECT workhour_find_included_v2(' +
                     req.user.id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     req.user._tenant_id + ',' +
                     req.params.id + ',' +
                     req.user.role_id + ');', {
                         raw: true,
                         type: Sequelize.QueryTypes.SELECT
                     })
                 .then(function(included) {

                     if (included[0].workhour_find_included_v2 && included[0].workhour_find_included_v2 != null) {
                         callback(null, included[0].workhour_find_included_v2);

                     } else {

                         callback(null, []);
                     }
                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
         }
     }, function(err, results) {
         if (err) {
             callback_(err, null);
         }
         if (results.data.length == 0 || !results.included) {
             callback_(utility.error404('id', 'Nessun\'ora lavorata trovata', 'Nessun\'ora lavorata trovata', '404'), null);
         } else {
             var response = {
                 'data': utility.check_find_datas(results.data, 'workhours', ['addresses','files']),
                 'included': utility.check_find_included(results.included, 'workhours', ['products', 'projects', 'contacts', 'addresses', 'workhour_states', 'contact_states', 'events',
                     'related_workhours', 'custom_fields', 'organization_custom_fields','files','tags'
                 ])
             };

             callback_(null, response);
         }


     });
 };