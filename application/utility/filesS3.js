var Stream = require('stream')
const { S3Client, GetObjectCommand } = require('@aws-sdk/client-s3')
var streamify = require('stream-array')
var concat = require('concat-stream')
var path = require('path')
var config = require('../../config')

var s3Files = {}
module.exports = s3Files

s3Files.connect = function (opts) {
  var self = this

  if ('s3' in opts) {
    self.s3 = opts.s3
  } else {
    
    self.s3 = new S3Client({
            region: config.aws_s3.region,
            credentials:{
                accessKeyId: config.aws_s3.accessKeyId,
                secretAccessKey: config.aws_s3.secretAccessKey,
            }
        });
  }

  self.bucket = opts.bucket

  return self
}

s3Files.createKeyStream = function (folder, keys) {

  if (!keys) return null
  var paths = []
  keys.forEach(function (key) {
    if (folder) {
      paths.push(path.posix.join(folder, key))
    } else {
      paths.push(key)
    }
  })


  return streamify(paths)
}

s3Files.createFileStream = function (keyStream, preserveFolderPath) {

  var self = this
  if (!self.bucket) return null

  var rs = new Stream()
  rs.readable = true
 

  var fileCounter = 0

 
 
  keyStream.on('data', function (file) {
    fileCounter += 1

    // console.log('->file', file);
    var params = { Bucket: self.bucket, Key: file }
    const command = new GetObjectCommand(params);
    
    self.s3.send(command, function(err, data_final){
      
      
      data_final.Body.pipe(
        concat(function buffersEmit (buffer) {
           //console.log('buffers concatenated, emit data for ');
          
           
          var path = preserveFolderPath ? file : file.replace(/^.*[\\/]/, '')
          //console.log( { 'data': buffer, 'path': path })
          rs.emit('data', { data: buffer, path: path })
          //rs.push({ 'data': buffer, 'path': path })
        })
      )
      data_final.Body.on('end', function () {
        fileCounter -= 1
        if (fileCounter < 1) {
           //console.log('all files processed, emit end');
          //console.log(rs);
          rs.emit('end')
         
           //callback(null,rs)
        }
      })

      data_final.Body.on('error', function (err) {
        rs.emit('error', err)
        //console.log(err)
      })


    })

  })
   return rs;
  
  
}
