var moment = require('moment');
var util = require('util');
var utility = require('./utility');
var crypto = require("crypto");
var constants = require("constants");
var config = require('../../config')


function togli_caratteri(s) {
    var r = s.toLowerCase();
    r = r.replace(new RegExp(/\s/g), "");
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/æ/g), "ae");
    r = r.replace(new RegExp(/ç/g), "c");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/ñ/g), "n");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/œ/g), "oe");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");
    r = r.replace(new RegExp(/[ýÿ]/g), "y");
    r = r.replace(new RegExp(/\W/g), "");
    return r;
};

function formatta_euro(number) {

    if (number) {
        var numberStr = parseFloat(number).toFixed(2).toString();
        var numFormatDec = numberStr.slice(-2);
        numberStr = numberStr.substring(0, numberStr.length - 3);
        var numFormat = [];
        while (numberStr.length > 3) {
            numFormat.unshift(numberStr.slice(-3));
            numberStr = numberStr.substring(0, numberStr.length - 3);
        }
        numFormat.unshift(numberStr);
        return numFormat.join('') + '.' + numFormatDec;
    }

    return '0.00';
};

function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
};

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
};

function checkContainNumber(str) {
    return str.match(/[0-9]/i);
};

function getIdFiscaleIVA(piva, utente, invoice) {
    if (!piva) { return null; }

    if (isLetter(piva[0]) && isLetter(piva[1])) {
        return piva.substring(2);
    }
   
     
    return piva.toUpperCase();
    
       
   
};


function codificaStringa(str){
    var key = config.ts_certificate.cert;
   
    var publicKey = key;

    var buffer = Buffer.from(str);

    return crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
   
};

function getProprietario(data) {
    if (!data) { return null; }

    var key = config.ts_certificate.cert;
   
    var publicKey = key;

   
    if(data.struttura && data.struttura === true){
        var buffer = Buffer.from(data.cod_fiscale);
        var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
   
        return {
            codiceRegione: data.codRegione,
            codiceAsl: data.codAsl,
            codiceSSA: data.codSSA,
            cfProprietario: encryptedPinCode
        };
    }else{
        var buffer = Buffer.from(data.username);
        var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
   
        return {
            cfProprietario: encryptedPinCode
        };
    }  
};

exports.Proprietario = function(data){

    if (!data) { return null; }

    var key = config.ts_certificate.cert;
   
    var publicKey = key;
   
    
    if(data.struttura && data.struttura === true){
        var buffer = Buffer.from(data.cod_fiscale);
        var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
   
        return {
            codiceRegione: data.codRegione,
            codiceAsl: data.codAsl,
            codiceSSA: data.codSSA,
            cfProprietario: encryptedPinCode
        };
    }else{
        var buffer = Buffer.from(data.username);
        var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
        
        return {
            cfProprietario: encryptedPinCode
        };
    }  
};

exports.CodificaStringa = function(str){

    return codificaStringa(str);
};


exports.GetDocumentiSpesa = function(invoices, tipo_spesa, operazione) {
    var linee = [];

    var key = config.ts_certificate.cert;
        
    var publicKey = key;

   

    //if (invoices && invoices.length > 0) {
      //  for(var i = 0; i < invoices.length;i++){
            if(invoices.attributes.contact_cf){

                var rimborso = null;
                var bollo_in_fattura = false;
               

               /* if(operazione && operazione == 'R'){
                    rimborso = {
                        pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
                        dataEmissione: invoices.attributes.creditnote_invoicedate ? moment(new Date(invoices.attributes.creditnote_invoicedate)).format('YYYY-MM-DD') : null,
                        numDocumentoFiscale: {
                            dispositivo: 1,
                            numDocumento: invoices.attributes.creditnote_invoicenumber ? invoices.attributes.creditnote_invoicenumber : null
                        }
                    };
                }*/



                var buffer = Buffer.from(invoices.attributes.contact_cf);
                var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
       
                if(invoices.attributes.anno >= 2021){

                    if(invoices.attributes.bollo_cliente !== null || invoices.attributes.bollo_riga !== null){
                        bollo_in_fattura = true;
                    }
                

                    var spesa =  
                    { 
                        idSpesa: {
                            pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
                            dataEmissione: invoices.attributes.date ? moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD') : null,
                            numDocumentoFiscale: {
                                dispositivo: 1,
                                numDocumento: invoices.attributes.number ? invoices.attributes.number : null
                            }
                        },
                        dataPagamento: invoices.attributes.data_saldo ? moment(new Date(invoices.attributes.data_saldo)).format('YYYY-MM-DD')  : null,
                        flagPagamentoAnticipato:  moment(new Date(invoices.attributes.data_saldo)).isBefore(moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD')) ? '1' : null,
                        //flagOperazione: operazione ? operazione : 'I',
                        cfCittadino: (invoices.attributes.ts_enable && invoices.attributes.ts_enable === true ? (encryptedPinCode ? encryptedPinCode : null) : null),
                        voceSpesa: getVoceSpesa(invoices.attributes, tipo_spesa, bollo_in_fattura),
                        pagamentoTracciato: invoices.attributes.metodo_pagamento.toLowerCase().indexOf('contanti') != -1 ? 'NO' : 'SI',
                        tipoDocumento: (invoices.attributes.document_type && invoices.attributes.document_type =='invoice' ? 'F' : 'D'),
                        flagOpposizione: (invoices.attributes.ts_enable && invoices.attributes.ts_enable === true ? '0' : '1'),
                       /* idRimborso: operazione && operazione == 'R' ? rimborso : null*/
                        
                    };

                   Object.keys(spesa).forEach(function(key) { 
                       
                        if (spesa[key] && typeof spesa[key] === 'object') 
                            utility.removeEmpty(spesa[key]);
                        else if (spesa[key] == null) 
                            delete spesa[key] 
                    }); 

                    
                
                    return spesa;
                }else{
                    if(invoices.attributes.ts_enable && invoices.attributes.ts_enable === true){
                        var spesa =  
                        { 
                            idSpesa: {
                                pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
                                dataEmissione: invoices.attributes.date ? moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD') : null,
                                numDocumentoFiscale: {
                                    dispositivo: 1,
                                    numDocumento: invoices.attributes.number ? invoices.attributes.number : null
                                }
                            },
                            dataPagamento: invoices.attributes.data_saldo ? moment(new Date(invoices.attributes.data_saldo)).format('YYYY-MM-DD')  : null,
                            flagPagamentoAnticipato: moment(new Date(invoices.attributes.data_saldo)).isBefore(moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD')) ? '1' : null,
                            //flagOperazione: operazione ? operazione : 'I',
                            cfCittadino: (invoices.attributes.ts_enable && invoices.attributes.ts_enable === true ? (encryptedPinCode ? encryptedPinCode : null) : null),
                            voceSpesa: getVoceSpesa(invoices.attributes, tipo_spesa),
                            pagamentoTracciato: invoices.attributes.metodo_pagamento.toLowerCase().indexOf('contanti') != -1 ? 'NO' : 'SI',
                             /* idRimborso: operazione && operazione == 'R' ? rimborso : null*/
                            
                        };

                         
               
                            Object.keys(spesa).forEach(function(key) { 
                               
                                if (spesa[key] && typeof spesa[key] === 'object') 
                                    utility.removeEmpty(spesa[key]);
                                else if (spesa[key] == null) 
                                    delete spesa[key] 
                            }); 

                           return spesa;
                    }
                    
                }

                
            }

            return null;
            
            
        //}
        

    //} else
     //   return null;
};

exports.GetDocumentoFiscale = function(invoices, tipo_spesa, operazione) {
    var linee = [];

    var key = config.ts_certificate.cert;
        
    var publicKey = key;


    if(invoices.attributes.contact_cf){

        var buffer = Buffer.from(invoices.attributes.contact_cf);
        var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');

        
        var spesa =  
        { 
            
            pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
            dataEmissione: invoices.attributes.creditnote_invoicedate ? moment(new Date(invoices.attributes.creditnote_invoicedate)).format('YYYY-MM-DD') : null,
            numDocumentoFiscale: {
                dispositivo: 1,
                numDocumento: invoices.attributes.creditnote_invoicenumber ? invoices.attributes.creditnote_invoicenumber : null
            }
            
            
        };

        return spesa
    }
        
};

function getDocumentiSpesa(invoices, tipo_spesa, operazione) {
    var linee = [];

    var key = config.ts_certificate.cert;
        
    var publicKey = key;
    var bollo_in_fattura = false;


    //if (invoices && invoices.length > 0) {
      //  for(var i = 0; i < invoices.length;i++){
            if(invoices.attributes.contact_cf){

                var rimborso = null;

                if(operazione && operazione == 'R'){
                    rimborso = {
                        pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
                        dataEmissione: invoices.attributes.date ? moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD') : null,
                        numDocumentoFiscale: {
                            dispositivo: 1,
                            numDocumento: invoices.attributes.number ? invoices.attributes.number : null
                        }
                    };
                }

                var buffer = Buffer.from(invoices.attributes.contact_cf);
                var encryptedPinCode = crypto.publicEncrypt({"key":publicKey, padding:constants.RSA_PKCS1_PADDING}, buffer).toString('base64');
       
                if(invoices.attributes.anno >= 2021){

                     if(invoices.attributes.bollo_cliente !== null || invoices.attributes.bollo_riga !== null){
                        bollo_in_fattura = true;
                    }

                    console.log('2'+bollo_in_fattura);
                
                
                    var spesa =  
                    { 
                        idSpesa: {
                            pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
                            dataEmissione: invoices.attributes.date ? moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD') : null,
                            numDocumentoFiscale: {
                                dispositivo: 1,
                                numDocumento: invoices.attributes.number ? invoices.attributes.number : null
                            }
                        },
                        dataPagamento: invoices.attributes.data_saldo ? moment(new Date(invoices.attributes.data_saldo)).format('YYYY-MM-DD')  : null,
                        flagPagamentoAnticipato: moment(new Date(invoices.attributes.data_saldo)).isBefore(moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD')) ? '1' : null,
                        //flagOperazione: operazione ? operazione : 'I',
                        cfCittadino: (invoices.attributes.ts_enable && invoices.attributes.ts_enable === true ? (encryptedPinCode ? encryptedPinCode : null) : null),
                        voceSpesa: getVoceSpesa(invoices.attributes, tipo_spesa, bollo_in_fattura),
                        pagamentoTracciato: invoices.attributes.metodo_pagamento.toLowerCase().indexOf('contanti') != -1 ? 'NO' : 'SI',
                        tipoDocumento: (invoices.attributes.document_type && invoices.attributes.document_type =='invoice' ? 'F' : 'D'),
                        flagOpposizione: (invoices.attributes.ts_enable && invoices.attributes.ts_enable === true ? '0' : '1'),
                        idRimborso: operazione && operazione == 'R' ? rimborso : null
                        
                    };
                }else{
                    if(invoices.attributes.ts_enable && invoices.attributes.ts_enable === true){
                        var spesa =  
                        { 
                            idSpesa: {
                                pIva: invoices.attributes.society_piva ? getIdFiscaleIVA(invoices.attributes.society_piva) : null,
                                dataEmissione: invoices.attributes.date ? moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD') : null,
                                numDocumentoFiscale: {
                                    dispositivo: 1,
                                    numDocumento: invoices.attributes.number ? invoices.attributes.number : null
                                }
                            },
                            dataPagamento: invoices.attributes.data_saldo ? moment(new Date(invoices.attributes.data_saldo)).format('YYYY-MM-DD')  : null,
                            flagPagamentoAnticipato: moment(new Date(invoices.attributes.data_saldo)).isBefore(moment(new Date(invoices.attributes.date)).format('YYYY-MM-DD')) ? '1' : null,
                            //flagOperazione: operazione ? operazione : 'I',
                            cfCittadino: (invoices.attributes.ts_enable && invoices.attributes.ts_enable === true ? (encryptedPinCode ? encryptedPinCode : null) : null),
                            voceSpesa: getVoceSpesa(invoices.attributes, tipo_spesa),
                            pagamentoTracciato: invoices.attributes.metodo_pagamento.toLowerCase().indexOf('contanti') != -1 ? 'NO' : 'SI',
                            idRimborso: operazione && operazione == 'R' ? rimborso : null
                            
                        };
                    }
                    
                }
               

                linee.push(utility.removeEmpty(spesa));
            }
            
            
        //}
        return linee;
        

    //} else
     //   return null;
};

function getVoceSpesa(invoice, tipo_spesa, bollo){
    var linee = [];

    if (invoice) {

        
        var flag_tipo_spesa = (tipo_spesa == 'PS' ? '1' : ( tipo_spesa == 'IN' ? '2' : null)); 
      
        var spesa_code = (tipo_spesa == 'PS' ? 'TK' : ( tipo_spesa == 'IN' ? 'SR' : tipo_spesa)); 
       
        if(invoice.anno >= 2021){

            var doc_type =  invoice.document_type;
            var natura = invoice.iva_natura && invoice.iva_natura != '' ? invoice.iva_natura.split(':')[0] : null;

            if(doc_type == 'invoice'){
                natura = natura;
            }
            else
            {
                if(natura && natura.indexOf('.') != -1)
                {
                    natura = natura.split('.')[0];
                }
            }

            //if(iv.attributes.tipospesa){
                var spesa = {
                    tipoSpesa: spesa_code,
                    /*
                        TipoSpesa 
                        TK: Ticket
                        FC: Farmaco
                        FV: Farmaco veterinario
                        SV: Spese veterinarie
                        SP: prestazioni sanitarie
                        AD: acquisto affitto dispositivo CE
                        AS: spese sanitarie per ECG e prestazioni da farmacia
                        SR: prestazioni assistenza specialistica ambulatoriale
                        CT: cure termali
                        PI: protesica e integrativa
                        IC: chirurgua estetica
                        AA: altre spese
                    */
                    flagTipoSpesa : flag_tipo_spesa ? flag_tipo_spesa : null,
                    importo: invoice.total ? (bollo == true ? formatta_euro(invoice.total - 2) : formatta_euro(invoice.total)) : null,
                    aliquotaIVA: invoice.iva_rate || invoice.iva_rate != 0 ? formatta_euro(invoice.iva_rate) : null,
                    naturaIVA: natura ? natura : null
                };

                 linee.push(spesa);


                if(bollo  == true && invoice.tot_pay_contanti != 2 && invoice.total > 2){
                    spesa = {
                        tipoSpesa: spesa_code,
                        /*
                            TipoSpesa 
                            TK: Ticket
                            FC: Farmaco
                            FV: Farmaco veterinario
                            SV: Spese veterinarie
                            SP: prestazioni sanitarie
                            AD: acquisto affitto dispositivo CE
                            AS: spese sanitarie per ECG e prestazioni da farmacia
                            SR: prestazioni assistenza specialistica ambulatoriale
                            CT: cure termali
                            PI: protesica e integrativa
                            IC: chirurgua estetica
                            AA: altre spese
                        */
                        flagTipoSpesa : flag_tipo_spesa ? flag_tipo_spesa : null,
                        importo: 2,
                        naturaIVA: invoice.bollo_natura && invoice.bollo_natura.split(':')[0] == 'N2.2' ? 'N2.2' : 'N1'
                    };

                    linee.push(spesa);
                }

                
        }
        else{

        //if(iv.attributes.tipospesa){
            var spesa = {
                tipoSpesa: spesa_code,
                /*
                    TipoSpesa 
                    TK: Ticket
                    FC: Farmaco
                    FV: Farmaco veterinario
                    SV: Spese veterinarie
                    SP: prestazioni sanitarie
                    AD: acquisto affitto dispositivo CE
                    AS: spese sanitarie per ECG e prestazioni da farmacia
                    SR: prestazioni assistenza specialistica ambulatoriale
                    CT: cure termali
                    PI: protesica e integrativa
                    IC: chirurgua estetica
                    AA: altre spese
                */
                flagTipoSpesa : flag_tipo_spesa ? flag_tipo_spesa : null,
                importo: invoice.total ? formatta_euro(invoice.total) : null
            };

             linee.push(spesa);
        }

            
        //}

                       
        
        return linee;
    }
    else
        return null;   
};


exports.generaTsBase = function(dati, invoices, tipo_spesa, operazione, callback_) {
    var async = require('async');


    async.parallel({
        ts_final: function(callback) {
            var tsBase = {
                'precompilata': {
                    '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                    '@xsi:noNamespaceSchemaLocation': '730_precompilata.xsd',

                    proprietario: getProprietario(dati),
                    documentoSpesa: getDocumentiSpesa(invoices, tipo_spesa, operazione)
                        
                }
            };

            callback(null, tsBase);

        }
    }, function(err, risultato) {

        if (err)
            console.log(err);
        else {
            //risultato.fattura['v1:FatturaElettronica'].FatturaElettronicaBody.Allegati = risultato.allegati;

        }

        callback_(null, risultato.ts_final);
    });
};

exports.generaXmlTs = function(Ts) {
    var xmlbuilder = require('xmlbuilder');
    var TsXml = xmlbuilder.create(Ts, { encoding: 'UTF-8' }, { skipNullNodes: true, skipNullAttributes: true });

    return TsXml.end({ pretty: true, allowEmpty: true });
};


