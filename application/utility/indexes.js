 var async = require('async');
 var Sequelize = require('sequelize');
 var utility = require('../utility/utility.js');
 var util = require('util');
 var moment = require('moment');

 exports.contact = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'contact', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT contacts_data_v10(' +
                 parameters.page_count + ',' +
                 (parameters.page_num - 1) * parameters.page_count + ',' +
                 req.user._tenant_id + ',' +
                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                 parameters.id + ',' +
                 parameters.sortDirection + ',' +
                 req.user.id + ',\'' +
                 req.headers['host'].split(".")[0] + '\',' +
                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                 req.user.role_id + ',' +
                 parameters.role + ',' +
                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                 parameters.all_any + ',' +
                 parameters.archivied + ',' +
                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                 parameters.notes + ',' +
                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ',' +
                 parameters.gdpr + ','+
                 parameters.sortField + ');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {

                 async.parallel({
                     data: function(callback) {
                         sequelize.query('SELECT all_contacts_from_organization_v8(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 req.user.id + ',' +
                                 parameters.sortDirection + ',' +
                                 req.user.role_id + ',' +
                                 parameters.role + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 parameters.notes + ',' +
                                 parameters.gdpr + ');', {
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(counter) {
                                 if (datas[0].contacts_data_v10 && datas[0].contacts_data_v10 != null) {

                                     var response = {
                                         'data': datas[0].contacts_data_v10,
                                         'meta': {
                                             'total_items': parseInt((counter[0].all_contacts_from_organization_v8.tot_items ? counter[0].all_contacts_from_organization_v8.tot_items : 0)),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_contacts_from_organization_v8.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_all_number': parseInt(counter[0].all_contacts_from_organization_v8.tot_items_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 } else {

                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt((counter[0].all_contacts_from_organization_v8.tot_items ? counter[0].all_contacts_from_organization_v8.tot_items : 0)),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_contacts_from_organization_v8 / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_all_number': parseInt(counter[0].all_contacts_from_organization_v8.tot_items_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 }
                             }).catch(function(err) {
                                 callback(err, null)
                             });

                     },
                     included: function(callback) {
                         sequelize.query('SELECT contacts_included_v10(' +
                                 parameters.page_count + ',' +
                                 (parameters.page_num - 1) * parameters.page_count + ',' +
                                 req.user._tenant_id + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 parameters.sortDirection + ',' +
                                 req.user.id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 req.user.role_id + ',' +
                                 parameters.role + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 parameters.notes + ',' +
                                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ',' +
                                 parameters.gdpr + ','+
                                 parameters.sortField + ');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(included) {

                                 if (included[0].contacts_included_v10 && included[0].contacts_included_v10 != null) {
                                     callback(null, included[0].contacts_included_v10);

                                 } else {

                                     callback(null, []);
                                 }
                             }).catch(function(err) {
                                 console.log(err);
                                 callback(err, null);
                             });

                     }
                 }, function(err, result) {
                     if (err) {
                         console.log(err);
                         //return res.status(400).render('invoice_index: '+err);
                     }
                     if (result.data.length == 0 || !result.included) {
                         callback_(null, utility.error404('id', 'Nessun contatto trovato', 'Nessun contatto trovato', '404'));
                     }

                     var response = {
                         'data': utility.check_find_datas(result.data.data, 'contacts', []),
                         'included': utility.check_find_included(result.included, 'contacts', ['contact_states', 'custom_fields', 'products', 'projects']),
                         'meta': result.data.meta
                     };

                     callback_(null, response);

                 });

             }).catch(function(err) {
                 callback_(err, null);
             });

     });
 };

 exports.event = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'event', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }
         async.parallel({
             meta: function(callback){
                sequelize.query('SELECT all_events_from_organization_v9(' +
                     req.user._tenant_id + ',\'' +
                     req.headers['host'].split(".")[0] + '\',' +
                     (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                     parameters.id + ',' +
                     parameters.start_date + ', ' +
                     parameters.end_date + ',' +
                     req.user.id + ',' +
                     parameters.sortDirection + ',' +
                     (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                     (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                     (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                     (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                     parameters.sortField + ',' +
                     (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                     req.user.role_id + ',' +
                     parameters.all_any + ',' +
                     (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                     parameters.archivied + ',' +
                     (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                     parameters.is_dispo + ',' +
                     (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                     (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ');')
                 .then(function(counter) {
                     var response = {
                            
                        
                                 'total_items': parseInt(counter[0][0].all_events_from_organization_v9.tot_items),
                                 'actual_page': parseInt(parameters.page_num),
                                 'total_pages': parseInt(counter[0][0].all_events_from_organization_v9.tot_items / parameters.page_count) + 1,
                                 'item_per_page': parseInt(parameters.page_count),
                                 'tot_all_number': parseInt(counter[0][0].all_events_from_organization_v9.tot_items_tutti)
                         };
                    callback(null, response);

                 }).catch(function(err) {
                     console.log(err);
                     callback(err, null);
                 });
             },
             data: function(callback) {
                 
                 sequelize.query('SELECT events_data_v1(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.id + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         parameters.sortDirection + ',' +
                         req.user.id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                         req.user.role_id + ',' +
                         parameters.all_any + ',' +
                         (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                         parameters.archivied + ',' +
                         (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                         parameters.is_dispo + ',' +
                         (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                         (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                         parameters.notes + ',' +
                         (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ','+
                         (parameters.exclude_ids && parameters.exclude_ids.length > 0 ? 'ARRAY[' + parameters.exclude_ids + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(datas) {
                         callback(null, datas[0].events_data_v1);                         

                     }).catch(function(err) {
                         console.log(err);
                         callback(err, null);
                     });
             },
             included: function(callback) {
                 sequelize.query('SELECT events_included_v1(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.id + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         parameters.sortDirection + ',' +
                         req.user.id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                         req.user.role_id + ',' +
                         parameters.all_any + ',' +
                         (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                         parameters.archivied + ',' +
                         (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                         parameters.is_dispo + ',' +
                         (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                         (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                         parameters.notes + ',' +
                         (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ','+
                         (parameters.exclude_ids && parameters.exclude_ids.length > 0 ? 'ARRAY[' + parameters.exclude_ids + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(included) {

                         if (included[0].events_included_v1 && included[0].events_included_v1 != null) {
                             callback(null, included[0].events_included_v1);

                         } else {

                             callback(null, []);
                         }
                     }).catch(function(err) {
                         console.log(err);
                         callback(err, null);
                     });
             }
         }, function(err, result) {
             if (err) {
                 callback_(err, null);
             }

            
             if (!result.included) {
                 callback_(null, utility.error404('id', 'Nessun evento trovato', 'Nessun evento trovato', '404'));
             }else{
                if(!result.data || result.data.length == 0){
                     var response = {
                         'data': [],
                         'included': [],
                         'meta': result.meta

                     };
                }else{

                     var response = {
                         'data': utility.check_find_datas(result.data, 'events', []),
                         'included': utility.check_find_included(result.included, 'events', ['products', 'projects', 'payments', 'addresses', 'reminders', 'event_states', 'contacts', 'workhours', 'invoices', 'expenses', 'custom_fields']),
                         'meta': result.meta

                     };
                }
                 
                 callback_(null, response);
             }

         });
     });
 };

 exports.custom_field_definition = function(req, results, callback_) {

     utility.get_parameters(req, 'custom_field_definition', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }
         async.parallel({
             data: function(callback) {
                 var response = {};
                 sequelize.query('SELECT custom_field_definitions_data(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.id + ',' +
                         req.user.id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         req.user.role_id + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(datas) {

                
                             response = {
                                 'data': datas[0].custom_field_definitions_data
                             };

                             callback(null, response);
                                

                     }).catch(function(err) {
                         console.log(err);
                         callback(err, null);
                     });
             }
         }, function(err, result) {
             if (err) {
                 callback_(err, null);
             }

             if (!result.data || result.data.length == 0) {
                 var response = {
                    'data': []
                 };

                 callback_(null, response);
            }else{ 
                var response = {
                 'data': utility.check_find_datas(result.data.data, 'custom_field_definitions', ['organization_custom_fields'])
                };

                 callback_(null, response);

            }

            

         });
     });
 };

 exports.expense = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'expense', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }
         async.parallel({
             data: function(callback) {
                 var response = {};
                 sequelize.query('SELECT expenses_data_v1(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         req.user.id + ',' +
                         parameters.sortDirection + ',' +
                         parameters.id + ',' +
                         parameters.string_status + ',' +
                         parameters.category + ',' +
                         parameters.is_paid + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         req.user.role_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.all_any + ',' +
                         parameters.archivied + ',' +
                         (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                         parameters.success + ','+
                         (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ',' +
                         (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                         (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(datas) {
                         sequelize.query('SELECT all_expenses_from_organization_v1(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 parameters.id + ',' +
                                 parameters.string_status + ',' +
                                 parameters.category + ',' +
                                 parameters.is_paid + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 req.user.role_id + ',' +
                                 req.user.id + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 parameters.success + ','+
                                 (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                                 (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(counter) {
                                 if (datas[0].expenses_data_v1 && datas[0].expenses_data_v1 != null) {
                                     response = {
                                         'data': datas[0].expenses_data_v1,
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_expenses_from_organization_v1.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_expenses_from_organization_v1.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count)
                                         }
                                     };
                                     callback(null, response);
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_expenses_from_organization_v1.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_expenses_from_organization_v1.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count)
                                         }
                                     };
                                     callback(null, response);
                                 }
                             }).catch(function(err) {
                                console.log(err);
                                 callback(err, null);
                             });

                     }).catch(function(err) {
                         callback(err, null);
                     });
             },
             included: function(callback) {
                 sequelize.query('SELECT expenses_included_v1(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         req.user.id + ',' +
                         parameters.sortDirection + ',' +
                         parameters.id + ',' +
                         parameters.string_status + ',' +
                         parameters.category + ',' +
                         parameters.is_paid + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         req.user.role_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.all_any + ',' +
                         parameters.archivied + ',' +
                         (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                         parameters.success + ','+
                         (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ',' +
                         (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                         (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(included) {

                         if (included[0].expenses_included_v1 && included[0].expenses_included_v1 != null) {
                             callback(null, included[0].expenses_included_v1);

                         } else {

                             callback(null, []);
                         }
                     }).catch(function(err) {
                         callback(err, null);
                     });
             }
         }, function(err, result) {
             if (err) {

                 callback_(err, null);
             }
             if (result.data.length == 0 || !result.included) {
                 callback_(utility.error404('id', 'Nessun acquisto trovato', 'Nessun acquisto trovato', '404'), null);
             }

             var response = {
                 'data': utility.check_find_datas(result.data.data, 'expenses', []),
                 'included': utility.check_find_included(result.included, 'expenses', ['contacts', 'expensearticles']),
                 'meta': result.data.meta
             };

             callback_(null, response);

         });
     });
 };

 exports.invoice = function(req, results, callback_) {
    
     utility.get_parameters_v1(req, 'invoice', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }
         async.parallel({
             data: function(callback) {
                 var response = {};
                 sequelize.query('SELECT invoices_data(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         req.user.id + ',' +
                         parameters.sortDirection + ',' +
                         parameters.id + ',' +
                         parameters.string_status + ',' +
                         parameters.estimate + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         parameters.user_id + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         req.user.role_id + ',' +
                         parameters.all_any + ',' +
                         parameters.archivied + ',' +
                         (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                         parameters.document_type + ',' +
                         parameters.success + ','+
                         (parameters.from_ids && parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ','+
                         parameters.sdi_status + ',' +
                         (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                         (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ','+
                         parameters.sezionale + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(datas) {
                         sequelize.query('SELECT all_invoices_from_organization(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 parameters.id + ',' +
                                 parameters.string_status + ',' +
                                 parameters.estimate + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 parameters.user_id + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 req.user.role_id + ',' +
                                 req.user.id + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 parameters.document_type + ','+
                                 parameters.success + ','+
                                 (parameters.from_ids && parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ','+
                                 parameters.sdi_status + ',' +
                                 (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                                 (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ','+
                                 parameters.sezionale + ');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(counter) {
                                 if (datas[0].invoices_data && datas[0].invoices_data != null) {
                                     response = {
                                         'data': datas[0].invoices_data,
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_invoices_from_organization.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_invoices_from_organization.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'assurance_diretta': counter[0].all_invoices_from_organization.assurance_diretta
                                         }
                                     };
                                     callback(null, response);
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_invoices_from_organization.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_invoices_from_organization.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'assurance_diretta': counter[0].all_invoices_from_organization.assurance_diretta
                                         }
                                     };
                                     callback(null, response);
                                 }
                             }).catch(function(err) {
                                console.log(err);
                                 callback(err, null);
                             });


                     }).catch(function(err) {
                         callback(err, null);
                     });
             }
         }, function(err, result) {
             if (err) {
                 callback_(err, null);
             }
             if (result.data.length == 0) {
                 callback_(null, utility.error404('id', 'Nessuna fattura trovata', 'Nessuna fattura trovata', '404'));
             }

             var response = {
                 'data': utility.check_find_datas(result.data.data, 'invoices', []),
                 'included': [],
                 'meta': result.data.meta
             };

             callback_(null, response);

         });
     });
 };

 exports.payment = function(req, results, callback_) {
     utility.get_parameters_v1(req, 'payment', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

         
         async.parallel({
             data: function(callback) {
                 var response = {};
                 sequelize.query('SELECT payments_data_v1(' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         (parameters.transaction_id && parameters.transaction_id !== null ? ('\'' + parameters.transaction_id.replace(/'/g, "''''") + '\'') : null) + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                         (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         parameters.success + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         parameters.sortDirection + ',' +
                         parameters.sent + ',' +
                         parameters.sortField + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         req.user.role_id + ',' +
                         req.user.id + ',' +
                         parameters.all_any + ',' +
                         parameters.residuo + ',' +
                         parameters.archivied + ',' +
                         parameters.verify + ','+
                         parameters.voucher+','+
                         (parameters.assurance_ids.length > 0 ? 'ARRAY[' + parameters.assurance_ids + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(datas) {
                         sequelize.query('SELECT all_payments_from_organization_v1(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (parameters.transaction_id !== null ? ('\'' + parameters.transaction_id.replace(/'/g, "''''") + '\'') : null) + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                                 parameters.success + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 parameters.sent + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 req.user.role_id + ',' +
                                 req.user.id + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ','+
                                 parameters.voucher+','+
                                 (parameters.assurance_ids.length > 0 ? 'ARRAY[' + parameters.assurance_ids + ']::bigint[]' : null) + ');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(counter) {
                                 if (datas[0].payments_data_v1 && datas[0].payments_data_v1 != null) {
                                     response = {
                                         'data': datas[0].payments_data_v1,
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_payments_from_organization_v1.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_payments_from_organization_v1.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count)
                                         }
                                     };
                                     callback(null, response);
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_payments_from_organization_v1.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_payments_from_organization_v1.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count)
                                         }
                                     };
                                     callback(null, response);
                                 }


                             }).catch(function(err) {
                                 console.log(err);
                                 callback(err, null);
                             });
                     }).catch(function(err) {
                         console.log(err);
                         callback(err, null);
                     });
             },
             included: function(callback) {
                 sequelize.query('SELECT payments_included_v1(' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         (parameters.transaction_id !== null ? ('\'' + parameters.transaction_id.replace(/'/g, "''''") + '\'') : null) + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                         (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         parameters.success + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         parameters.sortDirection + ',' +
                         parameters.sent + ',' +
                         parameters.sortField + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         req.user.role_id + ',' +
                         req.user.id + ',' +
                         parameters.all_any + ',' +
                         parameters.residuo + ',' +
                         parameters.archivied + ',' +
                         parameters.verify + ','+
                         parameters.voucher+','+
                         (parameters.assurance_ids.length > 0 ? 'ARRAY[' + parameters.assurance_ids + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(included) {

                         if (included[0].payments_included_v1 && included[0].payments_included_v1 != null) {
                             callback(null, included[0].payments_included_v1);

                         } else {

                             callback(null, []);
                         }
                     }).catch(function(err) {
                         callback(err, null);
                     });
             }
         }, function(err, result) {
             if (err) {
                 callback_(err, null);
             }
             if (result.data.length == 0 || !result.included) {
                 callback_(null, utility.error404('id', 'Nessun pagamento trovato', 'Nessun pagamento trovato', '404'));
             }

             var response = {
                 'data': utility.check_find_datas(result.data.data, 'payments', []),
                 'included': utility.check_find_included(result.included, 'payments', ['products', 'projects', 'events', 'files', 'tags', 'contacts', 'invoices', 'expenses','assurances']),
                 'meta': result.data.meta
             };

             callback_(null, response);

         });
     });
 };

 exports.product = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'product', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT products_data_v8(' +
                 parameters.page_count + ',' +
                 (parameters.page_num - 1) * parameters.page_count + ',' +
                 req.user._tenant_id + ',' +
                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                 parameters.id + ',' +
                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                 parameters.sortDirection + ',' +
                 req.user.id + ',\'' +
                 req.headers['host'].split(".")[0] + '\',' +
                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                 req.user.role_id + ',' +
                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                 parameters.all_any + ',' +
                 parameters.archivied + ',' +
                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ','+
                 parameters.sortField + ');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {
                 async.parallel({
                     data: function(callback) {
                         sequelize.query('SELECT all_products_from_organization_v6(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 req.user.id + ',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 req.user.role_id + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ');')
                             .then(function(counter) {
                                 if (datas[0].products_data_v8 && datas[0].products_data_v8 != null) {
                                     response = {
                                         'data': datas[0].products_data_v8,
                                         'meta': {
                                             'total_items': parseInt(counter[0][0].all_products_from_organization_v6.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0][0].all_products_from_organization_v6.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_all_number': parseInt(counter[0][0].all_products_from_organization_v6.tot_items_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0][0].all_products_from_organization_v6.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0][0].all_products_from_organization_v6 / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_all_number': parseInt(counter[0][0].all_products_from_organization_v6.tot_items_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 }
                             }).catch(function(err) {
                                 callback(err, null);
                             });
                     },
                     included: function(callback) {
                         sequelize.query('SELECT products_included_v8(' +
                                 parameters.page_count + ',' +
                                 (parameters.page_num - 1) * parameters.page_count + ',' +
                                 req.user._tenant_id + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 parameters.sortDirection + ',' +
                                 req.user.id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 req.user.role_id + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ','+
                                 parameters.sortField+');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(included) {

                                 if (included[0].products_included_v8 && included[0].products_included_v8 != null) {
                                     callback(null, included[0].products_included_v8);

                                 } else {

                                     callback(null, []);
                                 }
                             }).catch(function(err) {
                                 console.log(err);
                                 callback(err, null);
                             });

                     }
                 }, function(err, result) {
                     if (err) {
                         callback_(err, null);
                     }
                     if (result.data.length == 0 || !result.included) {
                         callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                     }

                     var response = {
                         'data': utility.check_find_datas(result.data.data, 'products', []),
                         'included': utility.check_find_included(result.included, 'products', ['product_states', 'projects', 'custom_fields', 'contacts']),
                         'meta': result.data.meta
                     };

                     callback_(null, response);

                 });

             }).catch(function(err) {
                 callback_(err, null);
             });
     });
 };

 exports.project = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'project', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT projects_data_v8(' +
                 parameters.page_count + ',' +
                 (parameters.page_num - 1) * parameters.page_count + ',' +
                 req.user._tenant_id + ',' +
                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                 parameters.id + ',' +
                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                 parameters.sortDirection + ',' +
                 req.user.id + ',\'' +
                 req.headers['host'].split(".")[0] + '\',' +
                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                 req.user.role_id + ',' +
                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                 parameters.start_date + ',' +
                 parameters.end_date + ',' +
                 parameters.sortField + ',' +
                 parameters.all_any + ',' +
                 parameters.archivied + ',' +
                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ',' +
                 parameters.color + ');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {
                 async.parallel({
                     data: function(callback) {
                         sequelize.query('SELECT all_projects_from_organization_v7(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 req.user.id + ',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 req.user.role_id + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 parameters.color + ');')
                             .then(function(counter) {
                                 if (datas[0].projects_data_v8 && datas[0].projects_data_v8 != null) {
                                     response = {
                                         'data': datas[0].projects_data_v8,
                                         'meta': {
                                             'total_items': parseInt(counter[0][0].all_projects_from_organization_v7.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0][0].all_projects_from_organization_v7.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_price': parseFloat(counter[0][0].all_projects_from_organization_v7.tot_pagato),
                                             'tot_cost': parseFloat(counter[0][0].all_projects_from_organization_v7.tot_dovuto),
                                             'tot_all_number': parseInt(counter[0][0].all_projects_from_organization_v7.tot_items_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0][0].all_projects_from_organization_v7.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0][0].all_projects_from_organization_v7.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_price': parseFloat(counter[0][0].all_projects_from_organization_v7.tot_pagato),
                                             'tot_cost': parseFloat(counter[0][0].all_projects_from_organization_v7.tot_dovuto),
                                             'tot_all_number': parseInt(counter[0][0].all_projects_from_organization_v7.tot_items_tutti)

                                         }
                                     };
                                     callback(null, response);
                                 }
                             }).catch(function(err) {
                                 callback(err, null);
                             });
                     },
                     included: function(callback) {
                         sequelize.query('SELECT projects_included_v8(' +
                                 parameters.page_count + ',' +
                                 (parameters.page_num - 1) * parameters.page_count + ',' +
                                 req.user._tenant_id + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 parameters.sortDirection + ',' +
                                 req.user.id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 req.user.role_id + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 parameters.sortField + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ',' +
                                 parameters.color + ');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(included) {

                                 if (included[0].projects_included_v8 && included[0].projects_included_v8 != null) {
                                     callback(null, included[0].projects_included_v8);

                                 } else {

                                     callback(null, []);
                                 }
                             }).catch(function(err) {
                                 callback(err, null);
                             });

                     }
                 }, function(err, result) {
                     if (err) {
                         callback_(err, null);
                     }
                     if (result.data.length == 0 || !result.included) {
                         callback_(null, utility.error404('id', 'Nessun progetto trovato', 'Nessun progetto trovato', '404'));
                     }

                     var response = {
                         'data': utility.check_find_datas(result.data.data, 'projects', []),
                         'included': utility.check_find_included(result.included, 'projects', ['project_states', 'contacts', 'products', 'custom_fields']),
                         'meta': result.data.meta
                     };

                     callback_(null, response);
                 });

             }).catch(function(err) {
                 callback_(err, null);
             });
     });
 };

 exports.workhour = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'workhour', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }
         async.parallel({
             data: function(callback) {
                 var response = {};
                 sequelize.query('SELECT workhours_data_v7(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         req.user.id + ',' +
                         parameters.sortDirection + ',' +
                         parameters.id + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         parameters.duration + ',' +
                         parameters.running + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                         req.user.role_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.all_any + ',' +
                         parameters.archivied + ',' +
                         (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                         (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                         (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(datas) {
                         sequelize.query('SELECT all_workhours_from_organization_v7(' +
                                 req.user._tenant_id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 req.user.id + ',' +
                                 parameters.id + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 parameters.duration + ',' +
                                 parameters.running + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 req.user.role_id + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                 parameters.all_any + ',' +
                                 parameters.archivied + ',' +
                                 (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                 (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ');', {
                                     raw: true,
                                     type: Sequelize.QueryTypes.SELECT
                                 })
                             .then(function(counter) {
                                 if (datas[0].workhours_data_v7 && datas[0].workhours_data_v7 != null) {
                                     response = {
                                         'data': datas[0].workhours_data_v7,
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_workhours_from_organization_v7.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_workhours_from_organization_v7.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'total_duration': parseInt(counter[0].all_workhours_from_organization_v7.tot_duration),
                                             'tot_all_number': parseInt(counter[0].all_workhours_from_organization_v7.tot_items_tutti),
                                             'tot_all_duration': parseInt(counter[0].all_workhours_from_organization_v7.tot_duration_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0].all_workhours_from_organization_v7.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0].all_workhours_from_organization_v7.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'total_duration': parseInt(counter[0].all_workhours_from_organization_v7.tot_duration),
                                             'tot_all_number': parseInt(counter[0].all_workhours_from_organization_v7.tot_items_tutti),
                                             'tot_all_duration': parseInt(counter[0].all_workhours_from_organization_v7.tot_duration_tutti)
                                         }
                                     };
                                     callback(null, response);
                                 }
                             }).catch(function(err) {
                                 console.log(err);
                                 callback(err, null);
                             });

                     }).catch(function(err) {
                         callback(err, null);
                     });
             },
             included: function(callback) {
                 sequelize.query('SELECT workhours_included_v7(' +
                         parameters.page_count + ',' +
                         (parameters.page_num - 1) * parameters.page_count + ',' +
                         req.user._tenant_id + ',\'' +
                         req.headers['host'].split(".")[0] + '\',' +
                         req.user.id + ',' +
                         parameters.sortDirection + ',' +
                         parameters.id + ',' +
                         parameters.start_date + ',' +
                         parameters.end_date + ',' +
                         (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                         (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                         parameters.duration + ',' +
                         parameters.running + ',' +
                         (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                         parameters.sortField + ',' +
                         (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                         req.user.role_id + ',' +
                         (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                         parameters.all_any + ',' +
                         parameters.archivied + ',' +
                         (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                         (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                         (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ');', {
                             raw: true,
                             type: Sequelize.QueryTypes.SELECT
                         })
                     .then(function(included) {

                         if (included[0].workhours_included_v7 && included[0].workhours_included_v7 != null) {
                             callback(null, included[0].workhours_included_v7);

                         } else {

                             callback(null, []);
                         }
                     }).catch(function(err) {
                         callback(err, null);
                     });
             }
         }, function(err, result) {
             if (err) {
                 callback_(err, null);
             }
             if (result.data.length == 0 || !result.included) {
                 callback_(null, utility.error404('id', 'Nessun\'ora lavorata trovata', 'Nessun\'ora lavorata trovata', '404'));
             }

             var response = {
                 'data': utility.check_find_datas(result.data.data, 'workhours', []),
                 'included': utility.check_find_included(result.included, 'workhours', ['products', 'projects', 'contacts', 'addresses', 'workhour_states', 'events', 'custom_fields']),
                 'meta': result.data.meta
             };

             callback_(null, response);

         });
     });
 };

 exports.file = function(req, results, callback_) {

     utility.get_parameters_v1(req, 'file', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

         var to_sign = null;
         var device_uid = null;

         if ((req.query.to_sign !== undefined && req.query.to_sign)) {
           to_sign = req.query.to_sign;
         }

         if ((req.query.device_uid !== undefined && req.query.device_uid)) {
           device_uid = '\''+req.query.device_uid +'\'';
         }

         sequelize.query('SELECT signed_files_data(' +
                 parameters.page_count + ',' +
                 (parameters.page_num - 1) * parameters.page_count + ',' +
                 req.user._tenant_id + ',' +
                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                 parameters.id + ',' +
                 parameters.sortDirection + ',' +
                 parameters.sortField + ',' +
                 parameters.start_date + ',' +
                 parameters.end_date + ',' +
                 req.user.id + ',\'' +
                 req.headers['host'].split(".")[0] + '\',' +
                 req.user.role_id + ',' +
                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                 parameters.all_any + ','+
                 to_sign+','+
                 device_uid+');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {
                 async.parallel({
                    data: function(callback) {
                         sequelize.query('SELECT all_signed_files_from_organization(' +
                                 req.user._tenant_id + ',' +
                                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                 parameters.id + ',' +
                                 parameters.sortDirection + ',' +
                                 parameters.sortField + ',' +
                                 parameters.start_date + ',' +
                                 parameters.end_date + ',' +
                                 req.user.id + ',\'' +
                                 req.headers['host'].split(".")[0] + '\',' +
                                 req.user.role_id + ',' +
                                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                                 (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                 parameters.all_any + ','+
                                 to_sign+','+
                                 device_uid+');')
                             .then(function(counter) {
                                 if (datas[0].signed_files_data && datas[0].signed_files_data != null) {
                                     response = {
                                         'data': datas[0].signed_files_data,
                                         'meta': {
                                             'total_items': parseInt(counter[0][0].all_signed_files_from_organization.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0][0].all_signed_files_from_organization.tot_items / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_all_number': parseInt(counter[0][0].all_signed_files_from_organization.tot_items_tutti)
                                         }
                                     };
                                 } else {
                                     response = {
                                         'data': [],
                                         'meta': {
                                             'total_items': parseInt(counter[0][0].all_signed_files_from_organization.tot_items),
                                             'actual_page': parseInt(parameters.page_num),
                                             'total_pages': parseInt(counter[0][0].all_signed_files_from_organization / parameters.page_count) + 1,
                                             'item_per_page': parseInt(parameters.page_count),
                                             'tot_all_number': parseInt(counter[0][0].all_signed_files_from_organization.tot_items_tutti)
                                         }
                                     };
                                     
                                 }

                                 callback(null, response);
                             }).catch(function(err) {
                                 callback(err, null);
                             });
                    }
                 }, function(err, result) {
                     if (err) {
                         callback_(err, null);
                     }


                     if (!result.data || !result.data.data || !result.data.data[0]) {
                         var response = {
                             'data': [],
                             'meta': result.data.meta
                         };
                     }
                     else{
                        var response = {
                             'data': utility.check_find_datas(result.data.data, 'files', []),
                             'meta': result.data.meta
                        };
                     }
                     
                     callback_(null, response);

                 });

             }).catch(function(err) {
                 callback_(err, null);
             });
     });
 };