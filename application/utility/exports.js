var logs = require('../utility/logs.js');
var async = require('async');
var Sequelize = require('sequelize');
var utility = require('../utility/utility.js');
var util = require('util');
var moment = require('moment');
/*
 * Check the model id if is a correct number
 *
 */
exports.contact = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'contact', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT contacts_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                parameters.id + ',' +
                parameters.sortDirection + ',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                req.user.role_id + ',' +
                parameters.role + ',' +
                (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
                 parameters.notes + ',' +
                 (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ',' +
                 parameters.gdpr + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                if (datas[0].contacts_export_new && datas[0].contacts_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun contatto trovato', 'Nessun contatto trovato', '404'));
                }

                else{
                    callback_(null, datas[0].contacts_export_new[0]);
                }
               

            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.event = function(req, results, callback_) {
    var is_pagato = null;
    utility.get_parameters_v1(req, 'event', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        if (req.query.sent !== undefined && req.query.sent !== '') {
            switch (parseInt(req.query.sent)) {
                case 0:
                    is_pagato = null;
                    break;

                case 1:
                    is_pagato = true;
                    break;

                case 2:
                    is_pagato = false;
                    break;

                default:
                    is_pagato = null;
                    break;
            }

        }


        sequelize.query('SELECT events_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                parameters.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.sortDirection + ',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.sortField + ',' +
                (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                req.user.role_id + ',' +
                parameters.all_any + ',' +
                (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                parameters.archivied + ','+
                is_pagato+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                if (datas[0].events_export_new && datas[0].events_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun evento trovato', 'Nessun contatto trovato', '404'));
                }
                else
                {
                     callback_(null, datas[0].events_export_new[0]);
                }


            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.product = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'product', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT products_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                parameters.id + ',' +
                (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                parameters.sortDirection + ',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                req.user.role_id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                if (datas[0].products_export_new && datas[0].products_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }else
                {
                     callback_(null, datas[0].products_export_new[0]);
                }



            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.project = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT projects_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                parameters.id + ',' +
                (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                parameters.sortDirection + ',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                req.user.role_id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ',' +
                (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.sortField + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                (parameters.custom_ids && parameters.custom_ids.length > 0 ? 'ARRAY[' + parameters.custom_ids + ']::bigint[]' : null) + ',' +
                parameters.color + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                if (datas[0].projects_export_new && datas[0].projects_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }else
                {
                     callback_(null, datas[0].projects_export_new[0]);
                }

               

            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.workhour = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'workhour', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT workhours_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                parameters.sortDirection + ',' +
                parameters.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                parameters.duration + ',' +
                parameters.running + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.sortField + ',' +
                (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                req.user.role_id + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                if (datas[0].workhours_export_new && datas[0].workhours_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }else
                {
                     callback_(null, datas[0].workhours_export_new[0]);
                }

              

            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.payment = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'payment', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT payments_export_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (parameters.transaction_id !== null ? ('\'' + parameters.transaction_id.replace(/'/g, "''''") + '\'') : null) + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                parameters.success + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.sortDirection + ',' +
                parameters.sent + ',' +
                parameters.sortField + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                parameters.all_any + ',' +
                parameters.residuo + ',' +
                parameters.archivied + ',' +
                parameters.verify + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                if (!datas[0].payments_export_new || datas[0].payments_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }
                else{
                    callback_(null, datas[0].payments_export_new[0]);
                }



            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.invoice = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'invoice', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT invoices_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                parameters.sortDirection + ',' +
                parameters.id + ',' +
                parameters.string_status + ',' +
                parameters.estimate + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.user_id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                parameters.sortField + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                req.user.role_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                parameters.document_type + ',' +
                parameters.success + ','+
                (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ','+
                parameters.sdi_status + ',' +
                 (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                 (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ','+
                 parameters.sezionale + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                if (!datas[0].invoices_export_new || datas[0].invoices_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }
                else{
                    callback_(null, datas[0].invoices_export_new[0]);
                }


            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.expense = function(req, results, callback_) {

    utility.get_parameters_v1(req, 'expense', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT expenses_export_new(' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                parameters.sortDirection + ',' +
                parameters.id + ',' +
                parameters.string_status + ',' +
                parameters.category + ',' +
                parameters.is_paid + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                parameters.sortField + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                req.user.role_id + ',' +
                (parameters.q && parameters.q.length > 0 ? 'ARRAY[' + parameters.q + ']' : null) + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                parameters.success + ','+
                (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                if (!datas[0].expenses_export_new || datas[0].expenses_export_new.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }
                else{

                    callback_(null, datas[0].expenses_export_new[0]);
                }

            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

    });
};

exports.prima_nota = function(req, results, callback_) {

    var conto = null,
        start_date = '',
        end_date = '',
        contact_id = null,
        order_by = null;


    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.query.contact_id != null && req.query.contact_id != '' && req.query.contact_id != undefined) {
        contact_id = req.query.contact_id;
    }

    if (req.query.conto != null && req.query.conto != '') {
        conto = '\'' + req.query.conto + '\'';
    }

    if (req.query.order_by != null && req.query.order_by != '') {
        order_by = '\'' + req.query.order_by + '\'';
    }

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    sequelize.query('SELECT export_prima_nota_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id+ ','+
            start_date+','+
            end_date+','+
            conto+','+
            contact_id+','+
            order_by+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
            })
        .then(function(datas) {

       
                if (datas && datas[0] && datas[0].export_prima_nota_v1 && datas[0].export_prima_nota_v1.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }
                else{

                    callback_(null, datas[0].export_prima_nota_v1[0]);
                }
                   
                


            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

};

exports.poppix_payment = function(req, results, callback_) {

   

    sequelize.query('SELECT poppix_export_payment_expense(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id+ ','+
            req.query.payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

       
                if (datas && datas[0] && datas[0].poppix_export_payment_expense && datas[0].poppix_export_payment_expense.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun pagamento trovato', 'Nessun pagamento trovato', '404'));
                }
                else{
                    async.parallel({
                        custom: function(callback) {
                            var contacts = [];

                            var cnt_contact = 0;
                            if (datas && datas[0] && datas[0].poppix_export_payment_expense && datas[0].poppix_export_payment_expense[0] && 
                                datas[0].poppix_export_payment_expense[0].attributes && datas[0].poppix_export_payment_expense[0].attributes.length > 0) {
                                
                                datas[0].poppix_export_payment_expense[0].attributes.forEach(function(contact) {
                                    var single_contact = {};
                                    single_contact = contact;
                                   

                                    contacts.push(single_contact);
                                    if (cnt_contact++ >= datas[0].poppix_export_payment_expense[0].attributes.length - 1) {
                                
                                        callback(null, contacts);
                                    }


                                });
                            } else {
                                callback(null, []);
                            }
                        },
                        fields: function(callback) {

                            if (datas && datas[0] && datas[0].poppix_export_payment_expense && datas[0].poppix_export_payment_expense.length > 0) {

                                var fields = datas[0].poppix_export_payment_expense[0].attributes[0].fields;
                                
                                callback(null, fields);

                            }
                            else{
                                callback(null, null);
                            }

                            

                        },
                        field_names: function(callback) {
                            if (datas && datas[0] && datas[0].poppix_export_payment_expense && datas[0].poppix_export_payment_expense.length > 0) {

                                var field_names = datas[0].poppix_export_payment_expense[0].attributes[0].field_names;
                                
                                callback(null, field_names);
                            }
                            else{
                                callback(null, null);
                            }

                        }

                    }, function(err, finals) {
                        if (err) {
                            console.log(err);
                        }


                        var response = {
                            'data': finals.custom,
                            'fields': finals.fields,
                            'field_names': finals.field_names
                        };


                        callback_(null, response);

                    });
                }
                


            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

};

exports.ritenute = function(req, results, callback_) {

    var util = require('util');
    var Sequelize = require('sequelize'),
        start_date = '',
        end_date = '',
        contact_ids = [];

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        if (util.isArray(req.query.contact_id)) {
            contact_ids = req.query.contact_id;
        } else {
            contact_ids.push(req.query.contact_id);
        }
    }

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    sequelize.query('SELECT export_ritenute_acconto(' +
            req.user._tenant_id + ',' +
            start_date+','+
            end_date+',\''+
            req.headers['host'].split(".")[0] + '\',' +
            (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

       
                if (datas && datas[0] && datas[0].export_ritenute_acconto && datas[0].export_ritenute_acconto.length == 0) {
                    callback_(null, utility.error404('id', 'Nessun prodotto trovato', 'Nessun prodotto trovato', '404'));
                }
                else{
                    async.parallel({
                        custom: function(callback) {
                            var contacts = [];

                            var cnt_contact = 0;
                            if (datas && datas[0] && datas[0].export_ritenute_acconto && datas[0].export_ritenute_acconto  && datas[0].export_ritenute_acconto.length > 0) {
                                
                                datas[0].export_ritenute_acconto.forEach(function(contact) {
                                    var single_contact = {};
                                    single_contact = contact.attributes;
                                    /*single_contact['society'] = contact.attributes.society;
                                    single_contact['society_piva_name'] = contact.attributes.society_piva_name;
                                    single_contact['society_piva'] = contact.attributes.society_piva;
                                    single_contact['indirizzo_mittente'] = contact.attributes.indirizzo_mittente;
                                    single_contact['imponibile'] = contact.attributes.imponibile;
                                    single_contact['total'] = contact.attributes.total;
                                    single_contact['tot_pagare'] = contact.attributes.tot_pagare;
                                    single_contact['status'] = contact.attributes.status;
                                    single_contact['contact_name'] = contact.attributes.contact_name;
                                    single_contact['contact_piva'] = contact.attributes.contact_piva;
                                    single_contact['indirizzo_destinatario'] = contact.attributes.indirizzo_destinatario;
                                    single_contact['estimate'] = contact.attributes.estimate;
                                    single_contact['lordo'] = contact.attributes.lordo;
                                    single_contact['number'] = contact.attributes.number;
                                    single_contact['date'] = contact.attributes.date ? moment(contact.attributes.date).format('YYYY-MM-DD HH:mm:ss') : '';
                                    single_contact['archivied'] = contact.attributes.archivied;
                                    single_contact['created_at'] = moment(contact.attributes.created_at).format('YYYY-MM-DD HH:mm:ss');
                                    single_contact['stato'] = contact.attributes.workhour_status_name;
                                    single_contact['prodotto'] = contact.attributes.prodotto;
                                    single_contact['contatti'] = contact.attributes.contatti ? JSON.stringify(contact.attributes.contatti).replace(/[\[\]"]/g, '') : '';
                                    single_contact['progetto'] = contact.attributes.progetto;
                                    single_contact['pagamenti_date'] = contact.attributes.pagamenti_date;
                                    single_contact['pagamenti_total'] = contact.attributes.pagamenti_total;
                                    single_contact['pagamenti_method'] = contact.attributes.pagamenti_method;
                                    single_contact['document_type'] = contact.attributes.document_type;*/


                                    contacts.push(single_contact);
                                    if (cnt_contact++ >= datas[0].export_ritenute_acconto.length - 1) {
                                
                                        callback(null, contacts);
                                    }


                                });
                            } else {
                                callback(null, []);
                            }
                        },
                        fields: function(callback) {

                            if (datas && datas[0] && datas[0].export_ritenute_acconto && datas[0].export_ritenute_acconto.length > 0) {

                                var fields = datas[0].export_ritenute_acconto[0].attributes.fields;
                                
                                callback(null, fields);

                            }
                            else{
                                callback(null, null);
                            }

                            

                        },
                        field_names: function(callback) {
                            if (datas && datas[0] && datas[0].export_ritenute_acconto && datas[0].export_ritenute_acconto.length > 0) {

                                var field_names = datas[0].export_ritenute_acconto[0].attributes.field_names;
                                
                                callback(null, field_names);
                            }
                            else{
                                callback(null, null);
                            }

                        }

                    }, function(err, finals) {
                        if (err) {
                            console.log(err);
                        }


                        var response = {
                            'data': finals.custom,
                            'fields': finals.fields,
                            'field_names': finals.field_names
                        };


                        callback_(null, response);

                    });
                }
                


            }).catch(function(err) {
                console.log(err);
                callback_(err, null);
            });

};