var moment = require('moment');
var util = require('util');

function togli_caratteri(s) {
    var r = s.toLowerCase();
    r = r.replace(new RegExp(/\s/g), "");
    r = r.replace(new RegExp(/[àáâãäå]/g), "a");
    r = r.replace(new RegExp(/æ/g), "ae");
    r = r.replace(new RegExp(/ç/g), "c");
    r = r.replace(new RegExp(/[èéêë]/g), "e");
    r = r.replace(new RegExp(/[ìíîï]/g), "i");
    r = r.replace(new RegExp(/ñ/g), "n");
    r = r.replace(new RegExp(/[òóôõö]/g), "o");
    r = r.replace(new RegExp(/œ/g), "oe");
    r = r.replace(new RegExp(/[ùúûü]/g), "u");
    r = r.replace(new RegExp(/[ýÿ]/g), "y");
    r = r.replace(new RegExp(/\W/g), "");
    return r;
};

function formatta_euro(number) {

    if (number) {
        var numberStr = parseFloat(number).toFixed(2).toString();
        var numFormatDec = numberStr.slice(-2);
        numberStr = numberStr.substring(0, numberStr.length - 3);
        var numFormat = [];
        while (numberStr.length > 3) {
            numFormat.unshift(numberStr.slice(-3));
            numberStr = numberStr.substring(0, numberStr.length - 3);
        }
        numFormat.unshift(numberStr);
        return numFormat.join('') + '.' + numFormatDec;
    }

    return '0.00';
};

function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
};

function calcolo_corretto_imponibile_iva(rate, natura, linee, previdenza) {
    var imponibile = 0.00;

    if(parseInt(rate) != 0){
        if (util.isArray(linee)) {
            linee.forEach(function(linea) {
                if (linea['AliquotaIVA'] / 100 == rate) {
                    imponibile += parseFloat(linea['PrezzoTotale']);
                }
            });
        } else {
            if (linee['AliquotaIVA'] / 100 == rate) {
                imponibile += parseFloat(linee['PrezzoTotale']);
            }
        }

        if (previdenza) {
            if (util.isArray(previdenza)) {
                previdenza.forEach(function(previd) {
                    if (previd['AliquotaIVA'] / 100 == rate) {
                        imponibile += parseFloat(previd['ImportoContributoCassa']);
                    }
                });
            } else {
                if (previdenza['AliquotaIVA'] / 100 == rate) {
                    imponibile += parseFloat(previdenza['ImportoContributoCassa']);
                }
            }
        }
    }
    else{
        if (util.isArray(linee)) {
            linee.forEach(function(linea) {

                if (linea['AliquotaIVA'] / 100 == rate && linea['Natura'] == natura) {
                    
                    imponibile += parseFloat(linea['PrezzoTotale']);
                  
                }
            });
        } else {
            if (linee['AliquotaIVA'] / 100 == rate && linee['Natura'] == natura) {
               
                imponibile += parseFloat(linee['PrezzoTotale']);
                
            }
        }

        if (previdenza) {
            if (util.isArray(previdenza)) {
                previdenza.forEach(function(previd) {
                    if (previd['AliquotaIVA'] / 100 == rate && previd['Natura'] == natura) {
                        imponibile += parseFloat(previd['ImportoContributoCassa']);
                    }
                });
            } else {
                if (previdenza['AliquotaIVA'] / 100 == rate && previdenza['Natura'] == natura) {
                    
                    imponibile += parseFloat(previdenza['ImportoContributoCassa']);
                   
                }
            }
        }
    }
    

    return formatta_euro(imponibile);
};

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
};

function checkContainNumber(str) {
    return str.match(/[0-9]/i);
};

function getNazione(utenza, invoice){
    var address = null;
    var nazione = '';


    if(utenza == 'cedente'){
        address = invoice.included.filter(function(i) {
            return i.type === 'address' && i.id === invoice.data.attributes.society_address_id;
        });
    }else{
        var address = invoice.included.filter(function(i) {
            return i.type === 'address' && i.id === invoice.data.attributes.contact_address_id;
        });
    }

   
    
    if (address && address[0] && address[0].attributes) {
        nazione = address[0].attributes.location_address_country_code ? address[0].attributes.location_address_country_code : null;
        if (nazione) {
            return nazione;

        } else {
            return 'IT';
        }
    } else {
        return 'IT';
    }
}

function getIdFiscaleIVA(piva, utente, invoice) {
    if (!piva) { return null; }

    if (isLetter(piva[0]) && isLetter(piva[1])) {
        return {
            IdPaese: piva.substring(0, 2),
            IdCodice: piva.substring(2)
        };
    }
   
     
    return {
        IdPaese: getNazione(utente,invoice),
        IdCodice: piva.toUpperCase()
    }
       
   
};

exports.getIdFiscaleIVAEx = function(piva, utente, invoice){
    if (!piva) { return null; }

    if (isLetter(piva[0]) && isLetter(piva[1])) {
        return {
            IdPaese: piva.substring(0, 2),
            IdCodice: piva.substring(2)
        };
    }
   
     
    return {
        IdPaese: getNazione(utente,invoice),
        IdCodice: piva.toUpperCase()
    }
};

function getPrivatoPa(invoice, user_id) {

    var contact = invoice.included.filter(function(i) {
        return i.type === 'contact' && i.id === user_id;
    });

    if (contact && contact.length > 0) {
        return contact[0].attributes.ente_pa && contact[0].attributes.ente_pa === true ? 'FPA12' : 'FPR12';
    }
};

function getIscrizioneREA(invoice) {
    var ufficio = getAddress(invoice, invoice.data.attributes.society_address_id);

    if (invoice.data.attributes.rea && ufficio.Provincia) {
        return { /* IscrizioneREA facoltativo */
            Ufficio: ufficio.Provincia,
            NumeroREA: invoice.data.attributes.rea,
            CapitaleSociale: null,
            /* CapitaleSociale facoltativo */
            /*
                SocioUnico restriction
                SU: socio unico
                SM: più soci
            */
            SocioUnico: null,
            /*SocioUnico facoltativo */
            /*
                SocioUnico restriction
                LS: in liquidazione
                LN: non in liquidazione
            */
            StatoLiquidazione: invoice.data.attributes.in_liquidazione ? 'LS' : 'LN'
        }
    } else
        return null;
};

function getContatti(invoice) {
    var contact = invoice.included.filter(function(i) {
        return i.type === 'contact' && i.id === invoice.data.attributes.user_id;
    });

    if (contact && contact.length > 0 && contact[0].attributes.telephone && contact[0].attributes.fax && contact[0].attributes.mail) {

        return { /* Contatti facoltativo */
            Telefono: contact[0].attributes.telephone ? contact[0].attributes.telephone : null,
            /* Telefono facoltativo */
            Fax: contact[0].attributes.fax ? contact[0].attributes.fax : null,
            /* Fax facoltativo */
            Email: contact[0].attributes.mail ? contact[0].attributes.mail : null /* Email facoltativo */
        }
    } else
        return null;
};

function calcolaCodiceDestinatario(invoice) {
    var cod = invoice.data.attributes.codice_destinatario ? invoice.data.attributes.codice_destinatario.toUpperCase() : null;

    if (cod) {
        return cod;
    } else {
        var address = invoice.included.filter(function(i) {
            return i.type === 'address' && i.id === invoice.data.attributes.contact_address_id;
        });

        if (address && address[0] && address[0].attributes) {
            var nazione = address[0].attributes.location_address_country_code ? address[0].attributes.location_address_country_code : null;
            if (nazione) {
                if (nazione != 'IT') {
                    return 'XXXXXXX';
                } else {
                    return '0000000';
                }

            } else {
                return '0000000';
            }
        } else {
            return '0000000';
        }

    }
};

function getTipologiaDocumento(invoice) {
    var doc_type = invoice.data.attributes.xml_document_type ? invoice.data.attributes.xml_document_type.split(':')[0] : null;

    if(doc_type){
        return doc_type;
    }else{
        return 'TD01';
    }
};

function getAddress(invoice, id) {
    var address = invoice.included.filter(function(i) {
        return i.type === 'address' && i.id === id;
    });

    if (address && address[0] && address[0].attributes) {
        return {
            Indirizzo: address[0].attributes.location_address_street ? address[0].attributes.location_address_street : null,
            NumeroCivico: address[0].attributes.location_address_house ? address[0].attributes.location_address_house : null,
            /* NumeroCivico facoltativo */
            CAP: address[0].attributes.location_address_postal_code ? address[0].attributes.location_address_postal_code : null,
            Comune: address[0].attributes.location_address_city ? address[0].attributes.location_address_city : null,
            //Provincia: address[0].attributes.location_address_county ? address[0].attributes.location_address_county : null,
            Nazione: address[0].attributes.location_address_country_code ? address[0].attributes.location_address_country_code : null
        }
    }

    return null;
};

function getIncluded(invoice, mType, id) {
    var iv = invoice.included.filter(function(i) {
        return i.type === mType && i.id === id;
    });

    return iv ? iv[0] : null;
};

function getCodiceArticolo(ivprod) {
    if (ivprod.attributes.articolo) {
        return { /* CodiceArticolo facoltativo */
            CodiceTipo: null,
            CodiceValore: ivprod.attributes.codice
        }
    } else
        return null;
};

function getAltriDatiGestionali(ivprod) {
    var linee = [];
    if (ivprod && ivprod.length > 0) {
        for(var i = 0; i < ivprod.length; i++){
            var dato_gestionale =  { /* AltriDatiGestionali facoltativo */
                TipoDato: ivprod[i].TipoDato ? ivprod[i].TipoDato : null,
                /* TipoDato facoltativo */
                RiferimentoTesto: ivprod[i].RiferimentoTesto ? ivprod[i].RiferimentoTesto : null,
                /* RiferimentoTesto facoltativo */
                RiferimentoNumero: ivprod[i].RiferimentoNumero ? ivprod[i].RiferimentoNumero : null,
                /* RiferimentoNumero facoltativo */
                RiferimentoData: ivprod[i].RiferimentoData ? ivprod[i].RiferimentoData : null
            }

            linee.push(dato_gestionale);

        }
        return linee;
        

    } else
        return null;
};

function getDocumentiCollegati(ivprod) {
    var linee = [];
    
    if (ivprod && ivprod.length > 0) {
        for(var i = 0; i < ivprod.length;i++){

           
            var dato_gestionale =  { /* AltriDatiGestionali facoltativo */
                RiferimentoNumeroLinea: ivprod[i].RiferimentoNumeroLinea ? ivprod[i].RiferimentoNumeroLinea : null,
                /* TipoDato facoltativo */
                IdDocumento: ivprod[i].IdDocumento ? ivprod[i].IdDocumento : null,
                /* RiferimentoTesto facoltativo */
                Data: ivprod[i].Data ? moment(new Date(ivprod[i].Data)).format('YYYY-MM-DD') : null,
                /* RiferimentoNumero facoltativo */
                NumItem: ivprod[i].NumItem ? ivprod[i].NumItem : null,
                CodiceCommessaConvenzione: ivprod[i].CodiceCommessaConvenzione ? ivprod[i].CodiceCommessaConvenzione : null,
                CodiceCUP: ivprod[i].CodiceCUP ? ivprod[i].CodiceCUP : null,
                CodiceCIG: ivprod[i].CodiceCIG ? ivprod[i].CodiceCIG : null,
            };

           
            linee.push(dato_gestionale);
            
        }
        return linee;
        

    } else
        return null;
};

function getDatiSAL(ivprod) {
    var linee = [];
    if (ivprod && ivprod.length > 0) {
        for(var i = 0; i < ivprod.length; i++){

           
            var dato_gestionale =  { /* AltriDatiGestionali facoltativo */
                RiferimentoFase: ivprod[i].RiferimentoFase ? ivprod[i].RiferimentoFase : null
            }

            linee.push(dato_gestionale);

        }
        return linee;
        

    } else
        return null;
};

function getDatiDDT(ivprod) {
    var linee = [];
    if (ivprod && ivprod.length > 0) {
        for(var i = 0; i < ivprod.length; i++){

           
            var dato_gestionale =  { /* AltriDatiGestionali facoltativo */
                NumeroDDT: ivprod[i].NumeroDDT ? ivprod[i].NumeroDDT : null,
                DataDDT: ivprod[i].DataDDT ? ivprod[i].DataDDT : null,
                RiferimentoNumeroLinea: ivprod[i].RiferimentoNumeroLinea ? ivprod[i].RiferimentoNumeroLinea : null
            }

            linee.push(dato_gestionale);

        }
        return linee;
        

    } else
        return null;
};

function getFatturaPrincipale(ivprod) {
    
    if(ivprod){
         var dato_gestionale =  { /* AltriDatiGestionali facoltativo */
            NumeroFatturaPrincipale: ivprod.NumeroFatturaPrincipale ? ivprod.NumeroFatturaPrincipale : null,
            DataFatturaPrincipale: ivprod.DataFatturaPrincipale ? ivprod.DataFatturaPrincipale : null
        }

        return dato_gestionale;    
    }else
        return null;
       
};

function getDatiTrasporto(ivprod){
    if(ivprod){


        var linee = {};

        var anagrafica_vettore = null;

        if(ivprod.DatiAnagraficiVettore){
            if(ivprod.DatiAnagraficiVettore.IdFiscaleIVA){
                anagrafica_vettore.IdFiscaleIVA ={ 
                    'IdPaese' : ivprod.DatiAnagraficiVettore.IdFiscaleIVA.IdPaese ? ivprod.DatiAnagraficiVettore.IdFiscaleIVA.IdPaese : null,
                    'IdCodice' : ivprod.DatiAnagraficiVettore.IdFiscaleIVA.IdCodice ? ivprod.DatiAnagraficiVettore.IdFiscaleIVA.IdCodice : null
                };
            }

            if(ivprod.DatiAnagraficiVettore.CodiceFiscale){
                anagrafica_vettore.CodiceFiscale = ivprod.DatiAnagraficiVettore.CodiceFiscale ? ivprod.DatiAnagraficiVettore.CodiceFiscale : null;
                
            }
            
            if(ivprod.DatiAnagraficiVettore.Anagrafica){
                anagrafica_vettore.Anagrafica ={ 
                    'Denominazione' : ivprod.DatiAnagraficiVettore.Anagrafica.Denominazione ? ivprod.DatiAnagraficiVettore.Anagrafica.Denominazione : null,
                    'Nome' : ivprod.DatiAnagraficiVettore.Anagrafica.Nome ? ivprod.DatiAnagraficiVettore.Anagrafica.Nome : null,
                    'Cognome' : ivprod.DatiAnagraficiVettore.Anagrafica.Cognome ? ivprod.DatiAnagraficiVettore.Anagrafica.Cognome : null,
                    'Titolo' : ivprod.DatiAnagraficiVettore.Anagrafica.Titolo ? ivprod.DatiAnagraficiVettore.Anagrafica.Titolo : null,
                    'CodEORI' : ivprod.DatiAnagraficiVettore.Anagrafica.CodEORI ? ivprod.DatiAnagraficiVettore.Anagrafica.CodEORI : null
                };
            }

            if(ivprod.DatiAnagraficiVettore.NumeroLicenzaGuida){
                anagrafica_vettore.NumeroLicenzaGuida = ivprod.DatiAnagraficiVettore.NumeroLicenzaGuida ? ivprod.DatiAnagraficiVettore.NumeroLicenzaGuida : null;
                
            }
            linee.DatiAnagraficiVettore = anagrafica_vettore;
        }

        if(ivprod.MezzoTrasporto){
            linee.MezzoTrasporto = ivprod.MezzoTrasporto ? ivprod.MezzoTrasporto : null;
        }

        if(ivprod.CausaleTrasporto){
            linee.CausaleTrasporto = ivprod.CausaleTrasporto ? ivprod.CausaleTrasporto : null;
        }

        if(ivprod.NumeroColli){
            linee.NumeroColli = ivprod.NumeroColli ? ivprod.NumeroColli : null;
        }

        if(ivprod.Descrizione){
            linee.Descrizione = ivprod.Descrizione ? ivprod.Descrizione : null;
        }

        if(ivprod.UnitaMisuraPeso){
            linee.UnitaMisuraPeso = ivprod.UnitaMisuraPeso ? ivprod.UnitaMisuraPeso : null;
        }

        if(ivprod.PesoLordo){
            linee.PesoLordo = ivprod.PesoLordo ? ivprod.PesoLordo : null;
        }

        if(ivprod.PesoNetto){
            linee.PesoNetto = ivprod.PesoNetto ? ivprod.PesoNetto : null;
        }

        if(ivprod.DataOraRitiro){
            linee.DataOraRitiro = ivprod.DataOraRitiro ? moment(new Date(ivprod.DataOraRitiro)).format('YYYY-MM-DD HH:mm:ss') : null;
        }

        if(ivprod.DataInizioTrasporto){
            linee.DataInizioTrasporto = ivprod.DataInizioTrasporto ? moment(new Date(ivprod.DataInizioTrasporto)).format('YYYY-MM-DD') : null;
        }

        if(ivprod.TipoResa){
            linee.TipoResa = ivprod.TipoResa ? ivprod.TipoResa : null;
        }

        if(ivprod.IndirizzoResa){
            
            linee.IndirizzoResa ={ 
                'Indirizzo' : ivprod.IndirizzoResa.Indirizzo ? ivprod.IndirizzoResa.Indirizzo : null,
                'NumeroCivico' : ivprod.IndirizzoResa.NumeroCivico ? ivprod.IndirizzoResa.NumeroCivico : null,
                'CAP' : ivprod.IndirizzoResa.CAP ? ivprod.IndirizzoResa.CAP : null,
                'Comune' : ivprod.IndirizzoResa.Comune ? ivprod.IndirizzoResa.Comune : null,
                'Provincia' : ivprod.IndirizzoResa.Provincia ? ivprod.IndirizzoResa.Provincia : null,
                'Nazione' : ivprod.IndirizzoResa.Nazione ? ivprod.IndirizzoResa.Nazione : null
            };
           

        }

        if(ivprod.DataOraConsegna){
            linee.DataOraConsegna = ivprod.DataOraConsegna ? moment(new Date(ivprod.DataOraConsegna)).format('YYYY-MM-DD HH:mm:ss') : null;
        }


        return linee;
    }
    else
            return null;
};

function getDatiVeicoli(ivprod){
    if(ivprod){
        var dato_gestionale =  { /* AltriDatiGestionali facoltativo */
            Data: ivprod.Data ? moment(new Date(ivprod.Data)).format('YYYY-MM-DD') : null,
            TotalePercorso: ivprod.TotalePercorso ? ivprod.TotalePercorso : null
        }

        return dato_gestionale;    
    }else
        return null;
};

function getScontoMaggiorazione(ivprod) {
    if (1 != 1) {
        return { /* ScontoMaggiorazione facoltativo */
            /*
                Tipo restriction
                SC = Sconto
                MG = Maggiorazione
            */
            Tipo: 'SC',
            Percentuale: null,
            /* Percentuale facoltativo */
            Importo: null /* Importo facoltativo */
        }
    } else
        return null;
};

function getAliquotaIva(invoice) {
    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    var iv_taxes = invoice.data.relationships.invoicetaxes.data;
    var aliquota = null;

    for (var i = 0; i < iv_taxes.length; i++) {
        var iv_tax = getIncluded(invoice, 'invoicetax', iv_taxes[i].id);
        if (iv_tax && iv_tax.attributes.is_iva) {
            aliquota = iv_tax.attributes.rate;
        }
    }

    return formatta_euro(aliquota);
};

function getLineaAliquotaIva(invoice, iv) {
    if (iv.relationships.invoiceproduct_taxes.data == 0) {
        return null;
    }

    var iv_taxes = iv.relationships.invoiceproduct_taxes.data;
    var aliquota = null;

    for (var i = 0; i < iv_taxes.length; i++) {
        var iv_tax = getIncluded(invoice, 'invoiceproduct_tax', iv_taxes[i].id);
        if (iv_tax && iv_tax.attributes.is_iva && iv_tax.attributes.is_active) {
            aliquota = iv_tax.attributes.rate;
        }
    }

    return aliquota;
};

function generaDatiRiepilogo(invoice) {
    var riepilogo = [];
    var tot_iva = null;
    var aliquota = null;
    var iva_natura = null;

    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    for (var i = 0; i < invoice.data.relationships.invoicetaxes.data.length; i++) {
        var tax = getIncluded(invoice, 'invoicetax', invoice.data.relationships.invoicetaxes.data[i].id);

        if ((tax.attributes.is_iva || (tax.attributes.name.toLowerCase()).indexOf('iva') != -1) ||
            (tax.attributes.is_bollo && invoice.data.attributes.bollo_cliente && !tax.attributes.bollo_imponibile)) {

            var natura = tax.attributes.iva_natura ? tax.attributes.iva_natura.split(':') : null;

            var lineaFattura = {
                AliquotaIVA: tax.attributes.rate || parseInt(tax.attributes.rate) == 0 ? formatta_euro(tax.attributes.rate) : null,
                /*
                    Natura restriction
                    N1: Escluse ex. art. 15
                    N2: Non soggette
                    N3: Non Imponibili
                    N4: Esenti
                    N5: Regime del margine
                    N6: Inversione contabile (reverse charge)
                    N7: IVA assolta in altro stato UE (vendite a distanza ex art. 40 commi 3 e 4 e art. 41 comma 1 lett. b, DL 331/93; prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, DPR 633/72 e art. 74-sexies, DPR 633/72)
                */
                Natura: (natura && natura[0]) && parseInt(tax.attributes.rate) == 0 ? natura[0] : null,
                /* Natura facoltativo */
                SpeseAccessorie: null,
                /* SpeseAccessorie facoltativo */
                Arrotondamento: null,
                /* Arrotondamento facoltativo */
                ImponibileImporto: tax.attributes.imponibile ? formatta_euro(tax.attributes.imponibile) : null,
                Imposta: tax.attributes.total && !tax.attributes.is_bollo ? formatta_euro(tax.attributes.total) : (parseInt(tax.attributes.rate) == 0 ? formatta_euro(0) : null),
                /*
                    D: esigibilità differita
                    I: esigibilità immediata
                    S: scissione dei pagamenti
                */
                EsigibilitaIVA: invoice.data.attributes.esigibilita_iva ? invoice.data.attributes.esigibilita_iva.split(':')[0] : null,
                /* EsigibilitaIVA facoltativo */
                RiferimentoNormativo: (natura && natura[1] && natura[1].lastIndexOf('(') != -1) && parseInt(tax.attributes.rate) == 0  ? natura[1].substring(natura[1].lastIndexOf('(') + 1, natura[1].lastIndexOf(')')) : null
                /* RiferimentoNormativo facoltativo */
            };

            riepilogo.push(lineaFattura);
        }

    }

    return riepilogo;
};

function generaLeneeFattura(invoice) {
    var linee = [];
    var payment_rigo = '';

    if(invoice.data.attributes.pag_riga_fattura){
        for (var i = 0; i < invoice.data.relationships.payments.data.length; i++) {
            var paym = getIncluded(invoice, 'payments', invoice.data.relationships.payments.data[i].id);
            if(invoice.data.attributes.status == 'done'){
                payment_rigo += ' pagata in data ' + moment(new Date(paym.attributes.date)).format('YYYY-MM-DD') + ' in ' + paym.attributes.payment_method; 
            }
        }

    }

    if (invoice.data.relationships.invoicegroups.data && invoice.data.relationships.invoicegroups.data.length > 0) {
        for (var j = 0; j < invoice.data.relationships.invoicegroups.data.length; j++) {
            var ig = getIncluded(invoice, 'invoicegroup', invoice.data.relationships.invoicegroups.data[j].id);
            var cnt = j + 1;
            var aliquotaIva = null;
            var ritenuta = null;
            var natura_iva = null;
            var tot_price = 0.00;
            var tot_importo = 0.00;
            var inizio_periodo = null;
            var fine_periodo = null;
            var riferimento_amministrazione = null;

            for (var i = 0; i < ig.relationships.invoiceproducts.data.length; i++) {
                var iv = getIncluded(invoice, 'invoiceproduct', ig.relationships.invoiceproducts.data[i].id);
                console.log(j + ' ' + JSON.stringify(iv.attributes));
                if (iv.attributes.price && iv.attributes.price > 0) {
                    tot_price += (invoice.data.attributes.lordo && iv.attributes.iva_rate && iv.attributes.iva_rate != 0 ? parseFloat(iv.attributes.price / (1 + parseFloat(iv.attributes.iva_rate / 100))) : iv.attributes.price);
                }

                if (iv.attributes.importo && iv.attributes.importo > 0) {
                    tot_importo += (invoice.data.attributes.lordo && iv.attributes.iva_rate && iv.attributes.iva_rate != 0 ? parseFloat(iv.attributes.importo / (1 + parseFloat(iv.attributes.iva_rate / 100))) : iv.attributes.importo);
                }

                if (iv.attributes.iva_rate || iv.attributes.iva_rate == 0) {
                    aliquotaIva = iv.attributes.iva_rate;
                }

                if (iv.attributes.iva_rate || iv.attributes.iva_rate == 0) {
                    aliquotaIva = iv.attributes.iva_rate;
                }

                if (iv.attributes.ritenuta || iv.attributes.ritenuta == 0) {
                    ritenuta = iv.attributes.ritenuta;
                }

                if (iv.attributes.iva_natura && iv.attributes.iva_natura == 0) {
                    natura_iva = iv.attributes.iva_natura;
                }

                if(iv.attributes.sdi_field && iv.attributes.sdi_field != ''  && iv.attributes.sdi_field != 'null'){
                    var sdi_json = JSON.parse(iv.attributes.sdi_field);

                    inizio_periodo = sdi_json.DataInizioPeriodo ? moment(new Date(sdi_json.DataInizioPeriodo)).format('YYYY-MM-DD') : null;
                    fine_periodo = sdi_json.DataFinePeriodo ? moment(new Date(sdi_json.DataFinePeriodo)).format('YYYY-MM-DD') : null;
                    riferimento_amministrazione = sdi_json.RiferimentoAmministrazione ? sdi_json.RiferimentoAmministrazione : null;
                
                }else{
                    inizio_periodo = null;
                    fine_periodo = null;
                    riferimento_amministrazione = null;
                }

            }

            var lineaFattura = {
                NumeroLinea: cnt ? cnt : null,
                /*
                    TipoCessionePrestazione restriction 
                    SC: Sconto
                    PR: Premio
                    AB: Abbuono
                    AC: Spesa accessoria
                */
                TipoCessionePrestazione: null,
                /* TipoCessionePrestazione facoltativo */
                CodiceArticolo: null, //getCodiceArticolo(iv),
                Descrizione: ig.attributes.name ? ig.attributes.name.replace(/["'&<>]/g, ' ') + payment_rigo.replace(/["'&<>]/g, ' ') : null,
                Quantita: null,
                /* Quantita facoltativo */
                UnitaMisura: null,
                /* UnitaMisura facoltativo */
                DataInizioPeriodo: inizio_periodo,
                /* DataInizioPeriodo facoltativo */
                DataFinePeriodo: fine_periodo,
                /* DataFinePeriodo facoltativo */
                PrezzoUnitario: tot_price || tot_price == 0 ? formatta_euro(tot_price) : null,
                ScontoMaggiorazione: null, //getScontoMaggiorazione(iv),
                PrezzoTotale: tot_importo || tot_importo == 0 ? formatta_euro(tot_importo) : null,
                AliquotaIVA: aliquotaIva || parseInt(aliquotaIva) == 0 ? formatta_euro(aliquotaIva) : null,
                Ritenuta: ritenuta ? 'SI' : null,
                /* Ritenuta facoltativo */
                /*
                    Natura restriction
                    N1: Escluse ex. art. 15
                    N2: Non soggette
                    N3: Non Imponibili
                    N4: Esenti
                    N5: Regime del margine
                    N6: Inversione contabile (reverse charge)
                    N7: IVA assolta in altro stato UE (vendite a distanza ex art. 40 commi 3 e 4 e art. 41 comma 1 lett. b, DL 331/93; prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, DPR 633/72 e art. 74-sexies, DPR 633/72)
                */
                Natura: natura_iva ? natura_iva.split(':')[0] : null,
                /* Natura facoltativo */

                RiferimentoAmministrazione: riferimento_amministrazione,
                AltriDatiGestionali: getAltriDatiGestionali(sdi_json.AltriDatiGestionali)
            };
            linee.push(lineaFattura);
        }

    } else {
        for (var i = 0; i < invoice.data.relationships.invoiceproducts.data.length; i++) {
            var iv = getIncluded(invoice, 'invoiceproduct', invoice.data.relationships.invoiceproducts.data[i].id);
            var cnt = i + 1;
            var sdi_json = null;
           

            if(iv.attributes.sdi_field && iv.attributes.sdi_field != '' && iv.attributes.sdi_field != 'null'){
                
                sdi_json = JSON.parse(iv.attributes.sdi_field);

                inizio_periodo = sdi_json.DataInizioPeriodo ? moment(new Date(sdi_json.DataInizioPeriodo)).format('YYYY-MM-DD') : null;
                fine_periodo = sdi_json.DataFinePeriodo ? moment(new Date(sdi_json.DataFinePeriodo)).format('YYYY-MM-DD') : null;
                riferimento_amministrazione = sdi_json.RiferimentoAmministrazione ? sdi_json.RiferimentoAmministrazione : null;
            
            }else{
                inizio_periodo = null;
                fine_periodo = null;
                riferimento_amministrazione = null;
            }

            var lineaFattura = {
                NumeroLinea: cnt ? cnt : null,
                /*
                    TipoCessionePrestazione restriction 
                    SC: Sconto
                    PR: Premio
                    AB: Abbuono
                    AC: Spesa accessoria
                */
                TipoCessionePrestazione: null,
                /* TipoCessionePrestazione facoltativo */
                CodiceArticolo: null, //getCodiceArticolo(iv),
                Descrizione: iv.attributes.name ? iv.attributes.name.replace(/["'&<>]/g, ' ')  + payment_rigo.replace(/["'&<>]/g, ' ') : null,
                Quantita: iv.attributes.qta ? formatta_euro(iv.attributes.qta) : null,
                /* Quantita facoltativo */
                UnitaMisura: iv.attributes.unit ? togli_caratteri(iv.attributes.unit) : null,
                /* UnitaMisura facoltativo */
                DataInizioPeriodo: inizio_periodo,
                /* DataInizioPeriodo facoltativo */
                DataFinePeriodo: fine_periodo,
                /* DataFinePeriodo facoltativo */
                PrezzoUnitario: iv.attributes.price || parseFloat(iv.attributes.price) == 0 ? (invoice.data.attributes.lordo && iv.attributes.iva_rate && iv.attributes.iva_rate != 0 ? formatta_euro(parseFloat(iv.attributes.price / (1 + parseFloat(iv.attributes.iva_rate / 100)))) : formatta_euro(iv.attributes.price)) : null,
                ScontoMaggiorazione: null, //getScontoMaggiorazione(iv),
                PrezzoTotale: iv.attributes.importo || parseFloat(iv.attributes.importo) == 0 ? (invoice.data.attributes.lordo && iv.attributes.iva_rate && iv.attributes.iva_rate != 0 ? formatta_euro(parseFloat(iv.attributes.importo / (1 + parseFloat(iv.attributes.iva_rate / 100)))) : formatta_euro(iv.attributes.importo)) : null,
                AliquotaIVA: iv.attributes.iva_rate || parseInt(iv.attributes.iva_rate) == 0 ? formatta_euro(iv.attributes.iva_rate) : null,
                Ritenuta: iv.attributes.ritenuta ? 'SI' : null,
                /* Ritenuta facoltativo */
                /*
                    Natura restriction
                    N1: Escluse ex. art. 15
                    N2: Non soggette
                    N3: Non Imponibili
                    N4: Esentinu
                    N5: Regime del margine
                    N6: Inversione contabile (reverse charge)
                    N7: IVA assolta in altro stato UE (vendite a distanza ex art. 40 commi 3 e 4 e art. 41 comma 1 lett. b, DL 331/93; prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, DPR 633/72 e art. 74-sexies, DPR 633/72)
                */
                Natura: iv.attributes.iva_natura && parseInt(iv.attributes.iva_rate) == 0 ? iv.attributes.iva_natura.split(':')[0] : null,
                /* Natura facoltativo */
                RiferimentoAmministrazione: riferimento_amministrazione,
                /* RiferimentoAmministrazione facoltativo */
                AltriDatiGestionali: sdi_json && sdi_json.AltriDatiGestionali ? getAltriDatiGestionali(sdi_json.AltriDatiGestionali) : null
            };
            linee.push(lineaFattura);
        }
    }


    return linee;
};

function generaPagamentiFattura(invoice) {
    var pagamento = { /* DatiPagamento facoltativo */
        /* Blocco relativo ai dati di Pagamento della Fattura   Elettronica */

        /*
            CondizioniPagamento restriction
            TP01: pagamento a rate
            TP02: pagamento completo
            TP03: anticipo
        */
        CondizioniPagamento: 'TP02',
        DettaglioPagamento: []
    };

    for (var i = 0; i < invoice.data.relationships.expire_dates.data.length; i++) {
        var exp = getIncluded(invoice, 'expire_date', invoice.data.relationships.expire_dates.data[i].id);

        var dettagio = { /* DettaglioPagamento facoltativo */
            Beneficiario: null,
            /* Beneficiario facoltativo */
            /*
                ModalitaPagamento restriction
                MP01: contanti
                MP02: assegno
                MP03: assegno circolare
                MP04: contanti presso Tesoreria
                MP05: bonifico
                MP06: vaglia cambiario
                MP07: bollettino bancario
                MP08: carta di pagamento
                MP09: RID
                MP10: RID utenze
                MP11: RID veloce
                MP12: RIBA
                MP13: MAV
                MP14: quietanza erario
                MP15: giroconto su conti di contabilità speciale
                MP16: domiciliazione bancaria
                MP17: domiciliazione postale
                MP18: bollettino di c/c postale
                MP19: SEPA Direct Debit
                MP20: SEPA Direct Debit CORE
                MP21: SEPA Direct Debit B2B
                MP22: Trattenuta su somme già riscosse
                MP23: PagoPA
            */
            ModalitaPagamento: exp.attributes.payment_method ? exp.attributes.payment_method.split(':')[0] : null,
            //DataRiferimentoTerminiPagamento: null,
            /* DataRiferimentoTerminiPagamento facoltativo */
            //GiorniTerminiPagamento: moment(exp.attributes.expire_date).diff(moment(invoice.data.attributes.date), 'days'),
            /* GiorniTerminiPagamento facoltativo */
            DataScadenzaPagamento: moment(exp.attributes.expire_date).format('YYYY-MM-DD'),
            /* DataScadenzaPagamento facoltativo */
            ImportoPagamento: exp.attributes.total ? formatta_euro(exp.attributes.total) : null,
            //CodUfficioPostale: null,
            /* CodUfficioPostale facoltativo */
            //CognomeQuietanzante: null,
            /* CognomeQuietanzante facoltativo */
            //NomeQuietanzante: null,
            /* NomeQuietanzante facoltativo */
            //CFQuietanzante: null,
            /* CFQuietanzante facoltativo */
            //TitoloQuietanzante: null,
            /* TitoloQuietanzante facoltativo */
            IstitutoFinanziario: invoice.data.attributes.bank_name ? invoice.data.attributes.bank_name.replace(/["'&<>]/g, ' ') : null,
            /* IstitutoFinanziario facoltativo */
            IBAN: invoice.data.attributes.bank_iban ? invoice.data.attributes.bank_iban : null,
            /* IBAN facoltativo */
            //ABI: null,
            /* ABI facoltativo */
            //CAB: null,
            /* CAB facoltativo */
            BIC: invoice.data.attributes.bank_bic ? invoice.data.attributes.bank_bic : null,
            /* BIC facoltativo */
            //ScontoPagamentoAnticipato: 1,
            /* ScontoPagamentoAnticipato facoltativo */
            //DataLimitePagamentoAnticipato: null,
            /* DataLimitePagamentoAnticipato facoltativo */
            //PenalitaPagamentiRitardati: 1,
            /* PenalitaPagamentiRitardati facoltativo */
           // DataDecorrenzaPenale: null,
            /* DataDecorrenzaPenale facoltativo */
            //CodicePagamento: null /* CodicePagamento facoltativo */
        };

        pagamento.DettaglioPagamento.push(dettagio);
    }

    return [pagamento];
};

function getTotaleRitenuta(invoice){
    var tot_ritenuta = 0;
    var aliquota = 0;
    var cnt = 0;
    var causale = null;

    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    for (var i = 0; i < invoice.data.relationships.invoicetaxes.data.length; i++) {
        var tax = getIncluded(invoice, 'invoicetax', invoice.data.relationships.invoicetaxes.data[i].id);

        if (tax.attributes.is_ritenuta || (tax.attributes.name.toLowerCase()).indexOf('ritenuta') != -1) {

            tot_ritenuta += tax.attributes.total;
          
        }

        if (cnt++ >= invoice.data.relationships.invoicetaxes.data.length - 1) {
            if (tot_ritenuta > 0) {
                return formatta_euro(tot_ritenuta);
            } else
                return 0.00;

        }

    }
}

function getDatiRitenuta(invoice) {

    var tot_ritenuta = 0;
    var aliquota = 0;
    var cnt = 0;
    var causale = null;
    var tipo_ritenuta = null;

    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    for (var i = 0; i < invoice.data.relationships.invoicetaxes.data.length; i++) {
        var tax = getIncluded(invoice, 'invoicetax', invoice.data.relationships.invoicetaxes.data[i].id);

        if (tax.attributes.is_ritenuta || (tax.attributes.name.toLowerCase()).indexOf('ritenuta') != -1) {

            tot_ritenuta += tax.attributes.total;
            aliquota = tax.attributes.rate;
            causale = tax.attributes.name.split(':')[0];
            tipo_ritenuta = tax.attributes.type_ritenuta ? tax.attributes.type_ritenuta.split(':')[0] : (invoice.data.attributes.fisica_giuridica ? 'RT01' : 'RT02')
        }

        if (cnt++ >= invoice.data.relationships.invoicetaxes.data.length - 1) {
            if (tot_ritenuta > 0) {
                return {
                    /* DatiRitenuta facoltativo */

                    /*
                        TipoRitenuta restriction
                        RT01: Ritenuta di acconto persone fisiche
                        RT02: Ritenuta di acconto persone giuridiche
                    */
                    TipoRitenuta: tipo_ritenuta,
                    ImportoRitenuta: formatta_euro(tot_ritenuta),
                    AliquotaRitenuta: formatta_euro(aliquota),
                    /*
                        CausalePagamento restriction
                        https://www.fatturapertutti.it/supporto/soggetti-a-ritenuta-causali-di-pagamento-come-da-istruzioni-modello-770s-874
                    */
                    CausalePagamento: causale ? causale : null
                };
            } else
                return null;

        }

    }
};

function getDatiBollo(invoice) {
    var tot_ritenuta = 0;
    var cnt = 0;
    var bollo_virtuale = false;

    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    for (var i = 0; i < invoice.data.relationships.invoicetaxes.data.length; i++) {
        var tax = getIncluded(invoice, 'invoicetax', invoice.data.relationships.invoicetaxes.data[i].id);


        if (tax.attributes.is_bollo || (tax.attributes.name.toLowerCase()).indexOf('bollo') != -1) {

            tot_ritenuta += tax.attributes.total;
            bollo_virtuale = tax.attributes.bollo_virtuale;

        }

        if (cnt++ >= invoice.data.relationships.invoicetaxes.data.length - 1) {
            if (tot_ritenuta > 0) {
                return {
                    /* DatiBollo facoltativo */
                    BolloVirtuale: bollo_virtuale ? 'SI' : null,
                    ImportoBollo: formatta_euro(tot_ritenuta),
                };
            } else
                return null;

        }

    }
};

function getDatiCassaPrevidenziale(invoice) {
    var riepilogo = [];
    var tot_ritenuta = 0;
    var tot_imponibile = 0;
    var aliquota = 0;
    var cnt = 0;
    var tipo_cassa = '';
    var aliquotaIva = null;
    var natura_iva = null;
    var ritenuta = null;

    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    for (var i = 0; i < invoice.data.relationships.invoicetaxes.data.length; i++) {
        var tax = getIncluded(invoice, 'invoicetax', invoice.data.relationships.invoicetaxes.data[i].id);

        if (tax.attributes.is_rivalsa || (tax.attributes.name.toLowerCase()).indexOf('rivalsa') != -1 ||
            (tax.attributes.name.toLowerCase()).indexOf('cassa') != -1) {

            if(tax.attributes.imponibile && tax.attributes.imponibile != 0 &&
                tax.attributes.total && tax.attributes.total != 0)
            {
                tot_ritenuta += tax.attributes.total;
                tot_imponibile += tax.attributes.imponibile;
                aliquota = tax.attributes.rate;
                tipo_cassa = tax.attributes.name.split(':')[0];
                aliquotaIva = tax.attributes.rivalsa_iva_rate || parseInt(tax.attributes.rivalsa_iva_rate) == 0 ? formatta_euro(tax.attributes.rivalsa_iva_rate) : null;
                natura_iva = tax.attributes.rivalsa_iva_natura ? tax.attributes.rivalsa_iva_natura.split(':')[0] : null;
                ritenuta = getDatiRitenuta(invoice) ? 'SI' : null;

                var cassa_prev = { /* DatiCassaPrevidenziale facoltativo */
                    /*
                        TipoCassa restriction
                        TC01: Cassa nazionale previdenza e assistenza avvocati e procuratori legali
                        TC02: Cassa previdenza dottori commercialisti
                        TC03: Cassa previdenza e assistenza geometri
                        TC04: Cassa nazionale previdenza e assistenza ingegneri e architetti liberi professionisti
                        TC05: Cassa nazionale del notariato
                        TC06: Cassa nazionale previdenza e assistenza ragionieri e periti commerciali
                        TC07: Ente nazionale assistenza agenti e rappresentanti di commercio (ENASARCO)
                        TC08: Ente nazionale previdenza e assistenza consulenti del lavoro (ENPACL)
                        TC09: Ente nazionale previdenza e assistenza medici (ENPAM)
                        TC10: Ente nazionale previdenza e assistenza farmacisti (ENPAF)
                        TC11: Ente nazionale previdenza e assistenza veterinari (ENPAV)
                        TC12: Ente nazionale previdenza e assistenza impiegati dell'agricoltura (ENPAIA)
                        TC13: Fondo previdenza impiegati imprese di spedizione e agenzie marittime
                        TC14: Istituto nazionale previdenza giornalisti italiani (INPGI)
                        TC15: Opera nazionale assistenza orfani sanitari italiani (ONAOSI)
                        TC16: Cassa autonoma assistenza integrativa giornalisti italiani (CASAGIT)
                        TC17: Ente previdenza periti industriali e periti industriali laureati (EPPI)
                        TC18: Ente previdenza e assistenza pluricategoriale (EPAP)
                        TC19: Ente nazionale previdenza e assistenza biologi (ENPAB)
                        TC20: Ente nazionale previdenza e assistenza professione infermieristica (ENPAPI)
                        TC21: Ente nazionale previdenza e assistenza psicologi (ENPAP)
                        TC22: INPS
                    */
                    TipoCassa: tipo_cassa,
                    AlCassa: formatta_euro(aliquota),
                    ImportoContributoCassa: formatta_euro(tot_ritenuta),
                    ImponibileCassa: formatta_euro(tot_imponibile),
                    AliquotaIVA: aliquotaIva,
                    Ritenuta: ritenuta,
                    Natura: natura_iva

                };

                riepilogo.push(cassa_prev);
            }
        }

    }

    return riepilogo;
};

function getDatiScontoMaggiorazioneDocumento(invoice) {
    if (invoice.data.attributes.discount_type) {
        return { /* ScontoMaggiorazione facoltativo */
            /*
                Tipo restriction
                SC = Sconto
                MG = Maggiorazione
            */
            Tipo: (!invoice.data.attributes.discount_type ? null : (invoice.data.attributes.discount < 0) ? 'SC' : 'MG'),
            Percentuale: invoice.data.attributes.discount_rate ? formatta_euro(invoice.data.attributes.discount_rate) : null,
            /* Percentuale facoltativo */
            Importo: invoice.data.attributes.discount ? formatta_euro(invoice.data.attributes.discount) : null /* Importo facoltativo */
        }
    } else
        return null;
};

function getImpostaIva(invoice) {
    var imposta = 0;

    if (invoice.data.relationships.invoicetaxes.data.length == 0) {
        return null;
    }

    for (var i = 0; i < invoice.data.relationships.invoicetaxes.data.length; i++) {
        var tax = getIncluded(invoice, 'invoicetax', invoice.data.relationships.invoicetaxes.data[i].id);


        if (tax.attributes.is_iva || (tax.attributes.name.toLowerCase()).indexOf('iva') != -1) {
            imposta = tax.attributes.total;
        }
    }

    if (imposta > 0)
        return imposta;
    else
        return null;
};

exports.generaFatturaBase = function(invoice, tok, callback_) {
    var async = require('async');


    /*
    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.FatturaPrincipale= { /* FatturaPrincipale facoltativo */
    /*    NumeroFatturaPrincipale: null,
        DataFatturaPrincipale: null
    };*/

    async.parallel({
        fattura: function(callback) {
            var FatturaElettronica = {
                'v1:FatturaElettronica': {
                    '@xmlns:v1': 'http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2',
                    '@versione': getPrivatoPa(invoice, invoice.data.attributes.contact_id),

                    FatturaElettronicaHeader: {
                        DatiTrasmissione: { /* Blocco relativo ai dati di trasmissione della Fattura Elettronica */

                            IdTrasmittente: {
                                IdPaese: 'IT',
                                IdCodice: '04599340967'
                            },
                            ProgressivoInvio: '00000',
                            /*
                                FormatoTrasmissione restriction
                                FPA12: Fattura verso PA
                                FPR12: Fattura verso privati
                            */
                            FormatoTrasmissione: getPrivatoPa(invoice, invoice.data.attributes.contact_id),
                            CodiceDestinatario: calcolaCodiceDestinatario(invoice),
                            /*ContattiTrasmittente: { /* ContattiTrasmittente facoltativo */
                            /*    Telefono: null,
                                Email: null
                            },*/
                            PECDestinatario: invoice.data.attributes.codice_destinatario ? null : (invoice.data.attributes.pec_destinatario ? invoice.data.attributes.pec_destinatario : null) /* PECDestinatario facoltativo */
                        },

                        CedentePrestatore: { /* Blocco relativo ai dati del Cedente / Prestatore */
                            DatiAnagrafici: {
                                IdFiscaleIVA: invoice.data.attributes.society_piva ? getIdFiscaleIVA(invoice.data.attributes.society_piva, 'cedente', invoice) : null,
                                /* IdFiscaleIVA facoltativo */
                                CodiceFiscale: invoice.data.attributes.society_cf && invoice.data.attributes.society_cf != null ? invoice.data.attributes.society_cf.toUpperCase() : null,
                                /* CodiceFiscale facoltativo */
                                Anagrafica: {
                                    /* Il campo Denominazione è in alternativa ai campi Nome e Cognome */
                                    Denominazione: invoice.data.attributes.society ? invoice.data.attributes.society.replace(/["'&<>]/g, ' ') : null,
                                    Nome: invoice.data.attributes.society ? null : invoice.data.attributes.society_first_name.replace(/["'&<>]/g, ' '),
                                    Cognome: invoice.data.attributes.society ? null : invoice.data.attributes.society_last_name.replace(/["'&<>]/g, ' '),

                                    Titolo: null,
                                    /* Titolo facoltativo */
                                    CodEORI: null,
                                    /* CodEORI facoltativo */
                                },
                                AlboProfessionale: null,
                                /* AlboProfessionale facoltativo */
                                ProvinciaAlbo: null,
                                /* ProvinciaAlbo facoltativo */
                                NumeroIscrizioneAlbo: null,
                                /* NumeroIscrizioneAlbo facoltativo */
                                DataIscrizioneAlbo: null,
                                /* DataIscrizioneAlbo facoltativo */
                                /*
                                    RegimeFiscale restriction
                                    RF01: Regime ordinario
                                    RF02: Regime dei contribuenti minimi (art. 1,c.96-117, L. 244/2007)
                                    RF04: Agricoltura e attività connesse e pesca (artt. 34 e 34-bis, D.P.R. 633/1972)
                                    RF05: Vendita sali e tabacchi (art. 74, c.1, D.P.R. 633/1972)
                                    RF06: Commercio dei fiammiferi (art. 74, c.1, D.P.R. 633/1972)
                                    RF07: Editoria (art. 74, c.1, D.P.R. 633/1972)
                                    RF08: Gestione di servizi di telefonia pubblica (art. 74, c.1, D.P.R. 633/1972)
                                    RF09: Rivendita di documenti di trasporto pubblico e di sosta (art. 74, c.1, D.P.R. 633/1972)
                                    RF10: Intrattenimenti, giochi e altre attività  di cui alla tariffa allegata al D.P.R. 640/72 (art. 74, c.6, D.P.R. 633/1972)       
                                    RF11: Agenzie di viaggi e turismo (art. 74-ter, D.P.R. 633/1972)
                                    RF12: Agriturismo (art. 5, c.2, L. 413/1991)
                                    RF13: Vendite a domicilio (art. 25-bis, c.6, D.P.R. 600/1973)
                                    RF14: Rivendita di beni usati, di oggetti   d’arte, d’antiquariato o da collezione (art.    36, D.L. 41/1995)
                                    RF15: Agenzie di vendite all’asta di oggetti d’arte, antiquariato o da collezione (art. 40-bis, D.L. 41/1995)
                                    RF16: IVA per cassa P.A. (art. 6, c.5, D.P.R. 633/1972)
                                    RF17: IVA per cassa (art. 32-bis, D.L. 83/2012)
                                    RF19: Regime forfettario
                                    RF18: Altro
                                */
                                RegimeFiscale: invoice.data.attributes.regime_fiscale ? invoice.data.attributes.regime_fiscale.split(':')[0] : (invoice.data.attributes.document_type == 'autofattura' ? 'RF01' : null)
                            },
                            Sede: getAddress(invoice, invoice.data.attributes.society_address_id),
                            /*StabileOrganizzazione: { /* StabileOrganizzazione facoltativo */
                            /*   Indirizzo: null,
                               NumeroCivico: null,
                               /* NumeroCivico facoltativo */
                            /*    CAP: null,
                                Comune: null,
                                Provincia: null,
                                Nazione: null,
                            },*/
                            IscrizioneREA: getIscrizioneREA(invoice),
                            Contatti: getContatti(invoice),
                            //RiferimentoAmministrazione: null /* RiferimentoAmministrazione facoltativo */
                        },
                        /*
                        RappresentanteFiscale: { /* RappresentanteFiscale facoltativo */
                        /* Blocco relativo ai dati del Rappresentante Fiscale */
                        /*   DatiAnagrafici: {
                               IdFiscaleIVA: {
                                   IdPaese: 'IT',
                                   IdCodice: '000'
                               },
                               CodiceFiscale: null /* CodiceFiscale facoltativo */
                        /*    }
                        },*/

                        CessionarioCommittente: {
                            /* Blocco relativo ai dati del Cessionario / Committente */
                            DatiAnagrafici: {
                                IdFiscaleIVA: invoice.data.attributes.contact_piva ? getIdFiscaleIVA(invoice.data.attributes.contact_piva ,'cessionario', invoice) : null,
                                /* IdFiscaleIVA facoltativo */
                                CodiceFiscale: (invoice.data.attributes.contact_cf ? invoice.data.attributes.contact_cf.toUpperCase() : null),
                                /* CodiceFiscale facoltativo */
                                Anagrafica: {
                                    /* Il campo Denominazione è in alternativa ai campi Nome e Cognome */
                                    Denominazione: invoice.data.attributes.contact_name ? invoice.data.attributes.contact_name.replace(/["'&<>]/g, ' ') : null,
                                    Nome: invoice.data.attributes.contact_first_name ? invoice.data.attributes.contact_first_name.replace(/["'&<>]/g, ' ') : null,
                                    Cognome: invoice.data.attributes.contact_last_name ? invoice.data.attributes.contact_last_name.replace(/["'&<>]/g, ' ') : null,



                                    Titolo: null,
                                    /* Titolo facoltativo */
                                    CodEORI: null,
                                    /* CodEORI facoltativo */
                                }
                            },
                            Sede: getAddress(invoice, invoice.data.attributes.contact_address_id),
                            /*RappresentanteFiscale: { /* RappresentanteFiscale facoltativo */
                            /* Blocco relativo ai dati del Rappresentante Fiscale */
                            /*   DatiAnagrafici: {
                                   IdFiscaleIVA: {
                                       IdPaese: 'IT',
                                       IdCodice: '000'
                                   },
                                   CodiceFiscale: null /* CodiceFiscale facoltativo */
                            /*    }
                            },*/
                        },
                        TerzoIntermediarioOSoggettoEmittente: { /* TerzoIntermediarioOSoggettoEmittente facoltativo */
                            /* Blocco relativo ai dati del Terzo Intermediario che emette fattura elettronica per conto del Cedente/Prestatore */
                            DatiAnagrafici: {
                                IdFiscaleIVA: { /* IdFiscaleIVA facoltativo */
                                    IdPaese: 'IT',
                                    IdCodice: '04599340967'
                                },
                                CodiceFiscale: null,
                                /* CodiceFiscale facoltativo */
                                Anagrafica: {
                                    /* Il campo Denominazione è in alternativa ai campi Nome e Cognome */
                                    Denominazione: 'TI Trust Technologies s.r.l.'
                                }
                            }

                        },
                        SoggettoEmittente: 'TZ'

                    },

                    FatturaElettronicaBody: {
                        DatiGenerali: {
                            /* Blocco relativo ai Dati Generali della Fattura Elettronica */
                            DatiGeneraliDocumento: {
                                /*
                                    TipoDocumento restriction 
                                    TD01: Fattura
                                    TD02: Acconto / anticipo su fattura
                                    TD03: Acconto / anticipo su parcella
                                    TD04: Nota di credito
                                    TD05: Nota di debito
                                    TD06: Parcella
                                */
                                TipoDocumento: getTipologiaDocumento(invoice),
                                Divisa: invoice.data.attributes.valuta ? invoice.data.attributes.valuta : null,
                                Data: moment(invoice.data.attributes.date).format('YYYY-MM-DD'),
                                Numero: invoice.data.attributes.number ? invoice.data.attributes.number : null,
                                DatiRitenuta: getDatiRitenuta(invoice),
                                DatiBollo: getDatiBollo(invoice),
                                DatiCassaPrevidenziale: getDatiCassaPrevidenziale(invoice),
                                ScontoMaggiorazione: getDatiScontoMaggiorazioneDocumento(invoice),
                                ImportoTotaleDocumento: invoice.data.attributes.total ?  formatta_euro(parseFloat(formatta_euro(invoice.data.attributes.total)) + parseFloat(getTotaleRitenuta(invoice))) : null,
                                /* ImportoTotaleDocumento facoltativo */
                                Arrotondamento: null,
                                /* Arrotondamento facoltativo */
                                Causale: invoice.data.attributes.causale ? invoice.data.attributes.causale.replace(/["'&<>]/g, ' ') : null,
                                /* Causale facoltativo */
                                /*
                                    Art73 restriction
                                    SI = Documento emesso secondo modalità e termini stabiliti con DM ai sensi dell'art. 73 DPR 633/72
                                */
                                Art73: null /* Art73 facoltativo */
                            }

                        },
                        DatiBeniServizi: {
                            /* Blocco relativo ai dati di Beni Servizi della Fattura    Elettronica */
                            DettaglioLinee: generaLeneeFattura(invoice),
                            DatiRiepilogo: generaDatiRiepilogo(invoice),
                        },
                        //DatiVeicoli: { /* DatiVeicoli facoltativo */
                        /* 
                            Blocco relativo ai dati dei Veicoli della Fattura
                            Elettronica (da indicare nei casi di cessioni tra Paesi
                            membri di mezzi di trasporto nuovi, in base all'art. 38,
                            comma 4 del dl 331 del 1993)
                        */
                        /*Data: null,
                            TotalePercorso: null
                        },*/
                        //DatiPagamento: generaPagamentiFattura(invoice)
                        /* DatiPagamento facoltativo */

                    }
                }
            };

            if(invoice.data.relationships.expire_dates.data.length > 0){
                FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiPagamento = generaPagamentiFattura(invoice);
            }
        
            var sdi_field = invoice.data.attributes.sdi_field ? invoice.data.attributes.sdi_field : null;
            if(sdi_field && sdi_field != ''  && sdi_field != 'null'){
                /* DATI ORDINE DI ACQUISTO */
             
                var sdi_json = JSON.parse(sdi_field);
               

                if (sdi_json.DatiOrdineAcquisto && sdi_json.DatiOrdineAcquisto.length > 0) {
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiOrdineAcquisto =[];
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiOrdineAcquisto = getDocumentiCollegati(sdi_json.DatiOrdineAcquisto);
                }

                if (sdi_json.DatiContratto && sdi_json.DatiContratto.length > 0) {
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiContratto =[];
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiContratto = getDocumentiCollegati(sdi_json.DatiContratto);
                }

                if (sdi_json.DatiFattureCollegate && sdi_json.DatiFattureCollegate.length > 0) {
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiFattureCollegate =[];
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiFattureCollegate = getDocumentiCollegati(sdi_json.DatiFattureCollegate);
                }

                if (sdi_json.DatiConvenzione && sdi_json.DatiConvenzione.length > 0) {
                    
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiConvenzione = getDocumentiCollegati(sdi_json.DatiConvenzione);
                }
                
                if (sdi_json.DatiRicezione && sdi_json.DatiRicezione.length > 0) {
                    
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiRicezione = getDocumentiCollegati(sdi_json.DatiRicezione);
                }

                if(sdi_json.DatiSAL && sdi_json.DatiSAL.length > 0){
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiSAL = getDatiSAL(sdi_json.DatiSAL);
                }

                if(sdi_json.DatiTrasporto && sdi_json.DatiTrasporto.length > 0){
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiTrasporto = getDatiTrasporto(sdi_json.DatiTrasporto);
                }

                
                if(sdi_json.FatturaPrincipale && sdi_json.FatturaPrincipale != ''){
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.FatturaPrincipale = getFatturaPrincipale(sdi_json.FatturaPrincipale);
                }

                if(sdi_json.DatiVeicoli && sdi_json.DatiVeicoli != ''){
                    FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiVeicoli = getDatiVeicoli(sdi_json.DatiVeicoli);
                }

            }
            

            /*var invoice_2 = invoice.included.filter(function(i) {
                return i.type === 'invoices' && i.attributes.document_type == 'invoice';
            });

            if (invoice_2 && invoice_2.length > 0) {

                FatturaElettronica['v1:FatturaElettronica'].FatturaElettronicaBody.DatiGenerali.DatiFattureCollegate = { 
                    //RiferimentoNumeroLinea: null,
                    IdDocumento: invoice_2[0].attributes.number ? invoice_2[0].attributes.number : null,
                    Data: invoice_2[0].attributes.date ? moment(invoice_2[0].attributes.date).format('YYYY-MM-DD') : null,
                    NumItem: null,
                    CodiceCommessaConvenzione: null,
                    CodiceCUP: null,
                    CodiceCIG: null
                };
            }*/

            callback(null, FatturaElettronica);
        },
        allegati: function(callback) {
            var https = require('https');
            var allegati = [];
            var pdf_file_name = (invoice.data.attributes.pdf_file_name && invoice.data.attributes.pdf_file_name != null) ? invoice.data.attributes.pdf_file_name : 'pdf.php';
            var url = `https://asset.beebeeboard.com/pdf_create/api/${pdf_file_name}?organization=${invoice.data.attributes.organization}&invoice_id=${invoice.data.attributes.id}&token=${tok}&$_t=${moment().format('HHmmss')}`;

            var request = https.get(url, function(result) {

                var chunks = [];

                result.on('data', function(chunk) {
                    chunks.push(chunk);
                });

                result.on('end', function() {

                    var pdfData = new Buffer.concat(chunks);

                    allegati.push({ /* Allegati facoltativo */
                        /* Blocco relativo ai dati di eventuali allegati */

                        NomeAttachment: 'Fattura.pdf',
                        AlgoritmoCompressione: null,
                        /* AlgoritmoCompressione facoltativo */
                        FormatoAttachment: null,
                        /* FormatoAttachment facoltativo */
                        DescrizioneAttachment: 'Fattura in pdf cartaceo',
                        /* DescrizioneAttachment facoltativo */
                        Attachment: pdfData.toString('base64')
                    });

                    callback(null, allegati);


                });
            }).on('error', function(err) {
                console.log(err);
                //callback(err,null);
            });

            //request.end(); 
        }
    }, function(err, risultato) {

        if (err)
            console.log(err);
        else {
            //risultato.fattura['v1:FatturaElettronica'].FatturaElettronicaBody.Allegati = risultato.allegati;

        }
        callback_(null, risultato.fattura);
    });
};

exports.generaXmlFattura = function(FatturaElettronica) {
    var xmlbuilder = require('xmlbuilder');
    var FatturaElettronicaXml = xmlbuilder.create(FatturaElettronica, { encoding: 'UTF-8' }, { skipNullNodes: true, skipNullAttributes: true });

    return FatturaElettronicaXml.end({ pretty: true, allowEmpty: true });
};

exports.veriricaFatturaXml = function(data, callback_) {
    var async = require('async');

    async.parallel({
        header_: function(callback) {
            var errors = [];
            /* HEADER */
            if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']) {
                errors.push({ 'posizione': 'FatturaElettronicaHeader', 'descrizione': 'tag mancante', 'soluzione': 'Formato xml Non valido manca il tag FatturaElettronicaHeader' });
            } else {
                /* Dati trasmissione */
                if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']) {
                    errors.push({ 'posizione': 'Header->DatiTrasmissione', 'descrizione': 'tag mancante', 'soluzione': 'Formato xml Non valido manca il tag Header->DatiTrasmissione'});
                } else {

                    /* Trasmittente */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']) {
                        errors.push({ 'posizione': 'Header->DatiTrasmissione->IdTrasmittente', 'descrizione': 'tag mancante', 'soluzione': 'Formato xml Non valido manca il tag Header->DatiTrasmissione->IdTrasmittente' });
                    } else {
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdPaese']) {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->IdTrasmittente->IdPaese', 'descrizione': 'tag mancante', 'soluzione': 'Formato xml Non valido manca il tag Header->DatiTrasmissione->IdTrasmittente->IdPaese'  });
                        } else {
                            if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdPaese'].length) != 2 ||
                                !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdPaese'][0]) ||
                                !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdPaese'][1])) {
                                errors.push({ 'posizione': 'Header->DatiTrasmissione->IdTrasmittente->IdPaese', 'descrizione': 'Dimensione o formato stringa errata', 'soluzione': 'La sigla della nazione deve essere di 2 caratteri invece di "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdPaese']+'"' });
                            }
                        }

                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdCodice']) {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->IdTrasmittente->IdCodice', 'descrizione': 'tag mancante', 'soluzione': 'Formato xml Non valido manca il codice fiscale / partita Iva' });
                        } else {
                            if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdCodice'].length) < 1 ||
                                parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdCodice'].length) > 28) {
                                errors.push({ 'posizione': 'Header->DatiTrasmissione->IdTrasmittente->IdCodice(00300)', 'descrizione': 'Dimensione o formato stringa errata','soluzione': 'Il dato "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente']['IdCodice']+'" non è valido (da 1 a 28 caratteri)'  });
                            }
                        }
                    }

                    /* Progressivo Invio */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['ProgressivoInvio']) {
                        errors.push({ 'posizione': 'Header->DatiTrasmissione->ProgressivoInvio', 'descrizione': 'tag mancante' });
                    } else {
                        if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['ProgressivoInvio'].length) < 1 ||
                            parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['ProgressivoInvio'].length) > 10) {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->ProgressivoInvio', 'descrizione': 'Dimensione o formato stringa errata', 'soluzione': 'Numero progressivo di invio errato (da 1 a 10 caratteri)' });
                        }
                    }

                    /* Formato Trasmissione */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['FormatoTrasmissione']) {
                        errors.push({ 'posizione': 'Header->DatiTrasmissione->FormatoTrasmissione', 'descrizione': 'tag mancante' });
                    } else {
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['FormatoTrasmissione'] != 'FPA12' &&
                            data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['FormatoTrasmissione'] != 'FPR12') {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->FormatoTrasmissione(00428)', 'descrizione': 'Dimensione o formato stringa errata o non valido' });
                        }
                    }

                    /* Codice Destinatario */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario']) {
                        errors.push({ 'posizione': 'Header->DatiTrasmissione->CodiceDestinatario', 'descrizione': 'tag mancante' });
                    } else {
                        if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'].length) != 6 &&
                            parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'].length) != 7) {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->CodiceDestinatario(00311)', 'descrizione': 'Dimensione stringa non corretta (6 o 7 caratteri)', 'soluzione': 'Codice destinatario univoco  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario']+'" non corretto (6 o 7 caratteri)'  });
                        }
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'].length != 6 &&
                            data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'] == 'FPR12') {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->CodiceDestinatario(00427)', 'descrizione': 'Per le fatture private deve essere di 7 caratteri', 'soluzione': 'Codice destinatario univoco  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario']+'" non corretto (deve essere di 6 caratteri)'  });
                        }
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'].length != 7 &&
                            data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'] == 'FPA12') {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->CodiceDestinatario(00427)', 'descrizione': 'Per le fatture PA deve essere di 6 caratteri', 'soluzione': 'Codice destinatario univoco  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario']+'" non corretto (deve essere di 7 caratteri per fatture a Enti Pubblici)'  });
                        }
                    }

                    /* PEC Destinatario */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'] == '0000000' &&
                        data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['FormatoTrasmissione'] == 'FPR12' &&
                        data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']
                    ) {

                        //if(!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['PECDestinatario']){
                        //    errors.push({'posizione':'Header->DatiTrasmissione->PECDestinatario(00426)','descrizione': 'tag mancante a fronte di CodiceDestinatario = 0000000'});
                        //}
                        //else 
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['PECDestinatario']) {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['PECDestinatario'].length < 7 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['PECDestinatario'].length > 256) {
                                errors.push({ 'posizione': 'Header->DatiTrasmissione->PECDestinatario(00426)', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il valore di Pec  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['PECDestinatario']+'" non è valido (da 7 a 256 caratteri)'  });
                            }
                        }

                    } else if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'] != '0000000') {
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['PECDestinatario']) {
                            errors.push({ 'posizione': 'Header->DatiTrasmissione->PECDestinatario(00426)', 'descrizione': 'tag valorizzato a fronte di CodiceDestinatario diverso da 000000' });
                        }
                    }
                }

                /* Cedente Prestatore */
                if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']) {
                    errors.push({ 'posizione': 'Header->CedentePrestatore', 'descrizione': 'tag mancante' });
                } else {
                    /* Dati Anagrafici */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']) {
                        errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici', 'descrizione': 'tag mancante' });
                    } else {
                        /* Id fiscale IVA */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->IdFiscaleIVA', 'descrizione': 'tag mancante' });
                        } else {
                            /* Id Paese */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese']) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->IdFiscaleIVA->IdPaese', 'descrizione': 'tag mancante' });
                            } else {
                                if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'].length) != 2 ||
                                    !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'][0]) ||
                                    !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'][1])) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->IdFiscaleIVA->IdPaese', 'descrizione': 'Dimensione o formato stringa errata', 'soluzione': 'Il valore di nazione  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese']+'" non è valido (deve essere di 2 caratteri)'  });
                                }
                            }

                            /* Id Codice*/
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice']) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->IdFiscaleIVA->IdCodice', 'descrizione': 'tag mancante' });
                            } else {
                                if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice'].length) < 1 ||
                                    parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice'].length) > 28) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->IdFiscaleIVA->IdCodice(00301)', 'descrizione': 'Dimensione o formato stringa errata', 'soluzione': 'Il valore di Partita Iva  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice']+'" non è valido (deve essere da 1 a 28 caratteri)' });
                                }
                            }
                        }

                        /* Codice Fiscale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['CodiceFiscale'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['CodiceFiscale'].length < 11 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['CodiceFiscale'].length > 16)) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->CodiceFiscale(00302)', 'descrizione': 'Formato stringa NON corretto', 'soluzione': 'Il valore di Codice Fiscale  "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['IdFiscaleIVA']['CodiceFiscale']+'" non è valido (deve essere da 11 a 16 caratteri)' });
                        }

                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica', 'descrizione': 'tag mancante' });
                        } else {
                            /* Denominazione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Denominazione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Denominazione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Denominazione'].length > 80) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Denominazione', 'descrizione': 'Formato stringa non valido', 'soluzione': 'La ragione sociale "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Denominazione']+'" non è valida (deve essere da 1 a 80 caratteri)'  });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome'] ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Denominazione(00200)', 'descrizione': 'Campo Denominazione valorizzato a fronte della valorizzazione dei campi di Nome e/o Cognome', 'soluzione': 'Risultano compilati sia i campi di Nome e Cognome, sia la ragione sociale. Determinare se Persona Fisica lasciare solo nome e Cognome, se invece persona giuridica lasciare solo la ragione sociale'  });
                                }
                            }
                            /* Nome */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome'].length > 60) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Nome', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il Nome inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome']+'" non è valido (deve essere da 1 a 60 caratteri)'  });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Denominazione'] ||
                                    !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Nome(00200)', 'descrizione': 'Campo Nome valorizzato a fronte della valorizzazione del campo Denominazione oppure mancanza del campo Cognome' });
                                }
                            }

                            /* Cognome */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Cognome'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Cognome'].length > 60) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Cognome', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il Cognome inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome']+'" non è valido (deve essere da 1 a 60 caratteri)'  });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Denominazione'] ||
                                    !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Nome']) {
                                    errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Cognome(00200)', 'descrizione': 'Campo Cognome valorizzato a fronte della valorizzazione del campo Denominazione oppure mancanza del campo Nome' });
                                }
                            }

                            /* Titolo */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Titolo'] && (
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Titolo'].length < 2 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['Titolo'].length > 10
                                )) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->Titolo', 'descrizione': 'Formato stringa non valido' });
                            }

                            /* CodEori */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['CodEORI'] && (
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['CodEORI'].length < 13 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['Anagrafica']['CodEORI'].length > 17
                                )) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->Anagrafica->CodEORI', 'descrizione': 'Formato stringa non valido' });
                            }
                        }

                        /* Albo Professionale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['AlboProfessionale'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['AlboProfessionale'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['AlboProfessionale'].length > 60)) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->AlboProfessionale', 'descrizione': 'Formato stringa NON corretto' });
                        }

                        /* Provincia Albo */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['ProvinciaAlbo'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['ProvinciaAlbo'].length != 2)) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->ProvinciaAlbo', 'descrizione': 'Formato stringa NON corretto' });
                        }

                        /* Numero Iscrizione Albo */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['NumeroIscrizioneAlbo'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['NumeroIscrizioneAlbo'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['NumeroIscrizioneAlbo'].length > 60)) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->NumeroIscrizioneAlbo', 'descrizione': 'Formato stringa NON corretto' });
                        }

                        /* Data Iscrizione Albo */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['DataIscrizioneAlbo'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['DataIscrizioneAlbo'].length != 10 ||
                                !moment(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['DataIscrizioneAlbo'], 'YYYY-MM-DD').isValid())) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->DataIscrizioneAlbo', 'descrizione': 'Formato stringa NON corretto' });
                        }

                        /* RegimeFiscale */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['RegimeFiscale']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->RegimeFiscale', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['RegimeFiscale'].length != 4) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->RegimeFiscale', 'descrizione': 'Formato stringa NON corretto' });
                            }
                            var stringArray = ["RF01", "RF02", "RF04", "RF05", "RF06", "RF07", "RF08", "RF09", "RF10", "RF11", "RF12", "RF13", "RF14", "RF15", "RF16", "RF17", "RF18", "RF19"];
                            if (stringArray.indexOf(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['RegimeFiscale']) == 'RF03') {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->RegimeFiscale(00459)', 'descrizione': 'Valore NON ammesso' });
                            } else if (stringArray.indexOf(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['DatiAnagrafici']['RegimeFiscale']) == -1) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->DatiAnagrafici->RegimeFiscale', 'descrizione': 'Valore NON ammesso' });
                            }
                        }
                    }

                    /* SEDE */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']) {
                        errors.push({ 'posizione': 'Header->CedentePrestatore->Sede', 'descrizione': 'tag mancante' });
                    } else {
                        /* Indirizzo */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Indirizzo']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Indirizzo', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Indirizzo'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Indirizzo'].length > 60) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Indirizzo', 'descrizione': 'Formato stringa non valido', 'soluzione': 'L\'Indirizzo inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Indirizzo']+'" non è valido (deve essere da 1 a 60 caratteri)'  });
                            }
                        }

                        /* Numero Civico */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['NumeroCivico'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['NumeroCivico'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['NumeroCivico'].length > 8)) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->NumeroCivico', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il numero civico inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['NumeroCivico']+'" non è valido (deve essere da 1 a 8 caratteri)' });
                        }

                        /* CAP */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['CAP']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->CAP', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['CAP'].length != 5) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->CAP', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il CAP inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['CAP']+'" non è valido (deve essere di 5 caratteri, nel caso sia più lungo togliere dei caratteri, se invece più corto aggiungere degli 0 davanti per arrivare a 5)' });
                            }
                        }

                        /* Comune */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Comune']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Comune', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Comune'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Comune'].length > 60) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Comune', 'descrizione': 'Formato stringa non valido' , 'soluzione': 'Il Comune inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Comune']+'" non è valido (deve essere da 1 a 60 caratteri)' });
                            }
                        }

                        /* PROVINCIA */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Provincia'] &&
                            data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Provincia'].length != 2) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Provincia', 'descrizione': 'Formato stringa non valido' });
                        }

                        /* Nazione */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Nazione']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Nazione', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Sede']['Nazione'].length != 2) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->Sede->Nazione', 'descrizione': 'Formato stringa non valido' });
                            }
                        }

                    }

                    /* Iscrizione REA */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']) {
                        /* UFFICIO */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['Ufficio']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->Ufficio', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['Ufficio'].length != 2) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->Ufficio', 'descrizione': 'Formato stringa non valido' });
                            }
                        }

                        /* Numero Rea */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['NumeroREA']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->NumeroREA', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['NumeroREA'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['NumeroREA'].length > 20) {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->NumeroREA', 'descrizione': 'Formato stringa non valido' });
                            }
                        }

                        /* CapitaleSociale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['CapitaleSociale'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['CapitaleSociale'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['CapitaleSociale'].length > 15 ||
                                !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['CapitaleSociale'])
                            )) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->CapitaleSociale', 'descrizione': 'Formato stringa non valido' });
                        }

                        /* SocioUnico */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['SocioUnico'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['SocioUnico'] != 'SU' &&
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['SocioUnico'] != 'SM'
                            )) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->SocioUnico', 'descrizione': 'Formato stringa non valido' });
                        }

                        /* In liquidazione */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['StatoLiquidazione']) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->StatoLiquidazione', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['StatoLiquidazione'] != 'LS' ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['IscrizioneREA']['StatoLiquidazione'] != 'LS') {
                                errors.push({ 'posizione': 'Header->CedentePrestatore->IscrizioneREA->StatoLiquidazione', 'descrizione': 'Formato stringa non valido' });
                            }
                        }
                    }

                    /* Contatti */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']) {

                        /* Telefono */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Telefono'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Telefono'].length < 5 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Telefono'].length > 12
                            )) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Contatti->Telefono', 'descrizione': 'Formato stringa non valido' });
                        }

                        /* Fax */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Fax'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Fax'].length < 5 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Fax'].length > 12
                            )) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Contatti->Fax', 'descrizione': 'Formato stringa non valido' });
                        }

                        /* Email */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Email'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Email'].length < 7 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CedentePrestatore']['Contatti']['Email'].length > 256
                            )) {
                            errors.push({ 'posizione': 'Header->CedentePrestatore->Contatti->Email', 'descrizione': 'Formato stringa non valido' });
                        }
                    }
                }

                /* Cessionario Committente */
                if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']) {
                    errors.push({ 'posizione': 'Header->CessionarioCommittente', 'descrizione': 'tag mancante' });
                } else {
                    /* Dati Anagrafici */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']) {
                        errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici', 'descrizione': 'tag mancante' });
                    } else {
                        /* Id fiscale IVA */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA'] &&
                            !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['CodiceFiscale'] &&
                            data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['CodiceDestinatario'] != '0000000') {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->IdFiscaleIVA(00417)', 'descrizione': 'IdFiscaleIva e CodiceFiscale mancanti', 'soluzione':'Mancano i campi di Partita Iva e/o codice fiscale' });
                        } else {
                            /* Id Paese */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']) {
                                if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese']) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->IdFiscaleIVA->IdPaese', 'descrizione': 'tag mancante' });
                                } else {
                                    if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'].length) != 2 ||
                                        !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'][0]) ||
                                        !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'][1])) {
                                        errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->IdFiscaleIVA->IdPaese', 'descrizione': 'Dimensione o formato stringa errata' });
                                    }
                                }
                                /* Id Codice*/
                                if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice']) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->IdFiscaleIVA->IdCodice', 'descrizione': 'tag mancante' });
                                } else {
                                    if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice'].length) < 1 ||
                                        parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice'].length) > 28) {
                                        errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->IdFiscaleIVA->IdCodice(00301)', 'descrizione': 'Dimensione o formato stringa errata' , 'soluzione':'La partita iva "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice']+'" inserita non è valida (da 1 a 28 caratteri)'});
                                    }
                                }
                            }

                        }

                        /* Codice Fiscale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['CodiceFiscale'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['CodiceFiscale'].length < 11 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['CodiceFiscale'].length > 16)) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->CodiceFiscale(00302)', 'descrizione': 'Formato stringa NON corretto', 'soluzione':'Il codice fiscale "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['CodiceFiscale']+'" inserito non è valida (da 11 a 16 caratteri)' });
                        }

                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica', 'descrizione': 'tag mancante' });
                        } else {
                            /* Denominazione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Denominazione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Denominazione'].length < 1 ||
                                    (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Denominazione'].length > 80 &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['FormatoTrasmissione'] == 'FPR12')) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Denominazione', 'descrizione': 'Formato stringa non valido', 'soluzione':'La ragione sociale "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Denominazione']+'" inserita non è valida (da 1 a 80 caratteri)' });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Nome'] ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Denominazione(00200)', 'descrizione': 'Campo Denominazione valorizzato a fronte della valorizzazione dei campi di Nome e/o Cognome', 'soluzione': 'Risultano compilati sia i campi di Nome e Cognome, sia la ragione sociale. Determinare se Persona Fisica lasciare solo nome e Cognome, se invece persona giuridica lasciare solo la ragione sociale'  });
                                }
                            }
                            /* Nome */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Nome']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Nome'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Nome'].length > 60) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Nome', 'descrizione': 'Formato stringa non valido' });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Denominazione'] ||
                                    !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Nome(00200)', 'descrizione': 'Campo Nome valorizzato a fronte della valorizzazione del campo Denominazione oppure mancanza del campo Cognome', 'soluzione': 'Risultano compilati sia i campi di Nome e Cognome, sia la ragione sociale. Determinare se Persona Fisica lasciare solo nome e Cognome, se invece persona giuridica lasciare solo la ragione sociale'  });
                                }
                            }

                            /* Cognome */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Cognome'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Cognome'].length > 60) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Cognome', 'descrizione': 'Formato stringa non valido' });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Denominazione'] ||
                                    !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Nome']) {
                                    errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Cognome(00200)', 'descrizione': 'Campo Cognome valorizzato a fronte della valorizzazione del campo Denominazione oppure mancanza del campo Nome', 'soluzione': 'Risultano compilati sia i campi di Nome e Cognome, sia la ragione sociale. Determinare se Persona Fisica lasciare solo nome e Cognome, se invece persona giuridica lasciare solo la ragione sociale'  });
                                }
                            }

                            /* Titolo */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Titolo'] && (
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Titolo'].length < 2 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['Titolo'].length > 10
                                )) {
                                errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->Titolo', 'descrizione': 'Formato stringa non valido' });
                            }

                            /* CodEori */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['CodEORI'] && (
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['CodEORI'].length < 13 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['DatiAnagrafici']['Anagrafica']['CodEORI'].length > 17
                                )) {
                                errors.push({ 'posizione': 'Header->CessionarioCommittente->DatiAnagrafici->Anagrafica->CodEORI', 'descrizione': 'Formato stringa non valido' });
                            }
                        }
                    }

                    /* SEDE */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']) {
                        errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede', 'descrizione': 'tag mancante' });
                    } else {
                        /* Indirizzo */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Indirizzo']) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Indirizzo', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Indirizzo'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Indirizzo'].length > 60) {
                                errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Indirizzo', 'descrizione': 'Formato stringa non valido', 'soluzione': 'L\'Indirizzo inserito "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Indirizzo']+'" non è valido (deve essere da 1 a 60 caratteri)'  });
                            }
                        }

                        /* Numero Civico */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['NumeroCivico'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['NumeroCivico'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['NumeroCivico'].length > 8)) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->NumeroCivico', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il numero civico "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['NumeroCivico']+'" non è valido (deve essere da 1 a 8 caratteri)' });
                        }

                        /* CAP */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['CAP']) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->CAP', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['CAP'].length != 5) {
                                errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->CAP', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il CAP "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['CAP']+'" non è valido (deve essere di 5 caratteri, nel caso sia più lungo togliere dei caratteri, se invece più corto aggiungere degli 0 davanti per arrivare a 5)' });
                            }
                        }

                        /* Comune */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Comune']) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Comune', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Comune'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Comune'].length > 60) {
                                errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Comune', 'descrizione': 'Formato stringa non valido', 'soluzione': 'Il comune "'+data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Comune']+'" non è valido (deve essere da 1 a 60 caratteri)' });
                            }
                        }

                        /* PROVINCIA */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Provincia'] &&
                            data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Provincia'].length != 2) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Provincia', 'descrizione': 'Formato stringa non valido' });
                        }

                        /* Nazione */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Nazione']) {
                            errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Nazione', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['CessionarioCommittente']['Sede']['Nazione'].length != 2) {
                                errors.push({ 'posizione': 'Header->CessionarioCommittente->Sede->Nazione', 'descrizione': 'Formato stringa non valido' });
                            }
                        }
                    }
                }

                /* Terzo Intermediario o Soggetto Emittente */
                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']) {
                    /* Dati Anagrafici */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']) {
                        errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici', 'descrizione': 'tag mancante' });
                    } else {
                        /* Id fiscale IVA */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']) {
                            /* Id Paese */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese']) {
                                errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->IdFiscaleIVA->IdPaese', 'descrizione': 'tag mancante' });
                            } else {
                                if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'].length) != 2 ||
                                    !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'][0]) ||
                                    !isLetter(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdPaese'][1])) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->IdFiscaleIVA->IdPaese', 'descrizione': 'Dimensione o formato stringa errata' });
                                }
                            }

                            /* Id Codice*/
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice']) {
                                errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->IdFiscaleIVA->IdCodice', 'descrizione': 'tag mancante' });
                            } else {
                                if (parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice'].length) < 1 ||
                                    parseInt(data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['IdFiscaleIVA']['IdCodice'].length) > 28) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->IdFiscaleIVA->IdCodice', 'descrizione': 'Dimensione o formato stringa errata' });
                                }
                            }
                        }

                        /* Codice Fiscale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['CodiceFiscale'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['CodiceFiscale'].length < 11 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['CodiceFiscale'].length > 16)) {
                            errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->CodiceFiscale(00302)', 'descrizione': 'Formato stringa NON corretto' });
                        }

                        if (!data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']) {
                            errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica', 'descrizione': 'tag mancante' });
                        } else {
                            /* Denominazione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Denominazione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Denominazione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Denominazione'].length > 80) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Denominazione', 'descrizione': 'Formato stringa non valido' });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Nome'] ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Denominazione(00200)', 'descrizione': 'Campo Denominazione valorizzato a fronte della valorizzazione dei campi di Nome e/o Cognome' });
                                }
                            }
                            /* Nome */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Nome']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Nome'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Nome'].length > 60) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Nome', 'descrizione': 'Formato stringa non valido' });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Denominazione'] ||
                                    !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Nome(00200)', 'descrizione': 'Campo Nome valorizzato a fronte della valorizzazione del campo Denominazione oppure mancanza del campo Cognome' });
                                }
                            }

                            /* Cognome */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Cognome']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Cognome'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Cognome'].length > 60) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Cognome', 'descrizione': 'Formato stringa non valido' });
                                }
                                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Denominazione'] ||
                                    !data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Nome']) {
                                    errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Cognome(00200)', 'descrizione': 'Campo Cognome valorizzato a fronte della valorizzazione del campo Denominazione oppure mancanza del campo Nome' });
                                }
                            }

                            /* Titolo */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Titolo'] && (
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Titolo'].length < 2 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['Titolo'].length > 10
                                )) {
                                errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->Titolo', 'descrizione': 'Formato stringa non valido' });
                            }

                            /* CodEori */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['CodEORI'] && (
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['CodEORI'].length < 13 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente']['DatiAnagrafici']['Anagrafica']['CodEORI'].length > 17
                                )) {
                                errors.push({ 'posizione': 'Header->TerzoIntermediarioOSoggettoEmittente->DatiAnagrafici->Anagrafica->CodEORI', 'descrizione': 'Formato stringa non valido' });
                            }
                        }
                    }
                }

                /* Soggetto Emittente */
                if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['SoggettoEmittente']) {
                    /* Dati Anagrafici */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaHeader']['SoggettoEmittente'] != 'CC' &&
                        data['v1:FatturaElettronica']['FatturaElettronicaHeader']['SoggettoEmittente'] != 'TZ') {
                        errors.push({ 'posizione': 'Header->SoggettoEmittente', 'descrizione': 'Formato non valido' });
                    }
                }
            }

            callback(null, errors);
        },
        body_: function(callback) {
            var errors = [];
            /* BODY */
            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']) {
                errors.push({ 'posizione': 'FatturaElettronicaBody', 'descrizione': 'tag mancante' });
            } else {
                /* Dati generali */
                if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']) {
                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali', 'descrizione': 'tag mancante' });
                } else {
                    /* DatiGeneraliDocumento */
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']) {
                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento', 'descrizione': 'tag mancante' });
                    } else {
                        /* TipoDocumento */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['TipoDocumento']) {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->TipoDocumento', 'descrizione': 'tag mancante' });
                        } else {
                            var stringArray = ['TD01', 'TD02', 'TD03', 'TD04', 'TD05', 'TD06','TD16','TD17','TD18','TD19','TD20','TD21','TD22','TD23','TD24','TD25','TD26','TD27'];
                            if (stringArray.indexOf(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['TipoDocumento']) == -1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['TipoDocumento'].length != 4) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->TipoDocumento', 'descrizione': 'Formato NON valido' });
                            }
                        }

                        /* DIVISA */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Divisa']) {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Divisa', 'descrizione': 'tag mancante' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Divisa'].length != 3) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Divisa', 'descrizione': 'Formato NON valido' });
                            }
                        }

                        /* Data  */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Data']) {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Data', 'descrizione': 'tag mancante','soluzione':'Inserire una data per la fattura' });
                        } else {
                            if ((data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Data'].length != 10 ||
                                    !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Data'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Data', 'descrizione': 'Formato stringa NON corretto','soluzione':'Formato della data "'+data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Data']+'" non corretta (deve essere di 10 caratteri)' });
                            }

                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiFattureCollegate']) {
                                if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiFattureCollegate']) &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiFattureCollegate'].length > 0) {
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiFattureCollegate'].forEach(function(collegata) {
                                        if (moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Data'], 'YYYY-MM-DD') < moment(collegata['Data'], 'YYYY-MM-DD')) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Data(00418)', 'descrizione': 'Data antecedente a una fattura collegata' });
                                        }
                                    });
                                } else {
                                    if (moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Data'], 'YYYY-MM-DD') < moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiFattureCollegate']['Data'], 'YYYY-MM-DD')) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Data(00418)', 'descrizione': 'Data antecedente a una fattura collegata' });
                                    }
                                }
                            }
                        }

                        /* Numero */
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Numero']) {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Numero', 'descrizione': 'tag mancante','soluzione':'Inserire un numero per la fattura' });
                        } else {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Numero'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Numero'].length > 20 ||
                                !checkContainNumber(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Numero'].toString())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Numero(00425)', 'descrizione': 'Formato NON valido','soluzione':'Il numero "'+data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Numero']+'" inserito non è valido (da 1 a 20 caratteri)'  });
                            }
                        }

                        /* Dati Ritenuta */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']) {
                            /* TipoRitenuta */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->TipoRitenuta', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta'] != 'RT01' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta'] != 'RT02' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta'] != 'RT03' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta'] != 'RT04' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta'] != 'RT05' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['TipoRitenuta'] != 'RT06') {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->TipoRitenuta', 'descrizione': 'Formato non valido' });
                                }
                            }

                            /* ImportoRitenuta */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['ImportoRitenuta']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->ImportoRitenuta', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['ImportoRitenuta'] &&
                                    (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['ImportoRitenuta'].length < 4 ||
                                        data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['ImportoRitenuta'].length > 15 ||
                                        !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['ImportoRitenuta'])
                                    )) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->ImportoRitenuta', 'descrizione': 'Formato numerico non valido' });
                                }
                            }

                            /* AliquotaRitenuta */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['AliquotaRitenuta']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->AliquotaRitenuta', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['AliquotaRitenuta'] &&
                                    (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['AliquotaRitenuta'].length < 4 ||
                                        data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['AliquotaRitenuta'].length > 6 ||
                                        !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['AliquotaRitenuta'])
                                    )) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->AliquotaRitenuta', 'descrizione': 'Formato numerico non valido' });
                                }
                            }

                            /* CausalePagamento */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['CausalePagamento']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->CausalePagamento', 'descrizione': 'tag mancante' });
                            } else {
                                var stringArray = ['A', 'B', 'C', 'D', 'E', 'G', 'H', 'I', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['CausalePagamento'] &&
                                    (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['CausalePagamento'].length < 1 ||
                                        data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['CausalePagamento'].length > 2 ||
                                        stringArray.indexOf(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']['CausalePagamento']) == -1
                                    )) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta->CausalePagamento', 'descrizione': 'Formato numerico non valido' });
                                }
                            }
                        }

                        /* Dati Bollo */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']) {
                            /* BolloVirtuale */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['BolloVirtuale']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiBollo->BolloVirtuale', 'descrizione': 'tag mancante','soluzione':'In presenza di bollo in fattura occorre settare il Bollo Virtuale' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['BolloVirtuale'] != 'SI' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['BolloVirtuale'] != 'NO') {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiBollo->BolloVirtuale', 'descrizione': 'Formato non valido' });
                                }
                            }

                            /* ImportoBollo */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['ImportoBollo']) {
                                //errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiBollo->ImportoBollo', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['ImportoBollo'] &&
                                    (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['ImportoBollo'].length < 4 ||
                                        data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['ImportoBollo'].length > 15 ||
                                        !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiBollo']['ImportoBollo'])
                                    )) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiBollo->ImportoBollo', 'descrizione': 'Formato numerico non valido' });
                                }
                            }
                        }

                        /* DatiCassaPrevidenziale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale']) {
                            if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale']) &&
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'].length > 0) {
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'].forEach(function(cassa) {
                                    /* TipoCassa */
                                    if (!cassa['TipoCassa']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->TipoCassa', 'descrizione': 'tag mancante' });
                                    } else {
                                        var stringArray = ['TC01', 'TC02', 'TC03', 'TC04', 'TC05', 'TC06', 'TC07', 'TC08', 'TC09', 'TC10', 'TC11', 'TC12', 'TC13', 'TC14', 'TC15', 'TC16', 'TC17', 'TC18', 'TC19', 'TC20', 'TC21', 'TC22'];
                                        if (!cassa['TipoCassa'] || cassa['TipoCassa'].length != 4 ||
                                            stringArray.indexOf(cassa['TipoCassa']) == -1) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->TipoCassa', 'descrizione': 'Valore stringa non valido' });
                                        }
                                    }

                                    /* AlCassa */
                                    if (!cassa['AlCassa']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AlCassa', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (cassa['AlCassa'] && cassa['AlCassa'].length > 6 ||
                                            !isNumeric(cassa['AlCassa'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AlCassa', 'descrizione': 'Valore stringa non valido' });
                                        }
                                    }

                                    /* ImportoContributoCassa */
                                    if (!cassa['ImportoContributoCassa']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->ImportoContributoCassa', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (cassa['ImportoContributoCassa'] && cassa['ImportoContributoCassa'].length < 4 ||
                                            cassa['ImportoContributoCassa'] && cassa['ImportoContributoCassa'].length > 15 ||
                                            !isNumeric(cassa['ImportoContributoCassa'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->ImportoContributoCassa', 'descrizione': 'Valore stringa non valido' });
                                        }
                                    }

                                    /* ImponibileCassa */
                                    if (cassa['ImponibileCassa']) {
                                        if (cassa['ImponibileCassa'] && cassa['ImponibileCassa'].length < 4 ||
                                            cassa['ImponibileCassa'] && cassa['ImponibileCassa'].length > 15 ||
                                            !isNumeric(cassa['ImponibileCassa'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->ImponibileCassa', 'descrizione': 'Valore stringa non valido' });
                                        }
                                    }

                                    /* AliquotaIVA */
                                    if (!cassa['AliquotaIVA']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AliquotaIVA', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (cassa['AliquotaIVA'] && cassa['AliquotaIVA'].length < 4 ||
                                            cassa['AliquotaIVA'] && cassa['AliquotaIVA'].length > 6 ||
                                            !isNumeric(cassa['AliquotaIVA'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AliquotaIVA(00424)', 'descrizione': 'Valore stringa non valido' });
                                        } else {
                                            if (cassa['AliquotaIVA'] == 0) {
                                                /* Natura */
                                                if (!cassa['Natura']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Natura(00413)', 'descrizione': 'tag mancante a fronte di AliquotaIVA pari a 0','soluzione':'La rivalsa/cassa previdenziale deve avere indicata la natura di esenzione iva' });
                                                } else {
                                                    var stringArray = ['N1', 'N2.1','N2.2', 'N3.1','N3.2','N3.3','N3.4','N3.5','N3.6', 'N4', 'N5', 'N6.1','N6.2','N6.3','N6.4','N6.5','N6.6','N6.7', 'N6.8','N7'];
                                                    if (cassa['Natura'].length > 4 || cassa['Natura'].length < 2 ||  
                                                        stringArray.indexOf(cassa['Natura']) == -1) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Natura', 'descrizione': 'Formato Stringa non valido' });

                                                    }
                                                }
                                            } else {
                                                if (cassa['Natura']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Natura(00414)', 'descrizione': 'Natura presente a fronte di AliquotaIVA diversa da 0' });
                                                }
                                            }
                                        }
                                    }

                                    /* Ritenuta */
                                    if (cassa['Ritenuta']) {
                                        if (cassa['Ritenuta'] != 'SI') {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Ritenuta', 'descrizione': 'Valore stringa non valido' });
                                        } else {
                                            /*if (cassa['Ritenuta'] == 'SI') {
                                                if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta(00415)', 'descrizione': 'tag mancante in presenza di Ritenuta SI in almeno una linea di DatiCassaPrevidenziale' });
                                                }
                                            }*/
                                        }
                                    }



                                });
                            } else {
                                var cassa = data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'];

                                /* TipoCassa */
                                if (!cassa['TipoCassa']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->TipoCassa', 'descrizione': 'tag mancante' });
                                } else {
                                    var stringArray = ['TC01', 'TC02', 'TC03', 'TC04', 'TC05', 'TC06', 'TC07', 'TC08', 'TC09', 'TC10', 'TC11', 'TC12', 'TC13', 'TC14', 'TC15', 'TC16', 'TC17', 'TC18', 'TC19', 'TC20', 'TC21', 'TC22'];

                                    if (!cassa['TipoCassa'] || cassa['TipoCassa'].length != 4 ||
                                        stringArray.indexOf(cassa['TipoCassa']) == -1) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->TipoCassa', 'descrizione': 'Valore stringa non valido' });
                                    }
                                }

                                /* AlCassa */
                                if (!cassa['AlCassa']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AlCassa', 'descrizione': 'tag mancante' });
                                } else {
                                    if (cassa['AlCassa'] && cassa['AlCassa'].length > 6 ||
                                        !isNumeric(cassa['AlCassa'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AlCassa', 'descrizione': 'Valore stringa non valido' });
                                    }
                                }

                                /* ImportoContributoCassa */
                                if (!cassa['ImportoContributoCassa']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->ImportoContributoCassa', 'descrizione': 'tag mancante' });
                                } else {
                                    if (cassa['ImportoContributoCassa'] && cassa['ImportoContributoCassa'].length < 4 ||
                                        cassa['ImportoContributoCassa'] && cassa['ImportoContributoCassa'].length > 15 ||
                                        !isNumeric(cassa['ImportoContributoCassa'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->ImportoContributoCassa', 'descrizione': 'Valore stringa non valido' });
                                    }
                                }

                                /* ImponibileCassa */
                                if (cassa['ImponibileCassa']) {
                                    if (cassa['ImponibileCassa'] && cassa['ImponibileCassa'].length < 4 ||
                                        cassa['ImponibileCassa'] && cassa['ImponibileCassa'].length > 15 ||
                                        !isNumeric(cassa['ImponibileCassa'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->ImponibileCassa', 'descrizione': 'Valore stringa non valido' });
                                    }
                                }

                                /* AliquotaIVA */
                                if (!cassa['AliquotaIVA']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AliquotaIVA', 'descrizione': 'tag mancante' });
                                } else {
                                    if (cassa['AliquotaIVA'] && cassa['AliquotaIVA'].length < 4 ||
                                        cassa['AliquotaIVA'] && cassa['AliquotaIVA'].length > 6 ||
                                        !isNumeric(cassa['AliquotaIVA'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->AliquotaIVA(00424)', 'descrizione': 'Valore stringa non valido' });
                                    } else {
                                        if (cassa['AliquotaIVA'] == 0) {
                                            /* Natura */
                                            if (!cassa['Natura']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Natura(00413)', 'descrizione': 'tag mancante a fronte di AliquotaIVA pari a 0','soluzione':'La rivalsa/cassa previdenziale deve avere indicata la natura di esenzione iva'  });
                                            } else {
                                                var stringArray = ['N1', 'N2.1','N2.2', 'N3.1','N3.2','N3.3','N3.4','N3.5','N3.6', 'N4', 'N5', 'N6.1','N6.2','N6.3','N6.4','N6.5','N6.6','N6.7', 'N6.8','N7'];
                                                if (cassa['Natura'].length < 2 || cassa['Natura'].length > 4 || 
                                                    stringArray.indexOf(cassa['Natura']) == -1) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Natura', 'descrizione': 'Formato Stringa non valido' });

                                                }
                                            }
                                        } else {
                                            if (cassa['Natura']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Natura(00414)', 'descrizione': 'Natura presente a fronte di AliquotaIVA diversa da 0' });
                                            }
                                        }
                                    }
                                }

                                /* Ritenuta */
                                if (cassa['Ritenuta']) {
                                    if (cassa['Ritenuta'] != 'SI') {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiCassaPrevidenziale->Ritenuta', 'descrizione': 'Valore stringa non valido' });
                                    } else {
                                        /*if (cassa['Ritenuta'] == 'SI') {
                                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta(00415)', 'descrizione': 'tag mancante in presenza di Ritenuta SI in almeno una linea di DatiCassaPrevidenziale' });
                                            }
                                        }*/
                                    }
                                }
                            }
                        }

                        /* ScontoMaggiorazione */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']) {
                            /* Tipo */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Tipo']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->ScontoMaggiorazione->Tipo', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Tipo'] != 'SC' &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Tipo'] != 'MG') {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->ScontoMaggiorazione->Tipo', 'descrizione': 'Stringa non valida' });
                                } else {
                                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Percentuale'] &&
                                        !data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Importo']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->ScontoMaggiorazione->Tipo(00437)', 'descrizione': 'Atteso valore di Percentuale o Importo a fronte di Tipo Sconto valorizzato' });
                                    }
                                }
                            }

                            /* Percentuale */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Percentuale'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Percentuale'].length < 4 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Percentuale'].length > 6 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Percentuale']))) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->ScontoMaggiorazione->Percentuale', 'descrizione': 'Formato stringa non valido' });
                            }

                            /* Importo */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Importo'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Importo'].length < 4 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Importo'].length > 15 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ScontoMaggiorazione']['Importo']))) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->ScontoMaggiorazione->Importo', 'descrizione': 'Formato stringa non valido' });
                            }
                        }

                        /* ImportoTotaleDocumento */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ImportoTotaleDocumento']) {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ImportoTotaleDocumento'].length < 4 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ImportoTotaleDocumento'].length > 15 ||
                                !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['ImportoTotaleDocumento'])) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->ImportoTotaleDocumento', 'descrizione': 'Formato NON valido' });
                            }
                        }

                        /* Arrotondamento */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Arrotondamento']) {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Arrotondamento'].length < 4 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Arrotondamento'].length > 15 ||
                                !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Arrotondamento'])) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Arrotondamento', 'descrizione': 'Formato NON valido' });
                            }
                        }

                        /* Causale */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Causale']) {
                            if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Causale']) &&
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Causale'].length > 0) {
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Causale'].forEach(function(caus) {
                                    if (caus.length < 1 || caus.length > 200) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Causale', 'descrizione': 'Formato NON valido','soluzione':'La causale inserita non è valida (massimo 200 caratteri)' });
                                    }
                                });
                            } else if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Causale'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['Causale'].length > 200) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->Causale', 'descrizione': 'Formato NON valido','soluzione':'La causale inserita non è valida (massimo 200 caratteri)' });
                            }
                        }
                    }

                    /* DatiOrdineAcquisto */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']) &&
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto'].length > 0) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto'].forEach(function(acquisto) {
                                /* RiferimentoNumeroLinea */
                                if (acquisto['RiferimentoNumeroLinea']) {
                                    if (util.isArray(acquisto['RiferimentoNumeroLinea']) &&
                                        acquisto['RiferimentoNumeroLinea'].length > 0) {
                                        acquisto['RiferimentoNumeroLinea'].forEach(function(caus) {
                                            if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                            }
                                        });
                                    } else if (acquisto['RiferimentoNumeroLinea'].length < 1 ||
                                        acquisto['RiferimentoNumeroLinea'].length > 4 ||
                                        !isNumeric(acquisto['RiferimentoNumeroLinea'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* IdDocumento */
                                if (!acquisto['IdDocumento']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->IdDocumento', 'descrizione': 'tag mancante' });
                                } else {
                                    if (acquisto['IdDocumento'].length < 1 ||
                                        acquisto['IdDocumento'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->IdDocumento', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* Data */
                                if (acquisto['Data'] &&
                                    (acquisto['Data'].length != 10 ||
                                        !moment(acquisto['Data'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->Data', 'descrizione': 'Formato stringa NON corretto' });
                                }

                                /* NumItem */
                                if (acquisto['NumItem']) {
                                    if (acquisto['NumItem'].length < 1 ||
                                        acquisto['NumItem'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->NumItem', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCommessaConvenzione */
                                if (acquisto['CodiceCommessaConvenzione']) {
                                    if (acquisto['CodiceCommessaConvenzione'].length < 1 ||
                                        acquisto['CodiceCommessaConvenzione'].length > 100) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCUP */
                                if (acquisto['CodiceCUP']) {
                                    if (acquisto['CodiceCUP'].length < 1 ||
                                        acquisto['CodiceCUP'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCIG */
                                if (acquisto['CodiceCIG']) {
                                    if (acquisto['CodiceCIG'].length < 1 ||
                                        acquisto['CodiceCIG'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                            });
                        } else {
                            /* RiferimentoNumeroLinea */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea']) {
                                if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea']) &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea'].length > 0) {
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea'].forEach(function(caus) {
                                        if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                        }
                                    });
                                } else if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea'].length > 4 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['RiferimentoNumeroLinea'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* IdDocumento */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['IdDocumento']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->IdDocumento', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['IdDocumento'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['IdDocumento'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->IdDocumento', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* Data */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['Data'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['Data'].length != 10 ||
                                    !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['Data'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->Data', 'descrizione': 'Formato stringa NON corretto' });
                            }

                            /* NumItem */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['NumItem']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['NumItem'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['NumItem'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->NumItem', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCommessaConvenzione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCommessaConvenzione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCommessaConvenzione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCommessaConvenzione'].length > 100) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCUP */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCUP']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCUP'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCUP'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCIG */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCIG']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCIG'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiOrdineAcquisto']['CodiceCIG'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiOrdineAcquisto->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                }
                            }
                        }
                    }

                    /* DatiContratto */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']) &&
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto'].length > 0) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto'].forEach(function(acquisto) {
                                /* RiferimentoNumeroLinea */
                                if (acquisto['RiferimentoNumeroLinea']) {
                                    if (util.isArray(acquisto['RiferimentoNumeroLinea']) &&
                                        acquisto['RiferimentoNumeroLinea'].length > 0) {
                                        acquisto['RiferimentoNumeroLinea'].forEach(function(caus) {
                                            if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                            }
                                        });
                                    } else if (acquisto['RiferimentoNumeroLinea'].length < 1 ||
                                        acquisto['RiferimentoNumeroLinea'].length > 4 ||
                                        !isNumeric(acquisto['RiferimentoNumeroLinea'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* IdDocumento */
                                if (!acquisto['IdDocumento']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->IdDocumento', 'descrizione': 'tag mancante' });
                                } else {
                                    if (acquisto['IdDocumento'].length < 1 ||
                                        acquisto['IdDocumento'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->IdDocumento', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* Data */
                                if (acquisto['Data'] &&
                                    (acquisto['Data'].length != 10 ||
                                        !moment(acquisto['Data'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->Data', 'descrizione': 'Formato stringa NON corretto' });
                                }

                                /* NumItem */
                                if (acquisto['NumItem']) {
                                    if (acquisto['NumItem'].length < 1 ||
                                        acquisto['NumItem'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->NumItem', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCommessaConvenzione */
                                if (acquisto['CodiceCommessaConvenzione']) {
                                    if (acquisto['CodiceCommessaConvenzione'].length < 1 ||
                                        acquisto['CodiceCommessaConvenzione'].length > 100) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCUP */
                                if (acquisto['CodiceCUP']) {
                                    if (acquisto['CodiceCUP'].length < 1 ||
                                        acquisto['CodiceCUP'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCIG */
                                if (acquisto['CodiceCIG']) {
                                    if (acquisto['CodiceCIG'].length < 1 ||
                                        acquisto['CodiceCIG'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                            });
                        } else {
                            /* RiferimentoNumeroLinea */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea']) {
                                if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea']) &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea'].length > 0) {
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea'].forEach(function(caus) {
                                        if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                        }
                                    });
                                } else if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea'].length > 4 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['RiferimentoNumeroLinea'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* IdDocumento */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['IdDocumento']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->IdDocumento', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['IdDocumento'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['IdDocumento'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->IdDocumento', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* Data */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['Data'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['Data'].length != 10 ||
                                    !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['Data'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->Data', 'descrizione': 'Formato stringa NON corretto' });
                            }

                            /* NumItem */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['NumItem']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['NumItem'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['NumItem'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->NumItem', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCommessaConvenzione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCommessaConvenzione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCommessaConvenzione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCommessaConvenzione'].length > 100) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCUP */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCUP']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCUP'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCUP'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCIG */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCIG']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCIG'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiContratto']['CodiceCIG'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiContratto->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                }
                            }
                        }
                    }

                    /* DatiFattureCollegate */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']) &&
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate'].length > 0) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate'].forEach(function(acquisto) {
                                /* RiferimentoNumeroLinea */
                                if (acquisto['RiferimentoNumeroLinea']) {
                                    if (util.isArray(acquisto['RiferimentoNumeroLinea']) &&
                                        acquisto['RiferimentoNumeroLinea'].length > 0) {
                                        acquisto['RiferimentoNumeroLinea'].forEach(function(caus) {
                                            if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                            }
                                        });
                                    } else if (acquisto['RiferimentoNumeroLinea'].length < 1 ||
                                        acquisto['RiferimentoNumeroLinea'].length > 4 ||
                                        !isNumeric(acquisto['RiferimentoNumeroLinea'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* IdDocumento */
                                if (!acquisto['IdDocumento']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->IdDocumento', 'descrizione': 'tag mancante' });
                                } else {
                                    if (acquisto['IdDocumento'].length < 1 ||
                                        acquisto['IdDocumento'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->IdDocumento', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* Data */
                                if (acquisto['Data'] &&
                                    (acquisto['Data'].length != 10 ||
                                        !moment(acquisto['Data'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->Data', 'descrizione': 'Formato stringa NON corretto' });
                                }

                                /* NumItem */
                                if (acquisto['NumItem']) {
                                    if (acquisto['NumItem'].length < 1 ||
                                        acquisto['NumItem'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->NumItem', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCommessaConvenzione */
                                if (acquisto['CodiceCommessaConvenzione']) {
                                    if (acquisto['CodiceCommessaConvenzione'].length < 1 ||
                                        acquisto['CodiceCommessaConvenzione'].length > 100) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCUP */
                                if (acquisto['CodiceCUP']) {
                                    if (acquisto['CodiceCUP'].length < 1 ||
                                        acquisto['CodiceCUP'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCIG */
                                if (acquisto['CodiceCIG']) {
                                    if (acquisto['CodiceCIG'].length < 1 ||
                                        acquisto['CodiceCIG'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                            });
                        } else {
                            /* RiferimentoNumeroLinea */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea']) {
                                if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea']) &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea'].length > 0) {
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea'].forEach(function(caus) {
                                        if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                        }
                                    });
                                } else if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea'].length > 4 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['RiferimentoNumeroLinea'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* IdDocumento */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['IdDocumento']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->IdDocumento', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['IdDocumento'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['IdDocumento'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->IdDocumento', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* Data */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['Data'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['Data'].length != 10 ||
                                    !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['Data'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->Data', 'descrizione': 'Formato stringa NON corretto' });
                            }

                            /* NumItem */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['NumItem']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['NumItem'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['NumItem'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->NumItem', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCommessaConvenzione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCommessaConvenzione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCommessaConvenzione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCommessaConvenzione'].length > 100) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCUP */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCUP']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCUP'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCUP'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCIG */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCIG']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCIG'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiFattureCollegate']['CodiceCIG'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiFattureCollegate->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                }
                            }
                        }
                    }

                    /* DatiConvenzione */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']) &&
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione'].length > 0) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione'].forEach(function(acquisto) {
                                /* RiferimentoNumeroLinea */
                                if (acquisto['RiferimentoNumeroLinea']) {
                                    if (util.isArray(acquisto['RiferimentoNumeroLinea']) &&
                                        acquisto['RiferimentoNumeroLinea'].length > 0) {
                                        acquisto['RiferimentoNumeroLinea'].forEach(function(caus) {
                                            if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                            }
                                        });
                                    } else if (acquisto['RiferimentoNumeroLinea'].length < 1 ||
                                        acquisto['RiferimentoNumeroLinea'].length > 4 ||
                                        !isNumeric(acquisto['RiferimentoNumeroLinea'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* IdDocumento */
                                if (!acquisto['IdDocumento']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->IdDocumento', 'descrizione': 'tag mancante' });
                                } else {
                                    if (acquisto['IdDocumento'].length < 1 ||
                                        acquisto['IdDocumento'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->IdDocumento', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* Data */
                                if (acquisto['Data'] &&
                                    (acquisto['Data'].length != 10 ||
                                        !moment(acquisto['Data'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->Data', 'descrizione': 'Formato stringa NON corretto' });
                                }

                                /* NumItem */
                                if (acquisto['NumItem']) {
                                    if (acquisto['NumItem'].length < 1 ||
                                        acquisto['NumItem'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->NumItem', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCommessaConvenzione */
                                if (acquisto['CodiceCommessaConvenzione']) {
                                    if (acquisto['CodiceCommessaConvenzione'].length < 1 ||
                                        acquisto['CodiceCommessaConvenzione'].length > 100) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCUP */
                                if (acquisto['CodiceCUP']) {
                                    if (acquisto['CodiceCUP'].length < 1 ||
                                        acquisto['CodiceCUP'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCIG */
                                if (acquisto['CodiceCIG']) {
                                    if (acquisto['CodiceCIG'].length < 1 ||
                                        acquisto['CodiceCIG'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                            });
                        } else {
                            /* RiferimentoNumeroLinea */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea']) {
                                if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea']) &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea'].length > 0) {
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea'].forEach(function(caus) {
                                        if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                        }
                                    });
                                } else if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea'].length > 4 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['RiferimentoNumeroLinea'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* IdDocumento */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['IdDocumento']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->IdDocumento', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['IdDocumento'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['IdDocumento'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->IdDocumento', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* Data */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['Data'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['Data'].length != 10 ||
                                    !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['Data'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->Data', 'descrizione': 'Formato stringa NON corretto' });
                            }

                            /* NumItem */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['NumItem']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['NumItem'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['NumItem'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->NumItem', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCommessaConvenzione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCommessaConvenzione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCommessaConvenzione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCommessaConvenzione'].length > 100) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCUP */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCUP']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCUP'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCUP'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCIG */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCIG']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCIG'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiConvenzione']['CodiceCIG'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiConvenzione->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                }
                            }
                        }
                    }

                    /* DatiRicezione */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']) &&
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione'].length > 0) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione'].forEach(function(acquisto) {
                                /* RiferimentoNumeroLinea */
                                if (acquisto['RiferimentoNumeroLinea']) {
                                    if (util.isArray(acquisto['RiferimentoNumeroLinea']) &&
                                        acquisto['RiferimentoNumeroLinea'].length > 0) {
                                        acquisto['RiferimentoNumeroLinea'].forEach(function(caus) {
                                            if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                            }
                                        });
                                    } else if (acquisto['RiferimentoNumeroLinea'].length < 1 ||
                                        acquisto['RiferimentoNumeroLinea'].length > 4 ||
                                        !isNumeric(acquisto['RiferimentoNumeroLinea'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* IdDocumento */
                                if (!acquisto['IdDocumento']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->IdDocumento', 'descrizione': 'tag mancante' });
                                } else {
                                    if (acquisto['IdDocumento'].length < 1 ||
                                        acquisto['IdDocumento'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->IdDocumento', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* Data */
                                if (acquisto['Data'] &&
                                    (acquisto['Data'].length != 10 ||
                                        !moment(acquisto['Data'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->Data', 'descrizione': 'Formato stringa NON corretto' });
                                }

                                /* NumItem */
                                if (acquisto['NumItem']) {
                                    if (acquisto['NumItem'].length < 1 ||
                                        acquisto['NumItem'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->NumItem', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCommessaConvenzione */
                                if (acquisto['CodiceCommessaConvenzione']) {
                                    if (acquisto['CodiceCommessaConvenzione'].length < 1 ||
                                        acquisto['CodiceCommessaConvenzione'].length > 100) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCUP */
                                if (acquisto['CodiceCUP']) {
                                    if (acquisto['CodiceCUP'].length < 1 ||
                                        acquisto['CodiceCUP'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                                /* CodiceCIG */
                                if (acquisto['CodiceCIG']) {
                                    if (acquisto['CodiceCIG'].length < 1 ||
                                        acquisto['CodiceCIG'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                    }
                                }

                            });
                        } else {
                            /* RiferimentoNumeroLinea */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea']) {
                                if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea']) &&
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea'].length > 0) {
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea'].forEach(function(caus) {
                                        if (caus.length < 1 || caus.length > 4 || !isNumeric(caus)) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                        }
                                    });
                                } else if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea'].length > 4 ||
                                    !isNumeric(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['RiferimentoNumeroLinea'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->RiferimentoNumeroLinea', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* IdDocumento */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['IdDocumento']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->IdDocumento', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['IdDocumento'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['IdDocumento'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->IdDocumento', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* Data */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['Data'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['Data'].length != 10 ||
                                    !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['Data'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->Data', 'descrizione': 'Formato stringa NON corretto' });
                            }

                            /* NumItem */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['NumItem']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['NumItem'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['NumItem'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->NumItem', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCommessaConvenzione */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCommessaConvenzione']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCommessaConvenzione'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCommessaConvenzione'].length > 100) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->CodiceCommessaConvenzione', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCUP */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCUP']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCUP'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCUP'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->CodiceCUP', 'descrizione': 'Formato NON valido' });
                                }
                            }

                            /* CodiceCIG */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCIG']) {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCIG'].length < 1 ||
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiRicezione']['CodiceCIG'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiRicezione->CodiceCIG', 'descrizione': 'Formato NON valido' });
                                }
                            }
                        }
                    }

                    /* Dati SAL */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiSAL']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiSAL'])) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiSAL'].forEach(function(sal) {
                                if (!sal['RiferimentoFase']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiSAL->RiferimentoFase', 'descrizione': 'tag mancante' });
                                } else {
                                    if (sal['RiferimentoFase'].length < 1 || sal['RiferimentoFase'].length > 3) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiSAL->RiferimentoFase', 'descrizione': 'Formato stringa non valido' });
                                    }
                                }
                            });
                        } else {
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiSAL']['RiferimentoFase']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiSAL->RiferimentoFase', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiSAL']['RiferimentoFase'].length < 1 || data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiSAL']['RiferimentoFase'].length > 3) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiSAL->RiferimentoFase', 'descrizione': 'Formato stringa non valido' });
                                }
                            }
                        }
                    }

                    /* Dati DDT */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']) {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT'])) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT'].forEach(function(ddt) {
                                if (!ddt['NumeroDDT']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->NumeroDDT', 'descrizione': 'tag mancante' });
                                } else {
                                    if (ddt['NumeroDDT'].length < 1 || ddt['NumeroDDT'].length > 20) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->NumeroDDT', 'descrizione': 'Formato stringa non valido' });
                                    }
                                }

                                if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['DataDDT']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->DataDDT', 'descrizione': 'tag mancante' });
                                } else{
                                    if (ddt['DataDDT'] &&
                                        (ddt['DataDDT'].length != 10 ||
                                            !moment(ddt['DataDDT'], 'YYYY-MM-DD').isValid())) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->DataDDT', 'descrizione': 'Formato stringa NON corretto' });
                                    }
                                }
                            });
                        } else {
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['NumeroDDT']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->NumeroDDT', 'descrizione': 'tag mancante' });
                            } else {
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['NumeroDDT'].length < 1 || data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['NumeroDDT'].length > 20) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->NumeroDDT', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['DataDDT']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->DataDDT', 'descrizione': 'tag mancante' });
                            } else{
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['DataDDT'] &&
                                    (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['DataDDT'].length != 10 ||
                                        !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiDDT']['DataDDT'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiDDT->DataDDT', 'descrizione': 'Formato stringa NON corretto' });
                                }
                            }
                        }
                    }

                    /* Dati Trasporto */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']) {
                        /* Dati Anagrafici Vettore */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']) {
                            /* ID FISCALE ID */
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['IdFiscaleIVA']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->IdFiscaleIVA', 'descrizione': 'tag mancante' });
                            } else {
                                if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['IdFiscaleIVA']['IdPaese']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->IdFiscaleIVA->IdPaese', 'descrizione': 'tag mancante' });
                                }else{
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['IdFiscaleIVA']['IdPaese'].length != 2){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->IdFiscaleIVA->IdPaese', 'descrizione': 'Formato non valido' });
                                    }
                                }

                                if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['IdFiscaleIVA']['IdCodice']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->IdFiscaleIVA->IdCodice', 'descrizione': 'tag mancante' });
                                }else{
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['IdFiscaleIVA']['IdCodice'].length < 1 || 
                                        data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['IdFiscaleIVA']['IdCodice'].length > 28){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->IdFiscaleIVA->IdCodice', 'descrizione': 'Formato non valido' });
                                    }
                                }
                            }

                            /* Codice Fiscale */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['CodiceFiscale']) {
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['CodiceFiscale'].length < 1 || 
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['CodiceFiscale'].length > 20){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->CodiceFiscale', 'descrizione': 'Formato non valido' });
                                }
                            }

                            /* Anagrafica */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']) {
                                
                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Denominazione']) {
                                   
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Denominazione'].length < 1 ||
                                       data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Denominazione'].length > 80 ){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->Anagrafica->Denominazione', 'descrizione': 'Formato non valido' });
                                    }
                                }

                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Nome']) {
                                   
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Nome'].length < 1 ||
                                       data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Nome'].length > 60 ){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->Anagrafica->Nome', 'descrizione': 'Formato non valido' });
                                    }
                                }

                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Cognome']) {
                                   
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Cognome'].length < 1 ||
                                       data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Cognome'].length > 60 ){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->Anagrafica->Cognome', 'descrizione': 'Formato non valido' });
                                    }
                                }

                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Titolo']) {
                                   
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Titolo'].length < 2 ||
                                       data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['Titolo'].length > 10 ){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->Anagrafica->Titolo', 'descrizione': 'Formato non valido' });
                                    }
                                }

                                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['CodEORI']) {
                                   
                                    if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['CodEORI'].length < 13 ||
                                       data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['Anagrafica']['CodEORI'].length > 17 ){
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->Anagrafica->CodEORI', 'descrizione': 'Formato non valido' });
                                    }
                                }
                            }

                            /* NumeroLicenzaGuida */
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['NumeroLicenzaGuida']) {
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['NumeroLicenzaGuida'].length < 1 || 
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DatiAnagraficiVettore']['NumeroLicenzaGuida'].length > 20){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DatiAnagraficiVettore->NumeroLicenzaGuida', 'descrizione': 'Formato non valido' });
                                }
                            }
                        }

                        /* Mezzo di Trasporto*/
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['MezzoTrasporto']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['MezzoTrasporto'].length < 1 || 
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['MezzoTrasporto'].length > 80){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->MezzoTrasporto', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* CausaleTrasporto */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['CausaleTrasporto']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['CausaleTrasporto'].length < 1 || 
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['CausaleTrasporto'].length > 100){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->CausaleTrasporto', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* NumeroColli */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['NumeroColli']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['NumeroColli'].length < 1 || 
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['NumeroColli'].length > 4 ||
                                !isNumeric( data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['NumeroColli'])){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->NumeroColli', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* Descrizione */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['Descrizione']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['Descrizione'].length < 1 || 
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['Descrizione'].length > 100){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->Descrizione', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* UnitaMisuraPeso */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['UnitaMisuraPeso']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['UnitaMisuraPeso'].length < 1 || 
                                    data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['UnitaMisuraPeso'].length > 10){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->UnitaMisuraPeso', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* PesoLordo */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoLordo']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoLordo'].length < 4 || 
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoLordo'].length > 7 ||
                                !isNumeric( data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoLordo'])){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->PesoLordo', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* PesoNetto */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoNetto']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoNetto'].length < 4 || 
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoNetto'].length > 7 ||
                                !isNumeric( data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['PesoNetto'])){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->PesoNetto', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* DataOraRitiro*/
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataOraRitiro'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataOraRitiro'].length != 19 ||
                            !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataOraRitiro'], 'YYYY-MM-DDTHH:MM:SS').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DataOraRitiro', 'descrizione': 'Formato data NON corretto' });
                        }

                        /* DataInizioTrasporto*/
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataInizioTrasporto'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataInizioTrasporto'].length != 10 ||
                            !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataInizioTrasporto'], 'YYYY-MM-DD').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DataInizioTrasporto', 'descrizione': 'Formato data NON corretto' });
                        }

                        /* TipoResa */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['TipoResa']) {
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['TipoResa'].length != 3 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->TipoResa', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* Indirizzo Resa */
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']) {
                            
                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Indirizzo']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Indirizzo', 'descrizione': 'Tag mancante' });
                            }else{
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Indirizzo'].length < 1 ||
                                   data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Indirizzo'].length > 60 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Indirizzo', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['NumeroCivico']) {
                                
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['NumeroCivico'].length < 1 ||
                                   data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['NumeroCivico'].length > 8 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->NumeroCivico', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['CAP']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->CAP', 'descrizione': 'Tag mancante' });
                            }else{
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['CAP'].length != 5 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->CAP', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Comune']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Comune', 'descrizione': 'Tag mancante' });
                            }else{
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Comune'].length < 1 ||
                                   data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Comune'].length > 60 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Comune', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Provincia']) {
                                
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Provincia'].length != 2 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Provincia', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Nazione']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Nazione', 'descrizione': 'Tag mancante' });
                            }else{
                                if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['IndirizzoResa']['Nazione'].length != 2 ){
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->IndirizzoResa->Nazione', 'descrizione': 'Formato non valido' });
                                }
                            }
                        }

                        /* DataOraConsegna*/
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataOraConsegna'] &&
                            (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataOraConsegna'].length != 19 ||
                            !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiTrasporto']['DataOraConsegna'], 'YYYY-MM-DDTHH:MM:SS').isValid())) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiTrasporto->DataOraConsegna', 'descrizione': 'Formato data NON corretto' });
                        }
                    }

                    /* FatturaPrincipale */
                    if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']) {
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['NumeroFatturaPrincipale']) {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->FatturaPrincipale->NumeroFatturaPrincipale', 'descrizione': 'tag mancante' });
                        }else{
                            if(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['NumeroFatturaPrincipale'].length < 1 ||
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['NumeroFatturaPrincipale'].length > 20){
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->FatturaPrincipale->NumeroFatturaPrincipale', 'descrizione': 'Formato non valido' });
                            }
                        }

                        /* DataFatturaPrincipale*/
                        if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['DataFatturaPrincipale']){
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->FatturaPrincipale->DataFatturaPrincipale', 'descrizione': 'tag mancante' });
                        }else{

                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['DataFatturaPrincipale'] &&
                                (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['DataFatturaPrincipale'].length != 10 ||
                                !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['FatturaPrincipale']['DataFatturaPrincipale'], 'YYYY-MM-DD').isValid())) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->FatturaPrincipale->DataFatturaPrincipale', 'descrizione': 'Formato data NON corretto' });
                            }
                        }
                    }
                }

                /* DatiBeniServizi */
                if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']) {

                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi', 'descrizione': 'tag mancante' });
                } else {
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee']) {

                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee', 'descrizione': 'tag mancante','soluzione':'Nessuna riga di fattura' });
                    } else {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'])) {
                            if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'].length == 0) {

                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee', 'descrizione': 'Array vuoto','soluzione':'Nessuna riga di fattura' });
                            } else {
                                data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'].forEach(function(linea) {
                                    /*  NumeroLinea */
                                    if (!linea['NumeroLinea']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->NumeroLinea', 'descrizione': 'tag mancante' });
                                    } else {
                                        //var num_linea = linea['NumeroLinea'].split('.')[0];
                                        var num_linea = linea['NumeroLinea'];
                                        if (num_linea.length < 1 || num_linea.length > 4 || !isNumeric(num_linea)) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->NumeroLinea', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* Tipo Cessione Prestazione */
                                    if (linea['TipoCessionePrestazione']) {
                                        var stringArray = ['SC', 'PR', 'AB', 'AC'];
                                        if (stringArray.indexOf(linea['TipoCessionePrestazione']) == -1) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->TipoCessionePrestazione', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* Codice Articolo */
                                    if (linea['CodiceArticolo']) {
                                        if (util.isArray(linea['CodiceArticolo'])) {
                                            linea['CodiceArticolo'].forEach(function(codice) {
                                                if (!codice['CodiceTipo']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'tag mancante' });
                                                } else {
                                                    if (codice['CodiceTipo'].length < 1 || codice['CodiceTipo'].length > 35) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (!codice['CodiceValore']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'tag mancante' });
                                                } else {
                                                    if (codice['CodiceValore'].length < 1 || codice['CodiceValore'].length > 35) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }
                                            });
                                        } else {
                                            if (!linea['CodiceArticolo']['CodiceTipo']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'tag mancante' });
                                            } else {
                                                if (linea['CodiceArticolo']['CodiceTipo'].length < 1 || linea['CodiceArticolo']['CodiceTipo'].length > 35) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (!linea['CodiceArticolo']['CodiceValore']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'tag mancante' });
                                            } else {
                                                if (linea['CodiceArticolo']['CodiceValore'].length < 1 || linea['CodiceArticolo']['CodiceValore'].length > 35) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }
                                        }
                                    }

                                    /* Descrizione */
                                    if (!linea['Descrizione']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Descrizione', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (linea['Descrizione'].length < 1 || linea['Descrizione'].length > 1000) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Descrizione', 'descrizione': 'Formato Stringa non valido','soluzione':'La riga di fattura "'+linea['Descrizione']+'" non è valida (massimo 1000 caratteri)' });
                                        }
                                    }

                                    /* Quantita */
                                    if (linea['Quantita']) {
                                        if (linea['Quantita'].length < 4 || linea['Quantita'].length > 21 || !isNumeric(linea['Quantita'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Quantita', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* UnitaMisura */
                                    if (linea['UnitaMisura']) {
                                        if (linea['UnitaMisura'].length < 1 || linea['UnitaMisura'].length > 10) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->UnitaMisura', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* PrezzoUnitario */
                                    if (!linea['PrezzoUnitario']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoUnitario', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (linea['PrezzoUnitario'].length < 4 || linea['PrezzoUnitario'].length > 21 || !isNumeric(linea['PrezzoUnitario'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoUnitario', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* ScontoMaggiorazione */
                                    if (linea['ScontoMaggiorazione']) {
                                        if (util.isArray(linea['ScontoMaggiorazione'])) {
                                            linea['ScontoMaggiorazione'].forEach(function(sconto) {
                                                /* Tipo */
                                                if (!sconto['Tipo']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'tag mancante' });
                                                } else {
                                                    if (sconto['Tipo'] != 'SC' &&
                                                        sconto['Tipo'] != 'MG') {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'Stringa non valida' });
                                                    } else {
                                                        if (!sconto['Percentuale'] &&
                                                            !sconto['Importo']) {
                                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo(00438)', 'descrizione': 'Atteso valore di Percentuale o Importo a fronte di Tipo Sconto valorizzato' });
                                                        }

                                                    }
                                                }

                                                /* Percentuale */
                                                if (sconto['Percentuale'] &&
                                                    (sconto['Percentuale'].length < 4 ||
                                                        sconto['Percentuale'].length > 6 ||
                                                        !isNumeric(sconto['Percentuale']))) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Percentuale', 'descrizione': 'Formato stringa non valido' });
                                                }

                                                /* Importo */
                                                if (sconto['Importo'] &&
                                                    (sconto['Importo'].length < 4 ||
                                                        sconto['Importo'].length > 15 ||
                                                        !isNumeric(sconto['Importo']))) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Importo', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            });
                                        } else {
                                            /* Tipo */
                                            if (!linea['ScontoMaggiorazione']['Tipo']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'tag mancante' });
                                            } else {
                                                if (linea['ScontoMaggiorazione']['Tipo'] != 'SC' &&
                                                    linea['ScontoMaggiorazione']['Tipo'] != 'MG') {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'Stringa non valida' });
                                                } else {
                                                    if (!linea['ScontoMaggiorazione']['Percentuale'] &&
                                                        !linea['ScontoMaggiorazione']['Importo']) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo(00438)', 'descrizione': 'Atteso valore di Percentuale o Importo a fronte di Tipo Sconto valorizzato' });
                                                    }
                                                }
                                            }

                                            /* Percentuale */
                                            if (linea['ScontoMaggiorazione']['Percentuale'] &&
                                                (linea['ScontoMaggiorazione']['Percentuale'].length < 4 ||
                                                    linea['ScontoMaggiorazione']['Percentuale'].length > 6 ||
                                                    !isNumeric(linea['ScontoMaggiorazione']['Percentuale']))) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Percentuale', 'descrizione': 'Formato stringa non valido' });
                                            }

                                            /* Importo */
                                            if (linea['ScontoMaggiorazione']['Importo'] &&
                                                (linea['ScontoMaggiorazione']['Importo'].length < 4 ||
                                                    linea['ScontoMaggiorazione']['Importo'].length > 15 ||
                                                    !isNumeric(linea['ScontoMaggiorazione']['Importo']))) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Importo', 'descrizione': 'Formato stringa non valido' });
                                            }
                                        }
                                    }

                                    /* DataFinePeriodo */
                                    if (linea['DataFinePeriodo']) {
                                        
                                        if (linea['DataFinePeriodo'].length != 10 ||
                                            !moment(linea['DataFinePeriodo'], 'YYYY-MM-DD').isValid()) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->DataFinePeriodo', 'descrizione': 'Formato Stringa non valido' });
                                        }
                                    }

                                    /* DataInizioPeriodo */
                                    if (linea['DataInizioPeriodo']) {
                                        
                                        if (linea['DataInizioPeriodo'].length != 10 ||
                                            !moment(linea['DataInizioPeriodo'], 'YYYY-MM-DD').isValid()) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->DataInizioPeriodo', 'descrizione': 'Formato Stringa non valido' });
                                        }
                                    }

                                    /* RiferimentoAmministrazione */
                                    if (linea['RiferimentoAmministrazione']) {
                                        if (linea['RiferimentoAmministrazione'].length < 1 || linea['RiferimentoAmministrazione'].length > 20) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->RiferimentoAmministrazione', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* AltriDatiGestionali */
                                    if (linea['AltriDatiGestionali']) {
                                        if (util.isArray(linea['AltriDatiGestionali'])) {
                                            linea['AltriDatiGestionali'].forEach(function(codice) {
                                                if (!codice['TipoDato']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'tag mancante' });
                                                } else {
                                                    if (codice['TipoDato'].length < 1 || codice['TipoDato'].length > 60) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (codice['RiferimentoTesto']) {
                                                   
                                                    if (codice['RiferimentoTesto'].length < 1 || codice['RiferimentoTesto'].length > 60) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoTesto', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (codice['RiferimentoNumero']) {
                                                   
                                                    if (codice['RiferimentoNumero'].length <4 || codice['RiferimentoNumero'].length > 21 ||
                                                        !is_numeric(codice['RiferimentoNumero'])) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoNumero', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (codice['RiferimentoData']) {
                                                   
                                                    if (codice['RiferimentoData'].length != 10 ||
                                                        !moment(codice['RiferimentoData'], 'YYYY-MM-DD').isValid()) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoData', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }
                                            });
                                        } else {
                                            if (!linea['TipoDato']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'tag mancante' });
                                            } else {
                                                if (linea['TipoDato'].length < 1 || linea['TipoDato'].length > 60) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (linea['RiferimentoTesto']) {
                                               
                                                if (linea['RiferimentoTesto'].length < 1 || linea['RiferimentoTesto'].length > 60) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoTesto', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (linea['RiferimentoNumero']) {
                                               
                                                if (linea['RiferimentoNumero'].length <4 || linea['RiferimentoNumero'].length > 21 ||
                                                    !is_numeric(linea['RiferimentoNumero'])) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoNumero', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (linea['RiferimentoData']) {
                                               
                                                if (linea['RiferimentoData'].length != 10 ||
                                                    !moment(linea['RiferimentoData'], 'YYYY-MM-DD').isValid()) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoData', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }
                                        }
                                    }

                                    /* PrezzoTotale */
                                    if (!linea['PrezzoTotale']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (linea['PrezzoTotale'].length < 4 || linea['PrezzoTotale'].length > 21 || !isNumeric(linea['PrezzoTotale'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale', 'descrizione': 'Formato stringa non valido' });
                                        } else {
                                            if (linea['ScontoMaggiorazione'] && linea['ScontoMaggiorazione']['Importo']) {
                                                if (linea['ScontoMaggiorazione']['Tipo'] == 'SC') {
                                                    if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] - linea['ScontoMaggiorazione']['Importo']) * linea['Quantita'])) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                                    }
                                                } else {
                                                    if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] + linea['ScontoMaggiorazione']['Importo']) * linea['Quantita'])) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                                    }
                                                }

                                            } else if (linea['ScontoMaggiorazione']) {
                                                if (linea['ScontoMaggiorazione']['Tipo'] == 'SC') {
                                                    if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] - (linea['ScontoMaggiorazione']['Percentuale'] * linea['PrezzoUnitario'])) * linea['Quantita'])) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                                    }
                                                } else {
                                                    if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] + (linea['ScontoMaggiorazione']['Percentuale'] * linea['PrezzoUnitario'])) * linea['Quantita'])) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                                    }
                                                }
                                            } else {
                                                if ((parseFloat(formatta_euro(linea['PrezzoTotale'])) > parseFloat(formatta_euro(parseFloat((linea['PrezzoUnitario'] * linea['Quantita']) + 0.01)))) ||
                                                    (parseFloat(formatta_euro(linea['PrezzoTotale'])) < parseFloat(formatta_euro(parseFloat((linea['PrezzoUnitario'] * linea['Quantita']) - 0.01))))) {

                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                                }
                                            }

                                        }
                                    }

                                    /* AliquotaIVA */
                                    if (!linea['AliquotaIVA']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AliquotaIVA', 'descrizione': 'tag mancante' });
                                    } else {

                                        if (linea['AliquotaIVA'] && linea['AliquotaIVA'].length < 4 ||
                                            linea['AliquotaIVA'] && linea['AliquotaIVA'].length > 6 ||
                                            !isNumeric(linea['AliquotaIVA'])) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AliquotaIVA(00424)', 'descrizione': 'Valore stringa non valido' });
                                        } else {
                                            if (linea['AliquotaIVA'] == 0) {
                                                /* Natura */
                                                if (!linea['Natura']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Natura(00413)', 'descrizione': 'tag mancante a fronte di AliquotaIVA pari a 0' });
                                                } else {
                                                    var stringArray = ['N1', 'N2.1','N2.2', 'N3.1','N3.2','N3.3','N3.4','N3.5','N3.6', 'N4', 'N5', 'N6.1','N6.2','N6.3','N6.4','N6.5','N6.6','N6.7', 'N6.8','N6.9','N7'];
                                                    if (linea['Natura'].length < 2 || linea['Natura'].length > 4 ||
                                                        stringArray.indexOf(linea['Natura']) == -1) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Natura', 'descrizione': 'Formato Stringa non valido' });

                                                    }
                                                }
                                            } else {
                                                if (linea['Natura']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Natura(00414)', 'descrizione': 'Natura presente a fronte di AliquotaIVA diversa da 0' });
                                                }
                                            }
                                        }
                                    }

                                    /* Ritenuta */
                                    if (linea['Ritenuta']) {
                                        if (linea['Ritenuta'] == 'SI') {
                                            if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta(00411)', 'descrizione': 'tag mancante in presenza di Ritenuta SI in almeno un dettaglio Linea' });
                                            }
                                        } else {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Ritenuta', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }
                                });
                            }
                        } else /* SINGOLA LINEA */ {
                            var linea = data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'];
                            /*  NumeroLinea */
                            if (!linea['NumeroLinea']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->NumeroLinea', 'descrizione': 'tag mancante' });
                            } else {
                                //var num_linea = linea['NumeroLinea'].split('.')[0];
                                var num_linea = linea['NumeroLinea'];
                                if (num_linea.length < 1 || num_linea.length > 4 || !isNumeric(num_linea)) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->NumeroLinea', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                            /* Tipo Cessione Prestazione */
                            if (linea['TipoCessionePrestazione']) {
                                var stringArray = ['SC', 'PR', 'AB', 'AC'];
                                if (stringArray.indexOf(linea['TipoCessionePrestazione']) == -1) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->TipoCessionePrestazione', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                            /* Codice Articolo */
                            if (linea['CodiceArticolo']) {
                                if (util.isArray(linea['CodiceArticolo'])) {
                                    linea['CodiceArticolo'].forEach(function(codice) {
                                        if (!codice['CodiceTipo']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'tag mancante' });
                                        } else {
                                            if (codice['CodiceTipo'].length < 1 || codice['CodiceTipo'].length > 35) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'Formato stringa non valido' });
                                            }
                                        }

                                        if (!codice['CodiceValore']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'tag mancante' });
                                        } else {
                                            if (codice['CodiceValore'].length < 1 || codice['CodiceValore'].length > 35) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'Formato stringa non valido' });
                                            }
                                        }
                                    });
                                } else {
                                    if (!linea['CodiceArticolo']['CodiceTipo']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (linea['CodiceArticolo']['CodiceTipo'].length < 1 || linea['CodiceArticolo']['CodiceTipo'].length > 35) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceTipo', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    if (!linea['CodiceArticolo']['CodiceValore']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (linea['CodiceArticolo']['CodiceValore'].length < 1 || linea['CodiceArticolo']['CodiceValore'].length > 35) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->CodiceArticolo->CodiceValore', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }
                                }
                            }

                            /* Descrizione */
                            if (!linea['Descrizione']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Descrizione', 'descrizione': 'tag mancante' });
                            } else {
                                if (linea['Descrizione'].length < 1 || linea['Descrizione'].length > 1000) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Descrizione', 'descrizione': 'Formato Stringa non valido','soluzione':'La riga di fattura "'+linea['Descrizione']+'" non è valida (massimo 1000 caratteri)' });
                                }
                            }

                            /* Quantita */
                            if (linea['Quantita']) {
                                if (linea['Quantita'].length < 4 || linea['Quantita'].length > 21 || !isNumeric(linea['Quantita'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Quantita', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                            /* UnitaMisura */
                            if (linea['UnitaMisura']) {
                                if (linea['UnitaMisura'].length < 1 || linea['UnitaMisura'].length > 10) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->UnitaMisura', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                            /* PrezzoUnitario */
                            if (!linea['PrezzoUnitario']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoUnitario', 'descrizione': 'tag mancante' });
                            } else {
                                if (linea['PrezzoUnitario'].length < 4 || linea['PrezzoUnitario'].length > 21 || !isNumeric(linea['PrezzoUnitario'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoUnitario', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                            /* ScontoMaggiorazione */
                            if (linea['ScontoMaggiorazione']) {
                                if (util.isArray(linea['ScontoMaggiorazione'])) {
                                    linea['ScontoMaggiorazione'].forEach(function(sconto) {
                                        /* Tipo */
                                        if (!sconto['Tipo']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'tag mancante' });
                                        } else {
                                            if (sconto['Tipo'] != 'SC' &&
                                                sconto['Tipo'] != 'MG') {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'Stringa non valida' });
                                            } else {
                                                if (!sconto['Percentuale'] &&
                                                    !sconto['Importo']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo(00438)', 'descrizione': 'Atteso valore di Percentuale o Importo a fronte di Tipo Sconto valorizzato' });
                                                }

                                            }
                                        }

                                        /* Percentuale */
                                        if (sconto['Percentuale'] &&
                                            (sconto['Percentuale'].length < 4 ||
                                                sconto['Percentuale'].length > 6 ||
                                                !isNumeric(sconto['Percentuale']))) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Percentuale', 'descrizione': 'Formato stringa non valido' });
                                        }

                                        /* Importo */
                                        if (sconto['Importo'] &&
                                            (sconto['Importo'].length < 4 ||
                                                sconto['Importo'].length > 15 ||
                                                !isNumeric(sconto['Importo']))) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Importo', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    });
                                } else {
                                    /* Tipo */
                                    if (!linea['ScontoMaggiorazione']['Tipo']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'tag mancante' });
                                    } else {
                                        if (linea['ScontoMaggiorazione']['Tipo'] != 'SC' &&
                                            linea['ScontoMaggiorazione']['Tipo'] != 'MG') {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo', 'descrizione': 'Stringa non valida' });
                                        } else {
                                            if (!linea['ScontoMaggiorazione']['Percentuale'] &&
                                                !linea['ScontoMaggiorazione']['Importo']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Tipo(00438)', 'descrizione': 'Atteso valore di Percentuale o Importo a fronte di Tipo Sconto valorizzato' });
                                            }
                                        }
                                    }

                                    /* Percentuale */
                                    if (linea['ScontoMaggiorazione']['Percentuale'] &&
                                        (linea['ScontoMaggiorazione']['Percentuale'].length < 4 ||
                                            linea['ScontoMaggiorazione']['Percentuale'].length > 6 ||
                                            !isNumeric(linea['ScontoMaggiorazione']['Percentuale']))) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Percentuale', 'descrizione': 'Formato stringa non valido' });
                                    }

                                    /* Importo */
                                    if (linea['ScontoMaggiorazione']['Importo'] &&
                                        (linea['ScontoMaggiorazione']['Importo'].length < 4 ||
                                            linea['ScontoMaggiorazione']['Importo'].length > 15 ||
                                            !isNumeric(linea['ScontoMaggiorazione']['Importo']))) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->ScontoMaggiorazione->Importo', 'descrizione': 'Formato stringa non valido' });
                                    }
                                }
                            }

                            /* DataFinePeriodo */
                                    if (linea['DataFinePeriodo']) {
                                        
                                        if (linea['DataFinePeriodo'].length != 10 ||
                                            !moment(linea['DataFinePeriodo'], 'YYYY-MM-DD').isValid()) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->DataFinePeriodo', 'descrizione': 'Formato Stringa non valido' });
                                        }
                                    }

                                    /* DataInizioPeriodo */
                                    if (linea['DataInizioPeriodo']) {
                                        
                                        if (linea['DataInizioPeriodo'].length != 10 ||
                                            !moment(linea['DataInizioPeriodo'], 'YYYY-MM-DD').isValid()) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->DataInizioPeriodo', 'descrizione': 'Formato Stringa non valido' });
                                        }
                                    }

                                    /* RiferimentoAmministrazione */
                                    if (linea['RiferimentoAmministrazione']) {
                                        if (linea['RiferimentoAmministrazione'].length < 1 || linea['RiferimentoAmministrazione'].length > 20) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->RiferimentoAmministrazione', 'descrizione': 'Formato stringa non valido' });
                                        }
                                    }

                                    /* AltriDatiGestionali */
                                    if (linea['AltriDatiGestionali']) {
                                        if (util.isArray(linea['AltriDatiGestionali'])) {
                                            linea['AltriDatiGestionali'].forEach(function(codice) {
                                                if (!codice['TipoDato']) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'tag mancante' });
                                                } else {
                                                    if (codice['TipoDato'].length < 1 || codice['TipoDato'].length > 60) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (codice['RiferimentoTesto']) {
                                                   
                                                    if (codice['RiferimentoTesto'].length < 1 || codice['RiferimentoTesto'].length > 60) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoTesto', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (codice['RiferimentoNumero']) {
                                                   
                                                    if (codice['RiferimentoNumero'].length <4 || codice['RiferimentoNumero'].length > 21 ||
                                                        !is_numeric(codice['RiferimentoNumero'])) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoNumero', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }

                                                if (codice['RiferimentoData']) {
                                                   
                                                    if (codice['RiferimentoData'].length != 10 ||
                                                        !moment(codice['RiferimentoData'], 'YYYY-MM-DD').isValid()) {
                                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoData', 'descrizione': 'Formato stringa non valido' });
                                                    }
                                                }
                                            });
                                        } else {
                                            if (!linea['TipoDato']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'tag mancante' });
                                            } else {
                                                if (linea['TipoDato'].length < 1 || linea['TipoDato'].length > 60) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->TipoDato', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (linea['RiferimentoTesto']) {
                                               
                                                if (linea['RiferimentoTesto'].length < 1 || linea['RiferimentoTesto'].length > 60) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoTesto', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (linea['RiferimentoNumero']) {
                                               
                                                if (linea['RiferimentoNumero'].length <4 || linea['RiferimentoNumero'].length > 21 ||
                                                    !is_numeric(linea['RiferimentoNumero'])) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoNumero', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }

                                            if (linea['RiferimentoData']) {
                                               
                                                if (linea['RiferimentoData'].length != 10 ||
                                                    !moment(linea['RiferimentoData'], 'YYYY-MM-DD').isValid()) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AltriDatiGestionali->RiferimentoData', 'descrizione': 'Formato stringa non valido' });
                                                }
                                            }
                                        }
                                    }

                            /* PrezzoTotale */
                            if (!linea['PrezzoTotale']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale', 'descrizione': 'tag mancante' });
                            } else {
                                if (linea['PrezzoTotale'].length < 4 || linea['PrezzoTotale'].length > 21 || !isNumeric(linea['PrezzoTotale'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale', 'descrizione': 'Formato stringa non valido' });
                                } else {
                                    if (linea['ScontoMaggiorazione'] && linea['ScontoMaggiorazione']['Importo']) {
                                        if (linea['ScontoMaggiorazione']['Tipo'] == 'SC') {
                                            if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] - linea['ScontoMaggiorazione']['Importo']) * linea['Quantita'])) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                            }
                                        } else {
                                            if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] + linea['ScontoMaggiorazione']['Importo']) * linea['Quantita'])) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                            }
                                        }

                                    } else if (linea['ScontoMaggiorazione']) {
                                        if (linea['ScontoMaggiorazione']['Tipo'] == 'SC') {
                                            if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] - (linea['ScontoMaggiorazione']['Percentuale'] * linea['PrezzoUnitario'])) * linea['Quantita'])) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                            }
                                        } else {
                                            if (linea['PrezzoTotale'] != ((linea['PrezzoUnitario'] + (linea['ScontoMaggiorazione']['Percentuale'] * linea['PrezzoUnitario'])) * linea['Quantita'])) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                            }
                                        }
                                    } else {
                                        /*console.log((formatta_euro(linea['PrezzoTotale'])));
                                            console.log(formatta_euro(parseFloat((linea['PrezzoUnitario'] * linea['Quantita']) + 0.01 )));
                                            console.log(formatta_euro(parseFloat((linea['PrezzoUnitario'] * linea['Quantita']) - 0.01 )));
                                        */
                                        if ((parseFloat(formatta_euro(linea['PrezzoTotale'])) > parseFloat(formatta_euro(parseFloat((linea['PrezzoUnitario'] * linea['Quantita']) + 0.01)))) ||
                                            (parseFloat(formatta_euro(linea['PrezzoTotale'])) < parseFloat(formatta_euro(parseFloat((linea['PrezzoUnitario'] * linea['Quantita']) - 0.01))))) {

                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->PrezzoTotale(00423)', 'descrizione': 'Importo totale non concorde con dati precedenti' });
                                        }
                                    }

                                }
                            }

                            /* AliquotaIVA */
                            if (!linea['AliquotaIVA']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AliquotaIVA', 'descrizione': 'tag mancante' });
                            } else {

                                if (linea['AliquotaIVA'] && linea['AliquotaIVA'].length < 4 ||
                                    linea['AliquotaIVA'] && linea['AliquotaIVA'].length > 6 ||
                                    !isNumeric(linea['AliquotaIVA'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->AliquotaIVA(00424)', 'descrizione': 'Valore stringa non valido' });
                                } else {
                                    if (linea['AliquotaIVA'] == 0) {
                                        /* Natura */
                                        if (!linea['Natura']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Natura(00413)', 'descrizione': 'tag mancante a fronte di AliquotaIVA pari a 0' });
                                        } else {
                                           var stringArray = ['N1', 'N2.1','N2.2', 'N3.1','N3.2','N3.3','N3.4','N3.5','N3.6', 'N4', 'N5', 'N6.1','N6.2','N6.3','N6.4','N6.5','N6.6','N6.7', 'N6.8','N6.9','N7'];
                                            if (linea['Natura'].length < 2 || linea['Natura'].length > 4 ||
                                                stringArray.indexOf(linea['Natura']) == -1) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Natura', 'descrizione': 'Formato Stringa non valido' });

                                            }
                                        }
                                    } else {
                                        if (linea['Natura']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Natura(00414)', 'descrizione': 'Natura presente a fronte di AliquotaIVA diversa da 0' });
                                        }
                                    }
                                }
                            }

                            /* Ritenuta */
                            if (linea['Ritenuta']) {
                                if (linea['Ritenuta'] == 'SI') {
                                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiRitenuta']) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiGenerali->DatiGeneraliDocumento->DatiRitenuta(00411)', 'descrizione': 'tag mancante in presenza di Ritenuta SI in almeno un dettaglio Linea' });
                                    }
                                } else {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DettaglioLinee->Ritenuta', 'descrizione': 'Formato stringa non valido' });
                                }
                            }

                        }

                    }

                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DatiRiepilogo']) {
                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo', 'descrizione': 'tag mancante' });
                    } else {
                        if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DatiRiepilogo'])) {
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DatiRiepilogo'].forEach(function(riepilogo) {
                                /* AliquotaIVA */
                                if (!riepilogo['AliquotaIVA']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->AliquotaIVA', 'descrizione': 'tag mancante' });
                                } else {
                                    /* Aliquota IVA */
                                    if (riepilogo['AliquotaIVA'] && riepilogo['AliquotaIVA'].length < 4 ||
                                        riepilogo['AliquotaIVA'] && riepilogo['AliquotaIVA'].length > 6 ||
                                        !isNumeric(riepilogo['AliquotaIVA'])) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->AliquotaIVA(00424)', 'descrizione': 'Valore stringa non valido' });
                                    } else {
                                        if (riepilogo['AliquotaIVA'] == 0) {
                                            /* Natura */
                                            if (!riepilogo['Natura']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Natura(00429)', 'descrizione': 'tag mancante a fronte di AliquotaIVA pari a 0' });
                                            } else {
                                                var stringArray = ['N1', 'N2.1','N2.2', 'N3.1','N3.2','N3.3','N3.4','N3.5','N3.6', 'N4', 'N5', 'N6.1','N6.2','N6.3','N6.4','N6.5','N6.6','N6.7', 'N6.8','N6.9','N7'];
                                                if (riepilogo['Natura'].length < 2 || riepilogo['Natura'].length > 4 ||
                                                       stringArray.indexOf(riepilogo['Natura']) == -1) {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Natura', 'descrizione': 'Formato Stringa non valido' });

                                                }
                                                if ((!riepilogo['RiferimentoNormativo'] || riepilogo['RiferimentoNormativo'] === '') && riepilogo['Natura'] != 'N1')  {
                                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->RiferimentoNormativo', 'descrizione': 'tag mancante a fronte di Natura' });
                                                }
                                            }
                                        } else {
                                            if (riepilogo['Natura']) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Natura(00430)', 'descrizione': 'Natura presente a fronte di AliquotaIVA diversa da 0' });
                                            }
                                        }
                                    }
                                }

                                /* EsigibilitaIVA */
                                if (riepilogo['EsigibilitaIVA']) {
                                    var stringArrayEs = ['D', 'I', 'S'];
                                    if (riepilogo['EsigibilitaIVA'].length != 1 ||
                                        stringArrayEs.indexOf(riepilogo['EsigibilitaIVA']) == -1) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->EsigibilitaIVA', 'descrizione': 'Formato Stringa non valido' });

                                    }
                                }

                                /* Spese Accessorie */
                                if (riepilogo['SpeseAccessorie']) {
                                    if (!isNumeric(riepilogo['SpeseAccessorie']) || riepilogo['SpeseAccessorie'].length < 4 ||
                                        riepilogo['SpeseAccessorie'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->SpeseAccessorie', 'descrizione': 'Formato Stringa non valido' });
                                    }
                                }

                                /* Arrotondamento */
                                if (riepilogo['Arrotondamento']) {
                                    if (!isNumeric(riepilogo['Arrotondamento']) || riepilogo['Arrotondamento'].length < 4 ||
                                        riepilogo['Arrotondamento'].length > 21) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Arrotondamento', 'descrizione': 'Formato Stringa non valido' });
                                    }
                                }

                                /* ImponibileImporto */

                                if (!riepilogo['ImponibileImporto']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->ImponibileImporto', 'descrizione': 'tag mancante' });
                                } else {
                                    if (!isNumeric(riepilogo['ImponibileImporto']) || riepilogo['ImponibileImporto'].length < 4 ||
                                        riepilogo['ImponibileImporto'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->ImponibileImporto', 'descrizione': 'Formato Stringa non valido' });
                                    } else {
                                        if((parseFloat(riepilogo['Imposta']) != 2 && (riepilogo['Natura'] != 'N1' && riepilogo['Natura'] != 'N2.2') )){
                                            if ((parseFloat(riepilogo['ImponibileImporto']) - parseFloat((calcolo_corretto_imponibile_iva(riepilogo['AliquotaIVA'] / 100, riepilogo['Natura'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'])))) > 1 ||
                                                (parseFloat(riepilogo['ImponibileImporto']) - parseFloat((calcolo_corretto_imponibile_iva(riepilogo['AliquotaIVA'] / 100, riepilogo['Natura'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'])))) < -1) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->ImponibileImporto(00422)', 'descrizione': 'Calcolo dell\'imponibile non corretto' });
                                            }
                                        }
                                        
                                    }
                                }

                                /* Imposta */
                                if (!riepilogo['Imposta']) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Imposta', 'descrizione': 'tag mancante' });
                                } else {
                                    if (!isNumeric(riepilogo['Imposta']) || riepilogo['Imposta'].length < 4 ||
                                        riepilogo['Imposta'].length > 15) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Imposta', 'descrizione': 'Formato Stringa non valido' });
                                    } else {
                                        if (parseFloat(riepilogo['Imposta']) != parseFloat((((parseFloat(riepilogo['AliquotaIVA'] / 100)) * parseFloat(riepilogo['ImponibileImporto'])))) &&
                                            Math.abs(parseFloat(riepilogo['Imposta']) - parseFloat((((parseFloat(riepilogo['AliquotaIVA'] / 100)) * parseFloat(riepilogo['ImponibileImporto']))))) > 0.01 && 
                                            (parseFloat(riepilogo['Imposta']) != 2 && riepilogo['Natura'] != 'N1' )
                                        ) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Imposta(00421)', 'descrizione': 'Calcolo dell\'imposta non corretto' });
                                        }
                                    }
                                }

                                /* Esigibilità IVA ??? */



                            });
                        } else {
                            var riepilogo = data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DatiRiepilogo'];

                            /* AliquotaIVA */
                            if (!riepilogo['AliquotaIVA']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->AliquotaIVA', 'descrizione': 'tag mancante' });
                            } else {
                                /* Aliquota IVA */
                                if (riepilogo['AliquotaIVA'] && riepilogo['AliquotaIVA'].length < 4 ||
                                    riepilogo['AliquotaIVA'] && riepilogo['AliquotaIVA'].length > 6 ||
                                    !isNumeric(riepilogo['AliquotaIVA'])) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->AliquotaIVA(00424)', 'descrizione': 'Valore stringa non valido' });
                                } else {
                                    if (riepilogo['AliquotaIVA'] == 0) {
                                        /* Natura */
                                        if (!riepilogo['Natura']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Natura(00429)', 'descrizione': 'tag mancante a fronte di AliquotaIVA pari a 0' });
                                        } else {
                                           var stringArray = ['N1', 'N2.1','N2.2', 'N3.1','N3.2','N3.3','N3.4','N3.5','N3.6', 'N4', 'N5', 'N6.1','N6.2','N6.3','N6.4','N6.5','N6.6','N6.7', 'N6.8','N6.9','N7'];
                                                if (riepilogo['Natura'].length < 2 || riepilogo['Natura'].length > 4 ||
                                                    stringArray.indexOf(riepilogo['Natura']) == -1) {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Natura', 'descrizione': 'Formato Stringa non valido' });

                                            }

                                            if ((!riepilogo['RiferimentoNormativo'] || riepilogo['RiferimentoNormativo'] === '') && riepilogo['Natura'] != 'N1')  {
                                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->RiferimentoNormativo', 'descrizione': 'tag mancante a fronte di Natura' });
                                            }
                                        }
                                    } else {
                                        if (riepilogo['Natura']) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Natura(00430)', 'descrizione': 'Natura presente a fronte di AliquotaIVA diversa da 0' });
                                        }
                                    }
                                }
                            }

                            /* EsigibilitaIVA */
                            if (riepilogo['EsigibilitaIVA']) {
                                var stringArrayEs = ['D', 'I', 'S'];
                                if (riepilogo['EsigibilitaIVA'].length != 1 ||
                                    stringArrayEs.indexOf(riepilogo['EsigibilitaIVA']) == -1) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->EsigibilitaIVA', 'descrizione': 'Formato Stringa non valido' });

                                }
                            }

                            /* Spese Accessorie */
                            if (riepilogo['SpeseAccessorie']) {
                                if (!isNumeric(riepilogo['SpeseAccessorie']) || riepilogo['SpeseAccessorie'].length < 4 ||
                                    riepilogo['SpeseAccessorie'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->SpeseAccessorie', 'descrizione': 'Formato Stringa non valido' });
                                }
                            }

                            /* Arrotondamento */
                            if (riepilogo['Arrotondamento']) {
                                if (!isNumeric(riepilogo['Arrotondamento']) || riepilogo['Arrotondamento'].length < 4 ||
                                    riepilogo['Arrotondamento'].length > 21) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Arrotondamento', 'descrizione': 'Formato Stringa non valido' });
                                }
                            }

                            /* ImponibileImporto */
                            if (!riepilogo['ImponibileImporto']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->ImponibileImporto', 'descrizione': 'tag mancante' });
                            } else {
                                if (!isNumeric(riepilogo['ImponibileImporto']) || riepilogo['ImponibileImporto'].length < 4 ||
                                    riepilogo['ImponibileImporto'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->ImponibileImporto', 'descrizione': 'Formato Stringa non valido' });
                                } else {
                                    if((parseFloat(riepilogo['Imposta']) != 2 && (riepilogo['Natura'] != 'N1' && riepilogo['Natura'] != 'N2.2') )){
                                        if ((parseFloat(riepilogo['ImponibileImporto']) - parseFloat((calcolo_corretto_imponibile_iva(riepilogo['AliquotaIVA'] / 100, riepilogo['Natura'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'])))) > 1 ||
                                            (parseFloat(riepilogo['ImponibileImporto']) - parseFloat((calcolo_corretto_imponibile_iva(riepilogo['AliquotaIVA'] / 100, riepilogo['Natura'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiBeniServizi']['DettaglioLinee'], data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiGenerali']['DatiGeneraliDocumento']['DatiCassaPrevidenziale'])))) < -1) {
                                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->ImponibileImporto(00422)', 'descrizione': 'Calcolo dell\'imponibile non corretto' });
                                        }
                                    }
                                   
                                }
                            }

                            /* Imposta */
                            if (!riepilogo['Imposta']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Imposta', 'descrizione': 'tag mancante' });
                            } else {
                                if (!isNumeric(riepilogo['Imposta']) || riepilogo['Imposta'].length < 4 ||
                                    riepilogo['Imposta'].length > 15) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Imposta', 'descrizione': 'Formato Stringa non valido' });
                                } else {
                                    if (parseFloat(riepilogo['Imposta']) != parseFloat((((parseFloat(riepilogo['AliquotaIVA'] / 100)) * parseFloat(riepilogo['ImponibileImporto'])))) &&
                                        Math.abs(parseFloat(riepilogo['Imposta']) - parseFloat((((parseFloat(riepilogo['AliquotaIVA'] / 100)) * parseFloat(riepilogo['ImponibileImporto']))))) > 0.01 &&
                                        (parseFloat(riepilogo['Imposta']) != 2 && riepilogo['Natura'] != 'N1' )) {
                                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiBeniServizi->DatiRiepilogo->Imposta(00421)', 'descrizione': 'Calcolo dell\'imposta non corretto' });
                                    }
                                }
                            }

                            /* Esigibilità IVA ??? */
                        }
                    }
                }

                /* Dati Veicoli*/
                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']) {
                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']['Data']) {

                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiVeicoli->Data', 'descrizione': 'tag mancante' });
                    } else {
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']['Data'].length != 10 ||
                            !moment(data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']['Data'], 'YYYY-MM-DD').isValid()) {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiVeicoli->Data', 'descrizione': 'Formato Stringa non valido' });
                        }
                    }

                    if (!data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']['TotalePercorso']) {

                        errors.push({ 'posizione': 'FatturaElettronicaBody->DatiVeicoli->TotalePercorso', 'descrizione': 'tag mancante' });
                    } else {
                        if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']['TotalePercorso'].length < 1 ||
                            data['v1:FatturaElettronica']['FatturaElettronicaBody']['DatiVeicoli']['TotalePercorso'].length > 15)  {
                            errors.push({ 'posizione': 'FatturaElettronicaBody->DatiVeicoli->TotalePercorso', 'descrizione': 'Formato Stringa non valido' });
                        }
                    }
                }

                /* Allegati */
                if (data['v1:FatturaElettronica']['FatturaElettronicaBody']['Allegati']) {
                    if (util.isArray(data['v1:FatturaElettronica']['FatturaElettronicaBody']['Allegati'])) {
                        data['v1:FatturaElettronica']['FatturaElettronicaBody']['Allegati'].forEach(function(allegato) {
                            if (!allegato['NomeAttachment']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->NomeAttachment', 'descrizione': 'tag mancante' });
                            } else {
                                if (allegato['NomeAttachment'].length < 1 || allegato['NomeAttachment'].length > 60) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->NomeAttachment', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (allegato['AlgoritmoCompressione']) {
                                if (allegato['AlgoritmoCompressione'].length < 1 || allegato['AlgoritmoCompressione'].length > 10) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->AlgoritmoCompressione', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (allegato['FormatoAttachment']) {
                                if (allegato['FormatoAttachment'].length < 1 || allegato['FormatoAttachment'].length > 10) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->FormatoAttachment', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (allegato['DescrizioneAttachment']) {
                                if (allegato['DescrizioneAttachment'].length < 1 || allegato['DescrizioneAttachment'].length > 100) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->DescrizioneAttachment', 'descrizione': 'Formato non valido' });
                                }
                            }

                            if (!allegato['Attachment']) {
                                errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->Attachment', 'descrizione': 'tag mancante' });
                            } else {
                                if (_base64ToArrayBuffer(allegato['Attachment']).length > 5000) {
                                    errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati->Attachment', 'descrizione': 'Formato non valido' });
                                }
                            }

                        });
                    }
                    errors.push({ 'posizione': 'FatturaElettronicaBody->Allegati', 'descrizione': 'tag mancante' });
                }
            }
            callback(null, errors);
        }
    }, function(err, results) {

        if (err) {
            return {};
        } else {
            var response = { 'errori': [] };
            response.errori.push(results.header_);
            response.errori.push(results.body_);
            callback_(null, response);
        }
    });
};