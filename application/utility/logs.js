var async = require('async');
var Sequelize = require('sequelize');
var utility = require('../utility/utility.js');

exports.save_log_model = function(_tenant, org, model, operation, user_id, attributes, new_id, description) {

    sequelize.query('SELECT insert_logs_model(' +
            user_id + ',' +
            new_id + ',\'' +
            attributes.replace(/'/g, "''''") + '\',\'' +
            operation + '\',' +
            _tenant + ',\'' +
            org + '\',\'' +
            model + '\',\'' +
            description + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            return 1;
        }).catch(function(err) {
            console.log(err);
            return 0;
        });
};

exports.save_log_relation = function(model_a, model_b, id_a, id_b, operation, user_id, org, _tenant) {

    sequelize.query('SELECT insert_logs_relation(' +
            user_id + ',\'' +
            model_a + '\',\'' +
            model_b + '\',' +
            id_a + ',' +
            id_b + ',\'' +
            operation + '\',\'' +
            org + '\',' +
            _tenant + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            return 1;
        }).catch(function(err) {
            console.log(err);
            return 0;
        });
};