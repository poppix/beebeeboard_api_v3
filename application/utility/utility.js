var logs = require('../utility/logs.js');
var utility = require('../utility/utility.js');
var request = require('request');
var config = require('../../config');
/*
 * Check the model id if is a correct number
 *
 */
exports.check_id = function(param) {
    if (isNaN(parseInt(param))) {
        return true;
    }

    return false;
};


exports.removeEmpty = function(obj) { 
    Object.keys(obj).forEach(function(key) { 
        if (obj[key] && typeof obj[key] === 'object') 
            utility.removeEmpty(obj[key]);
        else if (obj[key] == null) 
            delete obj[key] 
    }); 
};

exports.check_subscription_expire = function(req, res, next) {
    var Sequelize = require('sequelize');
    sequelize.query('SELECT check_subscription_expire(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(old) {
            if (old[0].check_subscription_expire == true) {
                next();
            } else {
                return res.status(403).send(utility.error403('Account bloccato. Contatta l\'amministratore', 'Account bloccato. Contatta l\'amministratore', '401'));
            }
        });
};



exports.save_socket = function(organization, model, operation, user_id, model_id, model_json) {
    var Sequelize = require('sequelize');
    var json_ = {};

    json_ = {
        'model_id': model_id,
        'operation': operation,
        'user_id': user_id ? user_id : null,
        'model': model,
        'model_json': model_json
    };


    sequelize_socket.query('SELECT save_socket_row(\'' +
            organization +'\',\''+
            JSON.stringify(json_).replace(/'/g, "''")+ '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(old) {
            return 0;
        }).catch(function(err) {
            console.log(err);
            return -1;
        });
};

exports.error422 = function(param, title, detail, status) {
    return {
        'errors': [{
            'status': status,
            'source': {
                'pointer': '/data/attributes/' + param
            },
            'title': title,
            'detail': detail
        }]
    };
};

exports.error403 = function(title, detail, status) {
    return {
        'status': status,
        'errors': [{
            'status': status,
            'title': title,
            'detail': detail
        }]
    };
};

exports.error404 = function(param, title, detail, status) {
    return {
        'errors': [{
            'status': status,
            'source': {
                'pointer': '/data/attributes/' + param
            },
            'title': title,
            'detail': detail
        }]
    };
};

exports.check_find_datas = function(results, model, datas) {

    if (results && results.relationships) {
        datas.forEach(function(dat) {
            if (results.relationships[dat] && (!results.relationships[dat].data || results.relationships[dat].data.length == 0)) {
                results.relationships[dat].data = [];
            }
        });
    }

    return results;

    /*
    datas.forEach(function(dat) {
        if (results.relationships) {
            switch (dat) {
                case 'addresses':
                    if (results.relationships.addresses.data == null || results.relationships.addresses.data.length == 0) {
                        results.relationships.addresses.data = [];
                    }
                    break;

                case 'contacts':
                    if (results.relationships.contacts.data == null || results.relationships.contacts.data.length == 0) {
                        results.relationships.contacts.data = [];
                    }
                    break;

                case 'custom_fields':
                    if (results.relationships.custom_fields.data == null || results.relationships.custom_fields.data.length == 0) {
                        results.relationships.custom_fields.data = [];
                    }
                    break;

                case 'expenses':
                    if (results.relationships && results.relationships.expenses && (results.relationships.expenses.data == null || results.relationships.expenses.data.length == 0)) {
                        results.relationships.expenses.data = [];
                    }
                    break;

                case 'expensearticles':
                    if (results.relationships.expensearticles.data == null || results.relationships.expensearticles.data.length == 0) {
                        results.relationships.expensearticles.data = [];
                    }
                    break;

                case 'expire_dates':
                    if (results.relationships.expire_dates.data == null || results.relationships.expire_dates.data.length == 0) {
                        results.relationships.expire_dates.data = [];
                    }
                    break;

                case 'files':
                    if (results.relationships && results.relationships.files && results.relationships.files.data == null || results.relationships.files.data.length == 0) {
                        results.relationships.files.data = [];
                    }
                    break;

                case 'invoice_styles':
                    if (results.relationships && results.relationships.invoice_styles && results.relationships.invoice_styles.data == null || results.relationships.invoice_styles.data.length == 0) {
                        results.relationships.invoice_styles.data = [];
                    }
                    break;

                case 'invoices':
                    if (results.relationships && results.relationships.invoices && (results.relationships.invoices.data == null || results.relationships.invoices.data.length == 0)) {
                        results.relationships.invoices.data = [];
                    }
                    break;

                case 'invoiceproducts':
                    if (results.relationships.invoiceproducts.data == null || results.relationships.invoiceproducts.data.length == 0) {
                        results.relationships.invoiceproducts.data = [];
                    }
                    break;

                case 'invoicegroups':
                    if (results.relationships.invoicegroups.data == null || results.relationships.invoicegroups.data.length == 0) {
                        results.relationships.invoicegroups.data = [];
                    }
                    break;

                case 'invoiceproduct_taxes':
                    if (results.relationships.invoiceproduct_taxes.data == null || results.relationships.invoiceproduct_taxes.data.length == 0) {
                        results.relationships.invoiceproduct_taxes.data = [];
                    }
                    break;

                case 'invoicetaxes':
                    if (results.relationships.invoicetaxes.data == null || results.relationships.invoicetaxes.data.length == 0) {
                        results.relationships.invoicetaxes.data = [];
                    }
                    break;

                case 'organization_term_options':
                    if (results.relationships.gdpr.organization_term_options.data == null || results.relationships.gdpr.organization_term_options.data.length == 0) {
                        results.relationships.gdpr.organization_term_options.data = [];
                    }
                    break;

                case 'payments':
                    if (results.relationships.payments.data == null || results.relationships.payments.data.length == 0) {
                        results.relationships.payments.data = [];
                    }
                    break;

                case 'products':
                    if (results.relationships.products.data == null || results.relationships.products.data.length == 0) {
                        results.relationships.products.data = [];
                    }
                    break;

                case 'projects':
                    if (results.relationships.projects.data == null || results.relationships.projects.data.length == 0) {
                        results.relationships.projects.data = [];
                    }
                    break;

                case 'reminder_settings':
                    if (results.relationships.reminder_settings.data == null || results.relationships.reminder_settings.data.length == 0) {
                        results.relationships.reminder_settings.data = [];
                    }
                    break;

                case 'reminders':
                    if (results.relationships.reminders.data == null || results.relationships.reminders.data.length == 0) {
                        results.relationships.reminders.data = [];
                    }
                    break;

                case 'tags':
                    if (results.relationships.tags.data == null || results.relationships.tags.data.length == 0) {
                        results.relationships.tags.data = [];
                    }
                    break;

                case 'todolists':
                    if (results.relationships.todolists.data == null || results.relationships.todolists.data.length == 0) {
                        results.relationships.todolists.data = [];
                    }
                    break;


                case 'user_calendars':
                    if (results.relationships.user_calendars.data == null || results.relationships.user_calendars.data.length == 0) {
                        results.relationships.user_calendars.data = [];
                    }
                    break;

                case 'user_taxes':
                    if (results.relationships.user_taxes.data == null || results.relationships.user_taxes.data.length == 0) {
                        results.relationships.user_taxes.data = [];
                    }
                    break;
            }
        }

    });*/
};

exports.check_find_included = function(results, model, included) {
    var inc = [];

    if (results) {
        included.forEach(function(includ) {
            if (results[includ]) {
                inc = inc.concat(results[includ]);
            }
        });
    }

    return inc;

    /*included.forEach(function(includ) {
        switch (includ) {
            case 'addresses':
                if (results.addresses) {
                    inc = inc.concat(results.addresses);
                }
                break;

            case 'applications':
                if (results.applications) {
                    inc = inc.concat(results.applications);
                }
                break;

            case 'categories':
                if (results.categories) {
                    inc = inc.concat(results.categories);
                }
                break;

            case 'contact_states':
                if (results.contact_states) {
                    inc = inc.concat(results.contact_states);
                }
                break;

            case 'contacts':
                if (results.contacts) {
                    inc = inc.concat(results.contacts);
                }
                break;

            case 'custom_fields':
                if (results.custom_fields) {
                    inc = inc.concat(results.custom_fields);
                }
                break;

            case 'custom_field_definitions':
                if (results.custom_field_definitions) {
                    inc = inc.concat(results.custom_field_definitions);
                }
                break;

            case 'custom_reminders':
                if (results.custom_reminders) {
                    inc = inc.concat(results.custom_reminders);
                }
                break;

            case 'event_states':
                if (results.event_states) {
                    inc = inc.concat(results.event_states);
                }
                break;

            case 'events':
                if (results.events) {
                    inc = inc.concat(results.events);
                }
                break;

            case 'expenses':
                if (results.expenses) {
                    inc = inc.concat(results.expenses);
                }
                break;

            case 'expensearticles':
                if (results.expensearticles) {
                    inc = inc.concat(results.expensearticles);
                }
                break;

            case 'expire_dates':
                if (results.expire_dates) {
                    inc = inc.concat(results.expire_dates);
                }
                break;

            case 'files':
                if (results.files) {
                    inc = inc.concat(results.files);
                }
                break;

            case 'invoice_styles':
                if (results.invoice_styles) {
                    inc = inc.concat(results.invoice_styles);
                }
                break;

            case 'invoices':
                if (results.invoices) {
                    inc = inc.concat(results.invoices);
                }
                break;

            case 'invoiceproducts':
                if (results.invoiceproducts) {
                    inc = inc.concat(results.invoiceproducts);
                }
                break;

            case 'invoicegroups':
                if (results.invoicegroups) {
                    inc = inc.concat(results.invoicegroups);
                }
                break;

            case 'invoiceproduct_taxes':
                if (results.invoiceproduct_taxes) {
                    inc = inc.concat(results.invoiceproduct_taxes);
                }
                break;

            case 'invoicetaxes':
                if (results.invoicetaxes) {
                    inc = inc.concat(results.invoicetaxes);
                }
                break;

            case 'organizations':

                if (results.organizations) {
                    inc = inc.concat(results.organizations);
                }
                break;

            case 'payments':
                if (results.payments) {
                    inc = inc.concat(results.payments);
                }
                break;

            case 'product_states':
                if (results.product_states) {
                    inc = inc.concat(results.product_states);
                }
                break;

            case 'products':
                if (results.products) {
                    inc = inc.concat(results.products);
                }
                break;

            case 'project_states':
                if (results.project_states) {
                    inc = inc.concat(results.project_states);
                }
                break;

            case 'projects':
                if (results.projects) {
                    inc = inc.concat(results.projects);
                }
                break;

            case 'organization_custom_fields':
                if (results.organization_custom_fields) {
                    inc = inc.concat(results.organization_custom_fields);
                }
                break;

            case 'payment_methods':
                if (results.payment_methods) {
                    inc = inc.concat(results.payment_methods);
                }
                break;

            case 'plans':
                if (results.plans) {
                    inc = inc.concat(results.plans);
                }
                break;

            case 'related_contacts':
                if (results.related_contacts) {
                    inc = inc.concat(results.related_contacts);
                }
                break;

            case 'related_events':
                if (results.related_events) {
                    inc = inc.concat(results.related_events);
                }
                break;

            case 'related_expenses':
                if (results.related_expenses) {
                    inc = inc.concat(results.related_expenses);
                }
                break;

            case 'related_invoices':
                if (results.related_invoices) {
                    inc = inc.concat(results.related_invoices);
                }
                break;

            case 'related_products':
                if (results.related_products) {
                    inc = inc.concat(results.related_products);
                }
                break;

            case 'related_projects':
                if (results.related_projects) {
                    inc = inc.concat(results.related_projects);
                }
                break;

            case 'related_workhours':
                if (results.related_workhours) {
                    inc = inc.concat(results.related_workhours);
                }
                break;

            case 'reminder_settings':
                if (results.reminder_settings) {
                    inc = inc.concat(results.reminder_settings);
                }
                break;

            case 'reminders':
                if (results.reminders) {
                    inc = inc.concat(results.reminders);
                }
                break;

            case 'roles':
                if (results.roles) {
                    inc = inc.concat(results.roles);
                }
                break;

            case 'sub_categories':
                if (results.sub_categories) {
                    inc = inc.concat(results.sub_categories);
                }
                break;

            case 'subscriptions':
                if (results.subscriptions) {
                    inc = inc.concat(results.subscriptions);
                }
                break;

            case 'tags':
                if (results.tags && results.tags[0].id !== null) {
                    inc = inc.concat(results.tags);
                }
                break;

            case 'todolists':
                if (results.todolists && results.todolists[0].id !== null) {
                    inc = inc.concat(results.todolists);
                }
                break;

            case 'todos':
                if (results.todos && results.todos[0].id !== null) {
                    inc = inc.concat(results.todos);
                }
                break;

            case 'user_calendars':
                if (results.user_calendars) {
                    inc = inc.concat(results.user_calendars);
                }
                break;

            case 'user_taxes':
                if (results.user_taxes) {
                    inc = inc.concat(results.user_taxes);
                }
                break;

            case 'workhour_states':
                if (results.workhour_states) {
                    inc = inc.concat(results.workhour_states);
                }
                break;

            case 'workhours':
                if (results.workhours) {
                    inc = inc.concat(results.workhours);
                }
                break;

        }

    });
    return inc;*/
};

exports.sanitizeRelations = function(obj, callback_) {
    var async = require('async'),
        Sequelize = require('sequelize'),
        new_id = obj.new_id,
        relationships = obj.body,
        model = obj.model,
        relations = obj.array,
        user_id = obj.user_id;


    async.parallel({
        address: function(callback) {
            if (relations.indexOf('address') != -1) {
                var cnt = 0;
                if (relationships && relationships.addresses && relationships.addresses !== null && relationships.addresses !== '' && relationships.addresses.data.length > 0) {
                    relationships.addresses.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_addresses(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'address',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );

                                if (cnt++ == relationships.addresses.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.addresses.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        application: function(callback) {
            if (relations.indexOf('application') != -1) {
                cnt = 0;
                if (relationships.applications && relationships.applications !== null && relationships.applications !== '' && relationships.applications.data.length > 0) {
                    relationships.applications.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_application(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(datas) {
                            logs.save_log_relation(
                                model,
                                'application',
                                new_id,
                                vr.id,
                                'insert',
                                user_id,
                                obj.organization,
                                obj._tenant
                            );

                            if (cnt++ == relationships.applications.data.length - 1) {
                                callback(null, cnt);
                            }

                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                    });
                    if (relationships.applications.data.length == 0) {
                        callback(null, null);

                    }
                } else {
                    callback(null, null);
                }

            } else {
                callback(null, 0);
            }
        },
        category: function(callback) {
            if (relations.indexOf('category') != -1) {
                var cnt = 0;
                if (relationships && relationships.category && relationships.category !== null && relationships.category !== '' && relationships.category.data && relationships.category.data.id) {

                    sequelize.query('SELECT insert_' + model + '_categories(' + new_id + ',' + relationships.category.data.id + ',\'' + obj.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                        .then(function(datas) {
                            logs.save_log_relation(
                                model,
                                'categorie',
                                new_id,
                                relationships.category.data.id,
                                'insert',
                                user_id,
                                obj.organization,
                                obj._tenant
                            );
                            callback(null, 1);


                        }).catch(function(err) {
                            console.log(err);
                            callback(err, 0);
                        });

                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        contact: function(callback) {
            if (relations.indexOf('contact') != -1) {
                var cnt = 0;
                if (relationships && relationships.contacts && relationships.contacts !== null && relationships.contacts !== '' && relationships.contacts.data.length > 0) {
                    relationships.contacts.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_contacts(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'contact',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.contacts.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.contacts.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        cumulable_invoiceproduct_taxe: function(callback) {
            if (relations.indexOf('cumulable_invoiceproduct_taxe') != -1) {
                var cnt = 0;

                if (relationships && relationships.cumulable_invoiceproduct_taxes && relationships.cumulable_invoiceproduct_taxes !== null && relationships.cumulable_invoiceproduct_taxes !== '' && relationships.cumulable_invoiceproduct_taxes.data.length > 0) {
                    relationships.cumulable_invoiceproduct_taxes.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_cumulable_invoiceproduct_taxe(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'cumulable_invoiceproduct_taxe',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.cumulable_invoiceproduct_taxes.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                callback(err, null);
                            });
                    });
                    if (relationships.cumulable_invoiceproduct_taxes.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {

                callback(null, 0);
            }
        },
        cumulable_taxe: function(callback) {
            if (relations.indexOf('cumulable_taxe') != -1) {
                var cnt = 0;
                if (relationships && relationships.cumulable_taxes && relationships.cumulable_taxes !== null && relationships.cumulable_taxes !== '' && relationships.cumulable_taxes.data.length > 0) {
                    relationships.cumulable_taxes.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_cumulable_taxe(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'cumulable_taxe',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.cumulable_taxes.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.cumulable_taxes.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        custom_field: function(callback) {
            if (relations.indexOf('custom_field') != -1) {
                var cnt = 0;
                if (relationships && relationships.custom_fields && relationships.custom_fields !== null && relationships.custom_fields !== '' && relationships.custom_fields.data.length > 0) {
                    relationships.custom_fields.data.forEach(function(cf) {
                        sequelize.query('SELECT insert_' + model + '_custom_fields(' + new_id + ',' + cf.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'custom_field',
                                    new_id,
                                    cf.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.custom_fields.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.custom_fields.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        custom_field_definition: function(callback) {
            if (relations.indexOf('custom_field_definition') != -1) {
                var cnt = 0;
                if (relationships && relationships.custom_field_definitions && relationships.custom_field_definitions !== null && relationships.custom_field_definitions !== '' && relationships.custom_field_definitions.data.length > 0) {
                    relationships.custom_field_definitions.data.forEach(function(vr) {

                        sequelize.query('SELECT update_custom_field_definitions(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'custom_field_definition',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.custom_field_definitions.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                callback(err, null);
                            });
                    });
                    if (relationships.custom_field_definitions.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        event: function(callback) {
            if (relations.indexOf('event') != -1) {
                var cnt = 0;
                if (relationships && relationships.events && relationships.events !== null && relationships.events !== '' && relationships.events.data && relationships.events.data.length > 0) {
                    relationships.events.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_event(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'event',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.events.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.events.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        expense: function(callback) {
            if (relations.indexOf('expense') != -1) {
                var cnt = 0;
                if (relationships && relationships.expenses && relationships.expenses !== null && relationships.expenses !== '' && relationships.expenses.data.length > 0) {
                    relationships.expenses.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_expenses(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'expense',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.expenses.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.expenses.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        expensearticle: function(callback) {
            if (relations.indexOf('expensearticle') != -1) {
                var cnt = 0;
                if (relationships.expensearticles && relationships.expensearticles !== null && relationships.expensearticles !== '' && relationships.expensearticles.data.length > 0) {
                    relationships.expensearticles.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + '_expensearticle(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'expensearticle',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.expensearticles.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.expensearticles.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        expire_dates: function(callback) {
            if (relations.indexOf('expire_date') != -1) {
                var cnt = 0;
                if (relationships.expire_dates && relationships.expire_dates !== null && relationships.expire_dates !== '' && relationships.expire_dates.data.length > 0) {
                    relationships.expire_dates.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + '_expire_date(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'expire_date',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.expire_dates.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.expire_dates.data.length == 0) {
                        callback(null, 0);

                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        file: function(callback) {
            if (relations.indexOf('file') != -1) {
                var cnt = 0;
                if (relationships && relationships.files && relationships.files !== null && relationships.files !== '' && relationships.files.data !== null && relationships.files.data.length > 0) {
                    relationships.files.data.forEach(function(file) {
                        sequelize.query('SELECT insert_' + model + '_file(' + new_id + ',' + file.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'file',
                                    new_id,
                                    file.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.files.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.files.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        invoice: function(callback) {
            if (relations.indexOf('invoice') != -1) {
                var cnt = 0;
                if (relationships && relationships.invoices && relationships.invoices !== null && relationships.invoices !== '' && relationships.invoices.data.length > 0) {
                    relationships.invoices.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_invoices(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'invoice',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.invoices.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.invoices.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        invoiceproduct: function(callback) {
            if (relations.indexOf('invoiceproduct') != -1) {
                var cnt = 0;
                if (relationships && relationships.invoiceproducts && relationships.invoiceproducts !== null && relationships.invoiceproducts !== '' && relationships.invoiceproducts.data.length > 0) {
                    relationships.invoiceproducts.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + '_invoiceproduct(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'invoiceproduct',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );

                                if (cnt++ == relationships.invoiceproducts.data.length - 1) {
                                    sequelize.query('SELECT delete_pending_invoiceproduct(\'' + obj.organization + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        })
                                        .then(function(datas) {
                                            callback(null, cnt);
                                        });
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });

                    });
                    if (relationships.invoiceproducts.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        invoicegroup: function(callback) {
            if (relations.indexOf('invoicegroup') != -1) {
                var cnt = 0;
                if (relationships && relationships.invoicegroups && relationships.invoicegroups !== null && relationships.invoicegroups !== '' && relationships.invoicegroups.data.length > 0) {
                    relationships.invoicegroups.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + 'invoicegroup(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'invoicegroup',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );

                                if (cnt++ == relationships.invoiceproducts.data.length - 1) {
                                    sequelize.query('SELECT delete_pending_invoicegroup(\'' + obj.organization + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        })
                                        .then(function(datas) {
                                            callback(null, cnt);
                                        });
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });

                    });
                    if (relationships.invoicegroups.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        invoiceproduct_taxe: function(callback) {
            if (relations.indexOf('invoiceproduct_taxe') != -1) {
                var cnt = 0;
                if (relationships && relationships.invoiceproduct_taxes && relationships.invoiceproduct_taxes !== null && relationships.invoiceproduct_taxes !== '' && relationships.invoiceproduct_taxes.data.length > 0) {
                    relationships.invoiceproduct_taxes.data.forEach(function(prod) {
                        sequelize.query('SELECT update_invoiceproduct_taxes(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'invoiceproduct_taxe',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.invoiceproduct_taxes.data.length - 1) {
                                    sequelize.query('SELECT delete_pending_invoiceproduct_taxe(' + new_id + ',\'' + obj.organization + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        })
                                        .then(function(datas) {
                                            callback(null, cnt);
                                        });
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.invoiceproduct_taxes.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        invoicetaxe: function(callback) {
            if (relations.indexOf('invoicetaxe') != -1) {
                var cnt = 0;
                if (relationships && relationships.invoicetaxes && relationships.invoicetaxes !== null && relationships.invoicetaxes !== '' && relationships.invoicetaxes.data.length > 0) {
                    relationships.invoicetaxes.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + '_invoicetaxe(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'invoicetaxe',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.invoicetaxes.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });

                    });
                    if (relationships.invoicetaxes.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        payment: function(callback) {
            if (relations.indexOf('payment') != -1) {
                var cnt = 0;
                if (relationships && relationships.payments && relationships.payments !== null && relationships.payments !== '' && relationships.payments.data.length > 0) {
                    relationships.payments.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + '_payments(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'payment',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.payments.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.payments.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        product: function(callback) {
            if (relations.indexOf('product') != -1) {
                var cnt = 0;
                if (relationships && relationships.products && relationships.products !== null && relationships.products !== '' && relationships.products.data.length > 0) {
                    relationships.products.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_products(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'product',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.products.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.products.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        project: function(callback) {
            if (relations.indexOf('project') != -1) {
                var cnt = 0;
                if (relationships && relationships.projects && relationships.projects !== null && relationships.projects !== '' && relationships.projects.data.length > 0) {
                    relationships.projects.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_' + model + '_projects(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'project',
                                    new_id,
                                    prod.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.projects.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.projects.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_contact: function(callback) {
            if (relations.indexOf('related_contact') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_contacts && relationships.related_contacts !== null && relationships.related_contacts !== '' && relationships.related_contacts.data.length > 0) {
                    relationships.related_contacts.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_contact_contact(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_contact_contact(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_contact',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_contacts.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_contacts.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_event: function(callback) {
            if (relations.indexOf('related_event') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_events && relationships.related_events !== null && relationships.related_events !== '' && relationships.related_events.data.length > 0) {
                    relationships.related_events.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_event_event(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_event_event(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_events',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_events.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_events.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_expense: function(callback) {
            if (relations.indexOf('related_expense') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_expenses && relationships.related_expenses !== null && relationships.related_expenses !== '' && relationships.related_expenses.data.length > 0) {
                    relationships.related_expenses.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_expense_expense(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_expense_expense(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_expenses',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_expenses.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_expenses.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_invoice: function(callback) {
            if (relations.indexOf('related_invoice') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_invoices && relationships.related_invoices !== null && relationships.related_invoices !== '' && relationships.related_invoices.data.length > 0) {
                    relationships.related_invoices.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_invoice_invoice(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_invoice_invoice(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_invoices',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_invoices.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_invoices.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_product: function(callback) {
            if (relations.indexOf('related_product') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_products && relationships.related_products !== null && relationships.related_products !== '' && relationships.related_products.data.length > 0) {
                    relationships.related_products.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_product_product(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_product_product(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_product',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_products.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_products.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_project: function(callback) {
            if (relations.indexOf('related_project') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_projects && relationships.related_projects !== null && relationships.related_projects !== '' && relationships.related_projects.data.length > 0) {
                    relationships.related_projects.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_project_project(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_project_project(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_project',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_projects.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_projects.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        related_workhour: function(callback) {
            if (relations.indexOf('related_workhour') != -1) {
                var cnt = 0;
                if (relationships && relationships.related_workhours && relationships.related_workhours !== null && relationships.related_workhours !== '' && relationships.related_workhours.data.length > 0) {
                    relationships.related_workhours.data.forEach(function(prod) {
                        sequelize.query('SELECT insert_workhour_workhour(' + prod.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                sequelize.query('SELECT insert_workhour_workhour(' + new_id + ',' + prod.id + ',\'' + obj.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                    .then(function(datas) {
                                        logs.save_log_relation(
                                            model,
                                            'related_workhour',
                                            new_id,
                                            prod.id,
                                            'insert',
                                            user_id,
                                            obj.organization,
                                            obj._tenant
                                        );
                                        if (cnt++ == relationships.related_workhours.data.length - 1) {
                                            callback(null, cnt);
                                        }

                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.related_workhours.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        reminder: function(callback) {
            if (relations.indexOf('reminder') != -1) {
                var cnt = 0;
                if (relationships && relationships.reminders && relationships.reminders !== null && relationships.reminders !== '' && relationships.reminders.data.length > 0) {
                    relationships.reminders.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_' + model + '_reminder(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'reminder',
                                    new_id,
                                    vr.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.reminders.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.reminders.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        sub_category: function(callback) {
            if (relations.indexOf('sub_category') != -1) {
                var cnt = 0;
                if (relationships && relationships.sub_category && relationships.sub_category !== null && relationships.sub_category !== '' && relationships.sub_category.data && relationships.sub_category.data.id) {

                    sequelize.query('SELECT insert_' + model + '_sub_categories(' + new_id + ',' + relationships.sub_category.data.id + ',\'' + obj.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                        .then(function(datas) {
                            logs.save_log_relation(
                                model,
                                'sub_categorie',
                                new_id,
                                relationships.sub_category.data.id,
                                'insert',
                                user_id,
                                obj.organization,
                                obj._tenant
                            );
                            callback(null, 1);


                        }).catch(function(err) {
                            console.log(err);
                            callback(err, 0);
                        });

                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        tag: function(callback) {
            if (relations.indexOf('tag') != -1) {
                var cnt = 0;

                if (relationships && relationships.tags && relationships.tags !== null && relationships.tags !== '' && relationships.tags.data.length > 0) {
                    relationships.tags.data.forEach(function(cont) {
                        sequelize.query('SELECT update_tags(' + cont.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'tag',
                                    new_id,
                                    cont.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.tags.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.tags.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        todolist: function(callback) {
            if (relations.indexOf('todolist') != -1) {
                var cnt = 0;
                if (relationships.todolists && relationships.todolists !== null && relationships.todolists !== '' && relationships.todolists.data.length > 0) {
                    relationships.todolists.data.forEach(function(todolist) {
                        sequelize.query('SELECT insert_' + model + '_todolists(' + new_id + ',' + todolist.id + ',\'' + obj.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model,
                                    'todolist',
                                    new_id,
                                    todolist.id,
                                    'insert',
                                    user_id,
                                    obj.organization,
                                    obj._tenant
                                );
                                if (cnt++ == relationships.todolists.data.length - 1) {
                                    callback(null, cnt);
                                }

                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });
                    });
                    if (relationships.todolists.data.length == 0) {
                        callback(null, 0);
                    }
                } else {
                    callback(null, 0);
                }
            } else {
                callback(null, 0);
            }
        },
        user_calendar: function(callback) {
            if (relations.indexOf('user_calendar') != -1) {
                cnt = 0;
                if (relationships.user_calendars && relationships.user_calendars !== null && relationships.user_calendars !== '' && relationships.user_calendars.data.length > 0) {
                    relationships.user_calendars.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_users_calendar(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(datas) {
                            logs.save_log_relation(
                                model,
                                'user_calendar',
                                new_id,
                                vr.id,
                                'insert',
                                user_id,
                                obj.organization,
                                obj._tenant
                            );
                            if (cnt++ == relationships.user_calendars.data.length - 1) {
                                callback(null, cnt);
                            }

                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                    });
                    if (relationships.user_calendars.data.length == 0) {
                        callback(null, null);

                    }
                } else {
                    callback(null, null);
                }
            } else {
                callback(null, 0);
            }
        },
        user_taxe: function(callback) {
            if (relations.indexOf('user_tax') != -1) {
                cnt = 0;
                if (relationships.user_taxes && relationships.user_taxes !== null && relationships.user_taxes !== '' && relationships.user_taxes.data.length > 0) {
                    relationships.user_taxes.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_user_taxe(' + new_id + ',' + vr.id + ',\'' + obj.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(datas) {
                            logs.save_log_relation(
                                model,
                                'user_taxe',
                                new_id,
                                vr.id,
                                'insert',
                                user_id,
                                obj.organization,
                                obj._tenant
                            );
                            if (cnt++ == relationships.user_taxes.data.length - 1) {
                                callback(null, cnt);
                            }

                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                    });
                    if (relationships.user_taxes.data.length == 0) {
                        callback(null, null);

                    }
                } else {
                    callback(null, null);
                }
            } else {
                callback(null, 0);
            }
        },
        workhour: function(callback) {
            if (relations.indexOf('workhour') != -1) {
                cnt = 0;
                if (relationships && relationships.workhours && relationships.workhours !== null && relationships.workhours !== '' && relationships.workhours.data.length > 0) {
                    relationships.workhours.data.forEach(function(vr) {

                        sequelize.query('SELECT insert_workhour_' + model + '(' + vr.id + ',' + new_id + ',\'' + obj.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(datas) {
                            logs.save_log_relation(
                                model,
                                'workhour',
                                new_id,
                                vr.id,
                                'insert',
                                user_id,
                                obj.organization,
                                obj._tenant
                            );
                            if (cnt++ == relationships.workhours.data.length - 1) {
                                callback(null, cnt);
                            }

                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                    });
                    if (relationships.workhours.data.length == 0) {
                        callback(null, null);

                    }
                } else {
                    callback(null, null);
                }
            } else {
                callback(null, 0);
            }
        }
    }, function(err, results) {

        if (err) {
            console.log(err);
        }
        callback_(null, results);
    });
};

exports.formatta_mese = function(first, last) {
    var moment = require('moment');
    return moment(first, 'MM').format('MMMM') + (first !== last ? ('-' + moment(last, 'MM').format('MMMM')) : ''); 
    
    /*
    var periodo = '';
    switch (first) {
        case 1:
            periodo += 'Gennaio';
            break;

        case 2:
            periodo += 'Febbraio';
            break;

        case 3:
            periodo += 'Marzo';
            break;

        case 4:
            periodo += 'Aprile';
            break;

        case 5:
            periodo += 'Maggio';
            break;

        case 6:
            periodo += 'Giugno';
            break;

        case 7:
            periodo += 'Luglio';
            break;

        case 8:
            periodo += 'Agosto';
            break;

        case 9:
            periodo += 'Settembre';
            break;

        case 10:
            periodo += 'Ottobre';
            break;

        case 11:
            periodo += 'Novembre';
            break;

        case 12:
            periodo += 'Dicembre';
            break;
    }
    if (first != last) {
        switch (last) {
            case 1:
                periodo += '-Gennaio';
                break;

            case 2:
                periodo += '-Febbraio';
                break;

            case 3:
                periodo += '-Marzo';
                break;

            case 4:
                periodo += '-Aprile';
                break;

            case 5:
                periodo += '-Maggio';
                break;

            case 6:
                periodo += '-Giugno';
                break;

            case 7:
                periodo += '-Luglio';
                break;

            case 8:
                periodo += '-Agosto';
                break;

            case 9:
                periodo += '-Settembre';
                break;

            case 10:
                periodo += '-Ottobre';
                break;

            case 11:
                periodo += '-Novembre';
                break;

            case 12:
                periodo += '-Dicembre';
                break;
        }
    }

    return periodo;*/
};

exports.check_type_variable = function(variable, type) {
    return ((typeof variable) == type) ? true : false;
};

exports.sanitize_file_relation = function(type, d, file_id, user_id, org, _tenant, res) {
    var Sequelize = require('sequelize');

    switch (type) {

        case 'contact':
        case 'product':
        case 'project':
        case 'invoice':
        case 'expense':
        case 'event':
        case 'payment':
        case 'organization':
        case 'invoice_style':
        case 'todolist':
        case 'workhour':

            sequelize.query('SELECT insert_' + type + '_file(' + d + ',' + file_id + ',\'' + org + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
                .then(function(datas) {
                    logs.save_log_relation(
                        type,
                        'file',
                        d,
                        file_id,
                        'insert',
                        user_id,
                        org,
                        _tenant
                    );

                    utility.save_socket(org, type, 'update', user_id, d, null);
        
                }).catch(function(err) {
                    return res.status(400).render('file_insert_' + type + '_file: ' + err);
                });
            break;

        default:
            sequelize.query('SELECT insert_contact_file(' + d + ',' + file_id + ',\'' + org + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
                .then(function(datas) {
                    logs.save_log_relation(
                        type,
                        'file',
                        d,
                        file_id,
                        'insert',
                        user_id,
                        org,
                        _tenant
                    );

                    utility.save_socket(org, type, 'update', user_id, d, null);

                }).catch(function(err) {
                    return res.status(400).render('file_insert_default_file: ' + err);
                });
            break;
    }
};

exports.relations = function(req, res) {

    var Sequelize = require('sequelize'),
        model_a = req.body.data[0].name,
        id_a = req.body.data[0].id,
        model_b = req.body.data[1].name,
        id_b = req.body.data[1].id,
        operation = req.body.operation;

    if (operation == 'add' && id_a && id_b) {
        sequelize.query('SELECT insert_' + model_a + '_' + model_b + '(' + id_a + ',' + id_b + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {
                logs.save_log_relation(
                    model_a,
                    model_b,
                    id_a,
                    id_b,
                    'insert',
                    req.user.id,
                    req.headers['host'].split(".")[0],
                    req.user._tenant_id
                );

                utility.save_socket(req.headers['host'].split(".")[0], model_a, 'update', req.user.id, id_a, null);
                utility.save_socket(req.headers['host'].split(".")[0], model_b, 'update', req.user.id, id_b, null);


                if ((model_a == 'contact' && model_b == 'contact') ||
                    (model_a == 'product' && model_b == 'product') ||
                    (model_a == 'project' && model_b == 'project') ||
                    (model_a == 'event' && model_b == 'event') ||
                    (model_a == 'invoice' && model_b == 'invoice') ||
                    (model_a == 'expense' && model_b == 'expense') ||
                    (model_a == 'workhour' && model_b == 'workhour')) {

                    sequelize.query('SELECT insert_' + model_a + '_' + model_b + '(' + id_b + ',' + id_a + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                        .then(function(datas) {
                            res.status(200).send({});
                        }).catch(function(err) {
                            console.log(err);
                            return res.status(400).render('relations_isnert: ' + err);
                        });
                } else {
                    res.status(200).send({});

                }

            }).catch(function(err) {
                console.log(err);
                return res.status(400).render('relations_insert: ' + err);
            });
    } else if (operation == 'del' && id_a && id_b) {
        sequelize.query('SELECT delete_' + model_a + '_' + model_b + '(' + id_a + ',' + id_b + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {
                logs.save_log_relation(
                    model_a,
                    model_b,
                    id_a,
                    id_b,
                    'destroy',
                    req.user.id,
                    req.headers['host'].split(".")[0],
                    req.user._tenant_id
                );

                utility.save_socket(req.headers['host'].split(".")[0], model_a, 'update', req.user.id, id_a, null);
                utility.save_socket(req.headers['host'].split(".")[0], model_b, 'update', req.user.id, id_b, null);

                if ((model_a == 'contact' && model_b == 'contact') ||
                    (model_a == 'product' && model_b == 'product') ||
                    (model_a == 'project' && model_b == 'project') ||
                    (model_a == 'event' && model_b == 'event') ||
                    (model_a == 'invoice' && model_b == 'invoice') ||
                    (model_a == 'expense' && model_b == 'expense') ||
                    (model_a == 'workhour' && model_b == 'workhour')) {
                    sequelize.query('SELECT delete_' + model_a + '_' + model_b + '(' + id_b + ',' + id_a + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                        .then(function(datas) {
                            res.status(200).send({});
                        }).catch(function(err) {
                            console.log(err);
                            return res.status(400).render('relations_delete: ' + err);
                        });
                } else {
                    res.status(200).send({});

                }


            }).catch(function(err) {
                console.log(err);
                return res.status(400).render('relations_delete: ' + err);
            });
    } else {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Il campo id non può essere vuoto', '422'));

    }
};

exports.sync_relations = function(req, res) {
    var utility = require('../utility/utility.js');
    var Sequelize = require('sequelize'),
        model_a = req.body.model,
        id_a = req.body.id;

    if (utility.check_id(id_a)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.add) {
        req.body.add.forEach(function(addition) {
            var model_b = addition.model;
            var ids = addition.ids;

            if (ids && ids.length > 0) {
                ids.forEach(function(id_b) {
                    if (id_b) {
                        sequelize.query('SELECT insert_' + model_a + '_' + model_b + '(' + id_a + ',' + id_b + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {
                                logs.save_log_relation(
                                    model_a,
                                    model_b,
                                    id_a,
                                    id_b,
                                    'insert',
                                    req.user.id,
                                    req.headers['host'].split(".")[0],
                                    req.user._tenant_id
                                );

                                utility.save_socket(req.headers['host'].split(".")[0], model_a, 'update', req.user.id, id_a, null);
                                utility.save_socket(req.headers['host'].split(".")[0], model_b, 'update', req.user.id, id_b, null);

                                if ((model_a == 'contact' && model_b == 'contact') ||
                                    (model_a == 'product' && model_b == 'product') ||
                                    (model_a == 'project' && model_b == 'project') ||
                                    (model_a == 'event' && model_b == 'event') ||
                                    (model_a == 'workhour' && model_b == 'workhour')) {
                                    sequelize.query('SELECT insert_' + model_a + '_' + model_b + '(' + id_b + ',' + id_a + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        })
                                        .then(function(datas) {

                                        }).catch(function(err) {
                                            console.log(err);
                                            return res.status(400).render('relations_insert: ' + err);
                                        });
                                } else {
                                    //res.status(200).send({});
                                }

                            }).catch(function(err) {
                                console.log(err);
                                return res.status(400).render('relations_insert: ' + err);
                            });
                    }

                });
            }
        });
    }

    if (req.body.del) {
        req.body.del.forEach(function(deleted) {
            var model_b = deleted.model;
            var ids = deleted.ids;

            if (ids && ids.length > 0) {
                ids.forEach(function(id_b) {
                    sequelize.query('SELECT delete_' + model_a + '_' + model_b + '(' + id_a + ',' + id_b + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                        .then(function(datas) {
                            logs.save_log_relation(
                                model_a,
                                model_b,
                                id_a,
                                id_b,
                                'destroy ',
                                req.user.id,
                                req.headers['host'].split(".")[0],
                                req.user._tenant_id
                            );

                            utility.save_socket(req.headers['host'].split(".")[0], model_a, 'update', req.user.id, id_a, null);
                            utility.save_socket(req.headers['host'].split(".")[0], model_b, 'update', req.user.id, id_b, null);

                            if ((model_a == 'contact' && model_b == 'contact') ||
                                (model_a == 'product' && model_b == 'product') ||
                                (model_a == 'project' && model_b == 'project') ||
                                (model_a == 'event' && model_b == 'event') ||
                                (model_a == 'workhour' && model_b == 'workhour')) {
                                sequelize.query('SELECT delete_' + model_a + '_' + model_b + '(' + id_b + ',' + id_a + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                    .then(function(datas) {
                                        //res.status(200).send({});
                                    }).catch(function(err) {
                                        console.log(err);
                                        return res.status(400).render('relations_delete: ' + err);
                                    });
                            } else {
                                //res.status(200).send({});
                            }


                        }).catch(function(err) {
                            console.log(err);
                            return res.status(400).render('relations_delete: ' + err);
                        });
                });
            }
        });
    }

    res.status(200).send({});

};

function customForEachSwith(datas, contact_ids, model, all_any) {
    var allowModel = ['contact', 'product', 'project', 'event', 'workhour'];

    if(allowModel.indexOf(model) === -1) {
        return;
    }

    
    if(all_any == 'all'){// OR
        datas.forEach(function(con_id) {

            contact_ids.push(parseInt(con_id[model + '_custom_fields']));     
        });
    }
    else //AND
    {
        let tmp_cnt_ids = [];
        tmp_cnt_ids = contact_ids;

        contact_ids = [];

        datas.forEach(function(con_id) {
            
            if(!tmp_cnt_ids.includes(parseInt(con_id[model + '_custom_fields']))){
                tmp_cnt_ids.push(parseInt(con_id[model + '_custom_fields']));    
                contact_ids.push(parseInt(con_id[model + '_custom_fields']));     
            }
        });
    }

};

exports.customs = function(model, req, callback_) {
    var async = require('async');
    var Sequelize = require('sequelize');
    var contact_ids = [];

    async.parallel({
        custom: function(callback) {
            var cnt = 0;
            if (req.query.custom !== undefined && req.query.custom !== '') {
                var arr_custom = req.query.custom.split(',');
                arr_custom.forEach(function(cus) {
                    switch (cus.split('-')[3]) {
                        case 'num':
                            sequelize.query('SELECT ' + model + '_custom_fields(\'' + req.headers['host'].split(".")[0] + '\',' + req.user._tenant_id + ',\'' + cus.split('-')[1] + '\',\'' + cus.split('-')[3] + '\',' + cus.split('-')[0] + ',' + cus.split('-')[2] + ',null,null,null);', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                                .then(function(datas) {
                                    customForEachSwith(datas, contact_ids, model, req.query.all_any);
                                    
                                    if (cnt++ == arr_custom.length - 1) {
                                        callback(null, contact_ids);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });
                            break;

                        case 'bool':
                            sequelize.query('SELECT ' + model + '_custom_fields(\'' + req.headers['host'].split(".")[0] + '\',' + req.user._tenant_id + ',\'' + cus.split('-')[1] + '\',\'' + cus.split('-')[3] + '\',' + cus.split('-')[0] + ',null,' + cus.split('-')[2] + ',null, null);', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                                .then(function(datas) {
                                    customForEachSwith(datas, contact_ids, model, req.query.all_any);
                                
                                    if (cnt++ == arr_custom.length - 1) {
                                        callback(null, contact_ids);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });
                            break;

                        case 'str':
                            sequelize.query('SELECT ' + model + '_custom_fields(\'' + req.headers['host'].split(".")[0] + '\',' + req.user._tenant_id + ',\'' + cus.split('-')[1] + '\',\'' + cus.split('-')[3] + '\',' + cus.split('-')[0] + ',null,null,\'' + cus.split('-')[2] + '\',null);', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                                .then(function(datas) {
                                    customForEachSwith(datas, contact_ids, model, req.query.all_any);
                                    
                                    if (cnt++ == arr_custom.length - 1) {

                                        callback(null, contact_ids);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });
                            break;

                        case 'date':
                            sequelize.query('SELECT ' + model + '_custom_fields(\'' + req.headers['host'].split(".")[0] + '\',' + req.user._tenant_id + ',\'' + cus.split('-')[1] + '\',\'' + cus.split('-')[3] + '\',' + cus.split('-')[0] + ',null,null,null,\'%' + cus.split('-')[2].replace(/\//g, "-") + '%\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                                .then(function(datas) {
                                    customForEachSwith(datas, contact_ids, model, req.query.all_any);
                                    
                                    if (cnt++ == arr_custom.length - 1) {
                                        callback(null, contact_ids);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });
                            break;

                        case 'datetime':
                            sequelize.query('SELECT ' + model + '_custom_fields(\'' + req.headers['host'].split(".")[0] + '\',' + req.user._tenant_id + ',\'' + cus.split('-')[1] + '\',\'' + cus.split('-')[3] + '\',' + cus.split('-')[0] + ',null,null,null,\'%' + cus.split('-')[2].replace(/\//g, "-") + '%\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                                .then(function(datas) {
                                    customForEachSwith(datas, contact_ids, model, req.query.all_any);
                                  
                                    if (cnt++ == arr_custom.length - 1) {
                                        callback(null, contact_ids);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });
                            break;
                    }
                });
            } else {
                callback(null, null);
            }
        }
    }, function(err, results) {
        if (err) {
            callback_(err, null);
        }
        callback_(null, results);
    });
};

exports.get_parameters = function(req, model, callback_) {
    var util = require('util');
    var moment = require('moment');
    var params = {};


    params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'asc\'' : '\'' + req.query.direction + '\'';
    params.all_any = (req.query.all_any === undefined || req.query.all_any === '') ? '\'any\'' : '\'' + req.query.all_any + '\'';
    params.sortField = '';
    params.page_num = 1;
    params.page_count = 50;
    params.contact_ids = [];
    params.q = [];
    params.q1 = [];
    params.id = null;
    params.ids = [];
    params.status = [];
    params.role = null;
    params.product_ids = [];
    params.event_ids = [];
    params.project_ids = [];
    params.invoice_ids = [];
    params.expense_ids = [];
    params.payment_ids = [];
    params.workhour_ids = [];
    params.archivied = false;
    params.start_date = null;
    params.end_date = null;
    params.file_id = null;
    params.running = null;
    params.duration = null;
    params.is_paid = null;
    params.string_status = null;
    params.category = null;
    params.user_id = null;
    params.estimate = null;
    params.transaction_id = null;
    params.sent = null;
    params.success = null;
    params.residuo = null;
    params.verify = null;
    params.item_ids = [];
    params.is_dispo = false;
    params.document_type = null;
    params.notes = null;
    params.custom_ids = [];
    params.gdpr = null;
    params.color = null;
    params.year = null;
    params.month = null;
    params.comp_year = null;
    params.comp_month = null;
    params.category_id = null;
    params.sub_category_id = null;
    params.exclude_ids = [];
    params.from_ids = [];
    params.sdi_status = null;
    params.voucher = null;

    switch (model) {
        case 'contact':
        case 'product':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'name\'' : '\'' + req.query.sort + '\'';
            break;

        case 'event':
        case 'project':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'start_date\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

        case 'workhour':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'text\'' : '\'' + req.query.sort + '\'';
            break;

        case 'expense':
        case 'invoice':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'date\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

        case 'payment':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'date\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

        case 'file':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'created_at\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

    }

    

    if (req.query.status !== undefined && req.query.status !== '' && (
            model == 'payment' ||
            model == 'expense' ||
            model == 'invoice'
        )) {
        if (!utility.check_type_variable(req.query.status, 'string')) {
            callback_(null, utility.error422('status', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.string_status = '\'' + req.query.status + '\'';
    }

    if (req.query.is_paid !== undefined && req.query.is_paid !== '') {
        if (utility.check_id(req.query.is_paid)) {
            callback_(null, utility.error422('is_paid', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.is_paid == 1) {
            params.is_paid = true;
        } else {
            params.is_paid = false;
        }
    }

    ['success', 'sent', 'residuo','voucher'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '') {
            if (utility.check_type_variable(req.query[mType], 'boolean')) {
                callback_(null, utility.error422(mType, 'Parametro non valido', 'Parametro non valido', '422'));
            }
            params[mType] = req.query[mType];
        }
    });

    /*if (req.query.success !== undefined && req.query.success !== '') {
        if (utility.check_type_variable(req.query.success, 'boolean')) {
            callback_(null, utility.error422('success', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.success = req.query.success;
    }
    if (req.query.sent !== undefined && req.query.sent !== '') {
        if (utility.check_type_variable(req.query.sent, 'boolean')) {
            callback_(null, utility.error422('sent', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.sent = req.query.sent;
    }

    if (req.query.residuo !== undefined && req.query.residuo !== '') {
        if (utility.check_type_variable(req.query.residuo, 'boolean')) {
            callback_(null, utility.error422('residuo', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.residuo = req.query.residuo;
    }*/

    if (req.query.item_id !== undefined && req.query.item_id != '' && req.query.item_id != 'undefined') {
        if (util.isArray(req.query.item_id)) {
            params.item_ids = req.query.item_id;
        } else {
            params.item_ids.push(req.query.item_id);
        }
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            callback_(null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {

            var q_array = req.query.q.split(' ');
            q_array.forEach(function(s) {
                params.q.push('\'%' + s.replace(/'/g, "''") + '%\'');
                params.q1.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }
    }

   
   if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            callback_(null, utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        params.start_date = '\'' + req.query.from_date + '\'';
    }
    
    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            callback_(null, utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        params.end_date = '\'' + req.query.to_date + '\'';
    }

    ['notes', 'document_type', 'color','category', 'gdpr', 'transaction_id', 'sdi_status'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '' && req.query[mType] !== 'undefined') {
            if (!utility.check_type_variable(req.query[mType], 'string')) {
                callback_(null, utility.error422(mType, 'Parametro non valido', 'Parametro non valido', '422'));
            }
            params[mType] = '\'' + req.query[mType] + '\'';
        }
    });

    /*
    if (req.query.transaction_id !== undefined && req.query.transaction_id !== '') {
        if (!utility.check_type_variable(req.query.transaction_id, 'string')) {
            callback_(null, utility.error422('transaction_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.transaction_id = req.query.transaction_id;
    }

    if (req.query.gdpr !== undefined && req.query.gdpr !== '') {
        if (!utility.check_type_variable(req.query.gdpr, 'string')) {
            callback_(null, utility.error422('gdpr', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.gdpr = '\'' + req.query.gdpr + '\'';
    }

    if (req.query.category !== undefined && req.query.category !== '') {
        if (utility.check_type_variable(req.query.category, 'string')) {
            callback_(utility.error422('category', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.category = '\'' + req.query.category + '\'';
    }

    if (req.query.notes !== undefined && req.query.notes !== '') {
        if (!utility.check_type_variable(req.query.notes, 'string')) {
            callback_(null, utility.error422('notes', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.notes = '\'' + req.query.notes + '\'';
    }

    if (req.query.document_type !== undefined && req.query.document_type !== '') {
        if (!utility.check_type_variable(req.query.document_type, 'string')) {
            callback_(null, utility.error422('notes', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.document_type = '\'' + req.query.document_type + '\'';
    }

    if (req.query.color !== undefined && req.query.color !== '') {
        if (!utility.check_type_variable(req.query.color, 'string')) {
            callback_(null, utility.error422('color', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.color = '\'' + req.query.color + '\'';
    }*/

    if ((req.query.id !== undefined && utility.check_id(req.query.id))) {
       callback_(null, utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    ['id', 'archivied', 'user_id',  'sezionale','duration', 'running'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '' && req.query[mType] !== 'undefined') {
            params[mType] = req.query[mType];
        }
    });

    ['year','month','comp_year','comp_month','category_id','sub_category_id'].forEach(function(mType) { 
        if (req.query[mType] !== undefined && req.query[mType] != '' && req.query[mType] !== 'undefined') {
            if (util.isArray(req.query[mType])) {
                params[mType] = req.query[mType].map(Number);
            } else {
                params[mType] = parseInt(req.query[mType]);
            }
        }
    });




    /*if (req.query.duration !== undefined && req.query.duration !== '') {
        params.duration = true;
    }

    if (req.query.running !== undefined && req.query.running !== '') {
        params.running = true;
    }

    if (req.query.id !== undefined && req.query.id !== '') {
        params.id = req.query.id;
    }

    if (req.query.archivied !== undefined && req.query.archivied !== '') {
        params.archivied = req.query.archivied;
    }
    
    if (req.query.user_id !== undefined && req.query.user_id !== '') {
        params.user_id = req.query.user_id;
    }*/

    if (req.query.dispo !== undefined && req.query.dispo !== '') {
        params.is_dispo = req.query.dispo;
    }

    if (req.query.pag !== undefined && req.query.pag !== '') {
        if (utility.check_id(req.query.pag)) {
            callback_(null, utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.page_num = req.query.pag;
    }

    if (req.query.xpag !== undefined && req.query.xpag !== '') {
        if (utility.check_id(req.query.xpag)) {
            callback_(null, utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.page_count = req.query.xpag;
    }

    ['verify', 'role', 'file_id'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '') {
            if (utility.check_id(req.query[mType])) {
                callback_(null, utility.error422(mType, 'Parametro non valido', 'Parametro non valido', '422'));
            }
            params[mType] = req.query[mType];
        }
    });

    /*
    if (req.query.file_id !== undefined && req.query.file_id !== '') {
        if (utility.check_id(req.query.file_id)) {
            callback_(null, utility.error422('file_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.file_id = req.query.file_id;
    }

    if (req.query.verify !== undefined && req.query.verify !== '') {
        if (utility.check_id(req.query.verify)) {
            callback_(null, utility.error422('verify', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.verify = req.query.verify;
    }


    if (req.query.role !== undefined && req.query.role != '') {
        if (utility.check_id(req.query.role)) {
            callback_(null, utility.error422('role', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.role = req.query.role;
    }*/

    if (req.query.status !== undefined && req.query.status !== '' && (
            model == 'contact' ||
            model == 'product' ||
            model == 'project' ||
            model == 'workhour' ||
            model == 'event'
        )) {
        if (util.isArray(req.query.status)) {
            params.status = req.query.status;
        } else {
            params.status.push(req.query.status);
        }
    }


    ['from_id','contact_id', 'product_id', 'custom_id', 'event_id', 'project_id', 'workhour_id', 'invoice_id', 'expense_id', 'payment_id','exclude_id'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] != '') {
            if (util.isArray(req.query[mType])) {
                params[mType + 's'] = req.query[mType].map(Number);
            } else {
                params[mType + 's'].push(req.query[mType]);
            }
        }
    });

    
    /*if (req.query.product_id !== undefined && req.query.product_id != '') {
        if (util.isArray(req.query.product_id)) {
            params.product_ids = req.query.product_id.map(Number);
        } else {
            params.product_ids.push(req.query.product_id);
        }
    }

    if (req.query.custom_id !== undefined && req.query.custom_id != '') {
        if (util.isArray(req.query.custom_id)) {
            params.custom_ids = req.query.custom_id.map(Number);
        } else {
            params.custom_ids.push(req.query.custom_id);
        }
    }

    if (req.query.event_id !== undefined && req.query.event_id != '') {
        if (util.isArray(req.query.event_id)) {
            params.event_ids = req.query.event_id.map(Number);
        } else {
            params.event_ids.push(req.query.event_id);
        }
    }

    if (req.query.project_id !== undefined && req.query.project_id != '') {
        if (util.isArray(req.query.project_id)) {
            params.project_ids = req.query.project_id.map(Number);
        } else {
            params.project_ids.push(req.query.project_id);
        }
    }

    if (req.query.workhour_id !== undefined && req.query.workhour_id != '') {
        if (util.isArray(req.query.workhour_id)) {
            params.workhour_ids = req.query.workhour_id.map(Number);
        } else {
            params.workhour_ids.push(req.query.workhour_id);
        }
    }

    if (req.query.invoice_id !== undefined && req.query.invoice_id != '') {
        if (util.isArray(req.query.invoice_id)) {
            params.invoice_ids = req.query.invoice_id.map(Number);
        } else {
            params.invoice_ids.push(req.query.invoice_id);
        }
    }

    if (req.query.expense_id !== undefined && req.query.expense_id != '') {
        if (util.isArray(req.query.expense_id)) {
            params.expense_ids = req.query.expense_id.map(Number);
        } else {
            params.expense_id.push(req.query.expense_id);
        }
    }

    if (req.query.payment_id !== undefined && req.query.payment_id != '') {
        if (util.isArray(req.query.payment_id)) {
            params.payment_ids = req.query.payment_id.map(Number);
        } else {
            params.payment_ids.push(req.query.payment_id);
        }
    }

    if (req.query.contact_id !== undefined && req.query.contact_id != '' && req.query.contact_id != 'undefined') {

        if (util.isArray(req.query.contact_id)) {
            params.contact_ids = req.query.contact_id.map(Number);
        } else {
            params.contact_ids.push(req.query.contact_id);
        }
    }
    */

    if (req.query.estimate !== undefined && req.query.estimate !== '') {
        if (utility.check_id(req.query.estimate)) {
            callback_(null, utility.error422('estimate', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.estimate == 1) {
            params.estimate = true;
        } else {
            params.estimate = false;
        }
    }

    callback_(null, params);
};
exports.get_parameters_v1 = function(req, model, callback_) {
    var util = require('util');
    var moment = require('moment');
    var params = {};


    params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'asc\'' : '\'' + req.query.direction + '\'';
    params.all_any = (req.query.all_any === undefined || req.query.all_any === '') ? '\'any\'' : '\'' + req.query.all_any + '\'';
    params.sortField = '';
    params.page_num = 1;
    params.page_count = 50;
    params.contact_ids = [];
    params.q = [];
    params.q1 = [];
    params.id = null;
    params.ids = [];
    params.status = [];
    params.role = null;
    params.product_ids = [];
    params.event_ids = [];
    params.project_ids = [];
    params.invoice_ids = [];
    params.expense_ids = [];
    params.payment_ids = [];
    params.workhour_ids = [];
    params.archivied = false;
    params.start_date = null;
    params.end_date = null;
    params.file_id = null;
    params.running = null;
    params.duration = null;
    params.is_paid = null;
    params.string_status = null;
    params.category = null;
    params.user_id = null;
    params.estimate = null;
    params.transaction_id = null;
    params.sent = null;
    params.success = null;
    params.sezionale = null;
    params.residuo = null;
    params.verify = null;
    params.item_ids = [];
    params.is_dispo = false;
    params.document_type = null;
    params.notes = null;
    params.custom_ids = [];
    params.gdpr = null;
    params.color = null;
    params.year = null;
    params.month = null;
    params.comp_year = null;
    params.comp_month = null;
    params.category_id = null;
    params.sub_category_id = null;
    params.exclude_ids = [];
    params.from_ids = [];
    params.sdi_status = null;
    params.voucher = null;
    params.assurance_ids = [];

    switch (model) {
        case 'contact':
        case 'product':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'name\'' : '\'' + req.query.sort + '\'';
            break;

        case 'event':
        case 'project':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'start_date\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

        case 'workhour':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'text\'' : '\'' + req.query.sort + '\'';
            break;

        case 'expense':
        case 'invoice':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'date\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

        case 'payment':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'date\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

        case 'file':
            params.sortField = (req.query.sort === undefined || req.query.sort === '') ? '\'created_at\'' : '\'' + req.query.sort + '\'';
            params.sortDirection = (req.query.direction === undefined || req.query.direction === '') ? '\'desc\'' : '\'' + req.query.direction + '\'';
            break;

    }

    if (req.query.status !== undefined && req.query.status !== '' && (
            model == 'payment' ||
            model == 'expense' ||
            model == 'invoice'
        )) {
        if (!utility.check_type_variable(req.query.status, 'string')) {
            callback_(null, utility.error422('status', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.string_status = '\'' + req.query.status + '\'';
    }

    if (req.query.is_paid !== undefined && req.query.is_paid !== '') {
        if (utility.check_id(req.query.is_paid)) {
            callback_(null, utility.error422('is_paid', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.is_paid == 1) {
            params.is_paid = true;
        } else {
            params.is_paid = false;
        }
    }

    ['success', 'sent', 'residuo', 'voucher'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '') {
            if (utility.check_type_variable(req.query[mType], 'boolean')) {
                callback_(null, utility.error422(mType, 'Parametro non valido', 'Parametro non valido', '422'));
            }
            params[mType] = req.query[mType];
        }
    });

    if (req.query.item_id !== undefined && req.query.item_id != '' && req.query.item_id != 'undefined') {
        if (util.isArray(req.query.item_id)) {
            params.item_ids = req.query.item_id;
        } else {
            params.item_ids.push(req.query.item_id);
        }
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            callback_(null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {
            let new_str = req.query.q.replace(/[^a-z0-9:]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
               
                params.q.push('\'' + s.replace(/'/g, "''") + '\'');
                params.q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

   
   if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            callback_(null, utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        params.start_date = '\'' + req.query.from_date + '\'';
    }
    
    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            callback_(null, utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        params.end_date = '\'' + req.query.to_date + '\'';
    }

    ['notes', 'document_type', 'color', 'category', 'gdpr', 'transaction_id', 'sdi_status'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '' && req.query[mType] !== 'undefined') {
            if (!utility.check_type_variable(req.query[mType], 'string')) {
                callback_(null, utility.error422(mType, 'Parametro non valido', 'Parametro non valido', '422'));
            }
            params[mType] = '\'' + req.query[mType] + '\'';
        }
    });

    if ((req.query.id !== undefined && utility.check_id(req.query.id))) {
       callback_(null, utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    ['id', 'archivied', 'user_id', 'sezionale','duration', 'running'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '' && req.query[mType] !== 'undefined') {
            params[mType] = req.query[mType];
        }
    });

    ['year','month','comp_year','comp_month','category_id','sub_category_id'].forEach(function(mType) { 
        if (req.query[mType] !== undefined && req.query[mType] != '' && req.query[mType] !== 'undefined') {
            if (util.isArray(req.query[mType])) {
                params[mType] = req.query[mType].map(Number);
            } else {
                params[mType] = parseInt(req.query[mType]);
            }
        }
    });

    if (req.query.dispo !== undefined && req.query.dispo !== '') {
        params.is_dispo = req.query.dispo;
    }

    if (req.query.pag !== undefined && req.query.pag !== '') {
        if (utility.check_id(req.query.pag)) {
            callback_(null, utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.page_num = req.query.pag;
    }

    if (req.query.xpag !== undefined && req.query.xpag !== '') {
        if (utility.check_id(req.query.xpag)) {
            callback_(null, utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        params.page_count = req.query.xpag;
    }

    ['verify', 'role', 'file_id'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] !== '') {
            if (utility.check_id(req.query[mType])) {
                callback_(null, utility.error422(mType, 'Parametro non valido', 'Parametro non valido', '422'));
            }
            params[mType] = req.query[mType];
        }
    });

    if (req.query.status !== undefined && req.query.status !== '' && (
            model == 'contact' ||
            model == 'product' ||
            model == 'project' ||
            model == 'workhour' ||
            model == 'event'
        )) {
        if (util.isArray(req.query.status)) {
            params.status = req.query.status;
        } else {
            params.status.push(req.query.status);
        }
    }


    ['from_id','contact_id', 'product_id', 'custom_id', 'event_id', 'project_id', 'workhour_id', 'invoice_id', 'expense_id', 'payment_id','exclude_id','assurance_id'].forEach(function(mType) {
        if (req.query[mType] !== undefined && req.query[mType] != '') {
            if (util.isArray(req.query[mType])) {
                params[mType + 's'] = req.query[mType].map(Number);
            } else {
                params[mType + 's'].push(req.query[mType]);
            }
        }
    });


    if (req.query.estimate !== undefined && req.query.estimate !== '') {
        if (utility.check_id(req.query.estimate)) {
            callback_(null, utility.error422('estimate', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.estimate == 1) {
            params.estimate = true;
        } else {
            params.estimate = false;
        }
    }

    callback_(null, params);
};


exports.get_specialita = function(specialita) {
    var spec = [];

    if (!utility.check_type_variable(specialita, 'string')) {
            return null;
        }
        if (specialita !== undefined && specialita !== '') {

            var spec_array = specialita.split(',');
            spec_array.forEach(function(s) {
                switch (s) {
                    case 'snowboard':
                        spec.push(157);
                    break;

                    case 'fuori pista':
                    case 'freestyle':
                        spec.push(159);
                    break;

                    case 'fondo':
                    case '170 - 190 L':
                        spec.push(160);
                    break;

                    case 'telemark':
                    case 'adulti':
                    case '160 - 180 M':
                        spec.push(161);
                    break;

                    case 'pali':
                        spec.push(158);
                    break;

                    case 'disabili':
                    case 'freeride':
                    case '140 - 165 Fat Junior':
                        spec.push(164);
                    break;

                    case 'bambini':
                    case '130 - 155 Junior':
                        spec.push(165);
                    break;

                    case 'guida alpina':
                    case 'sci alpinismo':
                        spec.push(198);
                    break;

                    case 'bambini principianti':
                        spec.push(166);
                    break;

                    case 'discesa':
                        spec.push(156);
                    break;

                    case 'non vedenti':
                    case 'ciaspole':
                    case '150 - 170 S':
                        spec.push(163);
                    break;

                    case 'carving clinic':
                        spec.push(343);
                    break;

                }
            });
        }
    

    return spec;
};

exports.sendSMS = function(auth, sendsms, callback) {
    var BASEURL = 'https://api.skebby.it/API/v1.0/REST/';
    var request = require('request');

    request({
        url: BASEURL + 'sms',
        method: 'POST',
        headers: { 'user_key' : auth.user_key, 'Session_key' : auth.session_key },
        json: true,
        body:  sendsms,

        callback: function (error, responseMeta, response) {
            if (!error && responseMeta.statusCode == 201) {
                callback(response.result !== 'OK', response);
            }
            else {
                callback(response.result, null);
            }
        }
    });  
};

exports.login_skebby = function(callback){
    var BASEURL = 'https://api.skebby.it/API/v1.0/REST/';

    var request = require('request');

    request({
        url: BASEURL + 'login?username=' + config.skebby.username + '&password=' + config.skebby.password,
        method: 'GET',
        callback: function (error, responseMeta, response) {
            if (!error && responseMeta.statusCode == 200) {
                var auth = response.split(';');
                callback(error, {
                    user_key : auth[0],
                    session_key : auth[1]
                });
            }
            else {
                callback(error);
            }
        }
    });

};

exports.get_lang = function(lang) {
    var lingua = [];

    if (!utility.check_type_variable(lang, 'string')) {
            return null;
        }
        if (lang !== undefined && lang !== '') {

            var lingua_array = lang.split(',');
            lingua_array.forEach(function(s) {
                switch (s) {
                    case 'italiano':
                        lingua.push(123);
                        break;

                    case 'inglese':
                        lingua.push(125);
                        break;

                    case 'francese':
                        lingua.push(124);
                        break;

                    case 'spagnolo':
                        lingua.push(128);
                        break;

                    case 'russo':
                        lingua.push(127);
                        break;

                    case 'tedesco':
                        lingua.push(126);
                        break;

                    case 'svedese':
                        lingua.push(255);
                        break;

                    case 'portoghese':
                        lingua.push(254);
                        break;
                    
                }
            });
        }
    

    return lingua;
};

exports.isJsonValid = function(sjson){
    try {
        JSON.parse(sjson);
        return true;
    } catch (e) {
        return false;
    }
}

exports.formatta_euro = function(number) {
    if (number) {
        var numberStr = parseFloat(number).toFixed(2).toString();
        var numFormatDec = numberStr.slice(-2);
        numberStr = numberStr.substring(0, numberStr.length - 3);
        var numFormat = [];
        while (numberStr.length > 3) {
            numFormat.unshift(numberStr.slice(-3));
            numberStr = numberStr.substring(0, numberStr.length - 3);
        }
        numFormat.unshift(numberStr);
        return numFormat.join('.') + '.' + numFormatDec;
    }
    return 0.00;
};