var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    indexes = require('../utility/indexes.js'),
    export_datas = require('../utility/exports.js'),
    logs = require('../utility/logs.js');

exports.stats = function(req, res) {

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
            return res.status(400).render('project_stats: ' + err);
        }

        sequelize.query('SELECT projects_stats_v2(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].projects_stats_v2
                });
            }).catch(function(err) {
                return res.status(400).render('projects_stats_v2: ' + err);
            });
    });
};

exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('project', req, function(err, results) {
        if (err) {
            return res.status(400).render('project_custom: ' + err);
        }
        indexes.project(req, results, function(err, index) {
            if (err) {
                return res.status(400).render('project_index: ' + err);
            }

            /*if (index.meta.total_items > 1000) {
                return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
            } else {*/
                export_datas.project(req, results, function(err, response) {
                    if (err) {
                        return res.status(400).render('project_index: ' + err);
                    }

                    json2csv({
                        data: response.projects,
                        fields: response.fields,
                        fieldNames: response.field_names
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=projects.csv'
                        });
                        res.end(csv);
                    });
                });
           // }
        });

    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('project', req, function(err, results) {
        if (err) {
            return res.status(400).render('project_custom: ' + err);
        }
        indexes.project(req, results, function(err, response) {
            if (err) {
                return res.status(400).render('project_index: ' + err);
            }
            res.json(response);
        });

    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Find a specific project searched by id
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_project(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * New project, send a notification at the creation of a todo, and control reminders
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var project = {};
    if (req.body.data.attributes !== undefined) {
        // Import case

        if (req.body.data.attributes.name == undefined || req.body.data.attributes.name === '') {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Il campo name non può essere vuoto', '422'));
        }

        project['owner_id'] = req.user.id;
        if (req.body.data.attributes.status != '' && req.body.data.attributes.status != undefined) {
            project['status'] = 'Aperto';
        } else {
            project['status'] = req.body.data.attributes.status;
        }
        project['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        project['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        project['price'] = (req.body.data.attributes.price ? req.body.data.attributes.price : 0.0);
        project['cost'] = (req.body.data.attributes.cost ? req.body.data.attributes.cost : 0.0);
        project['duration'] = req.body.data.attributes.duration;
        project['set_event_color'] = req.body.data.attributes.set_event_color;
        project['color'] = (req.body.data.attributes.color && utility.check_type_variable(req.body.data.attributes.color, 'string') && req.body.data.attributes.color !== null ? req.body.data.attributes.color.replace(/'/g, "''''") : '');
        project['thumbnail_url'] = (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url.replace(/'/g, "''''") : '');
        project['thumbnail_name'] = (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name.replace(/'/g, "''''") : '');
        project['thumbnail_etag'] = (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag.replace(/'/g, "''''") : '');
        project['thumbnail_type'] = (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type.replace(/'/g, "''''") : '');
        project['_tenant_id'] = req.user._tenant_id;
        project['organization'] = req.headers['host'].split(".")[0];
        project['mongo_id'] = null;
        project['project_status_id'] = ((req.body.data.relationships && req.body.data.relationships.project_state && req.body.data.relationships.project_state.data && req.body.data.relationships.project_state.data.id && !utility.check_id(req.body.data.relationships.project_state.data.id)) ? req.body.data.relationships.project_state.data.id : null);
        project['start_date'] = req.body.data.attributes.start_date;
        project['end_date'] = req.body.data.attributes.end_date;
        project['archivied'] = req.body.data.attributes.archivied;
        project['notes'] = (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');


        sequelize.query('SELECT insert_project_v4(\'' +
                JSON.stringify(project) + '\',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].insert_project_v4 && datas[0].insert_project_v4 != null) {
                    req.body.data.id = datas[0].insert_project_v4;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_project_v4,
                        'body': req.body.data.relationships,
                        'model': 'project',
                        'user_id': req.user.id,
                        'array': ['product', 'contact', 'payment', 'custom_field', 'todolist', 'related_project', 'address']
                    }, function(err, results) {

                        req.body.data.attributes.total_files_number = results.file;
                        req.body.data.attributes.total_contacts_number = results.contact;
                        req.body.data.attributes.total_payments = results.payment;

                        sequelize.query('SELECT project_custom_fields_trigger(' +
                                datas[0].insert_project_v4 + ',' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                            .then(function(cus) {
                                req.params.id = datas[0].insert_project_v4;
                                finds.find_project(req, function(err, response) {
                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'project',
                                        'insert',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        datas[0].insert_project_v4,
                                        'Aggiunto progetto ' + datas[0].insert_project_v4
                                    );

                                    utility.save_socket(req.headers['host'].split(".")[0], 'project', 'insert', req.user.id, datas[0].insert_project_v4, response.data);
                        
                                    res.json(response);
                                });

                            }).catch(function(err) {
                                return res.status(400).render('project_update: permesso negato');
                            });
                    });

                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }
            }).catch(function(err) {
                return res.status(400).render('project_insert: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Update an existing project searched by id
 * When a project change we must change also all embedded documents related
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var project = {};
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes.name == undefined || req.body.data.attributes.name === '') {
        return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Il campo name non può essere vuoto', '422'));
    }

    if (req.body.data.attributes.start_date != null && !moment(req.body.data.attributes.start_date).isValid()) {
        return res.status(422).send(utility.error422('start_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.end_date != null && !moment(req.body.data.attributes.end_date).isValid()) {
        return res.status(422).send(utility.error422('end_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_project(req, function(err, old_project) {

        sequelize.query('SELECT update_project_v4(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (req.body.data.attributes.price ? req.body.data.attributes.price : 0.0) + ',' +
                (req.body.data.attributes.cost ? req.body.data.attributes.cost : 0.0) + ',' +
                req.body.data.attributes.duration + ',' +
                req.body.data.attributes.set_event_color + ',\'' +
                (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') && req.body.data.attributes.status !== null ? req.body.data.attributes.status : 'Aperto') + '\',\'' +
                (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type : '') + '\',' +
                ((req.body.data.relationships && req.body.data.relationships.project_state && req.body.data.relationships.project_state.data && req.body.data.relationships.project_state.data.id && !utility.check_id(req.body.data.relationships.project_state.data.id)) ? req.body.data.relationships.project_state.data.id : null) + ',' +
                req.user.role_id + ',' + req.user.id + ',' +
                (req.body.data.attributes.start_date !== null ? '\'' + req.body.data.attributes.start_date + '\'' : null) + ',' +
                (req.body.data.attributes.end_date !== null ? '\'' + req.body.data.attributes.end_date + '\'' : null) + ',\'' +
                (req.body.data.attributes.color && utility.check_type_variable(req.body.data.attributes.color, 'string') && req.body.data.attributes.color !== null ? req.body.data.attributes.color : '') + '\',' +
                req.body.data.attributes.archivied + ',\'' +
                (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                var old = old_project;
                sequelize.query('SELECT delete_project_relations_v1(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'project',
                            'user_id': req.user.id,
                            'array': ['todolist', 'file', 'address']
                        }, function(err, results) {

                            req.body.data.attributes.total_files_number = results.file;
                            req.body.data.attributes.total_contacts_number = results.contact;
                            req.body.data.attributes.total_payments = results.payment;

                            finds.find_project(req, function(err, response) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'project',
                                    'update',
                                    req.user.id,
                                    JSON.stringify(req.body.data.attributes),
                                    req.params.id,
                                    'Modificato progetto ' + req.params.id
                                );

                                utility.save_socket(req.headers['host'].split(".")[0], 'project', 'update', req.user.id, req.params.id, response.data);
                        
                                res.json(response);
                            });
                        });
                    }).catch(function(err) {
                        console.log(err);
                        return res.status(400).render('project_update: ' + err);
                    });

            }).catch(function(err) {
                console.log(err);
                return res.status(400).render('project_update: ' + err);
            });
    });
};

exports.update_all = function(req, res) {
    var util = require('util');

    if(!util.isArray(req.body)){
        sequelize.query('SELECT update_project_all_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_project_all_v1'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_project_all_v1']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_project_all_v1'];
                finds.find_project(req, function(err, results) {
                    if (err) {
                        return res.status(err.errors[0].status).send(err);
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'project', 'update', req.user.id, req.params.id, results.data);
                        
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
    }else{
        sequelize.query('SELECT update_project_all_array(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_project_all_array'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_project_all_array']) {
               res.status(200).send({});
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Delete an existing project searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT delete_project_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(!datas[0].delete_project_v1){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }else{

                utility.save_socket(req.headers['host'].split(".")[0], 'project', 'delete', req.user.id, req.params.id, null);
                        
                res.json({
                    'data': {
                        'type': 'projects',
                        'id': datas[0].delete_project_v1
                    }
                });
            }

        }).catch(function(err) {
            return res.status(400).render('project_destroy: ' + err);
        });


};


exports.sblocca_progetto = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        project_id = req.body.project_id;

    sequelize.query('SELECT sblocca_progetto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            project_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});
        }).catch(function(err) {
            return res.status(400).render('sblocca_progetto: ' + err);
        });
};