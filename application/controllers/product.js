var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    export_datas = require('../utility/exports.js'),
    indexes = require('../utility/indexes.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');

exports.stats = function(req, res) {
    sequelize.query('SELECT products_stats(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\');')
        .then(function(datas) {
            res.json({
                'meta': datas[0][0].products_stats
            });
        }).catch(function(err) {
            return res.status(400).render('product_stats: ' + err);
        });
};

exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.product_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('product', req, function(err, results) {
        if (err) {
            return res.status(400).render('product_custom: ' + err);
        }
        indexes.product(req, results, function(err, index) {
            if (err) {
                return res.status(400).render('product_index: ' + err);
            }

            /*if (index.meta.total_items > 1000) {
                return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
            } else {*/
                export_datas.product(req, results, function(err, response) {
                    if (err) {
                        return res.status(400).render('product_index: ' + err);
                    }

                    json2csv({
                        data: response.products,
                        fields: response.fields,
                        fieldNames: response.field_names
                    }, function(err, csv) {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=products.csv'
                        });
                        res.end(csv);
                    });
                });
            //}
        });

    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.product_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('product', req, function(err, results) {
        if (err) {
            return res.status(400).render('product_custom: ' + err);
        }
        indexes.product(req, results, function(err, response) {
            if (err) {
                return res.status(400).render('product_index: ' + err);
            }
            res.json(response);
        });
    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Find a specific product searched by id
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.product_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_product(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * New product, send a notification at the creation of a todo, and control reminders
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.product_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var product = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        if (req.body.data.attributes.name == undefined ||
            req.body.data.attributes.name === '' ||
            !utility.check_type_variable(req.body.data.attributes.name, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Il campo nome non può essere vuoto', '422'));
        }


        product['owner_id'] = req.user.id;
        if (req.body.data.attributes.status != '' && req.body.data.attributes.status != undefined) {
            product['status'] = 'Aperto';
        } else {
            product['status'] = req.body.data.attributes.status;
        }
        product['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        product['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        product['price'] = req.body.data.attributes.price;
        product['cost'] = req.body.data.attributes.cost;
        product['duration'] = req.body.data.attributes.duration;
        product['set_event_color'] = req.body.data.attributes.set_event_color;
        product['color'] = (req.body.data.attributes.color && utility.check_type_variable(req.body.data.attributes.color, 'string') && req.body.data.attributes.color !== null ? req.body.data.attributes.color.replace(/'/g, "''''") : '');
        product['thumbnail_url'] = (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url.replace(/'/g, "''''") : '');
        product['thumbnail_name'] = (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name.replace(/'/g, "''''") : '');
        product['thumbnail_etag'] = (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag.replace(/'/g, "''''") : '');
        product['thumbnail_type'] = (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type.replace(/'/g, "''''") : '');
        product['_tenant_id'] = req.user._tenant_id;
        product['organization'] = req.headers['host'].split(".")[0];
        product['mongo_id'] = null;
        product['over_price'] = req.body.data.attributes.over_price;
        product['product_status_id'] = ((req.body.data.relationships && req.body.data.relationships.product_state && req.body.data.relationships.product_state.data && req.body.data.relationships.product_state.data.id && !utility.check_id(req.body.data.relationships.product_state.data.id)) ? req.body.data.relationships.product_state.data.id : null);
        product['archivied'] = req.body.data.attributes.archivied;
        product['notes'] = (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
        product['option'] = (req.body.data.attributes.option && utility.check_type_variable(req.body.data.attributes.option, 'string') && req.body.data.attributes.option !== null ? req.body.data.attributes.option.replace(/'/g, "''''") : null);

        sequelize.query('SELECT insert_product_v5(\'' +
                JSON.stringify(product) + '\',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].insert_product_v5 && datas[0].insert_product_v5 != null) {
                    req.body.data.id = datas[0].insert_product_v5;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_product_v5,
                        'body': req.body.data.relationships,
                        'model': 'product',
                        'user_id': req.user.id,
                        'array': ['project', 'contact', 'payment', 'custom_field', 'todolist', 'related_product', 'address']
                    }, function(err, results) {

                        req.body.data.attributes.total_files_number = results.file;
                        req.body.data.attributes.total_contacts_number = results.contact;
                        req.body.data.attributes.total_payments = results.payment;

                        sequelize.query('SELECT product_custom_fields_trigger(' +
                                datas[0].insert_product_v5 + ',' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                     useMaster: true
                                })
                            .then(function(cus) {

                                req.params.id = datas[0].insert_product_v5;
                                finds.find_product(req, function(err, response) {
                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'product',
                                        'insert',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        datas[0].insert_product_v5,
                                        'Aggiunto prodotto ' + datas[0].insert_product_v5
                                    );

                                    utility.save_socket(req.headers['host'].split(".")[0], 'product', 'insert', req.user.id, datas[0].insert_product_v5, response.data);
                
                                    res.json(response);
                                });

                            }).catch(function(err) {
                                return res.status(500).send(err);
                            });

                    });

                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }
            }).catch(function(err) {
                console.log(err);
                return res.status(500).send(err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Update an existing product searched by id
 * When a product change we must change also all embedded documents related
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.product_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var product = {};

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.name == undefined ||
        req.body.data.attributes.name === '' ||
        !utility.check_type_variable(req.body.data.attributes.name, 'string')) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Il campo nome non può essere vuoto', '422'));
    }

    finds.find_product(req, function(err, old_product) {

        sequelize.query('SELECT update_product_v5(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.body.data.attributes.price + ',' +
                req.body.data.attributes.cost + ',' +
                req.body.data.attributes.duration + ',' +
                req.body.data.attributes.set_event_color + ',\'' +
                (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') && req.body.data.attributes.status !== null ? req.body.data.attributes.status : 'Aperto') + '\',\'' +
                (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type : '') + '\',' +
                ((req.body.data.relationships && req.body.data.relationships.product_state && req.body.data.relationships.product_state.data && req.body.data.relationships.product_state.data.id && !utility.check_id(req.body.data.relationships.product_state.data.id)) ? req.body.data.relationships.product_state.data.id : null) + ',' +
                req.user.role_id + ',' + req.user.id + ',' +
                req.body.data.attributes.over_price + ',' +
                req.body.data.attributes.archivied + ',\'' +
                (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.color && utility.check_type_variable(req.body.data.attributes.color, 'string') && req.body.data.attributes.color !== null ? req.body.data.attributes.color.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.option && utility.check_type_variable(req.body.data.attributes.option, 'string') && req.body.data.attributes.option !== null ? '\'' + req.body.data.attributes.option.replace(/'/g, "''''") + '\'' : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(product) {
                console.log(product[0].update_product_v5);

                if (!product[0].update_product_v5) {
                    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                }

                var old = old_product;
                sequelize.query('SELECT delete_product_relations_v1(' +
                        req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'product',
                            'user_id': req.user.id,
                            'array': ['payment', 'todolist', 'file', 'address']
                        }, function(err, results) {

                            req.body.data.attributes.total_files_number = results.file;
                            req.body.data.attributes.total_contacts_number = results.contact;
                            req.body.data.attributes.total_payments = results.payment;

                            finds.find_product(req, function(err, response) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'product',
                                    'update',
                                    req.user.id,
                                    JSON.stringify(req.body.data.attributes),
                                    req.params.id,
                                    'Modificato prodotto ' + req.params.id
                                );

                                utility.save_socket(req.headers['host'].split(".")[0], 'product', 'update', req.user.id, req.params.id, response.data);
                
                                res.json(response);
                            });
                        });
                    }).catch(function(err) {
                        return res.status(500).send(err);
                    });

            }).catch(function(err) {
                return res.status(500).send(err);
            });
    });
};

exports.update_all = function(req, res) {
    var util = require('util');

    if(!util.isArray(req.body)){
        sequelize.query('SELECT update_product_all_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\', ' +
                req.user.id + ',\'' +
                JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if(datas && datas[0]['update_product_all_v1'] == 0)
                {
                    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                }
                else if (datas && datas[0]['update_product_all_v1']) {
                    //res.status(200).send({'data':datas[0]['update_contact_all']});
                    req.params.id = datas[0]['update_product_all_v1'];
                    finds.find_product(req, function(err, results) {
                        if (err) {
                            return res.status(err.errors[0].status).send(err);
                        } else {
                            utility.save_socket(req.headers['host'].split(".")[0], 'product', 'update', req.user.id, req.params.id, results.data);
                        
                            res.json(results);
                        }
                    });
                } else {
                    res.status(422).send({});
                }
            }).catch(function(err) {
                return res.status(500).send({});
            });
    }else{
        sequelize.query('SELECT update_product_all_array(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_product_all_array'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_product_all_array']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});

                res.status(200).send({});
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
    }
};


/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Delete an existing product searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.product_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }



    sequelize.query('SELECT delete_product_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(!datas[0].delete_product_v1){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }else{

                utility.save_socket(req.headers['host'].split(".")[0], 'product', 'delete', req.user.id, req.params.id, null);
                        
                res.json({
                    'data': {
                        'type': 'products',
                        'id': datas[0].delete_product_v1
                    }
                });
            }

        }).catch(function(err) {
            return res.status(400).render('product_destroy: ' + err);
        });
};


exports.product_sales_charts = function(req, res) {
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    sequelize.query('SELECT product_sales_chart_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            (req.query.month ? req.query.month : null) + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].product_sales_chart_v1[0] && datas[0].product_sales_chart_v1[0].Prodotti) {
             
                    res.status(200).send(
                        {'data':datas[0].product_sales_chart_v1[0].Prodotti}
                    );
                   
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('product_sales_chart_v1: ' + err);
        });
};