var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    config = require('../../../config'),
    logs = require('../../utility/logs.js');
var util = require('util');
var mandrill = require('mandrill-api/mandrill');
var export_datas = require('../../utility/exports.js');


exports.export_payment_contabilita = function(req, res) {
    var json2csv = require('json2csv');
    
    export_datas.poppix_payment(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('poppix_export_payment_expense: ' + err);
        }

        json2csv({
            data: response.data,
            fields: response.fields,
            fieldNames: response.field_names
        }, function(err, csv) {
            if (err) {
                return res.status(500).send(err);
            }
            res.writeHead(200, {
                'Content-Type': 'text/csv',
                'Content-Disposition': 'attachment; filename=esportazione.csv'
            });
            res.end(csv);
        });
    });
};

exports.dashboard_poppix = function(req, res) {
    
    var start_date = null,
        end_date = null,
        q1 = [],
        type = null,
        paganti = null,
        page_num = 1;
        page_count = 50,
        iscritti = null,
        scadenza = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    if (req.query.type !== undefined && req.query.type !== '') {
        
        type = '\'' + req.query.type + '\'';
    }

    if (req.query.paganti !== undefined && req.query.paganti !== '') {
        
        paganti = '\'' + req.query.paganti + '\'';
    }

    if (req.query.pag !== undefined && req.query.pag !== '') {
        if (utility.check_id(req.query.pag)) {
            callback_(null, utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_num = req.query.pag;
    }

    if (req.query.xpag !== undefined && req.query.xpag !== '') {
        if (utility.check_id(req.query.xpag)) {
            callback_(null, utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_count = req.query.xpag;
    }


    if (req.query.iscritti !== undefined && req.query.iscritti !== '') {
       
        iscritti = '\'' + req.query.iscritti + '\'';
    }

    if (req.query.scadenza !== undefined && req.query.scadenza !== '') {
       
        scadenza = '\'' + req.query.scadenza + '\'';
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            callback_(null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }
    }

    sequelize.query('SELECT dashboard_poppix('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            iscritti +','+
            scadenza +','+
            req.user.id + ',' +
            req.user.role_id + ','+
            type +','+
            paganti +','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) + ',' +
            page_count+','+
            page_num+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].dashboard_poppix) {
                return res.status(200).send(datas[0].dashboard_poppix);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('dashboard_poppix: ' + err);
        });
};

exports.todo_list = function(req, res) {
    var from_date = null,
        to_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT poppixsrl_todo_scadenze(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        from_date + ',' +
        to_date + ',' +
        req.user.id + ',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send({ 'data': datas[0].poppixsrl_todo_scadenze[0] });
    }).catch(function(err) {
        return res.status(400).render('poppixsrl_todo_scadenze: ' + err);
    });
};

exports.licenses_expire = function(req, res) {
    var from_date = null,
        to_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT poppixsrl_firma_scadenze(' +
        from_date + ',' +
        to_date + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send({ 'data': datas[0].poppixsrl_firma_scadenze[0] });
    }).catch(function(err) {
        return res.status(400).render('poppixsrl_firma_scadenze: ' + err);
    });
};

exports.trust = function(req, res) {
    var from_date = null,
        to_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT poppixsrl_trust(' +
        from_date + ',' +
        to_date + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send({ 'data': datas[0].poppixsrl_trust[0] });
    }).catch(function(err) {
        return res.status(400).render('poppixsrl_trust: ' + err);
    });
};


exports.our_invoices = function(req, res) {
    var from_date = null,
        to_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

     sequelize.query('SELECT poppixsrl_get_user( '+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', 
        {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
            if (!datas[0].poppixsrl_get_user || !datas[0].poppixsrl_get_user) {
                return res.status(404).send(utility.error422('organization', 'Invalid attribute', 'Nessun attributo trovato contatta l\'amministratore', '422'));

            } else {

                 sequelize.query('SELECT poppixsrl_our_invoices(' +
                    datas[0].poppixsrl_get_user +','+
                    from_date + ',' +
                    to_date + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    }).then(function(datas) {
                    res.status(200).send({ 'data': datas[0].poppixsrl_our_invoices[0] });
                }).catch(function(err) {
                    return res.status(400).render('poppixsrl_our_invoices: ' + err);
                });

                
            }
    }).catch(function(err) {
        return res.status(400).render('our_invoices: ' + err);
    });
};

exports.poppix_beebeesign_errors = function(req, res) {
    var from_date = null,
        to_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT poppix_beebeesign_errors(' +
        from_date + ',' +
        to_date + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send({ 'data': datas[0].poppix_beebeesign_errors[0] });
    }).catch(function(err) {
        return res.status(400).render('poppix_beebeesign_errors: ' + err);
    });
};


exports.update_trust = function(req, res) {
     sequelize.query('SELECT poppix_update_trust('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].poppix_update_trust) {
                return res.status(200).send(datas[0].poppix_update_trust);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('poppix_update_trust: ' + err);
        });
};

exports.trust_activation_mail = function(req, res) {
    let dati = req.body;
                

    var template_content = [ 
            {
                "name": "NAME",
                "content": (dati.last_name ?  dati.last_name : '') + ' ' +(dati.first_name ?  dati.first_name : ''),
            }, {
                "name": "REGMAIL",
                "content": dati.mail
            }
        ],
        message = {
            "global_merge_vars": template_content,
            "metadata": {
                "organization": 'poppixsrl'
            },
            "subject": 'Beebeeboard - Attivazione Fatturazione Elettronica',
            "from_email": "no-reply@beebeemailer.com",
            "from_name": 'Beebeeboard',
            "to": [{
                "email":  dati.mail,
                "name":  (dati.last_name ?  dati.last_name : '') + ' ' +(dati.first_name ?  dati.first_name : ''),
                "type": "to"
            }],
            "headers": {
                "Reply-To": 'info@beebeeboard.com'
            }/*,
            "attachments": [{
                'type': 'text/calendar',
                'name': 'Appuntamento.ics',
                'content': resul.calendar_links.ical_file
            }]*/,
            "tags": ["attivazione-sdi", dati.organization],
            "subaccount": "beebeeboard",
            "tracking_domain": "mandrillapp.com",
            "google_analytics_domains": ["poppix.it"]
        };

    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

    mandrill_client.messages.sendTemplate({
        "template_name": "attivazione-sdi",
        "template_content": template_content,
        "message": message,
        "async": true,
        "send_at": false
    }, function(result) {

        if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
            console.log('Mail attivazione Sdi inviata');
            
            sequelize.query('SELECT insert_mandrill_send(' +
                    req.user._tenant_id+ ',\'' +
                    req.headers['host'].split(".")[0] + '\',null,null,\'' +
                    result[0].status + '\',null,\'' +
                    result[0]._id + '\',\'' + dati.organization + ' - Attivazione Sdi\',\'' +
                    dati.mail.replace(/'/g, "''''") + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                    return res.status(400).render('poppix_update_trust: ' + err);

                }).catch(function(err) {
                    return res.status(400).render('mandrill send mail: ' + err);
                   
                });

              
        } else {
            sequelize.query('SELECT insert_mandrill_send(' +
                    req.user._tenant_id+ ',\'' +
                    req.headers['host'].split(".")[0] + '\',null,null,\'' +
                    result[0].status + '\',\'' +
                    result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + dati.organization + ' - Attivazione Sdi\',\'' +
                    dati.mail.replace(/'/g, "''''") + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                    return res.status(400).render('poppix_update_trust: ' + err);

                }).catch(function(err) {
                    return res.status(400).render('mandrill send mail: ' + err);
                });
        }
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
};

exports.set_new_trust = function(req, res) {
     sequelize.query('SELECT poppix_set_new_trust('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].poppix_set_new_trust) {
             

                return res.status(200).send(datas[0].poppix_set_new_trust);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('poppix_set_new_trust: ' + err);
        });
};

exports.new_trust = function(req, res) {
     sequelize.query('SELECT insert_new_trust('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].insert_new_trust) {
                return res.status(200).send(datas[0].insert_new_trust);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('insert_new_trust: ' + err);
        });
};


exports.poppix_update_beebeesign_licenses = function(req, res) {
     sequelize.query('SELECT poppix_update_beebeesign_licenses('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].poppix_update_beebeesign_licenses) {
                return res.status(200).send(datas[0].poppix_update_beebeesign_licenses);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('poppix_update_beebeesign_licenses: ' + err);
        });
};

exports.modify_organization = function(req, res) {

    sequelize.query('SELECT poppix_modify_organization('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].poppix_modify_organization) {
                return res.status(200).send(datas[0].poppix_modify_organization);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('poppix_modify_organization: ' + err);
        });
};

exports.update_organization_new = function(req, res) {

    sequelize.query('SELECT update_organization_new('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            null + ',' +
            null + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas[0].update_organization_new) {
                return res.status(200).send(datas[0].update_organization_new);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('update_organization_new: ' + err);
        });
};

exports.update_organization_single = function(req, res) {

    sequelize.query('SELECT update_organization_new('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            (req.body.organization ? req.body.organization : '') + '\',' +
            null + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas[0].update_organization_new) {
                return res.status(200).send(datas[0].update_organization_new);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('update_organization_new: ' + err);
        });
};

exports.upload_certificato_fea = function(req, res) {
    
    sequelize.query('SELECT upload_organization_certificato_fea('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            (req.body.organization ? req.body.organization : '') + '\',\'' +
            (req.body.firma_grafometrica_nome ? req.body.firma_grafometrica_nome : '') + '\',\'' +
            (req.body.firma_grafometrica_via ? req.body.firma_grafometrica_via : '') + '\',\'' +
            (req.body.firma_grafometrica_cap ? req.body.firma_grafometrica_cap : '') + '\',\'' +
            (req.body.firma_grafometrica_city ? req.body.firma_grafometrica_city : '') + '\',' +
            (req.body.firma_grafometrica_versione ? req.body.firma_grafometrica_versione : null) + ',\'' +
            (req.body.firma_grafometrica_residence_prov ? req.body.firma_grafometrica_residence_prov : '') + '\',\'' +
            (req.body.firma_grafometrica_mail ? req.body.firma_grafometrica_mail : '') + '\',\'' +
            (req.body.firma_grafometrica_pec ? req.body.firma_grafometrica_pec : '') + '\',\'' +
            (req.body.firma_grafometrica_telephone ? req.body.firma_grafometrica_telephone : '') + '\',\'' +
            (req.body.firma_grafometrica_file ? req.body.firma_grafometrica_file : '') + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas[0].upload_organization_certificato_fea) {
                return res.status(200).send(datas[0].upload_organization_certificato_fea);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('upload_organization_certificato_fea: ' + err);
        });
};

exports.create_school = function(req, res) {
    const { Route53Client, ChangeResourceRecordSetsCommand } = require('@aws-sdk/client-route-53');

    if (!req.body.organization || !req.body.organization) {
        return res.status(422).send(utility.error422('organization', 'Richiesta non valida', 'Richiesta non valida'));
    }

    if (!req.body.organization ||
        req.body.organization == undefined ||
        req.body.organization == '' ||
        !utility.check_type_variable(req.body.organization, 'string')) {
        return res.status(422).send(utility.error422('organization', 'Invalid attribute', 'Il campo company non può essere vuoto', '422'));

    } else {
        req.body.organization = req.body.organization.toLowerCase().replace('.', '_').replace(/[^A-Z0-9]+/ig, '');
    }


    sequelize.query('SELECT search_organization(\'' +
        req.body.organization + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (datas[0].search_organization && datas[0].search_organization.id) {
            return res.status(422).send(utility.error422('organization', 'Invalid attribute', 'Company già esistente', '422'));

        } else {

            
            sequelize.query('SELECT beebeeskischool_crea(\'' +
                 req.body.organization + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
                .then(function(datas) {

                    if (datas[0].beebeeskischool_crea && datas[0].beebeeskischool_crea != null) {
                        sequelize.query('SELECT search_organization(\'' +
                            req.body.organization + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(org) {

                            

                                async.parallel({
                                    user: function(callback) {
                                        
                                        sequelize.query('SELECT insert_poppix_client(44975,\''+req.body.organization+'\',\'info@poppix.it\',null);', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(new_poppix_user) {});
                                       
                                        /**
                                         * Create RecordSet in AWS Amazon S53 for subdomain: ex: poppix.beebeeboard.com
                                         */
                                        const route53 = new Route53Client({
                                            accessKeyId: config.aws_route53.accessKeyId,
                                            secretAccessKey: config.aws_route53.secretAccessKey,
                                            region: config.aws_route53.region
                                        });

                                                    

                                        var params = {
                                            "HostedZoneId": config.aws_route53.hostedZoneId, // our Id from the first call
                                            "ChangeBatch": {
                                                "Changes": [{
                                                    "Action": "CREATE",
                                                    "ResourceRecordSet": {
                                                        "Name": req.body.organization + ".beebeeboard.com",
                                                        "Type": "CNAME",
                                                        "TTL": 300,
                                                        "ResourceRecords": [{
                                                            "Value": 'appv5.beebeeboard.com'
                                                        }],
                                                        
                                                    }
                                                }]
                                            }
                                        };
                                        const command = new ChangeResourceRecordSetsCommand(params);
                                        route53.send(command, function(err, data) {
                                        //route53.changeResourceRecordSets(params, function(err, data) {
                                            if (err) {
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Create Account -> Route53 Amazon create RecordSet Error:  ' + err);
                                                return res.status(400).render('user_insert_route53_fail: ' + err);
                                            }

                                            callback(null, 1);
                                        });
                                                
                                    }
                                }, function(err, results) {
                                    if (err) {
                                        return res.status(400).render('user_insert: ' + err);
                                    }

                                    res.json({
                                        'data': req.body
                                    });
                                });
                        }).catch(function(err) {
                            return res.status(400).render('user_insert: ' + err);
                        });

                    }
                }).catch(function(err) {
                    return res.status(400).render('user_insert: ' + err);
                });
            

        }
    }).catch(function(err) {
        return res.status(400).render('user_insert: ' + err);
    });
};


exports.urgence_token = function(req, res){
    var organization = req.headers['host'].split(".")[0];

    var send_organization = req.body.organization;

    if(organization == 'poppixsrl'){
        
        sequelize.query('SELECT get_urgence_token(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (req.body.organization ? '\''+req.body.organization+'\'' : null) + ','+
            (req.body.user_id ? req.body.user_id : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({'url':datas[0].get_urgence_token});
        }).catch(function(err) {
            return res.status(400).render('status error: ' + err);
        });
    }
};  

exports.organization_users = function(req, res){
    var organization = req.headers['host'].split(".")[0];

    var send_organization = req.query.organization;

    if(organization == 'poppixsrl'){
        
        sequelize.query('SELECT get_organization_users(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (req.query.organization ? '\''+req.query.organization+'\'' : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            res.status(200).send(datas[0].get_organization_users[0]);
        }).catch(function(err) {
            return res.status(400).render('status error: ' + err);
        });
    }

    

};  