var moment = require('moment'),
    Sequelize = require('sequelize'),
    mandrill = require('mandrill-api/mandrill');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Read in mandrill a mail
 */
exports.read = function(req, res) {
    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);
    mandrill_client.messages.info({
        'id': req.params.id
    }, function(result) {
        res.send(result);
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
        res.send({
            'mails': 0
        });
    });
};

exports.write = function(req, res) {
    var util = require('util');
    var lrecipients = [];

    if (req.body.contact_id !== undefined && req.body.contact_id != '') {
        if (util.isArray(req.body.contact_id)) {
            lrecipients.push(req.body.contact_id);
        } else {
            lrecipients.push(req.body.contact_id);
        }
    }

    sequelize.query('SELECT insert_mandrill_send(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
            req.user.id + ',\'' +
            req.body.status + '\',null,\'' + req.body._id + '\',\'' + req.body.object.replace(/'/g, "''''") + '\',null);', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
            res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('mandrill send mail: ' + err);
        });
};

exports.webhooks = function(req, res) {

    if (Object.keys(req.body.mandrill_events).length === 0 && req.body.mandrill_events.constructor === Object) {
        res.send('OK');
        return true;
    }

    var payload = req.body.mandrill_events;
    var events = JSON.parse(payload);

    if(events){
        events.forEach(function(item) {
            if ((item.msg && item.msg.metadata && item.msg.metadata.organization && item.msg.metadata.organization != 'appv5' && item.msg.metadata.organization != 'appv4' && item.msg.metadata.organization != '')) {
                sequelize.query('SELECT insert_mandrill_v1(' +
                    (item._id ? '\'' + item._id + '\'' : '\'\'') + ',' +
                    (item.event ? '\'' + item.event + '\'' : '\'\'') + ',' +
                    (item.ts ? item.ts : null) + ',' +
                    (item.url ? '\'' + item.url.replace(/'/g, "''''") + '\'' : '\'\'') + ',' +
                    (item.ip ? '\'' + item.ip + '\'' : '\'\'') + ',' +
                    (item.user_agent ? '\'' + item.user_agent + '\'' : '\'\'') + ',' +
                    (item.location ? '\'' + JSON.stringify(item.location).replace(/'/g, "''''") + '\'' : null) + ',' +
                    (item.user_agent_parsed ? '\'' + JSON.stringify(item.user_agent_parsed).replace(/'/g, "''''") + '\'' : null) + ',' +
                    ((item.msg && item.msg.metadata && item.msg.metadata.organization && item.msg.metadata.organization != 'appv5' && item.msg.metadata.organization != 'appv4' && item.msg.metadata.organization != '') ? ('\'' + item.msg.metadata.organization + '\'') : '\'\'') + ','+
                    (item.msg ? '\'' + JSON.stringify(item.msg).replace(/'/g, "''''") + '\'' : null) + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    console.log('Mandrill webhook  ' + item._id);
                }).catch(function(err) {
                    return res.status(400).render('insert_mandrill: ' + err);
                });
            }

        });
    }

    res.send('OK');
};

exports.index = function(req, res) {


    sequelize.query('SELECT find_mandrill_v1(\'' +
            req.user._tenant_id + '\',\'' +
            req.user.organization + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            ((req.query.contact_id && req.query.contact_id != undefined) ? req.query.contact_id : null) + ',' +
            ((req.query.invoice_id && req.query.invoice_id != undefined) ? req.query.invoice_id : null) + ',' +
            ((req.query.expense_id && req.query.expense_id != undefined) ? req.query.expense_id : null) + ',' +
            (req.query.q ? ('\'%(' + req.query.q.replace(/'/g, "''''") + ')%\'') : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].find_mandrill_v1) {
                res.json({
                    'data': datas[0].find_mandrill_v1
                });
            } else {
                res.json({
                    'data': {}
                })
            }
        }).catch(function(err) {
            return res.status(400).render('find_mandrill: ' + err);
        });
};


exports.search_by_id = function(req, res) {


    sequelize.query('SELECT find_mandrill_by_id_new(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            ((req.query.order_id && req.query.order_id != undefined) ? '\''+req.query.order_id+'\'' : null) + ',' +
            ((req.query.contact_id && req.query.contact_id != undefined) ? req.query.contact_id : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].find_mandrill_by_id_new) {
                res.json({
                    'data': datas[0].find_mandrill_by_id_new[0]
                });
            } else {
                res.json({
                    'data': {}
                })
            }
        }).catch(function(err) {
            return res.status(400).render('find_mandrill_by_id_new: ' + err);
        });
};

exports.invoice_mandrill = function(req, res) {
    var contacts = [],
        invoice_id = req.body.invoice_id,
        mail_id = req.body.mail_id,
        object = req.body.object;

    contacts.push(req.body.contact_id);

    sequelize.query('SELECT insert_mandrill_invoice_send(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
            req.user.id + ',\'' +
            mail_id + '\',\'' +
            object + '\',' +
            invoice_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
            return res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('mandrill send mail: ' + err);
        });
};


exports.project_mandrill = function(req, res) {
    var contacts = [],
        project_id = req.body.project_id,
        mail_id = req.body.mail_id,
        object = req.body.object;

    contacts.push(req.body.contact_id);

    sequelize.query('SELECT insert_mandrill_project_send(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
            req.user.id + ',\'' +
            mail_id + '\',\'' +
            object + '\',' +
            project_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
            return res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('mandrill send mail: ' + err);
        });
};