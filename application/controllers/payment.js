var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    export_datas = require('../utility/exports.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js');


exports.export_prima_nota = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    
    export_datas.prima_nota(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('prima_nota_export: ' + err);
        }

        json2csv({
            data: response.attributes,
            fields: response.attributes[0].fields,
            fieldNames: response.attributes[0].field_names
        }, function(err, csv) {
            if (err) {
                return res.status(500).send(err);
            }
            res.writeHead(200, {
                'Content-Type': 'text/csv',
                'Content-Disposition': 'attachment; filename=prima_nota.csv'
            });
            res.end(csv);
        });
    });
};

exports.export_ritenute = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    
    export_datas.ritenute(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('ritenute_export: ' + err);
        }

        json2csv({
            data: response.data,
            fields: response.fields,
            fieldNames: response.field_names
        }, function(err, csv) {
            if (err) {
                return res.status(500).send(err);
            }
            res.writeHead(200, {
                'Content-Type': 'text/csv',
                'Content-Disposition': 'attachment; filename=ritenute.csv'
            });
            res.end(csv);
        });
    });
};

exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    indexes.payment(req, null, function(err, index) {
        if (err) {
            return res.status(400).render('payment_index: ' + err);
        }
        /*if (index.meta.total_items > 5000) {
            return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
        } else {*/
            export_datas.payment(req, null, function(err, response) {
                if (err) {
                    return res.status(400).render('payment_export: ' + err);
                }

                json2csv({
                    data: response.payments,
                    fields: response.fields,
                    fieldNames: response.field_names
                }, function(err, csv) {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=payments.csv'
                    });
                    res.end(csv);
                });
            });
       // }
    });
};

exports.stats = function(req, res) {
    utility.get_parameters_v1(req, 'payment', function(err, parameters) {
        if (err) {
            return res.status(400).render('contact_states_index: ' + err);
        }

        sequelize.query('SELECT payment_stats(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.success + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ','+
                parameters.voucher+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                res.json({
                    'meta': datas[0].payment_stats
                });
            }).catch(function(err) {
                return res.status(400).render('payment_stats: ' + err);
            });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    indexes.payment(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('payment_index: ' + err);
        }
        res.json(response);
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_payment(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var payment = {};

    if (req.body.data.attributes !== undefined) {
        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        payment['owner_id'] = req.user.id;
        payment['date'] = (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : null);
        payment['status'] = (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') ? req.body.data.attributes.status : null);
        payment['total'] = req.body.data.attributes.total;
        payment['payment_method'] = (req.body.data.attributes.payment_method && utility.check_type_variable(req.body.data.attributes.payment_method, 'string') && req.body.data.attributes.payment_method !== null ? req.body.data.attributes.payment_method.replace(/'/g, "''''") : '');
        payment['_tenant_id'] = req.user._tenant_id;
        payment['organization'] = req.headers['host'].split(".")[0];
        //payment['invoice_id'] = ((req.body.data.relationships && req.body.data.relationships.invoice && req.body.data.relationships.invoice.data && req.body.data.relationships.invoice.data.id && !utility.check_id(req.body.data.relationships.invoice.data.id)) ? req.body.data.relationships.invoice.data.id : null);
        //payment['expense_id'] = ((req.body.data.relationships && req.body.data.relationships.expense && req.body.data.relationships.expense.data && req.body.data.relationships.expense.data.id) && !utility.check_id(req.body.data.relationships.expense.data.id) ? req.body.data.relationships.expense.data.id : null);
        payment['error'] = (req.body.data.attributes.error && utility.check_type_variable(req.body.data.attributes.error, 'string') && req.body.data.attributes.error !== null ? req.body.data.attributes.error.replace(/'/g, "''''") : '');
        payment['success'] = (req.body.data.attributes.success !== undefined && utility.check_type_variable(req.body.data.attributes.success, 'boolean') && req.body.data.attributes.success !== null ? req.body.data.attributes.success : false);
        payment['transaction_type'] = (req.body.data.attributes.transaction_type && utility.check_type_variable(req.body.data.attributes.transaction_type, 'string') && req.body.data.attributes.transaction_type !== null ? req.body.data.attributes.transaction_type.replace(/'/g, "''''") : '');
        payment['transaction_id'] = (req.body.data.attributes.transaction_id && utility.check_type_variable(req.body.data.attributes.transaction_id, 'string') && req.body.data.attributes.transaction_id !== null ? req.body.data.attributes.transaction_id.replace(/'/g, "''''") : '');
        payment['transaction_ref'] = (req.body.data.attributes.transaction_ref && utility.check_type_variable(req.body.data.attributes.transaction_ref, 'string') && req.body.data.attributes.transaction_ref !== null ? req.body.data.attributes.transaction_ref.replace(/'/g, "''''") : '');
        payment['currency'] = (req.body.data.attributes.currency && utility.check_type_variable(req.body.data.attributes.currency, 'string') && req.body.data.attributes.currency !== null ? req.body.data.attributes.currency.replace(/'/g, "''''") : '');
        payment['payment_id'] = (req.body.data.attributes.payment_id && utility.check_type_variable(req.body.data.attributes.payment_id, 'string') && req.body.data.attributes.payment_id !== null ? req.body.data.attributes.payment_id.replace(/'/g, "''''") : '');
        payment['enr_status'] = (req.body.data.attributes.enr_status && utility.check_type_variable(req.body.data.attributes.enr_status, 'string') && req.body.data.attributes.enr_status !== null ? req.body.data.attributes.enr_status.replace(/'/g, "''''") : '');
        payment['auth_status'] = (req.body.data.attributes.auth_status && utility.check_type_variable(req.body.data.attributes.auth_status, 'string') && req.body.data.attributes.auth_status !== null ? req.body.data.attributes.auth_status.replace(/'/g, "''''") : '');
        payment['tran_bank_id'] = (req.body.data.attributes.tran_bank_id && utility.check_type_variable(req.body.data.attributes.tran_bank_id, 'string') && req.body.data.attributes.tran_bank_id !== null ? req.body.data.attributes.tran_bank_id.replace(/'/g, "''''") : '');
        payment['sent'] = (req.body.data.attributes.sent !== undefined && utility.check_type_variable(req.body.data.attributes.sent, 'boolean') ? req.body.data.attributes.sent : false);
        payment['mongo_id'] = null;
        payment['residuo'] = (req.body.data.attributes.total ? req.body.data.attributes.total : 0.0);
        payment['archivied'] = req.body.data.attributes.archivied;
        payment['data_caparra'] = (req.body.data.attributes.data_caparra && moment(req.body.data.attributes.data_caparra).isValid() ? req.body.data.attributes.data_caparra : null);
        payment['data_valuta'] = (req.body.data.attributes.data_valuta && moment(req.body.data.attributes.data_valuta).isValid() ? req.body.data.attributes.data_valuta : null);
        payment['notes'] = (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
        payment['assurance_id'] = ((req.body.data.relationships && req.body.data.relationships.assurance && req.body.data.relationships.assurance.data && req.body.data.relationships.assurance.data.id && !utility.check_id(req.body.data.relationships.assurance.data.id)) ? req.body.data.relationships.assurance.data.id : null);
        

        sequelize.query('SELECT insert_payment_v6(\'' + JSON.stringify(payment) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_payment_v6;

                utility.sanitizeRelations({
                    'organization': req.headers['host'].split(".")[0],
                    '_tenant': req.user._tenant_id,
                    'new_id': datas[0].insert_payment_v6,
                    'body': req.body.data.relationships,
                    'model': 'payment',
                    'user_id': req.user.id,
                    'array': ['file', 'contact', 'product', 'project', 'event', 'expense', 'invoice']
                }, function(err, results) {

                    req.params.id = datas[0].insert_payment_v6;
                    finds.find_payment(req, function(err, response) {
                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'payment',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_payment_v6,
                            'Aggiunto pagamento ' + datas[0].insert_payment_v6
                        );
                        utility.save_socket(req.headers['host'].split(".")[0], 'payment', 'insert', req.user.id, datas[0].insert_payment_v6, response.data);
                
                        res.json(response);
                    });

                });

            }).catch(function(err) {
                return res.status(400).render('payment_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        finds.find_payment(req, function(err, old_payment) {

            sequelize.query('SELECT update_payment_v7(' +
                    req.params.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',\'' +
                    (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : '') + '\',\'' +
                    (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') ? req.body.data.attributes.status : '') + '\',' +
                    req.body.data.attributes.total + ',\'' +
                    (req.body.data.attributes.payment_method && utility.check_type_variable(req.body.data.attributes.payment_method, 'string') && req.body.data.attributes.payment_method !== null ? req.body.data.attributes.payment_method.replace(/'/g, "''''") : '') + '\',\'' +
                    //((req.body.data.relationships && req.body.data.relationships.invoice && req.body.data.relationships.invoice.data && req.body.data.relationships.invoice.data.id && !utility.check_id(req.body.data.relationships.invoice.data.id)) ? req.body.data.relationships.invoice.data.id : null) + ',' + 
                    //((req.body.data.relationships && req.body.data.relationships.expense && req.body.data.relationships.expense.data && req.body.data.relationships.expense.data.id && !utility.check_id(req.body.data.relationships.expense.data.id)) ? req.body.data.relationships.expense.data.id : null) + ',\'' + 
                    (req.body.data.attributes.error && utility.check_type_variable(req.body.data.attributes.error, 'string') && req.body.data.attributes.error !== null ? req.body.data.attributes.error.replace(/'/g, "''''") : '') + '\',' +
                    (req.body.data.attributes.success !== undefined && utility.check_type_variable(req.body.data.attributes.success, 'boolean') && req.body.data.attributes.success !== null ? req.body.data.attributes.success : false) + ',\'' +
                    (req.body.data.attributes.transaction_type && utility.check_type_variable(req.body.data.attributes.transaction_type, 'string') && req.body.data.attributes.transaction_type !== null ? req.body.data.attributes.transaction_type.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.transaction_id && utility.check_type_variable(req.body.data.attributes.transaction_id, 'string') && req.body.data.attributes.transaction_id !== null ? req.body.data.attributes.transaction_id.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.transaction_ref && utility.check_type_variable(req.body.data.attributes.transaction_ref, 'string') && req.body.data.attributes.transaction_ref !== null ? req.body.data.attributes.transaction_ref.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.currency && utility.check_type_variable(req.body.data.attributes.currency, 'string') && req.body.data.attributes.currency !== null ? req.body.data.attributes.currency.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.payment_id && utility.check_type_variable(req.body.data.attributes.payment_id, 'string') && req.body.data.attributes.payment_id !== null ? req.body.data.attributes.payment_id.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.enr_status && utility.check_type_variable(req.body.data.attributes.enr_status, 'string') && req.body.data.attributes.enr_status !== null ? req.body.data.attributes.enr_status.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.auth_status && utility.check_type_variable(req.body.data.attributes.auth_status, 'string') && req.body.data.attributes.auth_status !== null ? req.body.data.attributes.auth_status.replace(/'/g, "''''") : '') + '\'' + ',\'' +
                    (req.body.data.attributes.tran_bank_id && utility.check_type_variable(req.body.data.attributes.tran_bank_id, 'string') && req.body.data.attributes.tran_bank_id !== null ? req.body.data.attributes.tran_bank_id.replace(/'/g, "''''") : '') + '\',' +
                    req.body.data.attributes.sent + ',' +
                    req.user.role_id + ',' +
                    req.user.id + ',' +
                    (req.body.data.attributes.residuo ? req.body.data.attributes.residuo : 0.0) + ',' +
                    req.body.data.attributes.archivied + ',' +
                    (req.body.data.attributes.verify !== undefined && utility.check_type_variable(req.body.data.attributes.verify, 'boolean') && req.body.data.attributes.verify !== null ? req.body.data.attributes.verify : false) + ',\''+
                    (req.body.data.attributes.data_caparra && moment(req.body.data.attributes.data_caparra).isValid() ? req.body.data.attributes.data_caparra : '') + '\',\''+
                    (req.body.data.attributes.data_valuta && moment(req.body.data.attributes.data_valuta).isValid() ? req.body.data.attributes.data_valuta : '') + '\',\'' +
                    (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(pay) {

                    if (!pay[0].update_payment_v7) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }

                    var old = old_payment;

                    sequelize.query('SELECT delete_payment_relations_v2(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                        .then(function(datas) {

                            utility.sanitizeRelations({
                                'organization': req.headers['host'].split(".")[0],
                                '_tenant': req.user._tenant_id,
                                'new_id': req.params.id,
                                'body': req.body.data.relationships,
                                'model': 'payment',
                                'user_id': req.user.id,
                                'array': ['file']
                            }, function(err, results) {

                                finds.find_payment(req, function(err, response) {
                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'payment',
                                        'update',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        req.params.id,
                                        'Modificato pagamento ' + req.params.id
                                    );
                                    utility.save_socket(req.headers['host'].split(".")[0], 'payment', 'update', req.user.id, req.params.id, response.data);
                
                                    res.json(response);
                                });

                            });
                        }).catch(function(err) {
                            return res.status(400).render('payment_update: ' + err);
                        });

                }).catch(function(err) {
                    return res.status(400).render('payment_update: ' + err);
                });
        });

    } else {
        res.json({
            'data': []
        });
    }
};

exports.update_all = function(req, res) {
    
    sequelize.query('SELECT update_payment_all(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_payment_all'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_payment_all']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_payment_all'];
                finds.find_payment(req, function(err, results) {
                    if (err) {
                        return res.status(err.errors[0].status).send(err);
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'payment', 'update', req.user.id, req.params.id, results.data);
                
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT delete_payment_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(!datas[0].delete_payment_v1 || datas[0].delete_payment_v1 == 0){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }else{
                utility.save_socket(req.headers['host'].split(".")[0], 'payment', 'delete', req.user.id, req.params.id, null);
                        
                res.json({
                    'data': {
                        'type': 'payments',
                        'id': datas[0].delete_payment_v1
                    }
                });
            }

        }).catch(function(err) {
            return res.status(400).render('payment_destroy: ' + err);
        });
};

exports.sanitize_event_payment = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        payment_id = req.body.payment_id,
        invoice_ids = [],
        expense_ids = [],
        event_ids = [];

    if (req.body.event_id !== undefined && req.body.event_id != '') {
        if (util.isArray(req.body.event_id)) {
            event_ids = req.body.event_id;
        } else {
            event_ids.push(req.body.event_id);
        }
    }

    if (req.body.expense_id !== undefined && req.body.expense_id != '') {
        if (util.isArray(req.body.expense_id)) {
            expense_ids = req.body.expense_id;
        } else {
            expense_ids.push(req.body.expense_id);
        }
    }

    if (req.body.invoice_id !== undefined && req.body.invoice_id != '') {
        if (util.isArray(req.body.invoice_id)) {
            invoice_ids = req.body.invoice_id;
        } else {
            invoice_ids.push(req.body.invoice_id);
        }
    }

    sequelize.query('SELECT sanitize_event_payment_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payment_id + ',' +
            (event_ids.length > 0 ? 'ARRAY[' + event_ids + ']::bigint[]' : null) + ',' +
            (invoice_ids.length > 0 ? 'ARRAY[' + invoice_ids + ']::bigint[]' : null) + ',' +
            (expense_ids.length > 0 ? 'ARRAY[' + expense_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});
        }).catch(function(err) {
            return res.status(400).render('sanitize_event_payment_v2: ' + err);
        });
};

exports.use_contact_credit = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        payer_id = req.body.contact_id,
        invoice_ids = [],
        event_ids = [];

    if (req.body.event_id !== undefined && req.body.event_id != '') {
        if (util.isArray(req.body.event_id)) {
            event_ids = req.body.event_id;
        } else {
            event_ids.push(req.body.event_id);
        }
    }


    if (req.body.invoice_id !== undefined && req.body.invoice_id != '') {
        if (util.isArray(req.body.invoice_id)) {
            invoice_ids = req.body.invoice_id;
        } else {
            invoice_ids.push(req.body.invoice_id);
        }
    }

    sequelize.query('SELECT use_contact_credit(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payer_id + ',' +
            (event_ids.length > 0 ? 'ARRAY[' + event_ids + ']::bigint[]' : null) + ',' +
            (invoice_ids.length > 0 ? 'ARRAY[' + invoice_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});
        }).catch(function(err) {
            return res.status(400).render('use_contact_credit: ' + err);
        });
};

exports.sblocca_pagamento = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        payment_id = req.body.payment_id;

    sequelize.query('SELECT sblocca_pagamento(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payment_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});
        }).catch(function(err) {
            return res.status(400).render('sblocca_pagamento: ' + err);
        });
};

exports.payment_request_success = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        payment_id = req.query.id;

    sequelize.query('SELECT update_payment_request_success(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payment_id + ',\''+
            req.query.date+'\','+
            req.query.success+',\''+
            req.query.payment_method+'\',\''+
            req.query.payment_id+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).json({});
        }).catch(function(err) {
            return res.status(400).render('update_payment_request_success: ' + err);
        });
};

exports.prima_nota = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        conto = null,
        start_date = '',
        end_date = '',
        contact_id = null,
        order_by = null;

    if (req.query.contact_id != null && req.query.contact_id != '' && req.query.contact_id != undefined) {
        contact_id = req.query.contact_id;
    }

    if (req.query.conto != null && req.query.conto != '') {
        conto = '\'' + req.query.conto + '\'';
    }

    if (req.query.order_by != null && req.query.order_by != '') {
        order_by = '\'' + req.query.order_by + '\'';
    }

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    sequelize.query('SELECT get_prima_nota_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id+ ','+
            start_date+','+
            end_date+','+
            conto+','+
            contact_id+','+
            order_by+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].get_prima_nota_v1){
                res.status(200).json(datas[0].get_prima_nota_v1);
            }
            else
            {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('get_prima_nota: ' + err);
        });
};

exports.get_ritenute = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        start_date = '',
        end_date = '',
        contact_ids = [];

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        if (util.isArray(req.query.contact_id)) {
            contact_ids = req.query.contact_id;
        } else {
            contact_ids.push(req.query.contact_id);
        }
    }

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    sequelize.query('SELECT get_ritenute_acconto(' +
            req.user._tenant_id + ',' +
            start_date+','+
            end_date+',\''+
            req.headers['host'].split(".")[0] + '\',' +
            (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].get_ritenute_acconto){
                res.status(200).json(datas[0].get_ritenute_acconto);
            }
            else
            {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('get_ritenute_acconto: ' + err);
        });
};


exports.get_bolli  = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        start_date = '',
        end_date = '',
        contact_ids = [];

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        if (util.isArray(req.query.contact_id)) {
            contact_ids = req.query.contact_id;
        } else {
            contact_ids.push(req.query.contact_id);
        }
    }

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    sequelize.query('SELECT get_bolli(' +
            req.user._tenant_id + ',' +
            start_date+','+
            end_date+',\''+
            req.headers['host'].split(".")[0] + '\',' +
            (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].get_bolli){
                res.status(200).json(datas[0].get_bolli);
            }
            else
            {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('get_bolli: ' + err);
        });
};

exports.salda_ritenute = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize');

    sequelize.query('SELECT salda_ritenute_acconto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].salda_ritenute_acconto){
                res.status(200).json({'data':datas[0].salda_ritenute_acconto});
            }
            else
            {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('salda_ritenute_acconto: ' + err);
        });
};


exports.save_blocco_pagamenti = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize');

    sequelize.query('SELECT save_blocco_pagamenti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].save_blocco_pagamenti){
                res.status(200).json({'data':datas[0].save_blocco_pagamenti});
            }
            else
            {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('save_blocco_pagamenti: ' + err);
        });
};