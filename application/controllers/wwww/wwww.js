var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');

exports.find_from_link = function(req, res) {
    var q = null;
    var util = require('util');

    if (req.query.q !== undefined && req.query.q != '') {
        q = req.query.q;
    }

    sequelize.query('SELECT wwww_find_project_from_link(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            q + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            if (data[0].wwww_find_project_from_link && data[0].wwww_find_project_from_link[0]) {
                res.json({
                    'data': data[0].wwww_find_project_from_link[0]
                });
            } else {
                res.json({
                    'data': {}
                });
            }
        }).catch(function(err) {
            return res.status(400).render('wwww_find_project_from_link: ' + err);
        });
};

exports.find_project = function(req, res) {
    var q = null;
    var util = require('util');

    if (req.query.q !== undefined && req.query.q != '') {
        q = req.query.q;
    }

    sequelize.query('SELECT wwww_find_project(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            q + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            if (data[0].wwww_find_project && data[0].wwww_find_project[0]) {
                res.json({
                    'data': data[0].wwww_find_project
                });
            } else {
                res.json({
                    'data': {}
                });
            }
        }).catch(function(err) {
            return res.status(400).render('wwww_find_project: ' + err);
        });
};

exports.find_product = function(req, res) {
    var q = null;
    var util = require('util');

    if (req.query.q !== undefined && req.query.q != '') {
        q = req.query.q;
    }

    sequelize.query('SELECT wwww_find_product(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            q + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            if (data[0].wwww_find_product) {
                res.json({
                    'data': data[0].wwww_find_product
                });
            } else {
                res.json({
                    'data': {}
                });
            }
        }).catch(function(err) {
            return res.status(400).render('wwww_find_product: ' + err);
        });
};