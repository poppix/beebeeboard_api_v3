var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.bulk_operation = function(req, res) {
    var util = require('util');
    var ids = [],
        delete_all_workhours = req.body.delete_all_workhours ? req.body.delete_all_workhours : null,
        delete_all_projects = req.body.delete_all_projects ? req.body.delete_all_projects : null,
        delete_all_events = req.body.delete_all_events ? req.body.delete_all_events : null,
        delete_all_products = req.body.delete_all_products ? req.body.delete_all_products : null,
        delete_all_expenses = req.body.delete_all_expenses ? req.body.delete_all_expenses : null,
        delete_all_invoices = req.body.delete_all_invoices ? req.body.delete_all_invoices : null,
        delete_all_contacts = req.body.delete_all_contacts ? req.body.delete_all_contacts : null,
        delete_all_payments = req.body.delete_all_payments ? req.body.delete_all_payments : null;

    if (req.body.ids !== undefined && req.body.ids != '') {
        if (util.isArray(req.body.ids)) {
            ids = req.body.ids.map(Number);
        } else {
            ids.push(req.body.ids);
        }
    }

    if(ids.length > 0 && req.body.model !== '' && req.body.model != undefined) {

        if(req.body.operation == 'archive' || req.body.operation == 'disarchive'){
            switch(req.body.model){
                case 'contacts':
                    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                 case 'products':
                    if (!req.user.role[0].json_build_object.attributes.product_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'projects':
                    if (!req.user.role[0].json_build_object.attributes.project_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'events':
                    if (!req.user.role[0].json_build_object.attributes.event_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'workhours':
                    if (!req.user.role[0].json_build_object.attributes.workhour_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'invoices':
                    if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'expenses':
                    if (!req.user.role[0].json_build_object.attributes.expense_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'payments':
                    if (!req.user.role[0].json_build_object.attributes.payment_edit) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                default:
                     return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                break;

            }
        }
        else if(req.body.operation == 'delete'){
            switch(req.body.model){
                case 'contacts':
                    if (!req.user.role[0].json_build_object.attributes.contact_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                 case 'products':
                    if (!req.user.role[0].json_build_object.attributes.product_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'projects':
                    if (!req.user.role[0].json_build_object.attributes.project_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'events':
                    if (!req.user.role[0].json_build_object.attributes.event_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'workhours':
                    if (!req.user.role[0].json_build_object.attributes.workhour_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'invoices':
                    if (!req.user.role[0].json_build_object.attributes.invoice_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'expenses':
                    if (!req.user.role[0].json_build_object.attributes.expense_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                case 'payments':
                    if (!req.user.role[0].json_build_object.attributes.payment_delete) {
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                    }
                break;

                default:
                     return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                break;

            }
        }


        if (req.body.operation == 'archive') {

            sequelize.query('SELECT archive_model_ids(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null) + ',\''+
                req.body.model+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
               res.status(200).json({});

            }).catch(function(err) {
                return res.status(400).render('archive_model_ids: ' + err);
            });
        } else if (req.body.operation == 'delete') {
             sequelize.query('SELECT delete_model_ids(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null) + ',\''+
                req.body.model+'\','+
                delete_all_workhours+','+
                delete_all_projects+','+
                delete_all_events+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
               res.status(200).json({});

            }).catch(function(err) {
                return res.status(400).render('delete_model_ids: ' + err);
            });

        } else if (req.body.operation == 'disarchive') {
             sequelize.query('SELECT disarchive_model_ids(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null) + ',\''+
                req.body.model+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
               res.status(200).json({});

            }).catch(function(err) {
                return res.status(400).render('disarchive_model_ids: ' + err);
            });
        } else {
            return res.status(422).send(utility.error422('operation', 'Parametro non valido', 'Parametro non valido', '422'));

        }
    }else {
        return res.status(422).send(utility.error422('ids', 'Parametro non valido', 'Parametro non valido', '422'));

    }
    
};
