var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    config = require('../../../config'),
    logs = require('../../utility/logs.js');
var util = require('util');


exports.rcb_get_fatture = function(req, res) {

    var util = require('util');

    var start_date = null,
        end_date = null;

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        start_date = '\'' + req.query.from_date + '\'';
    }

    

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
         end_date = '\'' + req.query.to_date + '\'';
    }


    console.log(req.query);

    sequelize.query('SELECT rcb_get_fatture(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+','+
            (req.query.user_id ? req.query.user_id : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].rcb_get_fatture && datas[0].rcb_get_fatture[0] && datas[0].rcb_get_fatture[0]) {
                res.json(datas[0].rcb_get_fatture[0].invoices);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('rcb_get_fatture: ' + err);
        });
};


exports.rcb_block_fatture = function(req, res) {

    var util = require('util');
    console.log(req.body);

    sequelize.query('SELECT rcb_block_fatture(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''")+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].rcb_block_fatture) {
                res.status(200).send({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('rcb_block_fatture: ' + err);
        });
};

