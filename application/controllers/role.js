var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js');

exports.index = function(req, res) {

    async.parallel({
        data: function(callback) {
            var response = {};
            sequelize.query('SELECT roles_data(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {

                    if (datas[0].roles_data && datas[0].roles_data != null) {
                        callback(null, datas[0].roles_data);

                    } else {

                        callback(null, []);
                    }

                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {

        if (err) {
            return res.status(400).render('roles_index: ' + err);
        }

        res.json({
            'data': results.data
        });
    });
};

exports.find = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            var response = {};
            sequelize.query('SELECT roles_find_data_v2(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.params.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {

                    if (datas[0].roles_find_data_v2 && datas[0].roles_find_data_v2 != null) {
                        callback(null, datas[0].roles_find_data_v2);

                    } else {

                        callback(null, []);
                    }

                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {

        if (err) {
            return res.status(400).render('roles_find_data: ' + err);
        }

        res.json({
            'data': results.data[0]
        });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var role = {};

    if (req.body.data.attributes !== undefined) {
        // Import case
        role['organization_id'] = req.user.organization_id;
        role['role_name'] = (req.body.data.attributes.role_name && utility.check_type_variable(req.body.data.attributes.role_name, 'string') && req.body.data.attributes.role_name !== null ? req.body.data.attributes.role_name.replace(/'/g, "''''") : '');
        role['_tenant_id'] = req.user._tenant_id;
        role['organization'] = req.headers['host'].split(".")[0];
        role['mongo_id'] = null;
        role['contact_view_all'] = req.body.data.attributes.contact_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_view_all, 'boolean') ? req.body.data.attributes.contact_view_all : false;
        role['contact_view_join'] = req.body.data.attributes.contact_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_view_join, 'boolean') ? req.body.data.attributes.contact_view_join : false;
        role['product_view_all'] = req.body.data.attributes.product_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_view_all, 'boolean') ? req.body.data.attributes.product_view_all : false;
        role['product_view_join'] = req.body.data.attributes.product_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_view_join, 'boolean') ? req.body.data.attributes.product_view_join : false;
        role['project_view_all'] = req.body.data.attributes.project_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_view_all, 'boolean') ? req.body.data.attributes.project_view_all : false;
        role['project_view_join'] = req.body.data.attributes.project_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_view_join, 'boolean') ? req.body.data.attributes.project_view_join : false;
        role['event_view_all'] = req.body.data.attributes.event_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_view_all, 'boolean') ? req.body.data.attributes.event_view_all : false;
        role['event_view_join'] = req.body.data.attributes.event_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_view_join, 'boolean') ? req.body.data.attributes.event_view_join : false;
        role['workhour_view_all'] = req.body.data.attributes.workhour_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_view_all, 'boolean') ? req.body.data.attributes.workhour_view_all : false;
        role['workhour_view_join'] = req.body.data.attributes.workhour_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_view_join, 'boolean') ? req.body.data.attributes.workhour_view_join : false;
        role['estimate_view_all'] = req.body.data.attributes.estimate_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_view_all, 'boolean') ? req.body.data.attributes.estimate_view_all : false;
        role['estimate_view_join'] = req.body.data.attributes.estimate_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_view_join, 'boolean') ? req.body.data.attributes.estimate_view_join : false;
        role['invoice_view_all'] = req.body.data.attributes.invoice_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_view_all, 'boolean') ? req.body.data.attributes.invoice_view_all : false;
        role['invoice_view_join'] = req.body.data.attributes.invoice_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_view_join, 'boolean') ? req.body.data.attributes.invoice_view_join : false;
        role['expense_view_all'] = req.body.data.attributes.expense_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_view_all, 'boolean') ? req.body.data.attributes.expense_view_all : false;
        role['expense_view_join'] = req.body.data.attributes.expense_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_view_join, 'boolean') ? req.body.data.attributes.expense_view_join : false;
        role['payment_view_all'] = req.body.data.attributes.payment_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_view_all, 'boolean') ? req.body.data.attributes.payment_view_all : false;
        role['payment_view_join'] = req.body.data.attributes.payment_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_view_join, 'boolean') ? req.body.data.attributes.payment_view_join : false;

        role['contact_list_all'] = req.body.data.attributes.contact_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_list_all, 'boolean') ? req.body.data.attributes.contact_list_all : false;
        role['contact_list_join'] = req.body.data.attributes.contact_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_list_join, 'boolean') ? req.body.data.attributes.contact_list_join : false;
        role['product_list_all'] = req.body.data.attributes.product_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_list_all, 'boolean') ? req.body.data.attributes.product_list_all : false;
        role['product_list_join'] = req.body.data.attributes.product_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_list_join, 'boolean') ? req.body.data.attributes.product_list_join : false;
        role['project_list_all'] = req.body.data.attributes.project_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_list_all, 'boolean') ? req.body.data.attributes.project_list_all : false;
        role['project_list_join'] = req.body.data.attributes.project_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_list_join, 'boolean') ? req.body.data.attributes.project_list_join : false;
        role['event_list_all'] = req.body.data.attributes.event_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_list_all, 'boolean') ? req.body.data.attributes.event_list_all : false;
        role['event_list_join'] = req.body.data.attributes.event_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_list_join, 'boolean') ? req.body.data.attributes.event_list_join : false;
        role['workhour_list_all'] = req.body.data.attributes.workhour_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_list_all, 'boolean') ? req.body.data.attributes.workhour_list_all : false;
        role['workhour_list_join'] = req.body.data.attributes.workhour_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_list_join, 'boolean') ? req.body.data.attributes.workhour_list_join : false;
        role['estimate_list_all'] = req.body.data.attributes.estimate_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_list_all, 'boolean') ? req.body.data.attributes.estimate_list_all : false;
        role['estimate_list_join'] = req.body.data.attributes.estimate_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_list_join, 'boolean') ? req.body.data.attributes.estimate_list_join : false;
        role['invoice_list_all'] = req.body.data.attributes.invoice_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_list_all, 'boolean') ? req.body.data.attributes.invoice_list_all : false;
        role['invoice_list_join'] = req.body.data.attributes.invoice_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_list_join, 'boolean') ? req.body.data.attributes.invoice_list_join : false;
        role['expense_list_all'] = req.body.data.attributes.expense_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_list_all, 'boolean') ? req.body.data.attributes.expense_list_all : false;
        role['expense_list_join'] = req.body.data.attributes.expense_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_list_join, 'boolean') ? req.body.data.attributes.expense_list_join : false;
        role['payment_list_all'] = req.body.data.attributes.payment_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_list_all, 'boolean') ? req.body.data.attributes.payment_list_all : false;
        role['payment_list_join'] = req.body.data.attributes.payment_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_list_join, 'boolean') ? req.body.data.attributes.payment_list_join : false;

        role['contact_delete_all'] = req.body.data.attributes.contact_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_delete_all, 'boolean') ? req.body.data.attributes.contact_delete_all : false;
        role['contact_delete_join'] = req.body.data.attributes.contact_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_delete_join, 'boolean') ? req.body.data.attributes.contact_delete_join : false;
        role['product_delete_all'] = req.body.data.attributes.product_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_delete_all, 'boolean') ? req.body.data.attributes.product_delete_all : false;
        role['product_delete_join'] = req.body.data.attributes.product_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_delete_join, 'boolean') ? req.body.data.attributes.product_delete_join : false;
        role['project_delete_all'] = req.body.data.attributes.project_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_delete_all, 'boolean') ? req.body.data.attributes.project_delete_all : false;
        role['project_delete_join'] = req.body.data.attributes.project_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_delete_join, 'boolean') ? req.body.data.attributes.project_delete_join : false;
        role['event_delete_all'] = req.body.data.attributes.event_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_delete_all, 'boolean') ? req.body.data.attributes.event_delete_all : false;
        role['event_delete_join'] = req.body.data.attributes.event_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_delete_join, 'boolean') ? req.body.data.attributes.event_delete_join : false;
        role['workhour_delete_all'] = req.body.data.attributes.workhour_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_delete_all, 'boolean') ? req.body.data.attributes.workhour_delete_all : false;
        role['workhour_delete_join'] = req.body.data.attributes.workhour_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_delete_join, 'boolean') ? req.body.data.attributes.workhour_delete_join : false;
        role['estimate_delete_all'] = req.body.data.attributes.estimate_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_delete_all, 'boolean') ? req.body.data.attributes.estimate_delete_all : false;
        role['estimate_delete_join'] = req.body.data.attributes.estimate_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_delete_join, 'boolean') ? req.body.data.attributes.estimate_delete_join : false;
        role['invoice_delete_all'] = req.body.data.attributes.invoice_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_delete_all, 'boolean') ? req.body.data.attributes.invoice_delete_all : false;
        role['invoice_delete_join'] = req.body.data.attributes.invoice_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_delete_join, 'boolean') ? req.body.data.attributes.invoice_delete_join : false;
        role['expense_delete_all'] = req.body.data.attributes.expense_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_delete_all, 'boolean') ? req.body.data.attributes.expense_delete_all : false;
        role['expense_delete_join'] = req.body.data.attributes.expense_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_delete_join, 'boolean') ? req.body.data.attributes.expense_delete_join : false;
        role['payment_delete_all'] = req.body.data.attributes.payment_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_delete_all, 'boolean') ? req.body.data.attributes.payment_delete_all : false;
        role['payment_delete_join'] = req.body.data.attributes.payment_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_delete_join, 'boolean') ? req.body.data.attributes.payment_delete_join : false;

        role['contact_edit_all'] = req.body.data.attributes.contact_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_edit_all, 'boolean') ? req.body.data.attributes.contact_edit_all : false;
        role['contact_edit_join'] = req.body.data.attributes.contact_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_edit_join, 'boolean') ? req.body.data.attributes.contact_edit_join : false;
        role['product_edit_all'] = req.body.data.attributes.product_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_edit_all, 'boolean') ? req.body.data.attributes.product_edit_all : false;
        role['product_edit_join'] = req.body.data.attributes.product_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_edit_join, 'boolean') ? req.body.data.attributes.product_edit_join : false;
        role['project_edit_all'] = req.body.data.attributes.project_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_edit_all, 'boolean') ? req.body.data.attributes.project_edit_all : false;
        role['project_edit_join'] = req.body.data.attributes.project_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_edit_join, 'boolean') ? req.body.data.attributes.project_edit_join : false;
        role['event_edit_all'] = req.body.data.attributes.event_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_edit_all, 'boolean') ? req.body.data.attributes.event_edit_all : false;
        role['event_edit_join'] = req.body.data.attributes.event_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_edit_join, 'boolean') ? req.body.data.attributes.event_edit_join : false;
        role['workhour_edit_all'] = req.body.data.attributes.workhour_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_edit_all, 'boolean') ? req.body.data.attributes.workhour_edit_all : false;
        role['workhour_edit_join'] = req.body.data.attributes.workhour_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_edit_join, 'boolean') ? req.body.data.attributes.workhour_edit_join : false;
        role['estimate_edit_all'] = req.body.data.attributes.estimate_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_edit_all, 'boolean') ? req.body.data.attributes.estimate_edit_all : false;
        role['estimate_edit_join'] = req.body.data.attributes.estimate_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_edit_join, 'boolean') ? req.body.data.attributes.estimate_edit_join : false;
        role['invoice_edit_all'] = req.body.data.attributes.invoice_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_edit_all, 'boolean') ? req.body.data.attributes.invoice_edit_all : false;
        role['invoice_edit_join'] = req.body.data.attributes.invoice_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_edit_join, 'boolean') ? req.body.data.attributes.invoice_edit_join : false;
        role['expense_edit_all'] = req.body.data.attributes.expense_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_edit_all, 'boolean') ? req.body.data.attributes.expense_edit_all : false;
        role['expense_edit_join'] = req.body.data.attributes.expense_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_edit_join, 'boolean') ? req.body.data.attributes.expense_edit_join : false;
        role['payment_edit_all'] = req.body.data.attributes.payment_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_edit_all, 'boolean') ? req.body.data.attributes.payment_edit_all : false;
        role['payment_edit_join'] = req.body.data.attributes.payment_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_edit_join, 'boolean') ? req.body.data.attributes.payment_edit_join : false;

        role['contact_insert_all'] = req.body.data.attributes.contact_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_insert_all, 'boolean') ? req.body.data.attributes.contact_insert_all : false;
        role['product_insert_all'] = req.body.data.attributes.product_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_insert_all, 'boolean') ? req.body.data.attributes.product_insert_all : false;
        role['project_insert_all'] = req.body.data.attributes.project_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_insert_all, 'boolean') ? req.body.data.attributes.project_insert_all : false;
        role['event_insert_all'] = req.body.data.attributes.event_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_insert_all, 'boolean') ? req.body.data.attributes.event_insert_all : false;
        role['workhour_insert_all'] = req.body.data.attributes.workhour_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_insert_all, 'boolean') ? req.body.data.attributes.workhour_insert_all : false;
        role['estimate_insert_all'] = req.body.data.attributes.estimate_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_insert_all, 'boolean') ? req.body.data.attributes.estimate_insert_all : false;
        role['invoice_insert_all'] = req.body.data.attributes.invoice_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_insert_all, 'boolean') ? req.body.data.attributes.invoice_insert_all : false;
        role['expense_insert_all'] = req.body.data.attributes.expense_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_insert_all, 'boolean') ? req.body.data.attributes.expense_insert_all : false;
        role['payment_insert_all'] = req.body.data.attributes.payment_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_insert_all, 'boolean') ? req.body.data.attributes.payment_insert_all : false;

        role['is_editable'] = req.body.data.attributes.is_editable !== undefined && utility.check_type_variable(req.body.data.attributes.is_editable, 'boolean') ? req.body.data.attributes.is_editable : false;
        role['is_erasable'] = req.body.data.attributes.is_erasable !== undefined && utility.check_type_variable(req.body.data.attributes.is_erasable, 'boolean') ? req.body.data.attributes.is_erasable : false;
        role['is_show'] = req.body.data.attributes.is_show !== undefined && utility.check_type_variable(req.body.data.attributes.is_show, 'boolean') ? req.body.data.attributes.is_show : false;

        role['contact_menu'] = req.body.data.attributes.contact_menu !== undefined && utility.check_type_variable(req.body.data.attributes.contact_menu, 'boolean') ? req.body.data.attributes.contact_menu : false;
        role['product_menu'] = req.body.data.attributes.product_menu !== undefined && utility.check_type_variable(req.body.data.attributes.product_menu, 'boolean') ? req.body.data.attributes.product_menu : false;
        role['project_menu'] = req.body.data.attributes.project_menu !== undefined && utility.check_type_variable(req.body.data.attributes.project_menu, 'boolean') ? req.body.data.attributes.project_menu : false;
        role['event_menu'] = req.body.data.attributes.event_menu !== undefined && utility.check_type_variable(req.body.data.attributes.event_menu, 'boolean') ? req.body.data.attributes.event_menu : false;
        role['workhour_menu'] = req.body.data.attributes.workhour_menu !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_menu, 'boolean') ? req.body.data.attributes.workhour_menu : false;
        role['estimate_menu'] = req.body.data.attributes.estimate_menu !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_menu, 'boolean') ? req.body.data.attributes.estimate_menu : false;
        role['invoice_menu'] = req.body.data.attributes.invoice_menu !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_menu, 'boolean') ? req.body.data.attributes.invoice_menu : false;
        role['expense_menu'] = req.body.data.attributes.expense_menu !== undefined && utility.check_type_variable(req.body.data.attributes.expense_menu, 'boolean') ? req.body.data.attributes.expense_menu : false;
        role['payment_menu'] = req.body.data.attributes.payment_menu !== undefined && utility.check_type_variable(req.body.data.attributes.payment_menu, 'boolean') ? req.body.data.attributes.payment_menu : false;

        role['is_admin'] = req.body.data.attributes.is_admin !== undefined && utility.check_type_variable(req.body.data.attributes.is_admin, 'boolean') ? req.body.data.attributes.is_admin : false;;
        role['is_client'] = req.body.data.attributes.is_client !== undefined && utility.check_type_variable(req.body.data.attributes.is_client, 'boolean') ? req.body.data.attributes.is_client : false;
        role['show_user_online'] = req.body.data.attributes.show_user_online !== undefined && utility.check_type_variable(req.body.data.attributes.show_user_online, 'boolean') ? req.body.data.attributes.show_user_online : false;
        role['redirect_page'] = (req.body.data.attributes.redirect_page && utility.check_type_variable(req.body.data.attributes.redirect_page, 'string') && req.body.data.attributes.redirect_page !== null ? req.body.data.attributes.redirect_page.replace(/'/g, "''''") : '');

        role['totals_view'] = req.body.data.attributes.totals_view !== undefined && utility.check_type_variable(req.body.data.attributes.totals_view, 'boolean') ? req.body.data.attributes.totals_view : false;
        role['report_view'] = req.body.data.attributes.report_view !== undefined && utility.check_type_variable(req.body.data.attributes.report_view, 'boolean') ? req.body.data.attributes.report_view : false;
        role['report_total_view'] = req.body.data.attributes.report_total_view !== undefined && utility.check_type_variable(req.body.data.attributes.report_total_view, 'boolean') ? req.body.data.attributes.report_total_view : false;
        role['show_settings'] = req.body.data.attributes.show_settings !== undefined && utility.check_type_variable(req.body.data.attributes.show_settings, 'boolean') ? req.body.data.attributes.show_settings : false;
        role['hide_account_settings'] = req.body.data.attributes.hide_account_settings !== undefined && utility.check_type_variable(req.body.data.attributes.hide_account_settings, 'boolean') ? req.body.data.attributes.hide_account_settings : false;
        role['hide_account'] = req.body.data.attributes.hide_account !== undefined && utility.check_type_variable(req.body.data.attributes.hide_account, 'boolean') ? req.body.data.attributes.hide_account : false;
        

        sequelize.query('SELECT insert_role_v2(\'' +
                JSON.stringify(role) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_role_v2;
                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('role_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        async.parallel({
            part1: function(callback) {
                sequelize.query('SELECT update_role(' +
                        req.params.id + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',\'' +
                        (req.body.data.attributes.role_name && utility.check_type_variable(req.body.data.attributes.role_name, 'string') && req.body.data.attributes.role_name !== null ? req.body.data.attributes.role_name.replace(/'/g, "''''") : '') + '\',' +
                        (req.body.data.attributes.contact_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_view_all, 'boolean') ? req.body.data.attributes.contact_view_all : false) + ',' +
                        (req.body.data.attributes.contact_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_view_join, 'boolean') ? req.body.data.attributes.contact_view_join : false) + ',' +
                        (req.body.data.attributes.product_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_view_all, 'boolean') ? req.body.data.attributes.product_view_all : false) + ',' +
                        (req.body.data.attributes.product_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_view_join, 'boolean') ? req.body.data.attributes.product_view_join : false) + ',' +
                        (req.body.data.attributes.project_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_view_all, 'boolean') ? req.body.data.attributes.project_view_all : false) + ',' +
                        (req.body.data.attributes.project_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_view_join, 'boolean') ? req.body.data.attributes.project_view_join : false) + ',' +
                        (req.body.data.attributes.event_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_view_all, 'boolean') ? req.body.data.attributes.event_view_all : false) + ',' +
                        (req.body.data.attributes.event_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_view_join, 'boolean') ? req.body.data.attributes.event_view_join : false) + ',' +
                        (req.body.data.attributes.estimate_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_view_all, 'boolean') ? req.body.data.attributes.estimate_view_all : false) + ',' +
                        (req.body.data.attributes.estimate_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_view_join, 'boolean') ? req.body.data.attributes.estimate_view_join : false) + ',' +
                        (req.body.data.attributes.invoice_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_view_all, 'boolean') ? req.body.data.attributes.invoice_view_all : false) + ',' +
                        (req.body.data.attributes.invoice_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_view_join, 'boolean') ? req.body.data.attributes.invoice_view_join : false) + ',' +
                        (req.body.data.attributes.expense_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_view_all, 'boolean') ? req.body.data.attributes.expense_view_all : false) + ',' +
                        (req.body.data.attributes.expense_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_view_join, 'boolean') ? req.body.data.attributes.expense_view_join : false) + ',' +
                        (req.body.data.attributes.payment_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_view_all, 'boolean') ? req.body.data.attributes.payment_view_all : false) + ',' +
                        (req.body.data.attributes.payment_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_view_join, 'boolean') ? req.body.data.attributes.payment_view_join : false) + ',' +
                        (req.body.data.attributes.workhour_view_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_view_all, 'boolean') ? req.body.data.attributes.workhour_view_all : false) + ',' +
                        (req.body.data.attributes.workhour_view_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_view_join, 'boolean') ? req.body.data.attributes.workhour_view_join : false) + ',' +
                        (req.body.data.attributes.contact_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_list_all, 'boolean') ? req.body.data.attributes.contact_list_all : false) + ',' +
                        (req.body.data.attributes.contact_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_list_join, 'boolean') ? req.body.data.attributes.contact_list_join : false) + ',' +
                        (req.body.data.attributes.product_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_list_all, 'boolean') ? req.body.data.attributes.product_list_all : false) + ',' +
                        (req.body.data.attributes.product_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_list_join, 'boolean') ? req.body.data.attributes.product_list_join : false) + ',' +
                        (req.body.data.attributes.project_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_list_all, 'boolean') ? req.body.data.attributes.project_list_all : false) + ',' +
                        (req.body.data.attributes.project_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_list_join, 'boolean') ? req.body.data.attributes.project_list_join : false) + ',' +
                        (req.body.data.attributes.event_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_list_all, 'boolean') ? req.body.data.attributes.event_list_all : false) + ',' +
                        (req.body.data.attributes.event_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_list_join, 'boolean') ? req.body.data.attributes.event_list_join : false) + ',' +
                        (req.body.data.attributes.estimate_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_list_all, 'boolean') ? req.body.data.attributes.estimate_list_all : false) + ',' +
                        (req.body.data.attributes.estimate_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_list_join, 'boolean') ? req.body.data.attributes.estimate_list_join : false) + ',' +
                        (req.body.data.attributes.invoice_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_list_all, 'boolean') ? req.body.data.attributes.invoice_list_all : false) + ',' +
                        (req.body.data.attributes.invoice_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_list_join, 'boolean') ? req.body.data.attributes.invoice_list_join : false) + ',' +
                        (req.body.data.attributes.expense_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_list_all, 'boolean') ? req.body.data.attributes.expense_list_all : false) + ',' +
                        (req.body.data.attributes.expense_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_list_join, 'boolean') ? req.body.data.attributes.expense_list_join : false) + ',' +
                        (req.body.data.attributes.payment_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_list_all, 'boolean') ? req.body.data.attributes.payment_list_all : false) + ',' +
                        (req.body.data.attributes.payment_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_list_join, 'boolean') ? req.body.data.attributes.payment_list_join : false) + ',' +
                        (req.body.data.attributes.workhour_list_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_list_all, 'boolean') ? req.body.data.attributes.workhour_list_all : false) + ',' +
                        (req.body.data.attributes.workhour_list_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_list_join, 'boolean') ? req.body.data.attributes.workhour_list_join : false) + ',' +
                        (req.body.data.attributes.contact_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_edit_all, 'boolean') ? req.body.data.attributes.contact_edit_all : false) + ',' +
                        (req.body.data.attributes.contact_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_edit_join, 'boolean') ? req.body.data.attributes.contact_edit_join : false) + ',' +
                        (req.body.data.attributes.product_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_edit_all, 'boolean') ? req.body.data.attributes.product_edit_all : false) + ',' +
                        (req.body.data.attributes.product_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_edit_join, 'boolean') ? req.body.data.attributes.product_edit_join : false) + ',' +
                        (req.body.data.attributes.project_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_edit_all, 'boolean') ? req.body.data.attributes.project_edit_all : false) + ',' +
                        (req.body.data.attributes.project_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_edit_join, 'boolean') ? req.body.data.attributes.project_edit_join : false) + ',' +
                        (req.body.data.attributes.event_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_edit_all, 'boolean') ? req.body.data.attributes.event_edit_all : false) + ',' +
                        (req.body.data.attributes.event_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_edit_join, 'boolean') ? req.body.data.attributes.event_edit_join : false) + ',' +
                        (req.body.data.attributes.estimate_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_edit_all, 'boolean') ? req.body.data.attributes.estimate_edit_all : false) + ',' +
                        (req.body.data.attributes.estimate_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_edit_join, 'boolean') ? req.body.data.attributes.estimate_edit_join : false) + ',' +
                        (req.body.data.attributes.invoice_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_edit_all, 'boolean') ? req.body.data.attributes.invoice_edit_all : false) + ',' +
                        (req.body.data.attributes.invoice_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_edit_join, 'boolean') ? req.body.data.attributes.invoice_edit_join : false) + ',' +
                        (req.body.data.attributes.expense_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_edit_all, 'boolean') ? req.body.data.attributes.expense_edit_all : false) + ',' +
                        (req.body.data.attributes.expense_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_edit_join, 'boolean') ? req.body.data.attributes.expense_edit_join : false) + ',' +
                        (req.body.data.attributes.payment_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_edit_all, 'boolean') ? req.body.data.attributes.payment_edit_all : false) + ',' +
                        (req.body.data.attributes.payment_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_edit_join, 'boolean') ? req.body.data.attributes.payment_edit_join : false) + ',' +
                        (req.body.data.attributes.workhour_edit_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_edit_all, 'boolean') ? req.body.data.attributes.workhour_edit_all : false) + ',' +
                        (req.body.data.attributes.workhour_edit_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_edit_join, 'boolean') ? req.body.data.attributes.workhour_edit_join : false) + ',' +
                        (req.body.data.attributes.contact_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_delete_all, 'boolean') ? req.body.data.attributes.contact_delete_all : false) + ',' +
                        (req.body.data.attributes.contact_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.contact_delete_join, 'boolean') ? req.body.data.attributes.contact_delete_join : false) + ',' +
                        (req.body.data.attributes.product_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_delete_all, 'boolean') ? req.body.data.attributes.product_delete_all : false) + ',' +
                        (req.body.data.attributes.product_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.product_delete_join, 'boolean') ? req.body.data.attributes.product_delete_join : false) + ',' +
                        (req.body.data.attributes.project_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_delete_all, 'boolean') ? req.body.data.attributes.project_delete_all : false) + ',' +
                        (req.body.data.attributes.project_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.project_delete_join, 'boolean') ? req.body.data.attributes.project_delete_join : false) + ',' +
                        (req.body.data.attributes.event_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_delete_all, 'boolean') ? req.body.data.attributes.event_delete_all : false) + ',' +
                        (req.body.data.attributes.event_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.event_delete_join, 'boolean') ? req.body.data.attributes.event_delete_join : false) + ',' +
                        (req.body.data.attributes.estimate_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_delete_all, 'boolean') ? req.body.data.attributes.estimate_delete_all : false) + ',' +
                        (req.body.data.attributes.estimate_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_delete_join, 'boolean') ? req.body.data.attributes.estimate_delete_join : false) + ',' +
                        (req.body.data.attributes.invoice_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_delete_all, 'boolean') ? req.body.data.attributes.invoice_delete_all : false) + ',' +
                        (req.body.data.attributes.invoice_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_delete_join, 'boolean') ? req.body.data.attributes.invoice_delete_join : false) + ',' +
                        (req.body.data.attributes.expense_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_delete_all, 'boolean') ? req.body.data.attributes.expense_delete_all : false) + ',' +
                        (req.body.data.attributes.expense_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.expense_delete_join, 'boolean') ? req.body.data.attributes.expense_delete_join : false) + ',' +
                        (req.body.data.attributes.payment_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_delete_all, 'boolean') ? req.body.data.attributes.payment_delete_all : false) + ',' +
                        (req.body.data.attributes.payment_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.payment_delete_join, 'boolean') ? req.body.data.attributes.payment_delete_join : false) + ',' +
                        (req.body.data.attributes.workhour_delete_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_delete_all, 'boolean') ? req.body.data.attributes.workhour_delete_all : false) + ',' +
                        (req.body.data.attributes.workhour_delete_join !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_delete_join, 'boolean') ? req.body.data.attributes.workhour_delete_join : false) + ',' +
                        (req.body.data.attributes.contact_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.contact_insert_all, 'boolean') ? req.body.data.attributes.contact_insert_all : false) + ',' +
                        (req.body.data.attributes.product_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.product_insert_all, 'boolean') ? req.body.data.attributes.product_insert_all : false) + ',' +
                        (req.body.data.attributes.project_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.project_insert_all, 'boolean') ? req.body.data.attributes.project_insert_all : false) + ',' +
                        (req.body.data.attributes.event_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.event_insert_all, 'boolean') ? req.body.data.attributes.event_insert_all : false) + ',' +
                        (req.body.data.attributes.estimate_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_insert_all, 'boolean') ? req.body.data.attributes.estimate_insert_all : false) + ',' +
                        (req.body.data.attributes.invoice_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_insert_all, 'boolean') ? req.body.data.attributes.invoice_insert_all : false) + ',' +
                        (req.body.data.attributes.expense_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.expense_insert_all, 'boolean') ? req.body.data.attributes.expense_insert_all : false) + ',' +
                        (req.body.data.attributes.payment_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.payment_insert_all, 'boolean') ? req.body.data.attributes.payment_insert_all : false) + ',' +
                        (req.body.data.attributes.workhour_insert_all !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_insert_all, 'boolean') ? req.body.data.attributes.workhour_insert_all : false) + ',' +
                        (req.body.data.attributes.is_editable !== undefined && utility.check_type_variable(req.body.data.attributes.is_editable, 'boolean') ? req.body.data.attributes.is_editable : false) + ',' +
                        (req.body.data.attributes.is_erasable !== undefined && utility.check_type_variable(req.body.data.attributes.is_erasable, 'boolean') ? req.body.data.attributes.is_erasable : false) + ',' +
                        (req.body.data.attributes.is_show !== undefined && utility.check_type_variable(req.body.data.attributes.is_show, 'boolean') ? req.body.data.attributes.is_show : false) + ',' +
                        (req.body.data.attributes.contact_menu !== undefined && utility.check_type_variable(req.body.data.attributes.contact_menu, 'boolean') ? req.body.data.attributes.contact_menu : false) + ',' +
                        (req.body.data.attributes.product_menu !== undefined && utility.check_type_variable(req.body.data.attributes.product_menu, 'boolean') ? req.body.data.attributes.product_menu : false) + ',' +
                        (req.body.data.attributes.project_menu !== undefined && utility.check_type_variable(req.body.data.attributes.project_menu, 'boolean') ? req.body.data.attributes.project_menu : false) + ',' +
                        (req.body.data.attributes.event_menu !== undefined && utility.check_type_variable(req.body.data.attributes.event_menu, 'boolean') ? req.body.data.attributes.event_menu : false) + ',' +
                        (req.body.data.attributes.workhour_menu !== undefined && utility.check_type_variable(req.body.data.attributes.workhour_menu, 'boolean') ? req.body.data.attributes.workhour_menu : false) + ',' +
                        (req.body.data.attributes.estimate_menu !== undefined && utility.check_type_variable(req.body.data.attributes.estimate_menu, 'boolean') ? req.body.data.attributes.estimate_menu : false) + ',' +
                        (req.body.data.attributes.invoice_menu !== undefined && utility.check_type_variable(req.body.data.attributes.invoice_menu, 'boolean') ? req.body.data.attributes.invoice_menu : false) + ',' +
                        (req.body.data.attributes.expense_menu !== undefined && utility.check_type_variable(req.body.data.attributes.expense_menu, 'boolean') ? req.body.data.attributes.expense_menu : false) + ',' +
                        (req.body.data.attributes.payment_menu !== undefined && utility.check_type_variable(req.body.data.attributes.payment_menu, 'boolean') ? req.body.data.attributes.payment_menu : false) + ',' +
                        (req.body.data.attributes.is_admin !== undefined && utility.check_type_variable(req.body.data.attributes.is_admin, 'boolean') ? req.body.data.attributes.is_admin : false) + ',' +
                        (req.body.data.attributes.is_client !== undefined && utility.check_type_variable(req.body.data.attributes.is_client, 'boolean') ? req.body.data.attributes.is_client : false) + ',' +
                        (req.body.data.attributes.show_user_online !== undefined && utility.check_type_variable(req.body.data.attributes.show_user_online, 'boolean') ? req.body.data.attributes.show_user_online : false) +
                        ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        callback(null, datas);

                    }).catch(function(err) {
                        callback(err, null);
                    });
            },
            part2: function(callback) {
                sequelize.query('SELECT update_role_part_2(' +
                        req.params.id + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',\'' +
                        (req.body.data.attributes.redirect_page && utility.check_type_variable(req.body.data.attributes.redirect_page, 'string') && req.body.data.attributes.redirect_page !== null ? req.body.data.attributes.redirect_page.replace(/'/g, "''''") : '') + '\',' +
                        (req.body.data.attributes.totals_view && utility.check_type_variable(req.body.data.attributes.totals_view, 'boolean') && req.body.data.attributes.totals_view !== null ? req.body.data.attributes.totals_view : false) + ',' +
                        (req.body.data.attributes.report_view && utility.check_type_variable(req.body.data.attributes.report_view, 'boolean') && req.body.data.attributes.report_view !== null ? req.body.data.attributes.report_view : false) + ',' +
                        (req.body.data.attributes.report_total_view && utility.check_type_variable(req.body.data.attributes.report_total_view, 'boolean') && req.body.data.attributes.report_total_view !== null ? req.body.data.attributes.report_total_view : false) + ','+
                        (req.body.data.attributes.show_settings && utility.check_type_variable(req.body.data.attributes.show_settings, 'boolean') && req.body.data.attributes.show_settings !== null ? req.body.data.attributes.show_settings : false) + ','+
                        (req.body.data.attributes.hide_account_settings && utility.check_type_variable(req.body.data.attributes.hide_account_settings, 'boolean') && req.body.data.attributes.hide_account_settings !== null ? req.body.data.attributes.hide_account_settings : false)  + ','+
                        (req.body.data.attributes.hide_account && utility.check_type_variable(req.body.data.attributes.hide_account, 'boolean') && req.body.data.attributes.hide_account !== null ? req.body.data.attributes.hide_account : false) + 
                         ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        callback(null, datas);

                    }).catch(function(err) {
                        callback(err, null);
                    });
            }
        }, function(err, results) {

            if (err) {
                return res.status(500).send({ err });
            }

            res.json({
                'data': req.body.data
            });
        });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_role_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'roles',
                    'id': datas[0].delete_role_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('role_destroy: ' + err);
        });
};