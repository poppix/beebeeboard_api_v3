var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var expire_date = {};

    if (req.body.data.attributes !== undefined) {
        // Import case
        if (req.body.data.attributes.expire_date != null && !moment(req.body.data.attributes.expire_date).isValid()) {
            return res.status(422).send(utility.error422('expire_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        expire_date['expire_date'] = req.body.data.attributes.expire_date;
        expire_date['total'] = req.body.data.attributes.total;
        expire_date['organization'] = req.headers['host'].split(".")[0];
        expire_date['mongo_id'] = null;
        expire_date['payment_method'] = (req.body.data.attributes.payment_method && utility.check_type_variable(req.body.data.attributes.payment_method, 'string') && req.body.data.attributes.payment_method !== null ? req.body.data.attributes.payment_method.replace(/'/g, "''''") : '');
        
        sequelize.query('SELECT insert_expire_date(\'' + JSON.stringify(expire_date) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_expire_date;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'expire_date',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_expire_date,
                    'Aggiunta scadenza ' + datas[0].insert_expire_date
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('expire_date_create: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        if (req.body.data.attributes.expire_date != null && !moment(req.body.data.attributes.expire_date).isValid()) {
            return res.status(422).send(utility.error422('expire_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sequelize.query('SELECT update_expire_date_v1(' +
                req.params.id + ',\'' +
                req.body.data.attributes.expire_date + '\',' +
                req.body.data.attributes.total + ',\'' +
                req.headers['host'].split(".")[0] + '\',\''+
                (req.body.data.attributes.payment_method && utility.check_type_variable(req.body.data.attributes.payment_method, 'string') && req.body.data.attributes.payment_method !== null ? req.body.data.attributes.payment_method.replace(/'/g, "''''") : '')+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'expire_date',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificata scadenza ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('expire_date_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_expire_date_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'expire_dates',
                    'id': datas[0].delete_expire_date_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('expire_date_destroy: ' + err);
        });
};