var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.create = function(req, res) {
    var expensearticle = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        expensearticle['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        expensearticle['importo'] = req.body.data.attributes.importo;
        expensearticle['first_rate'] = req.body.data.attributes.first_rate;
        expensearticle['first_rate_included'] = req.body.data.attributes.first_rate_included;
        expensearticle['iva_rate'] = req.body.data.attributes.iva_rate;
        expensearticle['iva_natura'] = (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '');
        expensearticle['mongo_id'] = null;
        expensearticle['ritenuta'] = req.body.data.attributes.ritenuta;
        expensearticle['price'] = req.body.data.attributes.price;
        expensearticle['qta'] = req.body.data.attributes.qta;
        
        sequelize.query('SELECT insert_expensearticle_v1(\'' + JSON.stringify(expensearticle) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].insert_expensearticle_v1 && datas[0].insert_expensearticle_v1 != null) {
                    req.body.data.id = datas[0].insert_expensearticle_v1;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_expensearticle_v1,
                        'body': req.body.data.relationships,
                        'model': 'expensearticle',
                        'user_id': req.user.id,
                        'array': ['category', 'sub_category']
                    }, function(err, results) {

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'expensearticle',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_expensearticle_v1,
                            'Aggiunto articolo di acquisto ' + datas[0].insert_expensearticle_v1
                        );
                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    });

                }
            }).catch(function(err) {
                return res.status(400).render('expensearticle_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case

        sequelize.query('SELECT delete_expensearticle_relations(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {


                sequelize.query('SELECT update_expensearticle_v2(' +
                        req.params.id + ',\'' +
                        (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') + '\',' +
                        req.body.data.attributes.importo + ',' +
                        req.body.data.attributes.first_rate + ',' +
                        req.body.data.attributes.first_rate_included + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',\'' +
                        (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '') + '\',' +
                        req.body.data.attributes.iva_rate + ',' +
                        req.body.data.attributes.ritenuta + ',' +
                        req.body.data.attributes.price + ',' +
                        req.body.data.attributes.qta + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'expensearticle',
                            'user_id': req.user.id,
                            'array': ['category', 'sub_category']
                        }, function(err, results) {

                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'expensearticle',
                                'update',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                req.params.id,
                                'Aggiunto articolo di acquisto ' + req.params.id
                            );
                            res.json({
                                'data': req.body.data,
                                'relationships': req.body.data.relationships
                            });
                        });

                    }).catch(function(err) {
                        return res.status(400).render('expensearticle_update: ' + err);
                    });

            }).catch(function(err) {
                return res.status(400).render('expensearticle_update: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_expensearticle_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'expensearticle',
                    'id': datas[0].delete_expensearticle_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('expensearticle_destroy: ' + err);
        });
};