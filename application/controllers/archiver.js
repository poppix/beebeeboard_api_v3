const { S3Client, PutObjectCommand, GetObjectCommand, ListObjectsV2Command, CopyObjectCommand } = require('@aws-sdk/client-s3');
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner');
const { Upload } = require('@aws-sdk/lib-storage');

var utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js'),
    archiver = require('archiver'),
    fs = require('fs'),
    stream = require('stream'),
    request = require('request'),
    moment = require('moment'),
    indexes = require('../utility/indexes.js'),
    s3Zip = require('../utility/zipS3.js'),
    finds = require('../utility/finds.js'),
    config = require('../../config'),
    async = require('async'),
    utils = require('../../utils'),
    fattureXml = require('../utility/fatture_xml.js'),
    https = require('https'),
    Sequelize = require('sequelize');

function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
};

exports.stop_process = function(req, res) {
    sequelize.query('SELECT delete_all_export_invoice_expense(\''+
        req.headers['host'].split(".")[0] +'\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        useMaster: true
    }).then(function(datas) {

        res.status(200).send({});

    }).catch(function(err) {
        return res.status(400).render('delete_all_export_invoice_expense: ' + err);
    });
};

exports.invoice = function(req, res) {
    // require modules
    //var fs = require('fs');
    req.setTimeout(500000);
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    const s3 = new S3Client({
            region: config.aws_s3.region,
            credentials:{
                accessKeyId: config.aws_s3.accessKeyId,
                secretAccessKey: config.aws_s3.secretAccessKey,
            }
        });


    var type_name = 'fattura';
    var type_names = 'fatture';
    switch (invoice.data.attributes.document_type) {
        case 'invoice':
            type_name = 'fattura';
            type_names = 'fatture';
            break;

        case 'estimate':
            type_name = 'preventivo';
            type_names = 'preventivi';
            break;

        case 'ddt':
            type_name = 'ddt';
            type_names = 'ddt';
            break;

        case 'receipt':
            type_name = 'ricevuta';
            type_names = 'ricevute';
            break;

        case 'order':
            type_name = 'ordine';
            type_names = 'ordini';
            break;

        case 'contract':
            type_name = 'contratto';
            type_names = 'contratti';
            break;
    }

    indexes.invoice(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('invoice_index: ' + err);
        }

        var archive = archiver('zip', {
            zlib: {
                level: 9
            } // Sets the compression level.
        });

        // This event is fired when the data source is drained no matter what was the data source.
        // It is not part of this library but rather from the NodeJS Stream API.
        // @see: https://nodejs.org/api/stream.html#stream_event_end
        archive.on('end', function() {
            console.log('Archive wrote %d bytes', archive.pointer());
        });

        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function(err) {
            console.log(err);
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function(err) {
            console.log(err);
            return res.status(500).send({
                error: err.message
            });
        });

        //set the archive name
        res.attachment(type_names + '.zip');

        // pipe archive data to the file
        archive.pipe(res);

        var baseUrl = 'https://' + req.headers['host'] + '/api/v1/';
        var token = req.query.access_token;
        //var baseUrl = 'http://maisonvive.beebeeboard.com:3000/api/v1/';
        //var token = 'YdnocAaS8Yzu7aD90goegrt6ibvYNzY8YkOaFpyQ52weiy7rsga6cimg2iKjfjmk7igFqesp62ha7x2hyaObktIxTW8Jg4JCKhGVHM1CvpNGc7lfg4694LTDKpykT4iMa1eoOo0F7qhvHIipk7d7r40OwpBbSKaC5pYdQF3di1ndMMhbjxZoxyPic3OxI5gNyHIIxus1RoSNQ2R4TAVYKhjNmmYLEibvC9bCSA6tj6e58hPwR7lSJTAPd7TeQJDk';

        let invoices = response.data.map(function(invoice) {

            if (isLetter(invoice.attributes.society_piva[0]) && isLetter(invoice.attributes.society_piva[1])) {
                return {
                    'name': invoice.attributes.number + ' del ' + moment(invoice.attributes.date).format('DD-MM-YYYY'),
                    'url_pdf': baseUrl + 'pdf?invoice_id=' + invoice.id + '&access_token=' + token,
                    'url_xml': baseUrl + 'invoices/xml/' + invoice.id + '?access_token=' + token,
                    'id': invoice.id,
                    'number': invoice.attributes.number,
                    'date': invoice.attributes.date,
                    'piva': invoice.attributes.society_piva,
                    'is_creditnote': invoice.attributes.is_creditnote
                }
            } else {
                return {
                    'name': invoice.attributes.number + ' del ' + moment(invoice.attributes.date).format('DD-MM-YYYY'),
                    'url_pdf': baseUrl + 'pdf?invoice_id=' + invoice.id + '&access_token=' + token,
                    'url_xml': baseUrl + 'invoices/xml/' + invoice.id + '?access_token=' + token,
                    'id': invoice.id,
                    'number': invoice.attributes.number,
                    'date': invoice.attributes.date,
                    'piva': 'IT' + invoice.attributes.society_piva,
                    'is_creditnote': invoice.attributes.is_creditnote
                }
            }

        });

        var cnt = 0;

        var length_inv = invoices.length;

        // for (var i = 0; i < invoices.length; i++) {
        async.eachLimit(invoices, 5, function(invoice1, callback1) {
            if (!req.user.role[0].json_build_object.attributes.invoice_view) {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }

            if (utility.check_id(invoice1.id)) {
                return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
            }

            req.params.id = invoice1.id;
            var url_pdf = invoice1.url_pdf;
            var url_xml = invoice1.url_xml;
            var date_invoice = invoice1.date;
            var num_invoice = invoice1.number;
            var piva_emesso = invoice1.piva;
            var nc = invoice1.is_creditnote === true ? 'NC' : '';
            var anno = moment(date_invoice).format('YYYY');

            finds.find_invoice(req, function(err, results) {

                if (err) {
                    console.log(err);
                    //return res.status(500).send(err);
                } else {

                    async.parallel({
                        finale: function(callback){
                            callback(null, results);
                        },
                        name: function(callback) {
                            var invoice = results;

                            callback(null, (piva_emesso + '_' + invoice.data.attributes.filename_xml.replace(/\//g, '_')));
                        },
                        pdf: function(callback) {
                            var invoice = results;
                            var doc_detail = generate_file_name(invoice);

                            var pdf_file_name = (invoice.data.attributes.pdf_file_name && invoice.data.attributes.pdf_file_name != '') ? invoice.data.attributes.pdf_file_name : 'pdf.php';
                            var url = `https://asset.beebeeboard.com/pdf_create/api/${pdf_file_name}?organization=${req.headers['host'].split(".")[0]}&invoice_id=${invoice.data.id}&test_invoice_style=&token=${token}&$_t=${moment().format('HHmmss')}`;
                            var richiesta = https.get(url, function(result) {

                                var chunks = [];

                                result.on('data', function(chunk) {
                                    chunks.push(chunk);
                                });

                                result.on('end', function() {
                                    var pdfData = new Buffer.concat(chunks);

                                    callback(null, pdfData);
                                });
                            }).on('error', function(err) {

                                callback(err, null);
                            });

                            richiesta.end();
                            
                        },
                        xml: function(callback) {
                            
                            fattureXml.generaFatturaBase(results, token, function(err, response) {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    response['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente'] = null;
                                    response['v1:FatturaElettronica']['FatturaElettronicaHeader']['SoggettoEmittente'] = null;
                                    response['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente'] = fattureXml.getIdFiscaleIVAEx(results.data.attributes.society_piva, 'cedente', results);

                                    var xmlData = fattureXml.generaXmlFattura(response);

                                    var piva_name = '';
                                    if (isLetter(results.data.attributes.society_piva[0]) && isLetter(results.data.attributes.society_piva[1])) {
                                        piva_name = results.data.attributes.society_piva;
                                    } else {
                                        piva_name = 'IT' + results.data.attributes.society_piva;
                                    }

                                    callback(null, xmlData);
                                }

                            });
                        }

                    }, function(err1, final) {
                        if (err1) {
                            console.log('Error1 ' + err1);
                            console.log('CNT' + cnt);
                            callback1(null, cnt++);
                        } else {
                            archive.append(final.pdf, {
                                'name': type_names + '/PDF/' + final.name + '.pdf'
                            });
                            archive.append(final.xml, {
                                'name': type_names + '/XML/' + final.name + '.xml'
                            });

                            console.log(final.finale);
                            if(final.finale.included && final.finale.included.length > 0){
                                var cnt_file = 0;
                                final.finale.included.forEach(function(includ) {

                                    async.parallel({
                                        extension: function(callback2){
                                            if (includ.type == 'file') {
                                                
                                                 callback2(null, includ.attributes.file_type);

                                            }else {
                                                callback2(null, null);
                                            }
                                        },
                                        file: function(callback2) {
                                            if (includ.type == 'file') {
                                                //var url = 'https://data.beebeeboard.com/' + includ.attributes.file_url;

                                                var params = {
                                                    Bucket: 'data.beebeeboard.com',
                                                    Key: includ.attributes.file_url,
                                                    IfMatch: includ.attributes.file_etag
                                                };


                                                const command = new GetObjectCommand(params);
                                                s3.send(command, function(err, result) {
                                               // s3.getObject(params, function(err, result) {
                                                    if (result) {
                                                        callback2(null, result.Body);
                                                    } else {
                                                        console.log(err);
                                                        callback2(null, null);
                                                    }
                                                });

                                            } else {
                                                callback2(null, null);
                                            }

                                        }
                                    }, function(err2, final1) {
                                        if (err2) {
                                            console.log('Error2 ' + err1);
                                        } else {
                                            if (final1.file) {
                                                
                                                archive.append(final1.file, {
                                                    'name': type_names + '/ALLEGATI/' + final.name + '.'+final1.extension
                                                });
                                                console.log(final.name);
                                            }

                                            if (cnt_file++ >= results.included.length - 1) {

                                                console.log('CNT' + cnt);
                                                callback1(null, cnt++);
                                            }

                                        }
                                    });
                                });
                            }else
                            {
                                console.log('CNT' + cnt);
                                callback1(null, cnt++);
                            }
                            
                        }

                    })
                }
            });
        }, function(err, final) {
            // if any of the file processing produced an error, err would equal that error
            if (err) {
                // One of the iterations produced an error.
                // All processing will now stop.
                console.log('An invoice failed to process');
            } else {
                console.log('All invoices have been processed successfully');

                archive.finalize();

            }
        });


        //}


    });
};

exports.expense = function(req, res) {
    // require modules
    //var fs = require('fs');
    req.setTimeout(500000);
    if (!req.user.role[0].json_build_object.attributes.expense_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    indexes.expense(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('expense_index: ' + err);
        }

        var archive = archiver('zip', {
            zlib: {
                level: 9
            } // Sets the compression level.
        });

        // This event is fired when the data source is drained no matter what was the data source.
        // It is not part of this library but rather from the NodeJS Stream API.
        // @see: https://nodejs.org/api/stream.html#stream_event_end
        archive.on('end', function() {
            console.log('Archive wrote %d bytes', archive.pointer());
        });

        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function(err) {
            console.log(err);
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function(err) {
            console.log(err);
            return res.status(500).send({
                error: err.message
            });
        });

        //set the archive name
        res.attachment('passive.zip');

        // pipe archive data to the file
        archive.pipe(res);

        var token = req.query.access_token;

        
        var http = require('http');

        /*
         *  AWS configuration for Aws S3 service
         *
         */
         const s3 = new S3Client({
            region: config.aws_s3.region,
            credentials:{
                accessKeyId: config.aws_s3.accessKeyId,
                secretAccessKey: config.aws_s3.secretAccessKey,
            }
        });

        let expenses = response.data.map(function(expense) {
            return {
                'name': expense.attributes.ref_number + ' del ' + moment(expense.attributes.date).format('DD-MM-YYYY'),
                'id': expense.id,
                'is_creditnote': expense.attributes.is_creditnote
            }
        });

        var cnt = 0;

        for (var i = 0; i < expenses.length; i++) {

            if (!req.user.role[0].json_build_object.attributes.expense_view) {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }

            if (utility.check_id(expenses[i].id)) {
                return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
            }

            req.params.id = expenses[i].id;
            //var url = 'https://data.beebeeboard.com';
            var length_inv = expenses.length;

            finds.find_expense(req, function(err, results) {

                if (err) {
                    return res.status(err.errors[0].status).send(err);
                } else {

                    var cnt_file = 0;
                    var final_file = 0;

                    results.included.forEach(function(includ) {

                        async.parallel({
                            name: function(callback) {
                                if (includ.type === 'files') {

                                    var fornitore_name = results.data.attributes.contact_name;
                                    if (!fornitore_name) {
                                        var arrName = [];
                                        if (results.data.attributes.contact_last_name) {
                                            arrName.push(results.data.attributes.contact_last_name);
                                        }

                                        if (results.data.attributes.contact_first_name) {
                                            arrName.push(results.data.attributes.contact_first_name);
                                        }

                                        fornitore_name = arrName.join('_');
                                    }

                                    fornitore_name = fornitore_name.replace(/[^a-z0-9]/gi, '_');

                                    var name = includ.attributes.file_name.replace('.' + includ.attributes.file_type, '');
                                    if (results.data.attributes.xml_name) {
                                        name = results.data.attributes.xml_name.replace(/\//g, '_').replace('.xml', '').replace('.p7m', '');
                                    } else if (results.data.attributes.ref_number) {
                                        name += '_' + results.data.attributes.ref_number;
                                    }

                                    if (results.data.attributes.is_creditnote) {
                                        name = 'NC_' + name;
                                    }

                                    name = name.replace(/[^a-z0-9]/gi, '_');
                                    name = fornitore_name + '_' + name + '_' + results.data.attributes.id +'_'+includ.attributes.id;

                                    var ret = 'passivo/' + name + '.' + includ.attributes.file_type;

                                    callback(null, ret);

                                } else {
                                    callback(null, null);
                                }

                            },
                            file: function(callback) {
                                if (includ.type == 'files') {
                                    //var url = 'https://data.beebeeboard.com/' + includ.attributes.file_url;

                                    var params = {
                                        Bucket: 'data.beebeeboard.com',
                                        Key: includ.attributes.file_url,
                                        IfMatch: includ.attributes.file_etag
                                    };

                                    const command = new GetObjectCommand(params);
                                    s3.send(command, function(err, result) {
                                    //s3.getObject(params, function(err, result) {
                                        if (result) {
                                            callback(null, result.Body);
                                        } else {
                                            console.log(err);
                                            callback(null, null);
                                        }
                                    });

                                    /*var request = https.get(url, function(result) {

                                        var chunks = [];

                                        result.on('data', function(chunk) {
                                            chunks.push(chunk);
                                        });

                                        result.on('end', function() {
                                            var pdfData = new Buffer.concat(chunks);
                                            //base64 = new Buffer(pdfData, 'binary');  

                                            callback(null, pdfData);

                                        });

                                        result.on('error', function(err) {
                                            callback(err, null);
                                        });
                                    }).on('error', function(err) {
                                        console.log(err);
                                        console.log('file_url', url);
                                    });

                                    request.end();*/
                                } else {
                                    callback(null, null);
                                }

                            }
                        }, function(err1, final) {
                            if (err1) {
                                console.log('Error1 ' + err1);
                            } else {
                                if (final.file) {
                                    final_file++;
                                    archive.append(final.file, {
                                        'name': final.name
                                    });
                                    console.log(final.name);
                                }

                                if (cnt_file++ >= results.included.length - 1) {

                                    if (cnt++ >= length_inv - 1) {
                                        console.log('FINE');
                                        //if(final_file > 0){
                                        archive.finalize();
                                        //}
                                        //else
                                        //{
                                        //res.status(404).send({});
                                        //}

                                        //res.status(200).send({});
                                    }
                                }

                            }
                        });
                    });
                }
            });

        }


    });
};

function generate_file_name(invoice) {
    var doc_detail = {
        'nome': 'Avviso di Fattura',
        'tipo': 'Avviso di Fattura'
    };


    if (invoice.data.attributes.document_type) {
        switch (invoice.data.attributes.document_type) {
            case 'invoice':
                if(invoice.data.attributes.status == 'draft'){
                    doc_detail.nome = 'Bozza di Fattura n° ';
                    doc_detail.tipo = 'Bozza di Fattura';
                }
                else
                {
                    doc_detail.nome = 'Fattura n° ';
                    doc_detail.tipo = 'Fattura';
                }
               
                break;

            case 'estimate':
                if(invoice.data.attributes.status == 'draft'){
                    doc_detail.nome = 'Bozza di Preventivo n° ';
                    doc_detail.tipo = 'Bozza di Preventivo';
                }
                else
                {
                    doc_detail.nome = 'Preventivo n° ';
                    doc_detail.tipo = 'Preventivo';
                }
                break;

            case 'receipt':
                if(invoice.data.attributes.status == 'draft'){
                    doc_detail.nome = 'Bozza di Ricevuta n° ';
                    doc_detail.tipo = 'Bozza di Ricevuta';
                }
                else
                {
                    doc_detail.nome = 'Ricevuta n° ';
                    doc_detail.tipo = 'Ricevuta';
                }
                break;

            case 'ddt':
                doc_detail.nome = 'Documento di trasporto n° ';
                doc_detail.tipo = 'Documento di trasporto';
                break;

            case 'order':
                doc_detail.nome = 'Ordine n° ';
                doc_detail.tipo = 'Ordine';
                break;

            case 'contract':
                doc_detail.nome = 'Proposta d\'acquisto n° ';
                doc_detail.tipo = 'Proposta d\'acquisto';
                break;
        }
    } else {

        if (invoice.data.attributes.estimate === true) {
            doc_detail.nome = 'Preventivo n° ';
            doc_detail.tipo = 'Preventivo';
        } else if (invoice.data.attributes.total < 0) {
            doc_detail.nome = 'Nota di Credito n° ';
            doc_detail.tipo = 'Nota di Credito';
        } else if (invoice.data.attributes.number != null && invoice.data.attributes.number != '' && invoice.data.attributes.estimate === false) {
            doc_detail.nome = 'Fattura n° ';
            doc_detail.tipo = 'Fattura';
        }
    }

    doc_detail.nome += `${invoice.data.attributes.number} del ${moment(invoice.data.attributes.date).format('DD/MM/YYYY')}`;
    doc_detail.nome = doc_detail.nome.replace(/ /g, "_").replace(/\//g, "_");
    return doc_detail;
};

exports.invoice_to_aws = function(req,res){
    
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT organizationfree_data(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(org) {

        indexes.invoice(req, null, function(err, response) {
            if (err) {
                console.log(err);
                return res.status(404).render('invoice_index: ' + err);
            }

            let invoices = response.data.map(function(invoice) {

                if (invoice.attributes.society_piva && isLetter(invoice.attributes.society_piva[0]) && isLetter(invoice.attributes.society_piva[1])) {
                    return {
                        'name': (invoice.attributes.number ? invoice.attributes.number.replace(/\//g, '_') : 'bozza_') + ' del ' + moment(invoice.attributes.date).format('DD-MM-YYYY'),
                        'id': invoice.id,
                        'number': (invoice.attributes.number ? invoice.attributes.number : 'bozza'),
                        'document_type': invoice.attributes.document_type,
                        'date': invoice.attributes.date,
                        'piva': invoice.attributes.society_piva,
                        'is_creditnote': invoice.attributes.is_creditnote
                    }
                } else {
                    return {
                        'name': (invoice.attributes.number ? invoice.attributes.number.replace(/\//g, '_') : 'bozza_') + ' del ' + moment(invoice.attributes.date).format('DD-MM-YYYY'),
                        'id': invoice.id,
                        'number':  (invoice.attributes.number ? invoice.attributes.number : 'bozza'),
                        'document_type': invoice.attributes.document_type,
                        'date': invoice.attributes.date,
                        'piva': 'IT' + invoice.attributes.society_piva,
                        'is_creditnote': invoice.attributes.is_creditnote
                    }
                }

            });
        
            if(invoices && invoices.length > 0){
                res.status(200).send({
                    'mail': org[0].organizationfree_data[0].attributes.admin_mail,
                    'time_to_mail': 3*invoices.length});
            }else{
                res.status(404).send({});
            }

            /*
             *  AWS configuration for Aws S3 service
             *
             */
             const s3 = new S3Client({
                region: config.aws_s3.region,
                credentials:{
                    accessKeyId: config.aws_s3.accessKeyId,
                    secretAccessKey: config.aws_s3.secretAccessKey,
                }
            });

            var time_now = moment().format('HHmmss');

            /* CREO IL BUCKET IN S3 */
            var params = {
                Bucket: 'exports.beebeeboard.com',
                Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/invoices/'+time_now,
                ACL: 'private',
                ServerSideEncryption: 'AES256',
                Body: 'no body'
            };

            var token = utils.uid(config.token.accessTokenLength);
            var url_key = req.headers['host'].split(".")[0] + '/' + req.user.id + '/invoices/'+time_now;
            
            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                req.user.id + ',null,\'export\',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(res) {


                sequelize.query('SELECT insert_export_invoice_expense(\'' + req.headers['host'].split(".")[0] + '\','+
                        req.user.id+',\''+
                        url_key+'\',\'invoice\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(abc) {

                    
                    const command1 = new PutObjectCommand(params);
                    getSignedUrl(s3, command1, { expiresIn: 3600 }).then(url =>{
                    //s3.getSignedUrl('putObject', params, function (err, url) {
                        if (err) {
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore creazione bucket : ' + err);
                        }

                        
                            
                                var i = 0;

                                let delay = 1000;

                                crea_fatture_in_aws(invoices, i, delay, url, org, req, s3, token, url_key, req.query.mail);


                     }).catch(e=>console.log(e));   
                 }).catch(function(err) {
                    return res.status(400).render('save_poppixsrl_bill_info_v1: ' + err);
                });  
            }).catch(function(err) {
                return res.status(400).render('insert_token: ' + err);
            }); 

        });

    });  
};

exports.invoice_info = function(req,res){
    
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT get_export_invoice_expense_v1(\'' +
        req.headers['host'].split(".")[0] + '\',\'invoice\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(actual) {

        console.log('actual', actual[0].get_export_invoice_expense_v1);

        if(actual[0].get_export_invoice_expense_v1.status == -1){
            res.status(422).send(utility.error422('Processo già attivo', 'Processo di esportazione già in atto', 'Processo di esportazione già in atto', '422'));
        }
        else{
             sequelize.query('SELECT organizationfree_data(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(org) {

                indexes.invoice(req, null, function(err, response) {
                    if (err) {
                        console.log(err);
                        return res.status(404).render('invoice_index: ' + err);
                    }

                    if(response.data && response.data.length > 0){
                        res.status(200).send({
                            'exports': actual[0].get_export_invoice_expense_v1.exports,
                            'mail': req.query.mail,
                            'time_to_mail': (3*response.data.length)/60});
                    }else{
                        res.status(404).send({});
                    }

                });

            });  
        }
       
    }); 
};

exports.expense_info = function(req, res){
    if (!req.user.role[0].json_build_object.attributes.expense_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT get_export_invoice_expense_v1(\'' +
        req.headers['host'].split(".")[0] + '\',\'expense\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(actual) {

        if(actual[0].get_export_invoice_expense_v1.status == -1){
            res.status(422).send(utility.error422('Processo già attivo', 'Processo di esportazione già in atto', 'Processo di esportazione già in atto', '422'));
        }
        else{
            sequelize.query('SELECT organizationfree_data(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(org) {

                indexes.expense(req, null, function(err, response) {
                    if (err) {
                        return res.status(400).render('expense_index: ' + err);
                    }

                

                    if(response.data && response.data.length > 0){
                        res.status(200).send({
                            'exports': actual[0].get_export_invoice_expense_v1.exports,
                            'mail': req.query.mail,
                            'time_to_mail': (3*response.data.length)/60});
                    }else{
                        res.status(404).send({});
                    }
                });

            });  
        }
    });  
};

exports.expense_to_aws = function(req, res){
    if (!req.user.role[0].json_build_object.attributes.expense_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT organizationfree_data(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(org) {

        indexes.expense(req, null, function(err, response) {
            if (err) {
                return res.status(400).render('expense_index: ' + err);
            }

            let expenses = response.data.map(function(expense) {
                return {
                    'name': (expense.attributes.ref_number ? expense.attributes.ref_number.replace(/\//g, '_') : 'SENZA_NUMERO') + ' del ' + moment(expense.attributes.date).format('DD-MM-YYYY'),
                    'id': expense.id,
                    'is_creditnote': expense.attributes.is_creditnote
                }
            });

            if(expenses && expenses.length > 0){
                res.status(200).send({
                    'mail': req.query.mail,
                    'time_to_mail': 3*expenses.length});
            }else{
                res.status(404).send({});
            }

            /*
             *  AWS configuration for Aws S3 service
             *
             */
             const s3 = new S3Client({
                region: config.aws_s3.region,
                credentials:{
                    accessKeyId: config.aws_s3.accessKeyId,
                    secretAccessKey: config.aws_s3.secretAccessKey,
                }
            });


            var time_now = moment().format('HHmmss');

            /* CREO IL BUCKET IN S3 */
            var params = {
                Bucket: 'exports.beebeeboard.com',
                Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/expenses/'+time_now,
                ACL: 'private',
                ServerSideEncryption: 'AES256',
                Body: 'no body'
            };

            var token = utils.uid(config.token.accessTokenLength);
            var url_key = req.headers['host'].split(".")[0] + '/' + req.user.id + '/expenses/'+time_now;
            
            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                req.user.id + ',null,\'export\',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(res) {


                sequelize.query('SELECT insert_export_invoice_expense(\'' + req.headers['host'].split(".")[0] + '\','+
                        req.user.id+',\''+
                        url_key+'\',\'expense\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(abc) {


                    const command2 = new PutObjectCommand(params);
                    getSignedUrl(s3, command2, { expiresIn: 3600 }).then(url =>{
                    //s3.getSignedUrl('putObject', params, function (err, url) {
                        if (err) {
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore creazione bucket : ' + err);
                        }

                        
                            
                                var i = 0;

                                let delay = 1000;

                                crea_expense_in_aws(expenses, i, delay, url, org, req, s3, token, url_key, req.query.mail);


                     }).catch(e=>console.log(e));   

                    
                 }).catch(function(err) {
                    return res.status(400).render('save_poppixsrl_bill_info_v1: ' + err);
                });  
            }).catch(function(err) {
                return res.status(400).render('insert_token: ' + err);
            }); 


        });
    });
};

function crea_fatture_in_aws(invoices, i, delay, url, org, req, s3, token, url_key, mail){

    console.log('Funzione '+i);
    setTimeout(function request() {
        sequelize.query('SELECT exist_export_invoice_expense(\'' + req.headers['host'].split(".")[0] + '\',\''+
            url_key+'\',\'invoice\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            }).then(function(datas) {
                if(datas[0]['exist_export_invoice_expense']){
                    
                        let inv = invoices[i];

                        req.params.id = inv.id;

                        finds.find_invoice(req, function(err, results) {

                            if (err) {
                                console.log(err);
                                //return res.status(500).send(err);
                            } else {

                                var url_pdf = inv.url_pdf;
                                var url_xml = inv.url_xml;
                                var document_type = inv.document_type;
                                var date_invoice = inv.date;
                                var num_invoice = inv.number;
                                var piva_emesso = inv.piva;
                                var nc = inv.is_creditnote === true ? 'NC' : '';
                                var anno = moment(inv.date).format('YYYY');

                                async.parallel({
                                   
                                    pdf: function(callback) {
                                        var invoice = results;
                                        var doc_detail = generate_file_name(invoice);

                                        var pdf_file_name = (invoice.data.attributes.pdf_file_name && invoice.data.attributes.pdf_file_name != '') ? invoice.data.attributes.pdf_file_name : 'pdf.php';
                                        var url = `https://asset.beebeeboard.com/pdf_create/api/${pdf_file_name}?organization=${req.headers['host'].split(".")[0]}&invoice_id=${invoice.data.id}&test_invoice_style=&token=${token}&$_t=${moment().format('HHmmss')}`;
                                        var richiesta = https.get(url, function(result) {

                                            var chunks = [];

                                            result.on('data', function(chunk) {
                                                chunks.push(chunk);
                                            });

                                            result.on('end', function() {
                                                var pdfData = new Buffer.concat(chunks);
                                                let xml_name = (invoice.data.attributes.filename_pdf.replace(/\//g, '_'));
                                               
                                                var params = {
                                                    Bucket: 'exports.beebeeboard.com',
                                                    Key: url_key + '/PDF/' + xml_name +'.pdf',
                                                    ACL: 'private',
                                                    ServerSideEncryption: 'AES256',
                                                    ContentType: 'application/pdf',
                                                    Body: pdfData
                                                };

                                                const command = new PutObjectCommand(params);
                                                s3.send(command, function(err, data) {

                                                //s3.putObject(params, function(err, data) {
                                                    if (err) {
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                    }

                                                    let file = {};

                                                    file.file_name = xml_name;
                                                    file.file_type = 'pdf';
                                                    file.file_size = pdfData.length;
                                                    file.file_url = url_key + '/PDF/' + xml_name +'.pdf';
                                                    file.is_thumbnail = false;
                                                    file.file_etag = data.ETag;
                                                    callback(null, file);

                                                });

                                                
                                            });
                                        }).on('error', function(err) {

                                            callback(err, null);
                                        });

                                        richiesta.end();    
                                    },
                                    xml: function(callback) {
                                        
                                        fattureXml.generaFatturaBase(results, token, function(err, response) {
                                            if (err) {
                                                callback(err, null);
                                            } else {
                                                response['v1:FatturaElettronica']['FatturaElettronicaHeader']['TerzoIntermediarioOSoggettoEmittente'] = null;
                                                response['v1:FatturaElettronica']['FatturaElettronicaHeader']['SoggettoEmittente'] = null;
                                                response['v1:FatturaElettronica']['FatturaElettronicaHeader']['DatiTrasmissione']['IdTrasmittente'] = fattureXml.getIdFiscaleIVAEx(results.data.attributes.society_piva, 'cedente', results);

                                                var xmlData = fattureXml.generaXmlFattura(response);

                                                var piva_name = '';
                                                if (results.data.attributes.society_piva && isLetter(results.data.attributes.society_piva[0]) && isLetter(results.data.attributes.society_piva[1])) {
                                                    piva_name = results.data.attributes.society_piva;
                                                } else {
                                                    piva_name = 'IT' + results.data.attributes.society_piva;
                                                }

                                                
                                                let xml_name = (piva_emesso + '_' + results.data.attributes.filename_xml.replace(/\//g, '_'));
                                                var params = {
                                                    Bucket: 'exports.beebeeboard.com',
                                                    Key: url_key+'/XML/'+xml_name+'.xml',
                                                    ACL: 'private',
                                                    ServerSideEncryption: 'AES256',
                                                    //ContentDisposition : 'attachment; filename="' + file.originalname + '"',
                                                    ContentType: 'application/xml',
                                                    Body: xmlData
                                                };

                                                const command = new PutObjectCommand(params);
                                                s3.send(command, function(err, data) {
                                                //s3.putObject(params, function(err, data) {
                                                    if (err) {
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                    }
                                                    
                                                    let file = {};

                                                
                                                    file.file_name = xml_name;
                                                    file.file_type = 'xml';
                                                    file.file_size = xmlData.length;
                                                    file.file_url = url_key + '/XML/' + xml_name;
                                                    file.is_thumbnail = false;
                                                    file.file_etag = data.ETag;
                                                    callback(null, file);

                                                });


                                               
                                            }



                                        });
                                    },
                                    allegati: function(callback){
                                        var invoice = results;
                                        if(invoice.included && invoice.included.length > 0){
                                            var cnt_file1 = 0;
                                            console.log('LENGTH:'+invoice.included.length);
                                            invoice.included.forEach(function(includ) {

                                                if(includ.type == 'file'){
                                                    console.log('ENTRO');
                                                    var params = {
                                                        CopySource: 'data.beebeeboard.com/'+includ.attributes.file_url,
                                                        Bucket: 'exports.beebeeboard.com',
                                                        Key: url_key+'/ALLEGATI/'+inv.name.replace(/\//g, '_')+'_'+includ.attributes.file_name,
                                                        ACL: 'private',
                                                        ServerSideEncryption: 'AES256'
                                                    };

                                                     const command = new CopyObjectCommand(params);
                                                    s3.send(command, function(err, result) {
                                                    //s3.getObject(params, function(err, result) {
                                                        
                                                            if (err) {
                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                            }
                                                            if(cnt_file1++ >= invoice.included.length-1){
                                                                console.log('0 '+cnt_file1);
                                                                callback(null, null);
                                                            }
                                                               
                                                                
                                                    });
                                                }else{
                                                    if(cnt_file1++ >= invoice.included.length-1){
                                                        console.log('2 '+cnt_file1);
                                                        callback(null, null);
                                                    }
                                                }
                                            });
                                        }else
                                        {
                                            console.log('3 '+cnt_file1);
                                            callback(null, null);
                                        }
                                    }

                                }, function(err1, final) {
                                    console.log(err1);
                                    
                                    if (err1) {
                                        console.log('Error1 ' + err1);
                                    } else {
                                        
                                        if(i++ < invoices.length - 1){

                                            crea_fatture_in_aws(invoices, i, delay, url, org, req, s3, token, url_key, mail);
                                        }else{
                                            
                                            setTimeout(function (){
                                                /* ZIP FILES */
                                                const filesArray = []
                                                const filesName = []
                                               
                                                //const XmlStream = require('xml-stream')

                                               
                                                //s3.putObject(params, function(err, data) {
                                                const command1 = new ListObjectsV2Command({
                                                  Bucket: 'exports.beebeeboard.com',
                                                  Prefix: url_key
                                                });

                                                
                                                s3.send(command1, function(err, blob){
                                                    
                                                    console.log('POST ListObjectsV2Command');

                                                    //const xml = new XmlStream(blob);
                                                   // xml.collect('Key')
                                                    //xml.on('endElement: Key', function(item) {
                                                   
                                                     
                                                    var cnt = 0;
                                                    blob.Contents.forEach(function(json_data){
                                                        //item['$text'].substr(url_key.length);
                                                        filesName.push({'name': json_data.Key.substr(url_key.length)});
                                                        filesArray.push(json_data.Key.substr(url_key.length));
                                                        
                                                        if(cnt++ >= blob.Contents.length -1){
                                                            
                                                            zip(filesArray, filesName, url_key, s3, org, document_type,req, mail);
                                                        }
                                                    })
                                                    
                                                });
                                           
                                                
                                            }, 5000);
                
                                        }
                                        
                                    }


                                });
                            }
                        });
                }else
                {
                    return 1; 
                }
            }).catch(function(err) {
                    console.log(err);
                    return 1;
            });
    }, delay);
                
           
    
}

function crea_expense_in_aws(expenses, i, delay, url, org, req, s3, token, url_key, mail){

    console.log('Funzione '+i);

   
    setTimeout(function request() {
         sequelize.query('SELECT exist_export_invoice_expense(\'' + req.headers['host'].split(".")[0] + '\',\''+
            url_key+'\',\'expense\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            }).then(function(datas) {
                if(datas[0]['exist_export_invoice_expense']){
                let inv = expenses[i];

                req.params.id = inv.id;

                finds.find_expense(req, function(err, results) {

                    if (err) {
                        console.log(err);
                        //return res.status(500).send(err);
                    } else {

                        var cnt_file = 0;
                        var final_file = 0;

                        

                            async.parallel({
                               
                                file: function(callback) {
                                    var expense_ = results;
                                        if(expense_.included && expense_.included.length > 0){
                                            var cnt_file1 = 0;
                                            console.log('LENGTH:'+expense_.included.length);
                                            expense_.included.forEach(function(includ) {

                                                if(includ.type == 'files'){

                                                    var fornitore_name = results.data.attributes.contact_name;
                                                    if (!fornitore_name) {
                                                        var arrName = [];
                                                        if (results.data.attributes.contact_last_name) {
                                                            arrName.push(results.data.attributes.contact_last_name);
                                                        }

                                                        if (results.data.attributes.contact_first_name) {
                                                            arrName.push(results.data.attributes.contact_first_name);
                                                        }

                                                        fornitore_name = arrName.join('_');
                                                    }

                                                    fornitore_name = fornitore_name.replace(/[^a-z0-9]/gi, '_');

                                                    var name = includ.attributes.file_name.replace('.' + includ.attributes.file_type, '');
                                                    if (results.data.attributes.xml_name) {
                                                        name = results.data.attributes.xml_name.replace(/\//g, '_').replace('.xml', '').replace('.p7m', '');
                                                    } else if (results.data.attributes.ref_number) {
                                                        name += '_' + results.data.attributes.ref_number;
                                                    }

                                                    if (results.data.attributes.is_creditnote) {
                                                        name = 'NC_' + name;
                                                    }

                                                    name = name.replace(/[^a-z0-9]/gi, '_');
                                                    name = fornitore_name + '_' + name + '_' + results.data.attributes.id +'_'+includ.attributes.id;

                                                    var ret = name + '.' + includ.attributes.file_type;

                                                    console.log('ENTRO');
                                                    var params = {
                                                        CopySource: 'data.beebeeboard.com/'+includ.attributes.file_url,
                                                        Bucket: 'exports.beebeeboard.com',
                                                        Key: url_key+'/PASSIVO/'+ret,
                                                        ACL: 'private',
                                                        ServerSideEncryption: 'AES256',
                                                    };

                                                     const command = new CopyObjectCommand(params);
                                                    s3.send(command, function(err, result) {
                                                    //s3.getObject(params, function(err, result) {
                                                       
                                                        
                                                            if (err) {
                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                            }
                                                            if(cnt_file1++ >= expense_.included.length-1){
                                                                console.log('0 '+cnt_file1);
                                                                callback(null, null);
                                                            }
                                                          
                                                    });
                                                }else{
                                                    if(cnt_file1++ >= expense_.included.length-1){
                                                        console.log('2 '+cnt_file1);
                                                        callback(null, null);
                                                    }
                                                }
                                            }); // END FOREACH LENGTH
                                        }else
                                        {
                                            console.log('3 '+cnt_file1);
                                            callback(null, null);
                                        }
                                    }
                        }, function(err1, final) {
                            if (err1) {
                                console.log('Error1 ' + err1);
                            } else {
                                
                               

                                
                                    if(i++ < expenses.length - 1){
                                        crea_expense_in_aws(expenses, i, delay, url, org, req, s3, token, url_key, mail);
                                    }else{
                                        setTimeout(function (){
                                             /* ZIP FILES */
                                                const filesArray = []
                                                const filesName = []
                                               
                                                //const XmlStream = require('xml-stream')

                                                
                                                //s3.putObject(params, function(err, data) {
                                                const command1 = new ListObjectsV2Command({
                                                  Bucket: 'exports.beebeeboard.com',
                                                  Prefix: url_key
                                                });

                                                
                                                s3.send(command1, function(err, blob){
                                                    
                                                    

                                                    //const xml = new XmlStream(blob);
                                                   // xml.collect('Key')
                                                    //xml.on('endElement: Key', function(item) {
                                                   
                                                    
                                                    var cnt = 0;
                                                    blob.Contents.forEach(function(json_data){
                                                        //item['$text'].substr(url_key.length);
                                                        filesName.push({'name': json_data.Key.substr(url_key.length)});
                                                        filesArray.push(json_data.Key.substr(url_key.length));
                                                        
                                                        if(cnt++ >= blob.Contents.length -1){
                                                            
                                                            zip(filesArray, filesName, url_key, s3, org, 'expense',req, mail);
                                                        }
                                                    })
                                                    
                                                });


                                        }, 5000);
            
                                    }
                                
                            }


                        });
                     
                    }
                });
            }
            else{
                return 1; 
            }
        }).catch(function(err) {
            console.log(err);
            return 1;
        });
    }, delay);
            
}

function zip(files, filesName, url_key, s3, org, type, req, mail) {
  
  //const join = require('path').join
  //const s3Zip = require('s3-zip')

  //const s3Zip = require('../utility/zipS3.js');
  //const output = fs.createWriteStream(join(__dirname, 'Fatture_attive.zip'))
  

    s3Zip
   .setArchiverOptions({zlib: {level: 9}})
   .archive({ region: 'eu-central-1', bucket: 'exports.beebeeboard.com', preserveFolderStructure: true }, url_key, files, filesName)
   .pipe(uploadFromStream(s3, url_key, org, type, req, mail))

      
}

function uploadFromStream(s3, url_key, org, type, req, mail) {

  var pass = new stream.PassThrough();
  var fileName = 'fatture';
  switch (type) {
        case 'invoice':
            fileName = 'fattura';
            break;

        case 'estimate':
            fileName = 'preventivi';
            break;

        case 'receipt':
            fileName = 'ricevute';
            break;

        case 'ddt':
            fileName = 'ddt';
            break;

        case 'order':
            fileName = 'ordini';
            break;

        case 'contract':
            fileName = 'contratti';
            break;

        case 'expense':
            fileName = 'fatture_passive';
            break;
    }

  console.log('ARRIVO ALLA FINE');
  //console.log(pass);
  var params_ = { Bucket: 'exports.beebeeboard.com', Key: url_key +'/' + fileName + '.zip', Body: pass}; //7 giorni in secondi

    
      const uploadToS3 = new Upload({
        client: s3,
        params: params_
      });

   
      uploadToS3.on("httpUploadProgress", (progress) => {
        console.log('progresso');
        
      });

      

      uploadToS3.done().then(function(data,err){

       
        const s3Params = {
            Bucket: data.Bucket,
            Key: data.Key,
            Expires: 604800
          };

          const command1 = new GetObjectCommand(s3Params);

        getSignedUrl(s3, command1, { expiresIn: 604800 }).then(data_f =>{
        
          //s3.getSignedUrl('getObject', s3Params, (err, data_f) => {
            
            sequelize.query('SELECT end_export_invoice_expense_v1(\'' + req.headers['host'].split(".")[0] + '\','+
                req.user.id+',\''+
                url_key+'\','+
                ((type == 'expense') ? '\'expense\'' : '\'invoice\'') +',\''+
                data_f+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(abc) {

            });
      
            send_mail_with_link(org, data_f, mail, fileName);
          }).catch(e=>console.log(e));   


         
      }).catch(e=>console.log(e));   
    

return pass;
}

function send_mail_with_link(organization, url, mail_usr, fileName){
    /* INVIO MAIL CON LINK  */
     var org = organization[0].organizationfree_data[0].attributes;
        var mandrill = require('mandrill-api/mandrill');
        var mails = [];
        var mail = {
            "email": mail_usr ? mail_usr : org.admin_mail,
            "name": org.name,
            "type": "to"
        };
        mails.push(mail);

        console.log(mails);
        var template_content = [{
                "name": "LANG",
                "content": 'ITA'
            }, {
                "name": "OBJECT",
                "content": 'Esportazione ' + fileName
            }, {
                "name": "MESSAGE",
                "content": 'File ' + fileName + ' pronto e per essere scaricato.<br />E\' possibile effettuare il download al seguente link: <br/><br/><a href="'+url+'">Scarica ' + fileName + ' da Beebeeboard</a><br/><br/><br/>Il link è valido per 7 giorni dalla data odierna'
            }, {
                "name": "COLOR",
                "content": org.primary_color ? org.primary_color : '#00587D'
            }, {
                "name": "LOGO",
                "content": org.thumbnail_url ? 'https://data.beebeeboard.com/' + org.thumbnail_url + '-small.jpeg' : null
            }],
            message = {
                "global_merge_vars": template_content,
                "metadata": {
                    "organization": org.organization ? org.organization : ''
                },
                "subject": 'Esportazione ' + fileName,
                "from_email": "no-reply@beebeemailer.com",
                "from_name": org.name ? org.name : '',
                "to": mails,
                "headers": {
                    "Reply-To": 'no-reply@beebeemailer.com'
                },
                "tags": ["rapid-message"],
                "subaccount": "beebeeboard",
                "tracking_domain": "mandrillapp.com",
                "google_analytics_domains": ["poppix.it"],
                "preserve_recipients": false
            };

        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);


        mandrill_client.messages.sendTemplate({
            "template_name": "rapid-message",
            "template_content": template_content,
            "message": message,
            "async": true,
            "send_at": false
        }, function(result) {

            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                console.log("Mail sended correctly");

                return 1;

            } else if(result && result[0]) {
                console.log("Mail NOT sended correctly");
               return -1;
            }else
            {
                console.log("Mail NOT sended correctly");
                 return -1;
            }
        }, function(e) {
            // Mandrill returns the error as an object with name and message keys
            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

        });
}

function streamToString (stream) {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on('data', (chunk) => chunks.push(Buffer.from(chunk)));
    stream.on('error', (err) => reject(err));
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  })
}
