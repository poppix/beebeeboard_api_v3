var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var reminder = {};
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (req.body.data.attributes !== undefined) {

        if (req.body.data.attributes.send_to_mail && req.body.data.attributes.send_to_mail != '' && req.body.data.attributes.send_to_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string')) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_to_mail) != true) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }

        if (req.body.data.attributes.send_from_mail && req.body.data.attributes.send_from_mail != '' && req.body.data.attributes.send_from_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string')) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_from_mail) != true) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }
        // Import case
        reminder['send_at'] = (req.body.data.attributes.send_at && moment(req.body.data.attributes.send_at).isValid() ? req.body.data.attributes.send_at : null);
        reminder['send_type'] = (req.body.data.attributes.send_type && utility.check_type_variable(req.body.data.attributes.send_type, 'string') ? req.body.data.attributes.send_type : null);
        reminder['send_when_number'] = req.body.data.attributes.send_when_number;
        reminder['send_when_unit'] = (req.body.data.attributes.send_when_unit && utility.check_type_variable(req.body.data.attributes.send_when_unit, 'string') ? req.body.data.attributes.send_when_unit : null);
        reminder['send_to'] = (req.body.data.attributes.send_to && utility.check_type_variable(req.body.data.attributes.send_to, 'string') ? req.body.data.attributes.send_to.replace(/'/g, "''''") : null);
        reminder['send_to_id'] = (req.body.data.attributes.send_to_id && !utility.check_id(req.body.data.attributes.send_to_id) ? req.body.data.attributes.send_to_id : null);
        reminder['send_to_mail'] = (req.body.data.attributes.send_to_mail && utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string') ? req.body.data.attributes.send_to_mail : null);
        reminder['send_to_mobile'] = (req.body.data.attributes.send_to_mobile && utility.check_type_variable(req.body.data.attributes.send_to_mobile, 'string') ? req.body.data.attributes.send_to_mobile : null);
        reminder['send_from_id'] = (req.body.data.attributes.send_from_id && !utility.check_id(req.body.data.attributes.send_from_id) ? req.body.data.attributes.send_from_id : null);
        reminder['send_from_mail'] = (req.body.data.attributes.send_from_mail && utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string') ? req.body.data.attributes.send_from_mail : null);
        reminder['is_client'] = req.body.data.attributes.is_client;
        reminder['_tenant_id'] = req.user._tenant_id;
        reminder['organization'] = req.headers['host'].split(".")[0];
        reminder['invoice_id'] = ((req.body.data.relationships && req.body.data.relationships.invoice && req.body.data.relationships.invoice.data && req.body.data.relationships.invoice.data.id && !utility.check_id(req.body.data.relationships.invoice.data.id)) ? req.body.data.relationships.invoice.data.id : null);
        reminder['expense_id'] = ((req.body.data.relationships && req.body.data.relationships.expense && req.body.data.relationships.expense.data && req.body.data.relationships.expense.data.id && !utility.check_id(req.body.data.relationships.expense.data.id)) ? req.body.data.relationships.expense.data.id : null);
        reminder['event_id'] = ((req.body.data.relationships && req.body.data.relationships.event && req.body.data.relationships.event.data && req.body.data.relationships.event.data.id && !utility.check_id(req.body.data.relationships.event.data.id)) ? req.body.data.relationships.event.data.id : null);
        reminder['todo_id'] = ((req.body.data.relationships && req.body.data.relationships.todo && req.body.data.relationships.todo.data && req.body.data.relationships.todo.data.id && !utility.check_id(req.body.data.relationships.todo.data.id)) ? req.body.data.relationships.todo.data.id : null);
        reminder['mongo_id'] = null;

        sequelize.query('SELECT insert_reminder(\'' +
                JSON.stringify(reminder) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_reminder;
                sequelize.query('SELECT set_reminder_send_at(' +
                        reminder['event_id'] + ',' +
                        reminder['todo_id'] + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        reminder['send_when_number'] + ',\'' +
                        reminder['send_when_unit'] + '\',' +
                        datas[0].insert_reminder + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(send) {

                        console.log(send);

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'reminder',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_reminder,
                            'Aggiunto promemoria  ' + datas[0].insert_reminder
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });
                    }).catch(function(err) {
                        return res.status(400).render('reminder_insert: ' + err);
                    });

            }).catch(function(err) {
                return res.status(400).render('reminder_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};



/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (req.body.data.attributes !== undefined) {

        if (req.body.data.attributes.send_to_mail && req.body.data.attributes.send_to_mail != '' && req.body.data.attributes.send_to_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string')) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_to_mail) != true) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }

        if (req.body.data.attributes.send_from_mail && req.body.data.attributes.send_from_mail != '' && req.body.data.attributes.send_from_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string')) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_from_mail) != true) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }


        sequelize.query('SELECT update_reminder(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                (req.body.data.attributes.send_type && utility.check_type_variable(req.body.data.attributes.send_type, 'string') ? req.body.data.attributes.send_type : null) + '\',' +
                req.body.data.attributes.send_when_number + ',\'' +
                (req.body.data.attributes.send_when_unit && utility.check_type_variable(req.body.data.attributes.send_when_unit, 'string') ? req.body.data.attributes.send_when_unit : null) + '\',\'' +
                (req.body.data.attributes.send_to && utility.check_type_variable(req.body.data.attributes.send_to, 'string') ? req.body.data.attributes.send_to.replace(/'/g, "''''") : null) + '\',' +
                (req.body.data.attributes.send_to_id && !utility.check_id(req.body.data.attributes.send_to_id) ? req.body.data.attributes.send_to_id : null) + ',\'' +
                (req.body.data.attributes.send_to_mail && utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string') ? req.body.data.attributes.send_to_mail : null) + '\',\'' +
                (req.body.data.attributes.send_to_mobile && utility.check_type_variable(req.body.data.attributes.send_to_mobile, 'string') ? req.body.data.attributes.send_to_mobile : null) + '\',' +
                (req.body.data.attributes.send_from_id && !utility.check_id(req.body.data.attributes.send_from_id) ? req.body.data.attributes.send_from_id : null) + ',\'' +
                (req.body.data.attributes.send_from_mail && utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string') ? req.body.data.attributes.send_from_mail : null) + '\',' +
                req.body.data.attributes.is_client + ',' +
                ((req.body.data.relationships && req.body.data.relationships.invoice && req.body.data.relationships.invoice.data && req.body.data.relationships.invoice.data.id && !utility.check_id(req.body.data.relationships.invoice.data.id)) ? req.body.data.relationships.invoice.data.id : null) + ',' +
                ((req.body.data.relationships && req.body.data.relationships.expense && req.body.data.relationships.expense.data && req.body.data.relationships.expense.data.id && !utility.check_id(req.body.data.relationships.expense.data.id)) ? req.body.data.relationships.expense.data.id : null) + ',' +
                ((req.body.data.relationships && req.body.data.relationships.event && req.body.data.relationships.event.data && req.body.data.relationships.event.data.id && !utility.check_id(req.body.data.relationships.event.data.id)) ? req.body.data.relationships.event.data.id : null) + ',' +
                ((req.body.data.relationships && req.body.data.relationships.todo && req.body.data.relationships.todo.data && req.body.data.relationships.todo.data.id && !utility.check_id(req.body.data.relationships.todo.data.id)) ? req.body.data.relationships.todo.data.id : null) +
                ');', {

                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                sequelize.query('SELECT set_reminder_send_at(' +
                        ((req.body.data.relationships && req.body.data.relationships.event && req.body.data.relationships.event.data && req.body.data.relationships.event.data.id) ? req.body.data.relationships.event.data.id : null) + ',' +
                        ((req.body.data.relationships && req.body.data.relationships.todo && req.body.data.relationships.todo.data && req.body.data.relationships.todo.data.id) ? req.body.data.relationships.todo.data.id : null) + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.body.data.attributes.send_when_number + ',\'' +
                        req.body.data.attributes.send_when_unit + '\',' +
                        req.params.id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(send) {

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'reminder',
                            'udpate',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            req.params.id,
                            'Modificato promemoria  ' + req.params.id
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });
                    }).catch(function(err) {
                        return res.status(400).render('reminder_update: ' + err);
                    });
            }).catch(function(err) {
                return res.status(400).render('reminder_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    sequelize.query('SELECT delete_reminder_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'reminders',
                    'id': req.params.id
                }
            });

        }).catch(function(err) {
            return res.status(400).render('reminder_destroy: ' + err);
        });
};

exports.send_sms_now = function(req, res) {
    var reminder = {};
  
    sequelize.query('SELECT send_sms_now(' +
        req.user._tenant_id+',\''+
        req.headers['host'].split(".")[0] +'\','+
        req.body.event_id + ',\'' +
        req.body.send_type + '\','+
        (req.body.contact_id ? req.body.contact_id : null)+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {

        res.status(200).json({});
           

    }).catch(function(err) {
        return res.status(400).render('send_sms_now: ' + err);
    });
    
};


exports.get_communications = function(req,res){

   utility.get_parameters_v1(req, 'event', function(err, parameters) {
         if (err) {
            callback_(err, null);
        }

         sequelize.query('SELECT get_communications_v1(' +
                 parameters.page_count + ',' +
                 (parameters.page_num - 1) * parameters.page_count + ',' +
                 req.user._tenant_id + ',\'' +
                 req.headers['host'].split(".")[0] + '\',' +
                 req.user.id +','+
                 req.user.role_id + ',' +
                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                 parameters.sortDirection + ',' +
                 parameters.all_any + ',' +
                 parameters.start_date + ', ' +
                 parameters.end_date + ',' +
                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ','+
                 (req.query.status ? '\''+req.query.status.replace(/'/g, "''''")+'\'' : null)+','+
                 (req.query.mode ? '\''+req.query.mode.replace(/'/g, "''''")+'\'' : null)+','+
                 (req.query.sent ? req.query.sent : null)+');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {
                res.json(
                     datas[0].get_communications_v1[0]
                );
             }).catch(function(err) {
                 console.log(err);
                 console.log('RIENTRO QUI');
                  return res.status(400).render('error paramenter: ' + err);
             });
  });

}


exports.communications_stats = function(req,res){

   utility.get_parameters_v1(req, 'event', function(err, parameters) {
         if (err) {
            callback_(err, null);
        }

         sequelize.query('SELECT communications_stats(' +
                 parameters.page_count + ',' +
                 (parameters.page_num - 1) * parameters.page_count + ',' +
                 req.user._tenant_id + ',\'' +
                 req.headers['host'].split(".")[0] + '\',' +
                 req.user.id +','+
                 req.user.role_id + ',' +
                 (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                 (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                 parameters.sortDirection + ',' +
                 parameters.all_any + ',' +
                 parameters.start_date + ', ' +
                 parameters.end_date + ',' +
                 (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' +
                 (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {
                res.json(
                     datas[0].communications_stats[0]
                );
             }).catch(function(err) {
                 console.log(err);
                 console.log('RIENTRO QUI');
                  return res.status(400).render('error paramenter: ' + err);
             });
  });

}



exports.communications_filter_clinic = function(req,res){

  

  utility.get_parameters_v1(req, 'event', function(err, parameters) {
        sequelize.query('SELECT communication_filter(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (req.query.first_app ? '\''+req.query.first_app+'\'' : null)+','+
            (req.query.last_app ? '\''+req.query.last_app+'\'' : null)+','+
            (req.query.sesso ? req.query.sesso : null)+','+
            (req.query.eta_min ? req.query.eta_min : null)+','+
            (req.query.eta_max ? req.query.eta_max : null)+','+
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ','+
            (req.query.app_status ? req.query.app_status : null)+','+
            (req.query.min_fatt ? req.query.min_fatt : null)+','+
            (req.query.max_fatt ? req.query.max_fatt : null)+','+
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            parameters.sortField + ',' +
            parameters.sortDirection + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
            req.user.role_id+','+
            req.user.id+','+
            (req.query.created ? '\''+req.query.created+'\'' : null)+',' + 
            (req.query.agrements ? '\''+JSON.stringify(req.query.agrements).replace(/'/g, "''''") + '\'' : null)+','+
            (req.query.status ? req.query.status : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
           
                if (err) {
                    console.log(err);
                    return res.status(500).send('Elenco comunicazioni filter' + err);
                }

                
                if(datas && datas[0] && datas[0].communication_filter){
                    res.status(200).json(datas[0].communication_filter);
                }else
                {
                    res.status(404).json({});
                }
                
            
        }).catch(function(err) {
            return res.status(400).render('communication_filter: ' + err);
        }); 
    });

}

exports.communications_filter_scuolasci = function(req,res){

  

  utility.get_parameters_v1(req, 'event', function(err, parameters) {
        sequelize.query('SELECT communication_filter_scuolasci(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (req.query.first_app ? '\''+req.query.first_app+'\'' : null)+','+
            (req.query.last_app ? '\''+req.query.last_app+'\'' : null)+','+
            (req.query.sesso ? req.query.sesso : null)+','+
            (req.query.eta_min ? req.query.eta_min : null)+','+
            (req.query.eta_max ? req.query.eta_max : null)+','+
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ','+
            (req.query.app_status ? req.query.app_status : null)+','+
            (req.query.min_fatt ? req.query.min_fatt : null)+','+
            (req.query.max_fatt ? req.query.max_fatt : null)+','+
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            parameters.sortField + ',' +
            parameters.sortDirection + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
            req.user.role_id+','+
            req.user.id+','+
            (req.query.created ? '\''+req.query.created+'\'' : null)+',' + 
            (req.query.agrements ? '\''+JSON.stringify(req.query.agrements).replace(/'/g, "''''") + '\'' : null)+','+
            (req.query.status ? req.query.status : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
           
                if (err) {
                    console.log(err);
                    return res.status(500).send('Elenco comunicazioni filter Scuola sci' + err);
                }

                
                if(datas && datas[0] && datas[0].communication_filter_scuolasci){
                    res.status(200).json(datas[0].communication_filter_scuolasci);
                }else
                {
                    res.status(404).json({});
                }
                
            
        }).catch(function(err) {
            return res.status(400).render('communication_filter_scuolasci: ' + err);
        }); 
    });

}