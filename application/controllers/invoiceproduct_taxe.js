var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

    var invoiceproduct_taxe = {};

    if (req.body.data.attributes !== undefined) {

        invoiceproduct_taxe['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        invoiceproduct_taxe['rate'] = req.body.data.attributes.rate;
        invoiceproduct_taxe['rank'] = req.body.data.attributes.rank;
        invoiceproduct_taxe['editable'] = req.body.data.attributes.editable !== undefined && utility.check_type_variable(req.body.data.attributes.editable, 'boolean') ? req.body.data.attributes.editable : null;
        invoiceproduct_taxe['is_active'] = req.body.data.attributes.is_active !== undefined && utility.check_type_variable(req.body.data.attributes.is_active, 'boolean') ? req.body.data.attributes.is_active : null;
        invoiceproduct_taxe['sum'] = req.body.data.attributes.sum;
        invoiceproduct_taxe['gt'] = req.body.data.attributes.gt;
        invoiceproduct_taxe['bind'] = req.body.data.attributes.bind;
        invoiceproduct_taxe['is_bollo'] = req.body.data.attributes.is_bollo !== undefined && utility.check_type_variable(req.body.data.attributes.is_bollo, 'boolean') ? req.body.data.attributes.is_bollo : null;;
        invoiceproduct_taxe['tot_bollo'] = req.body.data.attributes.tot_bollo;
        //invoiceproduct_taxe['invoiceproduct_id'] = ((req.body.data.relationships && req.body.data.relationships.invoiceproduct && req.body.data.relationships.invoiceproduct.data && req.body.data.relationships.invoiceproduct.data.id && !utility.check_id(req.body.data.relationships.invoiceproduct.data.id)) ? req.body.data.relationships.invoiceproduct.data.id : null);
        invoiceproduct_taxe['is_iva'] = req.body.data.attributes.is_iva !== undefined && utility.check_type_variable(req.body.data.attributes.is_iva, 'boolean') ? req.body.data.attributes.is_iva : null;;
        invoiceproduct_taxe['is_ritenuta'] = req.body.data.attributes.is_ritenuta !== undefined && utility.check_type_variable(req.body.data.attributes.is_ritenuta, 'boolean') ? req.body.data.attributes.is_ritenuta : null;;
        invoiceproduct_taxe['is_rivalsa'] = req.body.data.attributes.is_rivalsa !== undefined && utility.check_type_variable(req.body.data.attributes.is_rivalsa, 'boolean') ? req.body.data.attributes.is_rivalsa : null;;
        invoiceproduct_taxe['type_rivalsa'] = (req.body.data.attributes.type_rivalsa && utility.check_type_variable(req.body.data.attributes.type_rivalsa, 'string') && req.body.data.attributes.type_rivalsa !== null ? req.body.data.attributes.type_rivalsa.replace(/'/g, "''''") : '');
        invoiceproduct_taxe['bollo_virtuale'] = req.body.data.attributes.bollo_virtuale !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_virtuale, 'boolean') ? req.body.data.attributes.bollo_virtuale : null;;
        
        sequelize.query('SELECT insert_invoiceproduct_taxe_v1(\'' + JSON.stringify(invoiceproduct_taxe) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_invoiceproduct_taxe_v1 && datas[0].insert_invoiceproduct_taxe_v1 != null) {
                    req.body.data.id = datas[0].insert_invoiceproduct_taxe_v1;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_invoiceproduct_taxe_v1,
                        'body': req.body.data.relationships,
                        'model': 'invoiceproduct_taxe',
                        'user_id': req.user.id,
                        'array': ['cumulable_invoiceproduct_taxe']
                    }, function(err, results) {

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'invoiceproduct_taxe',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_invoiceproduct_taxe_v1,
                            'Aggiunta tassa a elemento in fattura ' + datas[0].insert_invoiceproduct_taxe_v1
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    });


                }

            }).catch(function(err) {
                return res.status(400).render('invoiceproduct_taxe_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case

        sequelize.query('SELECT delete_invoiceproduct_taxe_relations(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {

                sequelize.query('SELECT update_invoiceproduct_taxe_v1(' +
                        req.params.id +
                        ',\'' + (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') +
                        '\',' + req.body.data.attributes.rate +
                        ',' + req.body.data.attributes.rank +
                        ',' + (req.body.data.attributes.editable !== undefined && utility.check_type_variable(req.body.data.attributes.editable, 'boolean') ? req.body.data.attributes.editable : null) +
                        ',' + (req.body.data.attributes.is_active !== undefined && utility.check_type_variable(req.body.data.attributes.is_active, 'boolean') ? req.body.data.attributes.is_active : null) +
                        ',' + req.body.data.attributes.sum +
                        ',' + req.body.data.attributes.gt +
                        ',' + req.body.data.attributes.bind +
                        ',' + (req.body.data.attributes.is_bollo !== undefined && utility.check_type_variable(req.body.data.attributes.is_bollo, 'boolean') ? req.body.data.attributes.is_bollo : null) +
                        ',' + req.body.data.attributes.tot_bollo + ',\'' +
                        req.headers['host'].split(".")[0] + '\','+
                        ',' + (req.body.data.attributes.is_iva !== undefined && utility.check_type_variable(req.body.data.attributes.is_iva, 'boolean') ? req.body.data.attributes.is_iva : null) +
                        ',' + (req.body.data.attributes.is_ritenuta !== undefined && utility.check_type_variable(req.body.data.attributes.is_ritenuta, 'boolean') ? req.body.data.attributes.is_ritenuta : null) +
                        ',' + (req.body.data.attributes.is_rivalsa !== undefined && utility.check_type_variable(req.body.data.attributes.is_rivalsa, 'boolean') ? req.body.data.attributes.is_rivalsa : null) +
                        ',\'' + (req.body.data.attributes.type_rivalsa && utility.check_type_variable(req.body.data.attributes.type_rivalsa, 'string') && req.body.data.attributes.type_rivalsa !== null ? req.body.data.attributes.type_rivalsa.replace(/'/g, "''''") : '') +
                        '\',' + (req.body.data.attributes.bollo_virtuale !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_virtuale, 'boolean') ? req.body.data.attributes.bollo_virtuale : null) +');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'invoiceproduct_taxe',
                            'user_id': req.user.id,
                            'array': ['cumulable_invoiceproduct_taxe']
                        }, function(err, results) {

                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'invoiceproduct_taxe',
                                'update',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                req.params.id,
                                'Modificata tassa a elemento in fattura ' + req.params.id
                            );
                            res.json({
                                'data': req.body.data,
                                'relationships': req.body.data.relationships
                            });

                        });

                    }).catch(function(err) {
                        return res.status(400).render('invoiceproduct_taxe_update: ' + err);
                    });

            }).catch(function(err) {
                return res.status(400).render('invoiceproduct_taxe_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_invoiceproduct_taxe_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'invoiceproduct_taxes',
                    'id': datas[0].delete_invoiceproduct_taxe_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('invoiceproduct_taxe_destroy: ' + err);
        });
};