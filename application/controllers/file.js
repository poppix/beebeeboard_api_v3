const { S3Client, PutObjectCommand, HeadObjectCommand, GetObjectCommand, DeleteObjectCommand, PutObjectAclCommand } = require('@aws-sdk/client-s3');
var 
    
    moment = require('moment'),
    fs = require('fs'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js'),
    async = require('async'),
    config = require('../../config'),
    indexes = require('../utility/indexes.js'),
    sharp = require('sharp'),
    multer = require('multer');


exports.insert_from_url = function(req, res) {

        if (req.body.url) {
            put_from_url(req.body.url, 'data.beebeeboard.com', req.body.key, req.body.filename, req.body.extension, function(err, result) {
                if (err) {
                    console.log(err);
                }

                var file = {};
                file.file_name = req.body.filename;
                file.file_type = req.body.extension;
                file.file_url = req.body.key;

                file.is_thumbnail = false;

                file.file_etag = result.ETag;
                file.upload_date = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                file.uploader_id = req.user.id;
                file._tenant_id = req.user._tenant_id;
                file.organization = req.headers['host'].split(".")[0];
                file.mongo_id = null;


                sequelize.query('SELECT insert_file(\'' + JSON.stringify(file).replace(/'/g, "''''") + '\',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(datas) {
                        file.id = datas[0].insert_file;

                        file._tenant_id = null;

                        utility.sanitize_file_relation(req.body.model, req.body.model_id, datas[0].insert_file, req.user.id, req.headers['host'].split(".")[0], req.user._tenant_id, res);

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'file',
                            'insert',
                            req.user.id,
                            JSON.stringify(file),
                            datas[0].insert_file,
                            'Aggiunto file ' + datas[0].insert_file
                        );

                        console.log('Uploaded data successfully!');

                        utility.save_socket(req.headers['host'].split(".")[0], 'file', 'insert', req.user.id, datas[0].insert_file, file);
        

                        res.status(200).send();

                    }).catch(function(err) {
                        return res.status(400).render('file_upload: ' + err);
                    });

            });
        } else {
            return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
}
    /**
     * 
     * @param {Object} req
     * @param {Object} res
     * 
     * Upload a file in Amazon structure, the file will be saved in
     * a bucket and in a folder tree structured: user_id/model_id/namefile
     * 
     */

exports.insert = function(req, res) {
    var param = [],
        files = [],
        file = {},
        pending = 0,
        content = '',
        tot_size = 0,
        cnt = 0;


    /*
     *	AWS configuration for Aws S3 service
     *
     */
    /*AWS.config.update({
        accessKeyId: config.aws_s3.accessKeyId,
        secretAccessKey: config.aws_s3.secretAccessKey,
        region: config.aws_s3.region
    });*/

    //var s3 = new AWS.S3();
    const s3 = new S3Client({
        region: config.aws_s3.region,
        credentials:{
            accessKeyId: config.aws_s3.accessKeyId,
            secretAccessKey: config.aws_s3.secretAccessKey,
        }
    });
    

    if (req.files) {

        req.files.map((file) => {
            tot_size += file.size;

            var arrExt = file.originalname.split('.');
            file.extension = arrExt[arrExt.length - 1];


            sequelize.query('SELECT control_file_size_on_plan(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    tot_size + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(size_ok) {
                    if (size_ok[0].control_file_size_on_plan != 0) {

                        content = set_content(file.extension);
                        file.originalname = file.originalname.replace(/[^a-zA-Z0-9.]/g, '_');

                        async.parallel({
                            path_file: function(callback) {

                                if (file.extension && (file.extension.toLowerCase() == 'jpg' || file.extension.toLowerCase() == 'png' || file.extension.toLowerCase() == 'jpeg' || file.extension.toLowerCase() == 'bmp' || file.extension.toLowerCase() == 'svg' || file.extension.toLowerCase() == 'gif' || file.extension.toLowerCase() == 'heic' || file.extension.toLowerCase() == 'heif')) {

                                    sharp(file.buffer)
                                        .rotate()
                                        .resize(1024, 1024, {
                                            fit: 'inside',

                                        })
                                        .withMetadata()
                                        .toBuffer()
                                        .then(data => {
                                            var params = {
                                                Bucket: 'data.beebeeboard.com',
                                                Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname + 'thumb-large.jpeg',
                                                ACL: 'private',
                                                ServerSideEncryption: 'AES256',
                                                //ContentDisposition : 'attachment; filename="' + file.originalname + '"',
                                                ContentType: content,
                                                Body: data
                                            };

                                            const command = new PutObjectCommand(params);
                                            s3.send(command, function(err, data) {
                                                if (err) {
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                }

                                                callback(null, req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname + 'thumb');

                                            });

                                        })
                                        .catch(err => {
                                            callback(err, null);
                                        });
                                } else {
                                    callback(null, null);
                                }
                            },
                            path_file1: function(callback) {
                                if (file.extension && (file.extension.toLowerCase() == 'jpg' || file.extension.toLowerCase() == 'png' || file.extension.toLowerCase() == 'jpeg' || file.extension.toLowerCase() == 'bmp' || file.extension.toLowerCase() == 'svg' || file.extension.toLowerCase() == 'gif' || file.extension.toLowerCase() == 'heic' || file.extension.toLowerCase() == 'heif')) {

                                    sharp(file.buffer)
                                        .rotate()
                                        .resize(300, 300, {
                                            fit: 'inside'
                                        })
                                        .withMetadata()
                                        .toBuffer()
                                        .then(data => {
                                            var params = {
                                                Bucket: 'data.beebeeboard.com',
                                                Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname + 'thumb-medium.jpeg',
                                                ACL: 'private',
                                                ServerSideEncryption: 'AES256',
                                                //ContentDisposition : 'attachment; filename="' + file.originalname + '"',
                                                ContentType: content,
                                                Body: data
                                            };

                                            const command = new PutObjectCommand(params);
                                            s3.send(command, function(err, data) {
                                                if (err) {
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                }

                                                callback(null, req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname + 'thumb');

                                            });

                                        })
                                        .catch(err => {
                                            callback(err, null);
                                        });
                                } else {
                                    callback(null, null);
                                }
                            },
                            path_file2: function(callback) {
                                if (file.extension && (file.extension.toLowerCase() == 'jpg' || file.extension.toLowerCase() == 'png' || file.extension.toLowerCase() == 'jpeg' || file.extension.toLowerCase() == 'bmp' || file.extension.toLowerCase() == 'svg' || file.extension.toLowerCase() == 'gif' || file.extension.toLowerCase() == 'heic' || file.extension.toLowerCase() == 'heif')) {

                                    sharp(file.buffer)
                                        .rotate()
                                        .resize(150, 150, {
                                            fit: 'inside'
                                        })
                                        .withMetadata()
                                        .toBuffer()
                                        .then(data => {
                                            var params = {
                                                Bucket: 'data.beebeeboard.com',
                                                Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname + 'thumb-small.jpeg',
                                                ACL: 'private',
                                                ServerSideEncryption: 'AES256',
                                                //ContentDisposition : 'attachment; filename="' + file.originalname + '"',
                                                ContentType: content,
                                                Body: data
                                            };

                                            const command = new PutObjectCommand(params);
                                            s3.send(command, function(err, data) {
                                                if (err) {
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                }

                                                callback(null, req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname + 'thumb');

                                            });

                                        })
                                        .catch(err => {
                                            callback(err, null);
                                        });

                                } else {
                                    callback(null, null);
                                }

                            },
                            upload_file: function(callback) {
                                if (file.extension && (file.extension.toLowerCase() == 'jpg' || file.extension.toLowerCase() == 'png' || file.extension.toLowerCase() == 'jpeg' || file.extension.toLowerCase() == 'bmp' || file.extension.toLowerCase() == 'svg' || file.extension.toLowerCase() == 'gif' || file.extension.toLowerCase() == 'heic' || file.extension.toLowerCase() == 'heif')) {
                                    sharp(file.buffer)
                                        .rotate()
                                        .withMetadata()
                                        .toBuffer()
                                        .then(data => {

                                            var params = {
                                                Bucket: 'data.beebeeboard.com',
                                                Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname,
                                                ACL: 'private',
                                                ServerSideEncryption: 'AES256',
                                                //ContentDisposition : 'attachment; filename="' + file.originalname + '"',
                                                ContentType: content,
                                                Body: data
                                            };

                                            const command = new PutObjectCommand(params);
                                            s3.send(command, function(err, data) {
                                                if (err) {
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                                }

                                                file.file_name = file.originalname;
                                                file.file_type = file.extension;
                                                file.file_size = file.size;
                                                file.file_url = req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname;

                                                if (file.extension && (file.extension.toLowerCase() == 'jpg' || file.extension.toLowerCase() == 'png' || file.extension.toLowerCase() == 'jpeg' || file.extension.toLowerCase() == 'pdf' || file.extension.toLowerCase() == 'bmp' || file.extension.toLowerCase() == 'svg' || file.extension.toLowerCase() == 'gif' || file.extension.toLowerCase() == 'heic' || file.extension.toLowerCase() == 'heif')) {
                                                    file.is_thumbnail = req.query.thumbnail;
                                                } else {
                                                    file.is_thumbnail = false;
                                                }
                                                file.file_etag = data.ETag;
                                                callback(null, file);

                                            });
                                        })
                                        .catch(err => {
                                            callback(err, null);
                                        });
                                }
                                else{
                                    var params = {
                                            Bucket: 'data.beebeeboard.com',
                                            Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname,
                                            ACL: 'private',
                                            ServerSideEncryption: 'AES256',
                                            //ContentDisposition : 'attachment; filename="' + file.originalname + '"',
                                            ContentType: content,
                                            Body: file.buffer
                                        };

                                        const command = new PutObjectCommand(params);
                                            s3.send(command, function(err, data) {
                                            if (err) {
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                            }

                                            file.file_name = file.originalname;
                                            file.file_type = file.extension;
                                            file.file_size = file.size;
                                            file.file_url = req.headers['host'].split(".")[0] + '/' + req.user.id + '/' + req.query.d + '/' + file.originalname;

                                            if (file.extension && (file.extension.toLowerCase() == 'jpg' || file.extension.toLowerCase() == 'png' || file.extension.toLowerCase() == 'jpeg' || file.extension.toLowerCase() == 'pdf' || file.extension.toLowerCase() == 'bmp' || file.extension.toLowerCase() == 'svg' || file.extension.toLowerCase() == 'gif' || file.extension.toLowerCase() == 'heic'  || file.extension.toLowerCase() == 'heif')) {
                                                file.is_thumbnail = req.query.thumbnail;
                                            } else {
                                                file.is_thumbnail = false;
                                            }
                                            file.file_etag = data.ETag;
                                            callback(null, file);

                                        });
                                }
                            }
                        }, function(err, results) {
                            if (err) {
                                return res.status(400).render('file_upload: ' + err);
                            }
                            var file_up = {};

                            file_up['file_name'] = results.upload_file.file_name;
                            file_up['file_type'] = results.upload_file.file_type;
                            file_up['file_size'] = results.upload_file.file_size;
                            file_up['file_url'] = results.upload_file.file_url;
                            file_up['file_etag'] = results.upload_file.file_etag;
                            file_up['thumbnail_url'] = results.path_file;
                            file_up['is_thumbnail'] = results.upload_file.is_thumbnail;
                            file_up['upload_date'] = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                            file_up['uploader_id'] = req.user.id;
                            file_up['_tenant_id'] = req.user._tenant_id;
                            file_up['organization'] = req.headers['host'].split(".")[0];
                            file_up['mongo_id'] = null;

                            sequelize.query('SELECT insert_file(\'' + JSON.stringify(file_up).replace(/'/g, "''''") + '\',\'' +
                                    req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {
                                    file_up['_tenant_id'] = null;
                                    results.upload_file.id = datas[0].insert_file;

                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'file',
                                        'insert',
                                        req.user.id,
                                        JSON.stringify(file_up),
                                        datas[0].insert_file,
                                        'Aggiunto file ' + datas[0].insert_file
                                    );

                                    utility.save_socket(req.headers['host'].split(".")[0], 'file', 'insert', req.user.id, datas[0].insert_file, file_up);
        

                                    if (req.query.tags) {
                                        var tags = req.query.tags.split(',');

                                        tags.forEach(function(tag) {

                                            sequelize.query('SELECT insert_tag(\'' +
                                                    (tag !== null ? tag.replace(/'/g, "''") : '') + '\',\'' +
                                                    req.headers['host'].split(".")[0] + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas_tag) {
                                                    var tag_id = datas_tag[0].insert_tag;
                                                    logs.save_log_model(
                                                        req.user._tenant_id,
                                                        req.headers['host'].split(".")[0],
                                                        'tag',
                                                        'insert',
                                                        req.user.id,
                                                        JSON.stringify('{}'),
                                                        tag_id,
                                                        'Aggiunto tag ' + tag.replace(/'/g, "''") + ' con id ' + tag_id
                                                    );

                                                    sequelize.query('SELECT update_tags(' +
                                                            tag_id + ',' +
                                                            results.upload_file.id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(datas_up) {
                                                            logs.save_log_relation(
                                                                'file',
                                                                'tag',
                                                                results.upload_file.id,
                                                                tag_id,
                                                                'insert',
                                                                req.user.id,
                                                                req.headers['host'].split(".")[0],
                                                                req.user._tenant_id
                                                            );

                                                        }).catch(function(err) {
                                                            console.log(err);
                                                            callback(err, null);
                                                        });

                                                }).catch(function(err) {
                                                    return res.status(400).render('tag_insert: ' + err);
                                                });
                                        });
                                    }

                                    utility.sanitize_file_relation(
                                        req.query.type,
                                        req.query.d,
                                        datas[0].insert_file,
                                        req.user.id,
                                        req.headers['host'].split(".")[0],
                                        req.user._tenant_id, res);

                                    var f = results.upload_file;
                                    if (results.path_file != null)
                                        f.thumbnail_url = results.path_file;

                                    files.push(f);
                                    if (req.query.thumbnail !== null &&
                                        req.query.thumbnail !== false &&
                                        req.query.thumbnail !== 'false') {

                                        sequelize.query('SELECT set_thumbnail(\'' +
                                                req.query.type + '\',' +
                                                req.query.d + ',\'' +
                                                f.thumbnail_url + '\',\'' +
                                                results.upload_file.file_type + '\',\'' +
                                                results.upload_file.file_name + '\',\'' +
                                                results.upload_file.file_etag + '\',\'' +
                                                req.headers['host'].split(".")[0] + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {

                                            }).catch(function(err) {
                                                return res.status(400).render('file_set_thumbnail: ' + err);
                                            });
                                    }


                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' File uploaded');

                                    if (cnt++ >= req.files.length - 1) {
                                        res.send(files);
                                    }

                                }).catch(function(err) {
                                    console.log(err);
                                    return res.status(400).render('file_upload: ' + err);
                                });
                        });
                    } else {
                        return res.status(422).send(utility.error422('id', 'Caricando questi file si supera il limite di Gb per il piano a cui si è abbonati', 'Caricando questi file si supera il limite di Gb per il piano a cui si è abbonati', '422'));
                    }

                });
        });
    } else {
        return res.status(422).send(utility.error422('id', 'Nessun file da caricare', 'Nessun file da caricare', '422'));
    }
};

exports.insert_signed_file = function(req, res) {
    req.setTimeout(500000);
    
    var doc_sign_id = req.body.document_sign_id ? req.body.document_sign_id : null;
    var spunte = req.body.spunte ? req.body.spunte : null;

    console.log(req.body);

    if(doc_sign_id){
        /*AWS.config.update({
            accessKeyId: config.aws_s3.accessKeyId,
            secretAccessKey: config.aws_s3.secretAccessKey,
            region: config.aws_s3.region
        });*/

        //var s3 = new AWS.S3();
        const s3 = new S3Client({
            region: config.aws_s3.region,
            credentials:{
                accessKeyId: config.aws_s3.accessKeyId,
                secretAccessKey: config.aws_s3.secretAccessKey,
            }
        });

        var buf = new Buffer(req.body.file,'base64')

        var data = {
            Bucket: 'data.beebeeboard.com',
            Key: req.headers['host'].split(".")[0] + '/' + req.user.id + '/signed/' + req.body.file_name.replace(/'/g, ""),
            ACL: 'private',
            ServerSideEncryption: 'AES256',
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: 'application/pdf'
        };


      
        const command = new PutObjectCommand(data);
        s3.send(command, function(err, data) {
            if (err) { 
                console.log(data);
                console.log('Error uploading data: ', data); 
                return res.status(400).render('signed_file_upload: ' + data);
            } else {
                console.log('succesfully uploaded the signed file!');

                var file = {};
                file._tenant_id = req.user._tenant_id;
                file.organization = req.headers['host'].split(".")[0];
                file.file_name = req.body.file_name.replace(/'/g, "''''");
                file.file_type = 'pdf';
                file.file_size = buf.length;
                file.file_url = req.headers['host'].split(".")[0] + '/' + req.user.id + '/signed/' + req.body.file_name.replace(/'/g, "");
                file.is_thumbnail = false;
                file.file_etag = data.ETag.replace(/\"/g, "");
                file.upload_date = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                file.uploader_id = req.user.id;

                sequelize.query('SELECT beebeesign_file_signed(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',\'' +
                    JSON.stringify(file) + '\',' +
                    doc_sign_id + ',\'' +
                    JSON.stringify(spunte) + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {

                    console.log(datas[0].beebeesign_file_signed);
                    var file_json = {
                        'attributes': file
                    };

                    utility.save_socket(req.headers['host'].split(".")[0], 'file_signed', datas[0].beebeesign_file_signed, req.user.id, doc_sign_id, file_json);
                        
                    res.status(200).send();

                }).catch(function(err) {
                    console.log(err);
                    return res.status(400).render('signed_file_upload: ' + data);
                });
            }
        });


    }else
    {
        return res.status(422).send(utility.error422('document_sign_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }  
};

exports.change_permission_file = function(req, res) {
  var type_permission = null;
  if (!req.body.file_url) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
      }

  /*AWS.config.update({
        accessKeyId: config.aws_s3.accessKeyId,
        secretAccessKey: config.aws_s3.secretAccessKey,
        region: config.aws_s3.region
    });

  var s3 = new AWS.S3();*/
  const s3 = new S3Client({
        region: config.aws_s3.region,
        credentials:{
            accessKeyId: config.aws_s3.accessKeyId,
            secretAccessKey: config.aws_s3.secretAccessKey,
        }
    });

  

  if (req.body.public != undefined && req.query.public != '') {
    type_permission = req.body.public;
  }

    /*var s3 = new AWS.S3();*/

    var data = {
        Bucket: 'data.beebeeboard.com',
        Key: req.body.file_url,
        ACL: type_permission == 'true' ? 'public-read' : 'private'
        
    };

    const command = new PutObjectAclCommand(data);
    s3.send(command, function(err, data) {
    //s3.putObjectAcl(data, function(err, data1){
        if (err) { 
            console.log(err);
            console.log('Error uploading data: ', err); 
            return res.status(400).render('signed_file_upload: ' + err);
        } else {
            console.log(data);
            console.log('succesfully uploaded the permission file!');

            if(req.body.file_id){
                sequelize.query('SELECT update_file_permission(' +
                    req.body.file_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\','+
                    (type_permission == 'true' ? true : false)+');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas_up) {
                    res.status(200).send({});

                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
            }

            if(req.body.thumbnail_url){

                var data = {
                    Bucket: 'data.beebeeboard.com',
                    Key: req.body.thumbnail_url + '-large.jpeg',
                    ACL: type_permission == 'true' ? 'public-read' : 'private'
                    
                };

                 const command = new PutObjectAclCommand(data);
                s3.send(command, function(err, data1) {
                //s3.putObjectAcl(data, function(err, data1){
                    if (err) { 
                        console.log(data1);
                        console.log('Error uploading data: ', data1); 
                        return res.status(400).render('signed_file_upload: ' + data1);
                    } else {


                    }

                });

                var data = {
                    Bucket: 'data.beebeeboard.com',
                    Key: req.body.thumbnail_url + '-medium.jpeg',
                    ACL: type_permission == 'true' ? 'public-read' : 'private'
                    
                };

                const command1 = new PutObjectAclCommand(data);
                s3.send(command1, function(err, data1) {

                //s3.putObjectAcl(data, function(err, data1){
                    if (err) { 
                        console.log(data1);
                        console.log('Error uploading data: ', data1); 
                        return res.status(400).render('signed_file_upload: ' + data1);
                    } else {


                    }

                });

                var data = {
                    Bucket: 'data.beebeeboard.com',
                    Key: req.body.thumbnail_url + '-small.jpeg',
                    ACL: type_permission == 'true' ? 'public-read' : 'private'
                    
                };
                 
                const command2 = new PutObjectAclCommand(data);
                s3.send(command2, function(err, data1) {

                //s3.putObjectAcl(data, function(err, data1){
                    if (err) { 
                        console.log(data1);
                        console.log('Error uploading data: ', data1); 
                        return res.status(400).render('signed_file_upload: ' + data1);
                    } else {


                    }

                });
            }
        }
    });

   
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * To view and download a file precedentially uploaded
 * 
 */
exports.read = function(req, res) {

    if (!req.query.url ||
    !req.query.name || 
    req.query.url === '' || 
    req.query.name === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
      }

   const s3 = new S3Client({
        region: config.aws_s3.region,
        credentials:{
            accessKeyId: config.aws_s3.accessKeyId,
            secretAccessKey: config.aws_s3.secretAccessKey,
        }
    });


  var params = {
    Bucket: 'data.beebeeboard.com',
    Key: decodeURIComponent(req.query.url)
  };

  if (req.query.etag != undefined && req.query.etag != '') {
    params.IfMatch = req.query.etag;
  }


 const command = new HeadObjectCommand(params);
s3.send(command, function(err, data) {
  //s3.headObject(params, function (err, data) {
    if (err) {
      // an error occurred
      console.log('Errore read file : ' + err);
      return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
      //return next();
    }


    const command = new GetObjectCommand(params);
    s3.send(command, function(err, blob) {



        
        //Add the content type to the response (it's not propagated from the S3 SDK)
        res.set('Content-Type', data.ContentType);
        res.set('Content-Length', data.ContentLength);
        //res.set('Last-Modified', data.LastModified);
        res.set('Content-Disposition', 'inline; filename=' + req.query.name)
        res.set('ETag', data.ETag);

        //Pipe the s3 object to the response
       // stream.pipe(res);
        
        blob.Body.pipe(res);
   });

     });

};

exports.index = function(req, res) {

    var sortField = (req.query.sort === undefined) ? 'file_name' : '\'' + req.query.sort + '\'',
        sortDirection = (req.query.direction === undefined) ? 'desc' : '\'' + req.query.direction + '\'',
        page_num = 1,
        page_count = 50,
        event_id = null,
        contact_id = null,
        product_id = null,
        project_id = null,
        invoice_id = null,
        payment_id = null,
        expense_id = null;

    if (req.query.pag !== undefined && req.query.pag !== '') {
        if (utility.check_id(req.query.pag)) {
            return res.status(422).send(utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_num = req.query.pag;
    }

    if (req.query.xpag !== undefined && req.query.xpag !== '') {
        if (utility.check_id(req.query.xpag)) {
            return res.status(422).send(utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_count = req.query.xpag;
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        if (utility.check_id(req.query.contact_id)) {
            return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        contact_id = req.query.contact_id;
    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {
        if (utility.check_id(req.query.product_id)) {
            return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        product_id = req.query.product_id;
    }

    if (req.query.event_id !== undefined && req.query.event_id != '') {
        if (utility.check_id(req.query.event_id)) {
            return res.status(422).send(utility.error422('event_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        event_id = req.query.event_id;
    }

    if (req.query.project_id !== undefined && req.query.project_id != '') {
        if (utility.check_id(req.query.project_id)) {
            return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        project_id = req.query.project_id;
    }

    if (req.query.invoice_id !== undefined && req.query.invoice_id != '') {
        if (utility.check_id(req.query.invoice_id)) {
            return res.status(422).send(utility.error422('invoice_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        invoice_id = req.query.invoice_id;
    }

    if (req.query.expense_id !== undefined && req.query.expense_id != '') {
        if (utility.check_id(req.query.expense_id)) {
            return res.status(422).send(utility.error422('expense_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        expense_id = req.query.expense_id;
    }


    if (req.query.payment_id !== undefined && req.query.payment_id != '') {
        if (utility.check_id(req.query.payment_id)) {
            return res.status(422).send(utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        payment_id = req.query.payment_id;
    }


    async.parallel({

        data: function(callback) {
            sequelize.query('SELECT files_data_v2(' +
                    page_count + ',' +
                    (page_num - 1) * page_count + ',' +
                    req.user._tenant_id + ',\'' +
                    sortDirection + '\',' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    contact_id + ',' +
                    product_id + ',' +
                    req.user.role_id + ',' +
                    event_id + ',' +
                    project_id + ',' +
                    invoice_id + ',' +
                    payment_id + ',' +
                    expense_id + ',\'' +
                    sortField + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    if (datas[0].files_data_v2 && datas[0].files_data_v2 != null) {
                        callback(null, datas[0].files_data_v2);

                    } else {

                        callback(null, []);
                    }

                }).catch(function(err) {
                    callback(err, null);
                });

        },
        included: function(callback) {
            sequelize.query('SELECT files_included_v2(' +
                    page_count + ',' +
                    (page_num - 1) * page_count + ',' +
                    req.user._tenant_id + ',\'' +
                    sortDirection + '\',' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    contact_id + ',' +
                    product_id + ',' +
                    req.user.role_id + ',' +
                    event_id + ',' +
                    project_id + ',' +
                    invoice_id + ',' +
                    payment_id + ',' +
                    expense_id + ',\'' +
                    sortField + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(inc) {
                    if (inc[0].files_included_v2 && inc[0].files_included_v2 != null) {
                        callback(null, inc[0].files_included_v2);

                    } else {

                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {

        if (err) {
            return res.status(400).render('file_index: ' + err);
        }

        if (results.data.length == 0 || !results.included) {
            return res.status(404).send(utility.error404('id', 'Nessun file trovato', 'Nessun file trovato', '404'));
        }
        console.log(results);
        res.json({
            'data': utility.check_find_datas(results.data, 'files', []),
            'included': utility.check_find_included(results.included, 'files', ['tags', 'contacts'])
        });

    });
};

exports.signed_files = function(req, res) {

   
    indexes.file(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('signed_file_index: ' + err);
        }
        res.json(response.data);
    });
};

exports.signed_files_platform = function(req, res) {

   
    indexes.file(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('signed_file_index: ' + err);
        }
        res.json(response);
    });
};

exports.request_sign_file = function(req, res) {
    
    if(req.body.device_id && (req.body.file_id || req.body.model_id) && req.body.file_url){

        if(!req.body.file_id && req.body.file_url !== undefined){

            var href = new URL('https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/'+req.body.file_url);
           
            href.searchParams.set('access_token', '');
        }
     
   
       sequelize.query('SELECT beebeesign_request_sign_document_v1(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id + ',' +
                req.user.id  + ',' +
                (req.body.model && req.body.model !== undefined ? '\''+req.body.model+'\'' : null)  + ',' +
                (req.body.model_id && req.body.model_id !== undefined ? req.body.model_id : null) + ',\'' +
                (req.body.file_id && req.body.file_id !== undefined  ? ('https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/api/v1/app/file?access_token='+req.headers['authorization'].replace('Bearer ','')+'&name='+req.body.file_name.replace(/'/g, "")+'&url='+req.body.file_url) : href.toString().replace('https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/','')) + '\',' +
                (req.body.file_id && req.body.file_id !== undefined ? req.body.file_id : null) + ','+
                req.body.device_id + ',\''+
                req.body.file_name.replace(/'/g, "") +'\','+
                (req.body.consenso_id && req.body.consenso_id !== undefined ? req.body.consenso_id : null)+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

                sequelize.query('SELECT signed_file_find_data(' +
                    req.user.id  + ',\'' +
                    req.headers['host'].split(".")[0] + '\','+
                    req.user._tenant_id + ',' +
                    datas[0].beebeesign_request_sign_document_v1  + ',' +
                    req.user.role_id+');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(results) {
                    utility.save_socket(req.headers['host'].split(".")[0], 'signed_file', 'sign_document', req.user.id, datas[0].beebeesign_request_sign_document_v1, results[0].signed_file_find_data[0]);
                        
                    res.status(200).send({});
                }).catch(function(err) {
                    return res.status(400).render('beebeesign_request_sign_document_v1: ' + err);
                });

             }).catch(function(err) {
                return res.status(400).render('file_update: ' + err);
            });
    }
    else{
        return res.status(422).send(utility.error422('parametri mancanti', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.find = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT file_find_data_v1(' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',' +
                    req.params.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    if (datas[0].file_find_data_v1 && datas[0].file_find_data_v1 != null) {
                        callback(null, datas[0].file_find_data_v1[0]);

                    } else {

                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });

        },
        included: function(callback) {
            sequelize.query('SELECT file_find_included_v1(' +
                    req.params.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(included) {

                    if (included[0].file_find_included_v1 && included[0].file_find_included_v1 != null) {
                        callback(null, included[0].file_find_included_v1);

                    } else {
                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('file_find: ' + err);
        }

        res.json({
            'data': utility.check_find_datas(results.data, 'files', ['tags']),
            'included': utility.check_find_included(results.included, 'files', ['tags'])
        });

    });
};

exports.update = function(req, res) {
    if (isNaN(parseInt(req.params.id))) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT update_file(' +
            req.params.id + ',\'' +
            (req.body.data.attributes.file_name && utility.check_type_variable(req.body.data.attributes.file_name, 'string') && req.body.data.attributes.file_name !== null ? req.body.data.attributes.file_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.file_type && utility.check_type_variable(req.body.data.attributes.file_type, 'string') && req.body.data.attributes.file_type !== null ? req.body.data.attributes.file_type.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.file_size && utility.check_type_variable(req.body.data.attributes.file_size, 'string') && req.body.data.attributes.file_size !== null ? req.body.data.attributes.file_size.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.file_url && utility.check_type_variable(req.body.data.attributes.file_url, 'string') && req.body.data.attributes.file_url !== null ? req.body.data.attributes.file_url.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.file_etag && utility.check_type_variable(req.body.data.attributes.file_etag, 'string') && req.body.data.attributes.file_etag !== null ? req.body.data.attributes.file_etag.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url.replace(/'/g, "''''") : '') + '\',' +
            req.body.data.attributes.is_thumbnail + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            utility.sanitizeRelations({
                'organization': req.headers['host'].split(".")[0],
                '_tenant': req.user._tenant_id,
                'new_id': req.params.id,
                'body': req.body.data.relationships,
                'model': 'file',
                'user_id': req.user.id,
                'array': ['tag']
            }, function(err, results) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'file',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato file ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });
            });

        }).catch(function(err) {
            return res.status(400).render('file_update: ' + err);
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete a file from the bucket in Amazon
 */
exports.destroy = function(req, res) {

    var file = {};

     const s3 = new S3Client({
        region: config.aws_s3.region,
        credentials:{
            accessKeyId: config.aws_s3.accessKeyId,
            secretAccessKey: config.aws_s3.secretAccessKey,
        }
    });

    if (req.query['url'] !== undefined) {
        var params = {
            Bucket: 'data.beebeeboard.com',
            Key: req.query['url']
        };

        const command = new DeleteObjectCommand(params);
        s3.send(command, function(err, data) {
        //s3.deleteObject(params, function(err, data) {
            if (err)
                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore delete file : ' + err);

            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' File deleted');
            file.file_url = req.query['url'];
            sequelize.query('SELECT delete_file_v1(' +
                    req.query.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    utility.save_socket(req.headers['host'].split(".")[0], 'file', 'delete', req.user.id, req.query.id, null);
                    
                    res.send(file);
                }).catch(function(err) {
                    return res.status(400).render('file_destroy: ' + err);
                });

        });
    }
};

exports.del_document_sign = function(req, res) {

    if (req.params.id && req.params.id !== undefined) {
        sequelize.query('SELECT delete_sign_document(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            utility.save_socket(req.headers['host'].split(".")[0], 'signed_file', 'delete', req.user.id, req.params.id, null);
                    
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('delete_sign_document: ' + err);
        });

    }else
    {
        return res.status(422).send(utility.error422('document_sign_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.zip_files = function(req, res) {
   
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Return all app.js file from the applications.bucket in Amazon
 */
exports.app = function(req, res) {

    sequelize.query('SELECT contact_me_included_v3(' +
        req.user.id + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(included) {
        if (included[0].contact_me_included_v3.applications !== null && included[0].contact_me_included_v3.applications.length > 0) {
            const s3 = new S3Client({
                region: config.aws_s3.region,
                credentials:{
                    accessKeyId: config.aws_s3.accessKeyId,
                    secretAccessKey: config.aws_s3.secretAccessKey,
                }
            });


            var params = {};
            var cnt = 0;
            var body = '';

            included[0].contact_me_included_v3.applications.forEach(function(application) {
                var files = [];
                if (application.attributes.contact_view_file != null) {
                    if (files.indexOf(application.attributes.contact_view_file) == -1) {
                        files.push(application.attributes.contact_view_file);
                    }
                }
                if (application.attributes.contact_edit_file != null) {
                    if (files.indexOf(application.attributes.contact_edit_file) == -1) {
                        files.push(application.attributes.contact_edit_file);
                    }
                }
                if (application.attributes.product_view_file != null) {
                    if (files.indexOf(application.attributes.product_view_file) == -1) {
                        files.push(application.attributes.product_view_file);
                    }
                }
                if (application.attributes.product_edit_file != null) {
                    if (files.indexOf(application.attributes.product_edit_file) == -1) {
                        files.push(application.attributes.product_edit_file);
                    }
                }
                if (application.attributes.project_view_file != null) {
                    if (files.indexOf(application.attributes.project_view_file) == -1) {
                        files.push(application.attributes.project_view_file);
                    }
                }
                if (application.attributes.project_edit_file != null) {
                    if (files.indexOf(application.attributes.project_edit_file) == -1) {
                        files.push(application.attributes.project_edit_file);
                    }
                }
                if (application.attributes.tracker_view_file != null) {
                    if (files.indexOf(application.attributes.tracker_view_file) == -1) {
                        files.push(application.attributes.tracker_view_file);
                    }
                }
                if (application.attributes.tracker_edit_file != null) {
                    if (files.indexOf(application.attributes.tracker_edit_file) == -1) {
                        files.push(application.attributes.tracker_edit_file);
                    }
                }
                if (application.attributes.event_view_file != null) {
                    if (files.indexOf(application.attributes.event_view_file) == -1) {
                        files.push(application.attributes.event_view_file);
                    }
                }
                if (application.attributes.event_edit_file != null) {
                    if (files.indexOf(application.attributes.event_edit_file) == -1) {
                        files.push(application.attributes.event_edit_file);
                    }
                }
                if (application.attributes.expense_view_file != null) {
                    if (files.indexOf(application.attributes.expense_view_file) == -1) {
                        files.push(application.attributes.expense_view_file);
                    }
                }
                if (application.attributes.expense_edit_file != null) {
                    if (files.indexOf(application.attributes.expense_edit_file) == -1) {
                        files.push(application.attributes.expense_edit_file);
                    }
                }

                if (application.attributes.invoice_view_file != null) {
                    if (files.indexOf(application.attributes.invoice_view_file) == -1) {
                        files.push(application.attributes.invoice_view_file);
                    }
                }
                if (application.attributes.invoice_edit_file != null) {
                    if (files.indexOf(application.attributes.invoice_edit_file) == -1) {
                        files.push(application.attributes.invoice_edit_file);
                    }
                }

                if (application.attributes.payment_view_file != null) {
                    if (files.indexOf(application.attributes.payment_view_file) == -1) {
                        files.push(application.attributes.payment_view_file);
                    }
                }
                if (application.attributes.payment_edit_file != null) {
                    if (files.indexOf(application.attributes.payment_edit_file) == -1) {
                        files.push(application.attributes.payment_edit_file);
                    }
                }

                if (application.attributes.dashboard_view_file != null) {
                    if (files.indexOf(application.attributes.dashboard_view_file) == -1) {
                        files.push(application.attributes.dashboard_view_file);
                    }
                }

                if (files.length > 0) {
                    res.writeHeader(200, {
                        "Content-Type": "text/plain"
                    });
                    files.forEach(function(file) {
                        params = {
                            Bucket: 'applications.beebeeboard.com',
                            Key: file
                        };


                        const command = new GetObjectCommand(params);
                        s3.send(command, function(err, data) {
                       // s3.getObject(params, function(err, data1) {
                            if (err)
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore read file : ' + err);

                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' File readed');
                            body = body + '\n' + data1.Body.toString();

                            res.write(body);
                            if (cnt++ == files.length - 1) {

                                res.end();
                            }
                        });
                    });

                } else {
                    console.log('No Applications for this user');
                    res.status(200).send({});
                }

            });

        } else {
            console.log('No Applications for this user');
            res.status(200).send({});
        }
    });
};

function put_from_url(url, bucket, key, filename, extension, callback) {
    
    var http = require('http');
    var https = require('https');

    /*
     *	AWS configuration for Aws S3 service
     *
     */


     const s3 = new S3Client({
            region: config.aws_s3.region,
            credentials:{
                accessKeyId: config.aws_s3.accessKeyId,
                secretAccessKey: config.aws_s3.secretAccessKey,
            }
        });

    console.log(url);

    if (url.indexOf('https') == -1) {
        var request = http.get(url, function(result) {

            var chunks = [];

            result.on('data', function(chunk) {
                chunks.push(chunk);
            });

            result.on('end', function() {
                var pdfData = new Buffer.concat(chunks);

                base64 = new Buffer(pdfData, 'binary');

                var params = {
                    Bucket: bucket,
                    Key: key,
                    ACL: 'private',
                    ServerSideEncryption: 'AES256',
                    ContentType: set_content(extension),
                    Body: base64
                };


                const command = new PutObjectCommand(params);
                s3.send(command, callback)

            });
        }).on('error', function(err) {

            return false;
        });
    } else {
        var request = https.get(url, function(result) {

            var chunks = [];

            result.on('data', function(chunk) {
                chunks.push(chunk);
            });

            result.on('end', function() {
                var pdfData = new Buffer.concat(chunks);

                base64 = new Buffer(pdfData, 'binary');

                var params = {
                    Bucket: bucket,
                    Key: key,
                    ACL: 'private',
                    ServerSideEncryption: 'AES256',
                    ContentType: set_content(extension),
                    Body: base64
                };

                const command = new PutObjectCommand(params);
                s3.send(command, callback);

            });
        }).on('error', function(err) {

            return false;
        });
    }
};

function set_content(type) {
    switch (type) {
        case 'pdf':
            return 'application/pdf';
            break;

        case 'jpg':
        case 'jpeg':
        case 'bmp':
        case 'png':
        case 'gif':
        case 'heic':
        case 'heif':
        case 'pjpeg':
        case 'svg+xml':
        case 'tiff':
        case 'vnd.djvu':
            return 'image/' + type;
            break;

        case 'json':
            return 'application/json';
            break;

        case 'xml':
            return 'application/xml';
            break;

        default:
            return 'application/text';
            break;
    }
};

