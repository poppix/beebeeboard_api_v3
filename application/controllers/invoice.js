var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    export_datas = require('../utility/exports.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js');


exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    indexes.invoice(req, null, function(err, index) {
        if (err) {
            return res.status(400).render('invoice_index: ' + err);
        }
        /*if (index.meta.total_items > 10000) {
            return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
        } else {*/

            export_datas.invoice(req, null, function(err, response) {
                if (err) {
                    return res.status(400).render('invoice_export: ' + err);
                }

                json2csv({
                    data: response.invoices,
                    fields: response.fields,
                    fieldNames: response.field_names
                }, function(err, csv) {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=invoices.csv'
                    });
                    res.end(csv);
                });
            });
        //}
    });
};

exports.stats = function(req, res) {


    utility.get_parameters_v1(req, 'invoice_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT invoices_stats(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ',' +
                parameters.document_type + ','+
                parameters.success+','+
                (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ','+
                parameters.sdi_status + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ','+
                parameters.sezionale + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].invoices_stats
                });
            }).catch(function(err) {
                return res.status(400).render('invoice_stats: ' + err);
            });
    });
};

exports.estimate_stats = function(req, res) {
    utility.get_parameters_v1(req, 'estimate_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT estimates_stats(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].estimates_stats
                });
            }).catch(function(err) {
                return res.status(400).render('estimate_stats: ' + err);
            });
    });
};

exports.orders_stats = function(req, res) {
    utility.get_parameters_v1(req, 'orders_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT orders_stats_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].orders_stats_v1
                });
            }).catch(function(err) {
                return res.status(400).render('orders_stats: ' + err);
            });
    });
};

exports.contracts_stats = function(req, res) {
    utility.get_parameters_v1(req, 'contracts_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT contracts_stats_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].contracts_stats_v1
                });
            }).catch(function(err) {
                return res.status(400).render('contracts_stats: ' + err);
            });
    });
};

exports.ddts_stats = function(req, res) {
    utility.get_parameters_v1(req, 'ddts_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT ddts_stats_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].ddts_stats_v1
                });
            }).catch(function(err) {
                return res.status(400).render('ddts_stats: ' + err);
            });
    });
};

exports.receipts_stats = function(req, res) {
    utility.get_parameters_v1(req, 'receipts_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT receipts_stats_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].receipts_stats_v1
                });
            }).catch(function(err) {
                return res.status(400).render('receipts_stats: ' + err);
            });
    });
};

exports.autofatture_stats = function(req, res) {
    utility.get_parameters_v1(req, 'invoices_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT autofatture_stats_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.user_id + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].autofatture_stats_v1
                });
            }).catch(function(err) {
                return res.status(400).render('autofatture_stats: ' + err);
            });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    indexes.invoice(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('invoice_index: ' + err);
        }
        res.json(response);
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific invoice searched by id
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_invoice(req, function(err, results) {
        if (err) {
            if(err.errors && err.errors[0]){
                return res.status(err.errors[0].status).send(err);
            }else
            {
                return res.status(403).send(err);
            }
            
        } else {
            res.json(results);
        }
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New invoice - Important: API send to application the Number (progressive)
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var invoice = {};

    if (!req.body.data.attributes.status || !utility.check_type_variable(req.body.data.attributes.status, 'string') || req.body.data.attributes.status == null || req.body.data.attributes.status == undefined) {
        req.body.data.attributes.status = 'draft';
    }

    if (req.body.data.attributes != null) {
        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        invoice['owner_id'] = req.user.id;
        invoice['number'] = (req.body.data.attributes.number && utility.check_type_variable(req.body.data.attributes.number, 'string') && req.body.data.attributes.number !== null ? req.body.data.attributes.number : '');
        invoice['contact_address_id'] = ((req.body.data.relationships && req.body.data.relationships.contact_address && req.body.data.relationships.contact_address.data && req.body.data.relationships.contact_address.data.id) && !utility.check_id(req.body.data.relationships.contact_address.data.id) ? req.body.data.relationships.contact_address.data.id : null);
        invoice['society_address_id'] = ((req.body.data.relationships && req.body.data.relationships.society_address && req.body.data.relationships.society_address.data && req.body.data.relationships.society_address.data.id) && !utility.check_id(req.body.data.relationships.society_address.data.id) ? req.body.data.relationships.society_address.data.id : null);
        invoice['prefix_number'] = (req.body.data.attributes.prefix_number && utility.check_type_variable(req.body.data.attributes.prefix_number, 'string') && req.body.data.attributes.prefix_number !== null ? req.body.data.attributes.prefix_number.replace(/'/g, "''''") : null);
        invoice['suffix_number'] = (req.body.data.attributes.suffix_number && utility.check_type_variable(req.body.data.attributes.suffix_number, 'string') && req.body.data.attributes.suffix_number !== null ? req.body.data.attributes.suffix_number.replace(/'/g, "''''") : null);
        invoice['real_number'] = req.body.data.attributes.real_number;
        invoice['estimate'] = req.body.data.attributes.estimate;
        invoice['date'] = (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : null);
        invoice['society'] = (req.body.data.attributes.society && utility.check_type_variable(req.body.data.attributes.society, 'string') && req.body.data.attributes.society !== null ? req.body.data.attributes.society.replace(/'/g, "''''") : '');
        invoice['society_piva_name'] = (req.body.data.attributes.society_piva_name && utility.check_type_variable(req.body.data.attributes.society_piva_name, 'string') && req.body.data.attributes.society_piva_name !== null ? req.body.data.attributes.society_piva_name.replace(/'/g, "''''") : '');
        invoice['society_piva'] = (req.body.data.attributes.society_piva && utility.check_type_variable(req.body.data.attributes.society_piva, 'string') && req.body.data.attributes.society_piva !== null ? req.body.data.attributes.society_piva.replace(/'/g, "''''") : '');
        invoice['imponibile'] = req.body.data.attributes.imponibile;
        invoice['total'] = req.body.data.attributes.total;
        invoice['status'] = (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') && req.body.data.attributes.status !== null ? req.body.data.attributes.status.replace(/'/g, "''''") : '');
        invoice['creditnote_invoicenumber'] = (req.body.data.attributes.creditnote_invoicenumber && utility.check_type_variable(req.body.data.attributes.creditnote_invoicenumber, 'string') && req.body.data.attributes.creditnote_invoicenumber !== null ? req.body.data.attributes.creditnote_invoicenumber.replace(/'/g, "''''") : '');
        invoice['creditnote_invoicedate'] = (req.body.data.attributes.creditnote_invoicedate && moment(req.body.data.attributes.creditnote_invoicedate).isValid() ? req.body.data.attributes.creditnote_invoicedate : null);
        invoice['thumbnail_url'] = (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url.replace(/'/g, "''''") : '');
        invoice['thumbnail_name'] = (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name.replace(/'/g, "''''") : '');
        invoice['thumbnail_etag'] = (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag.replace(/'/g, "''''") : '');
        invoice['thumbnail_type'] = (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type.replace(/'/g, "''''") : '');
        invoice['contact_name'] = (req.body.data.attributes.contact_name && utility.check_type_variable(req.body.data.attributes.contact_name, 'string') && req.body.data.attributes.contact_name !== null ? req.body.data.attributes.contact_name.replace(/'/g, "''''") : '');
        invoice['contact_piva'] = (req.body.data.attributes.contact_piva && utility.check_type_variable(req.body.data.attributes.contact_piva, 'string') && req.body.data.attributes.contact_piva !== null ? req.body.data.attributes.contact_piva.replace(/'/g, "''''") : '');
        invoice['contact_id'] = ((req.body.data.relationships && req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id && !utility.check_id(req.body.data.relationships.contact.data.id)) ? req.body.data.relationships.contact.data.id : null);
        invoice['user_id'] = ((req.body.data.relationships && req.body.data.relationships.user && req.body.data.relationships.user.data && req.body.data.relationships.user.data.id && !utility.check_id(req.body.data.relationships.user.data.id)) ? req.body.data.relationships.user.data.id : null);
        invoice['notes'] = (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
        invoice['_tenant_id'] = req.user._tenant_id;
        invoice['organization'] = req.headers['host'].split(".")[0];
        invoice['mongo_id'] = null;
        invoice['lordo'] = (req.body.data.attributes.lordo !== null && utility.check_type_variable(req.body.data.attributes.lordo, 'boolean') ? req.body.data.attributes.lordo : false);
        invoice['archivied'] = (req.body.data.attributes.archivied);
        invoice['invoice_style_id'] = ((req.body.data.relationships && req.body.data.relationships.invoice_style && req.body.data.relationships.invoice_style.data && req.body.data.relationships.invoice_style.data.id) && !utility.check_id(req.body.data.relationships.invoice_style.data.id) ? req.body.data.relationships.invoice_style.data.id : null);
        invoice['document_type'] = (req.body.data.attributes.document_type && utility.check_type_variable(req.body.data.attributes.document_type, 'string') && req.body.data.attributes.document_type !== null ? req.body.data.attributes.document_type.replace(/'/g, "''''") : '');
        invoice['fisica_giuridica'] = (req.body.data.attributes.fisica_giuridica);
        invoice['regime_fiscale'] = (req.body.data.attributes.regime_fiscale && utility.check_type_variable(req.body.data.attributes.regime_fiscale, 'string') && req.body.data.attributes.regime_fiscale !== null ? req.body.data.attributes.regime_fiscale.replace(/'/g, "''''") : '');
        invoice['pec_destinatario'] = (req.body.data.attributes.pec_destinatario && utility.check_type_variable(req.body.data.attributes.pec_destinatario, 'string') && req.body.data.attributes.pec_destinatario !== null ? req.body.data.attributes.pec_destinatario.replace(/'/g, "''''") : '');
        invoice['codice_destinatario'] = (req.body.data.attributes.codice_destinatario && utility.check_type_variable(req.body.data.attributes.codice_destinatario, 'string') && req.body.data.attributes.codice_destinatario !== null ? req.body.data.attributes.codice_destinatario.replace(/'/g, "''''") : '');
        invoice['rea'] = (req.body.data.attributes.rea && utility.check_type_variable(req.body.data.attributes.rea, 'string') && req.body.data.attributes.rea !== null ? req.body.data.attributes.rea.replace(/'/g, "''''") : '');
        invoice['contact_first_name'] = (req.body.data.attributes.contact_first_name && utility.check_type_variable(req.body.data.attributes.contact_first_name, 'string') && req.body.data.attributes.contact_first_name !== null ? req.body.data.attributes.contact_first_name.replace(/'/g, "''''") : '');
        invoice['contact_last_name'] = (req.body.data.attributes.contact_last_name && utility.check_type_variable(req.body.data.attributes.contact_last_name, 'string') && req.body.data.attributes.contact_last_name !== null ? req.body.data.attributes.contact_last_name.replace(/'/g, "''''") : '');
        invoice['pec_mittente'] = (req.body.data.attributes.pec_mittente && utility.check_type_variable(req.body.data.attributes.pec_mittente, 'string') && req.body.data.attributes.pec_mittente !== null ? req.body.data.attributes.pec_mittente.replace(/'/g, "''''") : '');
        invoice['society_first_name'] = (req.body.data.attributes.society_first_name && utility.check_type_variable(req.body.data.attributes.society_first_name, 'string') && req.body.data.attributes.society_first_name !== null ? req.body.data.attributes.society_first_name.replace(/'/g, "''''") : '');
        invoice['society_last_name'] = (req.body.data.attributes.society_last_name && utility.check_type_variable(req.body.data.attributes.society_last_name, 'string') && req.body.data.attributes.society_last_name !== null ? req.body.data.attributes.society_last_name.replace(/'/g, "''''") : '');
        invoice['bank_name'] = (req.body.data.attributes.bank_name && utility.check_type_variable(req.body.data.attributes.bank_name, 'string') && req.body.data.attributes.bank_name !== null ? req.body.data.attributes.bank_name.replace(/'/g, "''''") : '');
        invoice['bank_iban'] = (req.body.data.attributes.bank_iban && utility.check_type_variable(req.body.data.attributes.bank_iban, 'string') && req.body.data.attributes.bank_iban !== null ? req.body.data.attributes.bank_iban.replace(/'/g, "''''") : '');
        invoice['bank_bic'] = (req.body.data.attributes.bank_bic && utility.check_type_variable(req.body.data.attributes.bank_bic, 'string') && req.body.data.attributes.bank_bic !== null ? req.body.data.attributes.bank_bic.replace(/'/g, "''''") : '');
        invoice['esigibilita_iva'] = (req.body.data.attributes.esigibilita_iva && utility.check_type_variable(req.body.data.attributes.esigibilita_iva, 'string') && req.body.data.attributes.esigibilita_iva !== null ? req.body.data.attributes.esigibilita_iva.replace(/'/g, "''''") : '');
        invoice['valuta'] = (req.body.data.attributes.valuta && utility.check_type_variable(req.body.data.attributes.valuta, 'string') && req.body.data.attributes.valuta !== null ? req.body.data.attributes.valuta.replace(/'/g, "''''") : '');
        invoice['tasso_cambio'] = (req.body.data.attributes.tasso_cambio);
        invoice['society_cf'] = (req.body.data.attributes.society_cf && utility.check_type_variable(req.body.data.attributes.society_cf, 'string') && req.body.data.attributes.society_cf !== null ? req.body.data.attributes.society_cf.replace(/'/g, "''''") : '');
        invoice['contact_cf'] = (req.body.data.attributes.contact_cf && utility.check_type_variable(req.body.data.attributes.contact_cf, 'string') && req.body.data.attributes.contact_cf !== null ? req.body.data.attributes.contact_cf.replace(/'/g, "''''") : '');
        invoice['in_liquidazione'] = (req.body.data.attributes.in_liquidazione);
        invoice['bollo'] = (req.body.data.attributes.bollo);
        invoice['contact_fisica_giuridica'] = (req.body.data.attributes.contact_fisica_giuridica);                          
        invoice['date_competence'] = (req.body.data.attributes.date_competence && moment(req.body.data.attributes.date_competence).isValid() ? req.body.data.attributes.date_competence : null);
        invoice['sdi_field'] = (req.body.data.attributes.sdi_field && utility.check_type_variable(req.body.data.attributes.sdi_field, 'string') && req.body.data.attributes.sdi_field !== null ? req.body.data.attributes.sdi_field.replace(/'/g, "''''") : null);
        invoice['contact_ente_pa'] = (req.body.data.attributes.contact_ente_pa);     
        invoice['rivalsa'] = (req.body.data.attributes.rivalsa);                     
        

        var num_find = 0;

        if (!req.body.data.real_number && req.body.data.attributes.number != null && req.body.data.attributes.number != undefined) {

            var suffix = '';
            var prefix = '';
            var num = req.body.data.attributes.number.replace(/(\d+|[^\d]+)/g, '$1,').split(',');

            for (var i = 0; i < num.length; i++) {

                if (num_find == 1) {
                    suffix += num[i];
                } else if (!isNaN(num[i])) {
                    invoice['real_number'] = num[i];
                    num_find = 1;
                } else {
                    prefix = num[i];
                    invoice['prefix_number'] = num[i];
                }
            }
            invoice['suffix_number'] = suffix;
            invoice['prefix_number'] = prefix;
        }


        sequelize.query('SELECT insert_invoice(\'' + JSON.stringify(invoice) + '\',' + req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_invoice && datas[0].insert_invoice != null) {
                    req.body.data.id = datas[0].insert_invoice;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_invoice,
                        'body': req.body.data.relationships,
                        'model': 'invoice',
                        'user_id': req.user.id,
                        'array': ['contact', 'payment', 'reminder', 'expire_date', 'file', 'invoicetaxe', 'invoiceproduct', 'product', 'project', 'event', 'invoicegroup']
                    }, function(err, results) {

                        req.params.id = datas[0].insert_invoice;

                        sequelize.query('SELECT control_invoice_event_payments(' + datas[0].insert_invoice + ',' + req.user.id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(controls) {
                                finds.find_invoice(req, function(err, response) {
                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'invoice',
                                        'insert',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        datas[0].insert_invoice,
                                        'Aggiunta fattura ' + datas[0].insert_invoice
                                    );

                                    utility.save_socket(req.headers['host'].split(".")[0], 'invoice', 'insert', req.user.id, datas[0].insert_invoice, response.data);
                        
                                    res.json(response);
                                });
                            }).catch(function(err) {
                                return res.status(400).render('invoice_insert_control_payments: ' + err);
                            });

                    });

                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }
            }).catch(function(err) {
                return res.status(400).render('invoice_insert: ' + err);
            });


    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New invoice from estimate - Important: API send to application all data of estimate, but with
 * the ref number of the new invoice
 */
exports.create_estimate_credit = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var moment = require('moment');
    var invoice = {};

    sequelize.query('SELECT invoice_find_data_v4(' +
            req.user.id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            req.params.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(find) {
            sequelize.query('SELECT select_real_number_last_invoice(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    moment().year() + ',false,' +
                    find[0].invoice_find_data_v4[0].attributes.user_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas1) {

                    utility.check_find_datas(find[0].invoice_find_data_v4[0], 'invoice', ['reminders', 'contacts', 'files', 'payments', 'invoicetaxes', 'invoiceproducts', 'expire_dates', 'products', 'projects']);

                    if (datas1[0].select_real_number_last_invoice && datas1[0].select_real_number_last_invoice != null && datas1[0].select_real_number_last_invoice != '') {
                        find[0].invoice_find_data_v4[0].attributes.real_number = parseInt(datas1[0].select_real_number_last_invoice) + 1;
                    } else {
                        find[0].invoice_find_data_v4[0].attributes.real_number = 1;
                    }

                    find[0].invoice_find_data_v4[0].attributes.number = find[0].invoice_find_data_v4[0].attributes.real_number;

                    var old_est = find[0].invoice_find_data_v4[0];

                    invoice['suffix_number'] = '';
                    invoice['prefix_number'] = '';
                    invoice['owner_id'] = req.user.id;
                    invoice['number'] = old_est.attributes.number;
                    invoice['estimate'] = false;
                    invoice['date'] = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                    invoice['society'] = (old_est.attributes.society && utility.check_type_variable(old_est.attributes.society, 'string') && old_est.attributes.society !== null ? old_est.attributes.society.replace(/'/g, "''''") : '');
                    invoice['society_piva_name'] = (old_est.attributes.society_piva_name && utility.check_type_variable(old_est.attributes.society_piva_name, 'string') && old_est.attributes.society_piva_name !== null ? old_est.attributes.society_piva_name.replace(/'/g, "''''") : '');
                    invoice['society_piva'] = (old_est.attributes.society_piva && utility.check_type_variable(old_est.attributes.society_piva, 'string') && old_est.attributes.society_piva !== null ? old_est.attributes.society_piva.replace(/'/g, "''''") : '');
                    invoice['imponibile'] = req.query.type == 'credit' ? (old_est.attributes.imponibile * -1) : old_est.attributes.imponibile;
                    invoice['total'] = req.query.type == 'credit' ? (old_est.attributes.total * -1) : old_est.attributes.total;
                    invoice['status'] = 'unpaied';
                    invoice['real_number'] = old_est.attributes.real_number;
                    invoice['creditnote_invoicenumber'] = req.query.type == 'credit' ? find[0].invoice_find_data_v4[0].attributes.number : null;
                    invoice['creditnote_invoicedate'] = req.query.type == 'credit' ? find[0].invoice_find_data_v4[0].attributes.date : null;
                    invoice['thumbnail_url'] = old_est.attributes.thumbnail_url;
                    invoice['thumbnail_name'] = old_est.attributes.thumbnail_name;
                    invoice['thumbnail_etag'] = old_est.attributes.thumbnail_etag;
                    invoice['thumbnail_type'] = old_est.attributes.thumbnail_type;
                    invoice['contact_name'] = (old_est.attributes.contact_name !== null ? old_est.attributes.contact_name.replace(/'/g, "''''") : '');
                    invoice['contact_piva'] = old_est.attributes.contact_piva;
                    invoice['contact_id'] = ((old_est.relationships && old_est.relationships.contact && old_est.relationships.contact.data && old_est.relationships.contact.data.id) ? old_est.relationships.contact.data.id : null);
                    //invoice['product_id'] = ((old_est.relationships && old_est.relationships.product && old_est.relationships.product.data && old_est.relationships.product.data.id) ? old_est.relationships.product.data.id : null);
                    //invoice['project_id'] = ((old_est.relationships && old_est.relationships.project && old_est.relationships.project.data && old_est.relationships.project.data.id) ? old_est.relationships.project.data.id : null);

                    invoice['user_id'] = ((old_est.relationships && old_est.relationships.user && old_est.relationships.user.data && old_est.relationships.user.data.id) ? old_est.relationships.user.data.id : null);
                    invoice['notes'] = (old_est.attributes.notes !== null ? old_est.attributes.notes.replace(/'/g, "''''") : '');
                    invoice['_tenant_id'] = req.user._tenant_id;
                    invoice['organization'] = req.headers['host'].split(".")[0];
                    invoice['mongo_id'] = null;
                    invoice['lordo'] = old_est.attributes.lordo;
                    invoice['archivied'] = old_est.attributes.archivied;
                    invoice['invoice_style_id'] = ((old_est.relationships && old_est.relationships.invoice_style && old_est.relationships.invoice_style.data && old_est.relationships.invoice_style.data.id) ? old_est.relationships.invoice_style.data.id : null);
                    invoice['document_type'] = 'invoice';
                    invoice['fisica_giuridica'] = (old_est.attributes.fisica_giuridica);
                    invoice['regime_fiscale'] = (old_est.attributes.regime_fiscale && utility.check_type_variable(old_est.attributes.regime_fiscale, 'string') && old_est.attributes.regime_fiscale !== null ? old_est.attributes.regime_fiscale.replace(/'/g, "''''") : '');
                    invoice['pec_destinatario'] = (old_est.attributes.pec_destinatario && utility.check_type_variable(old_est.attributes.pec_destinatario, 'string') && old_est.attributes.pec_destinatario !== null ? old_est.attributes.pec_destinatario.replace(/'/g, "''''") : '');
                    invoice['codice_destinatario'] = (old_est.attributes.codice_destinatario && utility.check_type_variable(old_est.attributes.codice_destinatario, 'string') && old_est.attributes.codice_destinatario !== null ? old_est.attributes.codice_destinatario.replace(/'/g, "''''") : '');
                    invoice['rea'] = (old_est.attributes.rea && utility.check_type_variable(old_est.attributes.rea, 'string') && old_est.attributes.rea !== null ? old_est.attributes.rea.replace(/'/g, "''''") : '');
                    invoice['contact_first_name'] = (old_est.attributes.contact_first_name && utility.check_type_variable(old_est.attributes.contact_first_name, 'string') && old_est.attributes.contact_first_name !== null ? old_est.attributes.contact_first_name.replace(/'/g, "''''") : '');
                    invoice['contact_last_name'] = (old_est.attributes.contact_last_name && utility.check_type_variable(old_est.attributes.contact_last_name, 'string') && old_est.attributes.contact_last_name !== null ? old_est.attributes.contact_last_name.replace(/'/g, "''''") : '');
                    invoice['pec_mittente'] = (old_est.attributes.pec_mittente && utility.check_type_variable(old_est.attributes.pec_mittente, 'string') && old_est.attributes.pec_mittente !== null ? old_est.attributes.pec_mittente.replace(/'/g, "''''") : '');
                    invoice['society_first_name'] = (old_est.attributes.society_first_name && utility.check_type_variable(old_est.attributes.society_first_name, 'string') && old_est.attributes.society_first_name !== null ? old_est.attributes.society_first_name.replace(/'/g, "''''") : '');
                    invoice['society_last_name'] = (old_est.attributes.society_last_name && utility.check_type_variable(old_est.attributes.society_last_name, 'string') && old_est.attributes.society_last_name !== null ? old_est.attributes.society_last_name.replace(/'/g, "''''") : '');
                    invoice['bank_name'] = (old_est.attributes.bank_name && utility.check_type_variable(old_est.attributes.bank_name, 'string') && old_est.attributes.bank_name !== null ? old_est.attributes.bank_name.replace(/'/g, "''''") : '');
                    invoice['bank_iban'] = (old_est.attributes.bank_iban && utility.check_type_variable(old_est.attributes.bank_iban, 'string') && old_est.attributes.bank_iban !== null ? old_est.attributes.bank_iban.replace(/'/g, "''''") : '');
                    invoice['bank_bic'] = (old_est.attributes.bank_bic && utility.check_type_variable(old_est.attributes.bank_bic, 'string') && old_est.attributes.bank_bic !== null ? old_est.attributes.bank_bic.replace(/'/g, "''''") : '');
                    invoice['esigibilita_iva'] = (old_est.attributes.esigibilita_iva && utility.check_type_variable(old_est.attributes.esigibilita_iva, 'string') && old_est.attributes.esigibilita_iva !== null ? old_est.attributes.esigibilita_iva.replace(/'/g, "''''") : '');
                    invoice['valuta'] = (old_est.attributes.valuta && utility.check_type_variable(old_est.attributes.valuta, 'string') && old_est.attributes.valuta !== null ? old_est.attributes.valuta.replace(/'/g, "''''") : '');
                    invoice['tasso_cambio'] = (old_est.attributes.tasso_cambio);
                    invoice['society_cf'] = (old_est.attributes.society_cf && utility.check_type_variable(old_est.attributes.society_cf, 'string') && old_est.attributes.society_cf !== null ? old_est.attributes.society_cf.replace(/'/g, "''''") : '');
                    invoice['contact_cf'] = (old_est.attributes.contact_cf && utility.check_type_variable(old_est.attributes.contact_cf, 'string') && old_est.attributes.contact_cf !== null ? old_est.attributes.contact_cf.replace(/'/g, "''''") : '');
                    invoice['in_liquidazione'] = (old_est.attributes.in_liquidazione);  
                    invoice['bollo'] = (old_est.attributes.bollo);
                    invoice['contact_fisica_giuridica'] = (old_est.attributes.contact_fisica_giuridica);                          
                    invoice['date_competence'] = (old_est.attributes.date_competence && moment(old_est.attributes.date_competence).isValid() ? old_est.attributes.date_competence : null);
                    invoice['sdi_field'] = (old_est.attributes.sdi_field && utility.check_type_variable(old_est.attributes.sdi_field, 'string') && old_est.attributes.sdi_field !== null ? old_est.attributes.sdi_field.replace(/'/g, "''''") : null);
                    invoice['contact_ente_pa'] = (old_est.attributes.contact_ente_pa);    
                    invoice['rivalsa'] = (old_est.attributes.rivalsa);
        

                    sequelize.query('SELECT insert_invoice(\'' + JSON.stringify(invoice) + '\', ' + req.user.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {
                            if (datas[0].insert_invoice && datas[0].insert_invoice != null) {
                                old_est.id = datas[0].insert_invoice;

                                utility.sanitizeRelations({
                                    'organization': req.headers['host'].split(".")[0],
                                    '_tenant': req.user._tenant_id,
                                    'new_id': datas[0].insert_invoice,
                                    'body': old_est.relationships,
                                    'model': 'invoice',
                                    'user_id': req.user.id,
                                    'array': ['contact', 'file', 'product', 'project', 'event']
                                }, function(err, results) {


                                    sequelize.query('SELECT duplicate_estimate_fields_for_invoices(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.user.id + ',' +
                                            datas[0].insert_invoice + ',' +
                                            req.params.id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                            })
                                        .then(function(datas) {

                                            req.params.id = old_est.id;
                                            finds.find_invoice(req, function(err, response) {
                                                logs.save_log_model(
                                                    req.user._tenant_id,
                                                    req.headers['host'].split(".")[0],
                                                    'invoice',
                                                    'insert',
                                                    req.user.id,
                                                    JSON.stringify(invoice),
                                                    req.params.id,
                                                    'Trasformato preventivo ' + req.params.id + ' in fattura'
                                                );
                                                utility.save_socket(req.headers['host'].split(".")[0], 'invoice', 'insert', req.user.id, old_est.id, response.data);
        
                                                res.json(response);
                                            });

                                        }).catch(function(err) {
                                            return res.status(400).render('duplicate_estimate_fields_for_invoices: ' + err);
                                        });
                                });


                            }
                        });


                }).catch(function(err) {
                    return res.status(400).render('invoice_create_estimate: ' + err);
                });

        }).catch(function(err) {
            return res.status(400).render('invoice_create_estimate: ' + err);
        });
};

exports.cast_document = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var invoice = {};
    var expire_date_id = null,
        invoicegroup_id = null,
        contact_id = null;

    if (req.body.expire_id !== undefined) {
        expire_date_id = req.body.expire_id;
    }
    if (req.body.invoicegroup_id !== undefined) {
        invoicegroup_id = req.body.invoicegroup_id;
    }
    if (req.body.contact_id !== undefined) {
        contact_id = req.body.contact_id;
    }


    if (req.body.document_type !== undefined && req.body.id !== undefined) {
        sequelize.query('SELECT invoice_find_data_v4(' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ',' +
                req.body.id + ',' +
                req.user.role_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(find) {
                if (find && find[0] && find[0].invoice_find_data_v4 && find[0].invoice_find_data_v4[0]) {
                   
                    utility.check_find_datas(find[0].invoice_find_data_v4[0], 'invoice', ['reminders', 'contacts', 'files', 'payments', 'invoicetaxes', 'invoicegroups', 'invoiceproducts', 'expire_dates', 'products', 'projects']);

                    var old_est = find[0].invoice_find_data_v4[0];

                    invoice['suffix_number'] = '';
                    invoice['prefix_number'] = '';
                    invoice['owner_id'] = req.user.id;
                    invoice['number'] = null;
                    invoice['date'] = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                    invoice['society'] = (old_est.attributes.society && utility.check_type_variable(old_est.attributes.society, 'string') && old_est.attributes.society !== null ? old_est.attributes.society.replace(/'/g, "''''") : '');
                    invoice['society_piva_name'] = (old_est.attributes.society_piva_name && utility.check_type_variable(old_est.attributes.society_piva_name, 'string') && old_est.attributes.society_piva_name !== null ? old_est.attributes.society_piva_name.replace(/'/g, "''''") : '');
                    invoice['society_piva'] = (old_est.attributes.society_piva && utility.check_type_variable(old_est.attributes.society_piva, 'string') && old_est.attributes.society_piva !== null ? old_est.attributes.society_piva.replace(/'/g, "''''") : '');
                    invoice['imponibile'] = old_est.attributes.imponibile;
                    invoice['total'] = old_est.attributes.total;
                    invoice['status'] = 'unpaied';
                    invoice['real_number'] = null;
                    invoice['creditnote_invoicenumber'] = old_est.creditnote_invoicenumber;
                    invoice['creditnote_invoicedate'] = old_est.creditnote_invoicedate;
                    invoice['thumbnail_url'] = old_est.attributes.thumbnail_url;
                    invoice['thumbnail_name'] = old_est.attributes.thumbnail_name;
                    invoice['thumbnail_etag'] = old_est.attributes.thumbnail_etag;
                    invoice['thumbnail_type'] = old_est.attributes.thumbnail_type;
                    invoice['contact_name'] = (old_est.attributes.contact_name !== null ? old_est.attributes.contact_name.replace(/'/g, "''''") : '');
                    invoice['contact_piva'] = old_est.attributes.contact_piva;
                    invoice['contact_id'] = ((old_est.relationships && old_est.relationships.contact && old_est.relationships.contact.data && old_est.relationships.contact.data.id) ? old_est.relationships.contact.data.id : null);
                    invoice['user_id'] = ((old_est.relationships && old_est.relationships.user && old_est.relationships.user.data && old_est.relationships.user.data.id) ? old_est.relationships.user.data.id : null);
                    invoice['notes'] = (old_est.attributes.notes !== null ? old_est.attributes.notes.replace(/'/g, "''''") : '');
                    invoice['_tenant_id'] = req.user._tenant_id;
                    invoice['organization'] = req.headers['host'].split(".")[0];
                    invoice['mongo_id'] = null;
                    invoice['lordo'] = old_est.attributes.lordo;
                    invoice['archivied'] = old_est.attributes.archivied;
                    invoice['invoice_style_id'] = ((old_est.relationships && old_est.relationships.invoice_style && old_est.relationships.invoice_style.data && old_est.relationships.invoice_style.data.id) ? old_est.relationships.invoice_style.data.id : null);
                    invoice['document_type'] = req.body.document_type;
                    invoice['fisica_giuridica'] = (old_est.attributes.fisica_giuridica);
                    invoice['regime_fiscale'] = (old_est.attributes.regime_fiscale && utility.check_type_variable(old_est.attributes.regime_fiscale, 'string') && old_est.attributes.regime_fiscale !== null ? old_est.attributes.regime_fiscale.replace(/'/g, "''''") : '');
                    invoice['pec_destinatario'] = (old_est.attributes.pec_destinatario && utility.check_type_variable(old_est.attributes.pec_destinatario, 'string') && old_est.attributes.pec_destinatario !== null ? old_est.attributes.pec_destinatario.replace(/'/g, "''''") : '');
                    invoice['codice_destinatario'] = (old_est.attributes.codice_destinatario && utility.check_type_variable(old_est.attributes.codice_destinatario, 'string') && old_est.attributes.codice_destinatario !== null ? old_est.attributes.codice_destinatario.replace(/'/g, "''''") : '');
                    invoice['rea'] = (old_est.attributes.rea && utility.check_type_variable(old_est.attributes.rea, 'string') && old_est.attributes.rea !== null ? old_est.attributes.rea.replace(/'/g, "''''") : '');
                    invoice['contact_first_name'] = (old_est.attributes.contact_first_name && utility.check_type_variable(old_est.attributes.contact_first_name, 'string') && old_est.attributes.contact_first_name !== null ? old_est.attributes.contact_first_name.replace(/'/g, "''''") : '');
                    invoice['contact_last_name'] = (old_est.attributes.contact_last_name && utility.check_type_variable(old_est.attributes.contact_last_name, 'string') && old_est.attributes.contact_last_name !== null ? old_est.attributes.contact_last_name.replace(/'/g, "''''") : '');
                    invoice['pec_mittente'] = (old_est.attributes.pec_mittente && utility.check_type_variable(old_est.attributes.pec_mittente, 'string') && old_est.attributes.pec_mittente !== null ? old_est.attributes.pec_mittente.replace(/'/g, "''''") : '');
                    invoice['society_first_name'] = (old_est.attributes.society_first_name && utility.check_type_variable(old_est.attributes.society_first_name, 'string') && old_est.attributes.society_first_name !== null ? old_est.attributes.society_first_name.replace(/'/g, "''''") : '');
                    invoice['society_last_name'] = (old_est.attributes.society_last_name && utility.check_type_variable(old_est.attributes.society_last_name, 'string') && old_est.attributes.society_last_name !== null ? old_est.attributes.society_last_name.replace(/'/g, "''''") : '');
                    invoice['bank_name'] = (old_est.attributes.bank_name && utility.check_type_variable(old_est.attributes.bank_name, 'string') && old_est.attributes.bank_name !== null ? old_est.attributes.bank_name.replace(/'/g, "''''") : '');
                    invoice['bank_iban'] = (old_est.attributes.bank_iban && utility.check_type_variable(old_est.attributes.bank_iban, 'string') && old_est.attributes.bank_iban !== null ? old_est.attributes.bank_iban.replace(/'/g, "''''") : '');
                    invoice['bank_bic'] = (old_est.attributes.bank_bic && utility.check_type_variable(old_est.attributes.bank_bic, 'string') && old_est.attributes.bank_bic !== null ? old_est.attributes.bank_bic.replace(/'/g, "''''") : '');
                    invoice['esigibilita_iva'] = (old_est.attributes.esigibilita_iva && utility.check_type_variable(old_est.attributes.esigibilita_iva, 'string') && old_est.attributes.esigibilita_iva !== null ? old_est.attributes.esigibilita_iva.replace(/'/g, "''''") : '');
                    invoice['valuta'] = (old_est.attributes.valuta && utility.check_type_variable(old_est.attributes.valuta, 'string') && old_est.attributes.valuta !== null ? old_est.attributes.valuta.replace(/'/g, "''''") : '');
                    invoice['tasso_cambio'] = (old_est.attributes.tasso_cambio);
                    invoice['society_cf'] = (old_est.attributes.society_cf && utility.check_type_variable(old_est.attributes.society_cf, 'string') && old_est.attributes.society_cf !== null ? old_est.attributes.society_cf.replace(/'/g, "''''") : '');
                    invoice['contact_cf'] = (old_est.attributes.contact_cf && utility.check_type_variable(old_est.attributes.contact_cf, 'string') && old_est.attributes.contact_cf !== null ? old_est.attributes.contact_cf.replace(/'/g, "''''") : '');
                    invoice['in_liquidazione'] = (old_est.attributes.in_liquidazione);             
                    invoice['bollo'] = (old_est.attributes.bollo);
                    invoice['contact_fisica_giuridica'] = (old_est.attributes.contact_fisica_giuridica);                          
                    invoice['date_competence'] = (old_est.attributes.date_competence && moment(old_est.attributes.date_competence).isValid() ? old_est.attributes.date_competence : null);
                    invoice['sdi_field'] = (old_est.attributes.sdi_field && utility.check_type_variable(old_est.attributes.sdi_field, 'string') && old_est.attributes.sdi_field !== null ? old_est.attributes.sdi_field.replace(/'/g, "''''") : null);
                    invoice['contact_ente_pa'] = (old_est.attributes.contact_ente_pa);
                    invoice['rivalsa'] = (old_est.attributes.rivalsa);

                    sequelize.query('SELECT insert_invoice(\'' + JSON.stringify(invoice) + '\', ' + req.user.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {
                            if (datas[0].insert_invoice && datas[0].insert_invoice != null) {
                                old_est.id = datas[0].insert_invoice;

                                utility.sanitizeRelations({
                                    'organization': req.headers['host'].split(".")[0],
                                    '_tenant': req.user._tenant_id,
                                    'new_id': datas[0].insert_invoice,
                                    'body': old_est.relationships,
                                    'model': 'invoice',
                                    'user_id': req.user.id,
                                    'array': ['contact', 'file', 'product', 'project', 'event']
                                }, function(err, results) {

                                    sequelize.query('SELECT cast_document(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.user.id + ',' +
                                            datas[0].insert_invoice + ',' +
                                            req.body.id + ',\'' +
                                            req.body.document_type + '\',' +
                                            expire_date_id + ',' +
                                            invoicegroup_id + ',' +
                                            contact_id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                            })
                                        .then(function(datas1) {


                                            logs.save_log_model(
                                                req.user._tenant_id,
                                                req.headers['host'].split(".")[0],
                                                req.body.document_type,
                                                'insert',
                                                req.user.id,
                                                JSON.stringify(invoice),
                                                req.body.id,
                                                'Trasformato documento ' + req.body.id + ' in ' + req.body.document_type
                                            );



                                            req.body.document_type = req.body.document_type == 'creditnote' ? 'invoice' : req.body.document_type;
                                            res.json({ 'id': datas[0].insert_invoice, 'document_type': req.body.document_type });



                                        }).catch(function(err) {
                                            return res.status(400).render('cast_document: ' + err);
                                        });

                                });
                            }


                        }).catch(function(err) {
                            return res.status(400).render('invoice_create_estimate: ' + err);
                        });
                }
            }).catch(function(err) {
                return res.status(400).render('invoice_create_estimate: ' + err);
            });
    } else {
        return res.status(404).send({});
    }
};


exports.cast_expense = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    if (req.body.id !== undefined) {


            sequelize.query('SELECT cast_from_invoice_to_expense(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +                
                req.body.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas1) {


                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    req.body.document_type,
                    'insert',
                    req.user.id,
                    JSON.stringify('{}'),
                    req.body.id,
                    'Trasformato documento ' + req.body.id + ' in expense' 
                );

                res.json({ 'id': datas1[0].cast_from_invoice_to_expense, 'document_type': 'expense' });



            }).catch(function(err) {
                return res.status(400).render('cast_to_expense: ' + err);
            });


    } else {
        return res.status(404).send({});
    }
};


exports.change_status_invoice = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var id = req.body.id;
    var number = null;
    var status = null;

    if (req.body.number !== undefined) {
        number = '\'' + req.body.number + '\'';
    }
    if (req.body.status !== undefined) {
        status = '\'' + req.body.status + '\'';
    }


    sequelize.query('SELECT invoice_change_status(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            id + ',' +
            number + ',' +
            status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas1) {

            res.json({});
        }).catch(function(err) {
            return res.status(400).render('invoice_change_status: ' + err);
        });
};


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New invoice - Important: API send to application all data of invoice like number, society data and other
 * parameters
 */
exports.new_invoice = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var invoice = {};

    var moment = require('moment');
    var estimate = req.query.estimate == 'false' ? false : true;
    var id_user = (req.query.user_id && !utility.check_id(req.query.user_id)) ? req.query.user_id : req.user.id;



    sequelize.query('SELECT select_real_number_last_invoice(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            moment().year() + ',' +
            estimate + ',' +
            id_user + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas1) {
            console.log(datas1[0].select_real_number_last_invoice);
            if (datas1[0].select_real_number_last_invoice && datas1[0].select_real_number_last_invoice != null && datas1[0].select_real_number_last_invoice != '') {
                invoice['number'] = parseInt(datas1[0].select_real_number_last_invoice) + 1;

            } else {
                invoice['number'] = 1;
            }


            res.json({
                data: {
                    invoice: {
                        number: invoice['number']
                    }
                }
            });
        }).catch(function(err) {
            return res.status(400).render('invoice_new_invoice: ' + err);
        });
};

exports.document_type_number = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var invoice = {};

    var moment = require('moment');
    var doc_type = req.query.document_type !== undefined ? req.query.document_type : 'invoice';
    var id_user = (req.query.user_id && !utility.check_id(req.query.user_id)) ? req.query.user_id : req.user.id;



    sequelize.query('SELECT select_real_number_last_document_type_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            moment().year() + ',\'' +
            doc_type + '\',' +
            id_user + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas1) {
            if (datas1[0].select_real_number_last_document_type_v1 && datas1[0].select_real_number_last_document_type_v1 != null && datas1[0].select_real_number_last_document_type_v1 != '') {
                res.json(datas1[0].select_real_number_last_document_type_v1);
            } else {
                res.json(datas1[0].select_real_number_last_document_type_v1);
            }


            
        }).catch(function(err) {
            return res.status(400).render('select_real_number_last_document_type: ' + err);
        });
};


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing invoice searched by id
 * When a invoice change we must set all reminders
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    
                        sequelize.query('SELECT update_invoice(' +
                            '' + req.params.id +
                            ',' + req.user._tenant_id +
                            ',\'' + req.headers['host'].split(".")[0] +
                            '\',' + req.user.id +
                            ',\'' + (req.body.data.attributes.number && utility.check_type_variable(req.body.data.attributes.number, 'string') && req.body.data.attributes.number !== null ? req.body.data.attributes.number : '') +
                            '\',' + req.body.data.attributes.estimate +
                            ',' + (req.body.data.attributes.date !== null && moment(req.body.data.attributes.date).isValid() ? '\'' + req.body.data.attributes.date + '\'' : null) +
                            ',\'' + (req.body.data.attributes.society && utility.check_type_variable(req.body.data.attributes.society, 'string') && req.body.data.attributes.society !== null ? req.body.data.attributes.society.replace(/'/g, "''''") : '') +
                            '\',\'' + (req.body.data.attributes.society_piva_name && utility.check_type_variable(req.body.data.attributes.society_piva_name, 'string') && req.body.data.attributes.society_piva_name !== null ? req.body.data.attributes.society_piva_name.replace(/'/g, "''''") : '') +
                            '\',\'' + (req.body.data.attributes.society_piva && utility.check_type_variable(req.body.data.attributes.society_piva, 'string') && req.body.data.attributes.society_piva !== null ? req.body.data.attributes.society_piva : '') +
                            '\',' + req.body.data.attributes.imponibile +
                            ',' + req.body.data.attributes.total +
                            ',\'' + (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') && req.body.data.attributes.status !== null ? req.body.data.attributes.status : '') +
                            '\',\'' + (req.body.data.attributes.creditnote_invoicenumber && utility.check_type_variable(req.body.data.attributes.creditnote_invoicenumber, 'string') && req.body.data.attributes.creditnote_invoicenumber !== null ? req.body.data.attributes.creditnote_invoicenumber : '') +
                            '\',' + (req.body.data.attributes.creditnote_invoicedate && req.body.data.attributes.creditnote_invoicedate !== null && moment(req.body.data.attributes.creditnote_invoicedate).isValid() ? '\'' + req.body.data.attributes.creditnote_invoicedate + '\'' : null) +
                            ',\'' + (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url : '') +
                            '\',\'' + (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name : '') +
                            '\',\'' + (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag : '') +
                            '\',\'' + (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type : '') +
                            '\',\'' + (req.body.data.attributes.contact_name && utility.check_type_variable(req.body.data.attributes.contact_name, 'string') && req.body.data.attributes.contact_name !== null ? req.body.data.attributes.contact_name.replace(/'/g, "''''") : '') +
                            '\',\'' + (req.body.data.attributes.contact_piva && utility.check_type_variable(req.body.data.attributes.contact_piva, 'string') && req.body.data.attributes.contact_piva !== null ? req.body.data.attributes.contact_piva : '') +
                            '\',' + ((req.body.data.relationships && req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id && !utility.check_id(req.body.data.relationships.contact.data.id)) ? req.body.data.relationships.contact.data.id : null) +
                            //',' + ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null) +
                            ',' + ((req.body.data.relationships && req.body.data.relationships.user && req.body.data.relationships.user.data && req.body.data.relationships.user.data.id && !utility.check_id(req.body.data.relationships.user.data.id)) ? req.body.data.relationships.user.data.id : null) +
                            ',\'' + (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') +
                            '\',' + (req.body.data.relationships && req.body.data.relationships.contact_address && req.body.data.relationships.contact_address.data !== null && req.body.data.relationships.contact_address.data.id !== null && !utility.check_id(req.body.data.relationships.contact_address.data.id) ? req.body.data.relationships.contact_address.data.id : null) +
                            ',' + (req.body.data.relationships && req.body.data.relationships.society_address && req.body.data.relationships.society_address.data !== null && req.body.data.relationships.society_address.data.id !== null && !utility.check_id(req.body.data.relationships.society_address.data.id) ? req.body.data.relationships.society_address.data.id : null) +
                            //',' + ((req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data && req.body.data.relationships.project.data.id && !utility.check_id(req.body.data.relationships.project.data.id)) ? req.body.data.relationships.project.data.id : null) +
                            ',\'' + req.body.data.attributes.suffix_number +
                            '\',\'' + req.body.data.attributes.prefix_number +
                            '\',' + (req.body.data.attributes.real_number ? req.body.data.attributes.real_number : null) +
                            ',' + req.user.role_id + ',' +
                            (req.body.data.attributes.lordo !== null ? req.body.data.attributes.lordo : null) + ',' +
                            req.body.data.attributes.archivied + ',' +
                            ((req.body.data.relationships && req.body.data.relationships.invoice_style && req.body.data.relationships.invoice_style.data && req.body.data.relationships.invoice_style.data.id && !utility.check_id(req.body.data.relationships.invoice_style.data.id)) ? req.body.data.relationships.invoice_style.data.id : null) + ',' +
                            '\'' + (req.body.data.attributes.document_type && utility.check_type_variable(req.body.data.attributes.document_type, 'string') && req.body.data.attributes.document_type !== null ? req.body.data.attributes.document_type.replace(/'/g, "''''") : '') +
                            '\','+ (req.body.data.attributes.fisica_giuridica ? req.body.data.attributes.fisica_giuridica : null)+
                            ',\''+ (req.body.data.attributes.regime_fiscale && utility.check_type_variable(req.body.data.attributes.regime_fiscale, 'string') && req.body.data.attributes.regime_fiscale !== null ? req.body.data.attributes.regime_fiscale.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.pec_destinatario && utility.check_type_variable(req.body.data.attributes.pec_destinatario, 'string') && req.body.data.attributes.pec_destinatario !== null ? req.body.data.attributes.pec_destinatario.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.codice_destinatario && utility.check_type_variable(req.body.data.attributes.codice_destinatario, 'string') && req.body.data.attributes.codice_destinatario !== null ? req.body.data.attributes.codice_destinatario.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.rea && utility.check_type_variable(req.body.data.attributes.rea, 'string') && req.body.data.attributes.rea !== null ? req.body.data.attributes.rea.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.contact_first_name && utility.check_type_variable(req.body.data.attributes.contact_first_name, 'string') && req.body.data.attributes.contact_first_name !== null ? req.body.data.attributes.contact_first_name.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.contact_last_name && utility.check_type_variable(req.body.data.attributes.contact_last_name, 'string') && req.body.data.attributes.contact_last_name !== null ? req.body.data.attributes.contact_last_name.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.pec_mittente && utility.check_type_variable(req.body.data.attributes.pec_mittente, 'string') && req.body.data.attributes.pec_mittente !== null ? req.body.data.attributes.pec_mittente.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.society_first_name && utility.check_type_variable(req.body.data.attributes.society_first_name, 'string') && req.body.data.attributes.society_first_name !== null ? req.body.data.attributes.society_first_name.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.society_last_name && utility.check_type_variable(req.body.data.attributes.society_last_name, 'string') && req.body.data.attributes.society_last_name !== null ? req.body.data.attributes.society_last_name.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.bank_name && utility.check_type_variable(req.body.data.attributes.bank_name, 'string') && req.body.data.attributes.bank_name !== null ? req.body.data.attributes.bank_name.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.bank_iban && utility.check_type_variable(req.body.data.attributes.bank_iban, 'string') && req.body.data.attributes.bank_iban !== null ? req.body.data.attributes.bank_iban.replace(/'/g, "''''") : '')+
                            '\',\''+ (req.body.data.attributes.bank_bic && utility.check_type_variable(req.body.data.attributes.bank_bic, 'string') && req.body.data.attributes.bank_bic !== null ? req.body.data.attributes.bank_bic.replace(/'/g, "''''") : '')+
                            '\',\''+(req.body.data.attributes.esigibilita_iva && utility.check_type_variable(req.body.data.attributes.esigibilita_iva, 'string') && req.body.data.attributes.esigibilita_iva !== null ? req.body.data.attributes.esigibilita_iva.replace(/'/g, "''''") : '')+
                            '\',\''+(req.body.data.attributes.valuta && utility.check_type_variable(req.body.data.attributes.valuta, 'string') && req.body.data.attributes.valuta !== null ? req.body.data.attributes.valuta.replace(/'/g, "''''") : '')+
                            '\',' + req.body.data.attributes.tasso_cambio +
                            ',\''+(req.body.data.attributes.contact_cf && utility.check_type_variable(req.body.data.attributes.contact_cf, 'string') && req.body.data.attributes.contact_cf !== null ? req.body.data.attributes.contact_cf.replace(/'/g, "''''") : '')+
                            '\',\''+(req.body.data.attributes.society_cf && utility.check_type_variable(req.body.data.attributes.society_cf, 'string') && req.body.data.attributes.society_cf !== null ? req.body.data.attributes.society_cf.replace(/'/g, "''''") : '')+
                            '\',' +(req.body.data.attributes.in_liquidazione ? req.body.data.attributes.in_liquidazione : null) +',' +
                            req.body.data.attributes.bollo +',' +
                            req.body.data.attributes.contact_fisica_giuridica +
                            ',' + (req.body.data.attributes.date_competence !== null && moment(req.body.data.attributes.date_competence).isValid() ? '\'' + req.body.data.attributes.date_competence + '\'' : null) +
                            ','+(req.body.data.attributes.sdi_field && utility.check_type_variable(req.body.data.attributes.sdi_field, 'string') && req.body.data.attributes.sdi_field !== null ? ('\''+req.body.data.attributes.sdi_field.replace(/'/g, "''''")+'\'') : null)+','+
                            req.body.data.attributes.contact_ente_pa +','+
                            req.body.data.attributes.rivalsa +');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            }).then(function(inv) {

                            if (!inv[0].update_invoice) {
                                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                            }

                            //var old = old_invoice;

                            finds.find_invoice(req, function(err, response) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'invoice',
                                    'update',
                                    req.user.id,
                                    JSON.stringify(req.body.data.attributes),
                                    req.params.id,
                                    'Modificata fattura ' + req.params.id
                                );
                                utility.save_socket(req.headers['host'].split(".")[0], 'invoice', 'update', req.user.id, req.params.id, response.data);
                        
                                res.json(response);
                            });

                        }).catch(function(err) {
                            return res.status(400).render('invoice_update: ' + err);
                        });
                 /*   });
                }).catch(function(err) {
                    return res.status(400).render('invoice_update: ' + err);
                });
        });
    }).catch(function(err) {
        return res.status(400).render('invoice_update: ' + err);
    });*/

};

exports.change_invoice_status = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.body.invoice_id)) {
        return res.status(422).send(utility.error422('invoice_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!req.body.status || req.body.status == null) {
        return res.status(422).send(utility.error422('status', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT invoice_change_status(' +
        req.body.invoice_id + ',\'' +
        req.body.status + '\',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(datas) {
        res.status(200).send({});

    }).catch(function(err) {
        return res.status(400).render('invoice_change_status: ' + err);
    });
   

};

exports.update_all = function(req, res) {
    
    var suffix = '';
    var prefix = '';
    var num_find = 0;

    if (!req.body.attributes.real_number && req.body.attributes.number != null && req.body.attributes.number != undefined) {
        req.body.attributes.number = req.body.attributes.number.toString();
        var num = req.body.attributes.number.replace(/(\d+|[^\d]+)/g, '$1,').split(',');

        for (var i = 0; i < num.length; i++) {

            if (num_find == 1) {
                suffix += num[i];
            } else if (!isNaN(num[i])) {
                req.body.attributes.real_number = num[i];
                num_find = 1;
            } else {
                prefix = num[i];
            }
        }
        req.body.attributes.suffix_number = suffix;
        req.body.attributes.prefix_number = prefix;

    }

    sequelize.query('SELECT update_invoice_all(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas && datas[0]['update_invoice_all']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_invoice_all'];
                finds.find_invoice(req, function(err, results) {
                    if (err) {
                        if(err.errors && err.errors[0]){
                            return res.status(err.errors[0].status).send(err);
                        }else
                        {
                            return res.status(403).send(err);
                        }
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'invoice', 'update', req.user.id, req.params.id, results.data);
                        
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing invoice searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT delete_invoice_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(!datas[0].delete_invoice_v1){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }else{
                utility.save_socket(req.headers['host'].split(".")[0], 'invoice', 'delete', req.user.id, req.params.id, null);
                        
                res.json({
                    'data': {
                        'type': 'invoices',
                        'id': datas[0].delete_invoice_v1
                    }
                });
            }
        }).catch(function(err) {
            return res.status(400).render('invoice_destroy: ' + err);
        });


};

exports.charts = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }



    sequelize.query('SELECT invoice_expense_chart_v2(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            (req.query.contact_id ? req.query.contact_id : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].invoice_expense_chart_v2[0]);

        }).catch(function(err) {
            return res.status(400).render('invoice_charts: ' + err);
        });
};


exports.customer_charts = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }


    sequelize.query('SELECT invoice_customer_chart(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            (req.query.contact_id ? req.query.contact_id : null) + ',\''+
            req.query.document_type+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].invoice_customer_chart[0]);

        }).catch(function(err) {
            return res.status(400).render('invoice_customer_chart: ' + err);
        });
};


exports.inv_categories_charts = function(req, res) {
   var start = null,
   end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT invoice_categories_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].invoice_categories_chart[0] && datas[0].invoice_categories_chart[0].Categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].invoice_categories_chart[0].Categorie.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name].push(cat.tot);
                    if (cnt++ >= datas[0].invoice_categories_chart[0].Categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('invoice_categories_chart: ' + err);
        });
};

exports.inv_annual_categories_charts = function(req, res) {
    var start = null,
        end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT invoice_annual_categories_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].invoice_annual_categories_chart[0] && datas[0].invoice_annual_categories_chart[0].categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].invoice_annual_categories_chart[0].categorie.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name] = cat.array_agg;
                    if (cnt++ >= datas[0].invoice_annual_categories_chart[0].categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('invoice_annual_categories_chart: ' + err);
        });
};

exports.inv_sub_categories_charts = function(req, res) {
    var start = null,
        end = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT invoice_sub_categories_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',\'' +
            req.query.category +
            '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].invoice_sub_categories_chart[0] && datas[0].invoice_sub_categories_chart[0].sub_categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].invoice_sub_categories_chart[0].sub_categorie.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name].push(cat.tot);
                    if (cnt++ >= datas[0].invoice_sub_categories_chart[0].sub_categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('invoice_sub_categories_chart: ' + err);
        });
};

exports.competenze_entrate = function(req, res) {

    var year_date = null,
        month_date = null,
        comp_year = null,
        comp_month = null,
        categ_id = null,
        sub_categ_id = null;


    


    utility.get_parameters(req, 'invoice', function(err, parameters) {
        if (err) {
            return res.status(500).send(utility.error403('get parameters', 'Non hai i diritti', '500'));

        }

        async.parallel({
            invoice: function(callback) {
                sequelize.query('SELECT invoice_competenze_entrate(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    parameters.year+','+
                    parameters.month+','+
                    parameters.comp_year+','+
                    parameters.comp_month+','+
                    parameters.category_id+','+
                    parameters.sub_category_id+','+
                    parameters.page_count + ',' +
                    (parameters.page_num - 1) * parameters.page_count + ','+
                    parameters.sortDirection + ',' +
                    parameters.sortField + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {

                    sequelize.query('SELECT all_invoice_competenze_entrate(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        parameters.year+','+
                        parameters.month+','+
                        parameters.comp_year+','+
                        parameters.comp_month+','+
                        parameters.category_id+','+
                        parameters.sub_category_id+');')
                    .then(function(counter) {
                            if (datas && datas[0] && datas[0].invoice_competenze_entrate && datas[0].invoice_competenze_entrate != null) {
                                response = {
                                    'data': datas[0].invoice_competenze_entrate,
                                    'meta': {
                                        'total_items': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_items),
                                        'tot_lordo': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_lordo),
                                        'tot_imponibile': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_imponibile),
                                        'total_invoiceproduct_number': parseInt(counter[0][0].all_invoice_competenze_entrate.total_invoiceproduct_number),
                                        'total_invoiceproducts': parseInt(counter[0][0].all_invoice_competenze_entrate.total_invoiceproducts),
                                        'actual_page': parseInt(parameters.page_num),
                                        'total_pages': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_items / parameters.page_count) + 1,
                                        'item_per_page': parseInt(parameters.page_count),
                                        'tot_all_number': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_items_tutti)
                                    }
                                };

                                callback(null, response);
                            } else {

                                response = {
                                    'data': [],
                                    'meta': {
                                        'total_items': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_items),
                                        'actual_page': parseInt(parameters.page_num),
                                        'tot_lordo': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_lordo),
                                        'tot_imponibile': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_imponibile),
                                        'total_invoiceproduct_number': parseInt(counter[0][0].all_invoice_competenze_entrate.total_invoiceproduct_number),
                                        'total_invoiceproducts': parseInt(counter[0][0].all_invoice_competenze_entrate.total_invoiceproducts),
                                        'total_pages': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_items / parameters.page_count) + 1,
                                        'item_per_page': parseInt(parameters.page_count),
                                        'tot_all_number': parseInt(counter[0][0].all_invoice_competenze_entrate.tot_items_tutti)
                                    }
                                };
                                callback(null, response);
                            }
                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });



                }).catch(function(err) {
                    callback(err, null);
                });
               
            }

        }, function(err, results) {
            if (err) {
                console.log(err);
                return res.status(500).send('Invoice Competenze entrate ' + err);
            }

            res.status(200).json({
                'invoices': results.invoice
            });
        });
    });
};