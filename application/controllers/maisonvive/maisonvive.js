var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    config = require('../../../config'),
    logs = require('../../utility/logs.js');
var util = require('util');


exports.maisonvive_contact = function(req, res) {

    var util = require('util');
    var contact_id = req.params.id;

    if(!req.params.id || req.params.id === undefined){
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    sequelize.query('SELECT maisonvive_contact(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.params.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_contact) {
                res.json(datas[0].maisonvive_contact[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_contact: ' + err);
        });
};

exports.maisonvive_scadenze_passive = function(req, res) {

    var util = require('util');
    var start_date = null,
        end_date = null,
        payment_method =  null;

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    if (req.query.payment_method) {
        payment_method = '\''+req.query.payment_method+'\'';
    }

   
    
    sequelize.query('SELECT maisonvive_scadenze_riba(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+','+
            payment_method+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_scadenze_riba) {
                res.json(datas[0].maisonvive_scadenze_riba[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_scadenze_riba: ' + err);
        });
};

exports.maisonvive_dashboard = function(req, res) {

    var util = require('util');
    var start_date = null,
        end_date = null;
  
    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    sequelize.query('SELECT maisonvive_dashboard(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_dashboard) {
                res.json(datas[0].maisonvive_dashboard);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_dashboard: ' + err);
        });
};

exports.maisonvive_contratti_aperti = function(req, res) {

    var util = require('util');
    var start_date = null,
        end_date = null;
  
    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    sequelize.query('SELECT maisonvive_contratti_aperti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_contratti_aperti) {
                res.json(datas[0].maisonvive_contratti_aperti);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_contratti_aperti: ' + err);
        });
};

exports.maisonvive_in_consegna = function(req, res) {

    var util = require('util');
    var start_date = null,
        end_date = null;
  
    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    sequelize.query('SELECT maisonvive_in_consegna(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_in_consegna) {
                res.json(datas[0].maisonvive_in_consegna);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_in_consegna: ' + err);
        });
};

exports.maisonvive_stats = function(req, res) {

    var util = require('util');
    var start_date = null,
        end_date = null;
  
    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    sequelize.query('SELECT maisonvive_stats(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_stats) {
                res.json(datas[0].maisonvive_stats);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_stats: ' + err);
        });
};

exports.maisonvive_contract_time = function(req, res) {

    var util = require('util');
    var contact_id = req.params.id;

    if(!req.params.id || req.params.id === undefined){
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    sequelize.query('SELECT maisonvive_contract_time(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.params.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_contract_time && datas[0].maisonvive_contract_time[0] && datas[0].maisonvive_contract_time[0].timelapse) {
                res.json(datas[0].maisonvive_contract_time[0].timelapse);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_contract_time: ' + err);
        });
};

exports.maisonvive_associa_contratti = function(req, res) {

    var util = require('util');
    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
       
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }
    
    sequelize.query('SELECT maisonvive_associa_contratti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_contratti) {
                res.json(datas[0].maisonvive_associa_contratti[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_contratti: ' + err);
        });
}; 

exports.maisonvive_associa_ddt_contratti = function(req, res) {

    var util = require('util');
    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
       
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }
    
    sequelize.query('SELECT maisonvive_associa_ddt_contratti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) +','+
            (req.query.invoice_id ? req.query.invoice_id : null)+ ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_ddt_contratti) {
                res.json(datas[0].maisonvive_associa_ddt_contratti[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_ddt_contratti: ' + err);
        });
}; 

exports.maisonvive_associa_ddt_ordini = function(req, res) {

    var util = require('util');
    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
       
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }
    
    sequelize.query('SELECT maisonvive_associa_ddt_ordini(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_ddt_ordini) {
                res.json(datas[0].maisonvive_associa_ddt_ordini[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_ddt_ordini: ' + err);
        });
}; 

exports.maisonvive_associa_contratto_expensearticle = function(req, res) {

    var util = require('util');
    var ids = [];

    if (req.body.expense_articles !== undefined && req.body.expense_articles != '') {
        if (util.isArray(req.body.expense_articles)) {
            ids = req.body.expense_articles.map(Number);
        } else {
            ids.push(req.body.expense_articles);
        }
    }
    
    sequelize.query('SELECT maisonvive_associa_contratto_expensearticle(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.contact_id +','+
            req.body.contratto_id +','+
            (ids && ids.length > 0 ? 'ARRAY[' + ids + ']' : null)  +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_contratto_expensearticle) {
                res.json(datas[0].maisonvive_associa_contratto_expensearticle[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_contratto_expensearticle: ' + err);
        });
}; 

exports.maisonvive_delete_contract = function(req, res) {

     
    if (!req.user.role[0].json_build_object.attributes.invoice_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
   
    var util = require('util');
    
    sequelize.query('SELECT maisonvive_delete_contract(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.contract_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_delete_contract) {
                res.json(datas[0].maisonvive_delete_contract[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_delete_contract: ' + err);
        });
}; 

exports.maisonvive_escludi_contract = function(req, res) {

     
    if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
   
    var util = require('util');
    
    sequelize.query('SELECT maisonvive_escludi_contract(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.contract_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_escludi_contract) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_escludi_contract: ' + err);
        });
}; 

exports.maisonvive_delete_order = function(req, res) {

     
    if (!req.user.role[0].json_build_object.attributes.event_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
   
    var util = require('util');
    
    sequelize.query('SELECT maisonvive_delete_order(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.order_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_delete_order) {
                res.json(datas[0].maisonvive_delete_order[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_delete_order: ' + err);
        });
}; 

exports.maisonvive_finish_contract = function(req, res) {
     if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_finish_contract(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.contract_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_finish_contract) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_finish_contract: ' + err);
        });
}; 

exports.maisonvive_sign_manual_contract = function(req, res) {
     if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_sign_manual_contract(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.contract_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_sign_manual_contract) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_sign_manual_contract: ' + err);
        });
}; 

exports.maisonvive_reopen_contract = function(req, res) {
     if (!req.user.role[0].json_build_object.attributes.invoice_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_reopen_contract(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.contract_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_reopen_contract) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_reopen_contract: ' + err);
        });
}; 

exports.maisonvive_goals = function(req, res) {

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_save_goal(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_save_goal) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_save_goal: ' + err);
        });
}; 

exports.maisonvive_paga_scadenza = function(req, res) {

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_paga_scadenza(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_paga_scadenza) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_paga_scadenza: ' + err);
        });
}; 

exports.maisonvive_expense_commercialista = function(req, res) {

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_expense_commercialista(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_expense_commercialista) {
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_expense_commercialista: ' + err);
        });
}; 

exports.maisonvive_cancella_consegna = function(req, res) {

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_cancella_consegna(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.event_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_cancella_consegna) {
               
                res.json({});
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_cancella_consegna: ' + err);
        });
}; 

exports.maisonvive_contratti_from_fornitore = function(req, res) {

    var util = require('util');
 
    sequelize.query('SELECT maisonvive_contratti_from_fornitore(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.query.contact_id +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_contratti_from_fornitore) {
                res.json(datas[0].maisonvive_contratti_from_fornitore[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_contratti_from_fornitore: ' + err);
        });
}; 

exports.maisonvive_associa_ordini = function(req, res) {

    var util = require('util');
    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
       
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

    
    sequelize.query('SELECT maisonvive_associa_ordini(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_ordini) {
                res.json(datas[0].maisonvive_associa_ordini[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_ordini: ' + err);
        });
}; 

exports.maisonvive_associa_ordine_expensearticle = function(req, res) {

    var util = require('util');
    sequelize.query('SELECT maisonvive_associa_ordine_expensearticle(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.expense_article_id +','+
            req.body.ordine_id +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_ordine_expensearticle) {
                res.json(datas[0].maisonvive_associa_ordine_expensearticle[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_ordine_expensearticle: ' + err);
        });
}; 

exports.maisonvive_associa_fattura_expire = function(req, res) {

    
    sequelize.query('SELECT maisonvive_associa_fattura_expire(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.expire_id +','+
            req.body.fattura_id +','+
            req.body.contratto_id+',\''+
            req.body.operation+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_fattura_expire) {
                res.json(datas[0].maisonvive_associa_fattura_expire[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_fattura_expire: ' + err);
        });
}; 

exports.maisonvive_disassocia_ordine_expensearticle = function(req, res) {

    var util = require('util');
    sequelize.query('SELECT maisonvive_disassocia_ordine_expensearticle(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.expense_id +','+
            req.body.ordine_id +','+
            req.body.ddt_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_disassocia_ordine_expensearticle) {
                res.json(datas[0].maisonvive_disassocia_ordine_expensearticle[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_disassocia_ordine_expensearticle: ' + err);
        });
}; 

exports.maisonvive_associa_ordine_ddt = function(req, res) {

    var util = require('util');
    sequelize.query('SELECT maisonvive_associa_ordine_ddt(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.ddt_id +','+
            req.body.ordine_id +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_associa_ordine_ddt) {
                res.json(datas[0].maisonvive_associa_ordine_ddt[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_associa_ordine_ddt: ' + err);
        });
}; 

exports.maisonvive_disassocia_ordine_ddt = function(req, res) {

    var util = require('util');
    sequelize.query('SELECT maisonvive_disassocia_ordine_ddt(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.ddt_id +','+
            req.body.ordine_id +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_disassocia_ordine_ddt) {
                res.json(datas[0].maisonvive_disassocia_ordine_ddt[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_disassocia_ordine_ddt: ' + err);
        });
}; 

exports.maisonvive_culletta = function(req, res) {

    var util = require('util');
    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
       
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }
    
    sequelize.query('SELECT maisonvive_culletta(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_culletta) {
                res.json(datas[0].maisonvive_culletta);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_culletta: ' + err);
        });
};

exports.maisonvive_slot_calendario_consegne = function(req, res) {

     var start_date = null,
        end_date = null,
        q = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

        sequelize.query('SELECT maisonvive_consegne_slot_calendar(' +
            req.user._tenant_id + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.start_date+ ',' +
            parameters.end_date+ ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            
            if (datas[0].maisonvive_consegne_slot_calendar && datas[0].maisonvive_consegne_slot_calendar != null && datas[0].maisonvive_consegne_slot_calendar[0]) {
                res.status(200).send(datas[0].maisonvive_consegne_slot_calendar[0]);

            } else {
                res.status(200).send('{}');
            }
        }).catch(function(err) {
            return res.status(400).render('datas[0].maisonvive_consegne_slot_calendar: ' + err);
        });
     }); 
};

exports.maisonvive_consegne = function(req, res) {

    var util = require('util');

    sequelize.query('SELECT maisonvive_consegne(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_consegne) {
                
                res.json(datas[0].maisonvive_consegne);

            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_consegne: ' + err);
        });
};

exports.maisonvive_conferma_consegna = function(req, res) {

    var util = require('util');

    sequelize.query('SELECT maisonvive_conferma_consegna(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.body.consegna_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_conferma_consegna) {
                res.json(datas[0].maisonvive_conferma_consegna);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_conferma_consegna: ' + err);
        });
};

exports.maisonvive_finalizza_contratto = function(req, res) {

    var util = require('util');
    var contact_id = req.params.id;

    if(!req.params.id || req.params.id === undefined){
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    sequelize.query('SELECT maisonvive_finalizza_contratto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.params.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_finalizza_contratto) {
                res.json(datas[0].maisonvive_finalizza_contratto[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_finalizza_contratto: ' + err);
        });
};

exports.maisonvive_ddt_upload_consegna = function(req, res) {

    var util = require('util');
    
    sequelize.query('SELECT maisonvive_ddt_upload_consegna(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.body.consegna_id + ',\''+
            req.body.operation+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_ddt_upload_consegna) {
                res.json(datas[0].maisonvive_ddt_upload_consegna[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_ddt_upload_consegna: ' + err);
        });
};

exports.maisonvive_save_importo_ordine = function(req, res) {

    sequelize.query('SELECT maisonvive_save_importo_ordine(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''")+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_save_importo_ordine) {
                res.json(datas[0].maisonvive_save_importo_ordine[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_save_importo_ordine: ' + err);
        });
};

exports.maisonvive_aggiungi_montatore = function(req, res) {

    sequelize.query('SELECT maisonvive_aggiungi_montatore(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.body.event_id+','+
            req.body.contact_id+',\''+
            req.body.operation+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_aggiungi_montatore) {
                
                res.json(datas[0].maisonvive_aggiungi_montatore[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_aggiungi_montatore: ' + err);
        });
};

exports.maisonvive_modifica_consegna = function(req, res) {

    console.log(req.body);


    sequelize.query('SELECT maisonvive_modifica_consegna(' +
        req.user._tenant_id + ',\''+
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id+','+
        req.user.role_id+',\''+
        JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {
        
        if (datas[0].maisonvive_modifica_consegna && datas[0].maisonvive_modifica_consegna != null && datas[0].maisonvive_modifica_consegna[0]) {
                
            res.status(200).send(datas[0].maisonvive_modifica_consegna[0]);

        } else {
            res.status(200).send('{}');
        }
    }).catch(function(err) {
        return res.status(400).render('datas[0].maisonvive_consegne_slot_calendar: ' + err);
    });    
};

exports.maisonvive_crea_ddt_attivo = function(req, res) {

    var consegna_id = req.body.consegna_id;

    if(!consegna_id || consegna_id === undefined){
        return res.status(422).send(utility.error422('consegna_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    sequelize.query('SELECT maisonvive_crea_ddt_attivo(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            consegna_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].maisonvive_crea_ddt_attivo) {
                res.json(datas[0].maisonvive_crea_ddt_attivo[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_crea_ddt_attivo: ' + err);
        });
};

exports.maisonvive_calendario_consegne = function(req, res) {

     
    var start_date = null,
        end_date = null,
        q = null,
        contact_id = null,
        product_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

        sequelize.query('SELECT maisonvive_calendario_consegne(' +
            req.user._tenant_id + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.start_date+ ',' +
            parameters.end_date+ ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ','+
            req.user.role_id+','+
            req.user.id+','+
            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_calendario_consegne && datas[0].maisonvive_calendario_consegne != null) {
                res.status(200).send(datas[0].maisonvive_calendario_consegne);

            } else {
                console.log('ciao');
                res.status(200).send('{}');
            }
        }).catch(function(err) {
            return res.status(400).render('datas[0].maisonvive_calendario_consegne: ' + err);
        });
     });  
};

exports.maisonvive_calendario_scadenze = function(req, res) {

     
    var start_date = null,
        end_date = null,
        q = null,
        contact_id = null,
        product_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

        sequelize.query('SELECT maisonvive_calendario_scadenze(' +
            req.user._tenant_id + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.start_date+ ',' +
            parameters.end_date+ ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ','+
            req.user.role_id+','+
            req.user.id+','+
            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_calendario_scadenze && datas[0].maisonvive_calendario_scadenze != null) {
                res.status(200).send(datas[0].maisonvive_calendario_scadenze[0]);

            } else {
                
                res.status(200).send('{}');
            }
        }).catch(function(err) {
            return res.status(400).render('datas[0].maisonvive_calendario_scadenze: ' + err);
        });
     });  
};

exports.maisonvive_contract_charts = function(req, res) {

    var util = require('util');
    var start_date = null,
        end_date = null;
  
    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!utility.check_type_variable(req.body.from_date, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\''+req.body.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!utility.check_type_variable(req.body.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\''+req.body.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    sequelize.query('SELECT maisonvive_contract_charts(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start_date + ','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].maisonvive_contract_charts) {
                res.json(datas[0].maisonvive_contract_charts[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('maisonvive_contract_charts: ' + err);
        });
};

