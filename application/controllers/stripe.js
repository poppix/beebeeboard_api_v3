var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
        utility = require('../utility/utility.js'),
        logs = require('../utility/logs.js'),
        config = require('../../config');

exports.create_customer = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var token = req.body.stripeToken;
    var redirect = req.query.redirect;

    if (token) {
        sequelize.query('SELECT stripe_get_customer_id_from_contact_id(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].stripe_get_customer_id_from_contact_id) {
                    sequelize.query('SELECT stripe_get_card_id_from_contact_id(\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user._tenant_id + ',' +
                            req.user.id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(card) {
                            stripe.customers.deleteCard(
                                datas[0].stripe_get_customer_id_from_contact_id,
                                card[0].stripe_get_card_id_from_contact_id,
                                function(err, confirmation) {
                                    if (err) {
                                        console.log(err);
                                    }

                                    stripe.customers.createSource(
                                        datas[0].stripe_get_customer_id_from_contact_id, { source: token },
                                        function(err, card) {
                                            if (err) {
                                                console.log(err);
                                            }

                                            sequelize.query('SELECT update_stripe_card_id(\'' +
                                                    datas[0].stripe_get_customer_id_from_contact_id + '\' ,' +
                                                    req.user._tenant_id + ',' +
                                                    req.user.id + ',\'' +
                                                    card.id + '\',\'' +
                                                    req.headers['host'].split(".")[0] + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                    })
                                                .then(function(updated) {

                                                    if (updated[0].update_stripe_card_id && updated[0].update_stripe_card_id != null) {
                                                        res.redirect(redirect);
                                                    }
                                                });

                                        }
                                    );
                                }
                            );

                        });
                } else {
                    stripe.customers.create({
                        description: 'Customer id ' + req.user.id + ' for organization ' + req.headers['host'].split(".")[0],
                        source: token
                    }, function(err, customer) {
                        if (err) {
                            return res.status(400).render('Stripe create customer: ' + err);
                        }

                        var stripe_record = {};
                        stripe_record['_tenant_id'] = req.user._tenant_id;
                        stripe_record['organization'] = req.headers['host'].split(".")[0];
                        stripe_record['customer_id'] = customer.id;
                        stripe_record['stripe_token'] = token;
                        stripe_record['card_id'] = customer.sources.data[0].id;

                        sequelize.query('SELECT insert_stripe_customer(\'' +
                                JSON.stringify(stripe_record) + '\',' +
                                req.user._tenant_id + ',' +
                                req.user.id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                            .then(function(datas) {

                                if (datas[0].insert_stripe_customer && datas[0].insert_stripe_customer != null) {
                                    res.redirect(redirect);
                                }
                            });
                    });
                }

            });

    } else {
        return res.status(422).send(utility.error422('', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.get_customer_info = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var contact_id = req.query.contact_id;

    sequelize.query('SELECT stripe_get_customer_id_from_contact_id(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].stripe_get_customer_id_from_contact_id) {
                stripe.customers.retrieve(
                    datas[0].stripe_get_customer_id_from_contact_id,
                    function(err, customer) {
                        if (err) {
                            return res.status(500).send('Stripe get customer info: ' + err);
                        }

                        res.status(200).send({
                            'brand': customer.sources.data[0].brand,
                            'last4': customer.sources.data[0].last4,
                            'funding': customer.sources.data[0].funding,
                            'exp_month': customer.sources.data[0].exp_month,
                            'exp_year': customer.sources.data[0].exp_year
                        });
                    }
                );
            } else {
                res.status(200).send({});
            }

        });
};

exports.get_amount = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT get_amount_customer_v1(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({ 'amount': datas[0].get_amount_customer_v1 });
        });
};

exports.you_must_pay = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    async.parallel({
        customer: function(callback) {
            sequelize.query('SELECT stripe_get_customer_id_from_contact_id(\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',' +
                    req.user.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                    callback(null, datas[0].stripe_get_customer_id_from_contact_id);
                }).catch(function(err) {
                    callback(err, null);
                });
        },
        subscription: function(callback) {
            sequelize.query('SELECT get_subscription_from_contact_id(\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',' +
                    req.user.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                    callback(null, datas[0].get_subscription_from_contact_id);
                }).catch(function(err) {
                    callback(err, null);
                });
        },
        amount: function(callback) {
            sequelize.query('SELECT get_amount_customer(\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',' +
                    req.user.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                    callback(null, parseFloat(utility.formatta_euro(datas[0].get_amount_customer * 1.22)));
                }).catch(function(err) {
                    callback(err, null);
                });
        }
    }, function(err, results) {
        if (err) {
            console.log(err);
            return res.status(400).render('payment from stripe: ' + err);

        } else if (results.customer && results.amount && results.subscription) {

            var charge = stripe.charges.create({
                amount: results.amount * 100,
                currency: "eur",
                customer: results.customer
            }, function(err, charge) {
                if(err){
                    console.log(err);
                    res.status(422).send({});
                }
                else if (charge.status == 'succeeded' && charge.paid == true) {
                    sequelize.query('SELECT beebeeboard_crea_fattura_poppix_srl(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user.id + ',' +
                            results.subscription + ',' +
                            moment().year() + ',' +
                            results.amount + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(fattura) {}).catch(function(err) {
                            return res.status(400).render('beebeeboard_crea_fattura_poppix_srl: ' + err);
                        });
                    res.status(200).send({});
                } else {
                    res.status(422).send({});
                }
            });
        } else {
            res.status(422).send({});
        }
    });
};

exports.request_permission = function(req, res) {
    var stripeClientLive = 'ca_9cMfyU5ej1O2QyjeSwMVKcPlpD9ecZ1p';
    //var stripeClientTest = 'ca_9cMf7YJbVqDJHuYFrTGJ9CK8LE5ToNCs';

    var redirect_uri = 'https://appv3.beebeeboard.com/api/v1/stripe/connect';
    res.redirect('https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=' + stripeClientLive + '&scope=read_write&state=' + req.headers['host'].split(".")[0] + '-' + req.user.id + '&redirect_uri=' + redirect_uri);
    //res.redirect('https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=' + stripeClientTest + '&scope=read_write&state=' + req.headers['host'].split(".")[0] + '-' + req.user.id + '&redirect_uri=' + redirect_uri);
};

exports.connect = function(req, res) {
    var request = require('request');
    //var stripeKey = config.stripe_keys.test_secret_key;
    var stripeKey = config.stripe_keys.live_secret_key;

    if (!req.query.code) {
        return false;
    }

    var postData = JSON.stringify({
        'client_secret': stripeKey,
        'code': req.query.code,
        'grant_type': 'authorization_code'
    });

    request({
        headers: {
            'Content-Length': postData.length,
            'Content-Type': 'application/json'
        },
        uri: 'https://connect.stripe.com/oauth/token',
        body: postData,
        method: 'POST'
    }, function(err, result, body) {
        console.log(body);

        if (!err) {
            var json_body = JSON.parse(body);

            if (!json_body.stripe_user_id) {
                console.log(err);
                return res.status(400).render('errore nel connettere stripe: ' + err);
            } else {
                let state = req.query.state.split('-');

                sequelize.query('SELECT insert_user_payments_pos(\'' +
                    state[0] + '\',' +
                    state[1] + ',\'stripe\',\'' +
                    json_body.stripe_user_id + '\', null);', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    res.redirect('https://' + state[0] + '.beebeeboard.com');
                });
            }
        }
    });
};

exports.create_session = function(req, res) {
    
    var request = require('request');
    var abbonamento = null;
    var retUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/start/#/pay_me?request_by='+req.body.contact_id+'&access_token=' + req.body.token + '&payment_id=' + req.body.payment_id;
    var errUrl = retUrl;
    
    if(req.body.ecommerce){
        retUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/scuolesci_ecommerce/#/payment_status/'+req.body.payment_id + '?access_token=' + req.body.token;
    }

    if(req.body.booking){
        retUrl = 'https://asset.beebeeboard.com/utility/booking/success_booking.php?contact_id='+req.body.contact_id+'&organization='+ req.headers['host'].split(".")[0]+'&access_token=' + req.body.token+ '&payment_id=' + req.body.payment_id+'&specialista_mail=' + encodeURIComponent(req.body.specialista_mail)+'&specialista_name=' + encodeURIComponent(req.body.specialista_name)+'&product_name=' + encodeURIComponent(req.body.product_name);
        errUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/start/#/pay_me?request_by='+req.body.contact_id+'&access_token=' + req.body.token + '&payment_id=' + req.body.payment_id;
    }


    if (req.body.abbonamento || req.headers['host'].split(".")[0] === 'poppixsrl') {

        if(req.body.abbonamento){
            retUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com';
            var totale = req.body.abbonamento.total.replace(/"/g, "");
         

            sequelize.query('SELECT aggiorna_abbonamento_v2(\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id +','+
                    req.user.id + ',\'' +
                    moment().format('YYYY-MM-DDTHH:mmZ') + '\',\'' +
                    JSON.stringify(req.body.abbonamento).replace(/'/g, "''''") + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                    
                    abbonamento = datas[0].aggiorna_abbonamento_v2;

                    console.log(req.body.abbonamento);

                    if(req.body.abbonamento.due_price && req.body.abbonamento.due_price > 0){

                        var items = [];
                        var total_parziale = req.body.abbonamento.total;

                        if(req.body.abbonamento.packages){
                            req.body.abbonamento.packages.forEach(function(pack){
                                items.push({
                                    'name': pack.pkg.name,
                                    'amount': Math.round(pack.pkg.price*100),
                                    'currency': 'eur',
                                    'quantity': 1
                                });

                                total_parziale = total_parziale - pack.pkg.price;
                            });
                        }

                        if(req.body.abbonamento.renew_plan && req.body.abbonamento.renew_plan == 'true'){
                                items.push({
                                'name': 'Abbonamento '+ (req.body.abbonamento.month_billing_cycle ? 'mensile' : 'annuale') +' Beebeeboard piano '+req.body.abbonamento.plan_name,
                                'amount': Math.round(req.body.abbonamento.due_price*100),
                                'currency': 'eur',
                                'quantity': 1
                            });

                        }

                        items.push({
                            'name': 'IVA 22%',
                            'amount': Math.round(req.body.abbonamento.total/1.22*0.22*100),
                            'currency': 'eur',
                            'quantity': 1
                        });

                        const session = stripe.checkout.sessions.create({
                            payment_method_types: ['card'],
                            line_items: items,
                            client_reference_id: req.headers['host'].split(".")[0]+'-'+req.body.abbonamento.subscription_id+'-'+req.user.id+'-'+req.user._tenant_id+'-'+abbonamento+'-'+totale,
                            success_url: retUrl,
                            cancel_url: retUrl
                          }).then(function(resStripe) {
                              
                              if(resStripe){
                                
                                res.status(200).send(resStripe);
                              }
                              else{
                                res.status(200).send({});
                              }    
                          });
                    }
                    else{
                        /* MODIFICA IL PIANO MA NON DEVE PAGARE NULLA */
                        res.status(200).send({});
                    }

                }).catch(function(err) {
                    console.log(err);
                    
                });
        }
        else{
            const session = stripe.checkout.sessions.create({
                payment_method_types: ['card'],
                line_items: [{
                  name: "Pagamento a Poppix Srl",
                  description: "Ti abbiamo richiesto un pagamento",
                  amount: Math.round(req.body.total),
                  currency: 'eur',
                  quantity: 1
                }],
                client_reference_id: req.headers['host'].split(".")[0]+'-'+req.body.payment_id+'-'+req.body.contact_id+'-'+req.user._tenant_id+'-'+abbonamento+'-'+req.body.total,
                success_url: retUrl,
                cancel_url: retUrl
              }).then(function(resStripe) {
                  
                  if(resStripe){
                    
                    res.status(200).send(resStripe);
                  }
                  else{
                    res.status(200).send({});
                  }    
              });
        }
        
    }
    else{
        sequelize.query('SELECT get_user_payments_pos(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.body.contact_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
       
            /*var postData = JSON.stringify({
                'success_url': "https://example.com/success",
                'cancel_url': "https://example.com/cancel",
                'payment_method_types': ["card"],
                'line_items': [{
                    'name': req.headers['host'].split(".")[0],
                    'description': "Pagamento e-commerce",
                    'amount': Math.round(req.body.total),
                    'currency': "eur",
                    'quantity': 1
                }]
            });

            request({
                headers: {
                    'Content-Length': postData.length,
                    'Content-Type': 'application/json'
                },
                uri: 'https://api.stripe.com/v1/checkout/sessions',
                body: postData,
                method: 'POST'
            }, function(err, result, body) {*/


            const session = stripe.checkout.sessions.create({
                payment_method_types: ['card'],
                client_reference_id: req.headers['host'].split(".")[0]+'-'+req.body.payment_id+'-'+req.body.contact_id+'-'+req.user._tenant_id+'-'+abbonamento+'-'+req.body.total,
                line_items: [{
                  name: req.body.organization,
                  amount: Math.round(req.body.total),
                  currency: 'eur',
                  quantity: 1,
                }],
                success_url: retUrl,
                cancel_url: errUrl
              }, {
                stripeAccount: datas[0].get_user_payments_pos.stripe_user_id
              }).then(function(resStripe) {
                  
                  

                  if(resStripe){
                    resStripe.stripe_account = datas[0].get_user_payments_pos.stripe_user_id;

                    res.status(200).send(resStripe);
                  }
                  else{
                    res.status(200).send({});
                  }    
              });
                
        }).catch(function(err) {
            console.log(err);
            res.status(500).send({});
        });
    }
    
};

exports.webhooks = function(req, res){
    
  const sig = req.headers['stripe-signature'];

  var event;
  var https = require('https');

  console.log(sig);

  try {
    event = stripe.webhooks.constructEvent(req.body.toString(), req.headers['stripe-signature'], 'whsec_CAHtJHfcIoRXL8CQqEiScgoGWC8Uu5Ps'); // LIVE
    //event = stripe.webhooks.constructEvent(req.body.toString(), req.headers['stripe-signature'], 'whsec_IvVurhvzsfAEEY1YKCuur4qufMechPbI'); // TEST
    
  } catch (err) {
    console.log(err.message);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }

  // Handle the checkout.session.completed event

  if (event.type === 'checkout.session.completed') {
    
    const session = event.data.object;

    var org = session.client_reference_id.split('-')[0];
    var payment_id = session.client_reference_id.split('-')[1];
    var contact_id = session.client_reference_id.split('-')[2];
    var tenant_id = session.client_reference_id.split('-')[3];
    var abbonamento = session.client_reference_id.split('-')[4];
    var totale = session.client_reference_id.split('-')[5];

    if(abbonamento && abbonamento != 'null'){
        sequelize.query('SELECT update_subscription_v3(' +
            tenant_id + ',\'' +
            org + '\',' +
            abbonamento + ','+
            payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
        }).catch(function(err) {
            console.log(err);
            
        });
        
    }
    else{
        sequelize.query('SELECT update_payment_stripe(\'' +
            org + '\',' +
            payment_id + ',\'' +
            moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
            true + ',\'stripe\', \'' +
            session.payment_intent + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
            sequelize.query('SELECT search_organization(\'' +
                org + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(datas_o) {
               
                    if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                        sequelize.query('SELECT update_payment_request_success(' +
                            tenant_id + ',\'' +
                            org + '\',' +
                            payment_id + ',null,true,\'Stripe\',null);', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            

                           
                        }).catch(function(err) {
                            console.log(err);
                           
                        });
                    }else{
                        sequelize.query('SELECT scuole_ecommerce_stripe_success(' +
                            datas_o[0].search_organization._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            payment_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(datas) {
                        
                                var url = '';
                                if(datas_o[0].search_organization.ecommerce_mail && 
                                    datas_o[0].search_organization.ecommerce_mail !== null && 
                                    datas_o[0].search_organization.ecommerce_mail != '' &&
                                    datas_o[0].search_organization.ecommerce_mail !== undefined){
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }else{
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }
                                

                                var request = https.get(url, function(result) {

                                    result.on('end', function() {
                                        
                                    });
                                }).on('error', function(err) {

                                    return res.status(400).render('Stripe Webhook : Mail Not Sended: ' + err);
                                });

                                request.end();

                                
                            }).catch(function(err) {
                                return res.status(400).render('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                            });    
                    }
                    
            }).catch(function(err) {
                return res.status(400).render('Stripe Webhook search_organization ' + err);
            });       
            
        }).catch(function(err) {
            console.log(err);
            
        });
    }
    
    // Fulfill the purchase...
    res.json({received: true});
    //handleCheckoutSession(session);
  }
  else{
    // Return a response to acknowledge receipt of the event
    res.json({received: true});
  } 
};

exports.webhooks_connect = function(req, res){
    
  const sig = req.headers['stripe-signature'];

  var event;

  console.log(req.body);

  var https = require('https');

  try {
    event = stripe.webhooks.constructEvent(req.body, req.headers['stripe-signature'], 'whsec_92XwheBxwxofXwldZZ6UZOL9NPtdxWAJ'); // LIVE
    //event = stripe.webhooks.constructEvent(req.body.toString(), req.headers['stripe-signature'], 'whsec_1Z0uOvbl2gL3l282dZwWqPadlIDc2nh4'); // TEST
    
  } catch (err) {
    console.log(err.message);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }

  // Handle the checkout.session.completed event

  if (event.type === 'checkout.session.completed') {
    
    const session = event.data.object;

    var org = session.client_reference_id.split('-')[0];
    var payment_id = session.client_reference_id.split('-')[1];
    var contact_id = session.client_reference_id.split('-')[2];
    var tenant_id = session.client_reference_id.split('-')[3];
    var abbonamento = session.client_reference_id.split('-')[4];
    var totale = session.client_reference_id.split('-')[5];

    if(abbonamento && abbonamento != 'null'){
        sequelize.query('SELECT update_subscription_v3(' +
            tenant_id + ',\'' +
            org + '\',' +
            abbonamento + ','+
            payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
        }).catch(function(err) {
            console.log(err);
            
        });
        
    }
    else{
        sequelize.query('SELECT update_payment_stripe(\'' +
            org + '\',' +
            payment_id + ',\'' +
            moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
            true + ',\'stripe\', \'' +
            session.payment_intent + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
            sequelize.query('SELECT search_organization(\'' +
                org + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas_o) {
               
                    if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                        sequelize.query('SELECT update_payment_request_success(' +
                            tenant_id + ',\'' +
                            org + '\',' +
                            payment_id + ',null,true,\'Stripe\',null);', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            

                           
                        }).catch(function(err) {
                            console.log(err);
                           
                        });
                    }else{
                        sequelize.query('SELECT scuole_ecommerce_stripe_success(' +
                            datas_o[0].search_organization._tenant_id + ',\'' +
                            org + '\',' +
                            payment_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(datas) {
                        
                                var url = '';
                                if(datas_o[0].search_organization.ecommerce_mail && 
                                    datas_o[0].search_organization.ecommerce_mail !== null && 
                                    datas_o[0].search_organization.ecommerce_mail != '' &&
                                    datas_o[0].search_organization.ecommerce_mail !== undefined){
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }else{
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }
                                

                                var request = https.get(url, function(result) {

                                    result.on('end', function() {
                                        res.status(200).send({});
                                    });
                                }).on('error', function(err) {

                                    return res.status(400).render('Stripe Webhook : Mail Not Sended: ' + err);
                                });

                                request.end();

                                
                            }).catch(function(err) {
                                console.log('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                               //return res.status(400).render('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                            });    
                    }
                    
            }).catch(function(err) {
                console.log('Stripe Webhook search_organization ' + err);
                //return res.status(400).render('Stripe Webhook search_organization ' + err);
            });       
            
        }).catch(function(err) {
            console.log(err);
            
        });
    }
    
    // Fulfill the purchase...
    res.json({received: true});
    //handleCheckoutSession(session);
  }
  else{
    // Return a response to acknowledge receipt of the event
    res.json({received: true});
  }
  
};

exports.webhooks_test = function(req, res){
    
  const sig = req.headers['stripe-signature'];

  var event;
  var https = require('https');

  console.log(req.body.toString());

  /*
  try {
    //event = stripe.webhooks.constructEvent(req.body.toString(), req.headers['stripe-signature'], 'whsec_CAHtJHfcIoRXL8CQqEiScgoGWC8Uu5Ps'); // LIVE
    //event = stripe.webhooks.constructEvent(req.body.toString(), req.headers['stripe-signature'], 'whsec_IvVurhvzsfAEEY1YKCuur4qufMechPbI'); // TEST
    event = JSON.parse(req.body.toString());
  } catch (err) {
    console.log(err.message);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }*/

   event = JSON.parse(req.body.toString());

  // Handle the checkout.session.completed event

  if (event.type === 'checkout.session.completed') {
    
    const session = event.data.object;

    var org = session.client_reference_id.split('-')[0];
    var payment_id = session.client_reference_id.split('-')[1];
    var contact_id = session.client_reference_id.split('-')[2];
    var tenant_id = session.client_reference_id.split('-')[3];
    var abbonamento = session.client_reference_id.split('-')[4];
    var totale = session.client_reference_id.split('-')[5];

    if(abbonamento && abbonamento != 'null'){
        sequelize.query('SELECT update_subscription_v3(' +
            tenant_id + ',\'' +
            org + '\',' +
            abbonamento + ','+
            payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
        }).catch(function(err) {
            console.log(err);
            
        });
        
    }
    else{
        sequelize.query('SELECT update_payment_stripe(\'' +
            org + '\',' +
            payment_id + ',\'' +
            moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
            true + ',\'stripe\', \'' +
            session.payment_intent + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
            sequelize.query('SELECT search_organization(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(datas_o) {
               
                    if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                        sequelize.query('SELECT update_payment_request_success(' +
                            tenant_id + ',\'' +
                            org + '\',' +
                            payment_id + ',null,true,\'Stripe\',null);', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            

                           
                        }).catch(function(err) {
                            console.log(err);
                           
                        });
                    }else{
                        sequelize.query('SELECT scuole_ecommerce_stripe_success(' +
                            datas_o[0].search_organization._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            payment_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(datas) {
                        
                                var url = '';
                                if(datas_o[0].search_organization.ecommerce_mail && 
                                    datas_o[0].search_organization.ecommerce_mail !== null && 
                                    datas_o[0].search_organization.ecommerce_mail != '' &&
                                    datas_o[0].search_organization.ecommerce_mail !== undefined){
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }else{
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }
                                

                                var request = https.get(url, function(result) {

                                    result.on('end', function() {
                                        
                                    });
                                }).on('error', function(err) {

                                    return res.status(400).render('Stripe Webhook : Mail Not Sended: ' + err);
                                });

                                request.end();

                                res.status(200).send({});
                            }).catch(function(err) {
                                return res.status(400).render('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                            });    
                    }
                    
            }).catch(function(err) {
                return res.status(400).render('Stripe Webhook search_organization ' + err);
            });       
            
        }).catch(function(err) {
            console.log(err);
            
        });
    }
    
    // Fulfill the purchase...
    res.json({received: true});
    //handleCheckoutSession(session);
  }
  else{
    // Return a response to acknowledge receipt of the event
    res.json({received: true});
  } 
};

exports.webhooks_connect_test = function(req, res){
    
  const sig = req.headers['stripe-signature'];

  var event;

  console.log(req.body);

  var https = require('https');

  try {
    //event = stripe.webhooks.constructEvent(req.body, req.headers['stripe-signature'], 'whsec_92XwheBxwxofXwldZZ6UZOL9NPtdxWAJ'); // LIVE
    event = stripe.webhooks.constructEvent(req.body.toString(), req.headers['stripe-signature'], 'whsec_0byDsi4lgYSXRwKb3qDedLsKMhN1x68F'); // TEST
    
  } catch (err) {
    console.log(err.message);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }

  // Handle the checkout.session.completed event

  if (event.type === 'checkout.session.completed') {
    
    const session = event.data.object;

    var org = session.client_reference_id.split('-')[0];
    var payment_id = session.client_reference_id.split('-')[1];
    var contact_id = session.client_reference_id.split('-')[2];
    var tenant_id = session.client_reference_id.split('-')[3];
    var abbonamento = session.client_reference_id.split('-')[4];
    var totale = session.client_reference_id.split('-')[5];

    if(abbonamento && abbonamento != 'null'){
        sequelize.query('SELECT update_subscription_v3(' +
            tenant_id + ',\'' +
            org + '\',' +
            abbonamento + ','+
            payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
        }).catch(function(err) {
            console.log(err);
            
        });
        
    }
    else{
        sequelize.query('SELECT update_payment_stripe(\'' +
            org + '\',' +
            payment_id + ',\'' +
            moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
            true + ',\'stripe\', \'' +
            session.payment_intent + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
            sequelize.query('SELECT search_organization(\'' +
                org + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas_o) {
               
                    if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                        sequelize.query('SELECT update_payment_request_success(' +
                            tenant_id + ',\'' +
                            org + '\',' +
                            payment_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            

                           
                        }).catch(function(err) {
                            console.log(err);
                           
                        });
                    }else{
                        sequelize.query('SELECT scuole_ecommerce_stripe_success(' +
                            datas_o[0].search_organization._tenant_id + ',\'' +
                            org + '\',' +
                            payment_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(datas) {
                        
                                var url = '';
                                if(datas_o[0].search_organization.ecommerce_mail && 
                                    datas_o[0].search_organization.ecommerce_mail !== null && 
                                    datas_o[0].search_organization.ecommerce_mail != '' &&
                                    datas_o[0].search_organization.ecommerce_mail !== undefined){
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }else{
                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${org}&payment_id=${payment_id}&completo=true&esito=OK`;
                                }
                                

                                var request = https.get(url, function(result) {

                                    result.on('end', function() {
                                        res.status(200).send({});
                                    });
                                }).on('error', function(err) {

                                    return res.status(400).render('Stripe Webhook : Mail Not Sended: ' + err);
                                });

                                request.end();

                                
                            }).catch(function(err) {
                                console.log('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                               //return res.status(400).render('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                            });    
                    }
                    
            }).catch(function(err) {
                console.log('Stripe Webhook search_organization ' + err);
                //return res.status(400).render('Stripe Webhook search_organization ' + err);
            });       
            
        }).catch(function(err) {
            console.log(err);
            
        });
    }
    
    // Fulfill the purchase...
    res.json({received: true});
    //handleCheckoutSession(session);
  }
  else{
    // Return a response to acknowledge receipt of the event
    res.json({received: true});
  }
  
};

/*
exports.create_charge = function(req, res) {
    var token = req.body.stripeToken;
    var contact_id = req.query.contact_id;
    var retUrl = 'https://' + req.headers['host'] + '/start/#/pay_me?access_token=' + req.query.access_token + '&payment_id=' + req.query.payment_id;
    
    if (req.headers['host'].split(".")[0] === 'poppixsrl') {
    	console.log('Creo pagamento per PoppixSrl');
        const charge = stripe.charges.create({
            amount: Math.round(req.query.total),
            currency: 'eur',
            description: 'pagamento a ' + req.headers['host'].split(".")[0],
            source: token
        }).then(function(resStripe) {
        	console.log(resStripe);
            saveStripeResponse(resStripe, req, res);
        }).catch(function(err) {
            console.log(err);
            res.redirect(retUrl);
        });
    } else {
        sequelize.query('SELECT get_user_payments_pos(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                //callback(null, datas[0].get_user_payments_pos);

                if (datas && datas[0] && datas[0].get_user_payments_pos && datas[0].get_user_payments_pos.stripe_user_id) {
                	console.log('Creo pagamento per Utente ' + datas[0].get_user_payments_pos.stripe_user_id);
                    const charge = stripe.charges.create({
                        amount: Math.round(req.query.total),
                        currency: 'eur',
                        description: 'pagamento a ' + req.headers['host'].split(".")[0],
                        source: token
                    }, {
                        stripe_account: datas[0].get_user_payments_pos.stripe_user_id
                    }).then(function(resStripe) {
                        saveStripeResponse(resStripe, req, res);
                    });
                } else {
                    console.log(err);
                    res.redirect(retUrl);
                }

            }).catch(function(err) {
                console.log(err);
                res.redirect(retUrl);
            });
    }
};*/

function saveStripeResponse(resStripe, req, res) {
	var retUrl = 'https://' + req.headers['host'] + '/start/#/pay_me?access_token=' + req.query.access_token + '&payment_id=' + req.query.payment_id;

    if (resStripe.status === 'succeeded') {
    	console.log('Pagamento riuscito');
    	console.log(resStripe);
        sequelize.query('SELECT update_payment_stripe(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.query.payment_id + ',\'' +
                moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
                true + ',\'stripe\', \'' +
                resStripe.balance_transaction + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                sequelize.query('SELECT update_payment_request_success(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.query.payment_id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {
                        res.redirect(retUrl + '&status=' + resStripe.status);
                    });
            }).catch(function(err) {
                console.log(err);
                res.redirect(retUrl);
            });
    } else {
    	console.log('Pagamento fallito');
    	console.log(resStripe);
		res.redirect(retUrl);
    }
}