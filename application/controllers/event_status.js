var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.index = function(req, res) {

    utility.get_parameters(req, 'event_status', function(err, parameters) {
        if (err) {
            return res.status(400).render('event_states_index: ' + err);
        }

        async.parallel({
            data: function(callback) {
                var response = {};
                sequelize.query('SELECT event_states_data_v3(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                        (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                        (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                        parameters.all_any + ',' +
                        parameters.archivied + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        if (datas[0].event_states_data_v3 && datas[0].event_states_data_v3 != null) {
                            callback(null, datas[0].event_states_data_v3);

                        } else {

                            callback(null, []);
                        }

                    }).catch(function(err) {
                        console.log(err);
                        callback(err, null);
                    });
            }
        }, function(err, results) {

            if (err) {
                return res.status(400).render('event_states_index: ' + err);
            }

            res.json({
                'data': results.data
            });
        });
    });
};

exports.find = function(req, res) {
    var start_date = null,
        end_date = null,
        q = [],
        archivied = false;

    utility.get_parameters(req, 'contact', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        async.parallel({
            data: function(callback) {
                var response = {};
                sequelize.query('SELECT event_states_find_data_v3(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.params.id + ',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                        parameters.archivied + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        if (datas[0].event_states_find_data_v3 && datas[0].event_states_find_data_v3 != null) {
                            callback(null, datas[0].event_states_find_data_v3);

                        } else {

                            callback(null, []);
                        }

                    }).catch(function(err) {
                        console.log(err);
                        callback(err, null);
                    });
            }
        }, function(err, results) {

            if (err) {
                return res.status(400).render('event_states_find: ' + err);
            }

            res.json({
                'data': results.data[0]
            });
        });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var event_status = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        event_status['rank'] = (req.body.data.attributes.rank && !utility.check_id(req.body.data.attributes.rank) ? req.body.data.attributes.rank : null);
        event_status['organization_id'] = req.user.organization_id;
        event_status['status_name'] = (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name, 'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '');
        event_status['status_color'] = (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color, 'string') && req.body.data.attributes.status_color !== null ? req.body.data.attributes.status_color.replace(/'/g, "''''") : '');
        event_status['status_default'] = req.body.data.attributes.status_default;
        event_status['_tenant_id'] = req.user._tenant_id;
        event_status['organization'] = req.headers['host'].split(".")[0];
        event_status['mongo_id'] = null;

        sequelize.query('SELECT insert_event_status(\'' + JSON.stringify(event_status) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_event_status;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'event_status',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_event_status,
                    'Aggiunto event_status ' + datas[0].insert_event_status
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'event_state', 'insert', req.user.id, datas[0].insert_event_status, req.body.data);
        
                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('event_status_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT update_event_status(' +
                req.params.id + ',' +
                (req.body.data.attributes.rank !== null && !utility.check_id(req.body.data.attributes.rank) ? req.body.data.attributes.rank : null) + ',\'' +
                (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name, 'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color, 'string') && req.body.data.attributes.status_color !== null ? req.body.data.attributes.status_color.replace(/'/g, "''''") : '') + '\',' +
                req.body.data.attributes.status_default + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'event_status',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato event_status ' + req.params.id
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'event_state', 'update', req.user.id, req.params.id, req.body.data);
        

                res.json({
                    'data': req.body.data
                });

            }).catch(function(err) {
                return res.status(400).render('event_status_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_event_status_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            utility.save_socket(req.headers['host'].split(".")[0], 'event_state', 'delete', req.user.id, req.params.id, null);
        

            res.json({
                'data': {
                    'type': 'events',
                    'id': datas[0].delete_event_status_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('eventstatus_destroy: ' + err);
        });
};