var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    async = require('async'),
    config = require('../../config'),
    utils = require('../../utils');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var sortField = (req.query.sort === undefined) ? 'organization' : req.query.sort,
        sortDirection = (req.query.direction === undefined) ? 'asc' : req.query.direction,
        page_num = 1,
        page_count = 50;

    if (req.query.pag !== undefined && req.query.pag !== '') {
        if (utility.check_id(req.query.pag)) {
            return res.status(422).send(utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_num = parseInt(req.query.pag);
    }

    if (req.query.xpag !== undefined && req.query.xpag !== '') {
        if (utility.check_id(req.query.xpag)) {
            return res.status(422).send(utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_count = parseInt(req.query.xpag);
    }

    sequelize.query('SELECT organizations_data(' +
        page_count + ',' +
        (page_num - 1) * page_count + ',' +
        req.user._tenant_id + ',\'' +
        sortDirection + '\',' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {
        if (datas[0].organizations_data && datas[0].organizations_data != null) {
            res.json({
                'data': datas[0].organizations_data
            });
        } else {
            res.json({
                'data': []
            });
        }

    }).catch(function(err) {
        return res.status(400).render('organization_index: ' + err);
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific organization searched by id
 */
exports.find = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT organization_find_data(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {

                if (datas[0].organization_find_data && datas[0].organization_find_data != null) {
                    callback(null, datas[0].organization_find_data[0]);

                } else {

                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });

        },
        included: function(callback) {
            sequelize.query('SELECT organization_find_included(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(included) {

                if (included[0].organization_find_included && included[0].organization_find_included != null) {
                    callback(null, included[0].organization_find_included);

                } else {

                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('organization_find: ' + err);
        }

        res.json({
            'data': utility.check_find_datas(results.data, 'organizations', []),
            'included': utility.check_find_included(results.included, 'organizations', ['contacts', 'organization_custom_fields', 'event_states', 'contact_states',
                'product_states', 'project_states', 'workhour_states'
            ])
        });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing organization searched by id
 * When a organization change we must change also all embedded documents related
 */
exports.update = function(req, res) {

    /*if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }*/
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var organization = {};

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes.organization == undefined || req.body.data.attributes.organization === '') {
        return res.status(422).send(utility.error422('organization', 'Invalid Attribute', 'Il campo organization non può essere vuoto', '422'));

    }

    sequelize.query('SELECT update_organization(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.dashboard_name && utility.check_type_variable(req.body.data.attributes.dashboard_name, 'string') && req.body.data.attributes.dashboard_name !== null ? req.body.data.attributes.dashboard_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.tracker_name && utility.check_type_variable(req.body.data.attributes.tracker_name, 'string') && req.body.data.attributes.tracker_name !== null ? req.body.data.attributes.tracker_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.trackers_name && utility.check_type_variable(req.body.data.attributes.trackers_name, 'string') && req.body.data.attributes.trackers_name !== null ? req.body.data.attributes.trackers_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.contact_name && utility.check_type_variable(req.body.data.attributes.contact_name, 'string') && req.body.data.attributes.contact_name !== null ? req.body.data.attributes.contact_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.contacts_name && utility.check_type_variable(req.body.data.attributes.contacts_name, 'string') && req.body.data.attributes.contacts_name !== null ? req.body.data.attributes.contacts_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.product_name && utility.check_type_variable(req.body.data.attributes.product_name, 'string') && req.body.data.attributes.product_name !== null ? req.body.data.attributes.product_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.products_name && utility.check_type_variable(req.body.data.attributes.products_name, 'string') && req.body.data.attributes.products_name !== null ? req.body.data.attributes.products_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.project_name && utility.check_type_variable(req.body.data.attributes.project_name, 'string') && req.body.data.attributes.project_name !== null ? req.body.data.attributes.project_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.projects_name && utility.check_type_variable(req.body.data.attributes.project_name, 'string') && req.body.data.attributes.projects_name !== null ? req.body.data.attributes.projects_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.event_name && utility.check_type_variable(req.body.data.attributes.event_name, 'string') && req.body.data.attributes.event_name !== null ? req.body.data.attributes.event_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.events_name && utility.check_type_variable(req.body.data.attributes.events_name, 'string') && req.body.data.attributes.events_name !== null ? req.body.data.attributes.events_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.estimate_name && utility.check_type_variable(req.body.data.attributes.estimate_name, 'string') && req.body.data.attributes.estimate_name !== null ? req.body.data.attributes.estimate_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.estimates_name && utility.check_type_variable(req.body.data.attributes.estimates_name, 'string') && req.body.data.attributes.estimates_name !== null ? req.body.data.attributes.estimates_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.invoice_name && utility.check_type_variable(req.body.data.attributes.invoice_name, 'string') && req.body.data.attributes.invoice_name !== null ? req.body.data.attributes.invoice_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.invoices_name && utility.check_type_variable(req.body.data.attributes.invoices_name, 'string') && req.body.data.attributes.invoices_name !== null ? req.body.data.attributes.invoices_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.expense_name && utility.check_type_variable(req.body.data.attributes.expense_name, 'string') && req.body.data.attributes.expense_name !== null ? req.body.data.attributes.expense_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.expenses_name && utility.check_type_variable(req.body.data.attributes.expenses_name, 'string') && req.body.data.attributes.expenses_name !== null ? req.body.data.attributes.expenses_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.cost_name && utility.check_type_variable(req.body.data.attributes.cost_name, 'string') && req.body.data.attributes.cost_name !== null ? req.body.data.attributes.cost_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.costs_name && utility.check_type_variable(req.body.data.attributes.costs_name, 'string') && req.body.data.attributes.costs_name !== null ? req.body.data.attributes.costs_name.replace(/'/g, "''''") : '') + '\',' +
            (req.body.data.attributes.sms_remained && !utility.check_id(req.body.data.attributes.sms_remained) ? req.body.data.attributes.sms_remained : null) + ',\'' +
            (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url : '') + '\',\'' +
            (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type : '') + '\',\'' +
            (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name : '') + '\',\'' +
            (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_eta : '') + '\',\'' +
            (req.body.data.attributes.primary_color && utility.check_type_variable(req.body.data.attributes.primary_color, 'string') && req.body.data.attributes.primary_color !== null ? req.body.data.attributes.primary_color.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.secondary_color && utility.check_type_variable(req.body.data.attributes.secondary_color, 'string') && req.body.data.attributes.secondary_color !== null ? req.body.data.attributes.secondary_color.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.primary_text_color && utility.check_type_variable(req.body.data.attributes.primary_text_color, 'string') && req.body.data.attributes.primary_text_color !== null ? req.body.data.attributes.primary_text_color.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.secondary_text_color && utility.check_type_variable(req.body.data.attributes.secondary_text_color, 'string') && req.body.data.attributes.secondary_text_color !== null ? req.body.data.attributes.secondary_text_color.replace(/'/g, "''''") : '') + '\',' +
            (req.body.data.attributes.new_registration && utility.check_type_variable(req.body.data.attributes.new_registration, 'boolean') ? req.body.data.attributes.new_registration : false) + ',\'' +
            (req.body.data.attributes.order_name && utility.check_type_variable(req.body.data.attributes.order_name, 'string') && req.body.data.attributes.order_name !== null ? req.body.data.attributes.order_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.orders_name && utility.check_type_variable(req.body.data.attributes.orders_name, 'string') && req.body.data.attributes.orders_name !== null ? req.body.data.attributes.orders_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.ddt_name && utility.check_type_variable(req.body.data.attributes.ddt_name, 'string') && req.body.data.attributes.ddt_name !== null ? req.body.data.attributes.ddt_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.ddts_name && utility.check_type_variable(req.body.data.attributes.ddts_name, 'string') && req.body.data.attributes.ddts_name !== null ? req.body.data.attributes.ddts_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.contract_name && utility.check_type_variable(req.body.data.attributes.contract_name, 'string') && req.body.data.attributes.contract_name !== null ? req.body.data.attributes.contract_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.contracts_name && utility.check_type_variable(req.body.data.attributes.contracts_name, 'string') && req.body.data.attributes.contracts_name !== null ? req.body.data.attributes.contracts_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.option_field && utility.check_type_variable(req.body.data.attributes.option_field, 'string') && req.body.data.attributes.option_field !== null ? req.body.data.attributes.option_field.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.receipt_name && utility.check_type_variable(req.body.data.attributes.receipt_name, 'string') && req.body.data.attributes.receipt_name !== null ? req.body.data.attributes.receipt_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.receipts_name && utility.check_type_variable(req.body.data.attributes.receipts_name, 'string') && req.body.data.attributes.receipts_name !== null ? req.body.data.attributes.receipts_name.replace(/'/g, "''''") : '') + '\'' +
            
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            utility.save_socket(req.headers['host'].split(".")[0], 'organization', 'update', req.user.id, req.params.id, req.body.data);
                        

            res.json({
                'data': req.body.data,
                'relationships': req.body.data.relationships
            });

        }).catch(function(err) {
            return res.status(400).render('organization_udpate: ' + err);
        });
};

exports.insert_organization_consensi = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT update_organization_consensi_all(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};


exports.update_option_field = function(req, res) {
    

    sequelize.query('SELECT update_organization_option_field(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.livi_connect = function(req, res) {
    
    sequelize.query('SELECT insert_livi_contacts(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            (req.body.livi_url).replace(/'/g, "''''") + '\','+
            req.body.contact_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.livi_disconnect = function(req, res) {
    
    sequelize.query('SELECT delete_livi_contacts(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            (req.body.livi_url).replace(/'/g, "''''") + '\','+
            req.body.contact_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.delete_organization_consensi = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT delete_organization_consensi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.params.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.update_gdpr = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT update_organization_gdpr_2018(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.dashboard = function(req, res) {
    var from_date = null,
        to_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT get_dashboard(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        from_date + ',' +
        to_date + ',' +
        req.user.id + ',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send({ 'data': datas[0].get_dashboard });
    }).catch(function(err) {
        return res.status(400).render('get_dashboard: ' + err);
    });
};

exports.onboarding_checklist = function(req, res) {
       
    sequelize.query('SELECT get_onboarding_checklist(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send(datas[0].get_onboarding_checklist );
    }).catch(function(err) {
        return res.status(400).render('get_onboarding_checklist: ' + err);
    });
};

exports.clinic_crea_dati_demo = function(req, res) {
   
    sequelize.query('SELECT clinic_crea_dati_demo(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {
            res.status(200).send({});
    }).catch(function(err) {
        return res.status(400).render('clinic_crea_dati_demo: ' + err);
    });
};

exports.create_backup = function(req, res) {
    req.setTimeout(500000);
    var request = require('request');
    
    var url = 'https://google-calendar.beebeeboard.com/api/v1/create_backup?contact_id='+req.user.id+'&organization_id='+req.user._tenant_id+'&organization='+req.headers['host'].split(".")[0];
    console.log(url);
    request.get(url, {
        timeout: 500000,
        json: false
    }, function(error, result) {
        if(error){
            console.log(error);
            return res.status(500).send({});
        }
        else
        {
            res.status(200).send();
        }
    });
};

exports.index_backup = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT organizations_backup(' +
        req.user._tenant_id + ',' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {
        if (datas[0].organizations_backup && datas[0].organizations_backup != null) {
            res.json({
                'data': datas[0].organizations_backup[0]
            });
        } else {
            res.json({
                'data': []
            });
        }

    }).catch(function(err) {
        return res.status(400).render('organization_index_backup: ' + err);
    });
};

exports.delete_backup = function(req, res) {
    const { S3Client,  DeleteObjectCommand } = require('@aws-sdk/client-s3');

    const s3 = new S3Client({
            region: config.aws_s3.region,
            credentials:{
                accessKeyId: config.aws_s3.accessKeyId,
                secretAccessKey: config.aws_s3.secretAccessKey,
            }
        });


   

    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (!req.body.name) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    var params = {
        Bucket: 'beebee-backups',
        Key: req.body.name
    };
     const command = new DeleteObjectCommand(params);
    s3.send(command, function(err, data) {
    //s3.deleteObject(params, function(err, data) {
        if (err)
            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore delete file : ' + err);

        sequelize.query('SELECT delete_organizations_backup(' +
            req.params.id + ',' +
            req.user._tenant_id + ',' +
            req.user.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {
            res.status(200).json({});

        }).catch(function(err) {
            return res.status(400).render('delete_organizations_backup: ' + err);
        });

    });
};

exports.restore_backup = function(req, res) {
    req.setTimeout(500000);
    // Load in our dependencies
    var assert = require('assert');
    var fs = require('fs');
   const { S3Client, PutObjectCommand, GetObjectCommand } = require('@aws-sdk/client-s3');
    var spawn = require('child_process').spawn;
    var async = require('async');

    // Define our constants upfront
    var dbName = config.db.type + '://' + config.db.user + ':' + config.db.dbpwd + '@' + config.db.dbUrl + ':5432/' + req.headers['host'].split(".")[0];
    var S3_BUCKET = 'beebee-backups';

    const s3 = new S3Client({
        region: config.aws_s3.region,
        credentials:{
            accessKeyId: config.aws_s3.accessKeyId,
            secretAccessKey: config.aws_s3.secretAccessKey,
        }
    });
    // Determine our filename

    if (req.body['name'] !== undefined && req.body['name'] !== '') {
        if (req.body['etag'] != undefined && req.body['etag'] != '') {
            var params = {
                Bucket: 'beebee-backups',
                Key: req.body['name'],
                IfMatch: req.body['etag'].replace(/"/g, "")
            };
        } else {
            var params = {
                Bucket: 'beebee-backups',
                Key: req.body['name']
            };
        }

        var filepath = fs.createWriteStream(req.body['name']);
        s3
            .getObject(params)
            .createReadStream()
            .pipe(filepath)
            .on('error', function(err) {
                console.log(err);
            })
            /*
                      .on('httpData', function (chunk) {
                          filepath.write(chunk);
                      })*/
            .on('finish', function() {
                console.log('ciao');
                // filepath.end();
                var gzipChild = spawn('gzip', ['-d', filepath.path], { stdio: ['pipe', 'pipe', 'inherit'] });

                gzipChild.on('exit', function(code) {
                    if (code !== 0) {
                        throw new Error('gzip: Bad exit code (' + code + ')');
                    }
                });

                gzipChild.on('close', function handleFinish1() {

                    async.parallel({
                        query: function(callback) {
                            sequelize.query('SELECT delete_before_restore_backups(' +
                                req.user.id + ',' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                }).then(function(datas) {
                                callback(null, 1);


                            }).catch(function(err) {
                                callback(err, null);
                            });
                        }
                    }, function(err, resultats) {
                        if (err) {
                            return res.status(400).render('delete_before_restore_backups: ' + err);
                        } else {
                            console.log(dbName);
                            var pgRestoreChild = spawn('pg_restore', ['-d', dbName, '--clean', '--disable-triggers', '--verbose', filepath.path.replace('.gz', '')]);


                            pgRestoreChild.on('error', function(code) {
                                if (code !== 0) {
                                    throw new Error('pg_restore -a: Bad exit code (' + code + ')');
                                }
                            });

                            pgRestoreChild.stderr.on('data', (data) => {
                                console.log(`grep stderr: ${data}`);
                            });

                            pgRestoreChild.stdout.on('data', (data) => {

                            });

                            pgRestoreChild.on('close', function handleFinish2() {

                                fs.unlink(filepath.path.replace('.gz', ''), function(err) {

                                    if (err)
                                        throw err;

                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' successfully deleted ' + filepath.path);
                                    res.status(200).json({});
                                });

                            });
                        }

                    });


                });
            });


    }
};

exports.cron_personalizzato = function(req, res) {
    sequelize.query('SELECT cron_personalizzato(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
    .then(function(datas) {
            res.status(200).send({});
    }).catch(function(err) {
        return res.status(422).render('Cron personalizzato: ' + err);
    });
};

exports.get_code_association = function(req, res) {
    sequelize.query('SELECT get_code_association(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].get_code_association ){
                if(datas[0].get_code_association != 0) {
                    res.status(200).send({
                        'code': datas[0].get_code_association
                    });
                }else
                {
                    return res.status(422).send('Nessuna registrazione di certificato abilitata. Contatta il team di Beebeeboard per attivare il servizio di Firma Grafometrica');
                }
            }
            
        });
};

exports.device_association = function(req, res) {

    var code_ = null;
    var device_id = null;

    if (req.query.code !== undefined && req.query.code !== '') {     
        code_ = parseInt(req.query.code);
    }else{
        return res.status(422).send(utility.error422('codice', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.device_uid !== undefined && req.query.device_uid !== '') {     
        device_id = '\''+req.query.device_uid+'\'';
    }else{
        return res.status(422).send(utility.error422('device_uid', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT device_association(' +
            code_ +','+
            device_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            
                if(datas[0].device_association) {
                    /* genero token senza scadenza */
                    var res_json = datas[0].device_association;

                    var token = utils.uid(config.token.accessTokenLength);
                  
                    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                        moment(new Date()).add(20, 'years').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                        res_json.user_id + ',' +
                        5 + ',\'\',\'' + res_json.organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(tok) {
                            res_json.token = token;
                            console.log(res_json);

                            utility.save_socket(res_json.organization, 'device', 'update', res_json.user_id, parseInt(code_), null);
        
                            res.status(200).json(res_json);
                        }).catch(function(err) {
                                return res.status(400).render('user_registration: ' + err);
                        });
                }else
                {
                    return res.status(422).send('Codice inserito non valido, Riprova da Impostazioni ad associare un altro codice');
                }
          
            
        });
};

exports.beebeesign_dashboard = function(req, res) {

    var device_uid = null;

    if (req.query.device_uid !== undefined && req.query.device_uid !== '') {     
       device_uid = '\''+req.query.device_uid+'\'';
    }
    
    sequelize.query('SELECT beebeesign_dashboard(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        device_uid+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        res.status(200).send(datas[0].beebeesign_dashboard[0]);
    }).catch(function(err) {
        return res.status(400).render('beebeesign_dashboard: ' + err);
    });
};

exports.get_certificate = function(req, res) {

    var device_uid = null;

    if (req.query.device_uid !== undefined && req.query.device_uid !== '') {     
       device_uid = '\''+req.query.device_uid+'\'';
    }
    
    sequelize.query('SELECT get_sign_certificate(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        device_uid+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
         res.status(200).send(datas[0].get_sign_certificate);
    }).catch(function(err) {
        return res.status(400).render('get_sign_certificate: ' + err);
    });
};

exports.license_device = function(req, res) {

    var device_uid = null;
    var certificate_id = null;
    var licenza = null;
    var expire_date = null;

    if (req.body.device_uid !== undefined && req.body.device_uid !== '') {     
       device_uid = '\''+req.body.device_uid+'\'';
    }else{
        return res.status(422).send(utility.error422('device_uid', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.certificate_id !== undefined && req.body.certificate_id !== '') {     
       certificate_id = req.body.certificate_id;
    }else{
        return res.status(422).send(utility.error422('certificate_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.expire_date !== undefined && req.body.expire_date !== '') {     
       expire_date = req.body.expire_date;
    }else{
        return res.status(422).send(utility.error422('expire_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    
    sequelize.query('SELECT license_device(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        device_uid+','+
        certificate_id+','+
        expire_date+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(datas) {
         res.status(200).send();
    }).catch(function(err) {
        return res.status(400).render('license_device: ' + err);
    });
};

exports.update_identifier_device = function(req, res) {

    var device_id = null;
    var name = null;

    if (req.body.id !== undefined && req.body.id !== '') {     
       device_id = req.body.id;
    }else{
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.identifier !== undefined && req.body.identifier !== '') {     
       name = '\''+req.body.identifier+'\'';
    }else{
        return res.status(422).send(utility.error422('identifier', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    sequelize.query('SELECT update_device_identifier(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        device_id+','+
        name+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(datas) {
         res.status(200).send();
    }).catch(function(err) {
        return res.status(400).render('update_device_identifier: ' + err);
    });
};

exports.beebeesign_errors = function(req, res) {
     sequelize.query('SELECT beebeesign_errors('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].beebeesign_errors) {
                return res.status(200).send(datas[0].beebeesign_errors);
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('beebeesign_errors: ' + err);
        });
};


exports.delete_device = function(req, res) {

   
    sequelize.query('SELECT delete_device(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(!datas[0].delete_device){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }else{
                res.json({
                    'data': {
                        'type': 'device',
                        'id': datas[0].delete_device
                    }
                });
            }

        }).catch(function(err) {
            return res.status(400).render('delete_device: ' + err);
        });
};