var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    /*var application = {};

      if (req.body.data.attributes !== undefined) {
        // Import case

        application['user_proper'] = req.body.data.attributes.user_proper;
        application['contact_view'] = (req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g,"''") : '');
        application['human_name'] = (req.body.data.attributes.human_name !== null ? req.body.data.attributes.human_name.replace(/'/g,"''") : '');
        application['_tenant_id'] = req.user._tenant_id;
        application['organization'] = req.headers['host'].split(".")[0];
        application['user_id'] = ((req.body.data.relationships.user && req.body.data.relationships.user.data && req.body.data.relationships.user.data.id) ? req.body.data.relationships.user.data.id : null);
        application['mongo_id'] = null;

        sequelize.query('SELECT insert_application(\'' + JSON.stringify(application) + '\');', {
          raw: true,
          type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
        .then(function (datas) {
          req.body.data.id = datas[0].insert_application;
          
          res.json({
            'data': req.body.data,
            'relationships': req.body.data.relationships
          });

        }).catch(function(err){
         return res.status(400).render('application_insert: '+err);
       });
      }
      */
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    /*
      if(isNaN(parseInt(req.params.id)))
      {
        return res.status(422).send({
          'errors': [{
            'status': '422',
            'source': {
              'pointer': '/data/attributes/id'
            },
            'title': 'Parametro non valido',
            'detail': 'Parametro non valido'
          }]
        });
      }
      if (req.body.data.attributes !== undefined) {
        // Import case


        sequelize.query('SELECT update_application(' + 
          req.params.id + ','+
          req.user._tenant_id+',\''+
          req.headers['host'].split(".")[0]+'\','+
          req.body.data.attributes.user_proper +','+
          (req.body.data.attributes.name !== null ? '\''+req.body.data.attributes.name+'\'' : '')+','+
          (req.body.data.attributes.human_name !== null ? '\''+req.body.data.attributes.human_name+'\'' : '')+','+
          ((req.body.data.relationships.user && req.body.data.relationships.user.data && req.body.data.relationships.user.data.id) ? req.body.data.relationships.user.data.id : null)+
          ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
          })
        .then(function (datas) {
          res.json({
            'data': req.body.data,
            'relationships': req.body.data.relationships
          });

        }).catch(function(err){
          return res.status(400).render('application_update: '+err);
        });
      }*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    /*
      if(isNaN(parseInt(req.params.id)))
      {
        return res.status(422).send({
          'errors': [{
            'status': '422',
            'source': {
              'pointer': '/data/attributes/id'
            },
            'title': 'Parametro non valido',
            'detail': 'Parametro non valido'
          }]
        });
      }
      sequelize.query('SELECT delete_application(' + req.params.id + ',' + req.user._tenant_id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
      })
      .then(function (datas) {
        
        res.json({
          'data': {
            'type': 'applications',
            'id': req.params.id
          }
        });

      }).catch(function(err){
       return res.status(400).render('application_destroy: '+err);
     });*/
};