var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

    var user_invoice_setting = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        user_invoice_setting['bank_name'] = (req.body.data.attributes.bank_name !== null ? req.body.data.attributes.bank_name.replace(/'/g, "''''") : '');
        user_invoice_setting['bank_iban'] = (req.body.data.attributes.bank_iban !== null ? req.body.data.attributes.bank_iban.replace(/'/g, "''''") : '');
        user_invoice_setting['bank_bic'] = (req.body.data.attributes.bank_bic !== null ? req.body.data.attributes.bank_bic.replace(/'/g, "''''") : '');
        user_invoice_setting['rea'] = (req.body.data.attributes.rea !== null ? req.body.data.attributes.rea.replace(/'/g, "''''") : '');
        user_invoice_setting['regime_fiscale'] = (req.body.data.attributes.regime_fiscale !== null ? req.body.data.attributes.regime_fiscale.replace(/'/g, "''''") : '');
        user_invoice_setting['user_id'] = ((req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id) ? req.body.data.relationships.contact.data.id : null);
        user_invoice_setting['_tenant_id'] = req.user._tenant_id;
        user_invoice_setting['organization'] = req.headers['host'].split(".")[0];
        user_invoice_setting['ts_username'] = (req.body.data.attributes.ts_username !== null ? req.body.data.attributes.ts_username.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_pwd'] = (req.body.data.attributes.ts_pwd !== null ? req.body.data.attributes.ts_pwd.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_pincode'] = (req.body.data.attributes.ts_pincode !== null ? req.body.data.attributes.ts_pincode.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_codregione'] = (req.body.data.attributes.ts_codregione !== null ? req.body.data.attributes.ts_codregione.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_codasl'] = (req.body.data.attributes.ts_codasl !== null ? req.body.data.attributes.ts_codasl.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_codssa'] = (req.body.data.attributes.ts_codssa !== null ? req.body.data.attributes.ts_codssa.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_struttura'] = (req.body.data.attributes.ts_struttura !== null ? req.body.data.attributes.ts_struttura : false);
        user_invoice_setting['ts_enable'] = (req.body.data.attributes.ts_enable !== null ? req.body.data.attributes.ts_enable : false);
        user_invoice_setting['ts_tipo_spesa'] = (req.body.data.attributes.ts_tipo_spesa !== null ? req.body.data.attributes.ts_tipo_spesa.replace(/'/g, "''''") : '');
        user_invoice_setting['ts_cod_fiscale'] = (req.body.data.attributes.ts_cod_fiscale !== null ? req.body.data.attributes.ts_cod_fiscale.replace(/'/g, "''''") : '');
        
        

        sequelize.query('SELECT insert_user_invoice_setting_json(\'' + JSON.stringify(user_invoice_setting) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_user_invoice_setting_json && datas[0].insert_user_invoice_setting_json != null) {
                    req.body.data.id = datas[0].insert_user_invoice_setting_json;

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'user_invoice_setting',
                        'insert',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        datas[0].insert_user_invoice_setting_json,
                        'Aggiunto user_invoice_setting ' + datas[0].insert_user_invoice_setting_json
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('user_invoice_setting_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        
            sequelize.query('SELECT update_user_invoice_setting_v1(' +
                    req.params.id +
                    ',' + (req.body.data.attributes.bank_name && utility.check_type_variable(req.body.data.attributes.bank_name, 'string') && req.body.data.attributes.bank_name != null ? '\'' + req.body.data.attributes.bank_name.replace(/'/g, "''''") + '\'' : null) +
                    ',' + (req.body.data.attributes.bank_iban && utility.check_type_variable(req.body.data.attributes.bank_iban, 'string') && req.body.data.attributes.bank_iban != null ? '\'' + req.body.data.attributes.bank_iban.replace(/'/g, "''''") + '\'' : null) +
                    ',' + (req.body.data.attributes.bank_bic && utility.check_type_variable(req.body.data.attributes.bank_bic, 'string') && req.body.data.attributes.bank_bic != null ? '\'' + req.body.data.attributes.bank_bic.replace(/'/g, "''''") + '\'' : null) +
                    ',' + (req.body.data.attributes.rea && utility.check_type_variable(req.body.data.attributes.rea, 'string') && req.body.data.attributes.rea != null ? '\'' + req.body.data.attributes.rea.replace(/'/g, "''''") + '\'' : null) +
                    ',' + (req.body.data.attributes.regime_fiscale && utility.check_type_variable(req.body.data.attributes.regime_fiscale, 'string') && req.body.data.attributes.regime_fiscale != null ? '\'' + req.body.data.attributes.regime_fiscale.replace(/'/g, "''''") + '\'' : null) +
                    ',' + req.user._tenant_id +
                    ',\'' + req.headers['host'].split(".")[0] + '\''+
                    ',' + (req.body.data.attributes.ts_username !== null ? '\'' + req.body.data.attributes.ts_username.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_pwd !== null ? '\'' + req.body.data.attributes.ts_pwd.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_pincode !== null ? '\'' + req.body.data.attributes.ts_pincode.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_codregione !== null ? '\'' + req.body.data.attributes.ts_codregione.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_codasl !== null ? '\'' + req.body.data.attributes.ts_codasl.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_codssa !== null ? '\'' + req.body.data.attributes.ts_codssa.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_struttura !== null ? req.body.data.attributes.ts_struttura : false)+
                    ',' + (req.body.data.attributes.ts_enable !== null ? req.body.data.attributes.ts_enable : false) +
                    ',' + (req.body.data.attributes.ts_tipo_spesa !== null ? '\'' + req.body.data.attributes.ts_tipo_spesa.replace(/'/g, "''''") +'\'': null)+
                    ',' + (req.body.data.attributes.ts_cod_fiscale !== null ? '\'' + req.body.data.attributes.ts_cod_fiscale.replace(/'/g, "''''") +'\'': null)+');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                   

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'user_invoice_setting',
                            'update',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            req.params.id,
                            'Modificato user_invoice_setting ' + req.params.id
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    }).catch(function(err) {
                        return res.status(400).render('update_user_invoice_setting_v1: ' + err);
                    });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_user_invoice_setting_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'user_invoice_settings',
                    'id': datas[0].delete_user_invoice_setting_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('user_invoice_setting_destroy: ' + err);
        });
};