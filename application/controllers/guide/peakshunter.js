var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    config = require('../../../config'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');
var util = require('util');

exports.scuolasci_events_calendar_ical = function(req, res) {
    var start_date = null,
        end_date = null,
        comprensorio = null,
        contact_id = null,
        product_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');


    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.query.to_date + '\'';
    }


    if (req.query.q !== undefined && req.query.q !== '') {

        comprensorio = '\'' + req.query.q + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {

        contact_id = req.query.contact_id;
    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {

        product_id = req.query.product_id;
    }





    sequelize.query('SELECT scuolasci_events_calendar_ical(' +
            req.user._tenant_id + ',' +
            comprensorio + ',' +
            start_date + ',' +
            end_date + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            contact_id + ',' +
            product_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].scuolasci_events_calendar_ical && datas[0].scuolasci_events_calendar_ical != null) {
                res.status(200).send({
                    'data': datas[0].scuolasci_events_calendar_ical
                });

            } else {
                res.status(200).send({
                    'data': []
                });
            }
        }).catch(function(err) {
            return res.status(400).render('scuolasci_events_calendar_ical: ' + err);
        });
};

exports.peakshunter_attivita = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [];

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }


         sequelize.query('SELECT peakshunter_attivita(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +','+
            (req.query.completed ? req.query.completed : null )+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].peakshunter_attivita) {
                res.json(datas[0].peakshunter_attivita[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter Attivita: ' + err);
        });
     });  
};

exports.peakshunter_scadenze = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [];

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }


         sequelize.query('SELECT peakshunter_scadenze(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +','+
            (req.query.completed ? req.query.completed : null )+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].peakshunter_scadenze) {
                res.json(datas[0].peakshunter_scadenze[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter Scadenze: ' + err);
        });
     });  
};

exports.copia_attivita_prodotto = function(req, res) {


    var project_id = null,
        product_id = null;

    if (req.body.project_id !== undefined && req.body.project_id != '') {
        
            project_id = req.body.project_id;
        
    }

     if (req.body.product_id !== undefined && req.body.product_id != '') {
        
            product_id = req.body.product_id;
        
    }


    sequelize.query('SELECT peakshunter_copia_attivita_prodotto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            project_id +','+
            product_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['peakshunter_copia_attivita_prodotto']);
            } else {
                res.json({});
            }

        });
};

exports.prenotazione_rifugi = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [],
        workhour_status = null,
        status_ = null;

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }

        if (req.query.status !== undefined && req.query.status !== '') {

            status_ = '\'' + req.query.status.replace(/'/g, "''''") + '\'';
        }

        if (req.query.workhour_status_id !== undefined && req.query.workhour_status_id !== '') {

            workhour_status = req.query.workhour_status_id;
        }


         sequelize.query('SELECT peakshunter_rifugi(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +','+
            (req.query.completed ? req.query.completed : null )+','+
            workhour_status+','+
            status_+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].peakshunter_rifugi) {
                res.json(datas[0].peakshunter_rifugi[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter Rifugi: ' + err);
        });
     });  
};

exports.peakshunter_timeline = function(req, res) {

    
         sequelize.query('SELECT peakshunter_timeline(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.params.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].peakshunter_timeline) {
                res.json(datas[0].peakshunter_timeline[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter TimeLine Projec: ' + err);
        });
};

exports.peakshunter_save_releaseform = function(req, res) {

   
         sequelize.query('SELECT peakshunter_save_releaseform(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].peakshunter_save_releaseform) {
                res.json(datas[0].peakshunter_save_releaseform[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter SAVE Release Form: ' + err);
        });
};

exports.peakshunter_booking_step = function(req, res) {

   console.log(req.body);
         sequelize.query('SELECT peakshunter_booking_step(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].peakshunter_booking_step) {
                res.json(datas[0].peakshunter_booking_step[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter Booking Step Form: ' + err);
        });
};

exports.peakshunter_update_timeline = function(req, res) {

    
         sequelize.query('SELECT peakshunter_update_timeline(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].peakshunter_update_timeline) {
                res.json(datas[0].peakshunter_update_timeline[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Peakshunter UPDATE TimeLine Projec: ' + err);
        });
};


exports.peakshunter_send_compila_dati = function(req, res) {
    var base58 = require('../../utility/base58.js');
    var mails = [];
    var emails = [];
    var https = require('https');
    var qs = require('querystring');
    var util = require('util');
    var type_contact =  'mail';
    var utils = require('../../../utils');
    var lrecipients = [];

     lrecipients.push(req.body.contact_id);
    

    var token = utils.uid(config.token.accessTokenLength);

    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
        moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
        req.body.contact_id + ',' +
        5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(tok) {
        /*  Token.create({
                token : token,
                expiration_date : config.token.calculateExpirationDate(),
                user_id : user.id,
                client_id : client.id,
                scope : scope
            }).then(function(tok) {*/
        var longUrl = 'https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/booking/#/guide_personal_data?contact_id='+req.body.contact_id+'&access_token=' + token +'&project_id='+req.body.project_id ;

       

        sequelize.query('SELECT insert_shortener(\'' +
                longUrl + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(shortner) {
                shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                 if(req.body.type == 'link'){
                    
                    res.status(200).send({'link':shortUrl});
                }
                else{
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        mandrill = require('mandrill-api/mandrill');

                    if (req.body.mail != '' && req.body.mail != undefined ) {
                         var address_string = '';
                        mails = req.body.mail.replace(' ', '').split(',');
                        mails.forEach(function(mail) {
                            if (re.test(mail) != true) {
                                return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
                            } else {
                                 address_string += ' ' + mail;
                                var email = {
                                    "email": mail,
                                    "name": null,
                                    "type": "to"
                                };
                                emails.push(email);
                            }

                        });

                    }
                     sequelize.query('SELECT search_organization_reply_to_new(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(reply_mail) {

                       var reply_to = 'no-reply@beebeemailer.com';
                        var domain_from_mail = 'no-reply@beebeemailer.com';

                        if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                            reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                            reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                            domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                            if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                reply_to = domain_from_mail;
                            }
                        }

                
                        var template_content = [{
                                "name": "PRODUCT",
                                "content": req.body.product_name
                            },{
                                "name": "ORGANIZATION",
                                "content": 'Peakshunter'
                            }, {
                                "name": "COLOR",
                                "content": '#1391ce'
                            }, {
                                "name": "LOGO",
                                "content": 'https://data.beebeeboard.com/41467/175/peackshunter_jpg'
                            }, {
                                "name": "NAME",
                                "content": uc_first(req.body.contact_name)
                            }, {
                                "name": "URL",
                                "content": shortUrl
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": 'Peaskhunter - Richiesta compilazione dati personali / Request for data compilation',
                                "from_email": domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                "from_name": 'Peakshunter',
                                "to": emails,
                                "headers": {
                                    "Reply-To":  reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                },
                                "tags": [ req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "peakshunter-richiesta-dati",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            

                               if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                    console.log("Mail sended correctly");


                                        sequelize.query('SELECT insert_mandrill_send(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                            req.user.id + ',\'' +
                                            result[0].status + '\',\'\',\'' + result[0]._id + '\',\'Richiesta Compilazione dati\',' +
                                            '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                 useMaster: true
                                            })
                                        .then(function(datas) {


                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                            //res.status(200).send({});

                                                 sequelize.query('SELECT peakshunter_update_send_mail(' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\','+
                                                    req.user.id+','+
                                                    req.user.role_id+','+
                                                    req.body.contact_id+','+
                                                    req.body.project_id+');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(oooo) {
                                                   if (oooo[0].peakshunter_update_send_mail) {
                                                        res.json(oooo[0].peakshunter_update_send_mail[0]);
                                                    } else
                                                        return res.status(422).send({});
                                                 }).catch(function(err) {
                                                    console.log(err);
                                                });


                                        }).catch(function(err) {
                                             console.log(err);
                                        });

                                } else {
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                    sequelize.query('SELECT insert_mandrill_send(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                            req.user.id + ',\'' +
                                            result[0].status + '\',\'' +
                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'Richiesta Compilazione dati\',' +
                                            '\'' + req.body.text.replace(/'/g, "''''") + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {


                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');

                                            //res.status(422).send(utility.error422('Reject', 'Reject Reason', result[0].reject_reason, '422'));

                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                }


                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                            console.log(e);
                        });
                }).catch(function(err) {
                     return res.status(400).render('search_organization_reply_to_new: ' + err);
                });
              }

            }).catch(function(err) {
                return res.status(400).render('tag_insert: ' + err);
            });

    }).catch(function(err) {
        return res.status(400).render('user_request_payment: ' + err);
    });
}


exports.peakshunter_wp = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [];

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }


         sequelize.query('SELECT peakshunter_wp_gite(null,\'' +
            req.headers['host'].split(".")[0] + '\',null,null,'+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +','+
            (req.query.completed ? req.query.completed : null )+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].peakshunter_wp_gite && datas[0].peakshunter_wp_gite[0].gite && datas[0].peakshunter_wp_gite[0].gite.length > 0) {
                res.json(datas[0].peakshunter_wp_gite[0].gite);
            } else
                return res.status(200).send([]);
        }).catch(function(err) {
            return res.status(400).render('Peakshunter Wp Gite: ' + err);
        });
     });  
};


function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};