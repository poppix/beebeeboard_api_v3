var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');
var util = require('util');



exports.prenotazioni_guide = function(req, res) {
    var start_date = null,
        end_date = null,
        q1 = [];

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            callback_(null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }
    }



    sequelize.query('SELECT guide_prenotazioni(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start_date + ','+
            end_date+','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guide_prenotazioni) {

                res.status(200).send(datas[0].guide_prenotazioni);
            } else {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_prenotazioni: ' + err);
        });
};

exports.dispo_guide = function(req, res) {
    var giorno_esatto = null,
        product_id = null,
        no_dispo = null;

    if (req.query.giorno !== undefined && req.query.giorno !== '') {
        if (!utility.check_type_variable(req.query.giorno, 'string')) {
            return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        giorno_esatto = '\'' + req.query.giorno + '\'';
    } else {
        return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {

        product_id = req.query.product_id;
    }

    if (req.query.no_dispo !== undefined && req.query.no_dispo !== '') {

        if (utility.check_id(req.query.no_dispo)) {
            callback_(null, utility.error422('no_dispo', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.no_dispo == 1) {
            no_dispo = true;
        } else {
            no_dispo = false;
        }
    } else {
        return res.status(422).send(utility.error422('no_dispo', 'Parametro non valido', 'Parametro non valido', '422'));
    }



    sequelize.query('SELECT guide_dispo(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            giorno_esatto + ',' +
            product_id + ',' +
            no_dispo + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guide_dispo) {

                res.status(200).send(datas[0].guide_dispo);
            } else {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_dispo: ' + err);
        });
};

exports.dispo_gruppi = function(req, res) {
    var giorno_esatto = null,
        product_id = null,
        guida_id = null;

    var start_date = null,
        end_date = null,
        visibili = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {

        product_id = req.query.product_id;
    }

    if (req.query.guida_id !== undefined && req.query.guida_id !== '') {

        guida_id = req.query.guida_id;
    }

    if(req.query.visibili !== undefined && req.query.visibili !== ''){
        visibili = '\''+req.query.visibili+'\'';
    }



    sequelize.query('SELECT guide_gruppi(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start_date + ',' +
            product_id + ','+
            guida_id + ','+
            req.user.id + ','+
            end_date + ','+
            visibili + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guide_gruppi) {

                res.status(200).send(datas[0].guide_gruppi);
            } else {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_gruppi: ' + err);
        });
};

exports.associa_guide = function(req, res) {
    var guida_id = null,
        event_id = null;


    if (req.body.guida_id !== undefined && req.body.guida_id !== '') {

        guida_id = req.body.guida_id;
    }

    
    if (req.body.event_id !== undefined && req.body.event_id !== '') {

        event_id = req.body.event_id;
    }else {
        return res.status(422).send(utility.error422('event_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT guide_associa_gruppo(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            guida_id + ',' +
            event_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_associa_gruppo != 1) {
                return res.status(200).send({ 'data': { 'id': datas[0].guide_associa_gruppo } });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_associa_gruppo: ' + err);
        });
};

exports.associa_gruppi = function(req, res) {
    var starts = [],
        ends = [],
        product_id = null,
        cliente_id = null,
        event_id = null,
        project_id = null;


    if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.cliente_id !== undefined && req.body.cliente_id !== '') {

        cliente_id = req.body.cliente_id;
    } else {
        return res.status(422).send(utility.error422('cliente_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.event_id !== undefined && req.body.event_id !== '') {

        event_id = req.body.event_id;
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {

        product_id = req.body.product_id;
    } else {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.project_id !== undefined && req.body.project_id !== '') {

        project_id = req.body.project_id;
    } else {
        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT guide_associa_preno_v1(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
            (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',' +
            product_id + ',' +
            cliente_id + ',' +
            event_id + ','+
            project_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_associa_preno_v1 != 1) {
                return res.status(200).send({ 'data': { 'id': datas[0].guide_associa_preno_v1 } });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_associa_preno_v1: ' + err);
        });
};

exports.disassocia_gruppi = function(req, res) {
    var event_id = null,
        cliente_id = null;

    if (req.body.cliente_id !== undefined && req.query.cliente_id !== '') {

        cliente_id = req.body.cliente_id;
    } else {
        return res.status(422).send(utility.error422('cliente_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.event_id !== undefined && req.body.event_id !== '') {

        event_id = req.body.event_id;
    } else {
        return res.status(422).send(utility.error422('event_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT guide_disassocia_gruppi_v1(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            event_id + ',' +
            cliente_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_disassocia_gruppi_v1 != 1) {
                return res.status(200).send({ 'data': { 'id': datas[0].guide_disassocia_gruppi_v1 } });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_disassocia_gruppi: ' + err);
        });
};

exports.disassocia_guide = function(req, res) {
    var event_id = null,
        cliente_id = null,
        guida_id = null;

    if (req.body.guida_id !== undefined && req.query.guida_id !== '') {

        guida_id = req.body.guida_id;
    } else {
        return res.status(422).send(utility.error422('guida_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.cliente_id !== undefined && req.query.cliente_id !== '') {

        cliente_id = req.body.cliente_id;
    } else {
        return res.status(422).send(utility.error422('cliente_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.event_id !== undefined && req.body.event_id !== '') {

        event_id = req.body.event_id;
    } else {
        return res.status(422).send(utility.error422('event_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT guide_disassocia_preno(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            guida_id + ',' +
            event_id + ',' +
            cliente_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_disassocia_preno != 1) {
                return res.status(200).send({ 'data': { 'id': datas[0].guide_disassocia_preno } });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_disassocia_preno: ' + err);
        });
};

exports.guide_events_calendar = function(req, res) {
    var start_date = null,
        end_date = null,
        product_id = null,
        contact_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');


    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.query.to_date + '\'';
    }



    if (req.query.product_id !== undefined && req.query.product_id !== '') {

        product_id = req.query.product_id;
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {

        contact_id = req.query.contact_id;
    }


    sequelize.query('SELECT guide_events_calendar(' +
            req.user._tenant_id + ',' +

            start_date + ',' +
            end_date + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            contact_id + ',' +
            product_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].guide_events_calendar && datas[0].guide_events_calendar != null) {
                res.status(200).send({
                    'data': datas[0].guide_events_calendar
                });

            } else {
                res.status(200).send({
                    'data': []
                });
            }
        }).catch(function(err) {
            return res.status(400).render('guide_events_calendar: ' + err);
        });
};

exports.guide_resoconto = function(req, res) {
    var start_date = null,
        end_date = null,
        guida_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');


    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.query.to_date + '\'';
    }


    if (req.query.guida_id !== undefined && req.query.guida_id !== '') {

        guida_id = req.query.guida_id;
    }


    sequelize.query('SELECT guide_resoconto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start_date + ',' +
            end_date + ',' +
            guida_id + ',' +
            req.user.role_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].guide_resoconto && datas[0].guide_resoconto != null) {
                res.status(200).send({
                    'data': datas[0].guide_resoconto
                });

            } else {
                res.status(200).send({
                    'data': []
                });
            }
        }).catch(function(err) {
            return res.status(400).render('guide_resoconto: ' + err);
        });
};

exports.guide_fatture = function(req, res) {
    var start_date = null,
        end_date = null,
        guida_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.body.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.body.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.body.to_date + '\'';
    }

    if (req.body.guida_id !== undefined && req.body.guida_id !== '') {

        guida_id = req.body.guida_id;
    }


    sequelize.query('SELECT guide_genera_proforma(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start_date + ',' +
            end_date + ',' +
            guida_id + ',' +
            req.user.role_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_genera_proforma && datas[0].guide_genera_proforma != null) {
                res.status(200).send({
                    'data': datas[0].guide_genera_proforma
                });

            } else {
                res.status(200).send({
                    'data': []
                });
            }
        }).catch(function(err) {
            return res.status(400).render('guide_genera_proforma: ' + err);
        });
};

exports.guide_notifica_assegna_gita = function(req, res) {
    var 
        event_id = null,
        guida_id = null;


    if (req.query.guida_id !== undefined && req.query.guida_id !== '') {

        guida_id = req.query.guida_id;
    } else {
        return res.status(422).send(utility.error422('guida_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.event_id !== undefined && req.query.event_id !== '') {

        event_id = req.query.event_id;
    }

    sequelize.query('SELECT guide_notifica_assegna_gita(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            event_id + ','+
            guida_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_notifica_assegna_gita != 1) {
                return res.status(200).send(datas[0].guide_notifica_assegna_gita);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_notifica_assegna_gita: ' + err);
        });
};

exports.guide_assicurazione = function(req, res) {
    var from_date = null,
        to_date = null;
    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT guide_assicurazione(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['guide_assicurazione']);
            } else {
                res.json({});
            }

        });
};

exports.dispo_guida_per_gruppo = function(req, res) {
    var event_id = null,
        guida_id = null;

    if (req.body.event_id !== undefined && req.body.event_id !== '') {

        event_id = req.body.event_id;
    }

    if (req.body.guida_id !== undefined && req.body.guida_id !== '') {

        guida_id = req.body.guida_id;
    }
    else
    {
        guida_id = req.user.id;
    }




    sequelize.query('SELECT guide_guida_dispo_per_gruppo(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            guida_id + ',' +
            event_id + ','+
            req.user.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guide_guida_dispo_per_gruppo) {

                res.status(200).send(datas[0].guide_guida_dispo_per_gruppo);
            } else {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_guida_dispo_per_gruppo: ' + err);
        });
};

exports.togli_dispo_guida_per_gruppo = function(req, res) {
    var event_id = null,
        guida_id = null;

    if (req.body.event_id !== undefined && req.body.event_id !== '') {

        event_id = req.body.event_id;
    }

    if (req.body.guida_id !== undefined && req.body.guida_id !== '') {

        guida_id = req.body.guida_id;
    }
    else
    {
        guida_id = req.user.id;
    }




    sequelize.query('SELECT guide_guida_togli_dispo_per_gruppo(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            guida_id + ',' +
            event_id + ','+
            req.user.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guide_guida_togli_dispo_per_gruppo) {

                res.status(200).send(datas[0].guide_guida_togli_dispo_per_gruppo);
            } else {
                res.status(200).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_guida_togli_dispo_per_gruppo: ' + err);
        });
};

exports.guide_da_turno = function(req, res) {
      
    sequelize.query('SELECT guide_assegna_da_turno(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            (req.body.event_id ? req.body.event_id : null)+','+
            (req.body.lungo ? req.body.lungo : true)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
                console.log(datas);
                return res.status(200).send(datas[0].guide_assegna_da_turno);
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_assegna_da_turno: ' + err);
        });
};

exports.guide_last_turno = function(req, res) {
      
    sequelize.query('SELECT guide_last_turno(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
           
                return res.status(200).send(datas[0].guide_last_turno);
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_last_turno: ' + err);
        });
};

exports.send_dispo_notification = function(req, res) {
    var https = require('https');
    
    var url = 'https://asset.beebeeboard.com/'+req.headers['host'].split(".")[0]+'/utility/notifica_guide.php?organization='+req.headers['host'].split(".")[0]+'&token='+req.query.access_token+'&$_t='+moment().format('HHmmss');

    var request = https.get(url, function(result) {

        result.on('data', function(chunk) {
           
        });

        result.on('end', function() {
            
            res.status(200).send({});
        });
    }).on('error', function(err) {

        return false;
    });

    request.end();
};

exports.guide_disdetta_preno = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    
    var project_id = null;
    
    if (req.body.project_id !== undefined && req.body.project_id !== '') {

        project_id = req.body.project_id;
    }else {
        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT guide_disdetta_preno(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            project_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].guide_disdetta_preno) {
                return res.status(200).send({ 'data': { 'id': datas[0].guide_disdetta_preno } });
            } 
        }).catch(function(err) {
            console.log(err);
            return res.status(400).render('guide_disdetta_preno: ' + err);
        });
};
