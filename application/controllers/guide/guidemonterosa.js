var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
     config = require('../../../config'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');
var util = require('util');


exports.guidemonterosa_get_prenotazione = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.project_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT guidemonterosa_get_prenotazione_data(' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',' +
                    req.params.id + ',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {

                    if (datas[0].guidemonterosa_get_prenotazione_data && datas[0].guidemonterosa_get_prenotazione_data != null) {
                        callback(null, datas[0].guidemonterosa_get_prenotazione_data[0]);

                    } else {

                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });

        },
        included: function(callback) {
            sequelize.query('SELECT guidemonterosa_get_prenotazione_included(' +
                    req.params.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.id + ',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(included) {

                    if (included[0].guidemonterosa_get_prenotazione_included && included[0].guidemonterosa_get_prenotazione_included != null) {
                        callback(null, included[0].guidemonterosa_get_prenotazione_included);

                    } else {

                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('project_find: ' + err);
        }
        if (results.data.length == 0 || !results.included) {
            res.status(404).render(utility.error404('id', 'Nessun progetto trovato', 'Nessun progetto trovato', '404'), null);
        } else {
            var response = {
                'data': utility.check_find_datas(results.data, 'projects', ['payments', 'contacts', 'products', 'custom_fields', 'events', 'workhours']),
                'included': utility.check_find_included(results.included, 'projects', ['products', 'contacts', 'payments', 'custom_fields', 'organization_custom_fields', 'addresses',
                    'project_states', 'contact_states', 'events', 'related_projects', 'workhours', 'invoices', 'expenses', 'addresses'
                ])
            };
            res.status(200).send(response);
        }


    });
};

exports.guidemonterosa_products_voucher = function(req, res) {

   
    if (utility.check_id(req.query.voucher_id)) {
        return res.status(422).send(utility.error422('voucher_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

  
        sequelize.query('SELECT guidemonterosa_products_voucher(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.query.voucher_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if (datas[0].guidemonterosa_products_voucher && datas[0].guidemonterosa_products_voucher != null) {
                    
                        res.status(200).send(datas[0].guidemonterosa_products_voucher[0]);

                } else {
                      res.status(404).send({});
                   
                }
            }).catch(function(err) {
                return res.status(400).render('Guidemonterosa Products Voucher:  ' + err);
            });


};

exports.guidemonterosa_dashboard = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        complete = null,
        paied = null,
        in_invoice = null,
        in_expense = null,
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [],
        servizi = false;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.fornitori !== undefined && req.query.fornitori != '') {
            if (util.isArray(req.query.fornitori)) {
                req.query.fornitori.forEach(function(cont) {
                    fornitore_ids.push(cont );
                });
            }
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

      /*  if (req.query.fornitori !== undefined && req.query.fornitori !== '') {

            complete = true;
        }*/

        if (req.query.paied !== undefined && req.query.paied !== '') {

            paied = req.query.paied.replace(/'/g, "''''");
        }

        if (req.query.in_invoice !== undefined && req.query.in_invoice !== '') {

            in_invoice = req.query.in_invoice.replace(/'/g, "''''");
        }

        if (req.query.in_expense !== undefined && req.query.in_expense !== '') {

            in_expense = req.query.in_expense.replace(/'/g, "''''");
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }

        if (req.query.servizi !== undefined && req.query.servizi !== '') {

            servizi = req.query.servizi;
        }

         sequelize.query('SELECT guidemonterosa_dashboard_v2(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            complete+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            paied+','+
            in_invoice+','+
            in_expense+','+
            servizi+',' +
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].guidemonterosa_dashboard_v2) {
                res.json(datas[0].guidemonterosa_dashboard_v2);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Guidemonterosa Dashboard: ' + err);
        });
     });  
};

exports.guidemonterosa_dashboard_preventivi = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        complete = null,
        paied = null,
        in_invoice = null,
        in_expense = null,
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [],
        servizi = null;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.fornitori !== undefined && req.query.fornitori != '') {
            if (util.isArray(req.query.fornitori)) {
                req.query.fornitori.forEach(function(cont) {
                    fornitore_ids.push(cont );
                });
            }
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

      /*  if (req.query.fornitori !== undefined && req.query.fornitori !== '') {

            complete = true;
        }*/

        if (req.query.paied !== undefined && req.query.paied !== '') {

            paied = req.query.paied.replace(/'/g, "''''");
        }

        if (req.query.in_invoice !== undefined && req.query.in_invoice !== '') {

            in_invoice = req.query.in_invoice.replace(/'/g, "''''");
        }

        if (req.query.in_expense !== undefined && req.query.in_expense !== '') {

            in_expense = req.query.in_expense.replace(/'/g, "''''");
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }

        if (req.query.servizi !== undefined && req.query.servizi !== '') {

            servizi = req.query.servizi;
        }

         sequelize.query('SELECT guidemonterosa_dashboard_preventivi(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            complete+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            paied+','+
            in_invoice+','+
            in_expense+','+
            servizi+',' +
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].guidemonterosa_dashboard_preventivi) {
                res.json(datas[0].guidemonterosa_dashboard_preventivi);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Guidemonterosa Dashboard Preventivi:  ' + err);
        });
     });  
};

exports.guidemonterosa_dashboard_voucher = function(req, res) {

    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        complete = null,
        paied = null,
        in_invoice = null,
        in_expense = null,
        cliente_ids = [], 
        agenzie_ids = [],
        guide_ids = [],
        q = [],
        servizi = null;

    utility.get_parameters(req, 'payment', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.fornitori !== undefined && req.query.fornitori != '') {
            if (util.isArray(req.query.fornitori)) {
                req.query.fornitori.forEach(function(cont) {
                    fornitore_ids.push(cont );
                });
            }
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

      /*  if (req.query.fornitori !== undefined && req.query.fornitori !== '') {

            complete = true;
        }*/

        if (req.query.paied !== undefined && req.query.paied !== '') {

            paied = req.query.paied.replace(/'/g, "''''");
        }

        if (req.query.in_invoice !== undefined && req.query.in_invoice !== '') {

            in_invoice = req.query.in_invoice.replace(/'/g, "''''");
        }

        if (req.query.da_utilizzare !== undefined && req.query.da_utilizzare !== '') {

            in_expense = req.query.da_utilizzare.replace(/'/g, "''''");
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }

        if (req.query.scaduti !== undefined && req.query.scaduti !== '') {

            servizi = req.query.scaduti;
        }

         sequelize.query('SELECT guidemonterosa_dashboard_voucher(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            complete+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            paied+','+
            in_invoice+','+
            in_expense+','+
            servizi+',' +
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].guidemonterosa_dashboard_voucher) {
                res.json(datas[0].guidemonterosa_dashboard_voucher[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('Guidemonterosa Dashboard Voucher:  ' + err);
        });
     });  
};

exports.guidemonterosa_economic_situation = function(req, res) {

    var from_date = null,
        to_date = null,
        contact_id = null;

    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        contact_id = req.query.contact_id;
    }

    sequelize.query('SELECT guidemonterosa_debiti_crediti_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',\'asc\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            contact_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['guidemonterosa_debiti_crediti_v1']);
            } else {
                res.json({});
            }

        });
};

exports.guidemonterosa_crea_fattura = function(req, res) {

    var util = require('util');
    var project_ids = [],
        payment = null;

    if (req.query.project_id !== undefined && req.query.project_id != '') {
        if (util.isArray(req.query.project_id)) {
            project_ids = req.query.project_id.map(Number);
        } else {
            project_ids.push(req.query.project_id);
        }
    }

    if (req.query.payment !== undefined && req.query.payment != '') {
        payment = req.query.payment;
    }

    sequelize.query('SELECT guidemonterosa_crea_fattura(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            (project_ids.length > 0 ? 'ARRAY[' + project_ids + ']::bigint[]' : null) +','+
            payment+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
            useMaster:true
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['guidemonterosa_crea_fattura']['data']);
            } else {
                res.json({});
            }

        });
};

exports.guidemonterosa_genera_fattura = function(req, res) {

    var util = require('util');
    var project_ids = [],
        payment = null,
        servizi = null;

    if (req.query.project_id !== undefined && req.query.project_id != '') {
        if (util.isArray(req.query.project_id)) {
            project_ids = req.query.project_id.map(Number);
        } else {
            project_ids.push(req.query.project_id);
        }
    }

    if (req.query.payment !== undefined && req.query.payment != '') {
        payment = req.query.payment;
    }

    if (req.query.servizi !== undefined && req.query.servizi != '') {
        servizi = req.query.servizi;
    }

    sequelize.query('SELECT guidemonterosa_genera_fattura(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            (project_ids.length > 0 ? 'ARRAY[' + project_ids + ']::bigint[]' : null) +','+
            payment+','+
            servizi+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
            useMaster:true
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['guidemonterosa_genera_fattura']['data']);
            } else {
                res.json({});
            }

        });
};

exports.guidemonterosa_dashboard_export = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    var comprensorio = null,
        start = null,
        end = null,
        fornitore_ids = [], //fornitore
        product_ids = [],
        complete = null,
        paied = null,
        in_invoice = null,
        in_expense = null,
        cliente_ids = [],
        agenzie_ids = [],
        guide_ids = [],
        q = [],
        servizi = false;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.fornitori !== undefined && req.query.fornitori != '') {
            if (util.isArray(req.query.fornitori)) {
                req.query.fornitori.forEach(function(cont) {
                    fornitore_ids.push(cont );
                });
            }
        }

        if (req.query.clienti !== undefined && req.query.clienti != '') {
            if (util.isArray(req.query.clienti)) {
                req.query.clienti.forEach(function(cont) {
                    cliente_ids.push( cont );
                });
            }
        }

        if (req.query.guide !== undefined && req.query.guide != '') {
            if (util.isArray(req.query.guide)) {
                req.query.guide.forEach(function(cont) {
                    guide_ids.push(cont );
                });
            }
        }

        if (req.query.agenzie !== undefined && req.query.agenzie != '') {
            if (util.isArray(req.query.agenzie)) {
                req.query.agenzie.forEach(function(cont) {
                    agenzie_ids.push( cont );
                });
            }
        }

        /*if (req.query.fornitore !== undefined && req.query.fornitore !== '') {

            complete = req.query.fornitore.replace(/'/g, "''''");
        }*/

        if (req.query.paied !== undefined && req.query.paied !== '') {

            paied = req.query.paied.replace(/'/g, "''''");
        }

        if (req.query.in_invoice !== undefined && req.query.in_invoice !== '') {

            in_invoice = req.query.in_invoice.replace(/'/g, "''''");
        }

        if (req.query.in_expense !== undefined && req.query.in_expense !== '') {

            in_expense = req.query.in_expense.replace(/'/g, "''''");
        }

        if (req.query.comp !== undefined && req.query.comp !== '' && req.query.comp !== 'tutti') {

            comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
        }

        if (req.query.servizi !== undefined && req.query.servizi !== '') {

            servizi = req.query.servizi;
        }

         sequelize.query('SELECT guidemonterosa_dashboard_export_v2(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            comprensorio+','+
            complete+','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            (cliente_ids.length > 0 ? 'ARRAY[' + cliente_ids + ']::bigint[]' : null) + ',' +
            (fornitore_ids.length > 0 ? 'ARRAY[' + fornitore_ids + ']::bigint[]' : null) +',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ','+
            parameters.sortDirection + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            paied+','+
            in_invoice+','+
            in_expense+','+
            servizi+',' +
            (guide_ids.length > 0 ? 'ARRAY[' + guide_ids + ']::bigint[]' : null) + ',' +
            (agenzie_ids.length > 0 ? 'ARRAY[' + agenzie_ids + ']::bigint[]' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            var fields_array = datas[0].guidemonterosa_dashboard_export_v2[0]['fields'];
            var fieldNames_array = datas[0].guidemonterosa_dashboard_export_v2[0]['field_names'];
            
            json2csv({
                data: datas[0].guidemonterosa_dashboard_export_v2,
                fields: fields_array,
                fieldNames: fieldNames_array
            }, function(err, csv) {
                if (err) {
                    console.log(err);
                    return res.status(500).send(err);
                }
                res.writeHead(200, {
                    'Content-Type': 'text/csv',
                    'Content-Disposition': 'attachment; filename=dashboard.csv'
                });
                res.end(csv);
            });
        }).catch(function(err) {
            return res.status(400).render('Guidemonterosa Dashboard Export: ' + err);
        });
     });  
};

exports.guidemonterosa_lettura_volo = function(req, res) {
    
    if (!req.user.role[0].json_build_object.attributes.project_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (!req.query.project_id || req.query.project_id === undefined) {
        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    var str_decode = Buffer.from(req.query.project_id, 'base64').toString(); 
    
    var control_bit = str_decode.split('!')[1];
    var str = str_decode.split('!')[0].split('?')[1];
    
    var project_id = parseInt(str.split('-')[1]);
    var contact_id = parseInt(str.split('-')[0]);
    var str_length = project_id.toString().length;
    
    if(parseInt(control_bit) != parseInt(str_length))
    {
        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }else
    {
    
        sequelize.query('SELECT guidemonterosa_lettura_volo_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            project_id +',\''+
            req.query.project_id+'\','+
            contact_id +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
            useMaster:true
            })
        .then(function(datas) {
            if (datas[0].guidemonterosa_lettura_volo_v1 && datas[0].guidemonterosa_lettura_volo_v1 != null && datas[0].guidemonterosa_lettura_volo_v1) {
                res.status(200).send(datas[0].guidemonterosa_lettura_volo_v1);

            } else {
                res.status(404).send('{}');
            }
        }).catch(function(err) {
            return res.status(400).render('guidemonterosa_lettura_volo ' + err);
        });
    }       
};

exports.guidemonterosa_elenco_letture_volo = function(req, res) {
    
    sequelize.query('SELECT guidemonterosa_elenco_letture_volo_v1(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {
        if (datas[0].guidemonterosa_elenco_letture_volo_v1 && datas[0].guidemonterosa_elenco_letture_volo_v1 != null && datas[0].guidemonterosa_elenco_letture_volo) {
            res.status(200).send(datas[0].guidemonterosa_elenco_letture_volo_v1);

        } else {
            res.status(404).send('{}');
        }
    }).catch(function(err) {
        return res.status(400).render('guidemonterosa_elenco_letture_volo ' + err);
    });     
};

exports.guidemonterosa_prenota_volo = function(req, res) {

   
    var contact_id = null,
        product_id = null,
        from_date = null,
        to_date = null,
        localita = null;

    if (!req.body.contact_id || req.body.contact_id === undefined) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        contact_id = req.body.contact_id;
    }

    if (!req.body.product_id || req.body.product_id === undefined) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        product_id = req.body.product_id;
    }

    if (req.body.start_date !== undefined && req.body.start_date !== '') {
        if (!moment(req.body.start_date).isValid()) {
            return res.status(422).send(utility.error422('start_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.body.start_date + '\'';
    }else
    {
        return res.status(422).send(utility.error422('start_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.end_date !== undefined && req.body.end_date !== '') {
        if (!moment(req.body.end_date).isValid()) {
            return res.status(422).send(utility.error422('end_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.body.end_date + '\'';
    }else
    {
        return res.status(422).send(utility.error422('end_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (!req.body.localita || req.body.localita === undefined ) {
       
            return res.status(422).send(utility.error422('localita', 'Parametro non valido', 'Parametro non valido', '422'));
    }else{
        localita = '\'' + req.body.localita + '\'';
    
    }

    sequelize.query('SELECT guidemonterosa_prenota_volo_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            contact_id+','+
            product_id+','+
            from_date+','+
            to_date+','+
            localita+','+
            (req.body.event_id ? req.body.event_id : null )+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guidemonterosa_prenota_volo_v1 && datas[0].guidemonterosa_prenota_volo_v1 != -1) {
                res.status(200).send({'id':datas[0].guidemonterosa_prenota_volo_v1});
            } else {
                return res.status(422).send(utility.error422('No Disponibile', 'Volo non disponibile', 'Parametro non valido', '422'));
            }

     }).catch(function(err) {
        return res.status(400).render('guidemonterosa_prenota_volo ' + err);
    });     
};

exports.guidemonterosa_aggiungi_volo_a_pratica = function(req, res) {

   
    var workhour_id = null,
        product_id = null;

    if (req.body.workhour_id && req.body.workhour_id !== undefined) {
        workhour_id = req.body.workhour_id;
    }

    if (!req.body.product_id || req.body.product_id === undefined) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        product_id = req.body.product_id;
    }


    sequelize.query('SELECT guidemonterosa_aggiungi_volo_a_pratica(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            workhour_id+','+
            product_id+','+
            (req.body.project_id ? req.body.project_id : null )+','+
            (req.body.event_id ? req.body.event_id : null )+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guidemonterosa_aggiungi_volo_a_pratica && datas[0].guidemonterosa_aggiungi_volo_a_pratica != -1) {
                res.status(200).send({'id':datas[0].guidemonterosa_aggiungi_volo_a_pratica});
            } else {
                return res.status(422).send(utility.error422('No Disponibile', 'Volo non disponibile', 'Parametro non valido', '422'));
            }

     }).catch(function(err) {
        return res.status(400).render('guidemonterosa_aggiungi_volo_a_pratica ' + err);
    });     
};

exports.guidemonterosa_report_servizi = function(req, res) {
   
    var contact_id = null,
        from_date = null,
        to_date = null,
        type = null,
        stampa = null;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.type !== undefined && req.query.type !== '') {

            type = '\''+req.query.type.replace(/'/g, "''''")+'\'';
        }
       
        sequelize.query('SELECT guidemonterosa_report_servizi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].guidemonterosa_report_servizi){
                res.status(200).send(datas[0].guidemonterosa_report_servizi);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('guidemonterosa_report_servizi: ' + err);
        });
 
     });  
};

exports.guidemonterosa_report_pacchetti = function(req, res) {
   
    var contact_id = null,
        from_date = null,
        to_date = null,
        type = null,
        stampa = null;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.type !== undefined && req.query.type !== '') {

            type = '\''+req.query.type.replace(/'/g, "''''")+'\'';
        }
       
        sequelize.query('SELECT guidemonterosa_report_pacchetti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null)  + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].guidemonterosa_report_pacchetti){
                res.status(200).send(datas[0].guidemonterosa_report_pacchetti);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('guidemonterosa_report_pacchetti: ' + err);
        });
 
     });  
};

exports.guidemonterosa_crea_preventivo = function(req, res) {


    sequelize.query('SELECT guidemonterosa_crea_preventivo(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") +'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guidemonterosa_crea_preventivo && datas[0].guidemonterosa_crea_preventivo != -1) {
                res.status(200).send({'id':datas[0].guidemonterosa_crea_preventivo});
            } else {
                return res.status(422).send(utility.error422('No Disponibile', 'Volo non disponibile', 'Parametro non valido', '422'));
            }

     }).catch(function(err) {
        return res.status(400).render('guidemonterosa_crea_preventivo ' + err);
    });     
};

exports.guidemonterosa_crea_pratica_preventivo = function(req, res) {
    var project_ids = [];


    if (req.body.project_ids !== undefined && req.body.project_ids != '') {
        if (util.isArray(req.body.project_ids)) {
                req.body.project_ids.forEach(function(cont) {
                    project_ids.push( cont );
                });
            }
    }

    sequelize.query('SELECT guidemonterosa_crea_pratica_preventivo_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            (project_ids.length > 0 ? 'ARRAY[' + project_ids + ']::bigint[]' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guidemonterosa_crea_pratica_preventivo_new && datas[0].guidemonterosa_crea_pratica_preventivo_new != -1) {
                res.status(200).send({'id':datas[0].guidemonterosa_crea_pratica_preventivo_new});
            } else {
                return res.status(422).send(utility.error422('No Disponibile', 'Volo non disponibile', 'Parametro non valido', '422'));
            }

     }).catch(function(err) {
        return res.status(400).render('guidemonterosa_crea_pratica_preventivo_new ' + err);
    });     
};

exports.guidemonterosa_aggiungi_preventivi_pratica = function(req, res) {
    var project_ids = [];


    if (req.body.project_ids !== undefined && req.body.project_ids != '') {
        if (util.isArray(req.body.project_ids)) {
                req.body.project_ids.forEach(function(cont) {
                    project_ids.push( cont );
                });
            }
    }

    sequelize.query('SELECT guidemonterosa_aggiungi_preventivi_a_pratica(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            (project_ids.length > 0 ? 'ARRAY[' + project_ids + ']::bigint[]' : null) +','+
            req.body.pratica_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guidemonterosa_aggiungi_preventivi_a_pratica && datas[0].guidemonterosa_aggiungi_preventivi_a_pratica != -1) {
                res.status(200).send({'id':datas[0].guidemonterosa_aggiungi_preventivi_a_pratica});
            } else {
                return res.status(422).send(utility.error422('No Disponibile', 'Pratica non disponibile', 'Parametro non valido', '422'));
            }

     }).catch(function(err) {
        return res.status(400).render('guidemonterosa_aggiungi_preventivi_a_pratica ' + err);
    });     
};

exports.guidemonterosa_stato_preventivo = function(req, res) {


    sequelize.query('SELECT guidemonterosa_stato_preventivo(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.body.project_id +','+
            req.body.status_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].guidemonterosa_stato_preventivo && datas[0].guidemonterosa_stato_preventivo != -1) {
                res.status(200).send({'id':datas[0].guidemonterosa_stato_preventivo});
            } else {
                return res.status(422).send(utility.error422('No Disponibile', 'Volo non disponibile', 'Parametro non valido', '422'));
            }

     }).catch(function(err) {
        return res.status(400).render('guidemonterosa_stato_preventivo ' + err);
    });     
};

exports.segreteria_use_voucher = function(req, res){
   
    sequelize.query('SELECT use_voucher_clinic(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        req.query.voucher_id + ', ' +
        req.query.project_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['use_voucher_clinic']) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.status(200).send({});
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}


exports.guidemonterosa_create_session = function(req,res){
    console.log(req.body);
      sequelize.query('SELECT guidemonterosa_create_session(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        req.user.role_id + ',' +
        req.body.project_id +');', { 
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['guidemonterosa_create_session'] && datas[0]['guidemonterosa_create_session'] != -1) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.status(200).send({'id': datas[0]['guidemonterosa_create_session']});
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}


exports.segreteria_update_voucher = function(req, res){
   
    sequelize.query('SELECT segreteria_update_voucher(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        req.body.voucher_id + ', \'' +
        JSON.stringify(req.body).replace(/'/g, "''''") +'\');', { 
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['segreteria_update_voucher']) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.status(200).send({});
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}


exports.guidemonterosa_send_compila_dati = function(req, res) {
    var base58 = require('../../utility/base58.js');
    var mails = [];
    var emails = [];
    var https = require('https');
    var qs = require('querystring');
    var util = require('util');
    var type_contact =  'mail';
    var utils = require('../../../utils');
    var lrecipients = [];

     lrecipients.push(req.body.contact_id);
    

    var token = utils.uid(config.token.accessTokenLength);

    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
        moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
        req.body.contact_id + ',' +
        5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(tok) {
        /*  Token.create({
                token : token,
                expiration_date : config.token.calculateExpirationDate(),
                user_id : user.id,
                client_id : client.id,
                scope : scope
            }).then(function(tok) {*/
        var longUrl = 'https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/booking/#/heliguides_personal_data?contact_id='+req.body.contact_id+'&access_token=' + token +'&project_id='+req.body.project_id ;

       

        sequelize.query('SELECT insert_shortener(\'' +
                longUrl + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(shortner) {
                shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                if(req.body.type == 'link'){

                    res.status(200).send({'link':shortUrl});
                }
                else{
                        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                            mandrill = require('mandrill-api/mandrill');

                        if (req.body.mail != '' && req.body.mail != undefined ) {
                             var address_string = '';
                            mails = req.body.mail.replace(' ', '').split(',');
                            mails.forEach(function(mail) {
                                if (re.test(mail) != true) {
                                    return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
                                } else {
                                     address_string += ' ' + mail;
                                    var email = {
                                        "email": mail,
                                        "name": null,
                                        "type": "to"
                                    };
                                    emails.push(email);
                                }

                            });

                        }
                         sequelize.query('SELECT search_organization_reply_to_new(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(reply_mail) {

                           var reply_to = 'no-reply@beebeemailer.com';
                            var domain_from_mail = 'no-reply@beebeemailer.com';

                            if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                                reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                                reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                    reply_to = domain_from_mail;
                                }
                            }

                    
                            var template_content = [{
                                    "name": "PRODUCT",
                                    "content": req.body.product_name
                                },{
                                    "name": "ORGANIZATION",
                                    "content": 'Heli Guides S.r.l.'
                                }, {
                                    "name": "COLOR",
                                    "content": '#F8C208'
                                }, {
                                    "name": "LOGO",
                                    "content": 'https://data.beebeeboard.com/guidemonterosa/44975/178/HG_FB_Profil_Bild_n.jpg'
                                }, {
                                    "name": "NAME",
                                    "content": uc_first(req.body.contact_name)
                                }, {
                                    "name": "URL",
                                    "content": shortUrl
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": req.headers['host'].split(".")[0]
                                    },
                                    "subject": 'Heli Guides S.r.l. - Richiesta compilazione dati personali / Request for data compilation',
                                    "from_email": domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                    "from_name": 'Heli Guides S.r.l.',
                                    "to": emails,
                                    "headers": {
                                        "Reply-To":  reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                    },
                                    "tags": [ req.headers['host'].split(".")[0]],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "guidemonterosa-richiesta-dati",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {

                                

                                   if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                        console.log("Mail sended correctly");


                                            sequelize.query('SELECT insert_mandrill_send(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                                req.user.id + ',\'' +
                                                result[0].status + '\',\'\',\'' + result[0]._id + '\',\'Richiesta Compilazione dati\',' +
                                                '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                     useMaster: true
                                                })
                                            .then(function(datas) {


                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                                //res.status(200).send({});

                                                     sequelize.query('SELECT peakshunter_update_send_mail(' +
                                                        req.user._tenant_id + ',\'' +
                                                        req.headers['host'].split(".")[0] + '\','+
                                                        req.user.id+','+
                                                        req.user.role_id+','+
                                                        req.body.contact_id+','+
                                                        req.body.project_id+');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(oooo) {
                                                       if (oooo[0].peakshunter_update_send_mail) {
                                                            res.json(oooo[0].peakshunter_update_send_mail[0]);
                                                        } else
                                                            return res.status(422).send({});
                                                     }).catch(function(err) {
                                                        console.log(err);
                                                    });


                                            }).catch(function(err) {
                                                 console.log(err);
                                            });

                                    } else {
                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                        sequelize.query('SELECT insert_mandrill_send(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                                req.user.id + ',\'' +
                                                result[0].status + '\',\'' +
                                                result[0].reject_reason + '\',\'' + result[0]._id + '\',\'Richiesta Compilazione dati\',' +
                                                '\'' + req.body.text.replace(/'/g, "''''") + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {


                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');

                                                //res.status(422).send(utility.error422('Reject', 'Reject Reason', result[0].reject_reason, '422'));

                                            }).catch(function(err) {
                                                console.log(err);
                                            });
                                    }


                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                console.log(e);
                            });
                    }).catch(function(err) {
                         return res.status(400).render('search_organization_reply_to_new: ' + err);
                    });

                }
                
            }).catch(function(err) {
                return res.status(400).render('tag_insert: ' + err);
            });

    }).catch(function(err) {
        return res.status(400).render('user_request_payment: ' + err);
    });
}


function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};

/*
exports.guidemonterosa_export = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    var contact_id = null,
        from_date = null,
        to_date = null,
        type = null,
        stampa = null;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

       
        if(req.query.type == 'pdf'){
            

             var url = `https://asset.beebeeboard.com/${req.headers['host'].split(".")[0]}/stampe/export.php?from_date=${parameters.start_date.replace(/'/g, "")}&to_date=${parameters.end_date.replace(/'/g, "")}&organization=${req.headers['host'].split(".")[0]}&contact_id=${req.query.contact_id}&stampa=${req.query.stampa}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;
             
             res.redirect(url);

        }else
        {
             if (req.query.stampa !== undefined && req.query.stampa !== '') {

                stampa = '\'' + req.query.stampa.replace(/'/g, "''''") + '\'';
            }


            sequelize.query('SELECT guidemonterosa_export(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                req.user.role_id + ','+
                req.query.contact_id + ','+
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                stampa+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                var fields_array = datas[0].guidemonterosa_export[0]['fields'];
                var fieldNames_array = datas[0].guidemonterosa_export[0]['field_names'];
                
                json2csv({
                    data: datas[0].guidemonterosa_export,
                    fields: fields_array,
                    fieldNames: fieldNames_array
                }, function(err, csv) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=export.csv'
                    });
                    res.end(csv);
                });
            }).catch(function(err) {
                return res.status(400).render('Guidemonterosa Export: ' + err);
            });
        }

         
     });  
};

exports.guidemonterosa_get_export = function(req, res) {
   
    var contact_id = null,
        from_date = null,
        to_date = null,
        type = null,
        stampa = null;

    utility.get_parameters(req, 'project', function(err, parameters) {
        if (err) {
             callback_(err, null);
        }

        if (req.query.type !== undefined && req.query.type !== '') {

            type = '\''+req.query.type.replace(/'/g, "''''")+'\'';
        }

        if (req.query.stampa !== undefined && req.query.stampa !== '') {

            stampa = '\'' + req.query.stampa.replace(/'/g, "''''") + '\'';
        }

       
        sequelize.query('SELECT guidemonterosa_export(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            req.query.contact_id + ','+
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            stampa+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].guidemonterosa_export){
                res.status(200).send(datas[0].guidemonterosa_export);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('guidemonterosa_get_export: ' + err);
        });
 
     });  
};
*/