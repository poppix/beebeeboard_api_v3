var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js');


exports.index = function(req, res) {

    utility.customs('custom_field_definition', req, function(err, results) {

        indexes.custom_field_definition(req, results, function(err, response) {
            if (err) {
                return res.status(400).render('custom_field_definition_index: ' + err);
            }
            
            if(!response || response === undefined || !response.data || response.data === undefined){
                res.json({'data':[]});
            }else
            {
               res.json(response);
            }    
        });

    });
   
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var custom_field_definition = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        custom_field_definition['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        custom_field_definition['rank'] = (req.body.data.attributes.rank !== null && !utility.check_id(req.body.data.attributes.rank) ? req.body.data.attributes.rank : null);
        custom_field_definition['organization_custom_field_id'] = ((req.body.data.relationships && req.body.data.relationships.organization_custom_field && req.body.data.relationships.organization_custom_field.data && req.body.data.relationships.organization_custom_field.data.id) && !utility.check_id(req.body.data.relationships.organization_custom_field.data.id) ? req.body.data.relationships.organization_custom_field.data.id : null);
        custom_field_definition['organization'] = req.headers['host'].split(".")[0];
        custom_field_definition['_tenant_id'] = req.user._tenant_id;
        custom_field_definition['mongo_id'] = null;

        sequelize.query('SELECT insert_custom_field_definition(\'' +
                JSON.stringify(custom_field_definition) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_custom_field_definition;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'custom_field_definition',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_custom_field_definition,
                    'Aggiunta definizione campo personalizzato ' + datas[0].insert_custom_field_definition
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('custom_field_definition_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_custom_field_definition(' +
                req.params.id + ',\'' +
                (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (req.body.data.attributes.rank !== null && !utility.check_id(req.body.data.attributes.rank) ? req.body.data.attributes.rank : 1) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'custom_field_definition',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificata definizione campo personalizzato ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('custom_field_definition_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_custom_field_definition_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'custom_field_definition',
                    'id': datas[0].delete_custom_field_definition_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('custom_field_definition_destroy: ' + err);
        });
};