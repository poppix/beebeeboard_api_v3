var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');


exports.find = function(req, res) {

    finds.find_invoicetaxe(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var invoicetaxe = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        invoicetaxe['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        invoicetaxe['rate'] = req.body.data.attributes.rate;
        invoicetaxe['imponibile'] = req.body.data.attributes.imponibile;
        invoicetaxe['total'] = req.body.data.attributes.total;
        invoicetaxe['sum'] = req.body.data.attributes.sum;
        invoicetaxe['rank'] = req.body.data.attributes.rank;
        invoicetaxe['is_iva'] = req.body.data.attributes.is_iva !== undefined && utility.check_type_variable(req.body.data.attributes.is_iva, 'boolean') ? req.body.data.attributes.is_iva : null;
        invoicetaxe['is_ritenuta'] = req.body.data.attributes.is_ritenuta !== undefined && utility.check_type_variable(req.body.data.attributes.is_ritenuta, 'boolean') ? req.body.data.attributes.is_ritenuta : null;
        invoicetaxe['is_rivalsa'] = req.body.data.attributes.is_rivalsa !== undefined && utility.check_type_variable(req.body.data.attributes.is_rivalsa, 'boolean') ? req.body.data.attributes.is_rivalsa : null;
        invoicetaxe['type_rivalsa'] = (req.body.data.attributes.type_rivalsa && utility.check_type_variable(req.body.data.attributes.type_rivalsa, 'string') && req.body.data.attributes.type_rivalsa !== null ? req.body.data.attributes.type_rivalsa.replace(/'/g, "''''") : '');
        invoicetaxe['bollo_virtuale'] = req.body.data.attributes.bollo_virtuale !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_virtuale, 'boolean') ? req.body.data.attributes.bollo_virtuale : null;
        invoicetaxe['iva_natura'] = (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '');
        invoicetaxe['rivalsa_iva_rate'] = req.body.data.attributes.rivalsa_iva_rate;
        invoicetaxe['rivalsa_iva_natura'] = (req.body.data.attributes.rivalsa_iva_natura && utility.check_type_variable(req.body.data.attributes.rivalsa_iva_natura, 'string') && req.body.data.attributes.rivalsa_iva_natura !== null ? req.body.data.attributes.rivalsa_iva_natura.replace(/'/g, "''''") : '');
        invoicetaxe['is_bollo'] = req.body.data.attributes.is_bollo !== undefined && utility.check_type_variable(req.body.data.attributes.is_bollo, 'boolean') ? req.body.data.attributes.is_bollo : null;

        sequelize.query('SELECT insert_invoicetaxe_v2(\'' + JSON.stringify(invoicetaxe) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_invoicetaxe_v2 && datas[0].insert_invoicetaxe_v2 != null) {
                    req.body.data.id = datas[0].insert_invoicetaxe_v2;

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'invoicetaxe',
                        'insert',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        datas[0].insert_invoicetaxe_v2,
                        'Aggiunta invoicetaxe ' + datas[0].insert_invoicetaxe_v2
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });
                }
            }).catch(function(err) {
                return res.status(400).render('invoicetaxe_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_invoicetaxe_v3(' +
                req.params.id +
                ',\'' + (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') +
                '\',' + req.body.data.attributes.rate +
                ',' + req.body.data.attributes.imponibile +
                ',' + req.body.data.attributes.total +
                ',' + req.body.data.attributes.sum +
                ',' + req.body.data.attributes.rank +
                ',\'' + req.headers['host'].split(".")[0] + 
                '\','+ (req.body.data.attributes.is_iva !== undefined && utility.check_type_variable(req.body.data.attributes.is_iva, 'boolean') ? req.body.data.attributes.is_iva : null) +
                ',' + (req.body.data.attributes.is_ritenuta !== undefined && utility.check_type_variable(req.body.data.attributes.is_ritenuta, 'boolean') ? req.body.data.attributes.is_ritenuta : null) +
                ',' + (req.body.data.attributes.is_rivalsa !== undefined && utility.check_type_variable(req.body.data.attributes.is_rivalsa, 'boolean') ? req.body.data.attributes.is_rivalsa : null) +
                ',\'' + (req.body.data.attributes.type_rivalsa && utility.check_type_variable(req.body.data.attributes.type_rivalsa, 'string') && req.body.data.attributes.type_rivalsa !== null ? req.body.data.attributes.type_rivalsa.replace(/'/g, "''''") : '') +
                '\',' + (req.body.data.attributes.bollo_virtuale !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_virtuale, 'boolean') ? req.body.data.attributes.bollo_virtuale : null) + ''+
                ',\'' + (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '') +
                '\',' + req.body.data.attributes.rivalsa_iva_rate +
                ',\'' + (req.body.data.attributes.rivalsa_iva_natura && utility.check_type_variable(req.body.data.attributes.rivalsa_iva_natura, 'string') && req.body.data.attributes.rivalsa_iva_natura !== null ? req.body.data.attributes.rivalsa_iva_natura.replace(/'/g, "''''") : '') +
                '\','+(req.body.data.attributes.is_bollo !== undefined && utility.check_type_variable(req.body.data.attributes.is_bollo, 'boolean') ? req.body.data.attributes.is_bollo : null)+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'invoicetaxe',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificata invoicetaxe ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });
            }).catch(function(err) {
                return res.status(400).render('invoicetaxe_update: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_invoicetaxe_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'invoicetaxes',
                    'id': datas[0].delete_invoicetaxe_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('invoicetaxe_destroy: ' + err);
        });
};