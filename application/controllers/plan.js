var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    async = require('async'),
        logs = require('../utility/logs.js');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {


    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT plans_data(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                //callback(null,datas[0].contacts_data);
                if (datas[0].plans_data && datas[0].plans_data != null) {
                    callback(null, datas[0].plans_data);

                } else {
                    callback(null, null);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('plan_index: ' + err);
        }

        if (results.data.length == 0) {
            return res.status(404).send(utility.error404('id', 'Nessun piano tariffario trovato', 'Nessun piano tariffario trovato', '404'));

        }

        res.json({
            'data': utility.check_find_datas(results.data, 'plans', []),
            'included': []
        });

    });
};

exports.find = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT plan_find_data(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                if (datas[0].plan_find_data && datas[0].plan_find_data != null) {
                    callback(null, datas[0].plan_find_data);
                } else {
                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('plan_find: ' + err);
        }
        if (results.data.length == 0) {
            return res.status(404).send(utility.error404('id', 'Nessun piano tariffario trovato', 'Nessun piano tariffario trovato', '404'));
        }
        res.json({
            'data': utility.check_find_datas(results.data, 'plans', []),
            'included': []
        });
    });
};

exports.additional_packages = function(req, res) {
    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT additional_packages();', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {
                //callback(null,datas[0].contacts_data);
                if (datas[0].additional_packages && datas[0].additional_packages != null) {
                    callback(null, datas[0].additional_packages);

                } else {

                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('plan_index: ' + err);
        }

        res.json(results.data);

    });
};

exports.buy_additional_packages = function(req, res) {
    sequelize.query('SELECT stripe_get_customer_id_from_contact_id(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            var customer = datas[0].stripe_get_customer_id_from_contact_id;
            var amount = (req.body.pkg_price) * 1.22;

            var charge = stripe.charges.create({
                amount: amount * 100,
                currency: "eur",
                customer: customer
            }, function(err, charge) {
                if (charge.status == 'succeeded' && charge.paid == true) {
                    return res.status(200).send({ 'success': true });
                } else {
                    return res.status(500).send('buy_additional_packages stripe error: ' + err);
                }
            });

        }).catch(function(err) {
            return res.status(400).render('buy_additional_packages: ' + err);
        });
};