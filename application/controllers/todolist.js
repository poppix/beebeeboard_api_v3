var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');

exports.find = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_todolist(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};


exports.create = function(req, res) {

    var todolist = {};

    if (req.body.data.attributes !== undefined) {
        todolist['title'] = (req.body.data.attributes.title && utility.check_type_variable(req.body.data.attributes.title, 'string') && req.body.data.attributes.title !== null ? req.body.data.attributes.title.replace(/'/g, "''''") : '');
        todolist['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        todolist['organization'] = req.headers['host'].split(".")[0];
        todolist['product_id'] = ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null);
        todolist['project_id'] = ((req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data && req.body.data.relationships.project.data.id && !utility.check_id(req.body.data.relationships.project.data.id)) ? req.body.data.relationships.project.data.id : null);
        todolist['_tenant_id'] = req.user._tenant_id;
        todolist['mongo_id'] = null;
        todolist['date'] = (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : null);
        

        sequelize.query('SELECT insert_todolist(\'' + JSON.stringify(todolist) + '\',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {
            req.body.data.id = parseInt(datas[0].insert_todolist);

            logs.save_log_model(
                req.user._tenant_id,
                req.headers['host'].split(".")[0],
                'todolist',
                'insert',
                req.user.id,
                JSON.stringify(req.body.data.attributes),
                datas[0].insert_todolist,
                'Aggiunto todolist ' + datas[0].insert_todolist
            );

            if (todolist['project_id']) {
                logs.save_log_relation(
                    'project',
                    'todolist',
                    todolist['project_id'],
                    datas[0].insert_todolist,
                    'insert',
                    req.user.id,
                    req.headers['host'].split(".")[0],
                    req.user._tenant_id
                );
            }

            if (todolist['product_id']) {
                logs.save_log_relation(
                    'product',
                    'todolist',
                    todolist['product_id'],
                    datas[0].insert_todolist,
                    'insert',
                    req.user.id,
                    req.headers['host'].split(".")[0],
                    req.user._tenant_id
                );
            }

            utility.save_socket(req.headers['host'].split(".")[0], 'todolist', 'insert', req.user.id, datas[0].insert_todolist, req.body.data);
        

            res.json({
                'data': req.body.data,
                'relationships': req.body.data.relationships
            });

        }).catch(function(err) {
            return res.status(400).render('todolist_insert: ' + err);
        });
    } else {
        res.json({ 'data': [] });
    }
};

exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_todolist_v1(' +
            req.params.id + ',\'' +
            (req.body.data.attributes.title && utility.check_type_variable(req.body.data.attributes.title, 'string') && req.body.data.attributes.title !== null ? req.body.data.attributes.title.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') + '\',' +
            ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null) + ',' +
            ((req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data && req.body.data.relationships.project.data.id && !utility.check_id(req.body.data.relationships.project.data.id)) ? req.body.data.relationships.project.data.id : null) + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+    
            (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? '\''+req.body.data.attributes.date +'\'' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

            logs.save_log_model(
                req.user._tenant_id,
                req.headers['host'].split(".")[0],
                'todolist',
                'update',
                req.user.id,
                JSON.stringify(req.body.data.attributes),
                req.params.id,
                'Modificato todolist ' + req.params.id
            );

            utility.save_socket(req.headers['host'].split(".")[0], 'todolist', 'update', req.user.id, req.params.id, req.body.data);
        
            res.json({
                'data': {
                    'id': datas[0].update_todolist_v1,
                    'type': 'todolists',
                    'relationships': req.body.data.relationships
                }
            });

        }).catch(function(err) {
            return res.status(400).render('todolist_udpate: ' + err);
        });

    } else {
        res.json({ 'data': [] });
    }
};

exports.destroy = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_todolist_v1(' +
        req.params.id + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

            utility.save_socket(req.headers['host'].split(".")[0], 'todolist', 'delete', req.user.id, req.params.id, null);
        
        res.json({
            'data': {
                'id': datas[0].delete_todolist_v1,
                'type': 'todolists',

            }
        });
    }).catch(function(err) {
        return res.status(400).render('todolist_destroy: ' + err);
    });
};