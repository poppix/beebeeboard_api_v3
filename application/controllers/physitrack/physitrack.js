var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../../utility/utility.js'),
    async = require('async'),
    config = require('../../../config'),
    utils = require('../../../utils');
 var request = require('request');


exports.physitrack_connect = function(req, res) {
    
    sequelize.query('SELECT physitrack_contacts_insert(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            (req.body.apikey).replace(/'/g, "''''") + '\','+
            req.body.contact_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.physitrack_disconnect = function(req, res) {
    
    sequelize.query('SELECT physitrack_contacts_delete(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            (req.body.apikey).replace(/'/g, "''''") + '\','+
            req.body.contact_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.physitrack_patient = function(req, res) {
    var request = require('request');
    
    sequelize.query('SELECT physitrack_get_patient(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ','+
            req.query.contact_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].physitrack_get_patient){

                if(datas[0].physitrack_get_patient[0].token){
                    /* POST TO PHYSITRACK AND REDIRECT */
                    var res_json = { 'data': datas[0].physitrack_get_patient[0] };

                    //var str_to_send = '?data[token]='+datas[0].physitrack_get_patient[0].token+'&data[patient][id]='+datas[0].physitrack_get_patient[0].patient.id+'&data[patient][email]='+datas[0].physitrack_get_patient[0].patient.email+'&data[patient][title]='+datas[0].physitrack_get_patient[0].patient.title+'&data[patient][first_name]='+datas[0].physitrack_get_patient[0].patient.first_name+'&data[patient][last_name]='+datas[0].physitrack_get_patient[0].patient.last_name+'&data[patient][birth_year]='+datas[0].physitrack_get_patient[0].patient.birth_year;
                    //console.log(str_to_send);

                    var request = require('request');

                    var options = {
                      'method': 'POST',
                      'url': 'https://it.physitrack.com/pmsapi/beebeeboard/v1/patients',
                      'headers': {
                        'Content-Type': 'application/json'
                        },
                      body: JSON.stringify({"data":{"token":datas[0].physitrack_get_patient[0].token,"patient":{"id":datas[0].physitrack_get_patient[0].patient.id,"email":datas[0].physitrack_get_patient[0].patient.email,"title":null,"first_name":datas[0].physitrack_get_patient[0].patient.first_name,"last_name":datas[0].physitrack_get_patient[0].patient.last_name,"birth_year":datas[0].physitrack_get_patient[0].patient.birth_year,"mobile_phone":datas[0].physitrack_get_patient[0].patient.mobile_phone,"ssn":datas[0].physitrack_get_patient[0].patient.id}}})

                    };
                    request(options, function (err1, response) {
                      if(err1){
                            console.log(err1);
                        }
                        if(response.body){
                            console.log(response.body);
                            var red = response.body.split('"')[1];
                            if(red){
                                res.redirect(red);
                            }else{
                                res.status(200).send({'response': response.body});
                            }
                            
                        }
                    });

                   
                }else
                {
                    res.status(422).send(utility.error422('apikey', 'Parametro non valido', 'Parametro non valido', '422'));
                }
            }else
            {
                res.status(404).send({});
            }
            
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
};

exports.physitrack_webhook = function(req, res) {
    var body_example = req.body;
   

    if(body_example.api_key == config.physitrack.token_key){

         sequelize.query('SELECT physitrack_get_data(\''+
            JSON.stringify(body_example).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
           
            if(datas && datas[0] && datas[0].physitrack_get_data){
               
                var token = utils.uid(config.token.accessTokenLength);

                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                    moment(new Date()).add(1, 'hour').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                    datas[0].physitrack_get_data.organization_admin_id + ',' +
                    5 + ',\'\',\'' + datas[0].physitrack_get_data.organization + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(tok) {


                   sequelize.query('SELECT physitrack_get_clinic(\''+
                        datas[0].physitrack_get_data.organization + '\',\''+
                        JSON.stringify(body_example).replace(/'/g, "''''") + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(datas1) {

                        if(datas1[0].physitrack_get_clinic[0].project_id != -1){
                            var json_send = {
                                'model': 'project',
                                'model_id': datas1[0].physitrack_get_clinic[0].project_id,
                                'url': body_example.file_url,
                                'key': datas[0].physitrack_get_data.organization + '/' +datas[0].physitrack_get_data.organization_admin_id + '/physitrack/' +body_example.name,
                                'filename': body_example.name,
                                'extension': 'pdf'

                            };
                        }else
                        {
                            var json_send = {
                                'model': 'contact',
                                'model_id': body_example.patient_id,
                                'url': body_example.file_url,
                                'key':  datas[0].physitrack_get_data.organization + '/' +datas[0].physitrack_get_data.organization_admin_id + '/physitrack/' +body_example.name,
                                'filename': body_example.name,
                                'extension': 'pdf'

                            };
                        }
                        


                        request({
                                headers: {
                                  'Authorization': 'Bearer '+token,
                                  'Content-Type': 'application/json'
                                },
                                uri: 'https://'+datas[0].physitrack_get_data.organization+'.beebeeboard.com/api/v1/app/file_url',
                                body: JSON.stringify(json_send),
                                method: 'POST'
                            },  function (err1, result1, body1) {

                                if(err1){
                                    console.log(err1);
                                }
                                
                                res.status(204).send({});
                                   
                            });
                        }).catch(function(err) {
                            return res.status(400).render('physitrack token: ' + err);
                        });
                    }).catch(function(err) {
                        return res.status(400).render('physitrack token: ' + err);
                    });

            }
            else{
                res.status(422).send({});
            }
           
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });

    }
    else{
        res.status(401).send({});
    }

    
};

