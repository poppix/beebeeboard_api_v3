var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');


exports.default = function(req, res) {

    sequelize.query('SELECT invoice_style_default(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.json({
                'data': datas[0].invoice_style_default
            });
        }).catch(function(err) {
            //next(new Error(err));
            return res.status(400).render('invoice_style_default: ' + err);

        });
}

exports.contact_default = function(req, res) {

    sequelize.query('SELECT invoice_style_contact_default(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            (req.query.contact_id ? req.query.contact_id : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.json({
                'data': datas[0].invoice_style_contact_default
            });
        }).catch(function(err) {
            //next(new Error(err));
            return res.status(400).render('invoice_style_contact_default: ' + err);

        });
}


exports.find = function(req, res) {

    finds.find_invoice_style(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var invoice_style = {};

    if (req.body.data.attributes !== undefined) {
        // Import case


        sequelize.query('SELECT insert_invoice_style(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_invoice_style && datas[0].insert_invoice_style != null) {
                    req.body.data.id = datas[0].insert_invoice_style;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_invoice_style,
                        'body': req.body.data.relationships,
                        'model': 'invoice_style',
                        'user_id': req.user.id,
                        'array': ['file']
                    }, function(err, results) {
                        req.params.id = datas[0].insert_invoice_style;
                        finds.find_invoice_style(req, function(err, results) {
                            console.log(results);
                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'invoice_style',
                                'insert',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                datas[0].insert_invoice_style,
                                'Aggiunto stile di fattura ' + datas[0].insert_invoice_style
                            );

                            res.json(results);
                        });
                    });
                }

            }).catch(function(err) {
                //next(new Error(err));
                return res.status(400).render('invoice_style_insert: ' + err);

            });


    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case

        sequelize.query('SELECT update_invoice_style(' +
                req.params.id +
                ',\'' + (req.body.data.attributes.background_color && utility.check_type_variable(req.body.data.attributes.background_color, 'string') && req.body.data.attributes.background_color !== null ? req.body.data.attributes.background_color.replace(/'/g, "''''") : '') +
                '\',\'' + (req.body.data.attributes.primary_color && utility.check_type_variable(req.body.data.attributes.primary_color, 'string') && req.body.data.attributes.primary_color !== null ? req.body.data.attributes.primary_color.replace(/'/g, "''''") : '') +
                '\',\'' + (req.body.data.attributes.text_color && utility.check_type_variable(req.body.data.attributes.text_color, 'string') && req.body.data.attributes.text_color !== null ? req.body.data.attributes.text_color.replace(/'/g, "''''") : '') +
                '\',' +
                (req.body.data.attributes.show_logo !== undefined && utility.check_type_variable(req.body.data.attributes.show_logo, 'boolean') ? req.body.data.attributes.show_logo : false) + ',\'' +
                (req.body.data.attributes.logo_position && utility.check_type_variable(req.body.data.attributes.logo_position, 'string') && req.body.data.attributes.logo_position !== null ? req.body.data.attributes.logo_position.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.show_society !== undefined && utility.check_type_variable(req.body.data.attributes.show_society, 'boolean') ? req.body.data.attributes.show_society : false) + ',\'' +
                (req.body.data.attributes.society_position && utility.check_type_variable(req.body.data.attributes.society_position, 'string') && req.body.data.attributes.society_position !== null ? req.body.data.attributes.society_position.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.contact_position && utility.check_type_variable(req.body.data.attributes.contact_position, 'string') && req.body.data.attributes.contact_position !== null ? req.body.data.attributes.contact_position.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.style && utility.check_type_variable(req.body.data.attributes.style, 'string') && req.body.data.attributes.style !== null ? req.body.data.attributes.style.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.show_qta !== undefined && utility.check_type_variable(req.body.data.attributes.show_qta, 'boolean') ? req.body.data.attributes.show_qta : false) + ',' +
                (req.body.data.attributes.show_unit !== undefined && utility.check_type_variable(req.body.data.attributes.show_unit, 'boolean') ? req.body.data.attributes.show_unit : false) + ',' +
                (req.body.data.attributes.show_price !== undefined && utility.check_type_variable(req.body.data.attributes.show_price, 'boolean') ? req.body.data.attributes.show_price : false) + ',' +
                (req.body.data.attributes.show_sign !== undefined && utility.check_type_variable(req.body.data.attributes.show_sign, 'boolean') ? req.body.data.attributes.show_sign : false) + ',' +
                (req.body.data.attributes.show_footer !== undefined && utility.check_type_variable(req.body.data.attributes.show_footer, 'boolean') ? req.body.data.attributes.show_footer : false) + ',' +
                (req.body.data.attributes.show_society_name !== undefined && utility.check_type_variable(req.body.data.attributes.show_society_name, 'boolean') ? req.body.data.attributes.show_society_name : false) + ',' +
                req.body.data.attributes.footer_height + ',' +
                req.body.data.attributes.header_height + ',' +
                (req.body.data.attributes.is_default !== undefined && utility.check_type_variable(req.body.data.attributes.is_default, 'boolean') ? req.body.data.attributes.is_default : false) + ',\'' +
                (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.is_editable !== undefined && utility.check_type_variable(req.body.data.attributes.is_editable, 'boolean') ? req.body.data.attributes.is_editable : false) + ',\'' +
                (req.body.data.attributes.custom_css && utility.check_type_variable(req.body.data.attributes.custom_css, 'string') && req.body.data.attributes.custom_css !== null ? req.body.data.attributes.custom_css.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.society_description && utility.check_type_variable(req.body.data.attributes.society_description, 'string') && req.body.data.attributes.society_description !== null ? req.body.data.attributes.society_description.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.footer_content && utility.check_type_variable(req.body.data.attributes.footer_content, 'string') && req.body.data.attributes.footer_content !== null ? req.body.data.attributes.footer_content.replace(/'/g, "''''") : '') + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {


                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'invoice_style',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato stile fattura ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });


            }).catch(function(err) {
                return res.status(400).render('invoice_style_update: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_invoice_style(' +
            req.params.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'invoice_styles',
                    'id': datas[0].delete_invoice_style
                }
            });

        }).catch(function(err) {
            return res.status(400).render('invoice_style_destroy: ' + err);
        });
};