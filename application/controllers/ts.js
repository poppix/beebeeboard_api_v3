var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    archiver = require('archiver'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    export_datas = require('../utility/exports.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js'),
    config = require('../../config'),
    xml = require('xml-parser'),
    xml_json = require('xml2json'),
    stringify = require('xml-stringify'),
    request = require('request'),
    FormData = require('form-data');

function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
};

exports.ts_control_enable = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
   
    if (req.body.contact_id !== undefined && req.body.contact_id != '') {
        contact_id = req.body.contact_id;
    }else{
        contact_id = req.user.id;
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        contact_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

        if(!tss[0] || !tss[0].get_ts_system_data_from_user){
            return res.status(422).send(utility.error422('NESSUN ACCOUNT ASSOCIATO', 'Non risulta associato alcun account', 'Non risulta associato alcun account', '422'));
        }

        if(tss[0].get_ts_system_data_from_user.trial === true){
            return res.status(422).send(utility.error422('ACCOUNT IN PROVA', 'Non risulta attivo un abbonamento a Beebeeboard', 'Non risulta attivo un abbonamento a Beebeeboard', '422'));
        }

        if(!tss[0].get_ts_system_data_from_user.ts_username || tss[0].get_ts_system_data_from_user.ts_username == ''){
            return res.status(422).send(utility.error422('USERNAME', 'Username non inserita', 'Username non inserita', '422'));
        }
           
        if(!tss[0].get_ts_system_data_from_user.ts_pwd || tss[0].get_ts_system_data_from_user.ts_pwd == ''){
            return res.status(422).send(utility.error422('PASSWORD', 'Password non inserita', 'Password non inserita', '422'));
        }

        if(!tss[0].get_ts_system_data_from_user.ts_pincode || tss[0].get_ts_system_data_from_user.ts_pincode == ''){
            return res.status(422).send(utility.error422('PIN CODE', 'Pin Code non inserito', 'Pin Code non inserito', '422'));
        }

        
        
        if(tss[0].get_ts_system_data_from_user.ts_struttura  && tss[0].get_ts_system_data_from_user.ts_struttura === true){

            if(!tss[0].get_ts_system_data_from_user.ts_cod_fiscale || tss[0].get_ts_system_data_from_user.ts_cod_fiscale == ''){
                return res.status(422).send(utility.error422('CODICE PROPRIETARIO', 'Codice proprietario non inserito', 'Codice proprietario non inserito', '422'));
            }

            if(!tss[0].get_ts_system_data_from_user.ts_codregione || tss[0].get_ts_system_data_from_user.ts_codregione == ''){
                return res.status(422).send(utility.error422('CODICE REGIONE', 'Codice Regione non inserito', 'Codice regione non inserito', '422'));
            }

            if(!tss[0].get_ts_system_data_from_user.ts_codasl || tss[0].get_ts_system_data_from_user.ts_codasl == ''){
                return res.status(422).send(utility.error422('CODICE ASL', 'Codice Asl non inserito', 'Codice regione non inserito', '422'));
            }

            if(!tss[0].get_ts_system_data_from_user.ts_codssa || tss[0].get_ts_system_data_from_user.ts_codssa == ''){
                return res.status(422).send(utility.error422('CODICE SSA', 'Codice Ssa non inserito', 'Codice regione non inserito', '422'));
            }

            
        }

        try{

            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;
            ts_datas.cod_fiscale = tss[0].get_ts_system_data_from_user.ts_cod_fiscale;


           /* var options = {
              'method': 'POST',
              'url': 'https://sistemats4.sanita.finanze.it/simossHome/j_security_check?j_username='+ts_datas.username+'&j_password='+ts_datas.pwd+'&conferma=CONFERMA',
              'headers': {
              }
            };
            request(options, function (error, response) {
              if (error) throw new Error(error);
              console.log(response.rawHeaders);
              if(response.rawHeaders.includes('https://sistemats4.sanita.finanze.it/simossHome/loginFailed.jsp') === true){
                    return res.status(422).send(utility.error422('CREDENZIALI TS INVALIDE O PASSWORD SCADUTA', 'Username o Password errati o password scaduta', 'Username o Password errati o password scaduta', '422'));
              }else
              {*/
                res.status(200).send({});
              /*}
            });*/

             /*request.post('https://sistemats4.sanita.finanze.it/simossHome/j_security_check?j_username='+ts_datas.username+'&j_password='+ts_datas.pwd+'&conferma=CONFERMA', function(err,body, risposta) {
                console.log(risposta);
                if(risposta.indexOf('Password Scaduta') != -1){
                    return res.status(422).send(utility.error422('PASSWORD SISTEMA TS SCADUTA', 'Password sistema TS scaduta', 'Password sistema TS scaduta', '422'));
                }
                else if(risposta.indexOf('Credenziali invalide') != -1){
                    return res.status(422).send(utility.error422('CREDENZIALI TS INVALIDE', 'Username o Password errati', 'Username o Password errati', '422'));
                }
                else{
                   res.status(200).send({});
                }
            });*/

            /*var soap = require('soap');

            var url =  'https://asset.beebeeboard.com/utility/ts_system/DocumentoSpesa730p.wsdl';

            soap.createClient(url,  function(err, client) {
        
                client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));


                if(err){
                    return res.status(422).send(utility.error422('Autenticazione', err, err, '422'));
                }

                res.status(200).send({});
               
            });*/
        } catch(err){
            return res.status(422).send(utility.error422('Autenticazione', err, err, '422'));
        };
    

    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};


exports.ts_single_xml = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    var tipo_spesa = null;
    var contact_id = null;
    var operazione = null;
    
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.body.id !== undefined && req.body.id != '') {
        id = req.body.id;
    }

    if (req.body.tipo_spesa !== undefined && req.body.tipo_spesa != '') {
        tipo_spesa = req.body.tipo_spesa.substring(0, 2);
    }

    if (req.body.operazione !== undefined && req.body.operazione != '') {
        operazione = req.body.operazione;
    }

    if (req.body.user_id !== undefined && req.body.user_id != '') {
        contact_id = req.body.user_id;
    }else{
        contact_id = req.user.id;
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        contact_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

         if(tss && tss[0] && tss[0].get_ts_system_data_from_user && tss[0].get_ts_system_data_from_user.ts_username){

            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;
            ts_datas.cod_fiscale = tss[0].get_ts_system_data_from_user.ts_cod_fiscale;

            
            sequelize.query('SELECT invoices_ts_send_ids(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if(datas[0].invoices_ts_send_ids && datas[0].invoices_ts_send_ids.length > 0){

                    //tsXml.generaTsBase(ts_datas, datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione, function(err, response) {

                        //var xmlData = tsXml.generaXmlTs(response);  
                        try{

                            var soap = require('soap');

                            var url =  'https://asset.beebeeboard.com/utility/ts_system/DocumentoSpesa730p.wsdl';
                        
                            soap.createClient(url, function(err, client) {
                                console.log(err);

                                client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                                if(datas[0].invoices_ts_send_ids[0].attributes.is_creditnote === true){
                                    operazione = 'R';
                                }

                                if(!operazione || operazione == 'I'){
                                    var args = {
                                         
                                         //inserimentoDocumentoSpesaRequest: {
                                            pincode: tsXml.CodificaStringa(ts_datas.pinCode),
                                            Proprietario: tsXml.Proprietario(ts_datas),
                                            idInserimentoDocumentoFiscale: tsXml.GetDocumentiSpesa(datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione)
                                        // }
                                    };

                                    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                                    console.log(JSON.stringify(args));

                                    var res1 = client.Inserimento(args, function(err, result) {
                                        console.log('Errore file TS ' + err);

                                        if(err){
                                            return res.status(422).send(utility.error422('ERRORE', 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), '422'));
                                        }

                                        
                                        console.log('Res file TS ');
                                        console.log(JSON.stringify(result));

                                        sequelize.query('SELECT update_invoice_ts_response_single(' +
                                            req.user._tenant_id+',\''+
                                            req.headers['host'].split(".")[0] + '\','+
                                            req.user.id+','+
                                            id+',\''+
                                            JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\',\'I\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            console.log(datas);
                                            var finale_attesa = null;
                                            finale_attesa = controllo_attesa(ts_datas, req.headers['host'].split(".")[0], req.user._tenant_id, req.user.id, datas[0].update_invoice_ts_response_single);
                                        }).catch(function(err) {
                                            return res.status(400).render('update_invoice_ts_response_single: ' + err);
                                        });

                                        

                                       res.status(200).json({});
                                    });
                                }else if(operazione == 'V'){
                                    var args = {
                                         
                                         //inserimentoDocumentoSpesaRequest: {
                                            pincode: tsXml.CodificaStringa(ts_datas.pinCode),
                                            Proprietario: tsXml.Proprietario(ts_datas),
                                            idVariazioneDocumentoFiscale: tsXml.GetDocumentiSpesa(datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione)
                                        // }
                                    };


                                    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                                    var res1 = client.Variazione(args, function(err, result) {
                                        if(err){
                                            return res.status(422).send(utility.error422('ERRORE', 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), '422'));
                                        }
                                        
                                        console.log('Errore file TS ' + err);
                                        console.log('Res file TS ');
                                        console.log(JSON.stringify(result));

                                        sequelize.query('SELECT update_invoice_ts_response_single(' +
                                            req.user._tenant_id+',\''+
                                            req.headers['host'].split(".")[0] + '\','+
                                            req.user.id+','+
                                            id+',\''+
                                            JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\',\'V\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            var finale_attesa = null;
                                            finale_attesa = controllo_attesa(ts_datas, req.headers['host'].split(".")[0], req.user._tenant_id, req.user.id, datas[0].update_invoice_ts_response_single);
                                        }).catch(function(err) {
                                            return res.status(400).render('update_invoice_ts_response_single: ' + err);
                                        });

                                        

                                       res.status(200).json({});
                                    });
                                }else if(operazione == 'R'){
                                    var args = {
                                         
                                         //inserimentoDocumentoSpesaRequest: {
                                            pincode: tsXml.CodificaStringa(ts_datas.pinCode),
                                            Proprietario: tsXml.Proprietario(ts_datas),
                                            idRimborsoDocumentoFiscale: tsXml.GetDocumentoFiscale(datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione),
                                            DocumentoSpesa: tsXml.GetDocumentiSpesa(datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione)
                                       
                                        // }
                                    };


                                    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                                    var res1 = client.Rimborso(args, function(err, result) {
                                        if(err){
                                            return res.status(422).send(utility.error422('ERRORE', 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), '422'));
                                        }
                                        
                                        console.log('Errore file TS ' + err);
                                        console.log('Res file TS ');
                                        console.log(JSON.stringify(result));

                                        sequelize.query('SELECT update_invoice_ts_response_single(' +
                                            req.user._tenant_id+',\''+
                                            req.headers['host'].split(".")[0] + '\','+
                                            req.user.id+','+
                                            id+',\''+
                                            JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\',\'R\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            var finale_attesa = null;
                                            finale_attesa = controllo_attesa(ts_datas, req.headers['host'].split(".")[0], req.user._tenant_id, req.user.id, datas[0].update_invoice_ts_response_single);
                                        }).catch(function(err) {
                                            return res.status(400).render('update_invoice_ts_response_single: ' + err);
                                        });

                                        

                                       res.status(200).json({});
                                    });
                                }else if(operazione == 'C'){
                                    var args = {
                                         
                                         //inserimentoDocumentoSpesaRequest: {
                                            pincode: tsXml.CodificaStringa(ts_datas.pinCode),
                                            Proprietario: tsXml.Proprietario(ts_datas),
                                            idCancellazioneDocumentoFiscale: tsXml.GetDocumentoFiscale(datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione)
                                        // }
                                    };


                                    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                                    var res1 = client.Cancellazione(args, function(err, result) {
                                        if(err){
                                            return res.status(422).send(utility.error422('ERRORE', 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), 'Errore invio:' +err.toString().replace('env:Client','').replace('Error:',''), '422'));
                                        }
                                        
                                        console.log('Errore file TS ' + err);
                                        console.log('Res file TS ');
                                        console.log(JSON.stringify(result));

                                        sequelize.query('SELECT update_invoice_ts_response_single(' +
                                            req.user._tenant_id+',\''+
                                            req.headers['host'].split(".")[0] + '\','+
                                            req.user.id+','+
                                            id+',\''+
                                            JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\',\'C\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            var finale_attesa = null;
                                            finale_attesa = controllo_attesa(ts_datas, req.headers['host'].split(".")[0], req.user._tenant_id, req.user.id, datas[0].update_invoice_ts_response_single);
                                        }).catch(function(err) {
                                            return res.status(400).render('update_invoice_ts_response_single: ' + err);
                                        });

                                        

                                       res.status(200).json({});
                                    });
                                }

                            });


                        } catch(err) {
                            return res.status(400).render('invoices_ts_send_ids: ' + err);
                        };
                        //var data = fs.readFileSync(__dirname + '/PROVA_INVIO_XXX000.zip');

                        
                     
                    //});
                    
                }else{
                    res.status(404).send();
                }
              

            }).catch(function(err) {
                return res.status(400).render('invoices_ts_send_ids: ' + err);
            });

        }else
        {
            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
        }

    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.ts_multiple_xml = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    var tipo_spesa = null;
    var operazione = null;
    
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.query.id !== undefined && req.query.id != '') {
        id = req.query.id;
    }

    if (req.query.tipo_spesa !== undefined && req.query.tipo_spesa != '') {
        tipo_spesa = req.query.tipo_spesa.substring(0, 2);
    }

    if (req.query.operazione !== undefined && req.query.operazione != '') {
        operazione = req.query.operazione;
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        req.user.id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

         if(tss && tss[0].get_ts_system_data_from_user.ts_username){
            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;

            
            sequelize.query('SELECT invoices_ts_send_ids(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if(datas[0].invoices_ts_send_ids && datas[0].invoices_ts_send_ids.length > 0){

                    tsXml.generaTsBase(ts_datas, datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione, function(err, response) {

                        var xmlData = tsXml.generaXmlTs(response);
                        var fs = require('fs');
                        
                        var nome_file = req.headers['host'].split(".")[0] +'_'+(datas[0].invoices_ts_send_ids[0].attributes.number ? datas[0].invoices_ts_send_ids[0].attributes.number.replace(/[^a-zA-Z0-9.]/g, '') : '')+'_'+moment().format('DD_MM_YYYY_HH_mm');

                        var output = fs.createWriteStream(__dirname + '/'+nome_file+'.zip');

                        var archive = archiver('zip', {
                          zlib: { level: 9 } // Sets the compression level.
                        });

                        // This event is fired when the data source is drained no matter what was the data source.
                        // It is not part of this library but rather from the NodeJS Stream API.
                        // @see: https://nodejs.org/api/stream.html#stream_event_end
                        archive.on('finish', function() {
                          console.log('Archive wrote %d bytes', archive.pointer());

                          setTimeout(function(){
                              fs.readFile(__dirname + '/'+nome_file+'.zip', function (err, data) {
                                    if (err) return console.error(err);
                                   
                                    var soap = require('soap');

                                    var url =  'https://asset.beebeeboard.com/utility/ts_system/InvioTelematicoSpeseSanitarie730p.wsdl';
                                
                                    soap.createClient(url, function(err, client) {

                                        client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                                        var args = {
                                             nomeFileAllegato: nome_file+'.zip',
                                             pincodeInvianteCifrato: tsXml.CodificaStringa(ts_datas.pinCode),
                                             datiProprietario: tsXml.Proprietario(ts_datas),
                                             documento: data.toString('base64')
                                        }

                                        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                                        var res1 = client.inviaFileMtom(args, function(err, result) {
                                            console.log('Errore file TS ' + err);
                                            console.log('Res file TS ');
                                            console.log(result);

                                            sequelize.query('SELECT update_invoice_ts_response(' +
                                                req.user._tenant_id+',\''+
                                                req.headers['host'].split(".")[0] + '\','+
                                                req.user.id+','+
                                                id+',\''+
                                                JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {

                                            }).catch(function(err) {
                                                return res.status(400).render('update_invoice_ts_response: ' + err);
                                            });

                                            fs.unlink(__dirname + '/'+nome_file+'.zip', function(err, r){
                                                console.log('Errore Unlink file TS ' + err);
                                                console.log('Res Unlink file TS ' + r);
                                            });

                                           res.status(200).json({});
                                        });

                                    });
                                });
                             } , 1500);
                        });


                        // good practice to catch warnings (ie stat failures and other non-blocking errors)
                        archive.on('warning', function(err) {
                            console.log(err);
                          if (err.code === 'ENOENT') {
                            // log warning
                          } else {
                            // throw error
                            throw err;
                          }
                        });

                        // good practice to catch this error explicitly
                        archive.on('error', function(err) {
                            console.log(err);
                          return res.status(500).send({error: err.message});
                        });

                        archive.pipe(output);
                        
                        archive.append(xmlData, {
                            'name': nome_file+'.xml'
                        });
                          
                        archive.finalize();

                        

                        //var data = fs.readFileSync(__dirname + '/PROVA_INVIO_XXX000.zip');

                        
                     
                    });
                    
                }else{
                    res.status(404).send();
                }
              

            }).catch(function(err) {
                return res.status(400).render('invoices_ts_send_ids: ' + err);
            });

        }else
        {
            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
        }

    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.ts_zip_xml = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    var fs = require('fs');
    
    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        req.user.id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

         if(tss && tss[0].get_ts_system_data_from_user.ts_username){
            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;
            ts_datas.cod_fiscale = tss[0].get_ts_system_data_from_user.ts_cod_fiscale;

            fs.readFile(__dirname + '/20200120_161556_AMBU_450000_1.xml.zip', function (err, data) {
                    if (err) return console.error(err);
                  
                    
                    var soap = require('soap');

                    var url =  'https://asset.beebeeboard.com/utility/ts_system/InvioTelematicoSpeseSanitarie730p.wsdl';
                
                    soap.createClient(url, function(err, client) {

                        client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                        var args = {
                             nomeFileAllegato: '20200120_161556_AMBU_450000_1.xml.zip',
                             pincodeInvianteCifrato: tsXml.CodificaStringa(ts_datas.pinCode),
                             datiProprietario: {
                                codiceRegione: ts_datas.codRegione,
                                codiceAsl: ts_datas.codAsl,
                                codiceSSA: ts_datas.codSSA,
                                cfProprietario: ts_datas.cod_fiscale
                             },
                             documento: data.toString('base64')
                        }

                        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                        console.log(args);

                        var res1 = client.inviaFileMtom(args, function(err, result) {
                            console.log('Errore file TS ' + err);
                            console.log('Res file TS ');
                            console.log(result);
                            /*
                            sequelize.query('SELECT update_invoice_ts_response(' +
                                req.user._tenant_id+',\''+
                                req.headers['host'].split(".")[0] + '\','+
                                req.user.id+','+
                                id+',\''+
                                JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(datas) {

                            }).catch(function(err) {
                                return res.status(400).render('update_invoice_ts_response: ' + err);
                            });*/

                            

                           res.status(200).json({});
                        });

                    });
                });
            }

    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.ts_response = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    var contact_id = null;

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        contact_id = req.query.contact_id;
    }else{
        contact_id = req.user.id;
    }
    
    if (req.params.id !== undefined && req.params.id != '') {
         id = req.params.id;  
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        contact_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

        if(tss && tss[0].get_ts_system_data_from_user.ts_username){
            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;

            sequelize.query('SELECT invoices_ts_protocol(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if(datas[0].invoices_ts_protocol){

                   var protocollo_ = datas[0].invoices_ts_protocol.protocollo;

                    if(protocollo_){
                            

                            var soap = require('soap');

                            var url =  'https://asset.beebeeboard.com/utility/ts_system/EsitoInvioDatiSpesa730Service.wsdl';
                        
                            soap.createClient(url, function(err, client) {

                                client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                                process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                                var args = {
                                    DatiInputRichiesta: {
                                        pinCode: tsXml.CodificaStringa(ts_datas.pinCode),
                                        protocollo: protocollo_
                                    }
                                };

                                var res1 = client.EsitoInvii(args, function(err, result) {
                                    console.log('Errore file TS ' + err);
                                    console.log('Res file TS ');
                                    console.log(result);

                                    sequelize.query('SELECT update_invoice_ts_ric_response(' +
                                        req.user._tenant_id+',\''+
                                        req.headers['host'].split(".")[0] + '\','+
                                        req.user.id+','+
                                        id+',\''+
                                        JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\',\''+
                                        protocollo_+'\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {

                                    }).catch(function(err) {
                                        return res.status(400).render('update_invoice_ts_ric_response: ' + err);
                                    });

                                    res.status(200).json({});
                                });

                            });
                        }else
                        {
                            res.status(404).send();
                        }
                    
                   
                    
                            
                }

            }).catch(function(err) {
                return res.status(400).render('invoices_ts_send_ids: ' + err);
            });
        }else
        {
            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
        }
    
    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.ts_ricevuta_pdf = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    var contact_id = null;
    
    if (req.params.id !== undefined && req.params.id != '') {
         id = req.params.id;  
    }

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        contact_id = req.query.contact_id;
    }else{
        contact_id = req.user.id;
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        contact_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

        if(tss && tss[0].get_ts_system_data_from_user.ts_username){
            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;

            sequelize.query('SELECT invoices_ts_protocol(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if(datas[0].invoices_ts_protocol){

                    var protocollo_ = datas[0].invoices_ts_protocol.protocollo;

                    if(protocollo_){
                            

                        var soap = require('soap');

                        var url =  'https://asset.beebeeboard.com/utility/ts_system/RicevutaPdf730Service.wsdl';
                    
                        soap.createClient(url, function(err, client) {

                            client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                            process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                            var args = {
                                DatiInputRichiesta: {
                                    pinCode: tsXml.CodificaStringa(ts_datas.pinCode),
                                    protocollo: protocollo_
                                }
                            };


                            var res1 = client.RicevutaPdf(args, function(err, result) {
                                console.log('Errore file TS ' + err);
                                console.log('Res file TS ');


                                
                                if(result){
                                    var json_res = result;
                                    if(json_res && json_res.DatiOutputRichiesta){
                                        if(json_res.DatiOutputRichiesta.esitiPositivi && 
                                            json_res.DatiOutputRichiesta.esitiPositivi.dettagliEsito){
                                            var pdfData = new Buffer.from(json_res.DatiOutputRichiesta.esitiPositivi.dettagliEsito.pdf, 'base64');
                                            res.writeHead(200, {
                                                'Content-Type': 'application/pdf',
                                                'Content-Disposition': `attachment; filename="${protocollo_}.pdf"`,
                                                'Content-Length': pdfData.length
                                            });
                                           
                                            res.end(pdfData);
                                        }else if(json_res.DatiOutputRichiesta.esitiNegativi && 
                                            json_res.DatiOutputRichiesta.esitiNegativi.dettagliEsito){
                                           var pdfData = Buffer.from(json_res.DatiOutputRichiesta.esitiNegativi.dettagliEsito.pdf);
                                            
                                           res.writeHead(200, {
                                                'Content-Type': 'application/pdf',
                                                'Content-Disposition': `attachment; filename=${protocollo_}.pdf`,
                                                'Content-Length': json_res.DatiOutputRichiesta.esitiNegativi.dettagliEsito.pdf.length
                                            });
                                           
                                            res.end(pdfData);
                                            //res.end(new Buffer.from(json_res.DatiOutputRichiesta.esitiNegativi.dettagliEsito.pdf, 'base64'));
                                        }
                                        else{
                                            res.status(404).json({});
                                        }

                                    }
                                }
                                else
                                {
                                    res.status(404).json({});
                                }

                               
                            });

                        });
                    }else
                    {
                        res.status(404).send();
                    }
                            
                }

            }).catch(function(err) {
                return res.status(400).render('invoices_ts_send_ids: ' + err);
            });
        }else
        {
            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
        }
    
    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.ts_dettaglio_errori = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    var contact_id = null;
    
    if (req.params.id !== undefined && req.params.id != '') {
         id = req.params.id;  
    }

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        contact_id = req.query.contact_id;
    }else{
        contact_id = req.user.id;
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        contact_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

        if(tss && tss[0].get_ts_system_data_from_user.ts_username){
            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;

            sequelize.query('SELECT invoices_ts_protocol(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if(datas[0].invoices_ts_protocol){

                    var protocollo_ = datas[0].invoices_ts_protocol.protocollo;

                    if(protocollo_){
                            

                        var soap = require('soap');

                        var url =  'https://asset.beebeeboard.com/utility/ts_system/DettaglioErrori730Service.wsdl';
                    
                        soap.createClient(url, function(err, client) {

                            client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                            process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                            var args = {
                                DatiInputRichiesta: {
                                    pinCode: tsXml.CodificaStringa(ts_datas.pinCode),
                                    protocollo: protocollo_
                                }
                            };


                            var res1 = client.DettaglioErrori(args, function(err, result) {
                                console.log('Errore file TS ' + err);
                                console.log('Res file TS ');
                                console.log(JSON.stringify(result));
                                var csv = null;
                                if(result.DatiOutputRichiesta.esitiPositivi && result.DatiOutputRichiesta.esitiPositivi.dettagliEsito){
                                    csv = new Buffer.from(result.DatiOutputRichiesta.esitiPositivi.dettagliEsito.csv, 'base64');
                                    
                                        res.writeHead(200, {
                                        'Content-Disposition': 'attachment; filename='+protocollo_+'_errori.zip',
                                        'Content-Length': csv.length
                                    });

                                    res.end(csv);
                                }else if(result.DatiOutputRichiesta.esitiNegativi && result.DatiOutputRichiesta.esitiNegativi.dettagliEsitoNegativo && result.DatiOutputRichiesta.esitiNegativi.dettagliEsitoNegativo.csv){
                                    csv = new Buffer.from(result.DatiOutputRichiesta.esitiNegativi.dettagliEsitoNegativo.csv, 'base64');
                               
                                    res.writeHead(200, {
                                        'Content-Disposition': 'attachment; filename='+protocollo_+'_errori.zip',
                                        'Content-Length': csv.length
                                    });

                                    res.end(csv);
                                }
                              
                                

                            });

                        });
                    }else
                    {
                        res.status(404).send();
                    }
                            
                }

            }).catch(function(err) {
                return res.status(400).render('invoices_ts_send_ids: ' + err);
            });
        }else
        {
            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
        }
    
    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.ts_xml = function(req, res){
    var id = null;
    var util = require('util');
    var tsXml = require('../utility/ts_xml.js');
    
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.query.id !== undefined && req.query.id != '') {
        id = req.query.id;
    }

    if (req.query.tipo_spesa !== undefined && req.query.tipo_spesa != '') {
            tipo_spesa = req.query.tipo_spesa.substring(0, 2);
    }

    if (req.query.operazione !== undefined && req.query.operazione != '') {
        operazione = req.query.operazione;
    }

    sequelize.query('SELECT get_ts_system_data_from_user(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+','+
        req.user.id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(tss) {
        var ts_datas = {};

         if(tss && tss[0].get_ts_system_data_from_user.ts_username){
            ts_datas.username = tss[0].get_ts_system_data_from_user.ts_username;
            ts_datas.pwd = tss[0].get_ts_system_data_from_user.ts_pwd;
            ts_datas.pinCode = tss[0].get_ts_system_data_from_user.ts_pincode;
            ts_datas.struttura = tss[0].get_ts_system_data_from_user.ts_struttura;
            ts_datas.codRegione = tss[0].get_ts_system_data_from_user.ts_codregione;
            ts_datas.codAsl = tss[0].get_ts_system_data_from_user.ts_codasl;
            ts_datas.codSSA = tss[0].get_ts_system_data_from_user.ts_codssa;
            ts_datas.cod_fiscale = tss[0].get_ts_system_data_from_user.ts_cod_fiscale;
            
            
            sequelize.query('SELECT invoices_ts_send_ids(\'' +
                req.headers['host'].split(".")[0] + '\','+
                req.user._tenant_id+','+
                req.user.id+','+
                id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if(datas[0].invoices_ts_send_ids && datas[0].invoices_ts_send_ids.length > 0){

                    tsXml.generaTsBase(ts_datas, datas[0].invoices_ts_send_ids[0], tipo_spesa, operazione, function(err, response) {

                        var xmlData = tsXml.generaXmlTs(response);
                       
                        res.writeHead(200, {
                            'Content-Type': 'text/xml; charset=UTF-8',
                            'Content-Disposition': 'attachment; filename='+req.headers['host'].split(".")[0] +'_'+(datas[0].invoices_ts_send_ids[0].attributes.number ? datas[0].invoices_ts_send_ids[0].attributes.number.replace(/[^a-zA-Z0-9.]/g, '') : '')+'_'+moment().format('DD_MM_YYYY_HH_mm')+'.xml',
                            'Content-Length': xmlData.length+1
                        });

                        res.end(xmlData);
                       
                    });
                    
                }else{
                    res.status(404).send();
                }
              

            }).catch(function(err) {
                return res.status(400).render('invoices_ts_send_ids: ' + err);
            });

        }else
        {
            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
        }

    }).catch(function(err) {
        return res.status(400).render('get_ts_system_data_from_user: ' + err);
    });  
};

exports.sistema_ts = function(req, res){
    var document_type = null,
        contact_id = null,
        utente_id = null,
        q1 = [],
        mese = null,
        anno = null,
        status = null;

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        contact_id = req.query.contact_id;
    }

    if (req.query.utente_id !== undefined && req.query.utente_id != '') {
        utente_id = req.query.utente_id;
    }else
    {
        return res.status(422).send(utility.error422('utente_id', 'Manca l\'utente di riferimento', 'Non risulta associato alcun account', '422'));
    }

    if (req.query.anno !== undefined && req.query.anno != '') {
        anno = req.query.anno;
    }else
    {
        return res.status(422).send(utility.error422('anno', 'Manca l\'anno di riferimento', 'Non risulta associato alcun account', '422'));
    }

    if (req.query.mese !== undefined && req.query.mese != '') {
        mese = req.query.mese;
    }

    if (req.query.mese !== undefined && req.query.mese != '') {
        mese = req.query.mese;
    }

    if (req.query.status !== undefined && req.query.status != '') {
        status = req.query.status;
    }

     if (req.query.document_type !== undefined && req.query.document_type != '') {
        document_type = '\''+req.query.document_type+'\'';
    }


    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            return res.status(422).send(utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {
            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {

                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

    sequelize.query('SELECT get_sistema_ts_new('+
        req.user._tenant_id+',\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user.id+','+
        req.user.role_id+','+
        utente_id+','+
        anno+','+
        contact_id+','+
        document_type+','+
        (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) + ','+
        mese+','+
        status+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
       
        res.status(200).send(datas[0].get_sistema_ts_new[0]);

    }).catch(function(err) {
        return res.status(400).render('get_sistema_ts_new: ' + err);
    });  
};

exports.dashboard_ts = function(req, res){
    var util = require('util');

    var document_type = null,
        contact_id = null,
        utente_id = null,
        q1 = [],
        mese = null,
        anno = null;

  
    if (!req.user.role[0].json_build_object.attributes.invoice_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.query.contact_id !== undefined && req.query.contact_id != '') {
        contact_id = req.query.contact_id;
    }

    if (req.query.utente_id !== undefined && req.query.utente_id != '') {
        utente_id = req.query.utente_id;
    }else
    {
        return res.status(422).send(utility.error422('utente_id', 'Manca l\'utente di riferimento', 'Non risulta associato alcun account', '422'));
    }

    if (req.query.anno !== undefined && req.query.anno != '') {
        anno = req.query.anno;
    }else
    {
        return res.status(422).send(utility.error422('anno', 'Manca l\'anno di riferimento', 'Non risulta associato alcun account', '422'));
    }

    if (req.query.mese !== undefined && req.query.mese != '') {
        mese = req.query.mese;
    }

    if (req.query.document_type !== undefined && req.query.document_type != '') {
        document_type = '\''+req.query.document_type+'\'';
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            return res.status(422).send(utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {

                q1.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

    sequelize.query('SELECT get_dashboard_ts('+
        req.user._tenant_id+',\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user.id+','+
        req.user.role_id+','+
        utente_id+','+
        anno+','+
        contact_id+','+
        document_type+','+
        (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) + ','+
        mese+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
       
        res.status(200).send(datas[0].get_dashboard_ts[0]);

    }).catch(function(err) {
        return res.status(400).render('get_dashboard_ts: ' + err);
    });  
};

exports.user_ts = function(req, res){
    var util = require('util');

  
    if (!req.user.role[0].json_build_object.attributes.contact_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT get_users_ts('+
        req.user._tenant_id+',\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user.id+','+
        req.user.role_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
       
        res.status(200).send(datas[0].get_users_ts[0]);

    }).catch(function(err) {
        return res.status(400).render('get_users_ts: ' + err);
    });  
};

exports.ts_corretta = function(req, res){
   
    if (!req.user.role[0].json_build_object.attributes.contact_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT ts_corretta('+
        req.user._tenant_id+',\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user.id+','+
        req.user.role_id+','+
        req.body.invoice_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
       
       if(datas && datas[0] && datas[0].ts_corretta && datas[0].ts_corretta != -1){
            res.status(200).send(datas[0].ts_corretta);
       }else
       {
            res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
       }
        

    }).catch(function(err) {
        return res.status(400).render('ts_corretta: ' + err);
    });  
};



function controllo_attesa (ts_datas, org, _ten, user_id, id){
    var tsXml = require('../utility/ts_xml.js');
    sequelize.query('SELECT invoices_ts_protocol(\'' +
        org + '\','+
        _ten+','+
        user_id+','+
        id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {

        if(datas[0].invoices_ts_protocol){

           var protocollo_ = datas[0].invoices_ts_protocol.protocollo;

            if(protocollo_){
                    

                    var soap = require('soap');

                    var url =  'https://asset.beebeeboard.com/utility/ts_system/EsitoInvioDatiSpesa730Service.wsdl';
                
                    soap.createClient(url, function(err, client) {

                        client.setSecurity(new soap.BasicAuthSecurity(ts_datas.username, ts_datas.pwd));

                        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

                        var args = {
                            DatiInputRichiesta: {
                                pinCode: tsXml.CodificaStringa(ts_datas.pinCode),
                                protocollo: protocollo_
                            }
                        };

                        var res1 = client.EsitoInvii(args, function(err, result) {
                            console.log('Errore file TS ' + err);
                            console.log('Res file TS ');
                            console.log(result);

                            sequelize.query('SELECT update_invoice_ts_ric_response(' +
                                _ten+',\''+
                                org + '\','+
                                user_id+','+
                                id+',\''+
                                JSON.stringify(result).replace(/\\"/g, "").replace(/'/g, "''''") + '\',\''+
                                protocollo_+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(datas) {
                                return 0;
                            }).catch(function(err) {
                                console.log(err);
                                return -1
                            });
                        });

                    });
                }else
                {
                    return -1;
                }
                    
        }

    }).catch(function(err) {
        console.log(err);
        return -1;
    });
}

exports.ts_storico = function(req, res){


    if (!req.user.role[0].json_build_object.attributes.invoice_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT get_ts_storico_fattura(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' + req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            

           if(datas && datas[0] && datas[0].get_ts_storico_fattura && datas[0].get_ts_storico_fattura[0] && datas[0].get_ts_storico_fattura[0].storico){
                res.status(200).send(datas[0].get_ts_storico_fattura[0].storico);
           }else
           {
                res.status(200).send({});
           }

                

        }).catch(function(err) {
            return res.status(500).send(err);
        });
  
}
