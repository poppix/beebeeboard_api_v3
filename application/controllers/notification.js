var moment = require('moment'),
    Sequelize = require('sequelize');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    /*
     	var async = require('async'), Contact = mongoose.model('Contact'), User = mongoose.model('User'), Product =mongoose.model('Product');

     	var searchObj = {
     		'_tenant_id' : req.user._tenant_id,
     		'to_id' : req.user.id,
     		'organization': req.headers['host'].split(".")[0]
     	}, sortField = (req.query.sort === undefined) ? 'date' : req.query.sort, sortDirection = (req.query.direction === undefined) ? 'desc' : req.query.direction, sortObj = {}, page_num = 1, page_count = 50;

     	sortObj[sortField] = sortDirection;

     	if (req.query.date !== undefined && req.query.date !== '') {
     		searchObj['date'] = {};
     		searchObj['date']['$gte'] = req.query.date.split(" ")[0] + ' 00:00:00';
     		searchObj['date']['$lte'] = req.query.date.split(" ")[0] + ' 23:59:59';
     	}

     	if (req.query.pag !== undefined && req.query.pag !== '') {
     		page_num = parseInt(req.query.pag);
     	}
     	if (req.query.from_id !== undefined && req.query.from_id !== '') {
     		searchObj['from_id'] = req.query.from_id;
     	}
     	if (req.query.xpag !== undefined && req.query.xpag !== '') {
     		page_count = parseInt(req.query.xpag);
     	}
     	if (req.query.read !== undefined && req.query.read !== '') {
     		searchObj['read'] = req.query.read;
     	}
     	
     	if (req.query.id !== undefined && req.query.id !== '') {
     		searchObj['_id'] = req.query.id;
     	}
     	
     	Notification.paginate(searchObj, page_num, page_count, function(error, pageCount, paginatedResults, itemCount) {
     		if (error) {
     			return res.status(500).send(error);
     		} else {
     			var contacts = [];
     			var users = [];
     			var products = [];
     			
     			for(var i =0; i<paginatedResults.length; i++){
     				
     				if(paginatedResults[i].to_id){
     					users.push(paginatedResults[i].to_id);				
     				}
     				if(paginatedResults[i].contact_ids){
     					for(var j = 0; j< paginatedResults[i].contact_ids.length; j++){
     						contacts.push(paginatedResults[i].contact_ids[j]);			
     					}
     				}
     				if(paginatedResults[i].product_ids){
     					for(var j = 0; j< paginatedResults[i].product_ids.length; j++){
     						products.push(paginatedResults[i].product_ids[j]);			
     					}
     				}
     			}
     			
     			async.parallel({
     				contacts: function(callback) { 
     					Contact.find({
     						'_id' : {
     							'$in' : contacts
     						}
     					},'_id name', function(err, all_suppliers){
     						if (err) {
     							return res.status(500).send(err);
     						}
     						callback(null, all_suppliers);
     						
     					});
     				},
     				users: function(callback) { 
     					User.find({
     						'_id' : {
     							'$in' : users
     						}
     					},'_id name', function(err, all_users){
     						if (err) {
     							return res.status(500).send(err);
     						}
     						callback(null, all_users);
     						
     					});
     				},
     				products: function(callback) { 
     					Product.find({
     						'_id' : {
     							'$in' : products
     						}
     					},'_id description', function(err, all_products){
     						if (err) {
     							return res.status(500).send(err);
     						}
     						callback(null, all_products);
     						
     					});
     				}
     			}, function(err, results) {
     				var metastring = {
     					'total_items' : itemCount,
     					'actual_page' : page_num,
     					'total_pages' : pageCount,
     					'item_per_page' : page_count
     				};
     				res.json({
     					notifications : paginatedResults,
     					contacts : results.contacts,
     					users : results.users,
     					products : results.products,
     					meta : metastring
     				});
     				
     			});

     		}
     	}, {
     		columns: (req.query.fields != undefined && req.query.fields !== '') ? '_id ' + req.query.fields.replace(/,/g,' ') : '_id',
     		sortBy : sortObj
     	});*/
    res.status(200).send({});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific notification searched by id
 */
exports.find = function(req, res) {
    /*
 	Notification.findOne({
 		'_tenant_id' : req.user._tenant_id,
 		'_id' : req.params.id,
 		'organization': req.headers['host'].split(".")[0]
 	}, function(err, notification) {
 		if (err) {
 			return res.status(500).send(err);
 		}
 		if(!notification)
 		{
 			 return res.status(401).send({
        'status':'401',
        'errors': [{
          'status': '401',
          'title': 'Non hai i diritti',
          'detail': 'Non hai i diritti'
        }]
      });
 		}
 		res.json({
 			notification : notification
 		});
 	});*/
    res.status(200).send({});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing notification searched by id
 */
exports.update = function(req, res) {
    /*
    Notification.findOneAndUpdate({
    	'_tenant_id' : req.user._tenant_id,
    	'_id' : req.params.id,
    	'organization': req.headers['host'].split(".")[0]
    }, req.body.notification,{'new':true},  function(err, notification) {
    	if (err) {
    		return res.status(500).send(err);
    	}

    	res.json({
    		notification : notification
    	});
    });*/
    res.status(200).send({});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing notification searched by id
 */
exports.destroy = function(req, res) {
    /*
    Notification.findOneAndRemove({
    	'_tenant_id' : req.user._tenant_id,
    	'_id' : req.params.id,
    	'organization': req.headers['host'].split(".")[0]
    }, function(err, notification) {
    	if (err) {
    		return res.status(500).send(err);
    	}
    	res.status(200).send({});
    });*/
    res.status(200).send({});
};