var Sequelize = require('sequelize'),
    async = require('async'),
    moment = require('moment'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    export_datas = require('../utility/exports.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js');

exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.workhour_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('workhour', req, function(err, results) {
        if (err) {
            return res.status(400).render('workhour_custom: ' + err);
        }
        indexes.workhour(req, results, function(err, index) {
            if (err) {
                return res.status(400).render('workhour_index: ' + err);
            }
           /* if (index.meta.total_items > 5000) {
                return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
            } else {*/

                export_datas.workhour(req, results, function(err, response) {
                    if (err) {
                        return res.status(400).render('workhour_export: ' + err);
                    }

                    json2csv({
                        data: response.workhours,
                        fields: response.fields,
                        fieldNames: response.field_names
                    }, function(err, csv) {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=workhours.csv'
                        });
                        res.end(csv);
                    });
                });
          //  }
        });

    });
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.workhour_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('workhour', req, function(err, results) {
        indexes.workhour(req, results, function(err, response) {
            if (err) {
                return res.status(400).render('workhour_index: ' + err);
            }
            res.json(response);
        });

    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific workhour searched by id
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.workhour_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var sortField = (req.query.sort === undefined) ? 'text' : req.query.sort,
        sortDirection = (req.query.direction === undefined) ? 'asc' : req.query.direction;

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_workhour(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New workhour
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.workhour_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var workhour = {};

    if (req.body.data.attributes !== undefined) {
        // Import case
        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        workhour['owner_id'] = req.user.id;
        workhour['date'] = req.body.data.attributes.date;
        workhour['duration'] = req.body.data.attributes.duration;
        workhour['text'] = (req.body.data.attributes.text && utility.check_type_variable(req.body.data.attributes.text, 'string') && req.body.data.attributes.text !== null ? req.body.data.attributes.text.replace(/'/g, "''''") : '');
        workhour['_tenant_id'] = req.user._tenant_id;
        workhour['organization'] = req.headers['host'].split(".")[0];
        workhour['mongo_id'] = null;
        workhour['workhour_status_id'] = ((req.body.data.relationships && req.body.data.relationships.workhour_state && req.body.data.relationships.workhour_state.data && req.body.data.relationships.workhour_state.data.id && !utility.check_id(req.body.data.relationships.workhour_state.data.id)) ? req.body.data.relationships.workhour_state.data.id : null);
        workhour['notes'] = (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
        workhour['archivied'] = req.body.data.attributes.archivied;

        sequelize.query('SELECT insert_workhour_v3(\'' +
                JSON.stringify(workhour) + '\',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].insert_workhour_v3 && datas[0].insert_workhour_v3 != null) {
                    req.body.data.id = datas[0].insert_workhour_v3;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_workhour_v3,
                        'body': req.body.data.relationships,
                        'user_id': req.user.id,
                        'model': 'workhour',
                        'array': ['address', 'contact', 'product', 'project', 'event', 'related_worhour', 'custom_field']
                    }, function(err, results) {

                        sequelize.query('SELECT workhour_custom_fields_trigger(' +
                                datas[0].insert_workhour_v3 + ',' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                })
                            .then(function(cus) {

                                req.params.id = datas[0].insert_workhour_v3;
                                finds.find_workhour(req, function(err, results) {


                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'workhour',
                                        'insert',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        datas[0].insert_workhour_v3,
                                        'Aggiunta workhour ' + datas[0].insert_workhour_v3
                                    );

                                    utility.save_socket(req.headers['host'].split(".")[0], 'workhour', 'insert', req.user.id, datas[0].insert_workhour_v3, results.data);
                

                                    res.json(results);
                                });

                            }).catch(function(err) {
                                console.log(err);
                                return res.status(400).render('workhour_create: ' + err);
                            });
                    });

                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }
            }).catch(function(err) {
                console.log(err);
                return res.status(400).render('workhour_insert: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing workhour searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.workhour_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var workhour = {};

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined) {

        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        finds.find_workhour(req, function(err, old_workhour) {

            sequelize.query('SELECT update_workhour_v3(' +
                    req.params.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    (req.body.data.attributes.date !== null ? '\'' + req.body.data.attributes.date + '\'' : null) + ',' +
                    req.body.data.attributes.duration + ',\'' +
                    (req.body.data.attributes.text !== null ? req.body.data.attributes.text.replace(/'/g, "''''") : '') + '\',' +
                    ((req.body.data.relationships.workhour_state && req.body.data.relationships.workhour_state.data && req.body.data.relationships.workhour_state.data.id) ? req.body.data.relationships.workhour_state.data.id : null) + ',' +
                    req.user.role_id + ',' + req.user.id + ',\'' +
                    (req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\',' +
                    req.body.data.attributes.archivied + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(work) {
                    if (!work[0].update_workhour_v3) {
                        return res.status(404).send(utility.error404('id', 'Nessun\'ora lavorata trovata', 'Nessun\'ora lavorata trovata', '404'));
                    }

                    var old = old_workhour;
                    sequelize.query('SELECT delete_workhour_relations_v1(' +
                            req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {

                            utility.sanitizeRelations({
                                'organization': req.headers['host'].split(".")[0],
                                '_tenant': req.user._tenant_id,
                                'new_id': req.params.id,
                                'body': req.body.data.relationships,
                                'model': 'workhour',
                                'user_id': req.user.id,
                                'array': ['address', 'event']
                            }, function(err, results) {

                                finds.find_workhour(req, function(err, response) {
                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'workhour',
                                        'update',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        req.params.id,
                                        'Modificato workhour ' + req.params.id
                                    );

                                    utility.save_socket(req.headers['host'].split(".")[0], 'workhour', 'update', req.user.id, req.params.id, response.data);
                
                                    res.json(response);
                                });

                            });
                        }).catch(function(err) {
                            return res.status(400).render('workhour_update: ' + err);
                        });

                }).catch(function(err) {
                    return res.status(400).render('workhour_update: ' + err);
                });
        });
    } else {
        res.json({
            'data': []
        });
    }
};

exports.update_all = function(req, res) {
    var util = require('util');

    if(!util.isArray(req.body)){
        sequelize.query('SELECT update_workhour_all_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_workhour_all_v1'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_workhour_all_v1']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_workhour_all_v1'];
                finds.find_workhour(req, function(err, results) {
                    if (err) {
                        return res.status(err.errors[0].status).send(err);
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'workhour', 'update', req.user.id, req.params.id, results.data);
                
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
    }else{
        sequelize.query('SELECT update_workhour_all_array(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_workhour_all_array'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_workhour_all_array']) {
                        
                res.status(200).send({});
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing workhour searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.workhour_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT delete_workhour_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(!datas[0].delete_workhour_v1){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }else{

                utility.save_socket(req.headers['host'].split(".")[0], 'workhour', 'delete', req.user.id, req.params.id, null);
                        
                res.json({
                    'data': {
                        'type': 'workhours',
                        'id': datas[0].delete_workhour_v1
                    }
                });
            }

        }).catch(function(err) {
            return res.status(400).render('workhour_destroy: ' + err);
        });


};