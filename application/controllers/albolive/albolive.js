var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js'),
    util = require('util');


exports.find_atto = function(req, res) {

    
        sequelize.query('SELECT albolive_find_atto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            (req.body.product_id !== undefined ? req.body.product_id : null) + ',' +
            (req.body.notes && req.body.notes !== null && utility.check_type_variable(req.body.notes, 'string') ? '\'' + req.body.notes.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.end_date && req.body.end_date !== null && utility.check_type_variable(req.body.end_date, 'string') ? '\'' + req.body.end_date.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.start_date && req.body.start_date !== null && utility.check_type_variable(req.body.start_date, 'string') ? '\'' + req.body.start_date.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.mongo_id && req.body.mongo_id !== null && utility.check_type_variable(req.body.mongo_id, 'string') ? '\'' + req.body.mongo_id.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.description && req.body.description !== null && utility.check_type_variable(req.body.description, 'string') ? '\'' + req.body.description.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.title && req.body.title !== null && utility.check_type_variable(req.body.title, 'string') ? '\'' + req.body.title.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.color && req.body.color !== null && utility.check_type_variable(req.body.color, 'string') ? '\'' + req.body.color.replace(/'/g, "''''") + '\'' : null) + ',' +
            (req.body.updated_at && req.body.updated_at !== null && utility.check_type_variable(req.body.updated_at, 'string') ? '\'' + req.body.updated_at.replace(/'/g, "''''") + '\'' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.json({
                'res': data[0].albolive_find_atto
            });

        }).catch(function(err) {
            return res.status(400).render('albolive_find_atto: ' + err);
        });
    

    
};

exports.albolive_salva_testo = function(req, res) {


    if(req.body && req.body.id){
        sequelize.query('SELECT albolive_salva_testo_file(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.body.id + ',' +
            (req.body.testo_pdf && req.body.testo_pdf !== null && utility.check_type_variable(req.body.testo_pdf, 'string') ? '\'' + req.body.testo_pdf.replace(/'/g, "''''") + '\'' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.json({
                'res': data[0].albolive_salva_testo_file
            });

        }).catch(function(err) {
            return res.status(400).render('albolive_salva_testo_file: ' + err);
        });
    }
    else
    {
        res.status(404).json({});
    }
    
};


exports.index_enti = function(req, res) {


    sequelize.query('SELECT albolive_index_enti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.json({
                'data': data[0].albolive_index_enti
            });

        }).catch(function(err) {
            return res.status(400).render('albolive_index_enti: ' + err);
        });
};

exports.dashboard = function(req, res) {


    sequelize.query('SELECT albolive_dashboard(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.json({
                'data': data[0].albolive_dashboard
            });

        }).catch(function(err) {
            return res.status(400).render('albolive_dashboard: ' + err);
        });
};

exports.index_atti = function(req, res) {

    var color = [],
        updated_at = null,
        fine_atto = null,
        description = null,
        title = null,
        mongo_id = null,
        sortfield1 = '\'data_atto\'',
        sortdirection1 = '\'desc\'',
        sortfield3 = null,
        sortdirection3 = null,
        sortfield4 = null,
        sortdirection4 = null,
        sortfield4 = null,
        sortdirection4 = null,
        sortfield5 = null,
        sortdirection5 = null,
        sortfield2 = null,
        sortdirection2 = null,
        q_atto = [];

    if (req.query.q_atto && req.query.q_atto !== undefined && req.query.q_atto !== '') {
        if (!utility.check_type_variable(req.query.q_atto, 'string')) {
            callback_(null, utility.error422('q_atto', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q_atto !== undefined && req.query.q_atto !== '') {

            var q_array = req.query.q_atto.split(' ');
            q_array.forEach(function(s) {
                q_atto.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }
    }

    if (req.query.albo_categ && req.query.albo_categ !== undefined && req.query.albo_categ != '') {
        if (util.isArray(req.query.albo_categ)) {
            req.query.albo_categ.forEach(function(s) {
                color.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }
    }


    if (!req.user.role[0].json_build_object.attributes.event_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.query.updated_at && req.query.updated_at !== undefined && req.query.updated_at !== '') {
        if (!moment(req.query.updated_at).isValid()) {
            return res.status(422).send(utility.error422('updated_at', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        updated_at = '\'' + req.query.updated_at + '\'';
    }

    if (req.query.fine_atto && req.query.fine_atto !== undefined && req.query.fine_atto !== '') {
        if (!moment(req.query.fine_atto).isValid()) {
            return res.status(422).send(utility.error422('fine_atto', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        fine_atto = '\'' + req.query.fine_atto + '\'';
    }

    if (req.query.description && req.query.description !== undefined && req.query.description !== '') {
        if (!utility.check_type_variable(req.query.description, 'string')) {
            return res.status(422).send(utility.error422('description', 'Parametro non valido', 'Parametro non valido', '422'));

        }

        description = '\'' + req.query.description + '\'';
    }

    if (req.query.title && req.query.title !== undefined && req.query.title !== '') {
        if (!utility.check_type_variable(req.query.title, 'string')) {
            return res.status(422).send(utility.error422('title', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        title = '\'' + req.query.title + '\'';
    }

    if (req.query.mongo_id && req.query.mongo_id !== undefined && req.query.mongo_id !== '') {
        if (!utility.check_type_variable(req.query.mongo_id, 'string')) {
            return res.status(422).send(utility.error422('mongo_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        mongo_id = '\'' + req.query.mongo_id + '\'';
    }

    if (req.query.sort_1 && req.query.sort_1 !== undefined && req.query.sort_1 !== '') {
        if (!utility.check_type_variable(req.query.sort_1, 'string')) {
            return res.status(422).send(utility.error422('sort_1', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortfield1 = '\'' + req.query.sort_1 + '\'';
    }

    if (req.query.direction_1  && req.query.direction_1 !== undefined && req.query.direction_1 !== '') {
        if (!utility.check_type_variable(req.query.direction_1, 'string')) {
            return res.status(422).send(utility.error422('direction_1', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortdirection1 = '\'' + req.query.direction_1 + '\'';
    }

    if (req.query.sort_2 && req.query.sort_2 !== undefined && req.query.sort_2 !== '') {
        if (!utility.check_type_variable(req.query.sort_2, 'string')) {
            return res.status(422).send(utility.error422('sort_2', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortfield2 = '\'' + req.query.sort_2 + '\'';
    }

    if (req.query.direction_2 && req.query.direction_2 !== undefined && req.query.direction_2 !== '') {
        if (!utility.check_type_variable(req.query.direction_2, 'string')) {
            return res.status(422).send(utility.error422('direction_2', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortdirection2 = '\'' + req.query.direction_2 + '\'';
    }

    if (req.query.sort_3 && req.query.sort_3 !== undefined && req.query.sort_3 !== '') {
        if (!utility.check_type_variable(req.query.sort_3, 'string')) {
            return res.status(422).send(utility.error422('sort_3', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortfield3 = '\'' + req.query.sort_3 + '\'';
    }

    if (req.query.direction_3 && req.query.direction_3 !== undefined && req.query.direction_3 !== '') {
        if (!utility.check_type_variable(req.query.direction_3, 'string')) {
            return res.status(422).send(utility.error422('direction_3', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortdirection3 = '\'' + req.query.direction_3 + '\'';
    }

    if (req.query.sort_4 && req.query.sort_4 !== undefined && req.query.sort_4 !== '') {
        if (!utility.check_type_variable(req.query.sort_4, 'string')) {
            return res.status(422).send(utility.error422('sort_4', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortfield4 = '\'' + req.query.sort_4 + '\'';
    }

    if (req.query.direction_4 && req.query.direction_4 !== undefined && req.query.direction_4 !== '') {
        if (!utility.check_type_variable(req.query.direction_4, 'string')) {
            return res.status(422).send(utility.error422('direction_4', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortdirection4 = '\'' + req.query.direction_4 + '\'';
    }

    if (req.query.sort_5 && req.query.sort_5 !== undefined && req.query.sort_5 !== '') {
        if (!utility.check_type_variable(req.query.sort_5, 'string')) {
            return res.status(422).send(utility.error422('sort_5', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortfield5 = '\'' + req.query.sort_5 + '\'';
    }

    if (req.query.direction_5 && req.query.direction_5 !== undefined && req.query.direction_5 !== '') {
        if (!utility.check_type_variable(req.query.direction_5, 'string')) {
            return res.status(422).send(utility.error422('direction_5', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        sortdirection5 = '\'' + req.query.direction_5 + '\'';
    }

    utility.customs('event', req, function(err, results) {

        utility.get_parameters(req, 'event', function(err, parameters) {
            if (err) {
                return res.status(400).render('index_atti: ' + err);
            }

            var response = {};
            sequelize.query('SELECT albolive_atti_data_v1(' +
                    parameters.page_count + ',' +
                    (parameters.page_num - 1) * parameters.page_count + ',' +
                    req.user._tenant_id + ',' +
                    parameters.start_date + ',' +
                    parameters.end_date + ',' +
                    parameters.sortDirection + ',' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                    (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                    parameters.sortField + ',' +
                    req.user.role_id + ',' +
                    parameters.archivied + ',' +
                    parameters.notes + ',' +
                    (color && color.length > 0 ? 'ARRAY[' + color + ']' : null) + ',' +
                    updated_at + ',' +
                    title + ',' +
                    mongo_id + ',' +
                    description + ',' +
                    (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                    (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                    fine_atto + ',' +
                    sortfield1 + ',' +
                    sortdirection1 + ',' +
                    sortfield2 + ',' +
                    sortdirection2 + ',' +
                    sortfield3 + ',' +
                    sortdirection3 + ',' +
                    sortfield4 + ',' +
                    sortdirection4 + ',' +
                    sortfield5 + ',' +
                    sortdirection5 + ',' +
                    (req.query.last ? req.query.last : false) + ',' +
                    (q_atto && q_atto.length > 0 ? 'ARRAY[' + q_atto + ']' : null) + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {

                    async.parallel({

                        data: function(callback) {
                            sequelize.query('SELECT all_albolive_atti_v1(' +
                                    parameters.page_count + ',' +
                                    (parameters.page_num - 1) * parameters.page_count + ',' +
                                    req.user._tenant_id + ',' +
                                    parameters.start_date + ',' +
                                    parameters.end_date + ',' +
                                    parameters.sortDirection + ',' +
                                    req.user.id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    (results.custom && results.custom.length > 0 ? 'ARRAY[' + results.custom + ']::bigint[]' : ((!results.custom || results.custom.length == 0) && req.query.custom !== undefined && req.query.custom !== '' ? 'ARRAY[0]::bigint[]' : null)) + ',' +
                                    (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                    parameters.sortField + ',' +
                                    req.user.role_id + ',' +
                                    parameters.archivied + ',' +
                                    parameters.notes + ',' +
                                    (color && color.length > 0 ? 'ARRAY[' + color + ']' : null) + ',' +
                                    updated_at + ',' +
                                    title + ',' +
                                    mongo_id + ',' +
                                    description + ',' +
                                    (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                    (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                    fine_atto + ',' +
                                    (req.query.last ? req.query.last : false) + ',' +
                                    (q_atto && q_atto.length > 0 ? 'ARRAY[' + q_atto + ']' : null) + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT
                                    })
                                .then(function(counter) {
                                    if (datas[0].albolive_atti_data_v1 && datas[0].albolive_atti_data_v1 != null) {

                                        var response = {
                                            'data': datas[0].albolive_atti_data_v1,
                                            'meta': {
                                                'total_items': parseInt((counter[0].all_albolive_atti_v1.tot_items ? counter[0].all_albolive_atti_v1.tot_items : 0)),
                                                'actual_page': parseInt(parameters.page_num),
                                                'total_pages': parseInt(counter[0].all_albolive_atti_v1.tot_items / parameters.page_count) + 1,
                                                'item_per_page': parseInt(parameters.page_count),
                                                'tot_all_number': parseInt(counter[0].all_albolive_atti_v1.tot_items_tutti)
                                            }
                                        };
                                        callback(null, response);
                                    } else {

                                        response = {
                                            'data': [],
                                            'meta': {
                                                'total_items': parseInt((counter[0].all_albolive_atti_v1.tot_items ? counter[0].all_albolive_atti_v1.tot_items : 0)),
                                                'actual_page': parseInt(parameters.page_num),
                                                'total_pages': parseInt(counter[0].all_albolive_atti_v1 / parameters.page_count) + 1,
                                                'item_per_page': parseInt(parameters.page_count),
                                                'tot_all_number': parseInt(counter[0].all_albolive_atti_v1.tot_items_tutti)
                                            }
                                        };
                                        callback(null, response);
                                    }
                                }).catch(function(err) {
                                    callback(err, null)
                                });

                        }

                    }, function(err, result) {
                        if (err) {
                            return res.status(400).render('index_atti: ' + err);
                        }

                        if (result.data.length == 0) {
                            return res.status(200).send(utility.error403('Nessun atto trovato', 'Nessun atto trovato', '404'));
                        }

                        var response = {
                            'data': result.data.data,
                            'meta': result.data.meta
                        };

                        res.json(response);

                    });
                }).catch(function(err) {
                    console.log(err);
                    return res.status(400).render('index_atti: ' + err);
                });
        });
    });

};