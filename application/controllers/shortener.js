var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    base58 = require('../utility/base58.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific shortener searched by id
 */
exports.find = function(req, res) {

    if (typeof req.params.id === 'undefined') {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    var base58Id = req.params.id,
        id = base58.decode(base58Id),
        param = null;

    if (req.query.param !== undefined && req.query.param !== '') {
        param = req.query.param;
    }

    if (utility.check_id(id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT find_shortener(' + id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0] != null && datas[0].find_shortener != null && datas[0].find_shortener.long_url != null) {
                if (datas[0].find_shortener.long_url.indexOf('?') != -1) {
                    res.redirect(datas[0].find_shortener.long_url + '&param=' + param);
                } else {
                    res.redirect(datas[0].find_shortener.long_url + '?param=' + param);
                }

            } else {
                res.redirect('https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com');
            }
        }).catch(function(err) {
            return res.status(400).render('find_shortener: ' + err);
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Create an new shortener
 */
exports.create = function(req, res) {
    var longUrl = req.body.url,
        shortUrl = '';

    if (longUrl !== undefined) {
        sequelize.query('SELECT insert_shortener(\'' +
                longUrl + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(datas[0].insert_shortener);

                res.json({
                    'short_url': shortUrl
                });

            }).catch(function(err) {
                return res.status(400).render('tag_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};