var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    async = require('async'),
    logs = require('../utility/logs.js');


exports.start_collaboration = function(req, res) {
 
    sequelize.query('SELECT insert_partner('+
        req.user.id+
        ',\''+req.headers['host'].split(".")[0]+'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(datas) {
        res.status(200).send();
    }).catch(function(err) {
        return res.status(400).render('start_collaboration: ' + err);
    });

};

exports.partner_info = function(req, res) {
 
    sequelize.query('SELECT get_partner('+
        req.user.id+
        ',\''+req.headers['host'].split(".")[0]+'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].get_partner && datas[0].get_partner[0] && datas[0].get_partner[0].partner && datas[0].get_partner[0].partner[0]){
            res.status(200).send(datas[0].get_partner[0].partner[0]);
        }else{
            res.status(200).send({});
        }
        
    }).catch(function(err) {
        return res.status(400).render('get_partner: ' + err);
    });

};