var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    util = require('util'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');

exports.medtrack_basivita_charts = function(req, res) {
    var base_vita_id = null,
        contact_id = null;


    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if(req.query.contact_id && req.query.contact_id !== undefined){
        contact_id = req.query.contact_id;
    }

    if(req.query.product_id && req.query.product_id !== undefined){
        base_vita_id = req.query.product_id;
    }

    sequelize.query('SELECT medtrack_basivita_charts(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            req.user.id + ','+
            req.user.role_id+','+
            contact_id+','+
            base_vita_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].medtrack_basivita_charts[0] && datas[0].medtrack_basivita_charts[0].categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].medtrack_basivita_charts[0].categorie.forEach(function(cat) {
                    if (!cat.description)
                        cat.description = 'Non categorizzato';

                    categorie[cat.description] = [];
                    categorie[cat.description] = cat.array_agg;
                    //categorie[cat.description].push(cat.tot);
                    if (cnt++ >= datas[0].medtrack_basivita_charts[0].categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('medtrack_basivita_charts: ' + err);
        });
};

exports.medtrack_charts = function(req, res) {
    var base_vita_id = null,
        contact_id = null;


    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if(req.query.contact_id && req.query.contact_id !== undefined){
        contact_id = req.query.contact_id;
    }

    if(req.query.product_id && req.query.product_id !== undefined){
        base_vita_id = req.query.product_id;
    }

    sequelize.query('SELECT medtrack_chart_tt(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            req.user.id + ','+
            req.user.role_id+','+
            contact_id+','+
            base_vita_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].medtrack_chart_tt[0] && datas[0].medtrack_chart_tt[0].trattamenti) {
                var cnt = 0;
                var categorie = {};
                datas[0].medtrack_chart_tt[0].trattamenti.forEach(function(cat) {
                    if (!cat.description)
                        cat.description = 'Non categorizzato';

                    categorie[cat.description] = [];
                    categorie[cat.description].push(cat.tot);
                    if (cnt++ >= datas[0].medtrack_chart_tt[0].trattamenti.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('medtrack_chart_tt: ' + err);
        });
};

exports.medtrack_charts_tot = function(req, res) {
    var base_vita_id = null,
        contact_id = null;


    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if(req.query.contact_id && req.query.contact_id !== undefined){
        contact_id = req.query.contact_id;
    }

    if(req.query.product_id && req.query.product_id !== undefined){
        base_vita_id = req.query.product_id;
    }

    sequelize.query('SELECT medtrack_chart_tot(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            req.user.id + ','+
            req.user.role_id+','+
            contact_id+','+
            base_vita_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            var t = {
                'name': 'medtrack',
                'children':[]
            };

            

            if (datas && datas[0] && datas[0].medtrack_chart_tot && datas[0].medtrack_chart_tot.length > 0) {
                datas[0].medtrack_chart_tot.forEach(function(trattamento){

                   var l1 = recursive_json(trattamento.l1, t, null, trattamento.count);
                   var l2 = recursive_json(trattamento.l2, l1, t, trattamento.count);
                   var l3 = recursive_json(trattamento.l3, l2, l1, trattamento.count);
                   var l4 = recursive_json(trattamento.l4, l3, l2, trattamento.count);
                   var l5 = recursive_json(trattamento.l5, l4, l3, trattamento.count);
                   var l6 = recursive_json(trattamento.l6, l5, l4, trattamento.count);
                   var l7 = recursive_json(trattamento.l7, l6, l5, trattamento.count);
                   var final = recursive_json(null, l7, l7, trattamento.count);

                    
                });
                console.log(JSON.stringify(t));
                res.status(200).send(t);
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('medtrack_chart_tt: ' + err);
        });
};

function recursive_json(livello, t, prev, count){

    
    if(livello){
        var lvl_str = livello.toString();
        
        if(t && t!== null){
            console.log(t);
            var e = t.children.filter(elem => elem.name === lvl_str);

            if(e && e.length > 0){
               return e[0];
            }else{
                var new_child = {
                    "name": lvl_str,
                    "children": []
                };

                t.children.push(new_child);
                return new_child;
            }
        }else{
            return null;
        }
    }
    else{
        if(prev){
            if(prev.children.length == 0)
                prev.size = count;
        }
        return null;
    }
    

}