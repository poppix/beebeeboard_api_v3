var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

    var user_taxe = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        user_taxe['name'] = (req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        user_taxe['rate'] = req.body.data.attributes.rate;
        user_taxe['rank'] = req.body.data.attributes.rank;
        user_taxe['editable'] = req.body.data.attributes.editable;
        user_taxe['sum'] = req.body.data.attributes.sum;
        user_taxe['gt'] = req.body.data.attributes.gt;
        user_taxe['bind'] = req.body.data.attributes.bind;
        user_taxe['is_bollo'] = req.body.data.attributes.is_bollo;
        user_taxe['tot_bollo'] = req.body.data.attributes.tot_bollo;
        user_taxe['is_active'] = req.body.data.attributes.is_active;
        user_taxe['user_id'] = ((req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id) ? req.body.data.relationships.contact.data.id : null);
        user_taxe['_tenant_id'] = req.user._tenant_id;
        user_taxe['organization'] = req.headers['host'].split(".")[0];
        user_taxe['is_iva'] = req.body.data.attributes.is_iva !== undefined && utility.check_type_variable(req.body.data.attributes.is_iva, 'boolean') ? req.body.data.attributes.is_iva : null;;
        user_taxe['is_ritenuta'] = req.body.data.attributes.is_ritenuta !== undefined && utility.check_type_variable(req.body.data.attributes.is_ritenuta, 'boolean') ? req.body.data.attributes.is_ritenuta : null;;
        user_taxe['is_rivalsa'] = req.body.data.attributes.is_rivalsa !== undefined && utility.check_type_variable(req.body.data.attributes.is_rivalsa, 'boolean') ? req.body.data.attributes.is_rivalsa : null;;
        user_taxe['type_rivalsa'] = (req.body.data.attributes.type_rivalsa && utility.check_type_variable(req.body.data.attributes.type_rivalsa, 'string') && req.body.data.attributes.type_rivalsa !== null ? req.body.data.attributes.type_rivalsa.replace(/'/g, "''''") : '');
        user_taxe['bollo_virtuale'] = req.body.data.attributes.bollo_virtuale !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_virtuale, 'boolean') ? req.body.data.attributes.bollo_virtuale : null;;
        user_taxe['iva_natura'] = (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '');
        user_taxe['rivalsa_iva_rate'] = req.body.data.attributes.rivalsa_iva_rate;
        user_taxe['rivalsa_iva_natura'] = (req.body.data.attributes.rivalsa_iva_natura && utility.check_type_variable(req.body.data.attributes.rivalsa_iva_natura, 'string') && req.body.data.attributes.rivalsa_iva_natura !== null ? req.body.data.attributes.rivalsa_iva_natura.replace(/'/g, "''''") : '');
        user_taxe['type_ritenuta'] = (req.body.data.attributes.type_ritenuta && utility.check_type_variable(req.body.data.attributes.type_ritenuta, 'string') && req.body.data.attributes.type_ritenuta !== null ? req.body.data.attributes.type_ritenuta.replace(/'/g, "''''") : '');
        user_taxe['bollo_cliente'] = req.body.data.attributes.bollo_cliente !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_cliente, 'boolean') ? req.body.data.attributes.bollo_cliente : null;;
        user_taxe['bollo_imponibile'] = req.body.data.attributes.bollo_imponibile !== undefined && utility.check_type_variable(req.body.data.attributes.bollo_imponibile, 'boolean') ? req.body.data.attributes.bollo_imponibile : null;;
        user_taxe['is_active_default'] = req.body.data.attributes.is_active_default !== undefined && utility.check_type_variable(req.body.data.attributes.is_active_default, 'boolean') ? req.body.data.attributes.is_active_default : null;;
                        

        sequelize.query('SELECT insert_user_taxe_json_v3(\'' + JSON.stringify(user_taxe) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_user_taxe_json_v3 && datas[0].insert_user_taxe_json_v3 != null) {
                    req.body.data.id = datas[0].insert_user_taxe_json_v3;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_user_taxe_json_v3,
                        'body': req.body.data.relationships,
                        'model': 'user_taxe',
                        'user_id': req.user.id,
                        'array': ['cumulable_taxe']
                    }, function(err, results) {

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'user_taxe',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_user_taxe_json_v3,
                            'Aggiunto user_taxe ' + datas[0].insert_user_taxe_json_v3
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('user_taxe_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT delete_user_taxe_relations(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {
                sequelize.query('SELECT update_user_taxe(' +
                        req.params.id +
                        ',' + req.user._tenant_id +
                        ',\'' + req.headers['host'].split(".")[0] + '\',\''
                         + JSON.stringify(req.body.data.attributes) + '\');', {
                 
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                             useMaster: true
                        })
                    .then(function(datas) {
                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'user_taxe',
                            'user_id': req.user.id,
                            'array': ['cumulable_taxe']
                        }, function(err, results) {

                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'user_taxe',
                                'update',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                req.params.id,
                                'Modificato user_taxe ' + req.params.id
                            );

                            res.json({
                                'data': req.body.data,
                                'relationships': req.body.data.relationships
                            });

                        });
                    }).catch(function(err) {
                        return res.status(400).render('user_taxe_update: ' + err);
                    });
            }).catch(function(err) {
                return res.status(400).render('user_taxe_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_user_taxe_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'user_taxes',
                    'id': datas[0].delete_user_taxe_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('user_taxe_destroy: ' + err);
        });
};