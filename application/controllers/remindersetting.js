var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var remindersetting = {};
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (req.body.data.attributes !== undefined) {
        // Import case

        if (req.body.data.attributes.send_to_mail && req.body.data.attributes.send_to_mail != '' && req.body.data.attributes.send_to_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string')) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_to_mail) != true) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }

        if (req.body.data.attributes.send_from_mail && req.body.data.attributes.send_from_mail != '' && req.body.data.attributes.send_from_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string')) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_from_mail) != true) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }

        remindersetting['send_type'] = (req.body.data.attributes.send_type && utility.check_type_variable(req.body.data.attributes.send_type, 'string') ? req.body.data.attributes.send_type : null);
        remindersetting['send_when_number'] = req.body.data.attributes.send_when_number;
        remindersetting['send_when_unit'] = (req.body.data.attributes.send_when_unit && utility.check_type_variable(req.body.data.attributes.send_when_unit, 'string') ? req.body.data.attributes.send_when_unit : null);
        remindersetting['send_to'] = (req.body.data.attributes.send_to && utility.check_type_variable(req.body.data.attributes.send_to, 'string') ? req.body.data.attributes.send_to.replace(/'/g, "''''")  : null);
        remindersetting['send_to_id'] = (req.body.data.attributes.send_to_id && !utility.check_id(req.body.data.attributes.send_to_id) ? req.body.data.attributes.send_to_id : null);
        remindersetting['send_to_mail'] = (req.body.data.attributes.send_to_mail && utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string') ? req.body.data.attributes.send_to_mail : null);
        remindersetting['send_to_mobile'] = (req.body.data.attributes.send_to_mobile && utility.check_type_variable(req.body.data.attributes.send_to_mobile, 'string') ? req.body.data.attributes.send_to_mobile : null);
        remindersetting['send_from_id'] = (req.body.data.attributes.send_from_id && !utility.check_id(req.body.data.attributes.send_from_id) ? req.body.data.attributes.send_from_id : null);
        remindersetting['send_from_mail'] = (req.body.data.attributes.send_from_mail && utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string') ? req.body.data.attributes.send_from_mail : null);
        remindersetting['is_client'] = req.body.data.attributes.is_client;
        remindersetting['_tenant_id'] = req.user._tenant_id;
        remindersetting['organization'] = req.headers['host'].split(".")[0];
        remindersetting['user_id'] = ((req.body.data.relationships && req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id && !utility.check_id(req.body.data.relationships.contact.data.id)) ? req.body.data.relationships.contact.data.id : null);
        remindersetting['mongo_id'] = null;
        remindersetting['contact_status_id'] = (req.body.data.attributes.contact_status_id && !utility.check_id(req.body.data.attributes.contact_status_id) ? req.body.data.attributes.contact_status_id : null);
        remindersetting['model'] = (req.body.data.attributes.model && utility.check_type_variable(req.body.data.attributes.model, 'string') ? req.body.data.attributes.model : null);
        remindersetting['testo_personalizzato'] = (req.body.data.attributes.testo_personalizzato && utility.check_type_variable(req.body.data.attributes.testo_personalizzato, 'string') ? req.body.data.attributes.testo_personalizzato : null);
        remindersetting['usa_personalizzato'] = (req.body.data.attributes.usa_personalizzato  ? req.body.data.attributes.usa_personalizzato : null);
        

        sequelize.query('SELECT insert_remindersetting_v1(\'' +
                JSON.stringify(remindersetting) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_remindersetting_v1;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'reminder_setting',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_remindersetting_v1,
                    'Aggiunto promemoria utente per fatture e acquisti ' + datas[0].insert_remindersetting_v1
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('remindersetting_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (req.body.data.attributes !== undefined) {

        if (req.body.data.attributes.model != 'event' && req.body.data.attributes.send_to_mail && req.body.data.attributes.send_to_mail != '' && req.body.data.attributes.send_to_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string')) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_to_mail) != true) {
                return res.status(422).send(utility.error422('send_to_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }

        if (req.body.data.attributes.model != 'event' && req.body.data.attributes.send_from_mail && req.body.data.attributes.send_from_mail != '' && req.body.data.attributes.send_from_mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string')) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.send_from_mail) != true) {
                return res.status(422).send(utility.error422('send_from_mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }

        sequelize.query('SELECT update_remindersetting_v1(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                (req.body.data.attributes.send_type && utility.check_type_variable(req.body.data.attributes.send_type, 'string') ? req.body.data.attributes.send_type : null) + '\',' +
                req.body.data.attributes.send_when_number + ',\'' +
                (req.body.data.attributes.send_when_unit && utility.check_type_variable(req.body.data.attributes.send_when_unit, 'string') ? req.body.data.attributes.send_when_unit : null) + '\',\'' +
                (req.body.data.attributes.send_to && utility.check_type_variable(req.body.data.attributes.send_to, 'string') ? req.body.data.attributes.send_to.replace(/'/g, "''''")  : null) + '\',' +
                (req.body.data.attributes.send_to_id && !utility.check_id(req.body.data.attributes.send_to_id) ? req.body.data.attributes.send_to_id : null) + ',\'' +
                (req.body.data.attributes.send_to_mail && utility.check_type_variable(req.body.data.attributes.send_to_mail, 'string') ? req.body.data.attributes.send_to_mail : null) + '\',\'' +
                (req.body.data.attributes.send_to_mobile && utility.check_type_variable(req.body.data.attributes.send_to_mobile, 'string') ? req.body.data.attributes.send_to_mobile : null) + '\',' +
                (req.body.data.attributes.send_from_id && !utility.check_id(req.body.data.attributes.send_from_id) ? req.body.data.attributes.send_from_id : null) + ',\'' +
                (req.body.data.attributes.send_from_mail && utility.check_type_variable(req.body.data.attributes.send_from_mail, 'string') ? req.body.data.attributes.send_from_mail : null) + '\',' +
                req.body.data.attributes.is_client + ',' +
                ((req.body.data.relationships && req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id && !utility.check_id(req.body.data.relationships.contact.data.id)) ? req.body.data.relationships.contact.data.id : null) + ',' +
                (req.body.data.attributes.contact_status_id && !utility.check_id(req.body.data.attributes.contact_status_id) ? req.body.data.attributes.contact_status_id : null)+',\'' +
                (req.body.data.attributes.model && utility.check_type_variable(req.body.data.attributes.model, 'string') ? req.body.data.attributes.model : null)+'\',\'' +
                (req.body.data.attributes.testo_personalizzato && utility.check_type_variable(req.body.data.attributes.testo_personalizzato, 'string') ? req.body.data.attributes.testo_personalizzato : null)+'\',' +
                (req.body.data.attributes.usa_personalizzato  ? req.body.data.attributes.usa_personalizzato : null)+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'reminder_setting',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato promemoria utente per fatture e acquisti ' + req.params.id
                );
                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('remindersetting_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_remindersetting_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'remindersettings',
                    'id': req.params.id
                }
            });

        }).catch(function(err) {
            return res.status(400).render('remindersetting_destroy: ' + err);
        });
};