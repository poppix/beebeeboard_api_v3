var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../../utility/utility.js'),
    async = require('async'),
    config = require('../../../config'),
    utils = require('../../../utils');

var mandrill = require('mandrill-api/mandrill');
var request = require('request');
const { S3Client, PutObjectCommand} = require('@aws-sdk/client-s3');


exports.yousign_webhook = function(req, res) {
    var body_example = req.body;
    var organization = null;
    var signature_request = null,
        signer = null;

    if(body_example){

        
        signature_request = body_example.data.signature_request;
       

        organization = signature_request.external_id.split('-')[0];


        sequelize.query('SELECT yousign_webhook_save(\'' +
            organization + '\',\''+
            JSON.stringify(body_example).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

                console.log(JSON.stringify(datas));
                if(datas && datas[0] && datas[0].yousign_webhook_save == 1){
                    res.status(200).send({});
                }
                else{
                    res.status(400).send({});
                }

                
          }).catch(function(err) {
            console.log(err);
            res.status(400).send({});
        });   

    }
    else{
        res.status(404).send({});
    }
    
};

exports.yousign_webhook_cron = function(req, res) {
    var body_example = null;
    var organization = null,
        project_id = null,
        contact_id = null;
    var signature_request = null,
        signer = null;


    sequelize.query('SELECT get_yousign_webhooks();', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(wh) {
        var cnt = 0;
        if(wh[0] &&  wh[0].get_yousign_webhooks[0] && wh[0].get_yousign_webhooks[0].sites && wh[0].get_yousign_webhooks[0].sites.length > 0){
            wh[0].get_yousign_webhooks[0].sites.forEach(function(site_){

                body_example = site_.wh_json;

                if(body_example){

            
                    signature_request = body_example.data.signature_request;
                    signer = body_example.data.signer;

                    organization = signature_request.external_id.split('-')[0];
                    project_id = signature_request.external_id.split('-')[1].substring(1);
                    contact_id = signature_request.external_id.split('-')[2].substring(1);


                    sequelize.query('SELECT search_organization(\'' +
                        organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(search_org) {

                    
                            if(signer.status == 'signed'){
                                /* vado a prendere il file per salvarlo */
                               
                                    
                                var options = {
                                  'method': 'GET',
                                  'url': (search_org[0].search_organization.yousign_sandbox == true ? config.yousign.url_test_base : config.yousign.url_base) + '/signature_requests/' + signature_request.id +'/documents/download?version=completed',
                                  'encoding': null,
                                  'headers': {
                                    'Authorization': 'Bearer '+ (search_org[0].search_organization.yousign_sandbox == true ? config.yousign.api_test_key : config.yousign.api_key)
                                  }
                                };
                                request(options, function (error, response) {
                                      if (error) throw new Error(error);
                                      

                                      if(response.body){
                                         const s3 = new S3Client({
                                            region: config.aws_s3.region,
                                            credentials:{
                                                accessKeyId: config.aws_s3.accessKeyId,
                                                secretAccessKey: config.aws_s3.secretAccessKey,
                                            }
                                        });

                                        var buf = new Buffer.from(response.body,'base64');

                                        var data = {
                                            Bucket: 'data.beebeeboard.com',
                                            Key: organization + '/' + contact_id + '/signed/' + signature_request.name + '_'+signature_request.id+'.pdf',
                                            ACL: 'private',
                                            ServerSideEncryption: 'AES256',
                                            Body: buf,
                                            ContentEncoding: 'base64',
                                            ContentType: 'application/pdf'
                                        };
                                      

                                        const command = new PutObjectCommand(data);
                                        s3.send(command, function(err, data) {
                                        //s3.putObject(data, function(err, data){
                                            if (err) { 
                                                
                                                console.log('Error uploading data: ', data); 
                                                return res.status(400).render('signed_file_upload: ' + data);
                                            } else {
                                                console.log('succesfully uploaded the signed file!');

                                                var file = {};
                                                file.organization = organization;
                                                file.file_name = signature_request.name.replace(/'/g, "''''") + '_'+signature_request.id+'.pdf';
                                                file.file_type = 'pdf';
                                                file.file_size = buf.length;
                                                file.file_url = organization + '/' + contact_id + '/signed/' + signature_request.name.replace(/'/g, "") + '_'+signature_request.id;
                                                file.is_thumbnail = false;
                                                file.file_etag = data.ETag.replace(/\"/g, "");
                                                file.upload_date = moment(new Date()).utc().format('YYYY/MM/DD HH:mm:ss');
                                                file.uploader_id = contact_id ? contact_id : null;

                                                sequelize.query('SELECT otp_file_signed(null,\'' +
                                                    organization + '\',\'' +
                                                    JSON.stringify(file) + '\',\'' +
                                                    signature_request.id + '\',\'' +
                                                    JSON.stringify(signature_request) + '\',\'' +
                                                    JSON.stringify(signer) + '\','+
                                                    (contact_id && contact_id !== undefined ? contact_id : null )+','+
                                                    (project_id && project_id !== undefined ? project_id : null ) +');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {

                                                    console.log(datas[0].otp_file_signed);
                                                    var file_json = {
                                                        'attributes': file
                                                    };

                                                    utility.save_socket(organization, 'file_signed', datas[0].otp_file_signed, contact_id, datas[0].otp_file_signed, file_json);
                                

                                                            /* TOLGO DALLA LISTA DA INVIARE */

                                                             sequelize.query('SELECT yousign_wh_sync(' +
                                                                site_.id + ');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas_o) {

                                                            }).catch(function(err) {
                                                                console.log(err);
                                                                //return res.status(400).render('yousign_wh_sync: ' + data);
                                                            });

                                                        
                                                            if (signer && signer.info && signer.info.email) {
                                                                var contact_name = signer.info.first_name + ' '+  signer.info.last_name ;
                                                                let mails = [];
                                                                let mail = {
                                                                    "email": signer.info.email,
                                                                    "name": contact_name,
                                                                    "type": "to"
                                                                };
                                                               
                                                                mails.push(mail);

                                                                if(search_org[0].search_organization.sign_mail){
                                                                    mail = {
                                                                        "email": search_org[0].search_organization.sign_mail,
                                                                        "name": search_org[0].search_organization.organization_name,
                                                                        "type": "to"
                                                                    };
                                                                   
                                                                    mails.push(mail);
                                                                }
                                                                
                                                               
                                                                var template_content = [{
                                                                        "name": "LANG",
                                                                        "content":  signer.info.locale && signer.info.locale === 'en' ? 'en-us' : (signer.info.locale && signer.info.locale === 'de' ? 'de-de' : null)
                                                                    }, {
                                                                        "name": "DOCUMENT_NAME",
                                                                        "content": signature_request.name.replace(/'/g, "''''") + '_'+signature_request.id+'.pdf'
                                                                    }, {
                                                                        "name": "CLIENT_NAME",
                                                                        "content": contact_name.toUpperCase()
                                                                    }, {
                                                                        "name": "ORGANIZATION",
                                                                        "content": search_org[0].search_organization.name
                                                                    }, {
                                                                        "name": "COLOR",
                                                                        "content": search_org[0].search_organization.primary_color ? search_org[0].search_organization.primary_color : '#00587D'
                                                                    }, {
                                                                        "name": "LOGO",
                                                                        "content": search_org[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + search_org[0].search_organization.thumbnail_url + '-small.jpeg' : null
                                                                    }],
                                                                    message = {
                                                                        "global_merge_vars": template_content,
                                                                        "subject": search_org[0].search_organization.subject_sign_success ? search_org[0].search_organization.subject_sign_success : 'Documento firmato',
                                                                        "from_email": search_org[0].search_organization.domain_from_mail ? search_org[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                                                        "from_name": search_org[0].search_organization.name,
                                                                        "to": mails,
                                                                        "headers": {
                                                                            "Reply-To": search_org[0].search_organization.domain_from_mail ? search_org[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                                                                        },
                                                                        "tags": ["beebeeboard-signed-document", organization],
                                                                        "subaccount": "beebeeboard",
                                                                        "tracking_domain": "mandrillapp.com",
                                                                        "google_analytics_domains": ["poppix.it"],
                                                                        "metadata": {
                                                                            "organization": organization
                                                                        },
                                                                        "attachments": [{
                                                                            'type': 'application/pdf',
                                                                            'name': signature_request.name.replace(/'/g, "''''") + '_'+signature_request.id+'.pdf',
                                                                            'content': response.body.toString('base64')
                                                                        }]
                                                                    };

                                                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                                                mandrill_client.messages.sendTemplate({
                                                                    "template_name": search_org[0].search_organization.template_sign_success ? search_org[0].search_organization.template_sign_success : "sign-success",
                                                                    "template_content": template_content,
                                                                    "message": message,
                                                                    "async": true,
                                                                    "send_at": false
                                                                }, function(result) {

                                                                    if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                                                        console.log("Mail sended correctly");

                                                                         sequelize.query('SELECT insert_mandrill_send(' +
                                                                                search_org[0].search_organization.id + ',\'' +
                                                                                organization + '\',null,null,\'' +
                                                                                result[0].status + '\',\'\',\'' + result[0]._id + '\',\'Invio documento firmato Yousign\',' +
                                                                                '\'Invio documento firmato '+signature_request.name.replace(/'/g, "''''") + '_'+signature_request.id+'.pdf\');', {
                                                                                    raw: true,
                                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                                     useMaster: true
                                                                                })
                                                                            .then(function(datas1) {


                                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');

                                                                                    res.status(200).send({});



                                                                            }).catch(function(err) {
                                                                                return res.status(400).render('mandrill send mail: ' + err);
                                                                            });

                                                                    } else if(result && result[0]) {
                                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                                                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                                        sequelize.query('SELECT insert_mandrill_send(' +
                                                                                search_org[0].search_organization.id + ',\'' +
                                                                                organization + '\',null,null,\'' +
                                                                                result[0].status + '\',\'' +
                                                                                result[0].reject_reason + '\',\'' + result[0]._id + '\',\'Invio documento firmato Yousign\',' +
                                                                                '\'Invio documento firmato '+signature_request.name.replace(/'/g, "''''") + '_'+signature_request.id+'.pdf\');', {
                                                                                    raw: true,
                                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                                      useMaster: true
                                                                                })
                                                                            .then(function(datas1) {
                                                                                 console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail sended saved');


                                                                                res.status(200).send({});


                                                                            }).catch(function(err) {
                                                                                return res.status(400).render('mandrill send mail: ' + err);
                                                                            });
                                                                    }
                                                                 });
                                                            }
                                                            
                                                  
                                                    
                                                }).catch(function(err) {
                                                    console.log(err);
                                                    return res.status(400).render('signed_file_upload: ' + data);
                                                });
                                            }
                                        });
                                        

                                    }else
                                    {
                                        return res.status(422).send(utility.error422('document_sign_id', 'Parametro non valido', 'Parametro non valido', '422'));
                                    }  

                                  
                                });

                            }
                      }).catch(function(err) {
                        console.log(err);
                        return res.status(400).render('search_organization: ' + data);
                    });   

                }
                else{
                    res.status(404).send({});
                }
            }); 
        }
        else{
            res.status(200).send({});
        }
    }).catch(function(err) {
        console.log(err);
        res.status(400).send({});
    });   
};

exports.yousign_counter = function(req, res) {
    
    var tipo = null;
    
   
    if (req.body.tipo !== undefined && req.body.tipo !== '') {
        
        tipo = '\'' + req.body.tipo  + '\'';
    }else{
        return res.status(422).send(utility.error422('tipologia', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if(tipo){

       
        
        sequelize.query('SELECT yousign_set_counter(\'' +
            req.headers['host'].split(".")[0]  + '\','+
            tipo+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(search_org) {
                res.status(200).send({});
          }).catch(function(err) {
            console.log(err);
            return res.status(400).render('yousign_set_counter: ' + data);
        });   

    }
    else{
        res.status(404).send({});
    }
    
};

exports.yousign_sended = function(req, res) {
    
  
        sequelize.query('SELECT yousign_sended(\'' +
            req.headers['host'].split(".")[0]  + '\',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(search_org) {
                res.status(200).send({});
          }).catch(function(err) {
            console.log(err);
            return res.status(400).render('yousign_sended: ' + err);
        });   
    
};

exports.set_test_yousign = function(req, res) {
    
    var tipo = null;
    
   
    if (req.body.test !== undefined && req.body.test !== '') {
        
        tipo =  req.body.test  ;
    }else{
        return res.status(422).send(utility.error422('tipologia', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if(tipo){

       
        
        sequelize.query('SELECT yousign_set_test(\'' +
            req.headers['host'].split(".")[0]  + '\','+
            tipo+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(search_org) {
                res.status(200).send({});
          }).catch(function(err) {
            console.log(err);
            return res.status(400).render('yousign_set_test: ' + data);
        });   

    }
    else{
        res.status(404).send({});
    }
    
};
