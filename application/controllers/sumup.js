var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
        utility = require('../utility/utility.js'),
        logs = require('../utility/logs.js'),
        config = require('../../config');


exports.create_checkout = function(req, res) {
   var request = require('request');


    sequelize.query('SELECT sumup_get_data(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+',' +
        req.query.user_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {

        if(datas && datas[0] && datas[0].sumup_get_data && datas[0].sumup_get_data.paypal_token){

            let json_data = datas[0].sumup_get_data;


            var postData = {
              'method': 'GET',
              'url': 'https://api.sumup.com/v0.1/me',
              'headers': {
                'Authorization': 'Bearer '+ json_data.paypal_token
              }
            };

            request(postData, function (err, response, tok) {
             

                if(err){
                    console.log(err);
                }
                else{
                 
                    if(tok && JSON.parse(tok).merchant_profile && JSON.parse(tok).merchant_profile.merchant_code){
                         var postData = {
                          'method': 'POST',
                          'url': 'https://api.sumup.com/v0.1/checkouts',
                          'headers': {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json', 
                            'Authorization': 'Bearer '+ json_data.paypal_token
                          },
                          form: {
                            'checkout_reference': req.query.contact_id + '_' + req.query.payment_id + '_' + moment().format('YYYYMMDDTHHmmss'),
                            'amount': req.query.amount,
                            'currency': req.query.currency,
                            'merchant_code': JSON.parse(tok).merchant_profile.merchant_code
                          }
                        };


                        request(postData, function (err, response, body) {

                            if(err){
                                console.log(err);
                                res.status(500).send({});
                            }
                            else{
                                
                                res.status(200).send(body);
                            }
                        });
                    }
                    else
                    {
                        res.status(200).send({});
                    }
                    
                }
            });
        }
        else{
            res.status(500).send({});
        }
    });
};


exports.verify = function(req, res) {
   var request = require('request');
   var https = require('https');
   var json_ = req.body ? req.body : null;

   console.log(json_);

   if(json_){

    let checkout_id = json_.sumup.id;
    sequelize.query('SELECT sumup_get_data(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+',' +
        req.body.user_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {

        if(datas && datas[0] && datas[0].sumup_get_data && datas[0].sumup_get_data.paypal_token){

            let json_data = datas[0].sumup_get_data;


            var postData = {
              'method': 'GET',
              'url': 'https://api.sumup.com/v0.1/checkouts/'+checkout_id,
              'headers': {
                'Authorization': 'Bearer '+ json_data.paypal_token
              }
            };

            request(postData, function (err, response, tok1) {
             

                if(err){
                    console.log(err);
                    res.status(400).render('Sumup Verify : https://api.sumup.com/v0.1/checkouts/'+checkout_id);
                }
                else{
                    let tok = JSON.parse(tok1);
                 
                    if(tok && tok.status && tok.status == 'PAID'){

                        
                        sequelize.query('SELECT update_payment_stripe(\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            json_.payment_id + ',\'' +
                            tok.date + '\',' +
                            true + ',\'sumup\', \'' +
                            tok.transaction_code + ' - '+tok.checkout_reference+'\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            
                            sequelize.query('SELECT search_organization(\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(datas_o) {
                               
                                    if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                                        sequelize.query('SELECT update_payment_request_success(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            json_.payment_id + ',null,true,\'Sumup\',null);', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            

                                           res.status(200).send({});
                                        }).catch(function(err) {
                                            console.log(err);
                                           return res.status(400).render('Sumup Verify update_payment_request_success ' + err);
                                        });
                                    }else{
                                        sequelize.query('SELECT scuole_ecommerce_sumup_success(' +
                                            datas_o[0].search_organization._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            json_.payment_id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            }).then(function(datas) {
                                        
                                                var url = '';
                                                if(datas_o[0].search_organization.ecommerce_mail && 
                                                    datas_o[0].search_organization.ecommerce_mail !== null && 
                                                    datas_o[0].search_organization.ecommerce_mail != '' &&
                                                    datas_o[0].search_organization.ecommerce_mail !== undefined){
                                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0]}&payment_id=${json_.payment_id}&completo=true&esito=OK`;
                                                }else{
                                                        url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0]}&payment_id=${json_.payment_id}&completo=true&esito=OK`;
                                                }
                                                

                                                var request = https.get(url, function(result) {

                                                    result.on('end', function() {
                                                        
                                                    });
                                                }).on('error', function(err) {

                                                    return res.status(400).render('Sumup Verify : Mail Not Sended: ' + err);
                                                });

                                                request.end();

                                                res.status(200).send({});
                                            }).catch(function(err) {
                                                return res.status(400).render('Sumup Verify scuole_ecommerce_paypal_success ' + err);
                                            });    
                                    }
                                    
                            }).catch(function(err) {
                                return res.status(400).render('Sumup Verify search_organization ' + err);
                            });       
                            
                        }).catch(function(err) {
                            console.log(err);
                            
                        });
                    }
                    else
                    {
                        sequelize.query('SELECT update_payment_stripe(\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            json_.payment_id + ',\'' +
                            tok.date + '\',' +
                            false + ',\'sumup\', \'' +
                            tok.transaction_code + ' - '+tok.checkout_reference+'\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            
                            sequelize.query('SELECT search_organization(\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(datas_o) {
                               
                                    if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                                        res.status(200).send({});
                                    }else{
                                        sequelize.query('SELECT scuole_ecommerce_sumup_error(' +
                                            datas_o[0].search_organization._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            json_.payment_id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            }).then(function(datas) {
                                        
                                                var url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0]}&payment_id=${json_.payment_id}&completo=true&esito=KO`;

                                                var request = https.get(url, function(result) {

                                                    result.on('end', function() {
                                                        
                                                    });
                                                }).on('error', function(err) {

                                                    return res.status(400).render('Sumup Verify : Mail Not Sended: ' + err);
                                                });

                                                request.end();

                                                res.status(200).send({});
                                            }).catch(function(err) {
                                                return res.status(400).render('Sumup Verify scuole_ecommerce_paypal_success ' + err);
                                            });    
                                    }
                                    
                            }).catch(function(err) {
                                return res.status(400).render('Sumup Verify search_organization ' + err);
                            });       
                            
                        }).catch(function(err) {
                            console.log(err);
                            
                        });
                        
                    }
                     
                }
            });
        }
        else{
            res.status(500).send({});
        }
    });

   }else
   {
     res.status(404).send({});
   }
};