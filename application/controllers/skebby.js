var moment = require('moment');
var config = require('../../config');
var Sequelize = require('sequelize');
var mandrill = require('mandrill-api/mandrill');
var utility = require('../utility/utility.js');

/*
exports.send_sms_journey = function(_tenant_id, org, cliente_id, start, end, maestro_id, comp, num_person, operator_id) {
    var https = require('https');
    var async = require('async');
    var qs = require('querystring');
    var util = require('util');
    var text = '';
    var number_character = 0;
    var sender_string = _tenant_id == 170 ? 'MonteBianco' : 'Beebeeboard';
    var method = _tenant_id == 170 ? 'send_sms_classic_report' : 'send_sms_classic_report';
    var lrecipients = [];
    var mobiles = [];

    lrecipients.push(maestro_id);

    sequelize.query('SELECT select_mobile_numbers_from_contact_id(' +
            _tenant_id + ',\'' +
            org + '\',' +
            (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
            operator_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(contacts) {

            async.parallel({
                mobile: function(callback) {
                    if (contacts[0]['select_mobile_numbers_from_contact_id']) {
                        var cnt = 0;
                        contacts[0]['select_mobile_numbers_from_contact_id'].forEach(function(contatto) {
                            if (contatto.attributes.mobile) {
                                if (contatto.attributes.mobile.indexOf('+') == -1) {
                                    contatto.attributes.mobile = '39' + contatto.attributes.mobile;
                                } else {
                                    contatto.attributes.mobile = contatto.attributes.mobile.replace('+', '');
                                }

                                mobiles.push((contatto.attributes.mobile ? contatto.attributes.mobile.replace(' ', '') : ''));
                            }
                            if (cnt++ >= contacts[0]['select_mobile_numbers_from_contact_id'].length - 1) {
                                callback(null, mobiles);
                            }
                        });
                    } else {
                        callback(null, null);
                    }
                },
                cliente: function(callback) {
                    sequelize.query('SELECT select_mail_from_contact_id(' +
                            _tenant_id + ',\'' +
                            org + '\',' +
                            ('ARRAY[' + cliente_id + ']::bigint[]') + ',' +
                            operator_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(contacts) {
                            if (contacts[0]['select_mail_from_contact_id']) {
                                contacts[0]['select_mail_from_contact_id'].forEach(function(contatto) {
                                    callback(null, contatto.attributes.name);
                                });
                            } else {
                                callback(null, null);
                            }
                        }).catch(function(err) {
                            callback(true, null);
                        });
                }
            }, function(err, results) {
                if (err)
                    console.log('Error:' + err);

                text = comp + ' Lez:' + moment(start).format('MM D hh:mm') + ' ' +
                    moment(end).format('hh:mm') + ' Cli:' +
                    results.cliente;
                var params = {
                    method: method,
                    username: config.skebby.username,
                    password: config.skebby.password,
                    "recipients[]": results.mobile,
                    text: text,
                    charset: "UTF-8",
                };

                if (sender_string) {
                    params.sender_string = sender_string;
                }

                var data = qs.stringify(params);

                var client = https.request({
                    port: 443,
                    path: "/api/send/smseasy/advanced/http.php",
                    host: "gateway.skebby.it",
                    method: "POST",
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Content-Length": data.length,
                        "Content-Encoding": "utf8",
                    }
                }, function(res_) {
                    var res_data = "";
                    res_.on('data', function(data) {
                        res_data += data;

                    });
                    res_.on("end", function() {

                        var res_parsed = qs.parse(res_data);
                        if (res_parsed.status == "success") {

                            console.log('SMS: send correctly ' + JSON.stringify(res_parsed));

                            sequelize.query('SELECT insert_skebby_send(' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                    req.user.id + ',\'' +
                                    res_parsed.status + '\',null,\'' + req.body.text + '\',' + req.body.sms_number + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                .then(function(datas) {

                                    if (err) {
                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved' + err);
                                    }

                                    res.status(200).send({});


                                }).catch(function(err) {
                                    return res.status(400).render('skebby send sms: ' + err);
                                });
                        } else {
                            // ------------------------------------------------------------------
                            // Check the complete documentation at http://www.skebby.com/business/index/send-docs/
                            // ------------------------------------------------------------------
                            // For eventual errors see http:#www.skebby.com/business/index/send-docs/#errorCodesSection
                            // WARNING: in case of error DON'T retry the sending, since they are blocking errors
                            // ------------------------------------------------------------------      

                            console.log('Error Send SMS: ' + res_parsed.status + '  ' + res_parsed.message);


                            sequelize.query('SELECT insert_skebby_send(' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                    req.user.id + ',\'' +
                                    res_parsed.status + '\',\'' +
                                    res_parsed.message + '\',\'' + req.body.text + '\',' + req.body.sms_number + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                    })
                                .then(function(datas) {

                                    if (err) {
                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved' + err);
                                    }

                                    res.status(422).send({});


                                }).catch(function(err) {
                                    return res.status(400).render('skebby send sms: ' + err);
                                });
                        }

                    });
                });

                client.end(data);

                client.on('error', function(e) {
                    if (!res_done) {
                        console.log('Send SMS Done: ' + e);
                        res_done = true;
                    }
                });
            });

        }).catch(function(err) {
            return res.status(400).render('skebby send sms: ' + err);
        });
};*/

exports.send_sms = function(req, res) {

    var https = require('https');
    var async = require('async');
    var qs = require('querystring');
    var util = require('util');
    var text = (req.body.text ? req.body.text.replace(/'/g, "''''") : '');
    var number_character = req.body.number;
    var sender_string = req.user._tenant_id == 170 ? 'MonteBianco' : 'Beebeeboard';
    var method = req.user._tenant_id == 170 ? 'send_sms_classic_report' : 'send_sms_classic_report';
    var lrecipients = [];
    var mobiles = [];
    var mobile_text = [];
    var res_done = false;

    if (req.body.contact_id !== undefined && req.body.contact_id != '') {
        if (util.isArray(req.body.contact_id)) {
            lrecipients.push(req.body.contact_id);
        } else {
            lrecipients.push(req.body.contact_id);
        }
    }

    if (lrecipients.length > 0) {
        sequelize.query('SELECT select_mobile_numbers_from_contact_id(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(contacts) {
                
                if((contacts && contacts[0] && contacts[0]['select_mobile_numbers_from_contact_id'] && 
                    util.isArray(contacts[0]['select_mobile_numbers_from_contact_id'])) || req.body.number ){
                    async.parallel({
                        mobile: function(callback) {
                            if(req.body.number){
                               
                              
                                if (req.body.number.indexOf('+') == -1) {
                                    req.body.number = '+39' + req.body.number;
                                } else {
                                    /*contatto.attributes.mobile = contatto.attributes.mobile.replace('+', '');*/
                                }
                                mobiles.push((req.body.number ? req.body.number.replace(' ', '') : ''));
                                 
                                callback(null, mobiles);
                                
                            }
                            else if (contacts[0]['select_mobile_numbers_from_contact_id']) {
                                var cnt = 0;
                                contacts[0]['select_mobile_numbers_from_contact_id'].forEach(function(contatto) {
                                    if (contatto.attributes.mobile) {
                                        if (contatto.attributes.mobile.indexOf('+') == -1) {
                                            contatto.attributes.mobile = '+39' + contatto.attributes.mobile;
                                        } else {
                                            /*contatto.attributes.mobile = contatto.attributes.mobile.replace('+', '');*/
                                        }
                                        mobiles.push((contatto.attributes.mobile ? contatto.attributes.mobile.replace(' ', '') : ''));
                                        
                                    }
                                    if (cnt++ >= contacts[0]['select_mobile_numbers_from_contact_id'].length - 1) {
                                        callback(null, mobiles);
                                    }
                                });
                            } else {
                                callback(null, null);
                            }
                        },
                         mobile_text: function(callback) {
                            if(req.body.number){
                               
                              
                                if (req.body.number.indexOf('+') == -1) {
                                    req.body.number = '+39' + req.body.number;
                                } else {
                                    /*contatto.attributes.mobile = contatto.attributes.mobile.replace('+', '');*/
                                }
                                mobile_text.push('\''+(req.body.number ? req.body.number.replace(' ', '') : '')+'\'');
                                 
                                callback(null, mobile_text);
                                    
                            }else if (contacts[0]['select_mobile_numbers_from_contact_id']) {
                                var cnt = 0;
                                contacts[0]['select_mobile_numbers_from_contact_id'].forEach(function(contatto) {
                                    if (contatto.attributes.mobile) {
                                        if (contatto.attributes.mobile.indexOf('+') == -1) {
                                            contatto.attributes.mobile = '+39' + contatto.attributes.mobile;
                                        } else {
                                            /*contatto.attributes.mobile = contatto.attributes.mobile.replace('+', '');*/
                                        }
                                        mobile_text.push('\''+(contatto.attributes.mobile ? contatto.attributes.mobile.replace(' ', '') : '')+'\'');
                                    }
                                    if (cnt++ >= contacts[0]['select_mobile_numbers_from_contact_id'].length - 1) {
                                        callback(null, mobile_text);
                                    }
                                });
                            } else {
                                callback(null, null);
                            }
                        },
                        mittente: function(callback) {
                            sequelize.query('SELECT get_sms_mittente(\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user._tenant_id + ');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                                })
                            .then(function(mitt) {
                                if (mitt[0]['get_sms_mittente']) {
                             
                                    callback(null, mitt[0]['get_sms_mittente']);
                                    
                                } else {
                                    callback(null, 'Beebeeboard');
                                }
                            }).catch(function(err) {
                                return res.status(400).render('get_sms_mittente: ' + err);
                            });
                        }
                    }, function(err, results) {
                        if (err)
                            console.log('Error:' + err);

                        utility.login_skebby(function(err, auth) {
                            if (!err) {
                                var smsData = {
                                    "returnCredits": true,
                                    "recipient": results.mobile,
                                    "message": text,
                                    "message_type": "GP",
                                    "sender" : results.mittente
                                };

                                utility.sendSMS(auth, smsData,
                                    function(error, data) {
                                        if (error) {
                                            sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                                req.user.id + ',\'' +
                                                (error ? error.replace(/'/g, "''''") : '') + '\',\'' +
                                                (error ? error.replace(/'/g, "''''") : '') + '\',\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\',' + req.body.sms_number + ','+
                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',null, null,'+
                                                (results.mobile_text.length > 0 ? 'ARRAY[' + results.mobile_text + ']::text[]' : null)+');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');
                                                res.status(200).send({});

                                            }).catch(function(err) {
                                                return res.status(400).render('skebby send sms: ' + err);
                                            });
                                         }
                                        else {
                                            console.log(data);
                                            console.log('SMS: send correctly ' + JSON.stringify(data));

                                            sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                                    req.user.id + ',\'success\',\'\',\'' + req.body.text.replace(/'/g, "''''") + '\',' +
                                                    req.body.sms_number + ','+
                                                    (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                     (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',null, null,'+
                                                    (results.mobile_text.length > 0 ? 'ARRAY[' + results.mobile_text + ']::text[]' : null)+');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {

                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                    res.status(200).send({});


                                                }).catch(function(err) {
                                                    return res.status(400).render('skebby send sms: ' + err);
                                                });
                                        }
                                });
                            }
                            else {
                              return res.status(400).render('skebby send sms: ' + err);
                            }
                        });

                        
                    });
                }
                else{
                    res.status(422).send({});
                }

            }).catch(function(err) {
                return res.status(400).render('skebby send sms: ' + err);
            });
    }
};

exports.send_mail = function(req, res) {
    var https = require('https');
    var async = require('async');
    var qs = require('querystring');
    var util = require('util');
    var text = req.body.text;
    var sender_string = 'Beebeeboard';
    var method = 'send_sms_classic';
    var lrecipients = [];
    var mails = [];

    if (util.isArray(req.body.contact_id)) {
        lrecipients.push(req.body.contact_id);
    } else {
        lrecipients.push(req.body.contact_id);
    }


    if (lrecipients.length > 0) {

        sequelize.query('SELECT select_mail_from_contact_id(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(contacts) {

                async.parallel({
                    mail: function(callback) {
                        if (contacts[0]['select_mail_from_contact_id'] && contacts[0]['select_mail_from_contact_id'].length > 0) {
                            var cnt = 0;
                            var mails = [];
                            contacts[0]['select_mail_from_contact_id'].forEach(function(contatto) {
                                if(req.body.mail){
                                    var addresses = req.body.mail.split(',');
                                    var address_string = '';
                                  
                                    addresses.forEach(function(address) {
                                        address_string += ' ' + address;
                                        var mail = {
                                            "email": address.replaceAll(' ',''),
                                            "name": null,
                                            "type": "to"
                                        };
                                        mails.push(mail);
                                    });
                                }
                                else if (contatto.attributes.mail) {
                                    var mail = {
                                        "email": contatto.attributes.mail,
                                        "name": contatto.attributes.name,
                                        "type": "to"
                                    };

                                    mails.push(mail);
                                }
                                if (cnt++ >= contacts[0]['select_mail_from_contact_id'].length - 1) {
                                    callback(null, mails);
                                }
                            });
                        } else {
                            callback(null, null);
                        }
                    }
                }, function(err, results) {

                     sequelize.query('SELECT search_organization_reply_to_new(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT
                        })
                    .then(function(reply_mail) {

                        var reply_to = 'no-reply@beebeemailer.com';
                            var domain_from_mail = 'no-reply@beebeemailer.com';

                             if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                                reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                                reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                    reply_to = domain_from_mail;
                                }
                            }


                        var template_content = [{
                                "name": "LANG",
                                "content": req.body.lang
                            }, {
                                "name": "OBJECT",
                                "content": req.body.object
                            }, {
                                "name": "MESSAGE",
                                "content": req.body.text
                            }, {
                                "name": "COLOR",
                                "content": req.body.color ? req.body.color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": (req.body.object ? req.body.object : null),
                                "from_email": domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                "from_name": req.body.organization_name ? req.body.organization_name : req.headers['host'].split(".")[0],
                                "to": results.mail,
                                "headers": {
                                    "Reply-To": reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                },
                                "tags": ["rapid-message",req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"],
                                "preserve_recipients": false
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "rapid-message",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                        

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                if(req.body.massive == 'true' || req.body.massive === true){
                                    sequelize.query('SELECT insert_mandrill_send_massive(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.object ? req.body.object.replace(/'/g, "''''") : '') + '\',' +
                                        '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\','+
                                        (result ? '\'' + JSON.stringify(result).replace(/'/g, "''''") + '\'' : null)+');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                             useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail massive sended saved');


                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail massive: ' + err);
                                    });
                                }
                                else
                                {
                                    sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.object ? req.body.object.replace(/'/g, "''''") : '') + '\',' +
                                        '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                             useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });
                                }
                                

                            } else if(result && result[0]) {
                                  if(req.body.massive == 'true' || req.body.massive === true){
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                    sequelize.query('SELECT insert_mandrill_send_massive(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                            req.user.id + ',\'' +
                                            result[0].status + '\',\'' +
                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.object.replace(/'/g, "''''") + '\',' +
                                            '\'' + req.body.text.replace(/'/g, "''''") + '\','+
                                            (result ? '\'' + JSON.stringify(result).replace(/'/g, "''''") + '\'' : null)+');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                 useMaster: true
                                            })
                                        .then(function(datas) {


                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail massive sended saved');


                                            res.status(422).send({});


                                        }).catch(function(err) {
                                            return res.status(400).render('mandrill massive send mail: ' + err);
                                        });

                                }
                                else
                                {
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                    sequelize.query('SELECT insert_mandrill_send(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            (lrecipients.length > 0 ? 'ARRAY[' + lrecipients + ']::bigint[]' : null) + ',' +
                                            req.user.id + ',\'' +
                                            result[0].status + '\',\'' +
                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.object.replace(/'/g, "''''") + '\',' +
                                            '\'' + req.body.text.replace(/'/g, "''''") + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                                            })
                                        .then(function(datas) {


                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail sended saved');


                                            res.status(422).send({});


                                        }).catch(function(err) {
                                            return res.status(400).render('mandrill send mail: ' + err);
                                        });
                                }
                            }else
                            {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill');

                                res.status(422).send({});
                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                        });
                    }).catch(function(err) {
                        return res.status(400).render('mandrill send mail: ' + err);
                    });
                });
            });
    } else {
        res.status(200).send({});
    }
};

exports.send_push_notification = function(req, res) {

    var https = require('https');
    var async = require('async');
    var qs = require('querystring');
    var util = require('util');
    var text = (req.body.text ? req.body.text.replace(/'/g, "''''") : '');
    var lrecipients = [];
    var mobiles = [];
    var res_done = false;

    if (req.body.contact_id !== undefined && req.body.contact_id != '') {
        if (util.isArray(req.body.contact_id)) {
            lrecipients.push(req.body.contact_id);
        } else {
            lrecipients.push(req.body.contact_id);
        }
    }

    if (lrecipients.length > 0) {
        var https = require('https');
        var cnt1 = 0;
        lrecipients.forEach(function(contact_to_push){
            sequelize.query('SELECT get_push_player_id(' +
                contact_to_push + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                var organization = req.headers['host'].split(".")[0];
                if (datas && datas[0] && datas[0].get_push_player_id && datas[0].get_push_player_id.length > 0) {
                    var cnt = 0;
                    var ids = [];
                    datas[0].get_push_player_id.forEach(function(player) {
                        ids.push(player);
                        if (++cnt > datas[0].get_push_player_id.length - 1) {

                            var message = {
                                app_id: config.onesignal.app_id,
                                contents: { "en": text },
                                web_url: 'https://' + organization + '.beebeeboard.com',
                                include_player_ids: ids
                            };

                            var headers = {
                                "Content-Type": "application/json; charset=utf-8",
                                "Authorization": "Basic " + config.onesignal.app_key
                            };

                            var options = {
                                host: "onesignal.com",
                                port: 443,
                                path: "/api/v1/notifications",
                                method: "POST",
                                headers: headers
                            };


                            var https = require('https');
                            var req1 = https.request(options, function(res1) {
                                res1.on('data', function(data) {
                                    if (++cnt1 > lrecipients.length - 1) {
                                        res.status(200).send({});
                                    }
                                });
                            });

                            req1.on('error', function(e) {
                                console.log("ERROR:");
                                console.log(e);
                            });

                            req1.write(JSON.stringify(message));
                            req1.end();

                            console.log('Push notification sended correctly');

                            
                        }
                    });

                } else {
                    console.log('Nessun account push');
                    if (++cnt1 > lrecipients.length - 1) {
                        res.status(200).send({});
                    }

                }
            }).catch(function(err) {
                return res.status(400).render('Send push notification: ' + err);
            });
        });
        
    }
};

exports.send_organization_mail = async function(req, res) {
    try {
        // Validazione dei dati della richiesta
        const {
            product_name = '',
            from_date = '',
            to_date = '',
            contact_name = '',
            contact_mail = '',
            contact_mobile = '',
            contact_message = '',
            color = '#00587D',
            thumbnail_url = ''
        } = req.body;

        let text = contact_message + '<br/>';

        if (contact_name !== '') {
            text += `<br/>Contatto: ${contact_name}`;
        }

        if (contact_mobile !== '') {
            text += `<br/>Numero telefono del contatto: ${contact_mobile}`;
        }

        if (contact_mail !== '') {
            text += `<br/>Mail del contatto: ${contact_mail}`;
        }

        if (contact_mail) {
            const contacts = await sequelize.query('SELECT select_organization_mail(?)', {
                replacements: [req.headers['host'].split(".")[0]],
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            });

            if (contacts.length > 0 && contacts[0]['select_organization_mail'][0]?.attributes?.mail) {
                const mail = [{
                    "email": contacts[0]['select_organization_mail'][0].attributes.mail,
                    "name": req.headers['host'].split(".")[0],
                    "type": "to"
                }];

                const template_content = [{
                    "name": "LANG",
                    "content": contacts[0]['select_organization_mail'][0].attributes.lang || 'it-it'
                }, {
                    "name": "OBJECT",
                    "content": 'Richiesta di informazioni'
                }, {
                    "name": "MESSAGE",
                    "content": text
                }, {
                    "name": "COLOR",
                    "content": color
                }, {
                    "name": "LOGO",
                    "content": thumbnail_url ? `https://data.beebeeboard.com/${thumbnail_url}-small.jpeg` : null
                }];

                const message = {
                    "global_merge_vars": template_content,
                    "metadata": {
                        "organization": req.headers['host'].split(".")[0]
                    },
                    "subject": 'Richiesta di informazioni',
                    "from_email": 'no-reply@beebeemailer.com',
                    "from_name": contact_name,
                    "to": mail,
                    "headers": {
                        "Reply-To": contact_mail
                    },
                    "tags": ["rapid-message", req.headers['host'].split(".")[0]],
                    "subaccount": "beebeeboard",
                    "tracking_domain": "mandrillapp.com",
                    "google_analytics_domains": ["poppix.it"],
                    "preserve_recipients": false
                };

                const result = await mandrill_client.messages.sendTemplate({
                    "template_name": "rapid-message",
                    "template_content": template_content,
                    "message": message,
                    "async": true,
                    "send_at": false
                });

                if (result && result[0] && ['rejected', 'invalid', 'error'].includes(result[0].status)) {
                    console.log("Errore nell'invio dell'email a Mandrill");
                    return res.status(422).send({});
                }

                console.log("Email inviata correttamente");
                return res.status(200).send({});
            } else {
                console.log("Indirizzo email dell'organizzazione non trovato");
                return res.status(422).send({});
            }
        } else {
            return res.status(200).send({});
        }
    } catch (error) {
        console.error("Errore durante l'invio dell'email:", error);
        return res.status(500).send({});
    }
};

exports.send_organization_mail_ = function(req, res) {
    var https = require('https');
    var async = require('async');
    var qs = require('querystring');
    var util = require('util');
    

    var product_name = req.body.product_name ? req.body.product_name : '';
    var from_date = req.body.from_date ? ' in periodo dal '+req.body.from_date : '';
    var to_date = req.body.to_date ? ' al '+req.body.to_date : '';

    var contact_name = req.body.contact_name ? ' '+req.body.contact_name : '';
    var contact_mail = req.body.contact_mail ? ' '+req.body.contact_mail : '';
    var contact_mobile = req.body.contact_mobile ? ' '+req.body.contact_mobile : '';

    var text = req.body.contact_message;

    text = text +'<br/>';

    if(contact_name != ''){
        text = text +'<br/>'+ 'Contatto: '+contact_name;
    }

    if(contact_mobile != ''){
        text = text +'<br/>'+ 'Numero telefono del contatto: '+contact_mobile;
    }

    if(contact_mail != ''){
        text = text +'<br/>'+ 'Mail del contatto: '+contact_mail;
    }
    

    if (req.body.contact_mail) {

        sequelize.query('SELECT select_organization_mail(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(contacts) {

                    if (contacts[0]['select_organization_mail'][0]) {
                      if (contacts[0]['select_organization_mail'][0].attributes.mail) {
                            
                            var mail = [{
                                    "email": contacts[0]['select_organization_mail'][0].attributes.mail,
                                    "name": req.headers['host'].split(".")[0],
                                    "type": "to"
                                }];
                            }

                            var template_content = [{
                                "name": "LANG",
                                "content": contacts[0]['select_organization_mail'][0].attributes.lang ? contacts[0]['select_organization_mail'][0].attributes.lang : 'it-it'
                            }, {
                                "name": "OBJECT",
                                "content": 'Richiesta di informazioni'
                            }, {
                                "name": "MESSAGE",
                                "content": text
                            }, {
                                "name": "COLOR",
                                "content": req.body.color ? req.body.color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": 'Richiesta di informazioni',
                                "from_email": 'no-reply@beebeemailer.com',
                                "from_name": req.body.contact_name,
                                "to": mail,
                                "headers": {
                                    "Reply-To": req.body.contact_mail
                                },
                                "tags": ["rapid-message",req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"],
                                "preserve_recipients": false
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "rapid-message",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                res.status(200).send({});

                            } else {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                res.status(422).send({});
                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                        });
                            
                    } else {
                        res.status(422).send({});
                    }
 
            });
    } else {
        res.status(200).send({});
    }
};

exports.webhooks = function(req, res) {

    console.log(req.body);
      
    if (req.body.order_id) {
        sequelize.query('SELECT insert_skebby(\'' +
             JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            console.log('Skebby webhook  ' + req.body.order_id);
        }).catch(function(err) {
            return res.status(400).render('insert_skebby: ' + err);
        });
    }

    

    res.send('OK');
};

exports.skebby_get_sms = function(req, res){
    var request = require('request');

   utility.login_skebby(function(err, auth) {
    if (!err) {
       
            var options = {
              'method': 'GET',
              'url': 'https://api.skebby.it/API/v1.0/REST/sms/'+req.query.order_id,
              'headers': {
                'user_key' : auth.user_key, 'Session_key' : auth.session_key 
              }
            };

            request(options, function (error, response) {
              if (error) throw new Error(error);
                 console.log(response.body);
                 res.status(200).send(response.body);
            });
        }
        else {
          return res.status(400).render('skebby send sms: ' + err);
        }
    });

}

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Read in Skebby a sms
 */
exports.read = function(req, res) {
    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);
    mandrill_client.messages.info({
        'id': req.params.id
    }, function(result) {
        res.send(result);
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
        res.json({
            'mails': 0
        });
    });
};