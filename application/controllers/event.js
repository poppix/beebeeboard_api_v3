var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    export_datas = require('../utility/exports.js'),
    finds = require('../utility/finds.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js'),
    config = require('../../config'),
    utils = require('../../utils');



exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.event_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('event', req, function(err, results) {
        if (err) {
            return res.status(400).render('event_custom: ' + err);
        }

        indexes.event(req, results, function(err, index) {
            if (err) {
                return res.status(400).render('event_index: ' + err);
            }

            /*if (index.meta.total_items > 1000) {
                return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
            } else {*/
                export_datas.event(req, results, function(err, response) {
                    if (err) {
                        return res.status(400).render('event_index: ' + err);
                    }

                    json2csv({
                        data: response.events,
                        fields: response.fields,
                        fieldNames: response.field_names
                    }, function(err, csv) {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=events.csv'
                        });
                        res.end(csv);
                    });
                });
            //}
        });
    });
};
/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    utility.customs('event', req, function(err, results) {

        indexes.event(req, results, function(err, response) {
            if (err) {
                return res.status(400).render('event_index: ' + err);
            }
            res.json(response);
        });

    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Find a specific event searched by id
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_event(req, function(err, results) {
        res.json(results);
    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * New event, send a notification in socket when created
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var event = {};

    if (req.body.data.attributes !== undefined) {
        // Import case
        if (req.body.data.attributes.start_date != null && !moment(req.body.data.attributes.start_date).isValid()) {
            return res.status(422).send(utility.error422('start_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        if (req.body.data.attributes.end_date != null && !moment(req.body.data.attributes.end_date).isValid()) {
            return res.status(422).send(utility.error422('end_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        if (req.body.data.attributes.updated_at != null && !moment(req.body.data.attributes.updated_at).isValid()) {
            return res.status(422).send(utility.error422('updated_at', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        event['owner_id'] = req.user.id;
        event['title'] = (req.body.data.attributes.title && utility.check_type_variable(req.body.data.attributes.title, 'string') && req.body.data.attributes.title !== null ? req.body.data.attributes.title.replace(/'/g, "''''") : '');
        event['start_date'] = req.body.data.attributes.start_date;
        event['updated_at'] = req.body.data.attributes.updated_at;
        event['end_date'] = req.body.data.attributes.end_date;
        event['all_day'] = req.body.data.attributes.all_day;
        event['color'] = (req.body.data.attributes.color && utility.check_type_variable(req.body.data.attributes.color, 'string') && req.body.data.attributes.color !== null ? req.body.data.attributes.color.replace(/'/g, "''''") : '');
        event['notes'] = (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
        event['_tenant_id'] = req.user._tenant_id;
        event['organization'] = req.headers['host'].split(".")[0];
        event['mongo_id'] = (req.body.data.attributes.mongo_id && utility.check_type_variable(req.body.data.attributes.mongo_id, 'string') && req.body.data.attributes.mongo_id !== null ? req.body.data.attributes.mongo_id.replace(/'/g, "''''") : '');
        event['players_number'] = (req.body.data.attributes.players_number !== null ? req.body.data.attributes.players_number : null);
        event['event_status_id'] = ((req.body.data.relationships && req.body.data.relationships.event_state && req.body.data.relationships.event_state.data && req.body.data.relationships.event_state.data.id && !utility.check_id(req.body.data.relationships.event_state.data.id)) ? req.body.data.relationships.event_state.data.id : null);
        event['archivied'] = (req.body.data.attributes.archivied !== undefined && req.body.data.attributes.archivied !== null ? req.body.data.attributes.archivied : false);
        event['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        event['price'] = (req.body.data.attributes.price ? req.body.data.attributes.price : 0.00);
        event['cost'] = (req.body.data.attributes.cost ? req.body.data.attributes.cost : 0.00);
        event['recursive_pattern'] = (req.body.data.attributes.recursive_pattern && utility.check_type_variable(req.body.data.attributes.recursive_pattern, 'string') && req.body.data.attributes.recursive_pattern !== null ? req.body.data.attributes.recursive_pattern.replace(/'/g, "''''") : '');
        event['is_dispo'] = (req.body.data.attributes.is_dispo !== undefined && req.body.data.attributes.is_dispo !== null ? req.body.data.attributes.is_dispo : false);

        sequelize.query('SELECT insert_event_v6(\'' +
                JSON.stringify(event) + '\',' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ',' +
                (req.body.data.attributes.recursive_pattern && utility.check_type_variable(req.body.data.attributes.recursive_pattern, 'string') && req.body.data.attributes.recursive_pattern !== null ? '\'' + req.body.data.attributes.recursive_pattern.replace(/'/g, "''''") + '\'' : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].insert_event_v6 && datas[0].insert_event_v6 != null) {
                    req.body.data.id = datas[0].insert_event_v6;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_event_v6,
                        'body': req.body.data.relationships,
                        'model': 'event',
                        'user_id': req.user.id,
                        'array': ['product', 'payment', 'project', 'address', 'contact', 'custom_field', 'reminder', 'workhour', 'related_event']
                    }, function(err, results) {
                        req.body.data.attributes.total_files_number = results.file;
                        sequelize.query('SELECT event_custom_fields_trigger(' +
                                datas[0].insert_event_v6 + ',' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(cus) {

                                req.params.id = datas[0].insert_event_v6;
                                finds.find_event(req, function(err, results) {
                                    logs.save_log_model(
                                        req.user._tenant_id,
                                        req.headers['host'].split(".")[0],
                                        'event',
                                        'insert',
                                        req.user.id,
                                        JSON.stringify(req.body.data.attributes),
                                        datas[0].insert_event_v6,
                                        'Aggiunto evento ' + datas[0].insert_event_v6
                                    );
                                    utility.save_socket(req.headers['host'].split(".")[0], 'event', 'insert', req.user.id, datas[0].insert_event_v6, results.data);
                        
                                    res.json(results);
                                });


                            }).catch(function(err) {
                                console.log(err);
                                return res.status(400).render('event_create: ' + err);
                            });
                    });
                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }
            }).catch(function(err) {
                console.log(err);
                return res.status(400).render('event_insert: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Update an existing event searched by id
 * When an event change we must change also all embedded documents related
 */
exports.update = function(req, res) {
    var recursive_rule = null;

    if (!req.user.role[0].json_build_object.attributes.event_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var event = {};

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.start_date != null && !moment(req.body.data.attributes.start_date).isValid()) {
        return res.status(422).send(utility.error422('start_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.end_date != null && !moment(req.body.data.attributes.end_date).isValid()) {
        return res.status(422).send(utility.error422('end_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.recursive_rule != null) {
        recursive_rule = req.body.data.attributes.recursive_rule;
    }

    finds.find_event(req, function(err, old_event) {

        sequelize.query('SELECT update_event_v7(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                '\'' + (req.body.data.attributes.title && utility.check_type_variable(req.body.data.attributes.title, 'string') && req.body.data.attributes.title !== null ? req.body.data.attributes.title.replace(/'/g, "''''") : '') + '\',' +
                '\'' + (req.body.data.attributes.start_date !== null ? req.body.data.attributes.start_date : '') + '\',' +
                '\'' + (req.body.data.attributes.end_date !== null ? req.body.data.attributes.end_date : '') + '\',' +
                '\'' + (req.body.data.attributes.color && utility.check_type_variable(req.body.data.attributes.color, 'string') && req.body.data.attributes.color !== null ? req.body.data.attributes.color.replace(/'/g, "''''") : '') + '\',' +
                '\'' + (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\',' +
                req.body.data.attributes.all_day + ',' +
                ((req.body.data.relationships && req.body.data.relationships.event_state && req.body.data.relationships.event_state.data && req.body.data.relationships.event_state.data.id && !utility.check_id(req.body.data.relationships.event_state.data.id)) ? req.body.data.relationships.event_state.data.id : null) + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.body.data.attributes.players_number + ',' +
                req.body.data.attributes.archivied + ',\'' +
                (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.price ? req.body.data.attributes.price : 0.00) + ',' +
                (req.body.data.attributes.cost ? req.body.data.attributes.cost : 0.00) + ',\'' +
                (req.body.data.attributes.recursive_pattern && utility.check_type_variable(req.body.data.attributes.recursive_pattern, 'string') && req.body.data.attributes.recursive_pattern !== null ? req.body.data.attributes.recursive_pattern.replace(/'/g, "''''") : '') + '\',' +
                (recursive_rule != null && recursive_rule != '' && recursive_rule != undefined ? ('\'' + recursive_rule + '\'') : null) + ',' +
                (req.body.data.attributes.is_dispo ? req.body.data.attributes.is_dispo : false) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(ev) {

                if (!ev[0].update_event_v7) {
                    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                }

                var old = old_event;
                sequelize.query('SELECT delete_event_relations_v1(' +
                        req.params.id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'event',
                            'user_id': req.user.id,
                            'array': ['address', 'reminder', 'file']
                        }, function(err, results) {

                            req.body.data.attributes.total_files_number = results.file;
                            finds.find_event(req, function(err, response) {

                                sequelize.query('SELECT update_recursive_relations(' +
                                        req.params.id + ',' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        (recursive_rule != null && recursive_rule != '' && recursive_rule != undefined ? ('\'' + recursive_rule + '\'') : null) + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        })
                                    .then(function(recursive) {
                                        logs.save_log_model(
                                            req.user._tenant_id,
                                            req.headers['host'].split(".")[0],
                                            'event',
                                            'update',
                                            req.user.id,
                                            JSON.stringify(req.body.data.attributes),
                                            req.params.id,
                                            'Modificato evento ' + req.params.id
                                        );
                                        utility.save_socket(req.headers['host'].split(".")[0], 'event', 'update', req.user.id, req.params.id, (response && response.data ? response.data : null));
                        
                                        res.json(response);
                                    });
                            }).catch(function(err) {
                                return res.status(400).render('update_event_recursive_relations: ' + err);
                            });
                        });
                    }).catch(function(err) {

                    });
            }).catch(function(err) {
                return res.status(400).render('event_update: ' + err);
            });
    });
};


exports.update_all = function(req, res) {
    var util = require('util');

    if(!util.isArray(req.body)){
        sequelize.query('SELECT update_event_all_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_event_all_v1'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_event_all_v1']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_event_all_v1'];
                finds.find_event(req, function(err, results) {
                    if (err) {
                        return res.status(err.errors[0].status).send(err);
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'event', 'update', req.user.id, req.params.id, results.data);
                        
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
    }else
    {
        sequelize.query('SELECT update_event_all_array(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_event_all_array'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_event_all_array']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                res.status(200).send({});
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });
    }  
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Delete an existing event searched by id
 */
exports.destroy = function(req, res) {
    var recursive_rule = null;

    if (!req.user.role[0].json_build_object.attributes.event_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.recursive_rule != null) {
        recursive_rule = req.query.recursive_rule;
    }

    sequelize.query('SELECT delete_event_v2(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            (recursive_rule != null && recursive_rule != '' && recursive_rule != undefined ? ('\'' + recursive_rule + '\'') : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if(!datas[0].delete_event_v2){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
            }
            else{
                utility.save_socket(req.headers['host'].split(".")[0], 'event', 'delete', req.user.id, req.params.id, null);
                        
                  res.json({
                    'data': {
                        'type': 'events',
                        'id': datas[0].delete_event_v2
                    }
                });
            }
          

        }).catch(function(err) {
            return res.status(400).render('event_destroy: ' + err);
        });
};

exports.sblocca_evento = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        event_id = req.body.event_id;

    sequelize.query('SELECT sblocca_evento(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            event_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});
        }).catch(function(err) {
            return res.status(400).render('sblocca_evento: ' + err);
        });
};

exports.add_event_invoice = function(req, res) {
    var util = require('util');
    var Sequelize = require('sequelize'),
        invoice_id = null,
        event_ids = [];

    if (req.body.event_id !== undefined && req.body.event_id != '') {
        if (util.isArray(req.body.event_id)) {
            event_ids = req.body.event_id;
        } else {
            event_ids.push(req.body.event_id);
        }
    }

    if (req.body.invoice_id !== undefined && req.body.invoice_id != '') {
        invoice_id = req.body.invoice_id;
        
    }

    sequelize.query('SELECT add_event_invoice(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            invoice_id + ',' +
            (event_ids.length > 0 ? 'ARRAY[' + event_ids + ']::bigint[]' : null) + ','+
            (req.body.force ? req.body.force : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if(datas && datas[0]['add_event_invoice'] == -2)
            {
                return res.status(422).send(utility.error422('Importi degli appuntamenti superiori al totale della fattura', 'Importi degli appuntamenti superiori al totale della fattura', '422'));
            }
            else if(datas && datas[0]['add_event_invoice'] == -1)
            {
                return res.status(422).send(utility.error422('Numero di appuntamenti superiori al numero di sedute nella fattura', 'Numero di appuntamenti superiori al numero di sedute nella fattura', '422'));
            }
            else {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                res.status(200).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('add_event_invoice: ' + err);
        });
};

exports.dispos_data = function(req, res) {
    var util = require('util');
    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            callback_(null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {
            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q1.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }
    }

    sequelize.query('SELECT dispos_data_v1(null,\'' +
            req.headers['host'].split(".")[0] + '\','+
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].dispos_data_v1 && datas[0].dispos_data_v1[0]){
                res.status(200).json(datas[0].dispos_data_v1[0]);
            }
            else{
                res.status(200).json({});
            }
            
        }).catch(function(err) {
            return res.status(400).render('dispos_data_v1: ' + err);
        });
};

exports.dispos = function(req, res) {
    var Sequelize = require('sequelize');
    var product_id = null,
        contact_id = null,
        start_date = null,
        end_date = null
        ;

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        start_date = '\''+req.query.from_date+'\'';
    }

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }  else{
        end_date = '\''+req.query.to_date+'\'';
    }

    if(req.query.product_id || req.query.contact_id){

        sequelize.query('SELECT dispos(\'' +
            req.headers['host'].split(".")[0] + '\','+
            (req.query.contact_id ? req.query.contact_id  : null )+','+
            (req.query.product_id ? req.query.product_id  : null) +','+
            start_date+','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
           
            if(datas && datas[0] && datas[0].dispos && datas[0].dispos[0]){
                res.status(200).json(datas[0].dispos[0]);
            }
            else{
                res.status(200).json({});
            }
            
        }).catch(function(err) {
            return res.status(400).render('dispos: ' + err);
        });
    }
};

exports.dispos_groups = function(req, res) {
    var Sequelize = require('sequelize');
    var product_id = null,
        contact_id = null,
        start_date = null,
        end_date = null;

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        start_date = '\''+req.query.from_date+'\'';
    }

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }  else{
        end_date = '\''+req.query.to_date+'\'';
    }

    if(req.query.product_id || req.query.contact_id){

        sequelize.query('SELECT dispos_groups(\'' +
            req.headers['host'].split(".")[0] + '\','+
            (req.query.contact_id ? req.query.contact_id  : null )+','+
            (req.query.product_id ? req.query.product_id  : null) +','+
            start_date+','+
            end_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
           
            if(datas && datas[0] && datas[0].dispos_groups && datas[0].dispos_groups[0]){
                res.status(200).json(datas[0].dispos_groups[0]);
            }
            else{
                res.status(200).json({});
            }
            
        }).catch(function(err) {
            return res.status(400).render('dispos_groups: ' + err);
        });
    }
};

exports.unit_dispo = function(req, res) {
    var start_date = null,
        end_date = null,
        q = null,
        contact_id = null,
        product_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

        sequelize.query('SELECT dispo_calendar(' +
            req.user._tenant_id + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.start_date+ ',' +
            parameters.end_date+ ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
            req.user.role_id+','+
            req.user.id+','+
            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            
            if (datas[0].dispo_calendar && datas[0].dispo_calendar != null && datas[0].dispo_calendar[0]) {
                res.status(200).send(datas[0].dispo_calendar[0]);

            } else {
                res.status(200).send('{}');
            }
        }).catch(function(err) {
            return res.status(400).render('datas[0].dispo_calendar: ' + err);
        });
     });  
};

exports.confirm_booking = function(req, res) {
    var bcrypt = require('bcryptjs'),
        config = require('../../config'),
        utils = require('../../utils'),
        SALT_WORK_FACTOR = 10;

    if (req.body.password && (
            !utility.check_type_variable(req.body.password, 'string') ||
            req.body.password == null ||
            req.body.password == '')
    ) {
        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    new_password = req.body.password;
    if(!req.body.password){
        sequelize.query('SELECT booking_confirm_v1(\'' +
                    req.headers['host'].split(".")[0] + '\',\''+
                    JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {

                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {

                     if(datas && datas[0] && datas[0].booking_confirm_v1 && datas[0].booking_confirm_v1){
                        var json_res = datas[0].booking_confirm_v1;

                        if(json_res.id != 404 && json_res.id != 422 && json_res.id != 403 && !req.body.user_id){
                             /* RITORNO A BOOKING L'ID DEL PAGAMENTO */
                                var token_id = utils.uid(config.token.accessTokenLength);
                                var mandrill = require('mandrill-api/mandrill');
                                
                                var create_user_expires = moment(new Date(moment())).add(7, 'days').format('YYYY/MM/DD HH:mm:ss'); // 1 day
                            
                        }

                        switch(json_res.id){
                            case 404:
                                return res.status(422).send(utility.error422('Non disponibile', 'Non disponibile', 'Non disponibile', '422'));
                            break;

                            case 422:
                                return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                            break;

                            case 403:
                                return res.status(422).send(utility.error422('Utente già esistente', 'Utente già esistente', 'Utente già esistente', '422'));
                            break;

                            case 0:
                                console.log(json_res.user_id);

                                
                                /* INVIO MAIL DI AVVENUTA PRENOTAZIONE */ 
                                var https = require('https');

                                var json_data = req.body;
                                var contact_id = json_data.contact_id;
                                var product_id = json_data.product_id;
                                var mail = json_data.mail;
                                var start_date = json_data.from_date.replace('+','%2B');
                                var end_date = json_data.to_date.replace('+','%2B');
                                var specialista_name = json_data.contact_name;
                                var specialista_mail = json_data.contact_mail;
                                var product_name = json_data.product_name;
                                var contact_name = json_data.first_name + ' '+json_data.last_name;
                                var model_id = json_res.model_id;

                                var urlPdf = 'https://asset.beebeeboard.com/utility/booking/booking_without_pay.php?contact_id'+contact_id+
                                    '&product_id='+product_id+
                                    '&start_date='+start_date+
                                    '&end_date='+end_date+
                                    '&organization='+req.headers['host'].split(".")[0]+
                                    '&mail='+mail+
                                    '&specialista_name='+specialista_name+
                                    '&specialista_mail='+specialista_mail+
                                    '&product_name='+product_name+
                                    '&contact_name='+contact_name+
                                    '&model_id='+model_id;

                                console.log(urlPdf);

                                var request = https.get(urlPdf, function(result) {

                                   res.status(200).send({'id':json_res.id});
                                }).on('error', function(err) {

                                    res.status(422).send({'id':json_res.id});
                                });

                                request.end();
               
                            break;

                            default:
                                /* RITORNO A BOOKING L'ID DEL PAGAMENTO */
                                var token = utils.uid(config.token.accessTokenLength);

                                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                    moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                    json_res.user_id + ',' +
                                    5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(tok) {

                                     res.status(200).send({'id':json_res.id, 'user_id':json_res.user_id, 'token': token,'payment_user_id':json_res.payment_user_id});
                                }).catch(function(err) {
                                    return res.status(400).render('insert_token: ' + err);
                                });
                            break;
                        }
                    }
                    else{
                       return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                    }
                    
                }).catch(function(err) {
                    return res.status(400).render('confirm_booking: ' + err);
                });
    }
    else if (req.body.password && (new_password !== null && new_password !== '' && new_password !== undefined)) {

        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if (err) {
                return res.status(400).render('user_registration: ' + err);
            }
            // hash the password using our new salt

            bcrypt.hash(new_password, salt, function(err, hash) {
                if (err) {
                    return res.status(400).render('user_registration: ' + err);
                }

                req.body.password = hash;

                sequelize.query('SELECT booking_confirm_v1(\'' +
                    req.headers['host'].split(".")[0] + '\',\''+
                    JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {

                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {

                     if(datas && datas[0] && datas[0].booking_confirm_v1 && datas[0].booking_confirm_v1){
                        var json_res = datas[0].booking_confirm_v1;


                        switch(json_res.id){
                            case 404:
                                return res.status(422).send(utility.error422('Non disponibile', 'Non disponibile', 'Non disponibile', '422'));
                            break;

                            case 422:
                                return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                            break;

                            case 403:
                                return res.status(422).send(utility.error422('Utente già esistente', 'Utente già esistente', 'Utente già esistente', '422'));
                            break;

                            case 0:
                                console.log(json_res.user_id);

                                
                                /* INVIO MAIL DI AVVENUTA PRENOTAZIONE */ 
                                var https = require('https');

                                var json_data = req.body;
                                var contact_id = json_data.contact_id;
                                var product_id = json_data.product_id;
                                var mail = json_data.mail;
                                var start_date = json_data.from_date.replace('+','%2B');
                                var end_date = json_data.to_date.replace('+','%2B');
                                var specialista_name = json_data.contact_name;
                                var specialista_mail = json_data.contact_mail;
                                var product_name = json_data.product_name;
                                var contact_name = json_data.first_name + ' '+json_data.last_name;
                                var model_id = json_res.model_id;

                                var urlPdf = 'https://asset.beebeeboard.com/utility/booking/booking_without_pay.php?contact_id'+contact_id+
                                    '&product_id='+product_id+
                                    '&start_date='+start_date+
                                    '&end_date='+end_date+
                                    '&organization='+req.headers['host'].split(".")[0]+
                                    '&mail='+mail+
                                    '&specialista_name='+specialista_name+
                                    '&specialista_mail='+specialista_mail+
                                    '&product_name='+product_name+
                                    '&contact_name='+contact_name+
                                    '&model_id='+model_id;

                                console.log(urlPdf);

                                var request = https.get(urlPdf, function(result) {

                                   res.status(200).send({'id':json_res.id});
                                }).on('error', function(err) {

                                    res.status(422).send({'id':json_res.id});
                                });

                                request.end();
               
                            break;

                            default:
                                /* RITORNO A BOOKING L'ID DEL PAGAMENTO */
                                var token = utils.uid(config.token.accessTokenLength);

                                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                    moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                    json_res.user_id + ',' +
                                    5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(tok) {

                                     res.status(200).send({'id':json_res.id, 'user_id':json_res.user_id, 'token': token,'payment_user_id':json_res.payment_user_id});
                                }).catch(function(err) {
                                    return res.status(400).render('insert_token: ' + err);
                                });
                            break;
                        }
                    }
                    else{
                       return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                    }
                    
                }).catch(function(err) {
                    return res.status(400).render('confirm_booking: ' + err);
                });
            });
        });
    }
    else {
        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Password non valida', '422'));
    }

};

exports.booking_calcola_prezzo = function(req, res) {
    sequelize.query('SELECT booking_calcola_prezzo_v1(\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['booking_calcola_prezzo_v1']) {
                res.status(200).send( datas[0]['booking_calcola_prezzo_v1']);
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.active_booking = function(req, res) {

    sequelize.query('SELECT get_active_booking(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].get_active_booking){
            res.status(200).send(datas[0].get_active_booking[0]);
        }else{
            res.status(422).send({});
        }
        
    }).catch(function(err) {
        return res.status(400).render('get_active_booking: ' + err);
    });
};

exports.save_active_booking = function(req, res) {
    var contact_ids = null;
    var util = require('util');
    if (req.body.contacts !== undefined && req.body.contacts != '') {
        if (util.isArray(req.body.contacts)) {
            contact_ids = req.body.contacts;
        } else {
            contact_ids.push(req.body.contacts);
        }
    }

    sequelize.query('SELECT save_active_booking(\'' +
        req.headers['host'].split(".")[0] + '\','+
        (contact_ids && contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].save_active_booking && datas[0].save_active_booking != -1){
            res.status(200).send(datas[0].save_active_booking);
        }else{
            res.status(422).send({});
        }
        
    }).catch(function(err) {
        return res.status(400).render('save_active_booking: ' + err);
    });
};

exports.promo_codes = function(req, res) {

    sequelize.query('SELECT get_promo_codes(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].get_promo_codes){
            res.status(200).send(datas[0].get_promo_codes[0]);
        }else{
            res.status(422).send({});
        }
        
    }).catch(function(err) {
        return res.status(400).render('get_promo_codes: ' + err);
    });
};

exports.save_promo_codes = function(req, res) {
    
   
    sequelize.query('SELECT save_promo_codes(\'' +
        req.headers['host'].split(".")[0] + '\',\''+
        JSON.stringify(req.body).replace(/'/g, "''''")+'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].save_promo_codes && datas[0].save_promo_codes != -1){
            res.status(200).send(datas[0].save_promo_codes);
        }else{
            res.status(422).send({});
        }
        
    }).catch(function(err) {
        return res.status(400).render('save_promo_codes: ' + err);
    });
};

exports.delete_promo_codes = function(req, res) {
    if(!req.params.id){
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT delete_promo_codes(' +
            req.params.id + ',\''+
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
        return res.status(400).render('delete_promo_codes: ' + err);
    });
};

exports.confirm_event = function(req,res){
    sequelize.query('SELECT booking_online_confirm_sms(\'' +
            req.headers['host'].split(".")[0] +'\','+
            req.user.id+','+
            req.user.role_id+','+
            req.query.event_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(boot) {
            if (boot[0].booking_online_confirm_sms && boot[0].booking_online_confirm_sms[0]) {
                res.status(200).send(boot[0].booking_online_confirm_sms[0]);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
    });
};

exports.confirm_event_post = function(req,res){
    var mail = null;
    var mails = [];
    var mandrill = require('mandrill-api/mandrill');

    sequelize.query('SELECT booking_online_confirm_sms(\'' +
            req.headers['host'].split(".")[0] +'\','+
            req.user.id+','+
            req.user.role_id+','+
            req.body.event_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(boot) {
            if (boot[0].booking_online_confirm_sms && boot[0].booking_online_confirm_sms[0]) {

                mail =  boot[0].booking_online_confirm_sms[0].information.mail;
                
                sequelize.query('SELECT booking_online_confirm_response(\'' +
                        req.headers['host'].split(".")[0] +'\','+
                        req.user.id+','+
                        req.user.role_id+','+
                        req.body.event_id+','+
                        req.body.action+',\''+
                        req.body.token+'\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster:true
                    }).then(function(book) {
                        console.log(req.body.action);
                        console.log(book[0].booking_online_confirm_response);
                        if (book[0].booking_online_confirm_response && book[0].booking_online_confirm_response && (req.body.action == false || req.body.action == 'false')) {
                                console.log('MAIL: '+mail);
                                if (mail !== '' && mail !== null && mail !== undefined) 
                                {

                                    mails.push({
                                        "email": mail,
                                        "type": "to"
                                    });
                                
                                    console.log(mails);
                                    var template_content = [{
                                        "name": "LANG",
                                        "content": 'IT'
                                    }, {
                                        "name": "OBJECT",
                                        "content": 'DISDETTA APPUNTAMENTO BEEBEEBOARD'
                                    }, {
                                        "name": "MESSAGE",
                                        "content": 'Il paziente '+boot[0].booking_online_confirm_sms[0].information.last_name+' '+boot[0].booking_online_confirm_sms[0].information.first_name+' ha disdetto l\'appuntamento del '+boot[0].booking_online_confirm_sms[0].information.data+' dalle '+boot[0].booking_online_confirm_sms[0].information.ora_inizio+' alle '+boot[0].booking_online_confirm_sms[0].information.ora_fine 
                                    }, {
                                        "name": "COLOR",
                                        "content": boot[0].booking_online_confirm_sms[0].information.color ? boot[0].booking_online_confirm_sms[0].information.color : '#00587D'
                                    }, {
                                        "name": "LOGO",
                                        "content": boot[0].booking_online_confirm_sms[0].information.thumbnail_url ? 'https://data.beebeeboard.com/' + boot[0].booking_online_confirm_sms[0].information.thumbnail_url + '-small.jpeg' : null
                                    }],
                                    message = {
                                        "global_merge_vars": template_content,
                                        "metadata": {
                                            "organization": req.headers['host'].split(".")[0]
                                        },
                                        "subject": 'DISDETTA APPUNTAMENTO BEEBEEBOARD',
                                        "from_email": boot[0].booking_online_confirm_sms[0].information.domain_from_mail ? boot[0].booking_online_confirm_sms[0].information.domain_from_mail : "no-reply@beebeemailer.com",
                                        "from_name": boot[0].booking_online_confirm_sms[0].information.organization_name ? boot[0].booking_online_confirm_sms[0].information.organization_name : req.headers['host'].split(".")[0],
                                        "to": mails,
                                        "headers": {
                                            "Reply-To": boot[0].booking_online_confirm_sms[0].information.domain_from_mail ? boot[0].booking_online_confirm_sms[0].information.domain_from_mail : 'no-reply@beebeemailer.com'
                                        },
                                        "tags": ["rapid-message",req.headers['host'].split(".")[0]],
                                        "subaccount": "beebeeboard",
                                        "tracking_domain": "mandrillapp.com",
                                        "google_analytics_domains": ["poppix.it"],
                                        "preserve_recipients": false
                                    };

                                    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                    mandrill_client.messages.sendTemplate({
                                        "template_name": "rapid-message",
                                        "template_content": template_content,
                                        "message": message,
                                        "async": true,
                                        "send_at": false
                                    }, function(result) {
                                        res.status(200).json({});
                                    }, function(e) {
                                        // Mandrill returns the error as an object with name and message keys
                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                        return res.status(422).send({});
                                    });
                                }
                                else{
                                    res.status(200).json({});
                                }
                            
                        }
                        else
                        {
                            res.status(200).send({});
                        }
                    }).catch(function(err) {
                        console.log(err);
                        return res.status(500).send(err);
                });
            }else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
    });
};

exports.get_group = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT event_get_group('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] +'\','+
            req.user.id+','+
            req.user.role_id+','+
            req.params.id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
            if (datas[0].event_get_group && datas[0].event_get_group) {
                res.status(200).send(datas[0].event_get_group[0]);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
    });
};

exports.group_add_people = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
   

    sequelize.query('SELECT group_add_people('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] +'\','+
            req.user.id+','+
            req.user.role_id+','+
            req.body.event_id+','+
            req.body.contact_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(datas) {
            if (datas[0].group_add_people && datas[0].group_add_people) {
                res.status(200).send(datas[0].group_add_people[0]);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
    });
};

exports.group_add_pack = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
   

    sequelize.query('SELECT group_add_pack('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] +'\','+
            req.user.id+','+
            req.user.role_id+','+
            req.body.event_id+','+
            req.body.pkg_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(datas) {
            if (datas[0].group_add_pack && datas[0].group_add_pack) {
                res.status(200).send(datas[0].group_add_pack[0]);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
    });
};

exports.group_del_people = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    

    sequelize.query('SELECT group_del_people('+
            req.user._tenant_id+',\'' +
            req.headers['host'].split(".")[0] +'\','+
            req.user.id+','+
            req.user.role_id+','+
            req.body.event_id+','+
            req.body.contact_id+','+
            req.body.group_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        }).then(function(datas) {
            if (datas[0].group_del_people && datas[0].group_del_people) {
                res.status(200).send(datas[0].group_del_people[0]);
            }
            else
            {
                res.status(404).send({});
            }
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
    });
};

exports.booking_bonifico = function(req, res) {

    var payment_id = null;
    var mandrill = require('mandrill-api/mandrill');
    var nome_banca = req.body.dati_banca_nome;
    var iban = req.body.dati_banca_iban;
    var swift = req.body.dati_banca_swift;
    var filiale = req.body.dati_banca_filiale;

    if (req.body.payment_id !== undefined && req.body.payment_id !== '') {
        if (utility.check_id(req.body.payment_id)) {
            callback_(null, utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        payment_id = req.body.payment_id;
    }

    sequelize.query('SELECT scuole_ecommerce_bonifico(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',' +
        payment_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

        sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {

                var mails = [{
                    "email": req.body.mail,
                    "type": "to"
                }];

                var mail = datas_o[0].search_organization.ecommerce_mail;

                if (mail !== '' && mail !== null && mail !== undefined) {
                    mails.push({
                        "email": mail,
                        "type": "to"
                    });
                }

                var template_content = [{
                        "name": "LANG",
                        "content": datas_o[0].search_organization.lang
                    }, {
                        "name": "ORGANIZATION",
                        "content": uc_first(datas_o[0].search_organization.name)
                    }, {
                        "name": "COLOR",
                        "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                    }, {
                        "name": "LOGO",
                        "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                    }, {
                        "name": "CLIENTE",
                        "content": req.body.nome ? req.body.nome : ''
                    }, {
                        "name": "IBAN",
                        "content": iban ? iban : ''
                    }, {
                        "name": "SWIFT",
                        "content": swift ? swift : ''
                    }, {
                        "name": "FILIALE",
                        "content": filiale ? filiale : ''
                    }, {
                        "name": "BANCA",
                        "content": nome_banca ? nome_banca : ''
                    }, {
                        "name": "IMPORTO",
                        "content": utility.formatta_euro(req.body.importo)
                    }],
                    message = {
                        "global_merge_vars": template_content,
                        "metadata": {
                            "organization": req.headers['host'].split(".")[0]
                        },
                        "subject": uc_first(datas_o[0].search_organization.name) + ' - Dati bonifico',
                        "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                        "from_name": uc_first(datas_o[0].search_organization.name),
                        "to": mails,
                        "headers": {
                            "Reply-To": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                        },
                        
                        "tags": ["mail_bonifico",req.headers['host'].split(".")[0]],
                        "subaccount": "beebeeboard",
                        "tracking_domain": "mandrillapp.com",
                        "google_analytics_domains": ["poppix.it"]
                    };

                console.log(template_content);

                console.log(message);
                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                mandrill_client.messages.sendTemplate({
                    "template_name": "mail-bonifico",
                    "template_content": template_content,
                    "message": message,
                    "async": true,
                    "send_at": false
                }, function(result) {
                    console.log(result);
                    res.status(200).json({});
                }, function(e) {
                    // Mandrill returns the error as an object with name and message keys
                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                    return res.status(422).send({});
                });
                   
        }).catch(function(err) {
            return res.status(400).render('mandrill-booking bonifico search organization: ' + err);
        });
    }).catch(function(err) {
        return res.status(400).render('mandrill-booking bonifico update payment: ' + err);
    });
};

exports.booking_get_voucher = function(req, res) {

    if(req.body.code && req.body.code !== ''){
        sequelize.query('SELECT booking_get_voucher(\'' +
            req.headers['host'].split(".")[0] + '\',\''+
            req.body.code+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            
            if(datas && datas[0] && datas[0].booking_get_voucher && datas[0].booking_get_voucher && datas[0].booking_get_voucher != '{}'){
                res.status(200).send(datas[0].booking_get_voucher);
            }else{
                res.status(404).send({});
            }
            
        }).catch(function(err) {
            return res.status(400).render('booking_get_voucher: ' + err);
        });
    }
    else
    {
         res.status(404).send({});
    }
};

exports.booking_in_loco = function(req, res) {

    var payment_id = null;
    var mandrill = require('mandrill-api/mandrill');
    var nome_banca = req.body.dati_banca_nome;
    var iban = req.body.dati_banca_iban;
    var swift = req.body.dati_banca_swift;
    var filiale = req.body.dati_banca_filiale;

    if (req.body.payment_id !== undefined && req.body.payment_id !== '') {
        if (utility.check_id(req.body.payment_id)) {
            callback_(null, utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        payment_id = req.body.payment_id;
    }

    sequelize.query('SELECT scuole_ecommerce_in_loco(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',' +
        payment_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {
            res.status(200).send({});
        
    }).catch(function(err) {
        return res.status(400).render('mandrill-booking bonifico update payment: ' + err);
    });
};


exports.booking_use_voucher = function(req, res) {
    var bcrypt = require('bcryptjs'),
        config = require('../../config'),
        utils = require('../../utils'),
        SALT_WORK_FACTOR = 10;

    if (req.body.password && (
            !utility.check_type_variable(req.body.password, 'string') ||
            req.body.password == null ||
            req.body.password == '')
    ) {
        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    new_password = req.body.password;
    if(!req.body.password){
        sequelize.query('SELECT booking_use_voucher(\'' +
                    req.headers['host'].split(".")[0] + '\',\''+
                    JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {

                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {

                     if(datas && datas[0] && datas[0].booking_use_voucher && datas[0].booking_use_voucher){
                        var json_res = datas[0].booking_use_voucher;
                       
                        switch(json_res.id){
                            case 404:
                                return res.status(422).send(utility.error422('Non disponibile', 'Non disponibile', 'Non disponibile', '422'));
                            break;

                            case 422:
                                return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                            break;

                            case 403:
                                return res.status(422).send(utility.error422('Utente già esistente', 'Utente già esistente', 'Utente già esistente', '422'));
                            break;

                           default:
                                console.log(json_res.user_id);

                                var token = utils.uid(config.token.accessTokenLength);

                                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                    moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                    json_res.payment_user_id + ',' +
                                    5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(tok) {

                                            /* INVIO MAIL DI AVVENUTA PRENOTAZIONE */ 
                                        var https = require('https');

                                        var json_data = req.body;
                                        var contact_id = json_data.contact_id;

                                        var product_id = json_data.product_id;
                                        var mail = json_data.mail;
                                        var start_date = json_data.from_date.replace('+','%2B');
                                        var end_date = json_data.to_date.replace('+','%2B');
                                        var specialista_name = json_data.contact_name;
                                        var specialista_mail = json_data.contact_mail;
                                        var product_name = json_data.product_name;
                                        var contact_name = json_data.first_name + ' '+json_data.last_name;
                                        var model_id = json_res.model_id;
                                        

                                        var urlPdf = 'https://asset.beebeeboard.com/utility/booking/success_booking.php?use_voucher=true&contact_id'+contact_id+

                                            '&access_token='+token+
                                            '&payment_id='+json_res.payment_id+
                                            '&product_id='+product_id+
                                            '&start_date='+start_date+
                                            '&end_date='+end_date+
                                            '&organization='+req.headers['host'].split(".")[0]+
                                            '&mail='+mail+
                                            '&specialista_name='+specialista_name+
                                            '&specialista_mail='+specialista_mail+
                                            '&product_name='+product_name+
                                            '&contact_name='+contact_name+
                                            '&model_id='+model_id;
                                       
                                        console.log(urlPdf);

                                        var request = https.get(urlPdf, function(result) {

                                           res.status(200).send({'id':json_res.id});
                                        }).on('error', function(err) {

                                            res.status(422).send({'id':json_res.id});
                                        });

                                        request.end();

                                    
                                }).catch(function(err) {
                                    return res.status(400).render('insert_token: ' + err);
                                });
                                
               
                            break;

                           
                        }
                    }
                    else{
                       return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                    }
                    
                }).catch(function(err) {
                    return res.status(400).render('confirm_booking: ' + err);
                });
    }
    else if (req.body.password && (new_password !== null && new_password !== '' && new_password !== undefined)) {

        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if (err) {
                return res.status(400).render('user_registration: ' + err);
            }
            // hash the password using our new salt

            bcrypt.hash(new_password, salt, function(err, hash) {
                if (err) {
                    return res.status(400).render('user_registration: ' + err);
                }

                req.body.password = hash;

                sequelize.query('SELECT booking_use_voucher(\'' +
                    req.headers['host'].split(".")[0] + '\',\''+
                    JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {

                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {

                     if(datas && datas[0] && datas[0].booking_use_voucher && datas[0].booking_use_voucher){
                        var json_res = datas[0].booking_use_voucher;


                        switch(json_res.id){
                            case 404:
                                return res.status(422).send(utility.error422('Non disponibile', 'Non disponibile', 'Non disponibile', '422'));
                            break;

                            case 422:
                                return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                            break;

                            case 403:
                                return res.status(422).send(utility.error422('Utente già esistente', 'Utente già esistente', 'Utente già esistente', '422'));
                            break;

                           default:
                                console.log(json_res.user_id);

                                  var token = utils.uid(config.token.accessTokenLength);

                                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                    moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                    json_res.user_id + ',' +
                                    5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(tok) {

                                    /* INVIO MAIL DI AVVENUTA PRENOTAZIONE */ 
                                    var https = require('https');

                                    var json_data = req.body;
                                    var contact_id = json_data.contact_id;
                                    var product_id = json_data.product_id;
                                    var mail = json_data.mail;
                                    var start_date = json_data.from_date.replace('+','%2B');
                                    var end_date = json_data.to_date.replace('+','%2B');
                                    var specialista_name = json_data.contact_name;
                                    var specialista_mail = json_data.contact_mail;
                                    var product_name = json_data.product_name;
                                    var contact_name = json_data.first_name + ' '+json_data.last_name;
                                    var model_id = json_res.model_id;

                                  var urlPdf = 'https://asset.beebeeboard.com/utility/booking/success_booking.php?use_voucher=true&contact_id'+contact_id+
                                            '&access_token='+token+
                                            '&payment_id='+json_res.payment_id+
                                            '&product_id='+product_id+
                                            '&start_date='+start_date+
                                            '&end_date='+end_date+
                                            '&organization='+req.headers['host'].split(".")[0]+
                                            '&mail='+mail+
                                            '&specialista_name='+specialista_name+
                                            '&specialista_mail='+specialista_mail+
                                            '&product_name='+product_name+
                                            '&contact_name='+contact_name+
                                            '&model_id='+model_id;
                                           


                                    console.log(urlPdf);
                                    

                                    var request = https.get(urlPdf, function(result) {

                                       res.status(200).send({'id':json_res.id});
                                    }).on('error', function(err) {

                                        res.status(422).send({'id':json_res.id});
                                    });

                                    request.end();
                                });
                            break;

                           
                        }
                    }
                    else{
                       return res.status(422).send(utility.error422('Parametro', 'Parametro non valido', 'Parametro non valido', '422'));
                    }
                    
                }).catch(function(err) {
                    return res.status(400).render('confirm_booking: ' + err);
                });
            });
        });
    }
    else {
        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Password non valida', '422'));
    }

};

function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};