function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function canvasReqDati(invalidMail = false, invalidOrg = false, stored_data = {}) {
  var canvas = {
      content: {
        components: [
          {
            "type": "text",
            "text": "*REGISTRATI SUBITO*",
            "style": "header"
          },
          {
            "type": "single-select",
            "id": "product",
            "value": stored_data.product ? stored_data.product : "beebeehealth",
            "options": [
              {
                "type": "option",
                "id": "beebeeboard",
                "text": "Beebeeboard"
              },
              {
                "type": "option",
                "id": "beebeehealth",
                "text": "Beebeehealth"
              }
            ]
          },
          {
            "type": "text",
            "text": "*Beebeeboard* Gestionale per PMI\n*Beebeehealth* Gestionale medico",
            "style": "muted"
          },
          {
            "type": "input",
            "id": "organization",
            "label": "Nome società o il tuo nome",
            "save_state": "unsaved",
            "placeholder": "Nome società",
            "value": stored_data.organization,
            "save_state": invalidOrg ? "failed" : "unsaved"
          },
          
        ], 
      }
    };
  
  if(invalidOrg) {
    canvas.content.components.push({
      "type":  "text",
      "text":  (typeof invalidOrg === 'string') ? invalidOrg : "Nome società non valido",
      "style": "error"
    });
  }
  
  canvas.content.components.push({
    "type": "input",
    "id": "mail",
    "label": "Mail",
    "save_state": "unsaved",
    "placeholder": "john@company.com",
    "value": stored_data.mail,
    "save_state": invalidMail ? "failed" : "unsaved"
  });
  
  if(invalidMail) {
      canvas.content.components.push({
        "type":  "text",
        "text":  "Mail non valida",
        "style": "error"
      });
  }
  
  canvas.content.components.push({
    "type": "spacer",
    "size": "m"
    },{
    type: "button",
    label: "PROCEDI",
    style: "primary", 
    id: "url_button",
    action: {
      type: "submit"
    }
  });
  
  canvas.stored_data = stored_data;
  return canvas;
};

function canvasReqDPrivacy(terminiAccettati, privacyAccettata, stored_data) {
  var canvas = {
      content: {
        components: [
          {
            "type": "text",
            "text": "*REGISTRATI SUBITO*",
            "style": "header"
          },
          {
            "type":  "text",
            "text":  "Accetto i [Termini e condizioni](https://beebeeboard.com/termini-e-condizioni).",
            "style": "paragraph"
          },
          {
            "type": "single-select",
            "id": "termini",
            "value": terminiAccettati ? terminiAccettati : null,
            "options": [
              {
                "type": "option",
                "id": "non-accetto",
                "text": "Non accetto"
              },
              {
                "type": "option",
                "id": "accetto",
                "text": "Accetto"
              }
            ]
          }
        ], 
      }
    };

    if(terminiAccettati && terminiAccettati !== 'accetto') {
      canvas.content.components.push({
        "type":  "text",
        "text":  "Devi accettare i Termini e condizioni",
        "style": "error"
      });
    }
  
    canvas.content.components.push({
        "type":  "text",
        "text":  "Accetto la [Normativa privacy](https://beebeeboard.com/normativa-privacy/).",
        "style": "paragraph"
      },
      {
        "type": "single-select",
        "id": "privacy",
        "value": privacyAccettata ? privacyAccettata : null,
        "options": [
          {
            "type": "option",
            "id": "non-accetto",
            "text": "Non accetto"
          },
          {
            "type": "option",
            "id": "accetto",
            "text": "Accetto"
          }
        ]
      });

    if(privacyAccettata && privacyAccettata !== 'accetto') {
      canvas.content.components.push({
        "type":  "text",
        "text":  "Devi accettare la privacy",
        "style": "error"
      });
    }
  
    canvas.content.components.push({
      "type": "spacer",
      "size": "m"
    }, {
      type: "button",
      label: "INIZIA LA MIA PROVA!",
      style: "primary", 
      id: "url_button",
      action: {
        type: "submit"
      }
    });
  
  canvas.stored_data = stored_data;
  
  return canvas;
};

exports.intercom_initialize = function(request, response) {
    const body = request.body;  
  response.send({
    canvas: canvasReqDati()
  });
};

exports.intercom_submit = function(request, response) {
    const body = request.body;
  
  var organization = (body.input_values && body.input_values.organization) ? body.input_values.organization : (body.current_canvas && body.current_canvas.stored_data ? body.current_canvas.stored_data.organization : null);
  var mail = (body.input_values && body.input_values.mail) ? body.input_values.mail : (body.current_canvas && body.current_canvas.stored_data ? body.current_canvas.stored_data.mail : null);
  var product = (body.input_values && body.input_values.product) ? body.input_values.product : (body.current_canvas && body.current_canvas.stored_data ? body.current_canvas.stored_data.product : null);
  
  var stored_data = {
    "organization": organization,
    "mail": mail,
    "product": product
  };
  console.log(stored_data);
  var invalidMail = validateEmail(mail);
  var invalidOrg = (organization && organization !== '');
  
  if(!invalidMail || !invalidOrg) {
    console.log('STEP 1 DATI NON VALIDI');
    return response.send({
      canvas: canvasReqDati(invalidMail ? false : true, invalidOrg ? false : true, stored_data)
    });
  } else if(organization && mail) {
    var terminiAccettati = body.input_values.termini;
    var privacyAccettata = body.input_values.privacy;
    console.log(terminiAccettati);
    if(terminiAccettati === 'accetto' && privacyAccettata === 'accetto') {
      console.log('FINE REGISTRAZIONE');
      var url = product === 'beebeeboard' ? 'https://appv4.beebeeboard.com/api/v1/organizations' : 'https://appv4.beebeeboard.com/api/v1/clinic';

      var data = {
          'data': {
              'attributes': {
                  'organization': organization.toLowerCase().replace(/[^A-Z0-9]+/ig, ''),
                  'mail': mail,
                  'lang': 'it'
              }
          }
      };

      var request = require('request');
      request({
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        },
        uri: url,
        body: JSON.stringify(data),
        method: 'POST'
      }, function (err, result, body) {
        var json_body = JSON.parse(body);
        if(result.statusCode !== 200) {   
            var isOrgError= json_body.errors[0].source.pointer === '/data/attributes/organization' ? true : false;
            var isMailError= json_body.errors[0].source.pointer === '/data/attributes/mail' ? true : false;
          
            return response.send({
              canvas: canvasReqDati(isMailError ? json_body.errors[0].detail : false, isOrgError ? json_body.errors[0].detail : false, stored_data)
            });
        } else {
          var json_body = JSON.parse(body);
          console.log('REGISTRAZIONE COMPLETATA');
          return response.send({
            canvas: {
              content: {
                components: [
                  {
                    "type": "image",
                    "align": "center",
                    "url": "https://sitejerk.com/images/green-check-mark-icon-transparent-background-7.png",
                    "width": 70,
                    "height": 70
                  },
                  {
                    "type": "text",
                    "text": "*REGISTRAZIONE COMPLETATA*",
                    "align": "center",
                    "style": "header"
                  },
                  {
                    "type": "text",
                    "text": "Riceverai a breve una mail a " + json_body.data.attributes.mail,
                    "align": "center",
                    "style": "muted"
                  }
                ]
              }
            }
          }); 
        }
      });
      
    } else {
      console.log('CHIEDO DI ACCETTARE I TERMINI');
        return response.send({
        canvas: canvasReqDPrivacy(terminiAccettati, privacyAccettata, stored_data)
      });
    }  
  }
}