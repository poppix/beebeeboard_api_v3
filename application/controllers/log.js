var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.contacts = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_contact', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_contacts_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_contacts_v1 && datas[0].log_contacts_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_contacts_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_contacts_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_contacts_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_contacts: ' + err);
            });
    });
};

exports.products = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_product', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_products_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_products_v1 && datas[0].log_products_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_products_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_products_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_products_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_products: ' + err);
            });
    });
};

exports.projects = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_project', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_projects_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_projects_v1 && datas[0].log_projects_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_projects_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_projects_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_projects_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_projects: ' + err);
            });
    });
};

exports.events = function(req, res) {


    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_event', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_events_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_events_v1 && datas[0].log_events_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_events_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_events_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_events_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_events: ' + err);
            });
    });
};

exports.invoices = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_invoice', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_invoices_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_invoices_v1 && datas[0].log_invoices_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_invoices_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_invoices_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_invoices_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_invoices: ' + err);
            });
    });
};

exports.expenses = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_expense', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_expenses_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_expenses_v1 && datas[0].log_expenses_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_expenses_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_expenses_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_expenses_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_expenses: ' + err);
            });
    });
};

exports.workhours = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_workhour', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_workhours_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_workhours_v1 && datas[0].log_workhours_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_workhours_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_workhours_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_workhours_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_workhours: ' + err);
            });
    });
};

exports.payments = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_payment', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_payments_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_payments_v1 && datas[0].log_payments_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_payments_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_payments_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_payments_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_payments: ' + err);
            });
    });
};

exports.todolists = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'log_todolist', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT log_todolists_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.params.id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas[0].log_todolists_v1 && datas[0].log_todolists_v1.length > 0) {
                    res.json({
                        'data': datas[0].log_todolists_v1,
                        'meta': {
                            'total_items': parseInt(datas[0].log_todolists_v1[0].totals),
                            'actual_page': parseInt(parameters.page_num),
                            'total_pages': parseInt(datas[0].log_todolists_v1[0].totals / parameters.page_count) + 1,
                            'item_per_page': parseInt(parameters.page_count)
                        }
                    });
                } else {
                    res.json({
                        'data': {}
                    })
                }
            }).catch(function(err) {
                return res.status(400).render('log_todolists: ' + err);
            });
    });
};

exports.users = function(req, res) {
    var start_date = null,
        end_date = null;

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT log_users(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            req.params.id + ',' +
            start_date + ',' +
            end_date + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].log_users) {
                res.json({
                    'data': datas[0].log_users[0]
                });
            } else {
                res.json({
                    'data': {}
                })
            }

        }).catch(function(err) {
            return res.status(400).render('log_users: ' + err);
        });
};

exports.logins = function(req, res) {
    var start_date = null,
        end_date = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT log_logins(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            start_date + ',' +
            end_date + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].log_logins) {
                res.json({
                    'data': datas[0].log_logins[0]
                });
            } else {
                res.json({
                    'data': {}
                })
            }

        }).catch(function(err) {
            return res.status(400).render('log_logins: ' + err);
        });
};

exports.save_log = function(req, res) {
    logs.save_log_model(
        req.user._tenant_id,
        req.headers['host'].split(".")[0],
        req.body.model,
        req.body.operation,
        req.user.id,
        JSON.stringify(req.body.attributes),
        req.body.created_id,
        req.body.description
    );
    res.status(200).send({});
};