var Sequelize = require('sequelize'),
    moment = require('moment'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    export_datas = require('../utility/exports.js'),
    finds = require('../utility/finds.js'),
    indexes = require('../utility/indexes.js'),
    logs = require('../utility/logs.js');

exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.expense_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    indexes.expense(req, null, function(err, index) {
        if (err) {
            return res.status(400).render('expense_index: ' + err);
        }
        /*if (index.meta.total_items > 5000) {
            return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
        } else {*/

            export_datas.expense(req, null, function(err, response) {
                if (err) {
                    return res.status(400).render('expense_export: ' + err);
                }

                json2csv({
                    data: response.expenses,
                    fields: response.fields,
                    fieldNames: response.field_names
                }, function(err, csv) {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=expenses.csv'
                    });
                    res.end(csv);
                });
            });
        //}
    });
};

exports.stats = function(req, res) {

    utility.get_parameters_v1(req, 'expense_stats', function(err, parameters) {
        if (err) {
            callback_(err, null);
        }

        sequelize.query('SELECT expenses_stats_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                parameters.all_any + ',' +
                parameters.archivied + ',' +
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ','+
                parameters.success + ','+
                (parameters.from_ids.length > 0 ? 'ARRAY[' + parameters.from_ids + ']::bigint[]' : null) + ',' +
                (parameters.category_id && parameters.category_id.length > 0 ? 'ARRAY[' + parameters.category_id + ']::bigint[]' : null) + ',' +
                (parameters.sub_category_id && parameters.sub_category_id.length > 0 ? 'ARRAY[' + parameters.sub_category_id + ']::bigint[]' : null) + ');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].expenses_stats_v1
                });
            }).catch(function(err) {
                return res.status(400).render('expense_stats: ' + err);
            });
    });
};

exports.categories_charts = function(req, res) {
    var start = null,
        end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT expense_categories_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if (datas[0].expense_categories_chart[0] && datas[0].expense_categories_chart[0].Categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].expense_categories_chart[0].Categorie.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name].push(cat.tot);
                    if (cnt++ >= datas[0].expense_categories_chart[0].Categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('expense_categories_chart: ' + err);
        });
};

exports.annual_categories_charts = function(req, res) {
    var start = null,
        end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT expense_annual_categories_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if (datas[0].expense_annual_categories_chart[0] && datas[0].expense_annual_categories_chart[0].categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].expense_annual_categories_chart[0].categorie.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name] = cat.array_agg;
                    if (cnt++ >= datas[0].expense_annual_categories_chart[0].categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('expense_annual_categories_chart: ' + err);
        });
};

exports.sub_categories_charts = function(req, res) {
    var start = null,
        end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT expense_sub_categories_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',\'' +
            req.query.category +
            '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if (datas[0].expense_sub_categories_chart[0] && datas[0].expense_sub_categories_chart[0].sub_categorie) {
                var cnt = 0;
                var categorie = {};
                datas[0].expense_sub_categories_chart[0].sub_categorie.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name].push(cat.tot);
                    if (cnt++ >= datas[0].expense_sub_categories_chart[0].sub_categorie.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('expense_sub_categories_chart: ' + err);
        });
};


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    indexes.expense(req, null, function(err, response) {
        if (err) {
            return res.status(400).render('expense_index: ' + err);
        }
        res.json(response);
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific expense searched by id
 */
exports.find = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var sortField = (req.query.sort === undefined) ? 'name' : req.query.sort,
        sortDirection = (req.query.direction === undefined) ? 'asc' : req.query.direction;

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_expense(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New expense, important to set reminders when created
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var expense = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        expense['owner_id'] = req.user.id;

        //expense['product_id'] = (req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data !== null && req.body.data.relationships.product.data.id !== null && !utility.check_id(req.body.data.relationships.product.data.id) ? req.body.data.relationships.product.data.id : null);
        //expense['project_id'] = (req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data !== null && req.body.data.relationships.project.data.id !== null && !utility.check_id(req.body.data.relationships.project.data.id) ? req.body.data.relationships.project.data.id : null);
        expense['date'] = (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : null);
        expense['ref_number'] = (req.body.data.attributes.ref_number && utility.check_type_variable(req.body.data.attributes.ref_number, 'string') ? req.body.data.attributes.ref_number.replace(/'/g, "''''") : null);
        expense['status'] = (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') ? req.body.data.attributes.status.replace(/'/g, "''''") : null);
        expense['first_rate'] = req.body.data.attributes.first_rate;
        expense['first_rate_included'] = req.body.data.attributes.first_rate_included;
        expense['description'] = (req.body.data.attributes.description !== null && utility.check_type_variable(req.body.data.attributes.description, 'string') ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        expense['total'] = req.body.data.attributes.total;
        expense['is_paid'] = req.body.data.attributes.is_paid;
        expense['paid_date'] = (req.body.data.attributes.paid_date && moment(req.body.data.attributes.paid_date).isValid() ? req.body.data.attributes.paid_date : null);
        expense['fourth_rate'] = req.body.data.attributes.fourth_rate;
        expense['notes'] = (req.body.data.attributes.notes !== null && utility.check_type_variable(req.body.data.attributes.notes, 'string') ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
        expense['expensecategory'] = (req.body.data.attributes.expensecategory !== null ? req.body.data.attributes.expensecategory.replace(/'/g, "''''") : '');
        expense['expensecategory_name'] = (req.body.data.attributes.expensecategory_name !== null && utility.check_type_variable(req.body.data.attributes.expensecategory_name, 'string') ? req.body.data.attributes.expensecategory_name.replace(/'/g, "''''") : '');
        expense['thumbnail_url'] = (req.body.data.attributes.thumbnail_url !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') ? req.body.data.attributes.thumbnail_url.replace(/'/g, "''''") : '');
        expense['thumbnail_name'] = (req.body.data.attributes.thumbnail_name !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') ? req.body.data.attributes.thumbnail_name.replace(/'/g, "''''") : '');
        expense['thumbnail_etag'] = (req.body.data.attributes.thumbnail_etag !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') ? req.body.data.attributes.thumbnail_etag.replace(/'/g, "''''") : '');
        expense['thumbnail_type'] = (req.body.data.attributes.thumbnail_type !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') ? req.body.data.attributes.thumbnail_type.replace(/'/g, "''''") : '');
        expense['_tenant_id'] = req.user._tenant_id;
        expense['organization'] = req.headers['host'].split(".")[0];
        expense['mongo_id'] = null;
        expense['archivied'] = req.body.data.attributes.archivied;
        expense['imponibile'] = req.body.data.attributes.imponibile;
        expense['esigibilita_iva'] = (req.body.data.attributes.esigibilita_iva && utility.check_type_variable(req.body.data.attributes.esigibilita_iva, 'string') && req.body.data.attributes.esigibilita_iva !== null ? req.body.data.attributes.esigibilita_iva.replace(/'/g, "''''") : '');
        expense['valuta'] = (req.body.data.attributes.valuta && utility.check_type_variable(req.body.data.attributes.valuta, 'string') && req.body.data.attributes.valuta !== null ? req.body.data.attributes.valuta.replace(/'/g, "''''") : '');
        expense['tasso_cambio'] = req.body.data.attributes.tasso_cambio;
        expense['society_id'] = ((req.body.data.relationships && req.body.data.relationships.society && req.body.data.relationships.society.data && req.body.data.relationships.society.data.id && !utility.check_id(req.body.data.relationships.society.data.id)) ? req.body.data.relationships.society.data.id : null);
        expense['contact_name'] = (req.body.data.attributes.contact_name && utility.check_type_variable(req.body.data.attributes.contact_name, 'string') && req.body.data.attributes.contact_name !== null ? req.body.data.attributes.contact_name.replace(/'/g, "''''") : '');
        expense['contact_piva'] = (req.body.data.attributes.contact_piva && utility.check_type_variable(req.body.data.attributes.contact_piva, 'string') && req.body.data.attributes.contact_piva !== null ? req.body.data.attributes.contact_piva.replace(/'/g, "''''") : '');
        expense['society'] = (req.body.data.attributes.society && utility.check_type_variable(req.body.data.attributes.society, 'string') && req.body.data.attributes.society !== null ? req.body.data.attributes.society.replace(/'/g, "''''") : '');
        expense['society_piva'] = (req.body.data.attributes.society_piva && utility.check_type_variable(req.body.data.attributes.society_piva, 'string') && req.body.data.attributes.society_piva !== null ? req.body.data.attributes.society_piva.replace(/'/g, "''''") : '');
        expense['contact_address_id'] = ((req.body.data.relationships && req.body.data.relationships.contact_address && req.body.data.relationships.contact_address.data && req.body.data.relationships.contact_address.data.id) && !utility.check_id(req.body.data.relationships.contact_address.data.id) ? req.body.data.relationships.contact_address.data.id : null);
        expense['society_address_id'] = ((req.body.data.relationships && req.body.data.relationships.society_address && req.body.data.relationships.society_address.data && req.body.data.relationships.society_address.data.id) && !utility.check_id(req.body.data.relationships.society_address.data.id) ? req.body.data.relationships.society_address.data.id : null);
        expense['contact_first_name'] = (req.body.data.attributes.contact_first_name && utility.check_type_variable(req.body.data.attributes.contact_first_name, 'string') && req.body.data.attributes.contact_first_name !== null ? req.body.data.attributes.contact_first_name.replace(/'/g, "''''") : '');
        expense['contact_last_name'] = (req.body.data.attributes.contact_last_name && utility.check_type_variable(req.body.data.attributes.contact_last_name, 'string') && req.body.data.attributes.contact_last_name !== null ? req.body.data.attributes.contact_last_name.replace(/'/g, "''''") : '');
        expense['society_first_name'] = (req.body.data.attributes.society_first_name && utility.check_type_variable(req.body.data.attributes.society_first_name, 'string') && req.body.data.attributes.society_first_name !== null ? req.body.data.attributes.society_first_name.replace(/'/g, "''''") : '');
        expense['society_last_name'] = (req.body.data.attributes.society_last_name && utility.check_type_variable(req.body.data.attributes.society_last_name, 'string') && req.body.data.attributes.society_last_name !== null ? req.body.data.attributes.society_last_name.replace(/'/g, "''''") : '');
        expense['is_creditnote'] = req.body.data.attributes.is_creditnote;
        expense['fisica_giuridica'] = req.body.data.attributes.fisica_giuridica;
        expense['contact_cf'] = (req.body.data.attributes.contact_cf && utility.check_type_variable(req.body.data.attributes.contact_cf, 'string') && req.body.data.attributes.contact_cf !== null ? req.body.data.attributes.contact_cf.replace(/'/g, "''''") : '');
        expense['society_cf'] = (req.body.data.attributes.society_cf && utility.check_type_variable(req.body.data.attributes.society_cf, 'string') && req.body.data.attributes.society_cf !== null ? req.body.data.attributes.society_cf.replace(/'/g, "''''") : '');
        expense['contact_fisica_giuridica'] = req.body.data.attributes.contact_fisica_giuridica;
        expense['bank_name'] = (req.body.data.attributes.bank_name && utility.check_type_variable(req.body.data.attributes.bank_name, 'string') && req.body.data.attributes.bank_name !== null ? req.body.data.attributes.bank_name.replace(/'/g, "''''") : '');
        expense['bank_iban'] = (req.body.data.attributes.bank_iban && utility.check_type_variable(req.body.data.attributes.bank_iban, 'string') && req.body.data.attributes.bank_iban !== null ? req.body.data.attributes.bank_iban.replace(/'/g, "''''") : '');
        expense['bank_bic'] = (req.body.data.attributes.bank_bic && utility.check_type_variable(req.body.data.attributes.bank_bic, 'string') && req.body.data.attributes.bank_bic !== null ? req.body.data.attributes.bank_bic.replace(/'/g, "''''") : '');
        expense['discount'] = req.body.data.attributes.discount;
        expense['discount_type'] = req.body.data.attributes.discount_type;
        expense['discount_rate'] = req.body.data.attributes.discount_rate;
        expense['causale'] = (req.body.data.attributes.causale && utility.check_type_variable(req.body.data.attributes.causale, 'string') && req.body.data.attributes.causale !== null ? req.body.data.attributes.causale.replace(/'/g, "''''") : '');
        expense['lordo'] = req.body.data.attributes.lordo;
        
        sequelize.query('SELECT insert_expense_v4(\'' + JSON.stringify(expense) + '\',' + req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].insert_expense_v4 && datas[0].insert_expense_v4 != null) {
                    req.body.data.id = datas[0].insert_expense_v4;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_expense_v4,
                        'body': req.body.data.relationships,
                        'model': 'expense',
                        'user_id': req.user.id,
                        'array': ['payment', 'contact', 'address', 'reminder', 'expensearticle', 'expire_date', 'product', 'project', 'event']
                    }, function(err, results) {

                        req.params.id = datas[0].insert_expense_v4;
                        finds.find_expense(req, function(err, response) {
                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'expense',
                                'insert',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                datas[0].insert_expense_v4,
                                'Aggiunto acquisto ' + datas[0].insert_expense_v4
                            );

                            utility.save_socket(req.headers['host'].split(".")[0], 'expense', 'insert', req.user.id, datas[0].insert_expense_v4, response.data);
        

                            res.json(response);
                        });


                    });

                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }
            }).catch(function(err) {
                return res.status(400).render('expense_insert: ' + err);
            });


    } else {
        res.json({
            'data': []
        });
    }
};

exports.change_expense_status = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.body.expense_id)) {
        return res.status(422).send(utility.error422('expense_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!req.body.status || req.body.status == null) {
        return res.status(422).send(utility.error422('status', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT expense_change_status(' +
        req.body.expense_id + ',\'' +
        req.body.status + '\',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(datas) {
        res.status(200).send({});

    }).catch(function(err) {
        return res.status(400).render('expense_change_status: ' + err);
    });

};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update expense, important to set reminders when updated
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    var expense = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        if (req.body.data.attributes.description != '' && req.body.data.attributes.description != undefined) {
            if (!utility.check_type_variable(req.body.data.attributes.description, 'string')) {
                return res.status(422).send(utility.error422('description', 'Invalid Attribute', 'Inserire una descrizione valida', '422'));
            }
        }

    }

    finds.find_expense(req, function(err, old_expense) {

        sequelize.query('SELECT update_expense_v5(\'' +
                //(req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data !== null && req.body.data.relationships.product.data.id !== null ? req.body.data.relationships.product.data.id : null) + ',\'' +
                (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : '') + '\',\'' +
                (req.body.data.attributes.ref_number && utility.check_type_variable(req.body.data.attributes.ref_number, 'string') && req.body.data.attributes.ref_number !== null ? req.body.data.attributes.ref_number : '') + '\',\'' +
                (req.body.data.attributes.status && utility.check_type_variable(req.body.data.attributes.status, 'string') && req.body.data.attributes.status !== null ? req.body.data.attributes.status : '') + '\',' +
                req.body.data.attributes.first_rate + ',' +
                req.body.data.attributes.first_rate_included + ',\'' +
                (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') + '\',' +
                req.body.data.attributes.total + ',' +
                req.body.data.attributes.is_paid + ',' +
                (req.body.data.attributes.paid_date && moment(req.body.data.attributes.paid_date).isValid() && req.body.data.attributes.paid_date !== null ? '\'' + req.body.data.attributes.paid_date + '\'' : null) + ',' +
                req.body.data.attributes.fourth_rate + ',\'' +
                (req.body.data.attributes.notes && utility.check_type_variable(req.body.data.attributes.notes, 'string') && req.body.data.attributes.notes !== null ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.expensecategory && utility.check_type_variable(req.body.data.attributes.expensecategory, 'string') && req.body.data.attributes.expensecategory !== null ? req.body.data.attributes.expensecategory.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.expensecategory_name && utility.check_type_variable(req.body.data.attributes.expensecategory_name, 'string') && req.body.data.attributes.expensecategory_name !== null ? req.body.data.attributes.expensecategory_name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_url && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') && req.body.data.attributes.thumbnail_url !== null ? req.body.data.attributes.thumbnail_url : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_name && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') && req.body.data.attributes.thumbnail_name !== null ? req.body.data.attributes.thumbnail_name : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_etag && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') && req.body.data.attributes.thumbnail_etag !== null ? req.body.data.attributes.thumbnail_etag : '') + '\',\'' +
                (req.body.data.attributes.thumbnail_type && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') && req.body.data.attributes.thumbnail_type !== null ? req.body.data.attributes.thumbnail_type : '') + '\',' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                //(req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data !== null && req.body.data.relationships.project.data.id !== null && !utility.check_id(req.body.data.relationships.project.data.id) ? req.body.data.relationships.project.data.id : null) + ',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                req.body.data.attributes.archivied +',' +
                req.body.data.attributes.imponibile +',' +
                req.body.data.attributes.tasso_cambio + ',\'' +
                (req.body.data.attributes.esigibilita_iva && utility.check_type_variable(req.body.data.attributes.esigibilita_iva, 'string') && req.body.data.attributes.esigibilita_iva !== null ? req.body.data.attributes.esigibilita_iva.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.valuta && utility.check_type_variable(req.body.data.attributes.valuta, 'string') && req.body.data.attributes.valuta !== null ? req.body.data.attributes.valuta.replace(/'/g, "''''") : '') + '\','+
                ((req.body.data.relationships && req.body.data.relationships.society && req.body.data.relationships.society.data && req.body.data.relationships.society.data.id && !utility.check_id(req.body.data.relationships.society.data.id)) ? req.body.data.relationships.society.data.id : null)+ ',\''+
                (req.body.data.attributes.contact_name && utility.check_type_variable(req.body.data.attributes.contact_name, 'string') && req.body.data.attributes.contact_name !== null ? req.body.data.attributes.contact_name.replace(/'/g, "''''") : '')+ '\',\''+
                (req.body.data.attributes.contact_piva && utility.check_type_variable(req.body.data.attributes.contact_piva, 'string') && req.body.data.attributes.contact_piva !== null ? req.body.data.attributes.contact_piva.replace(/'/g, "''''") : '')+ '\',\''+
                (req.body.data.attributes.society && utility.check_type_variable(req.body.data.attributes.society, 'string') && req.body.data.attributes.society !== null ? req.body.data.attributes.society.replace(/'/g, "''''") : '')+ '\',\''+
                (req.body.data.attributes.society_piva && utility.check_type_variable(req.body.data.attributes.society_piva, 'string') && req.body.data.attributes.society_piva !== null ? req.body.data.attributes.society_piva.replace(/'/g, "''''") : '')+ '\','+
                ((req.body.data.relationships && req.body.data.relationships.contact_address && req.body.data.relationships.contact_address.data && req.body.data.relationships.contact_address.data.id) && !utility.check_id(req.body.data.relationships.contact_address.data.id) ? req.body.data.relationships.contact_address.data.id : null)+ ','+
                ((req.body.data.relationships && req.body.data.relationships.society_address && req.body.data.relationships.society_address.data && req.body.data.relationships.society_address.data.id) && !utility.check_id(req.body.data.relationships.society_address.data.id) ? req.body.data.relationships.society_address.data.id : null)+ ',\''+
                (req.body.data.attributes.contact_first_name && utility.check_type_variable(req.body.data.attributes.contact_first_name, 'string') && req.body.data.attributes.contact_first_name !== null ? req.body.data.attributes.contact_first_name.replace(/'/g, "''''") : '')+ '\',\''+
                (req.body.data.attributes.contact_last_name && utility.check_type_variable(req.body.data.attributes.contact_last_name, 'string') && req.body.data.attributes.contact_last_name !== null ? req.body.data.attributes.contact_last_name.replace(/'/g, "''''") : '')+ '\',\''+
                (req.body.data.attributes.society_first_name && utility.check_type_variable(req.body.data.attributes.society_first_name, 'string') && req.body.data.attributes.society_first_name !== null ? req.body.data.attributes.society_first_name.replace(/'/g, "''''") : '')+ '\',\''+
                (req.body.data.attributes.society_last_name && utility.check_type_variable(req.body.data.attributes.society_last_name, 'string') && req.body.data.attributes.society_last_name !== null ? req.body.data.attributes.society_last_name.replace(/'/g, "''''") : '')+ '\','+
                req.body.data.attributes.is_creditnote+',\''+
                (req.body.data.attributes.contact_cf && utility.check_type_variable(req.body.data.attributes.contact_cf, 'string') && req.body.data.attributes.contact_cf !== null ? req.body.data.attributes.contact_cf.replace(/'/g, "''''") : '')+'\',\''+
                (req.body.data.attributes.society_cf && utility.check_type_variable(req.body.data.attributes.society_cf, 'string') && req.body.data.attributes.society_cf !== null ? req.body.data.attributes.society_cf.replace(/'/g, "''''") : '')+'\','+
                req.body.data.attributes.fisica_giuridica+','+
                req.body.data.attributes.contact_fisica_giuridica+','+
                req.body.data.attributes.discount+','+
                req.body.data.attributes.discount_type+','+
                req.body.data.attributes.discount_rate+',\''+
                (req.body.data.attributes.bank_name && utility.check_type_variable(req.body.data.attributes.bank_name, 'string') && req.body.data.attributes.bank_name !== null ? req.body.data.attributes.bank_name.replace(/'/g, "''''") : '')+'\',\''+
                (req.body.data.attributes.bank_iban && utility.check_type_variable(req.body.data.attributes.bank_iban, 'string') && req.body.data.attributes.bank_iban !== null ? req.body.data.attributes.bank_iban.replace(/'/g, "''''") : '')+'\',\''+
                (req.body.data.attributes.bank_bic && utility.check_type_variable(req.body.data.attributes.bank_bic, 'string') && req.body.data.attributes.bank_bic !== null ? req.body.data.attributes.bank_bic.replace(/'/g, "''''") : '')+'\','+
                req.body.data.attributes.lordo+',\''+
                (req.body.data.attributes.causale && utility.check_type_variable(req.body.data.attributes.causale, 'string') && req.body.data.attributes.causale !== null ? req.body.data.attributes.causale.replace(/'/g, "''''") : '')+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(ex) {
                if (!ex[0].update_expense_v5) {
                    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                }

                var old = old_expense;
                sequelize.query('SELECT delete_expense_relations_v1(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'expense',
                            'user_id': req.user.id,
                            'array': ['payment', 'address', 'reminder', 'expensearticle', 'file', 'expire_date']
                        }, function(err, results) {


                            finds.find_expense(req, function(err, response) {

                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'expense',
                                    'update',
                                    req.user.id,
                                    JSON.stringify(req.body.data.attributes),
                                    req.params.id,
                                    'Aggiunto acquisto ' + req.params.id
                                );

                                utility.save_socket(req.headers['host'].split(".")[0], 'expense', 'udpate', req.user.id, req.params.id, response.data);
        
                                res.json(response);
                            });
                        });

                    }).catch(function(err) {
                        return res.status(400).render('expense_update: ' + err);
                    });

            }).catch(function(err) {
                return res.status(400).render('expense_update: ' + err);
            })
    });
};


exports.update_all = function(req, res) {
   
    sequelize.query('SELECT update_expense_all(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_expense_all'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_expense_all']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_expense_all'];
                finds.find_expense(req, function(err, results) {
                    if (err) {
                        return res.status(err.errors[0].status).send(err);
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'expense', 'update', req.user.id, req.params.id, results.data);
                        
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.expense_credit = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var expense_id = null;

    if (req.body.id !== undefined) {
        expense_id = req.body.id;
    }


    sequelize.query('SELECT expense_to_creditnote(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            expense_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {


            res.json({ 'id': datas[0].expense_to_creditnote });



        }).catch(function(err) {
            return res.status(400).render('expense_to_creditnote: ' + err);
        });



};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete expense searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.expense_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT delete_expense_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
             if(!datas[0].delete_expense_v1){
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
             }else{

                utility.save_socket(req.headers['host'].split(".")[0], 'expense', 'delete', req.user.id, req.params.id, null);
                        
                res.json({
                    'data': {
                        'type': 'expenses',
                        'id': datas[0].delete_expense_v1
                    }
                });
            }

        }).catch(function(err) {
            return res.status(400).render('expense_destroy: ' + err);
        });

};


exports.autofattura = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    sequelize.query('SELECT autofattura(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +                
                req.body.id + ',\''+
                (req.body.tipo_documento ? req.body.tipo_documento.replace(/'/g, "''''") : '' )+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
            })
        .then(function(datas1) {


            logs.save_log_model(
                req.user._tenant_id,
                req.headers['host'].split(".")[0],
                'autofattura',
                'insert',
                req.user.id,
                JSON.stringify('{}'),
                req.body.id,
                'Generata Autofattura da acquisto' + req.body.id
            );



            res.json({ 'id': datas1[0].autofattura, 'document_type': 'autofattura' });



        }).catch(function(err) {
            return res.status(400).render('autofattura: ' + err);
        });

                               
};

exports.storno_autofattura = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.invoice_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    sequelize.query('SELECT autofattura_storno(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +                
                req.body.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
            })
        .then(function(datas1) {


            logs.save_log_model(
                req.user._tenant_id,
                req.headers['host'].split(".")[0],
                'autofattura',
                'insert',
                req.user.id,
                JSON.stringify('{}'),
                req.body.id,
                'Stornata Autofattura da acquisto' + req.body.id
            );



            res.status(200).send({ 'id': datas1[0].autofattura_storno, 'document_type': 'autofattura' });



        }).catch(function(err) {
            return res.status(400).render('autofattura: ' + err);
        });

                               
};