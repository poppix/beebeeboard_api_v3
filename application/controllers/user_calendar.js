var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

    var user_calendar = {};

    if (req.body.data.attributes !== undefined) {

        user_calendar['calendar_rank'] = req.body.data.attributes.calendar_rank;
        user_calendar['_tenant_id'] = req.user._tenant_id;
        user_calendar['organization'] = req.headers['host'].split(".")[0];
        user_calendar['mongo_id'] = null;
        user_calendar['user_id'] = ((req.body.data.relationships && req.body.data.relationships.user && req.body.data.relationships.user.data && req.body.data.relationships.user.data.id && !utility.check_id(req.body.data.relationships.user.data.id)) ? req.body.data.relationships.user.data.id : null);
        user_calendar['user_to_show'] = ((req.body.data.relationships && req.body.data.relationships.user_to_show && req.body.data.relationships.user_to_show.data && req.body.data.relationships.user_to_show.data.id && !utility.check_id(req.body.data.relationships.user_to_show.data.id)) ? req.body.data.relationships.user_to_show.data.id : null);

        sequelize.query('SELECT insert_user_calendar(\'' +
                JSON.stringify(user_calendar) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_user_calendar;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'user_calendar',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_user_calendar,
                    'Aggiunto user_calendar ' + datas[0].insert_user_calendar
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('user_calendar_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        })
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_user_calendar(' +
                req.params.id + ',' +
                req.body.data.attributes.calendar_rank + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                ((req.body.data.relationships && req.body.data.relationships.user && req.body.data.relationships.user.data && req.body.data.relationships.user.data.id && !utility.check_id(req.body.data.relationships.user.data.id)) ? req.body.data.relationships.user.data.id : null) + ',' +
                ((req.body.data.relationships && req.body.data.relationships.user_to_show && req.body.data.relationships.user_to_show.data && req.body.data.relationships.user_to_show.data.id && !utility.check_id(req.body.data.relationships.user_to_show.data.id)) ? req.body.data.relationships.user_to_show.data.id : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'user_calendar',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato user_calendar ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('user_calendar_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_user_calendar_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'user_calendars',
                    'id': datas[0].delete_user_calendar_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('user_calendar_destroy: ' + err);
        });
};

exports.google_calendar_list = function(req, res) {
   var organization = req.headers['host'].split(".")[0];
   var contact_id = req.query.contact_id;
   var request = require('request');

    async.parallel({
        token: function(callback) {
            sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
                .then(function(datas) {


                    if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                        if(tok.refresh_token) {

                            request.post('https://accounts.google.com/o/oauth2/token', {
                                form: {
                                    grant_type: 'refresh_token',
                                    refresh_token: tok.refresh_token,
                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                }
                            }, function(err, res, body) {
                                body = JSON.parse(body);
                                sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                    '\', \'' + body.access_token + '\',' + contact_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster:true
                                    }).then(function(datas) {
                                        callback(null, body.access_token);

                                });
                            });
                        }
                        else {

                            callback(null, null);
                        }
                    } else {

                        callback(null, null);
                    }
                });
        },
        calendar: function(callback){
            sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
                .then(function(datas) {


                    if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                        if (tok.calendar_id) {
                            callback(null, tok.calendar_id);
                        } 
                        else {
                            callback(null, null);
                        }
                    } else {
                        callback(null, null);
                    }
                });
        },
        org: function(callback) {
            callback(null, organization);
        },
        contact:  function(callback) {
            callback(null, contact_id);
        }
    }, function(err, results) {

        if (results.token) {
           
            var headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + results.token
            }

            // Configure the request
            var options = {
                url: 'https://www.googleapis.com/calendar/v3/users/me/calendarList?minAccessRole=owner',
                method: 'GET',
                headers: headers
            }

            request(options, function(error, response, body) {
                
                if (!error && response.statusCode == 200) {

                    
                    var body_json = JSON.parse(body);
                    if(results.calendar){
                        body_json.actual_calendar = results.calendar;
                    }
                   
                    res.status(200).send(body_json);
                } else {
                    console.log(error);
                    res.status(404).send({});
                }
            });
               
        }

    });
};

exports.save_google_calendar = function(req, res) {

    var organization = req.headers['host'].split(".")[0];
    var contact_id = req.body.contact_id;
    var calendar_id = req.body.calendar_id;
    var request = require('request');

    console.log(req.body);

    async.parallel({
        token: function(callback) {
            
            sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
                .then(function(datas) {


                    if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                        if(tok.refresh_token) {

                            request.post('https://accounts.google.com/o/oauth2/token', {
                                form: {
                                    grant_type: 'refresh_token',
                                    refresh_token: tok.refresh_token,
                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                }
                            }, function(err, res, body) {
                                body = JSON.parse(body);
                                sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                    '\', \'' + body.access_token + '\',' + contact_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster:true
                                    }).then(function(datas) {
                                        callback(null, body.access_token);

                                });
                            });
                        }
                        else {

                            callback(null, null);
                        }
                    } else {

                        callback(null, null);
                    }
                });     
        },
        old_calendar: function(callback){
            sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
                .then(function(datas) {

                    if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                        
                            callback(null, tok);
                       
                    } else {
                        callback(null, null);
                    }
                });
        },
        new_calendar: function(callback){
            callback(null, calendar_id);
        },
        org: function(callback) {
            callback(null, organization);
        },
        contact:  function(callback) {
            callback(null, contact_id);
        }
    }, function(err, results) {

        console.log(results);

        if (results.token) {

            if(results.old_calendar.calendar_id && results.old_calendar.calendar_id == results.new_calendar){
                res.status(200).send({});
            }
            else{
                
                /* STOP del vecchio calendario */
                console.log('ENTRATO');
                var headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + results.token
                }

                var options = {
                    url: 'https://www.googleapis.com/calendar/v3/channels/stop',
                    method: 'POST',
                    headers: headers,
                    json: JSON.parse('{"id": "gc-beebeeboard-notification-'+results.org+'","resourceId":"'+results.old_calendar.resource+'"}')
                }
                // Start the request
                 
                request(options, function(error1, response1, body1) {
                    console.log(options);
                    console.log('367');
                    console.log(body1);
                    if (error1) {
                        console.log(error1);
                    }


                    /* DELETE del vecchio calendario */
                    sequelize.query('SELECT delete_google_token(\'' + results.org +
                        '\', ' + results.contact + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(err, datas) {
                        if (err) {
                            console.log(err);
                        }


                         var headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + results.token
                            }

                            // Configure the request
                            var options = {
                                url: 'https://www.googleapis.com/calendar/v3/calendars/' + results.new_calendar + '/events?alt=json',
                                method: 'GET',
                                headers: headers
                            }
                            // Start the request
                            
                            request(options, function(error1, response1, body1) {
                                console.log(options);
                                console.log('400');
                                if (!error1 && response1.statusCode == 200) {
                                    // Print out the response body
                                    console.log(JSON.parse(body1).nextSyncToken);
                                    if(JSON.parse(body1).nextSyncToken && JSON.parse(body1).nextSyncToken !== 'undefined'){
                                        sequelize.query('SELECT insert_google_calendar_token(\'' + results.org +
                                            '\', \'' + results.token + '\', \'' + results.old_calendar.refresh_token + '\',' + results.contact + ',\'' +
                                            results.old_calendar.mail + '\',\'' + results.new_calendar + '\',\'' + JSON.parse(body1).nextSyncToken + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster:true
                                            }).then(function(err, datas) {
                                            if (err) {
                                                console.log(err);
                                            }

                                           
                                            var headers2 = {
                                                'Content-Type': 'application/json',
                                                'Authorization': 'Bearer ' + results.token
                                            };

                                            // Configure the request
                                            var options2 = {
                                                url: 'https://www.googleapis.com/calendar/v3/calendars/' + results.new_calendar + '/events/watch?alt=json&showdeleted=true',
                                                method: 'POST',
                                                headers: headers2,
                                                json: JSON.parse('{"id": "gc-beebeeboard-notification-' + results.org + '","type": "web_hook","address": "https://google-calendar.beebeeboard.com/api/v1/calendar_notification" }')
                                            };

                                           
                                            // Start the request
                                            request(options2, function(error, response2, body2) {
                                                console.log(options2);
                                                console.log('432');
                                                console.log(body2);
                                                if (!error && response2.statusCode == 200) {
                                                    
                                                    if(body2){
                                                        
                                                        sequelize.query('SELECT update_google_calendar_watch(\'' + results.org +
                                                            '\', ' + results.contact + ',\'' + results.new_calendar + '\',\'' + 
                                                            body2.resourceId + '\',\'' + 
                                                            body2.resourceUri + '\',\'' + 
                                                            body2.expiration + '\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster:true
                                                        }).then(function(err, datas) {
                                                             res.status(200).send({});
                                                        });
                                                    }
                                                    
                                                } else {
                                                    console.log('WATCH:'+error);
                                                    res.status(422).send({});
                                                }
                                            });

                                           
                                        });
                                    }
                                    else{
                                         console.log('ENTRO QUI');
                                          async.parallel({

                                            sync_: function(callback1){
                                                let calendar = {
                                                    "calendar_id": results.new_calendar,
                                                    "organization": results.org 
                                                };

                                                let tok = {
                                                    'access_token': results.token,
                                                    'contact_id':results.contact
                                                };

                                                callback1(null,google_calendar_sync_token(calendar, tok, null, null));
                                            }
                                         }, function(err, results1) {

                                             sequelize.query('SELECT insert_google_calendar_token(\'' + results.org +
                                                '\', \'' + results.token + '\', \'' + results.old_calendar.refresh_token + '\',' + results.contact + ',\'' +
                                                results.old_calendar.mail + '\',\'' + results.new_calendar + '\',\'' + results1.sync_ + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster:true
                                                }).then(function(err, datas) {
                                                if (err) {
                                                    console.log(err);
                                                }

                                               
                                                var headers2 = {
                                                    'Content-Type': 'application/json',
                                                    'Authorization': 'Bearer ' + results.token
                                                };

                                                // Configure the request
                                                var options2 = {
                                                    url: 'https://www.googleapis.com/calendar/v3/calendars/' + results.new_calendar + '/events/watch?alt=json&showdeleted=true',
                                                    method: 'POST',
                                                    headers: headers2,
                                                    json: JSON.parse('{"id": "gc-beebeeboard-notification-' + results.org + '","type": "web_hook","address": "https://google-calendar.beebeeboard.com/api/v1/calendar_notification" }')
                                                };

                                               
                                                // Start the request
                                                request(options2, function(error, response2, body2) {
                                                    console.log(options2);
                                                    console.log('432');
                                                    console.log(body2);
                                                    if (!error && response2.statusCode == 200) {
                                                        
                                                        if(body2){
                                                            
                                                            sequelize.query('SELECT update_google_calendar_watch(\'' + results.org +
                                                                '\', ' + results.contact + ',\'' + results.new_calendar + '\',\'' + 
                                                                body2.resourceId + '\',\'' + 
                                                                body2.resourceUri + '\',\'' + 
                                                                body2.expiration + '\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster:true
                                                            }).then(function(err, datas) {
                                                                 res.status(200).send({});
                                                            });
                                                        }
                                                        
                                                    } else {
                                                        console.log('WATCH:'+error);
                                                        res.status(422).send({});
                                                    }
                                                });

                                               
                                            });
                                        });
                                            
                                    }
                                }else
                                {
                                    console.log('NEW_CAL'+error1);
                                     res.status(422).send({});
                                }
                            });
                    });
                    
                    
                });
               
            }
            
        
        }else
        {
             res.status(422).send({});
        }

    });  
};

exports.import_from_google_to_beebee = function(req, res) {

    var organization = req.headers['host'].split(".")[0];
    var calendar_id = req.body.calendar_id;
    var contact_id = req.body.contact_id;
    var request = require('request');

    async.parallel({
        token: function(callback) {
            
            sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
                .then(function(datas) {


                    if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                        if(tok.refresh_token) {

                            request.post('https://accounts.google.com/o/oauth2/token', {
                                form: {
                                    grant_type: 'refresh_token',
                                    refresh_token: tok.refresh_token,
                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                }
                            }, function(err, res, body) {
                                body = JSON.parse(body);
                                sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                    '\', \'' + body.access_token + '\',' + contact_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster:true
                                    }).then(function(datas) {
                                        callback(null, body.access_token);

                                });
                            });
                        }
                        else {

                            callback(null, null);
                        }
                    } else {

                        callback(null, null);
                    }
                });
        
        },
        google_url: function(callback){
            callback(null, calendar_id);
        },
        org: function(callback) {
            callback(null, organization);
        },
        contact:  function(callback) {
            callback(null, contact_id);
        }
    }, function(err, results) {

        if (results.token) {
            sequelize.query('SELECT get_google_calendar_info(\'' + results.org + '\',\'' + results.google_url + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].get_google_calendar_info) {
                    datas = JSON.parse(datas[0].get_google_calendar_info);

                    var url = 'https://google-calendar.beebeeboard.com/api/v1/import_from_google_calendar?token='+results.token+'&google_url='+results.google_url+'&contact='+results.contact+'&org='+results.org;
                    console.log(url);
                    request.get(url, {
                        timeout: 500000,
                        json: false
                    }, function(error, result) {
                        if(error){
                            console.log(error);
                           // return res.status(500).send({});
                        }
                        
                    });

                    res.status(200).send();

                   
                }
            });
        }

    });
};

exports.export_from_beebee_to_google = function(req, res) {

    var organization = req.headers['host'].split(".")[0];
    var calendar_id = req.body.calendar_id;
    var contact_id = req.body.contact_id;
    var request = require('request');

    async.parallel({
        token: function(callback) {
            
            sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
                .then(function(datas) {


                    if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                        if(tok.refresh_token) {

                            request.post('https://accounts.google.com/o/oauth2/token', {
                                form: {
                                    grant_type: 'refresh_token',
                                    refresh_token: tok.refresh_token,
                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                }
                            }, function(err, res, body) {
                                body = JSON.parse(body);
                                sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                    '\', \'' + body.access_token + '\',' + contact_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster:true
                                    }).then(function(datas) {
                                        callback(null, body.access_token);

                                });
                            });
                        }
                        else {

                            callback(null, null);
                        }
                    } else {

                        callback(null, null);
                    }
                });
        
        },
        google_url: function(callback){
            callback(null, calendar_id);
        },
        org: function(callback) {
            callback(null, organization);
        },
        contact:  function(callback) {
            callback(null, contact_id);
        }
    }, function(err, results) {

        if (results.token) {
            sequelize.query('SELECT get_google_calendar_info(\'' + results.org + '\',\'' + results.google_url + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].get_google_calendar_info) {
                    datas = JSON.parse(datas[0].get_google_calendar_info);

                    var url = 'https://google-calendar.beebeeboard.com/api/v1/export_from_google_calendar?token='+results.token+'&google_url='+results.google_url+'&contact='+results.contact+'&org='+results.org;
                    console.log(url);
                    request.get(url, {
                        timeout: 500000,
                        json: false
                    }, function(error, result) {
                        if(error){
                            console.log(error);
                           // return res.status(500).send({});
                        }
                        
                    });

                    res.status(200).send();

                   
                }
            });
        }

    });
};



function google_calendar_sync_token(cal, tok, new_page_tok, syn_tok){
    var request = require('request');
    var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tok.access_token
    }

    if(syn_tok){
         var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal.calendar_id + '/events?maxResults=10000&singleEvents=true&syncToken=' + syn_tok + '&alt=json',
            method: 'GET',
            headers: headers
        }


    }else if(new_page_tok){
         var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal.calendar_id + '/events?maxResults=10000&singleEvents=true&pageToken=' + new_page_tok + '&alt=json',
            method: 'GET',
            headers: headers
        }
    }
    else{
         var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal.calendar_id + '/events?maxResults=10000&singleEvents=true&alt=json',
            method: 'GET',
            headers: headers
        }
    }

    console.log(options);
   
    // Start the request
    request(options, function(error, response, body) {
        if (error) {
            console.log('error' + error);
            return null;
        } else {
            body = JSON.parse(body);
            //console.log(body);
           

                
                if (body.nextSyncToken) {
                    sequelize.query('SELECT update_google_calendar_sync_token(\'' + cal.organization +
                        '\', \'' + body.nextSyncToken + '\',' + tok.contact_id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(datas) {
                         return body.nextSyncToken;

                    });
                     return body.nextSyncToken;

                    
                } else if (body.nextPageToken) {

                    console.log('ASPETTO IL SYNC TOKEN PASSO IL NEXT PAGE '+body.nextPageToken);
                   return google_calendar_sync_token(cal, tok, body.nextPageToken, null);
                }

            }
        });

      
};