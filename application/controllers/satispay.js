var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js'),
    config = require('../../config');


exports.create_key_id = function(req, res) {
   const request = require('request');

  

    const options = {
      method: 'POST',
      url: config.satispay.sandbox_url+'g_business/v1/authentication_keys',
      headers: {accept: 'application/json', 'content-type': 'application/json'},
      body: {public_key: config.satispay.pk, token: req.body.codice},
      json: true
    };

 

    request(options, function (err, response, body) {
      if(err){
                console.log(err);
                res.status(500).send({});
            }
            else{
                if(body && body.key_id){
                    sequelize.query('SELECT satispay_save_key(\'' +
                        req.headers['host'].split(".")[0] + '\','+
                        req.user._tenant_id + ',' +
                        req.body.user_id + ',\''+
                        body.key_id +'\',\''+
                        req.body.codice+'\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {

                            res.status(200).send({});
                    }).catch(function(error) {
                        console.log(error);
                       res.status(400).render('Satispay Save Key ' + error);
                    });

                }
                else{
                     res.status(400).render('Satispat Key Generation error Contact administrator');
                }
                
            }
        });
        
        
};

exports.create_checkout = function(req, res) {
   const request = require('request');

    sequelize.query('SELECT satispay_get_data(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.user._tenant_id+',' +
            req.body.user_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if(datas && datas[0] && datas[0].satispay_get_data && datas[0].satispay_get_data.paypal_token_secret){
                let json_data = datas[0].satispay_get_data;
                var access_token = req.headers['authorization'].replace('Bearer ','');
                var retUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/start/#/pay_me?request_by='+req.body.contact_id+'&access_token=' + access_token + '&payment_id=' + req.body.payment_id;
                var errUrl = retUrl;
               
                if(req.body.ecommerce){
                    retUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/scuolesci_ecommerce/#/payment_status/'+req.body.payment_id + '?access_token=' + access_token;
                 }

                if(req.body.booking){
                    retUrl = 'https://asset.beebeeboard.com/utility/booking/success_booking.php?contact_id='+req.body.contact_id+'&organization='+ req.headers['host'].split(".")[0]+'&access_token=' + access_token+ '&payment_id=' + req.body.payment_id+'&specialista_mail=' + encodeURIComponent(req.body.specialista_mail)+'&specialista_name=' + encodeURIComponent(req.body.specialista_name)+'&product_name=' + encodeURIComponent(req.body.product_name);
                    errUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/start/#/pay_me?request_by='+req.body.contact_id+'&access_token=' + req.body.token + '&payment_id=' + req.body.payment_id;
                }
                  
                  const crypto = require('crypto')

                   let data_sign = moment().format('ddd, DD MMM YYYY HH:mm:ss +0000');
                  
                   const body_ = `{"flow":"MATCH_CODE","amount_unit":${req.body.amount},"currency":"${req.body.currency}","external_code":"${req.body.contact_id}_${req.body.payment_id}_${moment().format('YYYYMMDDTHHmmss')}","expiration_date":"${moment().add(30,'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS')}Z","callback_url":"https://${req.headers['host'].split(".")[0]}.beebeeboard.com/api/v1/satispay/check_payment?user_id=${req.body.user_id}&access_token=${access_token}&payment_id=${req.body.payment_id}","redirect_url":"https://${req.headers['host'].split(".")[0]}.beebeeboard.com/api/v1/satispay/check_payment?user_id=${req.body.user_id}&access_token=${access_token}&payment_id=${req.body.payment_id}"}`;

                   
                const digest = crypto.createHash('sha256').update(body_).digest('base64')
                 const string = `(request-target): post /g_business/v1/payments\nhost: ${config.satispay.sandbox_host}\ndate: ${data_sign}\ndigest: ${digest}`


                const signature = crypto.createSign('RSA-SHA256').update(string).sign(config.satispay.private_pk, 'base64')

                const authorizationHeader = `Signature keyId="${datas[0].satispay_get_data.paypal_token_secret}", algorithm="rsa-sha256", headers="(request-target) host date digest", signature="${signature}"`;

                const options = {
                      method: 'POST',
                      url: 'https://'+config.satispay.sandbox_host+'/g_business/v1/payments',
                      headers: {
                        Accept: 'application/json',
                        Host: config.satispay.sandbox_host,
                        Date: data_sign,
                        Digest: digest,
                        Authorization: authorizationHeader,
                        'Content-Type': 'application/json'
                     },
                     body: body_
                    };

                    request(options, function (error, response, body) {
                      if (error) throw new Error(error);

                      console.log(body);
                     sequelize.query('SELECT update_payment_satispay(\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.body.payment_id + ',\'' +
                            moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
                            false + ',\'satispay\', \'' +
                            JSON.parse(body).id + '\',\''+
                            retUrl+'\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                                  res.status(200).send(body);
                         }).catch(function(err) {
                            console.log(err);
                               
                        });
                     
                    });
                
         }
        else{
            res.status(500).send({});
        }
    });
};

exports.check_payment = function(req, res) {
   const request = require('request');
   var payment_id = req.query.payment_id;
 
   sequelize.query('SELECT satispay_get_payment(\'' +
        req.headers['host'].split(".")[0] + '\','+
        req.query.payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(pay) {

            if(pay && pay[0] && pay[0].satispay_get_payment && pay[0].satispay_get_payment.payment_id){

                console.log(pay[0].satispay_get_payment.payment_id);
                     sequelize.query('SELECT satispay_get_data(\'' +
                        req.headers['host'].split(".")[0] + '\','+
                        req.user._tenant_id+',' +
                        req.query.user_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(datas) {

                            if(datas && datas[0] && datas[0].satispay_get_data && datas[0].satispay_get_data.paypal_token_secret){
                                console.log(datas[0].satispay_get_data.paypal_token_secret);
                                let json_data = datas[0].satispay_get_data;
                                
                                  
                                  const crypto = require('crypto')

                                   let data_sign = moment().format('ddd, DD MMM YYYY HH:mm:ss +0000');
                                  
                                   
                                 const digest = crypto.createHash('sha256').update('').digest('base64')
                                 const string = `(request-target): get /g_business/v1/payments/${pay[0].satispay_get_payment.payment_id}\nhost: ${config.satispay.sandbox_host}\ndate: ${data_sign}\ndigest: ${digest}`


                                const signature = crypto.createSign('RSA-SHA256').update(string).sign(config.satispay.private_pk, 'base64')

                                const authorizationHeader = `Signature keyId="${datas[0].satispay_get_data.paypal_token_secret}", algorithm="rsa-sha256", headers="(request-target) host date digest", signature="${signature}"`;

                                const options = {
                                      method: 'GET',
                                      url: 'https://'+config.satispay.sandbox_host+'/g_business/v1/payments/'+pay[0].satispay_get_payment.payment_id,
                                      headers: {
                                        Accept: 'application/json',
                                        Host: config.satispay.sandbox_host,
                                        Date: data_sign,
                                        Digest: digest,
                                        Authorization: authorizationHeader,
                                        'Content-Type': 'application/json'
                                     }
                                    };

                                    request(options, function (error, response, body) {
                                      if (error) throw new Error(error);

                                      console.log(body);
                                      if(body && JSON.parse(body).status == 'ACCEPTED'){

                                        
                                        sequelize.query('SELECT update_payment_satispay(\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                payment_id + ',\'' +
                                                moment().format('YYYY-MM-DDTHH:mmZ') + '\',' +
                                                true + ',\'satispay\', \''+
                                                pay[0].satispay_get_payment.payment_id+'\',null);', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                
                                                sequelize.query('SELECT search_organization(\'' +
                                                    req.headers['host'].split(".")[0] + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                    }).then(function(datas_o) {
                                                   
                                                        if(datas_o[0].search_organization.redirect_page != 'scuolesci_segreteria'){

                                                            sequelize.query('SELECT update_payment_request_success(' +
                                                                req.user._tenant_id + ',\'' +
                                                                req.headers['host'].split(".")[0] + '\',' +
                                                                payment_id + ');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {
                                                                

                                                               
                                                            }).catch(function(err) {
                                                                console.log(err);
                                                               
                                                            });
                                                        }else{
                                                            sequelize.query('SELECT scuole_ecommerce_stripe_success(' +
                                                                datas_o[0].search_organization._tenant_id + ',\'' +
                                                                req.headers['host'].split(".")[0] + '\',' +
                                                                payment_id + ');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                }).then(function(datas) {
                                                            
                                                                    var url = '';
                                                                    if(datas_o[0].search_organization.ecommerce_mail && 
                                                                        datas_o[0].search_organization.ecommerce_mail !== null && 
                                                                        datas_o[0].search_organization.ecommerce_mail != '' &&
                                                                        datas_o[0].search_organization.ecommerce_mail !== undefined){
                                                                            url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0] }&payment_id=${payment_id}&completo=true&esito=OK`;
                                                                    }else{
                                                                            url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0] }&payment_id=${payment_id}&completo=true&esito=OK`;
                                                                    }
                                                                    

                                                                    var request = https.get(url, function(result) {

                                                                        result.on('end', function() {
                                                                            res.status(200).send({});
                                                                        });
                                                                    }).on('error', function(err) {

                                                                        return res.status(400).render('Satispay Webhook : Mail Not Sended: ' + err);
                                                                    });

                                                                    request.end();

                                                                    
                                                                }).catch(function(err) {
                                                                    console.log('Satispay Webhook scuole_ecommerce_paypal_success ' + err);
                                                                   //return res.status(400).render('Stripe Webhook scuole_ecommerce_paypal_success ' + err);
                                                                });    
                                                        }
                                                        
                                                }).catch(function(err) {
                                                    console.log('Satispay Webhook search_organization ' + err);
                                                    //return res.status(400).render('Stripe Webhook search_organization ' + err);
                                                });       
                                                
                                            }).catch(function(err) {
                                                console.log(err);
                                                
                                            });
                                      }
                                      
                                    });
                                    res.redirect(pay[0].satispay_get_payment.transaction_ref);
                                
                            }
                            else{
                                 res.redirect(pay[0].satispay_get_payment.transaction_ref);
                            }
                     }).catch(function(err) {
                        console.log(err);
                        
                    });
            }
            else
            {
                res.status(404).send({});
            }
       
     }).catch(function(err) {
        console.log(err);
        
    });
};