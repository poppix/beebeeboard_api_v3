
/**
 * @apiDefine IdParamEntryError
 *
 * @apiError (Error 422) {422} IdParamEntryError The parameter is not a valid id
 * @apiErrorExample {json} Error-Response:
 * {
 * "errors": [{
 *     "status": '422',
 *     "source": {
 *       "pointer": "/data/attributes/id"
 *     },
 *     "title": "Parametro non valido",
 *     "detail": "Parametro non valido"
 *   }] 
 * }
 */

var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');


exports.index = function(req, res) {
   
    sequelize.query('SELECT product_categories(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({'data':datas[0].product_categories});
        }).catch(function(err) {
            return res.status(400).render('product_category get: ' + err);
        });
   
};

exports.find = function(req, res) {
   
    sequelize.query('SELECT product_categories_find(\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.params.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({'data':datas[0].product_categories_find[0]});
        }).catch(function(err) {
            return res.status(400).render('product_categories_find get: ' + err);
        });
   
};
/**
 * 
 * @api{post}/categories 1 - Insert new category
 * @apiName PostCategory
 * @apiGroup Categories
 * @apiVersion 1.0.0
 * 
 * @apiParam{Object} data Category attributes in JSON Api Standard (all attributes can be null)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "data":
 *  {
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New category name",
 *    "rank":2
 *   }
 *  }
 * }
 *
 * @apiSuccess {json} data Category in JSON Api Standard with id
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "data":
 *  {
 *   "id": "12",
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New category name",
 *    "rank":2
 *   }
 *  }
 * }
 *
 */
exports.create = function(req, res) {
    var category = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        category['name'] = (req.body.data.attributes.name && req.body.data.attributes.name !== null && utility.check_type_variable(req.body.data.attributes.name, 'string') ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        category['rank'] = (req.body.data.attributes.rank !== null && utility.check_id(req.body.data.attributes.rank)) ? req.body.data.attributes.rank : null;
        
        category['_tenant_id'] = req.user._tenant_id;
        category['organization'] = req.headers['host'].split(".")[0];

      
        sequelize.query('SELECT insert_product_category(\'' + JSON.stringify(category).replace(/'/g, "''''") + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                     useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_product_category && datas[0].insert_product_category != null) {
                    req.body.data.id = datas[0].insert_product_category;

                    

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'product_category',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_product_category,
                            'Aggiunta categoria prodotto' + datas[0].insert_product_category
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    
                }
            }).catch(function(err) {
                return res.status(400).render('insert_product_category: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @api{patch}/categories/:id 2 - Update specific category
 * @apiName PatchCategory
 * @apiGroup Categories
 * @apiVersion 1.0.0
 * 
 * @apiParam{Object} data Category attributes in JSON Api Standard (all attributes can be null)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "data":
 *  {
 *   "id": "12",
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New Name for the category",
 *    "rank":4
 *   }
 *  }
 * }
 *
 * @apiSuccess {json} data Category in JSON Api Standard with new fields value
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "data":
 *  {
 *   "id": "12",
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New Name for the category",
 *    "rank":4
 *   }
 *  }
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT update_product_category(' +
                req.params.id + ',\'' +
                (req.body.data.attributes.name && req.body.data.attributes.name !== null && utility.check_type_variable(req.body.data.attributes.name, 'string') ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.rank !== null && !utility.check_id(req.body.data.attributes.rank) ? req.body.data.attributes.rank : null) + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {

               

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'product_category',
                        'update',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        req.params.id,
                        'Modificato Product categoria ' + req.params.id
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });

               

            }).catch(function(err) {
                return res.status(400).render('product_category_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @api{delete}/categories/:id 3 - Delete a specific category
 * @apiName DeleteCategory
 * @apiGroup Categories
 * @apiVersion 1.0.0
 *
 * @apiParam{Number} id The category id
 *
 * @apiSuccess{Obect[]} data 
 * @apiSuccess{Number} data.id Category id
 * @apiSuccess{String} data.type Model name
 * @apiSuccessExample Example data on success
 * {
 *  "data":
 *  {
 *    "type":"categories",
 *    "id":"12"
 *  }
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.destroy = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    sequelize.query('SELECT delete_product_category_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'product_categories',
                    'id': datas[0].delete_product_category_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('product_category_destroy: ' + err);
        });
            
};