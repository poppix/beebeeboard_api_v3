var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    config = require('../../../config'),
    logs = require('../../utility/logs.js');
    const { S3Client, PutObjectCommand, GetObjectCommand } = require('@aws-sdk/client-s3');
    var https = require('https');

exports.find_scadenza_certificati = function(req, res) {
    var data = null;

    if (req.query.date !== undefined && req.query.date !== '') {
        if (!utility.check_type_variable(req.query.date, 'string')) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data = '\'' + req.query.date + '\'';
    }

    sequelize.query('SELECT sciclub_find_scadenza_certificati(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            data + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.status(200).json({ 'data': data[0].sciclub_find_scadenza_certificati });
        });
};

exports.iscritti_sciclub = function(req, res) {
    var q = null;
    var data_inizio = null;
    var data_fine = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\'' + req.query.from_date + '\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = '\'' + req.query.to_date + '\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            return res.status(422).send(utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        q = '\'' + req.query.q + '\'';
    }

    sequelize.query('SELECT sciclub_iscritti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            q+','+
            data_inizio+','+
            data_fine+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            if(data && data[0] && data[0].sciclub_iscritti){
                res.status(200).json({ 'data': data[0].sciclub_iscritti });
            }
            else{
                res.status(200).json({ 'data': [] });
            }
        });
};

exports.iscritti_sciclub_csv = function(req, res) {
    var json2csv = require('json2csv');
    var q = null;
    var data_inizio = null;
    var data_fine = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\'' + req.query.from_date + '\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = '\'' + req.query.to_date + '\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            return res.status(422).send(utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        q = '\'' + req.query.q + '\'';
    }

    sequelize.query('SELECT sciclub_iscritti_export(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            q+','+
            data_inizio+','+
            data_fine+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            if(data && data[0] && data[0].sciclub_iscritti_export){
                console.log(data[0].sciclub_iscritti_export[0]);
                var fields_array = data[0].sciclub_iscritti_export[0]['fields'];
                var fieldNames_array = data[0].sciclub_iscritti_export[0]['field_names'];
                json2csv({
                    data: data[0].sciclub_iscritti_export,
                    fields: fields_array,
                    fieldNames: fieldNames_array
                }, function(err, csv) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=iscritti.csv'
                    });
                    res.end(csv);
                });
            }
            else{
                res.status(200).json({ 'data': [] });
            }
        });
};

exports.sciclub_iscrizione_form = function(req,res){
    console.log(req.body);
    var body_json = req.body;

    sequelize.query('SELECT sciclub_iscrizione_form(\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            JSON.stringify(body_json.data).replace(/'/g, "''''") +'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
    .then(function(data) {
        if(data && data[0] && data[0].sciclub_iscrizione_form){

            console.log(data[0].sciclub_iscrizione_form);
            /* CARICO I FILES */

            const s3 = new S3Client({
                region: config.aws_s3.region,
                credentials:{
                    accessKeyId: config.aws_s3.accessKeyId,
                    secretAccessKey: config.aws_s3.secretAccessKey,
                }
            });

            var annullare = false;
            var url_api = 'https://asset.beebeeboard.com/'+req.headers['host'].split(".")[0]+'/form/uploads/';

            async.parallel({
                finale: function(callback){
                    callback(null, data[0].sciclub_iscrizione_form);
                },
                certificato: function(callback) {
                    if(body_json.files && body_json.files.uploadedFileCm){
                            var request = https.get(url_api + body_json.files.uploadedFileCm, function(result) {

                            var chunks = [];

                            result.on('data', function(chunk) {
                                chunks.push(chunk);
                            });

                            result.on('end', function() {
                                var pdfData = new Buffer.concat(chunks);
                                var base64 = new Buffer.from(pdfData,'base64');
                                //base64 = new Buffer(pdfData, 'binary');

                                console.log(base64);
                                var params = {
                                    Bucket: 'data.beebeeboard.com',
                                    Key: req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta+'/'+body_json.files.uploadedFileCm,
                                    ACL: 'private',
                                    ServerSideEncryption: 'AES256',
                                    ContentType: set_content(body_json.files.uploadedFileCm.split('.')[1]),
                                    Body: base64
                                };

                                console.log(params);

                                 const command = new PutObjectCommand(params);
                                s3.send(command, function(err, result) {
                                //s3.putObject(params, function(err, result){
                                    if (err) {
                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                    }       

                                    let file = {};
                                        file.file_name =  body_json.files.uploadedFileCm;
                                        file.file_type =  body_json.files.uploadedFileCm.split('.')[1];
                                        file.file_url = req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta+'/'+body_json.files.uploadedFileCm;

                                        file.is_thumbnail = false;

                                        file.file_etag = result.ETag;
                                        file.upload_date = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                                        file.uploader_id = null;
                                        file._tenant_id = data[0].sciclub_iscrizione_form.tenant;
                                        file.organization = req.headers['host'].split(".")[0];
                                        file.mongo_id = null;


                                        sequelize.query('SELECT insert_file(\'' + JSON.stringify(file).replace(/'/g, "''''") + '\',\'' +
                                            req.headers['host'].split(".")[0] + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {

                                                utility.sanitize_file_relation(
                                                    'contact',
                                                    data[0].sciclub_iscrizione_form.atleta,
                                                    datas[0].insert_file,
                                                    data[0].sciclub_iscrizione_form.atleta,
                                                    req.headers['host'].split(".")[0],
                                                    data[0].sciclub_iscrizione_form.tenant, 
                                                    res
                                                );

                                                sequelize.query('SELECT insert_tag_v1(\'certificato\',' +
                                                        datas[0].insert_file+',\'' +
                                                        req.headers['host'].split(".")[0] + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(datas) {
                                                        
                                                    }).catch(function(err) {
                                                       
                                                    });

                                                if(body_json.data && body_json.data.cm_scadenza_atleta &&
                                                   body_json.data.cm_scadenza_atleta != '' ){
                                                     sequelize.query('SELECT insert_tag_v1(\'scadenza:'+
                                                            moment(body_json.data.cm_scadenza_atleta).format('YYYY-MM-DD')+'\',' +
                                                            datas[0].insert_file+',\'' +
                                                            req.headers['host'].split(".")[0] + '\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(datas) {
                                                            
                                                    }).catch(function(err) {
                                                           
                                                    });
                                                }
                                               

                                                if(body_json.data.cm == 'Agonistico'){
                                                    sequelize.query('SELECT insert_tag_v1(\'agonistico\',' +
                                                        datas[0].insert_file+',\'' +
                                                        req.headers['host'].split(".")[0] + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                        .then(function(datas) {
                                                            
                                                        }).catch(function(err) {
                                                       
                                                    });
                                                }

                                                callback(null, datas[0].insert_file);

                                        }).catch(function(err) {
                                            annullare = true;
                                            callback(err,null);
                                        });
                                });

                            });
                        }).on('error', function(err) {

                                annullare = true;
                                callback(err,null);
                        });
                    }else
                    {
                        callback(null, 0);
                    }
                },
                bonifico: function(callback) {
                    if(body_json.files.uploadedFileBon){
                        var request = https.get(url_api + body_json.files.uploadedFileBon, function(result) {

                            var chunks = [];

                            result.on('data', function(chunk) {
                                chunks.push(chunk);
                            });

                            result.on('end', function() {
                                var pdfData = new Buffer.concat(chunks);

                                base64 = new Buffer(pdfData, 'binary');

                                var params = {
                                    Bucket: 'data.beebeeboard.com',
                                    Key: req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta+'/'+body_json.files.uploadedFileBon,
                                    ACL: 'private',
                                    ServerSideEncryption: 'AES256',
                                    ContentType: set_content(body_json.files.uploadedFileBon.split('.')[1]),
                                    Body: base64
                                };

                                 const command = new PutObjectCommand(params);
                                s3.send(command, function(err, result) {
                                 //s3.putObject(params, function(err, result){
                                if (err) {
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Errore upload file : ' + err);
                                }   

                                    let file = {};
                                        file.file_name =  body_json.files.uploadedFileBon;
                                        file.file_type =  body_json.files.uploadedFileBon.split('.')[1];
                                        file.file_url = req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta+'/'+body_json.files.uploadedFileBon;

                                        file.is_thumbnail = false;

                                        file.file_etag = result.ETag;
                                        file.upload_date = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                                        file.uploader_id = null;
                                        file._tenant_id = data[0].sciclub_iscrizione_form.tenant;
                                        file.organization = req.headers['host'].split(".")[0];
                                        file.mongo_id = null;


                                        sequelize.query('SELECT insert_file(\'' + JSON.stringify(file).replace(/'/g, "''''") + '\',\'' +
                                                req.headers['host'].split(".")[0] + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                 utility.sanitize_file_relation(
                                                    'contact',
                                                    data[0].sciclub_iscrizione_form.atleta,
                                                    datas[0].insert_file,
                                                    data[0].sciclub_iscrizione_form.atleta,
                                                    req.headers['host'].split(".")[0],
                                                    data[0].sciclub_iscrizione_form.tenant, 
                                                    res
                                                );
                                                 
                                                callback(null, datas[0].insert_file);

                                            }).catch(function(err) {
                                                annullare = true;
                                                callback(err,null);
                                            });
                                });

                            });
                        }).on('error', function(err) {

                                annullare = true;
                                callback(err,null);
                        });
                    }else
                    {
                        callback(null, 0);
                    }

                }/*,
                doc_fronte: function(callback) {
                     var request = https.get(url_api + body_json.files.uploadedFileCif, function(result) {

                        var chunks = [];

                        result.on('data', function(chunk) {
                            chunks.push(chunk);
                        });

                        result.on('end', function() {
                            var pdfData = new Buffer.concat(chunks);

                            base64 = new Buffer(pdfData, 'binary');

                            var params = {
                                Bucket: 'data.beebeeboard.com',
                                Key: req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta,
                                ACL: 'private',
                                ServerSideEncryption: 'AES256',
                                ContentType: set_content(body_json.files.uploadedFileCif.split('.')[1]),
                                Body: base64
                            };

                            s3.putObject(params, function(result){

                                let file = {};
                                    file.file_name =  body_json.files.uploadedFileCif;
                                    file.file_type =  body_json.files.uploadedFileCif.split('.')[1];
                                    file.file_url = req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta;

                                    file.is_thumbnail = false;

                                    file.file_etag = result.ETag;
                                    file.upload_date = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                                    file.uploader_id = null;
                                    file._tenant_id = data[0].sciclub_iscrizione_form.tenant;
                                    file.organization = req.headers['host'].split(".")[0];
                                    file.mongo_id = null;


                                    sequelize.query('SELECT insert_file(\'' + JSON.stringify(file) + '\',\'' +
                                            req.headers['host'].split(".")[0] + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            callback(null, datas[0].insert_file);

                                        }).catch(function(err) {
                                            annullare = true;
                                            callback(err,null);
                                        });
                            });

                        });
                    }).on('error', function(err) {

                            annullare = true;
                            callback(err,null);
                    });

                },
                doc_retro: function(callback) {
                    var request = https.get(url_api + body_json.files.uploadedFileCir, function(result) {

                        var chunks = [];

                        result.on('data', function(chunk) {
                            chunks.push(chunk);
                        });

                        result.on('end', function() {
                            var pdfData = new Buffer.concat(chunks);

                            base64 = new Buffer(pdfData, 'binary');

                            var params = {
                                Bucket: 'data.beebeeboard.com',
                                Key: req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta,
                                ACL: 'private',
                                ServerSideEncryption: 'AES256',
                                ContentType: set_content(body_json.files.uploadedFileCir.split('.')[1]),
                                Body: base64
                            };

                            s3.putObject(params, function(result){

                                let file = {};
                                    file.file_name =  body_json.files.uploadedFileCir;
                                    file.file_type =  body_json.files.uploadedFileCir.split('.')[1];
                                    file.file_url = req.headers['host'].split(".")[0]+'/'+data[0].sciclub_iscrizione_form.atleta;

                                    file.is_thumbnail = false;

                                    file.file_etag = result.ETag;
                                    file.upload_date = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
                                    file.uploader_id = null;
                                    file._tenant_id = data[0].sciclub_iscrizione_form.tenant;
                                    file.organization = req.headers['host'].split(".")[0];
                                    file.mongo_id = null;


                                    sequelize.query('SELECT insert_file(\'' + JSON.stringify(file) + '\',\'' +
                                        req.headers['host'].split(".")[0] + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                    })
                                    .then(function(datas) {
                                        callback(null, datas[0].insert_file);

                                    }).catch(function(err) {
                                        annullare = true;
                                        callback(err,null);
                                    });
                            });

                        });
                    }).on('error', function(err) {

                            annullare = true;
                            callback(err,null);
                    });

                }*/

            }, function(err, results) {
                if(err){
                    console.log('ERR: '+err)
                }     

                console.log(results);   
                res.status(200).json(results.finale);


            });
        }
        else{
            res.status(404).json({ 'data': [] });
        }
    });            
}


exports.generate_token_client = function(req, res) {
    var utils = require('../../../utils');

   
    var token = utils.uid(config.token.accessTokenLength);

    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
        moment(new Date()).add(1, 'months').format('YYYY/MM/DD HH:mm:ss') + '\',' +
        req.query.user_id + ',' +
        5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(tok) {

         res.status(200).send({'token': token});
    }).catch(function(err) {
        return res.status(400).render('insert_token: ' + err);
    });


};

function set_content(type) {
    switch (type) {
        case 'pdf':
            return 'application/pdf';
            break;

        case 'jpg':
        case 'jpeg':
        case 'bmp':
        case 'png':
        case 'gif':
        case 'heic':
        case 'heif':
        case 'pjpeg':
        case 'svg+xml':
        case 'tiff':
        case 'vnd.djvu':
            return 'image/' + type;
            break;

        case 'json':
            return 'application/json';
            break;

        case 'xml':
            return 'application/xml';
            break;

        default:
            return 'application/text';
            break;
    }
};   