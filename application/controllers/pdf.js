var moment = require('moment'),
    Sequelize = require('sequelize'),
    logs = require('../utility/logs.js'),
    finds = require('../utility/finds.js'),
    mandrill = require('mandrill-api/mandrill'),
    config = require('../../config'),
    utils = require('../../utils');

function formatta_euro(number) {
    if (number) {
        var numberStr = parseFloat(number).toFixed(2).toString();
        var numFormatDec = numberStr.slice(-2);
        numberStr = numberStr.substring(0, numberStr.length - 3);
        var numFormat = [];
        while (numberStr.length > 3) {
            numFormat.unshift(numberStr.slice(-3));
            numberStr = numberStr.substring(0, numberStr.length - 3);
        }
        numFormat.unshift(numberStr);
        return numFormat.join('.') + ',' + numFormatDec;
    } else {
        return 0.00;
    }
}

function generate_file_name(invoice) {
    var doc_detail = {
        'nome': 'Avviso di Fattura',
        'tipo': 'Avviso di Fattura'
    };


    if (invoice.data.attributes.document_type) {
        switch (invoice.data.attributes.document_type) {
            case 'invoice':
                doc_detail.nome = 'Fattura n° ';
                doc_detail.tipo = 'Fattura';
                break;

            case 'receipt':
                doc_detail.nome = 'Ricevuta n° ';
                doc_detail.tipo = 'Ricevuta';
                break;

            case 'estimate':
                doc_detail.nome = 'Preventivo n° ';
                doc_detail.tipo = 'Preventivo';
                break;

            case 'ddt':
                doc_detail.nome = 'Documento di trasporto n° ';
                doc_detail.tipo = 'Documento di trasporto';
                break;

            case 'order':
                doc_detail.nome = 'Ordine n° ';
                doc_detail.tipo = 'Ordine';
                break;

            case 'contract':
                doc_detail.nome = 'Proposta d\'acquisto n° ';
                doc_detail.tipo = 'Proposta d\'acquisto';
                break;
        }
    } else {

        if (invoice.data.attributes.estimate === true) {
            doc_detail.nome = 'Preventivo n° ';
            doc_detail.tipo = 'Preventivo';
        } else if (invoice.data.attributes.total < 0) {
            doc_detail.nome = 'Nota di Credito n° ';
            doc_detail.tipo = 'Nota di Credito';
        } else if (invoice.data.attributes.number != null && invoice.data.attributes.number != '' && invoice.data.attributes.estimate === false) {
            doc_detail.nome = 'Fattura n° ';
            doc_detail.tipo = 'Fattura';
        }
    }

    doc_detail.nome += `${invoice.data.attributes.number} del ${moment(invoice.data.attributes.date).format('DD/MM/YYYY')}`;
    doc_detail.nome = doc_detail.nome.replace(/ /g, "_");
    return doc_detail;
}

exports.index = function(req, res) {
    var https = require('https');
    var download = req.query.download ? req.query.download : false;
    var test = null;

    if (req.query.test_invoice_style !== undefined && req.query.test_invoice_style !== null) {
        test = req.query.test_invoice_style;
    }

    req.params.id = req.query.invoice_id;
    //req.headers['host'].split(".")[0] = 'poppixsrl';
    //req.query.access_token = 'HqYQaRz2eNIPxRG4HZaaICLEkz3WYCPHx14hONhGIgwF02S015nl52Oq046Y3Nojh6iWgAVNTv8IYf33zFg9C2PeefU8rR7XJCps9408GQ80yWDeqBtNAGNR4jBGGQuqVwrrYoolNAJja9ulrrJRYZCligd3XM6uyJstIMew8tdUiAmCpTEQ29vnm3Elk3csvfsN4IlVl1n7OXm8JFKd2J3WIuNwyvzWyAnZSBzbnGwTH7OJXejGyRbsKsgMoZ7z';
    if (req.params.id) {
        finds.find_invoice(req, function(err, invoice) {
            var doc_detail = generate_file_name(invoice);

            var pdf_file_name = (invoice.data.attributes.pdf_file_name && invoice.data.attributes.pdf_file_name != '') ? invoice.data.attributes.pdf_file_name : 'pdf.php';
            var url = `https://asset.beebeeboard.com/pdf_create/api/${pdf_file_name}?organization=${req.headers['host'].split(".")[0]}&invoice_id=${req.query.invoice_id}&test_invoice_style=${test}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;

            var request = https.get(url, function(result) {

                var chunks = [];

                result.on('data', function(chunk) {
                    chunks.push(chunk);
                });

                result.on('end', function() {
                    var pdfData = new Buffer.concat(chunks);

                    if (download !== false) {
                        res.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': `attachment; filename=${doc_detail.nome.replace(/ /g, "_")}.pdf`,
                            'Content-Length': pdfData.length
                        });
                    } else {
                        res.writeHead(200, {
                            'Content-Disposition': `inline; filename=${doc_detail.nome.replace(/ /g, "_")}.pdf`,
                            'Content-Length': pdfData.length
                        });
                    }

                    res.end(pdfData);
                });
            }).on('error', function(err) {

                return false;
            });

            request.end();

        });
    } else {
        var doc_detail = {
            'nome': 'Fattura',
            'tipo': 'Fattura'
        };
        var pdf_file_name = 'pdf.php';
        var url = `https://asset.beebeeboard.com/pdf_create/api/${pdf_file_name}?organization=${req.headers['host'].split(".")[0]}&invoice_id=&test_invoice_style=${test}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;

        var request = https.get(url, function(result) {

            var chunks = [];

            result.on('data', function(chunk) {
                chunks.push(chunk);
            });

            result.on('end', function() {


                var pdfData = new Buffer.concat(chunks);

                if (download !== false) {
                    res.writeHead(200, {
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${doc_detail.nome.replace(/ /g, "_")}.pdf`,
                        'Content-Length': pdfData.length
                    });
                } else {
                    res.writeHead(200, {
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `filename=${doc_detail.nome.replace(/ /g, "_")}.pdf`,
                        'Content-Length': pdfData.length
                    });
                }
                res.end(pdfData);
            });
        }).on('error', function(err) {

            return false;
        });

        request.end();
    }
};

exports.pdf_our_invoices = function(req, res) {
    var https = require('https');
    var download = req.query.download ? req.query.download : false;
    var token = utils.uid(config.token.accessTokenLength);

 sequelize.query('SELECT poppixsrl_get_user( '+
        req.user._tenant_id+',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.user.role_id + ');', 
    {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
    }).then(function(datas) {
        if (!datas[0].poppixsrl_get_user || !datas[0].poppixsrl_get_user) {
            return res.status(404).send(utility.error422('organization', 'Invalid attribute', 'Nessun attributo trovato contatta l\'amministratore', '422'));

        } else {
     
            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                moment(new Date()).add(10, 'minutes').format('YYYY/MM/DD HH:mm:ss') + '\','+
                datas[0].poppixsrl_get_user+',' +
                5 + ',\'\',\'poppixsrl\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(tok) {

            
                    var doc_detail = {
                        'nome': 'Fattura',
                        'tipo': 'Fattura'
                    };
                    var pdf_file_name = 'pdf.php';
                    var url = `https://asset.beebeeboard.com/pdf_create/api/pdf.php?organization=poppixsrl&invoice_id=${req.query.invoice_id}&test_invoice_style=&token=${token}&$_t=${moment().format('HHmmss')}`;
                    
                    var request = https.get(url, function(result) {

                        var chunks = [];

                        result.on('data', function(chunk) {
                            chunks.push(chunk);
                        });

                        result.on('end', function() {


                            var pdfData = new Buffer.concat(chunks);


                            if (download !== 'false') {
                                
                                res.writeHead(200, {
                                    'Content-Type': 'application/pdf',
                                    'Content-Disposition': `attachment; filename=${doc_detail.nome.replace(/ /g, "_")}.pdf`,
                                    'Content-Length': pdfData.length
                                });
                            } else {
                               
                                res.writeHead(200, {
                                    'Content-Type': 'application/pdf',
                                    'Content-Disposition': `filename=${doc_detail.nome.replace(/ /g, "_")}.pdf`,
                                    'Content-Length': pdfData.length
                                });
                            }
                            res.end(pdfData);
                        });
                    }).on('error', function(err) {

                        return false;
                    });

                    request.end();
              }).catch(function(err) {
                return res.status(400).render('insert_token: ' + err);
            });
        }
    }).catch(function(err) {
        return res.status(400).render('poppixsrl_get_user: ' + err);
    });
};


exports.privacy = function(req, res) {
    var https = require('https');
    var download = req.query.download ? req.query.download : false;
    var test = null;

    req.params.id = req.query.contact_id;


    if (req.params.id) {
        finds.find_contact(req, function(err, contact) {

            /*if(req.headers['host'].split(".")[0] == 'fisiotest'){
                var url = `https://asset.beebeeboard.com/pdf_create/tcpdf/privacy.php?organization=${req.headers['host'].split(".")[0]}&spunte=${req.query.spunte}&index=${req.query.index}&contact_id=${req.query.contact_id}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;

            }else{*/
               var url = `https://asset.beebeeboard.com/pdf_create/api/privacy.php?organization=${req.headers['host'].split(".")[0]}&index=${req.query.index}&spunte=${req.query.spunte}&contact_id=${req.query.contact_id}&token=${req.query.access_token}&yousign=${req.query.yousign}&$_t=${moment().format('HHmmss')}`;
                console.log(url);
            //}
            
            var request = https.get(url, function(result) {

                var chunks = [];

                result.on('data', function(chunk) {
                    chunks.push(chunk);
                });

                result.on('end', function() {
                    var pdfData = new Buffer.concat(chunks);

                    if (download !== false) {
                        res.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': `attachment; filename=${contact.data.attributes.name.replace(/ /g, "_")}.pdf`,
                            'Content-Length': pdfData.length
                        });
                    } else {
                        res.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': `filename=${contact.data.attributes.name.replace(/ /g, "_")}.pdf`,
                            'Content-Length': pdfData.length
                        });
                    }

                    res.end(pdfData);
                });
            }).on('error', function(err) {

                return false;
            });

            request.end();

        });
    } else {
        return res.status(404).render('privacy not found');
    }
};

exports.project = function(req, res) {
    var https = require('https');
    var download = req.query.download ? req.query.download : false;
    var test = null;

    req.params.id = req.query.project_id;

    if (req.params.id) {
        finds.find_project(req, function(err, invoice) {

            if (invoice && invoice.included) {
                var url = `https://asset.beebeeboard.com/pdf_create/api/beebeefisio_report.php?organization=${req.headers['host'].split(".")[0]}&project_id=${req.query.project_id}&custom_field_id=${req.query.custom_field_id}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;

                var contact_name = invoice.included.filter(function(i) {
                    return i.type === 'contacts' && i.attributes.contact_status_id === 111;
                });

                var file_name = 'report';


                if (contact_name && contact_name.length > 0) {
                    file_name = (contact_name[0] && contact_name[0] != '') ? (contact_name[0].attributes.name + '_report') : 'report';
                }

                var request = https.get(url, function(result) {

                    var chunks = [];

                    result.on('data', function(chunk) {
                        chunks.push(chunk);
                    });

                    result.on('end', function() {
                        var pdfData = new Buffer.concat(chunks);

                        if (download !== false) {
                            res.writeHead(200, {
                                'Content-Type': 'application/pdf',
                                'Content-Disposition': `attachment; filename=${file_name.replace(/ /g, "_")}.pdf`,
                                'Content-Length': pdfData.length
                            });
                        } else {
                            res.writeHead(200, {
                                'Content-Type': 'application/pdf',
                                'Content-Disposition': `filename=${file_name.replace(/ /g, "_")}.pdf`,
                                'Content-Length': pdfData.length
                            });
                        }

                        res.end(pdfData);
                    });
                }).on('error', function(err) {

                    return false;
                });

                request.end();
            }

        });
    } else {
        return res.status(404).render('report_pdf not found: ' + err);
    }
};

exports.send_signed_document = function(req, res) {
    var https = require('https');

    var urlPdf = 'https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/api/v1/app/file?access_token='+req.headers['authorization'].replace('Bearer ','')+'&name='+req.body.document_name.replace(/'/g, "''''")+'&url='+req.body.url_document;
    var sendMailTo = req.body.send_mail_to;

    var request = https.get(urlPdf, function(result) {

        var chunks = [];

        result.on('data', function(chunk) {
            chunks.push(chunk);
        });

        result.on('end', function() {

            var pdfData = new Buffer.concat(chunks);

            if (sendMailTo) {
                var addresses = req.body.send_mail_to.split(',');
                var address_string = '';
                var mails = [];
                addresses.forEach(function(address) {
                    address_string += ' ' + address;
                    var mail = {
                        "email": address,
                        "name": null,
                        "type": "to"
                    };
                    mails.push(mail);
                });

                var contact_name = req.body.contact_name;

                 sequelize.query('SELECT search_organization(\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(datas_o) {

                        var template_content = [{
                                "name": "LANG",
                                "content": req.body.lang
                            }, {
                                "name": "CONTACT_NAME",
                                "content": contact_name
                            }, {
                                "name": "ORGANIZATION",
                                "content": req.body.organization_name
                            }, {
                                "name": "COLOR",
                                "content": req.body.color ? req.body.color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "subject": 'Documento firmato',
                                "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                "from_name": req.body.organization_name,
                                "to": mails,
                                "headers": {
                                    "Reply-To": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : 'no-reply@beebeemailer.com'
                                },
                                "tags": ["beebeeboard-signed-document", req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"],
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "attachments": [{
                                    'type': 'application/pdf',
                                    'name': req.body.document_name,
                                    'content': pdfData.toString('base64')
                                }]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "signed-document-send",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                 sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',null,' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.object ? req.body.object.replace(/'/g, "''''") : '') + '\',' +
                                        '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                             useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        console.log('ERROR SEND 1: '+err);
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });

                            } else if(result && result[0]) {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',null,' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'' +
                                        result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.object.replace(/'/g, "''''") + '\',' +
                                        '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                              useMaster: true
                                        })
                                    .then(function(datas) {
                                         console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail sended saved');


                                        res.status(422).send({});


                                    }).catch(function(err) {
                                         console.log('ERROR SEND : '+err);
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });
                            }
                         }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                        });
                   }).catch(function(err) {
                        console.log('ERROR SEARCH ORG: '+err);
                        return res.status(400).render('search_organization: ' + err);
                   });
            }
       
        });

    }).on('error', function(err) {
            console.log(err);
             console.log('ERRORE GENERICO: '+err);
            return false;
        });

    request.end();
};

exports.send_tagliando = function(req, res) {
    var https = require('https');

    var urlPdf = req.body.url_document;
    var sendMailTo = req.body.send_mail_to;

    var request = https.get(urlPdf, function(result) {

        var chunks = [];

        result.on('data', function(chunk) {
            chunks.push(chunk);
        });

        result.on('end', function() {

            var pdfData = new Buffer.concat(chunks);

            if (sendMailTo) {
                var addresses = req.body.send_mail_to.split(',');
                var address_string = '';
                var mails = [];
                addresses.forEach(function(address) {
                    address_string += ' ' + address;
                    var mail = {
                        "email": address,
                        "name": null,
                        "type": "to"
                    };
                    mails.push(mail);
                });

                if (req.body.sendMailCc) {
                    addresses = req.body.sendMailCc.split(',');
                    addresses.forEach(function(address) {
                        address_string += ' ' + address;
                        var mail = {
                            "email": address,
                            "name": null,
                            "type": "bcc"
                        };
                        mails.push(mail);
                    });
                }


                 sequelize.query('SELECT search_organization(\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(datas_o) {

                        var contact_name = req.body.contact_name;

                        var template_content = [{
                                "name": "LANG",
                                "content": req.body.lang
                            }, {
                                "name": "CONTACT_NAME",
                                "content": contact_name
                            }, {
                                "name": "TEXT",
                                "content": conferma_prenotazione_scuola(req.headers['host'].split(".")[0])
                            }, {
                                "name": "ENGLISHTEXT",
                                "content": conferma_prenotazione_scuola_english(req.headers['host'].split(".")[0])
                            }, {
                                "name": "ORGANIZATION",
                                "content": req.body.organization_name
                            }, {
                                "name": "COLOR",
                                "content": req.body.color ? req.body.color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                "from_name": req.body.organization_name,
                                "to": mails,
                                "headers": {
                                    "Reply-To": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : 'no-reply@beebeemailer.com'
                                },
                                "tags": ["beebeeboard-tagliando", req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"],
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "attachments": [{
                                    'type': 'application/pdf',
                                    'name': req.body.document_name,
                                    'content': pdfData.toString('base64')
                                }]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "tagliando-send-personalizzato",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                 sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',null,' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.object ? req.body.object.replace(/'/g, "''''") : '') + '\',' +
                                        '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                             useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });

                            } else if(result && result[0]) {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',null,' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'' +
                                        result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.object.replace(/'/g, "''''") + '\',' +
                                        '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                              useMaster: true
                                        })
                                    .then(function(datas) {
                                         console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail sended saved');


                                        res.status(422).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });
                            }
                           }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                        });
                     }).catch(function(err) {
                        return res.status(400).render('search_organization' + err);
                    });
            }
       
        });

    }).on('error', function(err) {
            console.log(err);
            return false;
        });

    request.end();
};

exports.send_mail = function(req, res) {
    var https = require('https');

    //var url = 'https://asset.beebeeboard.com/invoice/invoice.php?organization=' + req.headers['host'].split(".")[0] + '&invoice_id=' + req.query.invoice_id + '&token=' + req.query.access_token + '&$_t=0921';
    req.params.id = req.body.invoice_id;

    finds.find_invoice(req, function(err, invoice) {
        var doc_detail = generate_file_name(invoice);

        //var pdf_file_name = (invoice.data.attributes.pdf_file_name && invoice.data.attributes.pdf_file_name != '') ? invoice.data.attributes.pdf_file_name : 'pdf.php';
        //var url = `https://asset.beebeeboard.com/pdf_create/api/${pdf_file_name}?organization=${req.headers['host'].split(".")[0]}&invoice_id=${req.query.invoice_id}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;

        var url = `https://${req.headers['host'].split(".")[0]}.beebeeboard.com/api/v1/pdf?invoice_id=${req.body.invoice_id}&access_token=${req.body.access_token}&$_t=${moment().format('HHmmss')}`;

        var expires_string = '';
        var cnt = 0;
        invoice.included.forEach(function(included) {
            if (included.type == 'expire_dates') {
                var expire_dates = included.expire_dates;
                if (cnt == 0) {
                    expire_string += '<br />';
                }
                expire_string += moment(expire_dates.expire_date).format('DD/MM/YYYY');
                if (expire_dates.total) {
                    expire_string += ' di &euro; '.formatta_euro(expire_dates.total);
                }
                expires_string += '';
                cnt++;
            }
        });

       
        var request = https.get(url, function(result) {

            var chunks = [];

            result.on('data', function(chunk) {
                chunks.push(chunk);
            });

            result.on('end', function() {

                var pdfData = new Buffer.concat(chunks);

                if (req.body.sendMailTo) {
                    var addresses = req.body.sendMailTo.split(',');
                    var address_string = '';
                    var mails = [];
                    addresses.forEach(function(address) {
                        address_string += ' ' + address;
                        var mail = {
                            "email": address,
                            "name": null,
                            "type": "to"
                        };
                        mails.push(mail);
                    });

                    if (req.body.sendMailCc) {
                        addresses = req.body.sendMailCc.split(',');
                        addresses.forEach(function(address) {
                            address_string += ' ' + address;
                            var mail = {
                                "email": address,
                                "name": null,
                                "type": "bcc"
                            };
                            mails.push(mail);
                        });
                    }


                    var joinName = [invoice.data.attributes.contact_last_name, invoice.data.attributes.contact_first_name];
                    var joinSocName = [invoice.data.attributes.society_last_name, invoice.data.attributes.society_first_name];
                    var contact_name = invoice.data.attributes.contact_fisica_giuridica === true ? invoice.data.attributes.contact_name : joinName.join(' ');
                    var society_name = invoice.data.attributes.fisica_giuridica === true ? invoice.data.attributes.fisica_giuridica : joinSocName.join(' ');

                     sequelize.query('SELECT search_organization_reply_to_new(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(reply_mail) {

                         var reply_to = 'no-reply@beebeemailer.com';
                            var domain_from_mail = 'no-reply@beebeemailer.com';

                            if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                                reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                                reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                    reply_to = domain_from_mail;
                                }
                            }



                        var template_content = [{
                                "name": "LANG",
                                "content": req.body.lang
                            }, {
                                "name": "DOCUMENT_TYPE",
                                "content": doc_detail.tipo
                            }, {
                                "name": "NUMBER",
                                "content": invoice.data.attributes.number
                            }, {
                                "name": "SOCIETY_NAME",
                                "content": society_name
                            }, {
                                "name": "CONTACT_NAME",
                                "content": contact_name
                            }, {
                                "name": "DATE",
                                "content": moment(invoice.data.attributes.date).format('DD/MM/YYYY')
                            }, {
                                "name": "EXPIRE",
                                "content": expires_string
                            }, {
                                "name": "ORGANIZATION",
                                "content": req.body.organization_name
                            }, {
                                "name": "USER_MESSAGE",
                                "content": (req.body.sendMailMessage ? req.body.sendMailMessage : '')
                            }, {
                                "name": "COLOR",
                                "content": req.body.color ? req.body.color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                            }, {
                                "name": "PAYMENT_URL",
                                "content": req.body.payment_url ? req.body.payment_url : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "subject": (req.body.sendMailObject ? req.body.sendMailObject : null),
                                "from_email":  domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                "from_name": req.body.sendMailFrom,
                                "to": mails,
                                "merge_language": "handlebars",
                                "headers": {
                                    "Reply-To": reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                },
                                "tags": ["beebeeboard-invoice", req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"],
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "attachments": [{
                                    'type': 'application/pdf',
                                    'name': doc_detail.nome + '.pdf',
                                    'content': pdfData.toString('base64')
                                }]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "invoice-send",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                sequelize.query('SELECT insert_mandrill_send_invoice(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        req.body.invoice_id + ',' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.sendMailObject ? req.body.sendMailObject.replace(/'/g, "''''") : '') + '\',\'' +
                                        address_string.replace(/'/g, "''''") + '\',' +
                                        (req.body.sendMailMessage ? '\'' + req.body.sendMailMessage.replace(/'/g, "''''") + '\'' : '\'\'') + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });

                            } else {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                sequelize.query('SELECT insert_mandrill_send_invoice(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        req.body.invoice_id + ',' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'' +
                                        result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.sendMailObject.replace(/'/g, "''''") + '\',\'' +
                                        address_string.replace(/'/g, "''''") + '\',' +
                                        (req.body.sendMailMessage ? '\'' + req.body.sendMailMessage.replace(/'/g, "''''") + '\'' : '\'\'') + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {

                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');

                                        res.status(422).send({});

                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });
                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                        });

                    }).catch(function(err) {
                        return res.status(400).render('search_organization_reply_to: ' + err);
                    });
                } else {
                    res.status(422).send({});
                }

            });
        }).on('error', function(err) {
            console.log('VVV'+err);
            res.status(422).send({});
        });

        request.end();

    });
};

exports.send_project_mail = function(req, res) {
    var https = require('https');

    /*req.params.id = req.body.project_id;

    finds.find_project(req, function(err, project) {
        var doc_detail = {
            'nome': 'Report',
            'tipo': 'Report'
        };*/

    var url = req.body.url; //`https://asset.beebeeboard.com/${req.headers['host'].split(".")[0]}/stampe/conto_economico.php?contact_id=${req.body.contact_id}&from_date=${req.body.from_date}&to_date=${req.body.to_date}&access_token=${req.body.token}&$_t=${moment().format('HHmmss')}`;

    var request = https.get(url, function(result) {

        var chunks = [];

        result.on('data', function(chunk) {
            chunks.push(chunk);
        });

        result.on('end', function() {

            var pdfData = new Buffer.concat(chunks);


            if (req.body.sendMailTo) {
                var addresses = req.body.sendMailTo.split(',');
                var address_string = '';
                var mails = [];
                addresses.forEach(function(address) {
                    address_string += ' ' + address;
                    var mail = {
                        "email": address,
                        "name": null,
                        "type": "to"
                    };
                    mails.push(mail);
                });

                if (req.body.sendMailCc) {
                    addresses = req.body.sendMailCc.split(',');
                    addresses.forEach(function(address) {
                        address_string += ' ' + address;
                        var mail = {
                            "email": address,
                            "name": null,
                            "type": "bcc"
                        };
                        mails.push(mail);
                    });
                }

                sequelize.query('SELECT search_organization_reply_to_new(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(reply_mail) {

                       var reply_to = 'no-reply@beebeemailer.com';
                            var domain_from_mail = 'no-reply@beebeemailer.com';

                            if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                                reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                                reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                    reply_to = domain_from_mail;
                                }
                            }

                        var template_content = [{
                                "name": "LANG",
                                "content": req.body.lang
                            }, {
                                "name": "CONTACT_NAME",
                                "content": (req.body.contact_name ? req.body.contact_name : '')
                            }, {
                                "name": "DATE",
                                "content": moment().format('DD/MM/YYYY')
                            }, {
                                "name": "ORGANIZATION",
                                "content": req.body.organization_name
                            }, {
                                "name": "USER_MESSAGE",
                                "content": (req.body.sendMailMessage ? req.body.sendMailMessage : '')
                            }, {
                                "name": "COLOR",
                                "content": req.body.color ? req.body.color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "subject": (req.body.sendMailObject ? req.body.sendMailObject : null),
                                "from_email": domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                "from_name": req.body.sendMailFrom,
                                "to": mails,
                                "headers": {
                                    "Reply-To":  reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                },
                                "tags": ["beebeeboard-report", req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"],
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "attachments": [{
                                    'type': 'application/pdf',
                                    'name': (req.body.contact_name ? req.body.contact_name : '') + '_report.pdf',
                                    'content': pdfData.toString('base64')
                                }]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "report-send",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                sequelize.query('SELECT insert_mandrill_send_project(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        req.body.project_id + ',' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.sendMailObject ? req.body.sendMailObject.replace(/'/g, "''''") : '') + '\',\'' +
                                        address_string.replace(/'/g, "''''") + '\',' +
                                        (req.body.sendMailMessage ? '\'' + req.body.sendMailMessage.replace(/'/g, "''''") + '\'' : '\'\'') + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });

                            } else {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                sequelize.query('SELECT insert_mandrill_send_project(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        req.body.project_id + ',' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'' +
                                        result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.sendMailObject.replace(/'/g, "''''") + '\',\'' +
                                        address_string.replace(/'/g, "''''") + '\',' +
                                        (req.body.sendMailMessage ? '\'' + req.body.sendMailMessage.replace(/'/g, "''''") + '\'' : '\'\'') + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {

                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');

                                        res.status(422).send(utility.error422('Reject', 'Reject Reason', result[0].reject_reason, '422'));

                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });
                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                        });
                    }).catch(function(err) {
                        return res.status(400).render('search_organization_reply_to ' + err);
                    });
            } else {
                res.status(422).send(utility.error422('sendMailTo', 'Invalid attribute', 'Il campo sendMailTo non può essere vuoto', '422'));
            }

        });

    }).on('error', function(err) {
        console.log(err);
        return false;
    });

    request.end();

    /*});*/
};

exports.send_contact_mail = function(req, res) {
    var https = require('https');

    req.params.id = req.body.contact_id;

    finds.find_contact(req, function(err, contact) {
        var doc_detail = 'Situazione_contabile_' + req.body.contact_name + '.pdf';

        var url = `https://asset.beebeeboard.com/asset_scuole_sci/stampa.php?oriented=P&organization=${req.headers['host'].split(".")[0]}&stampa=conto_economico&pdf=true&contact_id=${req.body.contact_id}&from_date=${req.body.from_date}&to_date=${req.body.to_date}&access_token=${req.body.token}&$_t=${moment().format('HHmmss')}`;

        var request = https.get(url, function(result) {

            var chunks = [];

            result.on('data', function(chunk) {
                chunks.push(chunk);
            });

            result.on('end', function() {

                var pdfData = new Buffer.concat(chunks);

                if (req.body.sendMailTo) {
                    var addresses = req.body.sendMailTo.split(',');
                    var address_string = '';
                    var mails = [];
                    addresses.forEach(function(address) {
                        address_string += ' ' + address;
                        var mail = {
                            "email": address,
                            "name": null,
                            "type": "to"
                        };
                        mails.push(mail);
                    });

                    if (req.body.sendMailCc) {
                        addresses = req.body.sendMailCc.split(',');
                        addresses.forEach(function(address) {
                            address_string += ' ' + address;
                            var mail = {
                                "email": address,
                                "name": null,
                                "type": "bcc"
                            };
                            mails.push(mail);
                        });
                    }

                    sequelize.query('SELECT search_organization_reply_to_new(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(reply_mail) {

                          
                            var reply_to = 'no-reply@beebeemailer.com';
                            var domain_from_mail = 'no-reply@beebeemailer.com';

                            if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                                reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                                reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                    reply_to = domain_from_mail;
                                }
                            }



                            var template_content = [{
                                    "name": "LANG",
                                    "content": req.body.lang
                                }, {
                                    "name": "SITUAZIONE",
                                    "content": true
                                }, {
                                    "name": "CONTACT_NAME",
                                    "content": (req.body.contact_name ? req.body.contact_name : '')
                                }, {
                                    "name": "DATE",
                                    "content": moment(new Date()).format('DD/MM/YYYY')
                                }, {
                                    "name": "ORGANIZATION",
                                    "content": uc_first(req.body.organization_name)
                                }, {
                                    "name": "USER_MESSAGE",
                                    "content": (req.body.sendMailMessage ? req.body.sendMailMessage : '')
                                }, {
                                    "name": "COLOR",
                                    "content": req.body.color ? req.body.color : '#00587D'
                                }, {
                                    "name": "LOGO",
                                    "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "subject": (req.body.sendMailObject && req.body.sendMailObject != 'Situazione cliente' ? req.body.sendMailObject : 'Situazione contabile cliente'),
                                    "from_email": domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                    "from_name": uc_first(req.body.organization_name),
                                    "to": mails,
                                    "headers": {
                                        "Reply-To": reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                    },
                                    "tags": ["beebeeboard-report", req.headers['host'].split(".")[0]],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"],
                                    "metadata": {
                                        "organization": req.headers['host'].split(".")[0]
                                    },
                                    "attachments": [{
                                        'type': 'application/pdf',
                                        'name': doc_detail,
                                        'content': pdfData.toString('base64')
                                    }]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "report-send",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {

                                if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                    console.log("Mail sended correctly");

                                    sequelize.query('SELECT insert_mandrill_send_conto_economico(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.body.contact_id + ',' +
                                            req.user.id + ',\'' +
                                            result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.sendMailObject ? req.body.sendMailObject.replace(/'/g, "''''") : '') + '\',\'' +
                                            address_string.replace(/'/g, "''''") + '\',' +
                                            (req.body.sendMailMessage ? '\'' + req.body.sendMailMessage.replace(/'/g, "''''") + '\'' : '\'\'') + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {


                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                            res.status(200).send({});


                                        }).catch(function(err) {
                                            return res.status(400).render('mandrill send mail: ' + err);
                                        });

                                } else {
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                    sequelize.query('SELECT insert_mandrill_send_conto_economico(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.body.contact_id + ',' +
                                            req.user.id + ',\'' +
                                            result[0].status + '\',\'' +
                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + req.body.sendMailObject.replace(/'/g, "''''") + '\',\'' +
                                            address_string.replace(/'/g, "''''") + '\',' +
                                            (req.body.sendMailMessage ? '\'' + req.body.sendMailMessage.replace(/'/g, "''''") + '\'' : '\'\'') + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {

                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');

                                            res.status(422).send({});

                                        }).catch(function(err) {
                                            return res.status(400).render('mandrill send mail: ' + err);
                                        });
                                }
                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

                            });
                        }).catch(function(err) {
                            return res.status(400).render('search_organization_reply_to: ' + err);
                        });


                } else {
                    res.status(422).send({});
                }

            });
        }).on('error', function() {
            console.log(err);
            return false;
        });

        request.end();

    });
};

function conferma_prenotazione_scuola(org){
    var testo = '<br/>In allegato potrà trovare la conferma della prenotazione valida per la lezione. <br/>';

    if(org == 'maestriscicristallo' || org == 'demo_scuole_di_sci'){
        testo += 'Le ricordiamo che sarà necessario presentarsi presso la nostra Segreteria almeno 1 ora prima dell\'inizio della lezione/corso per il saldo finale e per le ultime informazioni. <br/>PRENOTA QUI L\'ATTREZZATURA AL NOLEGGIO CRISTALLO: <a href="http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/">http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/</a>.<br/>Paghi solo la caparra del 20% e poi usufruisci degli sconti previsti sul saldo finale grazie alla prenotazione delle lezioni / corsi con i nostri Maestri.<br/>';
    }
    else if(org == 'snowacademy'){
           testo += 'Le ricordiamo di mostrare al maestro tale conferma sul telefono al momento della lezione.<br/>';
    }
    else if(org == 'scuolascipaganella'){
           testo += 'in allegato può trovare il PDF con il riepilogo della prenotazione richiesta.<br/>Le ricordiamo che, salvo diversi accordi con la segreteria, le lezioni per essere confermate devono essere saldate con due giorni di anticipo rispetto all’inizio delle stesse.<br/>Grazie per averci scelto!';
    }
    else if(org == 'promescaiol'){
           testo += '<br/>Grazie per averci scelto per la tua vacanza sulla neve!<br/>In allegato troverai la conferma della prenotazione. Verifica che date ed orari siano corretti e conserva il tagliando sul tuo telefono. Il primo giorno di lezione il tuo maestro ti chiederà di mostrarglielo.<br/>Ti ricordiamo che se non hai saldato l’intera quota dovrai passare in segreteria prima dell’inizio del corso / lezione. Troverai i nostri uffici a valle presso il noleggio Promescaiol, sotto la partenza della telecabina di Daolasa, oppure direttamente sulle piste all’Alpe Daolasa a 2045mt, all’interno del deposito sci Promescaiol.<br/>Il punto di ritrovo per la lezione / corso sarà, all’orario indicato sul tagliando, direttamente all’interno del deposito riscaldato.<br/>Una volta arrivati in quota con la telecabina di Daolasa:<br/>• Entra nello stabile a vetri che trovi sulla tua sinistra appena sceso dalla telecabina<br/>• Prendi le scale mobili all’interno dello stabile di vetro e sali al primo piano<br/>• Troverai sulla tua destra il nostro deposito e al suo interno la scuola sci.<br/><br/>Assicurati di acquistare lo skipass corretto richiesto per il corso/lezione! Troverai il riquadro “comprensorio” nel riepilogo della prenotazione qui sotto. Il nome del comprensorio è uguale al nome dello skipass che dovrai acquistare.<br/>Qui trovi i nostri consigli per prepararti al meglio per la tua lezione: <a href="https://promescaiol.com/consigli-dai-tuoi-maestri/" target="_blank">Consigli dai tuoi maestri</a><br/>Ti aspettiamo sulle piste!<br/><br/>Promescaiol Ski & Snow Academy';
    }
    else{
           testo += 'In allegato il tagliando delle lezioni.<br/>';
    }

    return testo + '<br/>Grazie per averci scelto.<br/><br/>';
};

function conferma_prenotazione_scuola_english(org){
    var testo = ' <br/>Please find attached the confirmation of your lesson. <br/>';

    if(org == 'maestriscicristallo' || org == 'demo_scuole_di_sci'){
        testo += 'We remind you to arrive at the Secretariat at least 1 hour before the lesson for the latest information and the balance of the reservation. <br/>BOOK HERE THE EQUIPMENT FOR CRISTALLO RENTAL: <a href="http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/">http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/</a><br/>You only pay the 20% deposit and then take advantage of the discounts provided on the final balance thanks to booking lessons / courses with our instructors.<br/>';
    }
    else if(org == 'snowacademy'){
           testo += 'We kindly remind you to show it on your phone to the instructor on the day of the lesson.<br/>';
    }
    else if(org == 'scuolascipaganella'){
           testo += 'in attachment you can find the PDF with the summary of the requested booking.<br/>We remind you that, unless otherwise agreed with the secretariat, in order to be confirmed, the lessons must be paid for two days in advance of the start of the same.<br />Thank you for choosing us!';
    }
    else if(org == 'promescaiol'){
        test += '<br/>Thank you for choosing us for your ski holiday!<br/>Attached you will find the booking confirmation. Check that the dates and times are correct and keep the coupon on your phone. On the first day of class, your teacher will ask you to show it to him.<br/>We remind you that if you have not paid the full class you will have to go to our office before the start of the course / lesson. You will visit our offices in the valley at the Promescaiol rental, under the departure of the Daolasa cable car, or directly on the slopes at Alpe Daolasa at 2045m, inside the Promescaiol ski depot.<br/>The meeting point for the lesson / course will be, at the time indicated on the coupon, directly inside the heated warehouse.<br/>Once you reach the top with the Daolasa cable car:<br/>• Enter the glass building on your left as soon as you get off the cable car<br/>• Take the escalators inside the glass building and go up to the first floor<br/>• You will find our depot on your right and the ski school inside.<br/><br/>Make sure you purchase the correct ski pass required for the course / lesson! You will find the "area" box in the booking summary below. The name of the area is the same as the name of the ski pass that you will have to buy.<br/>Here you will find our tips to better prepare you for your lesson:: <a href="https://promescaiol.com/consigli-dai-tuoi-maestri/" target="_blank">Click here</a><br/>We are waiting for you on the slopes!<br/><br/>Promescaiol Ski & Snow Academy';
    }
    else{
           testo += '';
    }

    return testo + '<br/>Kind regards<br/><br/>';
};

function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};