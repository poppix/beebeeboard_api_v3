var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var payment_method = {};

    if (req.body.data.attributes !== undefined) {

        payment_method['rank'] = req.body.data.attributes.rank;
        payment_method['expand'] = req.body.data.attributes.expand;
        payment_method['organization_id'] = req.user.organization_id;
        payment_method['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        payment_method['conto_prima_nota'] = (req.body.data.attributes.conto_prima_nota && utility.check_type_variable(req.body.data.attributes.conto_prima_nota, 'string') && req.body.data.attributes.conto_prima_nota !== null ? req.body.data.attributes.conto_prima_nota.replace(/'/g, "''''") : '');
        payment_method['icon'] = (req.body.data.attributes.icon && utility.check_type_variable(req.body.data.attributes.icon, 'string') && req.body.data.attributes.icon !== null ? req.body.data.attributes.icon.replace(/'/g, "''''") : '');
        payment_method['_tenant_id'] = req.user._tenant_id;
        payment_method['organization'] = req.headers['host'].split(".")[0];

        sequelize.query('SELECT insert_payment_method(\'' +
                JSON.stringify(payment_method) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_payment_method;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'payment_method',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_payment_method,
                    'Aggiunto payment_method ' + datas[0].insert_payment_method
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('payment_method_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT update_payment_method_v1(' +
                req.params.id + ',' +
                req.body.data.attributes.rank + ',' +
                req.body.data.attributes.expand + ',\'' +
                (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.icon && utility.check_type_variable(req.body.data.attributes.icon, 'string') && req.body.data.attributes.icon !== null ? req.body.data.attributes.icon.replace(/'/g, "''''") : '') + '\',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',\''+
                (req.body.data.attributes.conto_prima_nota && utility.check_type_variable(req.body.data.attributes.conto_prima_nota, 'string') && req.body.data.attributes.conto_prima_nota !== null ? req.body.data.attributes.conto_prima_nota.replace(/'/g, "''''") : '') + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'payment_method',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato payment_method ' + req.params.id
                );

                res.json({
                    'data': req.body.data
                });

            }).catch(function(err) {
                return res.status(400).render('payment_method_update: ' + err);
            });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_payment_method_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'payment_method',
                    'id': datas[0].delete_payment_method_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('payment_method_destroy: ' + err);
        });
};