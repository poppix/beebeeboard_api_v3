var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.index = function(req, res) {

    utility.get_parameters(req, 'product_status', function(err, parameters) {
        if (err) {
            return res.status(400).render('product_states_index: ' + err);
        }

        async.parallel({
            data: function(callback) {
                var response = {};
                sequelize.query('SELECT product_states_data_v3(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                        parameters.archivied + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        if (datas[0].product_states_data_v3 && datas[0].product_states_data_v3 != null) {
                            callback(null, datas[0].product_states_data_v3);

                        } else {

                            callback(null, []);
                        }

                    }).catch(function(err) {
                        console.log(err);
                        callback(err, null);
                    });
            }
        }, function(err, results) {

            if (err) {
                return res.status(400).render('product_states_index: ' + err);
            }

            res.json({
                'data': results.data
            });
        });
    });
};

exports.find = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    utility.get_parameters(req, 'product_status', function(err, parameters) {
        if (err) {
            return res.status(400).render('product_states_index: ' + err);
        }

        async.parallel({
            data: function(callback) {
                var response = {};
                sequelize.query('SELECT product_states_find_data_v3(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.params.id + ',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                        parameters.archivied + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        if (datas[0].product_states_find_data_v3 && datas[0].product_states_find_data_v3 != null) {
                            callback(null, datas[0].product_states_find_data_v3);

                        } else {
                            callback(null, []);
                        }

                    }).catch(function(err) {
                        console.log(err);
                        callback(err, null);
                    });
            }
        }, function(err, results) {
            if (err) {
                return res.status(400).render('product_states_find: ' + err);
            }
            res.json({
                'data': results.data[0]
            });
        });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var product_status = {};

    if (req.body.data.attributes !== undefined) {

        product_status['rank'] = req.body.data.attributes.rank;
        product_status['organization_id'] = req.user.organization_id;
        product_status['status_name'] = (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name, 'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '');
        product_status['status_color'] = (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color, 'string') && req.body.data.attributes.status_color !== null ? req.body.data.attributes.status_color.replace(/'/g, "''''") : '');
        product_status['status_default'] = (req.body.data.attributes.status_default !== undefined && utility.check_type_variable(req.body.data.attributes.status_default, 'boolean') && req.body.data.attributes.status_default !== null ? req.body.data.attributes.status_default : false);
        product_status['_tenant_id'] = req.user._tenant_id;
        product_status['organization'] = req.headers['host'].split(".")[0];
        product_status['mongo_id'] = null;

        sequelize.query('SELECT insert_product_status(\'' +
                JSON.stringify(product_status) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_product_status;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'product_status',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_product_status,
                    'Aggiunto product_status ' + datas[0].insert_product_status
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'product_state', 'insert', req.user.id, datas[0].insert_product_status, req.body.data);
        

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('product_status_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT update_product_status(' +
                req.params.id + ',' +
                req.body.data.attributes.rank + ',\'' +
                (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name, 'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color, 'string') && req.body.data.attributes.status_color !== null ? req.body.data.attributes.status_color.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.status_default !== undefined && utility.check_type_variable(req.body.data.attributes.status_default, 'boolean') && req.body.data.attributes.status_default !== null ? req.body.data.attributes.status_default : false) + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'product_status',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato product_status ' + req.params.id
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'product_state', 'update', req.user.id, req.params.id, req.body.data);
        

                res.json({
                    'data': req.body.data
                });

            }).catch(function(err) {
                return res.status(400).render('product_status_update: ' + err);
            });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_product_status_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            utility.save_socket(req.headers['host'].split(".")[0], 'product_state', 'delete', req.user.id, req.params.id, null);
        

            res.json({
                'data': {
                    'type': 'product_status',
                    'id': datas[0].delete_product_status_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('product_status_destroy: ' + err);
        });
};