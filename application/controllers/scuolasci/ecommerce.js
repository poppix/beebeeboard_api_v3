var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    config = require('../../../config');
var https = require('https');

exports.ecommerce_check_product = function(req, res) {
    
    sequelize.query('SELECT scuolasci_ecommerce_check_product(null,\'' +
        req.headers['host'].split(".")[0] + '\',null,\'' +
        JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].scuolasci_ecommerce_check_product && datas[0].scuolasci_ecommerce_check_product != -1){
            res.status(200).send({'data':datas[0].scuolasci_ecommerce_check_product});
        }else
        {
            res.status(422).send({});
        }
    }).catch(function(err) {
        //console.log(err);
        return res.status(400).render('scuolasci_ecommerce_check_product: ' + err);
    });
            

};

exports.ecommerce_in_vendita = function(req, res) {

    var comprensorio = null,
        product_id = null,
        from_date = null,
        to_date = null;

    if (req.query.comp !== undefined && req.query.comp !== '') {
        comprensorio = req.query.comp.replace(/'/g, "''''");
    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {
        product_id = req.query.product_id;
    }

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\''+req.query.from_date+'\'';
    } 

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\''+req.query.to_date+'\'';
    } 


    sequelize.query('SELECT scuolasci_in_vendita_ecommerce(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        from_date+','+
        to_date+','+ 
        req.user.id + ','+
        req.user.role_id+','+
        comprensorio+','+
        product_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].scuolasci_in_vendita_ecommerce){
            res.status(200).send(datas[0].scuolasci_in_vendita_ecommerce);
        }else
        {
            res.status(422).send({});
        }
    }).catch(function(err) {
        //console.log(err);
        return res.status(400).render('scuolasci_in_vendita_ecommerce: ' + err);
    });
            

};

exports.ecommerce_vetrina = function(req, res) {
    
    sequelize.query('SELECT scuolesci_ecommerce_vetrina(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',\'' +
        JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
       
        if(datas && datas[0] && datas[0].scuolesci_ecommerce_vetrina){
            res.status(200).send({'data':datas[0].scuolesci_ecommerce_vetrina});
        }
    }).catch(function(err) {
        //console.log(err);
        return res.status(400).render('scuolesci_ecommerce_vetrina: ' + err);
    });
            

};

exports.get_ecommerce_vetrina = function(req, res) {
    var util = require('util');
    
   
    sequelize.query('SELECT scuolesci_get_ecommerce_vetrina(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
        if(datas && datas[0] && datas[0].scuolesci_get_ecommerce_vetrina && datas[0].scuolesci_get_ecommerce_vetrina[0]){
            res.status(200).send({'data':datas[0].scuolesci_get_ecommerce_vetrina[0]});
        }
    }).catch(function(err) {
        //console.log(err);
        return res.status(400).render('scuolesci_get_ecommerce_vetrina: ' + err);
    });
            

};

exports.set_dispo_pacchetto = function(req, res) {
    var util = require('util');

    var contact_id = null,
        starts = [],
        ends = [],
        dispo = 0,
        product_id = null
    comprensorio = null;

    if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.dispo !== undefined && req.body.dispo !== '') {
        dispo = req.body.dispo;
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        comprensorio = req.body.comp.replace(/'/g, "''''");
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }


    if(starts.length > 0){
       
        var i,j,new_start, new_end,chunk = 10;
        for (i=0,j=starts.length; i<j; i+=chunk) {
            new_start = starts.slice(i,i+chunk);
            new_end = ends.slice(i,i+chunk);

            
                sequelize.query('SELECT scuolasci_crea_dispo_pacchetto_ecommerce_new(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.id + ',' +
                    (new_start.length > 0 ? 'ARRAY[' + new_start + ']::character varying[]' : null) + ',' +
                    (new_end.length > 0 ? 'ARRAY[' + new_end + ']::character varying[]' : null) + ',' +
                    dispo + ',\'' +
                    comprensorio + '\',' +
                    product_id + ','+
                    contact_id+');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                    new_start = [];
                    new_end = [];
                    cnt = 0;
                    console.log('DISPO ONLINE FISSATA');
                }).catch(function(err) {
                    console.log(err);
                    //return res.status(400).render('scuolasci_crea_dispo_pacchetto_ecommerce: ' + err);
                });
            

        }

        
        return res.status(200).send({});
        
    }else{
        return res.status(422).send({});
    }   
};

exports.ecommerce_bonifico = function(req, res) {

    var payment_id = null;
    var mandrill = require('mandrill-api/mandrill');
    var nome_banca = req.body.dati_banca_nome;
    var iban = req.body.dati_banca_iban;
    var swift = req.body.dati_banca_swift;
    var filiale = req.body.dati_banca_filiale;

    if (req.body.payment_id !== undefined && req.body.payment_id !== '') {
        if (utility.check_id(req.body.payment_id)) {
            callback_(null, utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        payment_id = req.body.payment_id;
    }

    sequelize.query('SELECT scuole_ecommerce_bonifico(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',' +
        payment_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

        sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {

            sequelize.query('SELECT scuolasci_ecommerce_find_projects_from_payment(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ',' +
                payment_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                   useMaster: true
                }).then(function(product) {

                var attachments = [];
                var vouchers = [];
                 
                if (product && product[0] && product[0].scuolasci_ecommerce_find_projects_from_payment && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length > 0) {
                    var cnt = 0;
                    //product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products.forEach(function(us) {
                    async.eachSeries(product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects, function iteratee(us, next) {
                        
                        if(us.use_voucher){
                            let voucher = JSON.parse(us.use_voucher);
                            
                            voucher.forEach(function(vouch){
                                console.log(vouch);
                                vouchers.push({'project_id': us.id,
                                                'payment_id':vouch.id});

                            });
                        }

                        if(us.is_voucher){
                             var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?payment_id=${payment_id}&voucher=true&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                var request = https.get(url, function(result) {

                                    var chunks = [];

                                    result.on('data', function(chunk) {
                                        chunks.push(chunk);
                                    });

                                    result.on('end', function() {
                                        var pdfData = new Buffer.concat(chunks);
                                        var attachment = {};
                                        
                                        console.log('tagliando');
                                        attachment = {
                                            'type':'application/pdf',
                                            'name': 'prenotazione.pdf',
                                            'content':  pdfData.toString('base64')
                                        };

                                        attachments.push(attachment);
                                       next(null,null);
                                        
                                    });
                                }).on('error', function(err) {

                                    next(1, null);
                                });

                                request.end();
                        }
                        else if(us.voucher){
                            var cnt = 0;
                            let voucher1 = (us.voucher);
                             voucher1.forEach(function(vouch){
                                
                                 var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?payment_id=${payment_id}&voucher_new=true&voucher_id=${vouch.id}&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;
                                 console.log(url);
                                    var request = https.get(url, function(result) {

                                        var chunks = [];

                                        result.on('data', function(chunk) {
                                            chunks.push(chunk);
                                        });

                                        result.on('end', function() {
                                            var pdfData = new Buffer.concat(chunks);
                                            var attachment = {};
                                            
                                            console.log('tagliando');
                                            attachment = {
                                                'type':'application/pdf',
                                                'name': 'prenotazione.pdf',
                                                'content':  pdfData.toString('base64')
                                            };

                                            attachments.push(attachment);
                                            if(cnt++ >= voucher1.length-1){
                                                    next(null,null);
                                                }
                                                
                                            });
                                        }).on('error', function(err) {
                                            console.log('err',err);
                                            if(cnt++ >= voucher1.length-1){
                                                next(1,null);
                                            }
                                          
                                        });
                                           
                                    
                                    request.end();
                                });
                        }
                    //product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.forEach(function(us) {
                        else
                        {
                            if(us.product_status_id == 92){
                                var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?albergo=true&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                var request = https.get(url, function(result) {

                                    var chunks = [];

                                    result.on('data', function(chunk) {
                                        chunks.push(chunk);
                                    });

                                    result.on('end', function() {
                                        var pdfData = new Buffer.concat(chunks);
                                        var attachment = {};
                                        
                                        console.log('tagliando');
                                        attachment = {
                                            'type':'application/pdf',
                                            'name': 'prenotazione.pdf',
                                            'content':  pdfData.toString('base64')
                                        };

                                        attachments.push(attachment);
                                       next(null,null);
                                        
                                    });
                                }).on('error', function(err) {

                                    next(1, null);
                                });

                                request.end();
                            }else
                            {
                                if(us.partecipanti){
                                    var cnt = 0;
                                    us.partecipanti.forEach(function(part){
                                        var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?albergo=true&partecipante_id=${part}&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                        var request = https.get(url, function(result) {

                                            var chunks = [];

                                            result.on('data', function(chunk) {
                                                chunks.push(chunk);
                                            });

                                            result.on('end', function() {
                                                var pdfData = new Buffer.concat(chunks);
                                                var attachment = {};
                                                
                                                console.log('tagliando gruppo');
                                                attachment = {
                                                    'type':'application/pdf',
                                                    'name': 'prenotazione.pdf',
                                                    'content': pdfData.toString('base64')
                                                };

                                                attachments.push(attachment);
                                                if(cnt++ >= us.partecipanti.length-1){
                                                    next(null,null);
                                                }
                                                
                                            });
                                        }).on('error', function(err) {

                                            if(cnt++ >= us.partecipanti.length-1){
                                                next(1,null);
                                            }
                                        });

                                        request.end();
                                    });
                                }
                                /*RICHIESTA PAGAMENTO A VUOTO*/
                                else{
                                    next(1, null);
                                }
                            }
                        }
                        
                    }, function done(){

                        if(vouchers && vouchers.length > 0){
                                /* HA ACQUISTATO DA ECOMMERCE USANDO UN VOUCHER, LO USO 
                                   PER ANDARE A SALDARE LE PRENOTAZIONI ASSOCIATE A QUESTO
                                   PAGAMENTO
                                */
                                vouchers.forEach(function(voucher){
                                    sequelize.query('SELECT use_voucher_new(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\', ' +
                                        req.user.id + ',' +
                                        voucher.payment_id + ', ' +
                                        voucher.project_id + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0]['use_voucher_new']) {
                                            //res.status(200).send({'data':datas[0]['update_contact_all']});
                                           
                                            console.log('VOUCHER utilizzato correttamente');
                                                
                                        } else {
                                            console.log('ERRORE UTILIZZO VOUCHER');
                                        }
                                     }).catch(function(err) {
                                            console.log('ERRORE UTILIZZO VOUCHER');
                                    });
                                    
                                });

                            }

                        var mails = [{
                            "email": req.body.mail,
                            "type": "to"
                        }];

                        var mail = datas_o[0].search_organization.ecommerce_mail;

                        if (mail !== '' && mail !== null && mail !== undefined) {
                            mails.push({
                                "email": mail,
                                "type": "to"
                            });
                        }

                        var template_content = [{
                                "name": "LANG",
                                "content": datas_o[0].search_organization.lang
                            }, {
                                "name": "ORGANIZATION",
                                "content": uc_first(datas_o[0].search_organization.name)
                            }, {
                                "name": "COLOR",
                                "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                            }, {
                                "name": "CLIENTE",
                                "content": req.body.nome ? req.body.nome : ''
                            }, {
                                "name": "IBAN",
                                "content": iban ? iban : ''
                            }, {
                                "name": "SWIFT",
                                "content": swift ? swift : ''
                            }, {
                                "name": "FILIALE",
                                "content": filiale ? filiale : ''
                            }, {
                                "name": "BANCA",
                                "content": nome_banca ? nome_banca : ''
                            }, {
                                "name": "IMPORTO",
                                "content": utility.formatta_euro(req.body.importo)
                            }, {
                                "name": "TEXT",
                                "content": testo_personalizzato_bonifico(req.headers['host'].split(".")[0])
                            }, {
                                "name": "TEXT_ENG",
                                "content": testo_personalizzato_bonifico_eng(req.headers['host'].split(".")[0])
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": uc_first(datas_o[0].search_organization.name) + ' - Dati bonifico',
                                "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                "from_name": uc_first(datas_o[0].search_organization.name),
                                "to": mails,
                                "headers": {
                                    "Reply-To":  datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                                },
                                "attachments": attachments,
                                "tags": ["mail_bonifico",req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "mail-bonifico",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {
                            res.status(200).json({});
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                            return res.status(422).send({});
                        });
                    });

                }

            }).catch(function(err) {
                return res.status(400).render('mandrill-ecommerce bonifico find products: ' + err);
            });
        }).catch(function(err) {
            return res.status(400).render('mandrill-ecommerce bonifico search organization: ' + err);
        });
    }).catch(function(err) {
        return res.status(400).render('mandrill-ecommerce bonifico update payment: ' + err);
    });
};

exports.ecommerce_in_loco = function(req, res) {

    var payment_id = null;
    var mandrill = require('mandrill-api/mandrill');
    var nome_banca = req.body.dati_banca_nome;
    var iban = req.body.dati_banca_iban;
    var swift = req.body.dati_banca_swift;
    var filiale = req.body.dati_banca_filiale;

    if (req.body.payment_id !== undefined && req.body.payment_id !== '') {
        if (utility.check_id(req.body.payment_id)) {
            callback_(null, utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        payment_id = req.body.payment_id;
    }

    sequelize.query('SELECT scuole_ecommerce_in_loco(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',' +
        payment_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

        sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {

            sequelize.query('SELECT scuolasci_ecommerce_find_projects_from_payment(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ',' +
                payment_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(product) {
                
                var attachments = [];
                var vouchers = [];

                if (product && product[0] && product[0].scuolasci_ecommerce_find_projects_from_payment && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length > 0) {
                    var cnt = 0;
                    //product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products.forEach(function(us) {
                    async.eachSeries(product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects, function iteratee(us, next) {
                    //product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.forEach(function(us) {
                        console.log(product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length);
                        if(us.use_voucher){
                            let voucher = JSON.parse(us.use_voucher);
                            
                            voucher.forEach(function(vouch){
                                console.log(vouch);
                                vouchers.push({'project_id': us.id,
                                                'payment_id':vouch.id});

                            });
                        }

                        if(us.is_voucher){
                             var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?payment_id=${payment_id}&voucher=true&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                var request = https.get(url, function(result) {

                                    var chunks = [];

                                    result.on('data', function(chunk) {
                                        chunks.push(chunk);
                                    });

                                    result.on('end', function() {
                                        var pdfData = new Buffer.concat(chunks);
                                        var attachment = {};
                                        
                                        console.log('tagliando');
                                        attachment = {
                                            'type':'application/pdf',
                                            'name': 'prenotazione.pdf',
                                            'content':  pdfData.toString('base64')
                                        };

                                        attachments.push(attachment);
                                       next(null,null);
                                        
                                    });
                                }).on('error', function(err) {

                                    next(1, null);
                                });

                                request.end();
                        }
                            //product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.forEach(function(us) {
                        else if(us.voucher){
                            var cnt = 0;

                            var voucher_json = (us.voucher);
                            voucher_json.forEach(function(vouch){
                                
                                 var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?payment_id=${payment_id}&voucher_new=true&voucher_id=${vouch.id}&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;
                                 console.log(url);
                                    var request = https.get(url, function(result) {

                                        var chunks = [];

                                        result.on('data', function(chunk) {
                                            chunks.push(chunk);
                                        });

                                        result.on('end', function() {
                                            var pdfData = new Buffer.concat(chunks);
                                            var attachment = {};
                                            
                                            console.log('tagliando');
                                            attachment = {
                                                'type':'application/pdf',
                                                'name': 'prenotazione.pdf',
                                                'content':  pdfData.toString('base64')
                                            };

                                            attachments.push(attachment);
                                            if(cnt++ >= voucher_json.length-1){
                                                    next(null,null);
                                                }
                                                
                                            });
                                        }).on('error', function(err) {
                                            console.log('err',err);
                                            if(cnt++ >= voucher_json.length-1){
                                                next(1,null);
                                            }
                                          
                                        });
                                           
                                    
                                    request.end();
                                });
                        }
                        else
                        {
                            if(us.product_status_id == 92){
                                var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?albergo=true&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                var request = https.get(url, function(result) {

                                    var chunks = [];

                                    result.on('data', function(chunk) {
                                        chunks.push(chunk);
                                    });

                                    result.on('end', function() {
                                        var pdfData = new Buffer.concat(chunks);
                                        var attachment = {};
                                        
                                        console.log('tagliando');
                                        attachment = {
                                            'type':'application/pdf',
                                            'name': 'prenotazione.pdf',
                                            'content':  pdfData.toString('base64')
                                        };

                                        attachments.push(attachment);
                                        console.log('ciclopriv');
                                        next(null, null);
                                        
                                       
                                        
                                    });
                                }).on('error', function(err) {
                                    console.log('err1',err);
                                    next(1, null);
                                    
                                });

                                request.end();
                            }else
                            {
                                if(us.partecipanti){
                                    var cnt = 0;
                                    us.partecipanti.forEach(function(part){
                                        var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?albergo=true&partecipante_id=${part}&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                        var request = https.get(url, function(result) {

                                            var chunks = [];

                                            result.on('data', function(chunk) {
                                                chunks.push(chunk);
                                            });

                                            result.on('end', function() {
                                                var pdfData = new Buffer.concat(chunks);
                                                var attachment = {};
                                                
                                                console.log('tagliando gruppo');
                                                attachment = {
                                                    'type':'application/pdf',
                                                    'name': 'prenotazione.pdf',
                                                    'content': pdfData.toString('base64')
                                                };

                                                attachments.push(attachment);
                                                console.log('ciclo ', cnt);
                                                if(cnt++ >= us.partecipanti.length-1){
                                                    next(null,null);
                                                }
                                                
                                            });
                                        }).on('error', function(err) {
                                            console.log('err',err);
                                            if(cnt++ >= us.partecipanti.length-1){
                                                next(1,null);
                                            }
                                          
                                        });

                                        request.end();  
                                    });
                                }
                                /*RICHIESTA PAGAMENTO A VUOTO*/
                                else{
                                    next(1, null);
                                }
                            }
                        }

                        
                    }, function done(){

                            if(vouchers && vouchers.length > 0){
                                /* HA ACQUISTATO DA ECOMMERCE USANDO UN VOUCHER, LO USO 
                                   PER ANDARE A SALDARE LE PRENOTAZIONI ASSOCIATE A QUESTO
                                   PAGAMENTO
                                */
                                vouchers.forEach(function(voucher){
                                    sequelize.query('SELECT use_voucher_new(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\', ' +
                                        req.user.id + ',' +
                                        voucher.payment_id + ', ' +
                                        voucher.project_id + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0]['use_voucher_new']) {
                                            //res.status(200).send({'data':datas[0]['update_contact_all']});
                                           
                                            console.log('VOUCHER utilizzato correttamente');
                                                
                                        } else {
                                            console.log('ERRORE UTILIZZO VOUCHER');
                                        }
                                     }).catch(function(err) {
                                            console.log('ERRORE UTILIZZO VOUCHER');
                                    });
                                    
                                });

                            }

                            var mails = [{
                                "email": req.body.mail,
                                "type": "to"
                            }];

                            var mail = datas_o[0].search_organization.ecommerce_mail;

                            if (mail !== '' && mail !== null && mail !== undefined) {
                                mails.push({
                                    "email": mail,
                                    "type": "to"
                                });
                            }

                            var template_content = [{
                                    "name": "LANG",
                                    "content": datas_o[0].search_organization.lang
                                }, {
                                    "name": "ORGANIZATION",
                                    "content": uc_first(datas_o[0].search_organization.name)
                                }, {
                                    "name": "COLOR",
                                    "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                                }, {
                                    "name": "LOGO",
                                    "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                                }, {
                                    "name": "CLIENTE",
                                    "content": req.body.nome ? req.body.nome : ''
                                }, {
                                    "name": "IMPORTO",
                                    "content": utility.formatta_euro(req.body.importo)
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": req.headers['host'].split(".")[0]
                                    },
                                    "subject": uc_first(datas_o[0].search_organization.name) + ' - Prenotazione effettuata',
                                    "from_email":  datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                    "from_name": uc_first(datas_o[0].search_organization.name),
                                    "to": mails,
                                    "headers": {
                                        "Reply-To":  datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                                    },
                                    "attachments": attachments,
                                    "tags": ["mail_bonifico",req.headers['host'].split(".")[0]],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "mail-in-loco",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {
                                res.status(200).json({});
                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                return res.status(422).send({});
                            });
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('mandrill-ecommerce bonifico find products: ' + err);
            });
        }).catch(function(err) {
            return res.status(400).render('mandrill-ecommerce bonifico search organization: ' + err);
        });
    }).catch(function(err) {
        return res.status(400).render('mandrill-ecommerce bonifico update payment: ' + err);
    });
};

exports.ecommerce_zero = function(req, res) {

    var payment_id = null;
    var mandrill = require('mandrill-api/mandrill');
    

    if (req.body.payment_id !== undefined && req.body.payment_id !== '') {
        if (utility.check_id(req.body.payment_id)) {
            callback_(null, utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        payment_id = req.body.payment_id;
    }


        sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {

            sequelize.query('SELECT scuolasci_ecommerce_find_projects_from_payment(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ',' +
                payment_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(product) {
                
                var attachments = [];
                var vouchers = [];

                if (product && product[0] && product[0].scuolasci_ecommerce_find_projects_from_payment && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length && 
                    product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length > 0) {
                    var cnt = 0;
                    //product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products.forEach(function(us) {
                    async.eachSeries(product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects, function iteratee(us, next) {
                    //product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.forEach(function(us) {
                        console.log(product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.length);
                        if(us.use_voucher){
                            let voucher = JSON.parse(us.use_voucher);
                            
                            voucher.forEach(function(vouch){
                                console.log(vouch);
                                vouchers.push({'project_id': us.id,
                                                'payment_id':vouch.id});

                            });
                        }

                        if(us.is_voucher){
                             var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?payment_id=${payment_id}&voucher=true&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                var request = https.get(url, function(result) {

                                    var chunks = [];

                                    result.on('data', function(chunk) {
                                        chunks.push(chunk);
                                    });

                                    result.on('end', function() {
                                        var pdfData = new Buffer.concat(chunks);
                                        var attachment = {};
                                        
                                        console.log('tagliando');
                                        attachment = {
                                            'type':'application/pdf',
                                            'name': 'prenotazione.pdf',
                                            'content':  pdfData.toString('base64')
                                        };

                                        attachments.push(attachment);
                                       next(null,null);
                                        
                                    });
                                }).on('error', function(err) {

                                    next(1, null);
                                });

                                request.end();
                        }
                            //product[0].scuolasci_ecommerce_find_projects_from_payment.json_build_object.projects.forEach(function(us) {
                        else if(us.voucher){
                            var cnt = 0;

                            var voucher_json = (us.voucher);
                            voucher_json.forEach(function(vouch){
                                
                                 var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?payment_id=${payment_id}&voucher_new=true&voucher_id=${vouch.id}&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;
                                 console.log(url);
                                    var request = https.get(url, function(result) {

                                        var chunks = [];

                                        result.on('data', function(chunk) {
                                            chunks.push(chunk);
                                        });

                                        result.on('end', function() {
                                            var pdfData = new Buffer.concat(chunks);
                                            var attachment = {};
                                            
                                            console.log('tagliando');
                                            attachment = {
                                                'type':'application/pdf',
                                                'name': 'prenotazione.pdf',
                                                'content':  pdfData.toString('base64')
                                            };

                                            attachments.push(attachment);
                                            if(cnt++ >= voucher_json.length-1){
                                                    next(null,null);
                                                }
                                                
                                            });
                                        }).on('error', function(err) {
                                            console.log('err',err);
                                            if(cnt++ >= voucher_json.length-1){
                                                next(1,null);
                                            }
                                          
                                        });
                                           
                                    
                                    request.end();
                                });
                        }
                        else
                        {
                            if(us.product_status_id == 92){
                                var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?albergo=true&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                var request = https.get(url, function(result) {

                                    var chunks = [];

                                    result.on('data', function(chunk) {
                                        chunks.push(chunk);
                                    });

                                    result.on('end', function() {
                                        var pdfData = new Buffer.concat(chunks);
                                        var attachment = {};
                                        
                                        console.log('tagliando');
                                        attachment = {
                                            'type':'application/pdf',
                                            'name': 'prenotazione.pdf',
                                            'content':  pdfData.toString('base64')
                                        };

                                        attachments.push(attachment);
                                        console.log('ciclopriv');
                                        next(null, null);
                                        
                                       
                                        
                                    });
                                }).on('error', function(err) {
                                    console.log('err1',err);
                                    next(1, null);
                                    
                                });

                                request.end();
                            }else
                            {
                                if(us.partecipanti){
                                    var cnt = 0;
                                    us.partecipanti.forEach(function(part){
                                        var url = `https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?albergo=true&partecipante_id=${part}&wrap=true&organization=${req.headers['host'].split(".")[0]}&completo=true&project_id=${us.id}&pdf=true&token=${us.token}&$_t=${moment().format('HHmmss')}`;

                                        var request = https.get(url, function(result) {

                                            var chunks = [];

                                            result.on('data', function(chunk) {
                                                chunks.push(chunk);
                                            });

                                            result.on('end', function() {
                                                var pdfData = new Buffer.concat(chunks);
                                                var attachment = {};
                                                
                                                console.log('tagliando gruppo');
                                                attachment = {
                                                    'type':'application/pdf',
                                                    'name': 'prenotazione.pdf',
                                                    'content': pdfData.toString('base64')
                                                };

                                                attachments.push(attachment);
                                                console.log('ciclo ', cnt);
                                                if(cnt++ >= us.partecipanti.length-1){
                                                    next(null,null);
                                                }
                                                
                                            });
                                        }).on('error', function(err) {
                                            console.log('err',err);
                                            if(cnt++ >= us.partecipanti.length-1){
                                                next(1,null);
                                            }
                                          
                                        });

                                        request.end();  
                                    });
                                }
                                /*RICHIESTA PAGAMENTO A VUOTO*/
                                else{
                                    next(1, null);
                                }
                            }
                        }

                        
                    }, function done(){

                            if(vouchers && vouchers.length > 0){
                                /* HA ACQUISTATO DA ECOMMERCE USANDO UN VOUCHER, LO USO 
                                   PER ANDARE A SALDARE LE PRENOTAZIONI ASSOCIATE A QUESTO
                                   PAGAMENTO
                                */
                                vouchers.forEach(function(voucher){
                                    sequelize.query('SELECT use_voucher_new(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\', ' +
                                        req.user.id + ',' +
                                        voucher.payment_id + ', ' +
                                        voucher.project_id + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0]['use_voucher_new']) {
                                            //res.status(200).send({'data':datas[0]['update_contact_all']});
                                           
                                            console.log('VOUCHER utilizzato correttamente');
                                                
                                        } else {
                                            console.log('ERRORE UTILIZZO VOUCHER');
                                        }
                                     }).catch(function(err) {
                                            console.log('ERRORE UTILIZZO VOUCHER');
                                    });
                                    
                                });

                            }

                            var mails = [{
                                "email": req.body.mail,
                                "type": "to"
                            }];

                            var mail = datas_o[0].search_organization.ecommerce_mail;

                            if (mail !== '' && mail !== null && mail !== undefined) {
                                mails.push({
                                    "email": mail,
                                    "type": "to"
                                });
                            }

                            var template_content = [{
                                    "name": "LANG",
                                    "content": datas_o[0].search_organization.lang
                                }, {
                                    "name": "ORGANIZATION",
                                    "content": uc_first(datas_o[0].search_organization.name)
                                }, {
                                    "name": "COLOR",
                                    "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                                }, {
                                    "name": "LOGO",
                                    "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                                }, {
                                    "name": "CLIENTE",
                                    "content": req.body.nome ? req.body.nome : ''
                                }, {
                                    "name": "IMPORTO",
                                    "content": 0.00
                                }, {
                                    "name": "DATA_PAGAMENTO",
                                    "content": moment().format('YYYY-MM-DD')
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": req.headers['host'].split(".")[0]
                                    },
                                    "subject": uc_first(datas_o[0].search_organization.name) + ' - Prenotazione effettuata',
                                    "from_email":  datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                    "from_name": uc_first(datas_o[0].search_organization.name),
                                    "to": mails,
                                    "headers": {
                                        "Reply-To":  datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                                    },
                                    "attachments": attachments,
                                    "tags": ["conferma-pagamento",req.headers['host'].split(".")[0]],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "conferma-pagamento",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {
                                 sequelize.query('SELECT delete_payment_v1(' +
                                    payment_id + ',' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    req.user.id + ',' +
                                    req.user.role_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {
                                });

                                res.status(200).json({});
                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                return res.status(422).send({});
                            });
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('mandrill-ecommerce pagamento a 0 find products: ' + err);
            });
        }).catch(function(err) {
            return res.status(400).render('mandrill-ecommerce pagamento a 0  search organization: ' + err);
        });
    
};

exports.calcola_prezzo = function(req, res) {
    sequelize.query('SELECT scuolasci_calcola_prezzo_prenotazione_new(null,\'' +
            req.headers['host'].split(".")[0] + '\', null,\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolasci_calcola_prezzo_prenotazione_new']) {
                res.status(200).send({ 'data': datas[0]['scuolasci_calcola_prezzo_prenotazione_new']['data'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.calcolatore_prezzo = function(req, res) {
    sequelize.query('SELECT scuolasci_calcolatore_prezzo(null,\'' +
            req.headers['host'].split(".")[0] + '\', null,\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolasci_calcolatore_prezzo']) {
                res.status(200).send({ 'data': datas[0]['scuolasci_calcolatore_prezzo']['data'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.ecommerce_freeprice = function(req, res) {
    sequelize.query('SELECT scuolasci_ecommerce_calcola_prezzo_new(null,\'' +
            req.headers['host'].split(".")[0] + '\', null,\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolasci_ecommerce_calcola_prezzo_new']) {
                res.status(200).send({ 'data': datas[0]['scuolasci_ecommerce_calcola_prezzo_new']['data'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.calcola_prezzo_id = function(req, res) {

    sequelize.query('SELECT scuolasci_calcola_prezzo_prenotazione_id_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolasci_calcola_prezzo_prenotazione_id_new']) {
                res.status(200).send({ 'data': datas[0]['scuolasci_calcola_prezzo_prenotazione_id_new']['data'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.calcola_prezzo_id_fisso = function(req, res) {

    sequelize.query('SELECT scuolasci_calcola_prezzo_fisso_prenotazione_id(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolasci_calcola_prezzo_fisso_prenotazione_id']) {
                res.status(200).send({ 'price': datas[0]['scuolasci_calcola_prezzo_fisso_prenotazione_id'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.product_status = function(req, res) {
    var product_id = req.params.id;

    if (utility.check_id(product_id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolascimontebianco_ecommerce_product_status_v2(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolascimontebianco_ecommerce_product_status_v2']) {

                var product = datas[0]['scuolascimontebianco_ecommerce_product_status_v2'][0]['attributes'];


                if (product.dates) {
                    //var giorni = product.dates.split('\n');
                    /*
                async.parallel({
                    gg1: function(callback){
                        if(giorni[0].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[0].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "1GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g1_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg2: function(callback){
                        if(giorni[1].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[1].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "2GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g2_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg3: function(callback){
                        if(giorni[2].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[2].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "3GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g3_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg4: function(callback){
                        if(giorni[3].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[3].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "4GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g4_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg5: function(callback){
                        if(giorni[4].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[4].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "5GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g5_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg6: function(callback){
                        if(giorni[5].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[5].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "6GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g6_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg10: function(callback){
                        if(giorni[9].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[9].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "10GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g10_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    },
                    gg11: function(callback){
                        if(giorni[10].split(';')[1]){
                            var var_1gg = [];
                            var array_date = giorni[10].split(';');
                            if(array_date.length == 1){
                                callback(null,[]);
                            }
                            for(var i = 1; i< array_date.length; i++){
                                console.log(array_date[i]);
                                var date = array_date[i];
                                if(date.split(',')[0]){
                                    var_1gg.push({
                                        "attributes": {
                                            "data"  : date.split(',')[0],
                                            "durata" : "11GG"
                                        },
                                        "dispo": date.split(',')[1],
                                        "price" : product.g11_price
                                    });
                                }
                            }
                            
        
                            callback(null,var_1gg);
                           
                        }
                        else
                        {
                            callback(null,null);
                        }
                    }
                }, function(err, results){
                    if(err){
                        console.log(err);
                    }

                    
                    var response = {"available_attributes": [
                            "data", "durata"
                        ]};

                    response.variations = [];

                    if(results.gg1){
                        results.gg1.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                    if(results.gg2){
                        results.gg2.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                    if(results.gg3){
                        results.gg3.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                    if(results.gg4){
                        results.gg4.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                    if(results.gg5){
                        results.gg5.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                    if(results.gg6){
                        results.gg6.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                     if(results.gg10){
                        results.gg10.forEach(function(g){
                            response.variations.push(g);
                        });
                    }
                    if(results.gg11){
                        results.gg11.forEach(function(g){
                            response.variations.push(g);
                        });
                    }

                    res.status(200).json(response);
                });*/
                    res.status(200).send();
                } else if (product.dispo_ecommerce) {
                    res.status(200).json({ 'dispo': product.dispo_ecommerce, 'price': product.price });
                } else {
                    res.status(422).json({});
                }

            } else {
                res.status(422).json({});
            }

        }).catch(function(err) {
            return res.status(400).render('scuolascimontebianco_ecommerce_product_status: ' + err);
        });;
};

exports.next_events = function(req, res) {
    var contact_id = req.params.id;

    if (utility.check_id(contact_id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolascimontebianco_next_events_v1(' +
            contact_id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolascimontebianco_next_events_v1']) {
                res.json(datas[0]['scuolascimontebianco_next_events_v1']);
            } else {
                res.status(422).send({});
            }
        });
};

exports.event_individuali = function(req, res) {
    var data_inizio = null,
        contact_id = null,
        comprensorio = null,
        product_id = null,
        ore_piene = false,
        user_id_ = req.query.user_id ? req.query.user_id : null;

    if (req.query.comp !== undefined && req.query.comp !== '') {
        comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.query.inizio !== undefined && req.query.inizio !== '') {
        if (!utility.check_type_variable(req.query.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\''+req.query.inizio+'\'';
    } 

    /*if (req.query.contact_id !== undefined && req.query.contact_id != '' && req.query.contact_id != 'undefined') {
        if(utility.check_id(req.query.contact_id)){
          return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        contact_id = req.query.contact_id;
    }*/

    contact_id = req.query.contact_id !== undefined ? req.query.contact_id : null;

    

    product_id = req.query.product_id !== undefined ? req.query.product_id : null;

    ore_piene = req.query.ore_piene !== undefined ? req.query.ore_piene : false;

    sequelize.query('SELECT scuolasci_events_ecommerce_individuali_new_v1(' +
            contact_id + ',' +
            data_inizio + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            comprensorio + ',' +
            product_id + ',' +
            ore_piene + ','+
            user_id_+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0]['scuolasci_events_ecommerce_individuali_new_v1']) {
                res.status(200).send({ 'data': datas[0]['scuolasci_events_ecommerce_individuali_new_v1'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.find_maestro = function(req, res) {
    var data_inizio = null,
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua = [],
        xpag = 100,
        pag = 0,
        product_id = null,
        q = [],
        filter_private = [];

    var util = require('util');

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {

        product_id = req.body.product_id;
    }

    if (req.body.gen !== undefined && req.body.gen !== '') {
        if (req.body.gen == 'maschio') {
            sesso = '\'true\'';
        } else {
            sesso = '\'false\'';
        }
    }


    if (req.body.lang !== undefined && req.body.lang !== '') {
        lingua = utility.get_lang(req.body.lang);

    }

    if (req.body.filter_private !== undefined && req.body.filter_private != '' && req.body.filter_private != 'undefined') {
        if (util.isArray(req.body.filter_private)) {
            filter_private = req.body.filter_private;
        } else {
            filter_private.push(req.body.filter_private);
        }
    }

    if (req.body.spec !== undefined && req.body.spec !== '') {
        spec = utility.get_specialita(req.body.spec);
        
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.pag !== undefined && req.body.pag !== '') {
        if (utility.check_id(req.body.pag)) {
            return res.status(422).send(utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        pag = req.body.pag;
    }

    if (req.body.xpag !== undefined && req.body.xpag !== '') {
        if (utility.check_id(req.body.xpag)) {
            return res.status(422).send(utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        xpag = req.body.xpag;
    }

    if (req.body.q !== undefined && req.body.q !== '') {
        if (!utility.check_type_variable(req.body.q, 'string')) {
            return (null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.body.q !== undefined && req.body.q !== '') {
            let new_str = req.body.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            
            q_array.forEach(function(s) {
                q.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

    sequelize.query('SELECT scuolascimontebianco_dispo_individuali_ecommerce_new(\'' +
            data_inizio + '\',' +
            sesso + ',' +
            comprensorio + ',' +
            (spec.length > 0 ? 'ARRAY[' + spec + ']::bigint[]' : null) + ',' +
            (lingua.length > 0 ? 'ARRAY[' + lingua + ']::bigint[]' : null) + ',' +
            xpag + ',' +
            ((pag - 1) * xpag) + ',' +
            (q && q.length > 0 ? 'ARRAY[' + q + ']' : null) + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ','+
            (filter_private.length > 0 ? 'ARRAY[' + filter_private + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0]['scuolascimontebianco_dispo_individuali_ecommerce_new'] && datas[0]['scuolascimontebianco_dispo_individuali_ecommerce_new'][0]) {
                if (datas[0]['scuolascimontebianco_dispo_individuali_ecommerce_new'][0]['maestri']) {
                    res.json(datas[0]['scuolascimontebianco_dispo_individuali_ecommerce_new']);
                } else {
                    datas[0]['scuolascimontebianco_dispo_individuali_ecommerce_new'][0]['maestri'] = [];
                    res.json(datas[0]['scuolascimontebianco_dispo_individuali_ecommerce_new']);
                }

            } else {
                res.status(422).send({});
            }
        });
};

exports.ecommerce_private_dashboard = function(req, res) {
    var data_inizio = null,
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua = [],
        lingua_char 
        = [],
        xpag = 20,
        pag = 0,
        product_id = null,
        q = [],
        spec_char = [],
        filter_private = [];

    var util = require('util');

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {

        product_id = req.body.product_id;
    }

    if (req.body.gen !== undefined && req.body.gen !== '') {
        if (req.body.gen == 'M') {
            sesso = true;
        } else {
            sesso = false;
        }
    }

    if (req.body.lang !== undefined && req.body.lang !== '') {
        lingua = utility.get_lang(req.body.lang); 
        if (!utility.check_type_variable(req.body.lang, 'string')) {
            return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        var lang_array = req.body.lang.split(',');
        lang_array.forEach(function(s) {
            lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });       
    }

    if (req.body.filter_private !== undefined && req.body.filter_private != '' && req.body.filter_private != 'undefined') {
        if (util.isArray(req.body.filter_private)) {
            filter_private = req.body.filter_private;
        } else {
            filter_private.push(req.body.filter_private);
        }
    }

    if (req.body.spec !== undefined && req.body.spec !== '') {
        spec = utility.get_lang(req.body.spec); 
        if (!utility.check_type_variable(req.body.spec, 'string')) {
            return (null, utility.error422('spec', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        
        var spec_array = req.body.spec.split(',');
        spec_array.forEach(function(s) {
            spec_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });
        
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.pag !== undefined && req.body.pag !== '') {
        if (utility.check_id(req.body.pag)) {
            return res.status(422).send(utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        pag = req.body.pag;
    }

    if (req.body.xpag !== undefined && req.body.xpag !== '') {
        if (utility.check_id(req.body.xpag)) {
            return res.status(422).send(utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        xpag = req.body.xpag;
    }

    if (req.body.q !== undefined && req.body.q !== '') {
        if (!utility.check_type_variable(req.body.q, 'string')) {
            return (null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.body.q !== undefined && req.body.q !== '') {

            let new_str = req.body.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

    sequelize.query('SELECT scuolesci_individuali_ecommerce_v1(\'' +
            data_inizio + '\',' +
            sesso + ',' +
            comprensorio + ',' +
            (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
            (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
            xpag + ',' +
            pag + ',' +
            (q && q.length > 0 ? 'ARRAY[' + q + ']' : null) + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ','+
            (filter_private.length > 0 ? 'ARRAY[' + filter_private + ']::bigint[]' : null) + ','+
            (req.body.access_token ? '\''+ req.body.access_token +'\'' : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0]['scuolesci_individuali_ecommerce_v1'] && datas[0]['scuolesci_individuali_ecommerce_v1'][0]) {
                
                    res.json(datas[0]['scuolesci_individuali_ecommerce_v1'][0]);
                

            } else {
                res.status(422).send({});
            }
        });
};


exports.ecommerce_private_dashboard_week = function(req, res) {
    var data_inizio = null,
        data_fine = null,
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua = [],
        lingua_char 
        = [],
        xpag = 20,
        pag = 0,
        product_id = null,
        contact_id = null,
        q = [],
        spec_char = [],
        filter_private = [];

    var util = require('util');

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {

        product_id = req.body.product_id;
    }

    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {

        contact_id = req.body.contact_id;
    }

    if (req.body.gen !== undefined && req.body.gen !== '') {
        if (req.body.gen == 'M') {
            sesso = true;
        } else {
            sesso = false;
        }
    }

    if (req.body.lang !== undefined && req.body.lang !== '') {
        lingua = utility.get_lang(req.body.lang); 
        if (!utility.check_type_variable(req.body.lang, 'string')) {
            return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        var lang_array = req.body.lang.split(',');
        lang_array.forEach(function(s) {
            lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });       
    }

    if (req.body.filter_private !== undefined && req.body.filter_private != '' && req.body.filter_private != 'undefined') {
        if (util.isArray(req.body.filter_private)) {
            filter_private = req.body.filter_private;
        } else {
            filter_private.push(req.body.filter_private);
        }
    }

    if (req.body.spec !== undefined && req.body.spec !== '') {
        spec = utility.get_lang(req.body.spec); 
        if (!utility.check_type_variable(req.body.spec, 'string')) {
            return (null, utility.error422('spec', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        
        var spec_array = req.body.spec.split(',');
        spec_array.forEach(function(s) {
            spec_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });
        
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } 

     if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = req.body.fine;
    } 


    if (req.body.pag !== undefined && req.body.pag !== '') {
        if (utility.check_id(req.body.pag)) {
            return res.status(422).send(utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        pag = req.body.pag;
    }

    if (req.body.xpag !== undefined && req.body.xpag !== '') {
        if (utility.check_id(req.body.xpag)) {
            return res.status(422).send(utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        xpag = req.body.xpag;
    }

    if (req.body.q !== undefined && req.body.q !== '') {
        if (!utility.check_type_variable(req.body.q, 'string')) {
            return (null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.body.q !== undefined && req.body.q !== '') {

            let new_str = req.body.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {
                q.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        }
    }

    if(!data_fine){

       async.parallel({
            dispo1: function(callback) {
                 sequelize.query('SELECT scuolesci_individuali_ecommerce_week(' +
                    (data_inizio ? '\''+data_inizio+'\'' : null)+ ',' +
                    sesso + ',' +
                    comprensorio + ',' +
                    (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
                    (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
                    xpag + ',' +
                    pag + ',' +
                    (q && q.length > 0 ? 'ARRAY[' + q + ']' : null) + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    product_id + ','+
                    (filter_private.length > 0 ? 'ARRAY[' + filter_private + ']::bigint[]' : null) + ','+
                    (req.body.access_token ? '\''+ req.body.access_token +'\'' : null)+','+
                    contact_id+',0,null);', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {
                    if (datas && datas[0] && datas[0]['scuolesci_individuali_ecommerce_week'] && datas[0]['scuolesci_individuali_ecommerce_week'][0]) {
                        
                            callback(null,datas[0]['scuolesci_individuali_ecommerce_week'][0]);
                        

                    } else {
                        callback(1,null);
                    }
                });
            }
        }, function(err, results) {

                if(err){
                    return res.status(422).send(utility.error422('scuolesci_individuali_ecommerce_week', 'scuolesci_individuali_ecommerce_week', 'scuolesci_individuali_ecommerce_week', '422'));

                }  else
                {
                    res.status(200).json(
                         results.dispo1
                    );
                }

            });
   
   }
   else{
        sequelize.query('SELECT scuolesci_individuali_ecommerce_week(' +
                     (data_inizio ? '\''+data_inizio+'\'' : null) + ',' +
                    sesso + ',' +
                    comprensorio + ',' +
                    (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
                    (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
                    xpag + ',' +
                    pag + ',' +
                    (q && q.length > 0 ? 'ARRAY[' + q + ']' : null) + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    product_id + ','+
                    (filter_private.length > 0 ? 'ARRAY[' + filter_private + ']::bigint[]' : null) + ','+
                    (req.body.access_token ? '\''+ req.body.access_token +'\'' : null)+','+
                    contact_id+',0,' +
                     (data_fine ? '\''+data_fine+'\'' : null) + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {
                    if (datas && datas[0] && datas[0]['scuolesci_individuali_ecommerce_week'] && datas[0]['scuolesci_individuali_ecommerce_week'][0]) {
                        
                            res.status(200).json(
                                 datas[0]['scuolesci_individuali_ecommerce_week'][0]
                            );
                        

                    } else {
                         return res.status(422).send(utility.error422('scuolesci_individuali_ecommerce_week', 'scuolesci_individuali_ecommerce_week', 'scuolesci_individuali_ecommerce_week', '422'));

                    }
                });
   }
};

exports.ecommerce_checkout_confirm = function(req, res) {
    var moment_timezone = require('moment-timezone');
    var sovrapposizione_carrello = false;

    var prezzo_preno = req.body.prezzo_preno,
        acconto_preno = (req.body.preno_acconto_totale ? req.body.preno_acconto_totale : req.body.prezzo_preno),
        notes = (req.body.notes ? req.body.notes.replace(/'/g, "''''") : ''),
        prenotations = [],
        da_annullare = false,
        cnt_prod = 0;

    if (!req.body.products || !req.body.products) {
        return res.status(422).send(utility.error422('products', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    if (req.body.products && req.body.products.length > 0) {
        var products = {};

        for(var ii = 0; ii <= req.body.products.length -1 ; ii++){
            if (req.body.products[ii].events && 
                req.body.products[ii].events.length > 0) {

                if(!products[req.body.products[ii].id]){
                    products[req.body.products[ii].id] = {
                        'events':[]
                    };
                }

                for(var jj = 0; jj <= req.body.products[ii].events.length-1; jj++){
                    var evento_new = req.body.products[ii].events[jj];

                    if(evento_new.maestro_id != '0'){
                        console.log(evento_new.maestro_id);

                        for(var zz = 0; zz <= products[req.body.products[ii].id].events.length -1 ; zz++){
                            var evento_old = products[req.body.products[ii].id].events[zz];

                            if(evento_old.maestro_id == evento_new.maestro_id &&
                                (
                                   ( moment(evento_new.start_date) >= moment(evento_old.start_date) &&
                                    moment(evento_new.start_date) < moment(evento_old.end_date)) ||
                                   ( moment(evento_new.end_date) > moment(evento_old.start_date) &&
                                    moment(evento_new.end_date) <= moment(evento_old.end_date))

                                )
                            ){
                                        sovrapposizione_carrello = true;
                            }
                        }  

                        products[req.body.products[ii].id].events.push(evento_new);
                   }

                }
            }
        }
        
        if(sovrapposizione_carrello){
           
            return res.status(422).send(utility.error422('No dispo', 'Ore di lezione sovrapposte nel carrello', 'Ore di lezione sovrapposte nel carrello', '422'));

               
        }
        else
        {

            req.body.products.forEach(function(product) {
                switch (parseInt(product.product_status_id)) {
                    case 92: // individuali
                        async.parallel({
                            dispo: function(callback2) {
                                var cnt = 0;
                                var dispos = [];


                                var starts = [];
                                var ends = [];
                               
                                if (product.events && product.events.length > 0) {
                                    var cnt_event = 0;
                                    var tot_ore = 0;
                                    console.log(product.events);

                                    

                                        product.events.forEach(function(event) {
                                            var start_date = moment(event.start_date);
                                            var end_date = moment(event.end_date);
                                            var comprensorio = event.comprensorio.replace(/'/g, "''''");



                                            if (event.maestro_id == '0') {

                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {
                                                    sequelize.query('SELECT scuolasci_ecommerce_control_lezioni_new(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.role_id + ',' +
                                                            req.user.id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],\'' +
                                                            comprensorio + '\',' +
                                                            product.id + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT
                                                            })
                                                        .then(function(data) {
                                                            console.log(data[0].scuolasci_ecommerce_control_lezioni_new);
                                                            if (data[0].scuolasci_ecommerce_control_lezioni_new &&
                                                                data[0].scuolasci_ecommerce_control_lezioni_new != null) {
                                                                if (data[0].scuolasci_ecommerce_control_lezioni_new &&
                                                                    data[0].scuolasci_ecommerce_control_lezioni_new >= (tot_ore * 2)) {

                                                                    dispos.push(1);
                                                                } else {
                                                                    dispos.push(0);
                                                                }
                                                            } else {
                                                                dispos.push(0);
                                                            }

                                                            callback2(null, dispos);

                                                        });

                                                }

                                            } else {




                                                var lingua = null;
                                                switch (event.lingua) {
                                                    case 'italiano':
                                                        lingua = 123;
                                                        break;

                                                    case 'inglese':
                                                        lingua = 125;
                                                        break;

                                                    case 'francese':
                                                        lingua = 124;
                                                        break;

                                                    case 'spagnolo':
                                                        lingua = 128;
                                                        break;

                                                    case 'russo':
                                                        lingua = 127;
                                                        break;

                                                    case 'tedesco':
                                                        lingua = 126;
                                                        break;

                                                    case 'svedese':
                                                        lingua = 255;
                                                        break;

                                                    case 'portoghese':
                                                        lingua = 254;
                                                        break;
                                                }

                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {

                                                    sequelize.query('SELECT scuolasci_ecommerce_control_individuale_new(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.role_id + ',' +
                                                            req.user.id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],' +
                                                            event.maestro_id + ',\'' +
                                                            comprensorio + '\',' +
                                                            product.id + ',' +
                                                            lingua + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT
                                                            })
                                                        .then(function(data) {
                                                            if (data[0].scuolasci_ecommerce_control_individuale_new &&
                                                                data[0].scuolasci_ecommerce_control_individuale_new != null) {
                                                                if (data[0].scuolasci_ecommerce_control_individuale_new[0] &&
                                                                    data[0].scuolasci_ecommerce_control_individuale_new[0].attributes.total >= tot_ore) {

                                                                    dispos.push(1);
                                                                } else {
                                                                    dispos.push(0);
                                                                }
                                                            } else {
                                                                dispos.push(0);
                                                            }

                                                            callback2(null, dispos);

                                                        });
                                                }
                                            }
                                        });
                                    }

                            }
                        }, function(err, results) {
                            if (err) {
                                console.log(err);
                            }

                            if (results.dispo.indexOf(0) == -1) {
                                console.log('TUTTE LIBERE');

                                async.parallel({
                                    preno: function(callback1) {
                                        var cnt = 0;
                                        var prenotazioni = [];

                                        var starts = [];
                                        var ends = [];

                                        if (product.events && product.events.length > 0) {
                                            var cnt_event = 0;
                                            var tot_ore = 0;
                                            var prezzo = 0.00;
                                            var prezzi = [];
                                            var scianti = [];

                                            product.events.forEach(function(event) {
                                                var start_date = moment(event.start_date);
                                                var end_date = moment(event.end_date);
                                                var comprensorio = event.comprensorio.replace(/'/g, "''''");
                                                var lingua = event.lingua;
                                                var gender = (event.gender == 'maschio' ? 'Richiesta maestro: maschio' : (event.gender == 'femmina' ? 'Richiesta maestro: femmina' : ''));
                                                var spec = event.specialità;
                                                var product_id = product.id;
                                                var product_name = product.name;

                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //console.log('start - end '+ start_date + ' '+end_date);
                                                //tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+moment.utc(start_date).format().toString()+'\'');
                                                //ends.push('\''+moment.utc(end_date).format().toString()+'\'');
                                                prezzo += parseFloat(event.prezzo);
                                                //prezzo = product.prezzo;
                                                prezzi.push(event.prezzo);
                                                //event.partecipanti
                                                scianti.push(event.persone);


                                                if (cnt_event++ >= product.events.length - 1) {

                                                    sequelize.query('SELECT scuolasci_ecommerce_preno_individuale_new_v1(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],' +
                                                            req.user.id + ',\'INDIVIDUALE - ' +
                                                            product_name + '\',\'' +
                                                            moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',true,' +
                                                            product.assicurazione + ',' +
                                                            event.maestro_id + ',\'' +
                                                            comprensorio + '\',\'' +
                                                            spec + '\',\'' +
                                                            lingua + '\',' +
                                                            product_id + ',ARRAY[' +
                                                            scianti + ']::bigint[],' +
                                                            prezzo + ',ARRAY[' +
                                                            prezzi + ']::double precision[],\'' +
                                                            gender + ' - ' +notes + '\',\''+
                                                            JSON.stringify(req.body).replace(/'/g, "''''")+'\',\''+
                                                            JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(data) {

                                                            if (data[0].scuolasci_ecommerce_preno_individuale_new_v1) {
                                                                prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_individuale_new_v1));
                                                                //var cnt_events2 = 0;
                                                                prenotazioni.push(data[0].scuolasci_ecommerce_preno_individuale_new_v1);
                                                                
                                                                callback1(null, prenotazioni);

                                                            }

                                                        });
                                                }

                                            });

                                        } else {
                                            if (cnt++ >= products.length - 1) {

                                                callback1(null, prenotazioni);
                                            }
                                        }

                                    }
                                }, function(err, results) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    console.log('FINE INDIVIDUALI: ' + cnt_prod);
                                    if (cnt_prod++ >= req.body.products.length - 1) {
                                        console.log('finito individuali:' + da_annullare);
                                        console.log(prenotations);
                                        if (da_annullare) {
                                            end_checkout_ecommerce(prenotations, da_annullare, req.body.products, req);
                                            return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                        } else {
                                            sequelize.query('SELECT scuolasci_ecommerce_crea_payment(ARRAY[' +
                                                    prenotations + ']::bigint[],' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    req.user.id + ',null,' +
                                                    acconto_preno + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    if (datas[0].scuolasci_ecommerce_crea_payment) {
                                                        return res.status(200).json({
                                                            'payment_id': datas[0].scuolasci_ecommerce_crea_payment
                                                        });

                                                    } else {
                                                        return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                    }

                                                }).catch(function(err) {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                });

                                        }
                                    }
                                    //callback(null, true);
                                    //res.status(200).send({'data': results.preno});
                                });
                            } else {
                                console.log('QUALCHE ORA NON DISPONIBILE');
                                da_annullare = true;
                                async.parallel({
                                    dispo1: function(callback1) {
                                        var cnt = 0;
                                        var dispos = [];

                                        var occupate = [];

                                        var starts = [];
                                        var ends = [];
                                        if (product.events && product.events.length > 0 && results.dispo.indexOf(2) == -1) {
                                            var cnt_event = 0;
                                            var tot_ore = 0;

                                            product.events.forEach(function(event) {
                                                var start_date = moment(event.start_date);
                                                var end_date = moment(event.end_date);
                                                var comprensorio = event.comprensorio.replace(/'/g, "''''");
                                                //console.log('start - end '+ start_date + ' '+end_date);
                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                //console.log('start - end '+ start_date.format('YYYY-MM-DD HH:mm:ss') + ' '+end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {
                                                    sequelize.query('SELECT scuolasci_ecommerce_no_dispo_new(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.role_id + ',' +
                                                            req.user.id + ',' +
                                                            event.maestro_id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],' +
                                                            product.id + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(confr) {
                                                            occupate.push(confr[0].scuolasci_ecommerce_no_dispo_new);

                                                            callback1(null, occupate);

                                                        });
                                                }
                                            });


                                        } else {

                                            callback1(null, occupate);

                                        }

                                    }
                                }, function(err, results) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    console.log('FINE INDIVIDUALI NO LIBERE: ' + cnt_prod);
                                    if (cnt_prod++ >= req.body.products.length - 1) {
                                        console.log('finito individuali:' + da_annullare);
                                        console.log(prenotations);
                                        if (results.dispo1.indexOf(2) != -1){
                                            end_checkout_ecommerce(prenotations, da_annullare, req.body.products, req);
                                            return res.status(422).send(utility.error422('No dispo', 'Ore di lezione sovrapposte nel carrello', 'Ore di lezione sovrapposte nel carrello', '422'));
                                        }
                                        else if(da_annullare) {
                                            end_checkout_ecommerce(prenotations, da_annullare, req.body.products, req);
                                            return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                        } else {
                                            sequelize.query('SELECT scuolasci_ecommerce_crea_payment(ARRAY[' +
                                                    prenotations + ']::bigint[],' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    req.user.id + ',null,' +
                                                    acconto_preno + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    if (datas[0].scuolasci_ecommerce_crea_payment) {
                                                        return res.status(200).json({
                                                            'payment_id': datas[0].scuolasci_ecommerce_crea_payment
                                                        });

                                                    } else {
                                                        return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                    }

                                                }).catch(function(err) {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                });

                                        }
                                    }
                                    //callback(null, false);
                                    //res.status(422).send({'data': results.dispo1});
                                });


                            }

                        });
                        break;

                    case 106: // collettiva
                        var scianti = [];

                        product.partecipanti.forEach(function(sciante) {
                            if (sciante.id) {
                                scianti.push(sciante.id);
                            }
                        });

                        sequelize.query('SELECT scuolasci_ecommerce_preno_collettiva_new_v1(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ',' +
                                '\'' + product.data_inizio + '\',' +
                                req.user.id + ',\' COLLETTIVA - ' +
                                product.name + '\',\'' +
                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',' +
                                product.assicurazione + ',\'' +
                                product.comprensorio + '\',\'' +
                                (product.lingua && product.lingua != '' ? product.lingua : '') + '\',' +
                                product.id + ',ARRAY[' +
                                scianti + ']::bigint[],' +
                                product.prezzo + ',' +
                                product.persone + ',' +
                                product.giorni + ',\'' +
                                notes + '\',\''+
                                JSON.stringify(req.body).replace(/'/g, "''''")+'\',\''+
                                JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(data) {
                                console.log('Collettiva inserted');
                                if (data[0].scuolasci_ecommerce_preno_collettiva_new_v1 != 0) {
                                    prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_collettiva_new_v1));
                                } else {
                                    da_annullare = true;
                                }
                                console.log('FINE COLLETTIVA: ' + cnt_prod);
                                if (cnt_prod++ >= req.body.products.length - 1) {
                                    console.log('finito COLLETTIVA:' + da_annullare);
                                    console.log(prenotations);
                                    if (da_annullare) {
                                        end_checkout_ecommerce(prenotations, da_annullare, req.body.products, req);
                                        return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                    } else {
                                        sequelize.query('SELECT scuolasci_ecommerce_crea_payment(ARRAY[' +
                                                prenotations + ']::bigint[],' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.id + ',null,' +
                                                acconto_preno + ');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                if (datas[0].scuolasci_ecommerce_crea_payment) {
                                                    return res.status(200).json({
                                                        'payment_id': datas[0].scuolasci_ecommerce_crea_payment
                                                    });

                                                } else {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                }

                                            }).catch(function(err) {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            });

                                    }
                                }
                            }).catch(function(err) {
                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                //return res.status(400).render('ecommerce_collettiva: ' + err);
                            });
                        break;

                    case 108: // corso annuale
                        var scianti = [];

                        product.partecipanti.forEach(function(sciante) {
                            if (sciante.id) {
                                scianti.push(sciante.id);
                            }
                        });

                        sequelize.query('SELECT scuolasci_ecommerce_preno_corso_annuale_new_v1(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ',' +
                                '\'' + product.data_inizio + '\',' +
                                req.user.id + ',\'ANNUALE - ' +
                                product.name.replace(/'/g, "''''") + '\',\'' +
                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',' +
                                product.assicurazione + ',\'' +
                                product.comprensorio.replace(/'/g, "''''") + '\',\'' +
                                (product.lingua && product.lingua != '' ? product.lingua : '') + '\',' +
                                product.id + ',ARRAY[' +
                                scianti + ']::bigint[],' +
                                product.prezzo + ',' +
                                product.persone + ',\'' +
                                notes + '\',\''+
                                JSON.stringify(req.body).replace(/'/g, "''''")+'\',\''+
                                JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(data) {
                                console.log('Corso Annuale inserted');
                                if (data[0].scuolasci_ecommerce_preno_corso_annuale_new_v1 != 0) {
                                    prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_corso_annuale_new_v1));
                                } else {
                                    da_annullare = true;
                                }
                                console.log('FINE CORSO: ' + cnt_prod);
                                if (cnt_prod++ >= req.body.products.length - 1) {
                                    console.log('finito CORSO ANNUALE:' + da_annullare);
                                    console.log(prenotations);
                                    if (da_annullare) {
                                        end_checkout_ecommerce(prenotations, da_annullare, req.body.products, req);
                                        return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                    } else {
                                        sequelize.query('SELECT scuolasci_ecommerce_crea_payment(ARRAY[' +
                                                prenotations + ']::bigint[],' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.id + ',null,' +
                                                acconto_preno + ');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                if (datas[0].scuolasci_ecommerce_crea_payment) {
                                                    return res.status(200).json({
                                                        'payment_id': datas[0].scuolasci_ecommerce_crea_payment
                                                    });

                                                } else {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                }

                                            }).catch(function(err) {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            });

                                    }
                                }
                            }).catch(function(err) {
                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                //return res.status(400).render('ecommerce_collettiva: ' + err);
                            });
                        break;

                    case 120: // pacchetto
                        var scianti = [];

                        product.partecipanti.forEach(function(sciante) {
                            if (sciante.id) {
                                scianti.push(sciante.id);
                            }
                        });

                        sequelize.query('SELECT scuolasci_ecommerce_preno_pacchetto_new_v1(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ',' +
                                '\'' + product.data_inizio + '\',' +
                                req.user.id + ',\'PACCHETTO - ' +
                                product.name.replace(/'/g, "''''") + '\',\'' +
                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',' +
                                product.assicurazione + ',\'' +
                                product.comprensorio.replace(/'/g, "''''") + '\',\'' +
                                (product.lingua && product.lingua != '' ? product.lingua : '') + '\',' +
                                product.id + ',ARRAY[' +
                                scianti + ']::bigint[],' +
                                product.prezzo + ',' +
                                product.persone + ',null,\'' +
                                notes + '\',\''+
                                JSON.stringify(req.body).replace(/'/g, "''''")+'\',\''+
                                JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(data) {
                                console.log('Pacchetto inserted');
                                if (data[0].scuolasci_ecommerce_preno_pacchetto_new_v1 != 0) {
                                    prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_pacchetto_new_v1));
                                } else {
                                    da_annullare = true;
                                }
                                console.log('FINE PACCHETTO: ' + cnt_prod);
                                if (cnt_prod++ >= req.body.products.length - 1) {
                                    console.log('finito PACCHETTO:' + da_annullare);
                                    console.log(prenotations);
                                    if (da_annullare) {
                                        end_checkout_ecommerce(prenotations, da_annullare, req.body.products, req);
                                        return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                    } else {
                                        sequelize.query('SELECT scuolasci_ecommerce_crea_payment(ARRAY[' +
                                                prenotations + ']::bigint[],' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.id + ',null,' +
                                                acconto_preno + ');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                if (datas[0].scuolasci_ecommerce_crea_payment) {
                                                    return res.status(200).json({
                                                        'payment_id': datas[0].scuolasci_ecommerce_crea_payment
                                                    });

                                                } else {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                }

                                            }).catch(function(err) {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            });

                                    }
                                }
                            }).catch(function(err) {
                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                //return res.status(400).render('ecommerce_collettiva: ' + err);
                            });
                        break;
                }

            }); // end foreach products
        }


    }
};

exports.ecommerce_checkout_confirm_new = function(req, res) {
    
    var moment_timezone = require('moment-timezone');
    var sovrapposizione_carrello = false;
    var carrello = req.body.data;
    var prodotti = [];
    var prenotations = [],
        da_annullare = false,
        cnt_prod = 0;

   console.log(JSON.stringify(req.body));
    var prezzo_preno = carrello.prezzo_preno,
        acconto_preno = (carrello.preno_acconto_totale ? carrello.preno_acconto_totale : carrello.prezzo_preno),
        notes = (carrello.notes ? carrello.notes.replace(/'/g, "''''") : '');
    
    if (!carrello) {
        return res.status(422).send(utility.error422('carrello', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if(carrello.voucher){
        for(var ii = 0; ii <= carrello.voucher.length -1 ; ii++){
                prodotti.push(carrello.voucher[ii]);
        }
    }
    
    if(carrello.private){
        var products = {};

        for(var ii = 0; ii <= carrello.private.length -1 ; ii++){
            
            if (carrello.private[ii].events && 
                carrello.private[ii].events.length > 0) {

                prodotti.push(carrello.private[ii]);
                if(!products[carrello.private[ii].id]){
                    products[carrello.private[ii].id] = {
                        'events':[]
                    };
                }

                for(var jj = 0; jj <= carrello.private[ii].events.length-1; jj++){
                    var evento_new = carrello.private[ii].events[jj];

                    if(evento_new.maestro_id != '0'){
                        console.log(evento_new.maestro_id);

                        for(var zz = 0; zz <= products[carrello.private[ii].id].events.length -1 ; zz++){
                            var evento_old = products[carrello.private[ii].id].events[zz];

                            if(evento_old.maestro_id == evento_new.maestro_id &&
                                (
                                   ( moment(evento_new.start_date) >= moment(evento_old.start_date) &&
                                    moment(evento_new.start_date) < moment(evento_old.end_date)) ||
                                   ( moment(evento_new.end_date) > moment(evento_old.start_date) &&
                                    moment(evento_new.end_date) <= moment(evento_old.end_date))

                                )
                            ){
                                        sovrapposizione_carrello = true;
                            }
                        }  

                        products[carrello.private[ii].id].events.push(evento_new);
                   }
                   
                }
            }
        } 
    }  
    
    if(sovrapposizione_carrello){
       
        return res.status(422).send(utility.error422('No dispo', 'Ore di lezione sovrapposte nel carrello', 'Ore di lezione sovrapposte nel carrello', '422'));
   
    }
    else
    {
        if(carrello.groups){

            for(var ii = 0; ii <= carrello.groups.length -1 ; ii++){
                prodotti.push(carrello.groups[ii]);
            }
        }

       

        prodotti.forEach(function(product) {

                 switch (parseInt(product.product_status_id.replace('"',''))) {

                    case 92: // individuali
                        async.parallel({
                            dispo: function(callback2) {
                                var cnt = 0;
                                var dispos = [];
                                var starts = [];
                                var ends = [];

                                if (product.events && product.events.length > 0) {
                                    var cnt_event = 0;
                                    var tot_ore = 0;
                                    console.log(product.events);

                                    

                                        product.events.forEach(function(event) {
                                            var start_date = moment(event.start_date);
                                            var end_date = moment(event.end_date);
                                            var comprensorio = event.comprensorio.replace(/'/g, "''''");



                                            if (!event.maestro_id || event.maestro_id == '0') {

                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {
                                                    sequelize.query('SELECT scuolasci_ecommerce_control_lezioni_new(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.role_id + ',' +
                                                            req.user.id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],\'' +
                                                            comprensorio + '\',' +
                                                            product.id + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT
                                                            })
                                                        .then(function(data) {
                                                            console.log(data[0].scuolasci_ecommerce_control_lezioni_new);
                                                            if (data[0].scuolasci_ecommerce_control_lezioni_new &&
                                                                data[0].scuolasci_ecommerce_control_lezioni_new != null) {
                                                                if (data[0].scuolasci_ecommerce_control_lezioni_new &&
                                                                    data[0].scuolasci_ecommerce_control_lezioni_new >= (tot_ore * 2)) {

                                                                    dispos.push(1);
                                                                } else {
                                                                    dispos.push(0);
                                                                }
                                                            } else {
                                                                dispos.push(0);
                                                            }

                                                            callback2(null, dispos);

                                                        });

                                                }

                                            } else {

                                                var lingua = null;
                                                switch (event.lingua) {
                                                    case 'italiano':
                                                        lingua = 123;
                                                        break;

                                                    case 'inglese':
                                                        lingua = 125;
                                                        break;

                                                    case 'francese':
                                                        lingua = 124;
                                                        break;

                                                    case 'spagnolo':
                                                        lingua = 128;
                                                        break;

                                                    case 'russo':
                                                        lingua = 127;
                                                        break;

                                                    case 'tedesco':
                                                        lingua = 126;
                                                        break;

                                                    case 'svedese':
                                                        lingua = 255;
                                                        break;

                                                    case 'portoghese':
                                                        lingua = 254;
                                                        break;
                                                }

                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {

                                                    sequelize.query('SELECT scuolasci_ecommerce_control_individuale_new(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.role_id + ',' +
                                                            req.user.id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],' +
                                                            event.maestro_id + ',\'' +
                                                            comprensorio + '\',' +
                                                            product.id + ',' +
                                                            lingua + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT
                                                            })
                                                        .then(function(data) {
                                                            if (data[0].scuolasci_ecommerce_control_individuale_new &&
                                                                data[0].scuolasci_ecommerce_control_individuale_new != null) {
                                                                if (data[0].scuolasci_ecommerce_control_individuale_new[0] &&
                                                                    data[0].scuolasci_ecommerce_control_individuale_new[0].attributes.total >= tot_ore) {

                                                                    dispos.push(1);
                                                                } else {
                                                                    dispos.push(0);
                                                                }
                                                            } else {
                                                                dispos.push(0);
                                                            }

                                                            callback2(null, dispos);

                                                        });
                                                }
                                            }
                                        });
                                    }

                            }
                        }, function(err, results) {
                            if (err) {
                                console.log(err);
                            }

                            if (results.dispo.indexOf(0) == -1) {
                                console.log('TUTTE LIBERE');

                                async.parallel({
                                    preno: function(callback1) {
                                        var cnt = 0;
                                        var prenotazioni = [];

                                        var starts = [];
                                        var ends = [];

                                        if (product.events && product.events.length > 0) {
                                            cnt_event = 0;
                                            product.events.forEach(function(event) {
                                                var start_date = moment(event.start_date);
                                                var end_date = moment(event.end_date);

                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                console.log(event);
                                                event.start_date = start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00';
                                                event.end_date = end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00';
                                                console.log(event);
                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {
                                                    /* CREO PRENOTAZIONE */
                                                    sequelize.query('SELECT scuolasci_ecommerce_confirm_individuale(' +
                                                        req.user._tenant_id + ',\'' +
                                                        req.headers['host'].split(".")[0] + '\',' +
                                                        req.user.id + ',\''+
                                                        JSON.stringify(product).replace(/'/g, "''''")+'\',\''+
                                                        JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(data) {

                                                        if (data[0].scuolasci_ecommerce_confirm_individuale && data[0].scuolasci_ecommerce_confirm_individuale!= -1) {
                                                            prenotations.push(parseInt(data[0].scuolasci_ecommerce_confirm_individuale));
                                                            //var cnt_events2 = 0;
                                                            prenotazioni.push(data[0].scuolasci_ecommerce_confirm_individuale);
                                                            
                                                            callback1(null, prenotazioni);

                                                        }else
                                                        {
                                                            da_annullare = true;
                                                            callback1(null, prenotazioni);
                                                        }

                                                    });
                                                }
                                                
                                            });

                                        } else {
                                            if (cnt++ >= products.length - 1) {

                                                callback1(null, prenotazioni);
                                            }
                                        }

                                    }
                                }, function(err, results) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    console.log('FINE INDIVIDUALI: ' + cnt_prod);
                                    if (cnt_prod++ >= prodotti.length - 1) {
                                        console.log('finito individuali:' + da_annullare);
                                        console.log(prenotations);
                                        if (da_annullare) {
                                            end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                            return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                        } else {
                                            sequelize.query('SELECT scuolasci_ecommerce_crea_payment_new(ARRAY[' +
                                                    prenotations + ']::bigint[],' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    req.user.id + ',null,' +
                                                    acconto_preno + ',\''+
                                                    JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    if (datas[0].scuolasci_ecommerce_crea_payment_new) {
                                                        return res.status(200).json({
                                                            'payment_id': datas[0].scuolasci_ecommerce_crea_payment_new
                                                        });

                                                    } else {
                                                        return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                    }

                                                }).catch(function(err) {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                });

                                        }
                                    }
                                    //callback(null, true);
                                    //res.status(200).send({'data': results.preno});
                                });
                            } else {
                                console.log('QUALCHE ORA NON DISPONIBILE');
                                da_annullare = true;
                                async.parallel({
                                    dispo1: function(callback1) {
                                        var cnt = 0;
                                        var dispos = [];

                                        var occupate = [];

                                        var starts = [];
                                        var ends = [];
                                        if (product.events && product.events.length > 0 && results.dispo.indexOf(2) == -1) {
                                            var cnt_event = 0;
                                            var tot_ore = 0;

                                            product.events.forEach(function(event) {
                                                var start_date = moment(event.start_date);
                                                var end_date = moment(event.end_date);
                                                var comprensorio = event.comprensorio.replace(/'/g, "''''");
                                                //console.log('start - end '+ start_date + ' '+end_date);
                                                var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                var hours = parseInt(timezone.split(':')[0]);

                                                //console.log('start - end '+ start_date.format('YYYY-MM-DD HH:mm:ss') + ' '+end_date.format('YYYY-MM-DD HH:mm:ss'));
                                                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                                //starts.push('\''+start_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                //ends.push('\''+end_date.add((-1*hours), 'hour').format('YYYY-MM-DD HH:mm:ss')+'+00:00\'');
                                                starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                                ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                                                //starts.push('\'' + start_date.format('YYYY-MM-DD HH:mm:ss') + '\'');
                                                //ends.push('\'' + end_date.format('YYYY-MM-DD HH:mm:ss') + '\'');

                                                if (cnt_event++ >= product.events.length - 1) {
                                                    sequelize.query('SELECT scuolasci_ecommerce_no_dispo_new(' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.role_id + ',' +
                                                            req.user.id + ',' +
                                                            event.maestro_id + ',' +
                                                            'ARRAY[' + starts + ']::character varying[],' +
                                                            'ARRAY[' + ends + ']::character varying[],' +
                                                            product.id + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(confr) {
                                                            occupate.push(confr[0].scuolasci_ecommerce_no_dispo_new);

                                                            callback1(null, occupate);

                                                        });
                                                }
                                            });


                                        } else {

                                            callback1(null, occupate);

                                        }

                                    }
                                }, function(err, results) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    console.log('FINE INDIVIDUALI NO LIBERE: ' + cnt_prod);
                                    if (cnt_prod++ >= prodotti.length - 1) {
                                        console.log('finito individuali:' + da_annullare);
                                        console.log(prenotations);
                                        if (results.dispo1.indexOf(2) != -1){
                                            end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                            return res.status(422).send(utility.error422('No dispo', 'Ore di lezione sovrapposte nel carrello', 'Ore di lezione sovrapposte nel carrello', '422'));
                                        }
                                        else if(da_annullare) {
                                            end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                            return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                        } else {
                                            sequelize.query('SELECT scuolasci_ecommerce_crea_payment_new(ARRAY[' +
                                                    prenotations + ']::bigint[],' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    req.user.id + ',null,' +
                                                    acconto_preno + ',\''+
                                                    JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    if (datas[0].scuolasci_ecommerce_crea_payment_new) {
                                                        return res.status(200).json({
                                                            'payment_id': datas[0].scuolasci_ecommerce_crea_payment_new
                                                        });

                                                    } else {
                                                        return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                    }

                                                }).catch(function(err) {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                });

                                        }
                                    }
                                    //callback(null, false);
                                    //res.status(422).send({'data': results.dispo1});
                                });


                            }

                        });
                        break;

                    case 106: // collettiva
                        var scianti = [];

                        product.partecipanti.forEach(function(sciante) {
                            if (sciante.id) {
                                scianti.push(sciante.id);
                            }
                        });

                        sequelize.query('SELECT scuolasci_ecommerce_preno_collettiva_new_v1(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ',' +
                                '\'' + product.data_inizio + '\',' +
                                req.user.id + ',\' COLLETTIVA - ' +
                                product.name + '\',\'' +
                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',' +
                                product.assicurazione + ',\'' +
                                product.comprensorio + '\',\'' +
                                (product.lingua && product.lingua != '' ? product.lingua : '') + '\',' +
                                product.id + ',ARRAY[' +
                                scianti + ']::bigint[],' +
                                product.prezzo + ',' +
                                product.persone + ',' +
                                product.giorni + ',\'' +
                                notes + '\',\''+
                                JSON.stringify(carrello).replace(/'/g, "''''")+'\',\''+
                                JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(data) {
                                console.log('Collettiva inserted ' + data[0].scuolasci_ecommerce_preno_collettiva_new_v1);
                                if (data[0].scuolasci_ecommerce_preno_collettiva_new_v1 != 0) {
                                    prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_collettiva_new_v1));
                                } else {
                                    da_annullare = true;
                                }
                                console.log('FINE COLLETTIVA: ' + cnt_prod);
                                if (cnt_prod++ >= prodotti.length - 1) {
                                    console.log('finito COLLETTIVA:' + da_annullare);
                                    console.log(prenotations);
                                    if (da_annullare) {
                                        end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                        return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                    } else {
                                        sequelize.query('SELECT scuolasci_ecommerce_crea_payment_new(ARRAY[' +
                                                prenotations + ']::bigint[],' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.id + ',null,' +
                                                acconto_preno + ',\''+
                                                JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                if (datas[0].scuolasci_ecommerce_crea_payment_new) {
                                                    return res.status(200).json({
                                                        'payment_id': datas[0].scuolasci_ecommerce_crea_payment_new
                                                    });

                                                } else {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                }

                                            }).catch(function(err) {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            });

                                    }
                                }
                            }).catch(function(err) {
                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                //return res.status(400).render('ecommerce_collettiva: ' + err);
                            });
                        break;

                    case 108: // corso annuale
                        var scianti = [];

                        product.partecipanti.forEach(function(sciante) {
                            if (sciante.id) {
                                scianti.push(sciante.id);
                            }
                        });

                        sequelize.query('SELECT scuolasci_ecommerce_preno_corso_annuale_new_v1(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ',' +
                                '\'' + product.data_inizio + '\',' +
                                req.user.id + ',\'ANNUALE - ' +
                                product.name.replace(/'/g, "''''") + '\',\'' +
                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',' +
                                product.assicurazione + ',\'' +
                                product.comprensorio.replace(/'/g, "''''") + '\',\'' +
                                (product.lingua && product.lingua != '' ? product.lingua : '') + '\',' +
                                product.id + ',ARRAY[' +
                                scianti + ']::bigint[],' +
                                product.prezzo + ',' +
                                product.persone + ',\'' +
                                notes + '\',\''+
                                JSON.stringify(carrello).replace(/'/g, "''''")+'\',\''+
                                JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(data) {
                                console.log('Corso Annuale inserted');
                                if (data[0].scuolasci_ecommerce_preno_corso_annuale_new_v1 != 0) {
                                    prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_corso_annuale_new_v1));
                                } else {
                                    da_annullare = true;
                                }
                                console.log('FINE CORSO: ' + cnt_prod);
                                if (cnt_prod++ >= prodotti.length - 1) {
                                    console.log('finito CORSO ANNUALE:' + da_annullare);
                                    console.log(prenotations);
                                    if (da_annullare) {
                                        end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                        return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                    } else {
                                        sequelize.query('SELECT scuolasci_ecommerce_crea_payment_new(ARRAY[' +
                                                prenotations + ']::bigint[],' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.id + ',null,' +
                                                acconto_preno + ',\''+
                                                JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                if (datas[0].scuolasci_ecommerce_crea_payment_new) {
                                                    return res.status(200).json({
                                                        'payment_id': datas[0].scuolasci_ecommerce_crea_payment_new
                                                    });

                                                } else {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                }

                                            }).catch(function(err) {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            });

                                    }
                                }
                            }).catch(function(err) {
                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                //return res.status(400).render('ecommerce_collettiva: ' + err);
                            });
                        break;

                    case 120: // pacchetto
                        var scianti = [];

                        product.partecipanti.forEach(function(sciante) {
                            if (sciante.id) {
                                scianti.push(sciante.id);
                            }
                        });

                        sequelize.query('SELECT scuolasci_ecommerce_preno_pacchetto_new_v1(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ',' +
                                '\'' + product.data_inizio + '\',' +
                                req.user.id + ',\'PACCHETTO - ' +
                                product.name.replace(/'/g, "''''") + '\',\'' +
                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',' +
                                product.assicurazione + ',\'' +
                                product.comprensorio.replace(/'/g, "''''") + '\',\'' +
                                (product.lingua && product.lingua != '' ? product.lingua : '') + '\',' +
                                product.id + ',ARRAY[' +
                                scianti + ']::bigint[],' +
                                product.prezzo + ',' +
                                product.persone + ',null,\'' +
                                notes + '\',\''+
                                JSON.stringify(carrello).replace(/'/g, "''''")+'\',\''+
                                JSON.stringify(product).replace(/'/g, "''''")+'\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(data) {
                                console.log('Pacchetto inserted');
                                if (data[0].scuolasci_ecommerce_preno_pacchetto_new_v1 != 0) {
                                    prenotations.push(parseInt(data[0].scuolasci_ecommerce_preno_pacchetto_new_v1));
                                } else {
                                    da_annullare = true;
                                }
                                console.log('FINE PACCHETTO: ' + cnt_prod);
                                if (cnt_prod++ >= prodotti.length - 1) {
                                    console.log('finito PACCHETTO:' + da_annullare);
                                    console.log(prenotations);
                                    if (da_annullare) {
                                        end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                        return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                    } else {


                                        sequelize.query('SELECT scuolasci_ecommerce_crea_payment_new(ARRAY[' +
                                                prenotations + ']::bigint[],' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.id + ',null,' +
                                                acconto_preno + ',\''+
                                                JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                if (datas[0].scuolasci_ecommerce_crea_payment_new) {
                                                    return res.status(200).json({
                                                        'payment_id': datas[0].scuolasci_ecommerce_crea_payment_new
                                                    });

                                                } else {
                                                    return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                                }

                                            }).catch(function(err) {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            });

                                    }
                                }
                            }).catch(function(err) {
                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                //return res.status(400).render('ecommerce_collettiva: ' + err);
                            });
                        break;

                    case 0: // voucher
                       
                            console.log('FINE VOUCHER: ' + cnt_prod);
                            if (cnt_prod++ >= prodotti.length - 1) {
                                console.log('finito VOUCHER:' + da_annullare);
                                console.log(prenotations);
                                if (da_annullare) {
                                    end_checkout_ecommerce(prenotations, da_annullare, prodotti, req);
                                    return res.status(422).send(utility.error422('No dispo', 'Alcuni dei prodotti selezionati non sono più disponibili', 'Alcuni dei prodotti selezionati non sono più disponibili', '422'));
                                } else {


                                    sequelize.query('SELECT scuolasci_ecommerce_crea_payment_new(ARRAY[' +
                                            prenotations + ']::bigint[],' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.user.id + ',null,' +
                                            acconto_preno + ',\''+
                                            JSON.stringify(carrello).replace(/'/g, "''''")+'\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            if (datas[0].scuolasci_ecommerce_crea_payment_new) {
                                                return res.status(200).json({
                                                    'payment_id': datas[0].scuolasci_ecommerce_crea_payment_new
                                                });

                                            } else {
                                                return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                            }

                                        }).catch(function(err) {
                                            return res.status(400).render('scuolasci_ecommerce_crea_payment: ' + err);
                                        });

                                }
                            }
                           
                        break;
                }

            }); // end foreach products
    }
};

exports.individuali = function(req, res) {

    var moment_timezone = require('moment-timezone');
    if (!req.body.maestri || !req.body.id_prenotante) {
        return res.status(422).send(utility.error422('maestri', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    var assicurazione = req.body.assicurazione,
        importo = req.body.totale,
        contact_id = req.body.id_prenotante,
        maestri = req.body.maestri;

    if (maestri && maestri.length > 0) {
        async.parallel({
            dispo: function(callback) {
                var cnt = 0;
                var dispos = [];

                maestri.forEach(function(maestro) {
                    var starts = [];
                    var ends = [];

                    if (maestro.events && maestro.events.length > 0) {
                        var cnt_event = 0;
                        var tot_ore = 0;

                        maestro.events.forEach(function(event) {
                            var start_date = moment(event.data + ' ' + event.start);
                            var end_date = moment(event.data + ' ' + event.end);
                            var comprensorio = event.luogo.replace(/'/g, "''''");

                            var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                            var hours = parseInt(timezone.split(':')[0]);

                            console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                            tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                            starts.push('\'' + start_date.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                            ends.push('\'' + end_date.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                            if (cnt_event++ >= maestro.events.length - 1) {
                                sequelize.query('SELECT scuolasci_ecommerce_control_individuale(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        req.user.role_id + ',' +
                                        req.user.id + ',' +
                                        'ARRAY[' + starts + ']::character varying[],' +
                                        'ARRAY[' + ends + ']::character varying[],' +
                                        maestro.id_maestro + ',\'' +
                                        comprensorio + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT
                                        })
                                    .then(function(data) {
                                        if (data[0].scuolasci_ecommerce_control_individuale &&
                                            data[0].scuolasci_ecommerce_control_individuale != null) {
                                            if (data[0].scuolasci_ecommerce_control_individuale[0] &&
                                                data[0].scuolasci_ecommerce_control_individuale[0].attributes.total >= tot_ore) {

                                                dispos.push(1);
                                            } else {
                                                dispos.push(0);
                                            }
                                        } else {
                                            dispos.push(0);
                                        }
                                        if (cnt++ >= maestri.length - 1) {
                                            callback(null, dispos);
                                        }
                                    });
                            }
                        });


                    } else {
                        callback(null, null);
                    }

                });
            }
        }, function(err, results) {
            if (err) {
                console.log(err);
            }

            if (results.dispo.indexOf(0) == -1) {
                console.log('TUTTE LIBERE');

                async.parallel({
                    preno: function(callback) {
                        var cnt = 0;
                        var prenotazioni = [];
                        maestri.forEach(function(maestro) {

                            var starts = [];
                            var ends = [];

                            if (maestro.events && maestro.events.length > 0) {
                                var cnt_event = 0;
                                var tot_ore = 0;
                                var prezzo = 0.00;
                                var prezzi = [];
                                var scianti = [];

                                maestro.events.forEach(function(event) {
                                    var start_date = moment(event.data + ' ' + event.start);
                                    var end_date = moment(event.data + ' ' + event.end);
                                    var comprensorio = event.luogo.replace(/'/g, "''''");
                                    var lingua = event.lingua;
                                    var spec = event.specialità;
                                    var product_id = null;
                                    var product_name = '';

                                    switch (event.specialità) {
                                        case 'discesa':
                                            product_id = 2072;
                                            product_name = 'Sci Alpino';
                                            break;

                                        case 'fondo':
                                            product_id = 2489;
                                            product_name = 'Sci Nordico';
                                            break;

                                        case 'snowboard':
                                            product_id = 2454;
                                            product_name = 'Snowboard';
                                            break;

                                        case 'telemark':
                                            product_id = 2071;
                                            product_name = 'Telemark';
                                            break;

                                        case 'pali':
                                            product_id = 2456;
                                            product_name = 'Pali';
                                            break;

                                        case 'fuori pista':
                                            product_id = 2519;
                                            product_name = 'Fuori Pista';
                                            break;

                                    }

                                    if (comprensorio == 'dolonne') {
                                        product_id = 2520;
                                    }


                                    var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                    var hours = parseInt(timezone.split(':')[0]);

                                    console.log('start - end ' + start_date.format('YYYY-MM-DD HH:mm:ss') + ' ' + end_date.format('YYYY-MM-DD HH:mm:ss'));
                                    tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                    starts.push('\'' + start_date.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                    ends.push('\'' + end_date.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                    //console.log('start - end '+ start_date + ' '+end_date);
                                    //tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                    //starts.push('\''+moment.utc(start_date).format().toString()+'\'');
                                    //ends.push('\''+moment.utc(end_date).format().toString()+'\'');
                                    prezzo += parseFloat((req.body.assicurazione.toString() == 'true') ? (parseFloat(event.totale * 1.05)) : parseFloat(event.totale));
                                    prezzi.push((req.body.assicurazione.toString() == 'true') ? (event.totale * 1.05) : event.totale);
                                    scianti.push(event.scianti.length);


                                    if (cnt_event++ >= maestro.events.length - 1) {

                                        sequelize.query('SELECT scuolasci_ecommerce_preno_individuale(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.body.id_prenotante + ',' +
                                                'ARRAY[' + starts + ']::character varying[],' +
                                                'ARRAY[' + ends + ']::character varying[],' +
                                                req.user.id + ',\'INDIVIDUALE ' +
                                                product_name + '\',\'' +
                                                moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\',false,' +
                                                req.body.assicurazione + ',' +
                                                maestro.id_maestro + ',\'' +
                                                comprensorio + '\',\'' +
                                                spec + '\',\'' +
                                                lingua + '\',' +
                                                product_id + ',ARRAY[' +
                                                scianti + ']::bigint[],' +
                                                prezzo + ',ARRAY[' +
                                                prezzi + ']::double precision[]);', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                })
                                            .then(function(data) {

                                                if (data[0].scuolasci_ecommerce_preno_individuale) {
                                                    var cnt_events2 = 0;
                                                    prenotazioni.push(data[0].scuolasci_ecommerce_preno_individuale);
                                                    maestro.events.forEach(function(event_new) {
                                                        var start_date_new = moment(event_new.data + ' ' + event_new.start);
                                                        var end_date_new = moment(event_new.data + ' ' + event_new.end);

                                                        var timezone = moment_timezone.tz(moment(start_date_new, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                                        var hours = parseInt(timezone.split(':')[0]);

                                                        sequelize.query('SELECT scuolasci_ecommerce_event_scianti(' +
                                                                req.user._tenant_id + ',\'' +
                                                                req.headers['host'].split(".")[0] + '\',' +
                                                                req.body.id_prenotante + ',' +
                                                                req.user.id + ',\'' +
                                                                start_date_new.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\',\'' +
                                                                end_date_new.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\',' +
                                                                data[0].scuolasci_ecommerce_preno_individuale + ',ARRAY[' +
                                                                event_new.scianti + ']::bigint[]);', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                                })
                                                            .then(function(data_event) {

                                                                if (cnt_events2++ >= maestro.events.length - 1) {
                                                                    if (cnt++ >= maestri.length - 1) {

                                                                        callback(null, prenotazioni);
                                                                    }
                                                                }
                                                            });
                                                    });

                                                }

                                            });
                                    }

                                });

                            } else {
                                if (cnt++ >= maestri.length - 1) {

                                    callback(null, prenotazioni);
                                }
                            }

                        });
                    }
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                    }

                    res.status(200).send({ 'data': results.preno });
                });
            } else {
                console.log('QUALCHE ORA NON DISPONIBILE');
                async.parallel({
                    dispo1: function(callback) {
                        var cnt = 0;
                        var dispos = [];

                        var occupate = [];
                        maestri.forEach(function(maestro) {
                            var starts = [];
                            var ends = [];
                            if (maestro.events && maestro.events.length > 0) {
                                var cnt_event = 0;
                                var tot_ore = 0;

                                maestro.events.forEach(function(event) {
                                    var start_date = moment(event.data + ' ' + event.start);
                                    var end_date = moment(event.data + ' ' + event.end);
                                    var comprensorio = event.luogo.replace(/'/g, "''''");
                                    //console.log('start - end '+ start_date + ' '+end_date);
                                    var timezone = moment_timezone.tz(moment(start_date, 'DD-MM-YYYY'), "Europe/Rome").format('Z');
                                    var hours = parseInt(timezone.split(':')[0]);

                                    //console.log('start - end '+ start_date.format('YYYY-MM-DD HH:mm:ss') + ' '+end_date.format('YYYY-MM-DD HH:mm:ss'));
                                    tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                                    starts.push('\'' + start_date.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');
                                    ends.push('\'' + end_date.add((-1 * hours), 'hour').format('YYYY-MM-DD HH:mm:ss') + '+00:00\'');

                                    if (cnt_event++ >= maestro.events.length - 1) {
                                        sequelize.query('SELECT scuolasci_ecommerce_no_dispo_new(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',' +
                                                req.user.role_id + ',' +
                                                req.user.id + ',' +
                                                maestro.id_maestro + ',' +
                                                'ARRAY[' + starts + ']::character varying[],' +
                                                'ARRAY[' + ends + ']::character varying[]);', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                })
                                            .then(function(confr) {
                                                occupate.push(confr[0].scuolasci_ecommerce_no_dispo_new);
                                                if (cnt++ >= maestri.length - 1) {

                                                    callback(null, occupate);
                                                }
                                            });
                                    }
                                });


                            } else {
                                if (cnt++ >= maestri.length - 1) {

                                    callback(null, occupate);
                                }
                            }

                        });
                    }
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                    }


                    res.status(422).send({ 'data': results.dispo1 });
                });


            }

        });

    } else {
        return res.status(422).send(utility.error422('maestri', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.ecommerce_cron_delete = function(req, res) {

    

    sequelize.query('SELECT scuolasci_ecommerce_cron_destroy_cron();', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
    });

};


exports.sistema_prenotazioni_pendenti = function(req, res) {
    sequelize.query('SELECT scuolasci_sistema_prenotazioni_pendenti();', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        });
};

exports.sanitize_dispo_verify = function(req, res) {

    sequelize.query('SELECT scuolasci_ecommerce_sanitize_dispo_verify(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.params.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        });
};

exports.ecommerce_products = function(req, res) {
    
    var dispo = null,
        spec = null,
        anni = null,
        stagione = null,
        livello = null,
        comp = null,
        user_id =  null;

    if (req.query.dispo !== undefined && req.query.dispo !== '') {
        dispo = req.query.dispo;
    }

    if (req.query.stagione !== undefined && req.query.stagione !== '') {
        stagione = '\''+req.query.stagione+'\'';
    }

    if (req.query.livello !== undefined && req.query.livello !== '') {
        livello = '\''+req.query.livello+'\'';
    }


    if (req.query.comp !== undefined && req.query.comp !== '') {
        comp = '\''+req.query.comp.replace(/'/g, "''''")+'\'';
    }

    if (req.query.spec !== undefined && req.query.spec !== '') {
        switch (req.query.spec) {
            case 'discesa':
                spec = '\'Sci\'';
                break;

            case 'snowboard':
                spec = '\'Snowboard\'';
                break;

            case 'telemark':
                spec = '\'Telemark\'';
                break;

            case 'fondo':
                spec = '\'Fondo\'';
                break;

            default:
                 spec = '\''+req.query.spec+'\'';
            break;
        }

    }

    if (req.query.anni !== undefined && req.query.anni !== '') {
        anni = req.query.anni;
    }

    if (req.query.levels !== undefined && req.query.levels != '') {
            if (util.isArray(req.query.levels)) {
                req.query.levels.forEach(function(start) {
                    levels.push('\'' + start + '\'');
                });
            }
        }



     utility.get_parameters(req, 'product', function(err, parameters) {
        if (err) {
            return res.status(400).render('ecommerce_product_online_get_parameters: ' + err);
        }


        if(req.query.access_token && req.query.access_token !== undefined){
            sequelize.query('SELECT find_token(\'' +
                req.query.access_token + '\', \'' + 
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                }).then(function(datas) {
                    if(datas && datas[0] && datas[0].find_token && datas[0].find_token[0])
                    {

                        user_id = datas[0].find_token[0].user_id;

                        async.parallel({
                            data: function(callback) {
                                var response = {};
                                sequelize.query('SELECT scuole_product_dispo_online_data_v3(' +
                                        parameters.page_count + ',' +
                                        (parameters.page_num - 1) * parameters.page_count + ',' +
                                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                        parameters.id + ',' +
                                        (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                        parameters.sortDirection + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        parameters.start_date + ',' +
                                        parameters.end_date + ',' +
                                        dispo + ',' +
                                        spec + ',' +
                                        anni + ','+
                                        stagione+','+
                                        comp+','+
                                        user_id+');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0] && datas[0].scuole_product_dispo_online_data_v3) {
                                            response = {
                                                'data': datas[0].scuole_product_dispo_online_data_v3
                                            };
                                            callback(null, response);
                                        } else {
                                            callback(null, []);
                                        }
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            },
                            included: function(callback) {
                                var response = {};
                                sequelize.query('SELECT scuole_product_dispo_online_included_v3(' +
                                        parameters.page_count + ',' +
                                        (parameters.page_num - 1) * parameters.page_count + ',' +
                                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                        parameters.id + ',' +
                                        (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                        parameters.sortDirection + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        parameters.start_date + ',' +
                                        parameters.end_date + ',' +
                                        dispo + ',' +
                                        spec + ',' +
                                        anni + ','+
                                        stagione+','+
                                        comp+','+
                                        user_id+');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT
                                        })
                                    .then(function(included) {
                                        if (included && included[0] && included[0].scuole_product_dispo_online_included_v3 && included[0].scuole_product_dispo_online_included_v3 != null) {
                                            callback(null, included[0].scuole_product_dispo_online_included_v3);

                                        } else {

                                            callback(null, []);
                                        }
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }
                        }, function(err, result) {
                            if (err) {
                                return res.status(400).render('ecommerce_product_online: ' + err);
                            }

                            if (result.data && (result.data.length == 0 || !result.included)) {
                                return res.status(200).send({ 'data': [] });
                            } else {
                                var response = {
                                    'data': utility.check_find_datas(result.data.data, 'products', ['custom_fields', 'files', 'events']),
                                    'included': utility.check_find_included(result.included, 'products', ['custom_fields', 'files', 'events', 'product_states', 'organization_custom_fields','tags'])

                                };

                                res.status(200).send(response);
                            }


                        });
                    }
                });
        }else
        {
            async.parallel({
                data: function(callback) {
                    var response = {};
                    sequelize.query('SELECT scuole_product_dispo_online_data_v3(' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                            parameters.id + ',' +
                            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                            parameters.sortDirection + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            dispo + ',' +
                            spec + ',' +
                            anni + ','+
                            stagione+','+
                            comp+','+
                            user_id+');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(datas) {
                            if (datas && datas[0] && datas[0].scuole_product_dispo_online_data_v3) {
                                response = {
                                    'data': datas[0].scuole_product_dispo_online_data_v3
                                };
                                callback(null, response);
                            } else {
                                callback(null, []);
                            }
                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                },
                included: function(callback) {
                    var response = {};
                    sequelize.query('SELECT scuole_product_dispo_online_included_v3(' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                            parameters.id + ',' +
                            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                            parameters.sortDirection + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            dispo + ',' +
                            spec + ',' +
                            anni + ','+
                            stagione+','+
                            comp+','+
                            user_id+');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(included) {
                            if (included && included[0] && included[0].scuole_product_dispo_online_included_v3 && included[0].scuole_product_dispo_online_included_v3 != null) {
                                callback(null, included[0].scuole_product_dispo_online_included_v3);

                            } else {

                                callback(null, []);
                            }
                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                }
            }, function(err, result) {
                if (err) {
                    return res.status(400).render('ecommerce_product_online: ' + err);
                }

                if (result.data && (result.data.length == 0 || !result.included)) {
                    return res.status(200).send({ 'data': [] });
                } else {
                    var response = {
                        'data': utility.check_find_datas(result.data.data, 'products', ['custom_fields', 'files', 'events']),
                        'included': utility.check_find_included(result.included, 'products', ['custom_fields', 'files', 'events', 'product_states', 'organization_custom_fields','tags'])

                    };

                    res.status(200).send(response);
                }


            });
        }


        

        //}
    });
};

exports.ecommerce_scuolesci_dashboard = function(req, res) {
    
    var dispo = null,
        spec = null,
        anni = null,
        stagione = null,
        livello = null,
        comp = null,
        user_id =  null;

    if (req.query.dispo !== undefined && req.query.dispo !== '') {
        dispo = req.query.dispo;
    }

    if (req.query.stagione !== undefined && req.query.stagione !== '') {
        stagione = '\''+req.query.stagione+'\'';
    }

    if (req.query.livello !== undefined && req.query.livello !== '') {
        livello = '\''+req.query.livello+'\'';
    }


    if (req.query.comp !== undefined && req.query.comp !== '') {
        comp = '\''+req.query.comp.replace(/'/g, "''''")+'\'';
    }

    if (req.query.spec !== undefined && req.query.spec !== '') {
        switch (req.query.spec) {
            case 'discesa':
                spec = '\'Sci\'';
                break;

            case 'snowboard':
                spec = '\'Snowboard\'';
                break;

            case 'telemark':
                spec = '\'Telemark\'';
                break;

            case 'fondo':
                spec = '\'Fondo\'';
                break;

            default:
                 spec = '\''+req.query.spec+'\'';
            break;
        }

    }

    if (req.query.anni !== undefined && req.query.anni !== '') {
        anni = req.query.anni;
    }


     utility.get_parameters(req, 'product', function(err, parameters) {
        if (err) {
            return res.status(400).render('ecommerce_product_online_get_parameters: ' + err);
        }


        if(req.query.access_token && req.query.access_token !== undefined){
            sequelize.query('SELECT find_token(\'' +
                req.query.access_token + '\', \'' + 
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                }).then(function(datas) {
                    if(datas && datas[0] && datas[0].find_token && datas[0].find_token[0])
                    {

                        user_id = datas[0].find_token[0].user_id;

                        async.parallel({
                            data: function(callback) {
                                var response = {};
                                sequelize.query('SELECT scuole_product_dispo_online_data_new(' +
                                        parameters.page_count + ',' +
                                        (parameters.page_num - 1) * parameters.page_count + ',' +
                                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                        parameters.id + ',' +
                                        (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                        parameters.sortDirection + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        parameters.start_date + ',' +
                                        parameters.end_date + ',' +
                                        dispo + ',' +
                                        spec + ',' +
                                        anni + ','+
                                        stagione+','+
                                        comp+','+
                                        user_id+','+
                                        (livello ? 'ARRAY[' + livello + ']' : null) + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0] && datas[0].scuole_product_dispo_online_data_new) {
                                            response = {
                                                'data': datas[0].scuole_product_dispo_online_data_new
                                            };
                                            callback(null, response);
                                        } else {
                                            callback(null, []);
                                        }
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }/*,
                            included: function(callback) {
                                var response = {};
                                sequelize.query('SELECT scuole_product_dispo_online_included_new(' +
                                        parameters.page_count + ',' +
                                        (parameters.page_num - 1) * parameters.page_count + ',' +
                                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                                        parameters.id + ',' +
                                        (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                                        parameters.sortDirection + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        parameters.start_date + ',' +
                                        parameters.end_date + ',' +
                                        dispo + ',' +
                                        spec + ',' +
                                        anni + ','+
                                        stagione+','+
                                        comp+','+
                                        user_id+');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT
                                        })
                                    .then(function(included) {
                                        if (included && included[0] && included[0].scuole_product_dispo_online_included_new && included[0].scuole_product_dispo_online_included_new != null) {
                                            callback(null, included[0].scuole_product_dispo_online_included_new);

                                        } else {

                                            callback(null, []);
                                        }
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                            }*/
                        }, function(err, result) {
                            if (err) {
                                return res.status(400).render('ecommerce_product_online: ' + err);
                            }

                            if (result.data && (result.data.length == 0 )) {
                                return res.status(200).send({ 'data': [] });
                            } else {
                                var response = {
                                    'data': result.data.data[0]
                                    //'included': utility.check_find_included(result.included, 'products', ['custom_fields', 'files', 'events', 'product_states', 'organization_custom_fields','tags'])

                                };

                                res.status(200).send(response);
                            }


                        });
                    }
                });
        }else
        {
            async.parallel({
                data: function(callback) {
                    var response = {};
                    sequelize.query('SELECT scuole_product_dispo_online_data_new(' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                            parameters.id + ',' +
                            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                            parameters.sortDirection + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            dispo + ',' +
                            spec + ',' +
                            anni + ','+
                            stagione+','+
                            comp+','+
                            user_id+','+
                            (livello ? 'ARRAY[' + livello + ']' : null) + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(datas) {
                            if (datas && datas[0] && datas[0].scuole_product_dispo_online_data_new) {
                                response = {
                                    'data': datas[0].scuole_product_dispo_online_data_new
                                };
                                callback(null, response);
                            } else {
                                callback(null, []);
                            }
                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                }/*,
                included: function(callback) {
                    var response = {};
                    sequelize.query('SELECT scuole_product_dispo_online_included_new(' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                            parameters.id + ',' +
                            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ',' +
                            parameters.sortDirection + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            dispo + ',' +
                            spec + ',' +
                            anni + ','+
                            stagione+','+
                            comp+','+
                            user_id+');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(included) {
                            if (included && included[0] && included[0].scuole_product_dispo_online_included_new && included[0].scuole_product_dispo_online_included_new != null) {
                                callback(null, included[0].scuole_product_dispo_online_included_new);

                            } else {

                                callback(null, []);
                            }
                        }).catch(function(err) {
                            console.log(err);
                            callback(err, null);
                        });
                }*/
            }, function(err, result) {
                if (err) {
                    return res.status(400).render('ecommerce_product_online: ' + err);
                }

                if (result.data && (result.data.length == 0 )) {
                    return res.status(200).send({ 'data': [] });
                } else {
                    var response = {
                        'data': result.data.data[0]
                        //'included': utility.check_find_included(result.included, 'products', ['custom_fields', 'files', 'events', 'product_states', 'organization_custom_fields','tags'])

                    };

                    res.status(200).send(response);
                }


            });
        }


        

        //}
    });
};

exports.ecommerce_dispo_pacchetti = function(req, res) {

    var dispo = false;

    utility.get_parameters(req, 'event', function(err, parameters) {
        if (err) {
            return res.status(400).render('ecommerce_dispo_pacchetti_get_parameters: ' + err);
        }


        sequelize.query('SELECT scuole_pacchetto_dispo_online_data(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.query.product_id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuole_pacchetto_dispo_online_data) {
                    response = {
                        'data': datas[0].scuole_pacchetto_dispo_online_data
                    };
                    res.status(200).json(response);
                } else {
                    res.status(200).json({});
                }
            }).catch(function(err) {
                return res.status(500).json(err);

            });

    });
};

exports.list_dispo_ecommerce = function(req, res) {

    utility.get_parameters(req, 'event', function(err, parameters) {
        if (err) {
            return res.status(400).render('list_dispo_ecommerce: ' + err);
        }

        var util = require('util');
        
        var days = [],
            comp = null,
            contact_ids = [],
            levels = [];

        console.log(req.body);

        if (req.query.days !== undefined && req.query.days != '' && req.query.days !== 'undefined') {
            if (util.isArray(req.query.days)) {
                days = req.query.days.map(Number);
            } else {
                days = parseInt(req.query.days);
            }
        }

        if (req.query.comp !== undefined && req.query.comp !== '') {
            comp = '\'' + req.query.comp.replace(/'/g, "''") + '\'';
        }

        if (req.query.contact_ids !== undefined && req.query.contact_ids != '' && req.query.contact_ids !== 'undefined') {
            if (util.isArray(req.query.contact_ids)) {
                contact_ids = req.query.contact_ids.map(Number);
            } else {
                contact_ids = parseInt(req.query.contact_ids);
            }
        }

        if (req.query.levels !== undefined && req.query.levels != '') {
            if (util.isArray(req.query.levels)) {
                req.query.levels.forEach(function(start) {
                    levels.push('\'' + start + '\'');
                });
            }
        }


        sequelize.query('SELECT scuolesci_list_dispo_ecommerce(\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.query.product_id + ',' +
                parameters.start_date + ',' +
                parameters.end_date + ','+
                (days && days.length > 0 ? 'ARRAY[' + days + ']' : null) + ',' +
                comp + ','+
                (levels && levels.length > 0 ? 'ARRAY[' + levels + ']' : null) + ',' +
                (contact_ids && contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']' : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuolesci_list_dispo_ecommerce) {
                    
                    res.status(200).json(datas[0].scuolesci_list_dispo_ecommerce[0].events);
                } else {
                    res.status(200).json({});
                }
            }).catch(function(err) {
                return res.status(500).json(err);

            });

    });
};


exports.ecommerce_turno_maestro = function(req, res) {

   
    var start_array = [],
        end_array = [],
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua = [],
        lingua_char = [],
        spec_char = [],
        product_id = null;

    let json_ = (req.body);

    if (json_.product && json_.product.events) {


        
        json_.product.events.forEach(function(timeslot) {
            start_array.push('\'' + timeslot.start_date.toString() + '\'');
            end_array.push('\'' + timeslot.end_date.toString() + '\'');
        });

        if (json_.product.gender !== undefined && json_.product.gender !== '') {
            if (json_.product.gender == 'M') {
                sesso = true;
            } else {
                sesso = false;
            }
        }

        if (json_.product.lingua !== undefined && json_.product.lingua !== '') {
            
            if (!utility.check_type_variable(json_.product.lingua, 'string')) {
                return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
            }
            

            var lang_array = json_.product.lingua.split(',');
            lang_array.forEach(function(s) {
                lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        
        }


        if (json_.product.id !== undefined && json_.product.id !== '') {
            product_id = json_.product.id;
        }

        sequelize.query('SELECT scuolesci_individuali_ecommerce_new(ARRAY[' +
                start_array + ']::character varying[],'+
                sesso + ',' +
                (json_.product.comprensorio ? '\'' +json_.product.comprensorio.replace(/'/g, "''''") + '\'' : null)+',' +
                (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
                (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',50,0,\'' +
                req.headers['host'].split(".")[0] + '\',' +
                product_id+',' +
                (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',ARRAY[' + end_array + ']::character varying[]);', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuolesci_individuali_ecommerce_new && datas[0].scuolesci_individuali_ecommerce_new != null) {
                    res.status(200).send({
                        'data': datas[0].scuolesci_individuali_ecommerce_new[0]
                    });

                } else {
                    res.status(404).send({});
                }
            }).catch(function(err) {
                return res.status(400).render('update turno: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

exports.change_dispo_ecommerce = function(req, res) {
    var util = require('util');
    var days = [],
        dispo = 0,
        product_id = 0,
        q = '\'\'',
        contact_ids = [],
        levels = [];

    console.log(req.body);

    if (req.body.days !== undefined && req.body.days != '') {
        if (util.isArray(req.body.days)) {
            req.body.days.forEach(function(start) {
                days.push('\'' + start + '\'');
            });
        }
    }else
    {
        if (req.body.day !== undefined && req.body.day != '') {
            
            days.push('\'' + req.body.day  + '\'');

        }
    }

    if (req.body.dispo !== undefined && req.body.dispo !== '') {
        dispo = req.body.dispo;
    }

    if (req.body.product_id !== undefined && req.body.product_id != '' && req.body.product_id != 'undefined') {
        product_id = req.body.product_id;
    }

    if (req.body.q !== undefined && req.body.q !== '') {

        q = '\'' + req.body.q.replace(/'/g, "''") + '\'';

    }

    if (req.body.contact_ids !== undefined && req.body.contact_ids != '' && req.body.contact_ids !== 'undefined') {
        if (util.isArray(req.body.contact_ids)) {
            contact_ids = req.body.contact_ids.map(Number);
        } else {
            contact_ids = parseInt(req.body.contact_ids);
        }
    }

    if (req.body.levels !== undefined && req.body.levels != '') {
        if (util.isArray(req.body.levels)) {
            req.body.levels.forEach(function(start) {
                levels.push('\'' + start + '\'');
            });
        }
    }

    sequelize.query('SELECT scuole_change_dispo_ecommerce_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            (days && days.length > 0 ? 'ARRAY[' + days + ']' : null) + ',' +
            dispo + ',' +
            product_id + ',' +
            q + ','+
            (levels && levels.length > 0 ? 'ARRAY[' + levels + ']' : null) + ',' +
            (contact_ids && contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

function end_checkout_ecommerce(preno_ids, da_cancellare, prodotti, req) {
    var cnt_prod = 0;


    if (da_cancellare) { // 
        preno_ids.forEach(function(preno) {

            sequelize.query('SELECT scuolasci_elimina_prenotazione_v5(' +
                    preno + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.id + ',null,null,null);', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                    if (cnt_prod++ >= preno_ids.length - 1) {

                        return 0;
                    }

                }).catch(function(err) {

                    return 0;
                });

        });

    }
};



function testo_personalizzato_bonifico(org){
    var testo = '';

    if(org == 'scuolascitorgnon'){
        testo += '<br/><b style="color:red;">LA PRENOTAZIONE SARA\' ANNULLATA SE NON VERRA\' INVIATA LA RICEVUTA DI PAGAMENTO ENTRO 30 MINUTI AL SEGUENTE INDIRIZZO MAIL: scuolascitorgnon@hotmail.com</b><br/>';
    }

    return testo ;
};


function testo_personalizzato_bonifico_eng(org){
    var testo = '';

    if(org == 'scuolascitorgnon'){
        testo += '<br/><b style="color:red;">THE BOOKING WILL BE CANCELED IF THE PAYMENT RECEIPT IS NOT SENT WITHIN 30 MINUTES TO THE FOLLOWING EMAIL ADDRESS: scuolascitorgnon@hotmail.com</b><br/>';
    }

    return testo ;
};

function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};