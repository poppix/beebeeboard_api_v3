var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
        utility = require('../../utility/utility.js');
/*
exports.payment_event_people = function(req,res){
  var Sequelize = require('sequelize'),
      events = req.body.events,
      payment_id = req.body.id;

  var cnt = 0;
  events.forEach(function(event){
    sequelize.query('SELECT scuolasci_event_payment_people('+
      req.user._tenant_id+',\''+
      req.headers['host'].split(".")[0]+'\','+
      event.id+','+
      payment_id+','+
      event.paganti+');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
      })
      .then(function(datas) {
        if(cnt++ >= events.length-1){
           res.status(200).send({});
        }
         
      }).catch(function(err) {
        return res.status(400).render('payment_event_people: ' + err);
      });
        
  }); 
};*/



exports.payment_project_ecommmerce = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var Sequelize = require('sequelize'),
        payment_id = req.body.payment_id,
        project_id = req.body.project_id;

    if (project_id !== null) {
        sequelize.query('SELECT scuolasci_payment_projects_sanitize_ecommerce(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                payment_id + ',' +
                project_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                res.json({});
            }).catch(function(err) {
                return res.status(400).render('payment_project_ecommmerce: ' + err);
            });
    } else {
        sequelize.query('SELECT scuolasci_sanitize_residuo_payment_v3(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                payment_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                res.status(200).send({});
            }).catch(function(err) {
                return res.status(400).render('Sanitize residuo payment: ' + err);
            });
    }
};

exports.payment_project = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var Sequelize = require('sequelize'),
        payment_id = req.body.payement_id,
        project_id = req.body.project_id;

    if(payment_id){
        if (project_id !== null && req.body.project_id && req.body.project_id !== undefined) {
            sequelize.query('SELECT scuolasci_payment_projects_sanitize_v2(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    payment_id + ',' +
                    project_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                    })
                .then(function(datas) {
                    sequelize.query('SELECT scuolasci_sanitize_residuo_payment_v3(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            payment_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                            })
                        .then(function(datas) {
                            res.status(200).send({});
                        }).catch(function(err) {
                            return res.status(400).render('Sanitize residuo payment: ' + err);
                        });

                }).catch(function(err) {
                    return res.status(400).render('scuolasci_payment_projects: ' + err);
                });
        } else {
            sequelize.query('SELECT scuolasci_sanitize_residuo_payment_v3(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    payment_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                    })
                .then(function(datas) {
                    res.status(200).send({});
                }).catch(function(err) {
                    return res.status(400).render('Sanitize residuo payment: ' + err);
                });
        }
    }else
    {
        res.status(422).send(utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.crea_fattura_da_payment_ecommerce = function(req, res) {
   

    var Sequelize = require('sequelize'),
        payment_id = req.params.id;

    if(payment_id){
        sequelize.query('SELECT crea_fattura_automatica_da_ecommerce(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                payment_id +');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                res.status(200).send(datas[0].crea_fattura_automatica_da_ecommerce);

            }).catch(function(err) {
                return res.status(400).render('crea_fattura_automatica_da_ecommerce: ' + err);
            });
        
    }else
    {
        res.status(422).send(utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.destroy_payment_event = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var Sequelize = require('sequelize'),
        payment_id = req.params.id;

    sequelize.query('SELECT scuolasci_destroy_payment_event_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payment_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('scuolasci_destroy_payment_event: ' + err);
        });
};

exports.annulla_prenotazione = function(req, res) {
    var projects = [];

    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.body.projects !== undefined && req.body.projects !== null) {
        projects = req.body.projects;
    }

    sequelize.query('SELECT scuolasci_annulla_prenotazione(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (projects.length > 0 ? 'ARRAY[' + projects + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('scuolasci_annulla_prenotazione: ' + err);
        });
};

exports.destroy_payment_project = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        payment_id = req.body.payment_id,
        project_id = req.body.project_id;

    sequelize.query('SELECT scuolasci_destroy_payment_project(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payment_id + ',' +
            project_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('scuolasci_destroy_payment_project: ' + err);
        });
};
/*
exports.payment_import_for_event = function(req, res){
  var Sequelize = require('sequelize'),
      event_id = req.body.event_id,
      payment_id = req.body.payment_id,
      importo = req.body.importo,
      project_id = req.body.project_id;

  sequelize.query('SELECT scuolasci_event_payment('+
      req.user._tenant_id+',\''+
      req.headers['host'].split(".")[0]+'\','+
      event_id+','+
      payment_id+','+
      importo+','+
      project_id+');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
      })
      .then(function(datas) {
           res.status(200).send({});
        
         
      }).catch(function(err) {
        return res.status(400).render('scuolasci_event_payment: ' + err);
      });    
};*/

exports.assegna_residuo_pagamento = function(req, res) {
    var Sequelize = require('sequelize'),
        async = require('async'),
            payment_id = req.body.payment_id,
            events = [],
            projects = [];

    if (req.body.events !== undefined && req.body.events !== null) {
        events = req.body.events;
    }

    if (req.body.projects !== undefined && req.body.projects !== null) {
        projects = req.body.projects;
    }

    if (payment_id) {
        async.parallel({
            event: function(callback) {
                sequelize.query('SELECT scuolasci_payment_for_event_v1(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        payment_id + ',' +
                        (events.length > 0 ? 'ARRAY[' + events + ']::bigint[]' : null) + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {
                        callback(null, 1);
                    }).catch(function(err) {
                        callback(err, null);
                    });
            },
            project: function(callback) {
                sequelize.query('SELECT scuolasci_payment_for_project_v1(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        payment_id + ',' +
                        (projects.length > 0 ? 'ARRAY[' + projects + ']::bigint[]' : null) + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {
                        callback(null, 1);
                    }).catch(function(err) {
                        callback(err, null);
                    });
            }

        }, function(err, results) {
            if (err) {
                return res.status(400).render('Sanitize residuo payment: ' + err);
            }
            sequelize.query('SELECT scuolasci_sanitize_residuo_payment_v3(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    payment_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                    res.status(200).send({});
                }).catch(function(err) {
                    return res.status(400).render('Sanitize residuo payment: ' + err);
                });
        });

    } else {
        res.status(422).send(utility.error422('payment_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
};

exports.salda_residui_contact = function(req, res) {
    var Sequelize = require('sequelize'),
        contact_id = req.params.id;

    sequelize.query('SELECT scuolasci_sanitize_residui_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            contact_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('Sanitize residui for contact: ' + err);
        });
};

exports.sanitize_conto_cliente = function(req, res) {
    var contact_id = req.params.id;

    sequelize.query('SELECT scuolasci_sanitize_conto_cliente(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            contact_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('Sanitize conto cliente: ' + err);
        });
};

exports.update_payment = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.payment_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        payment_id = req.params.id;

    sequelize.query('SELECT scuolasci_update_payment_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            payment_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci Update payment: ' + err);
        });
};

exports.update_project = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        project_id = req.params.id;

    sequelize.query('SELECT scuolasci_update_project_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            project_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci Update project: ' + err);
        });
};

exports.load_split = function(req, res) {
    var Sequelize = require('sequelize'),
        event_id = req.params.id;

    if (event_id && event_id !== undefined && event_id !== 'undefined') {
        sequelize.query('SELECT scuolasci_find_split(' +
                event_id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                req.user.role_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                res.status(200).send(datas[0]['scuolasci_find_split']);

            }).catch(function(err) {
                return res.status(400).render('scuolasci_event_contact: ' + err);
            });
    } else {
        res.status(200).send({});
    }
};

exports.event_split_contact = function(req, res) {
    var Sequelize = require('sequelize'),
        events = req.body.data,
        assicurazione = req.body.assicurazione,
        tot_percentuale_evento = 0,
        errore = 0;

  /*  
    var ev = 0;

    if (events) {

        events.forEach(function(evento) {

            var event_id = evento.id,
                price = parseFloat(evento.price),
                contacts = evento.contacts;

            var cnt = 0;
            console.log(evento);
            if (contacts && contacts.length > 0) {
                contacts.forEach(function(cliente) {
                    var contact_id = cliente.id;
                    var partecipanti = cliente.option;

                    if (partecipanti) {
                        var cnt_part = 0;
                        var tot_partecipanti = 0;
                        partecipanti.forEach(function(part) {
                            if (part.selected == 'true' || part.selected == true) {
                                tot_partecipanti++;
                            }

                            if (cnt_part++ >= partecipanti.length - 1) {

                                console.log(tot_partecipanti);
                                
                                    var dovuto = 0.00;
                                    if (!tot_partecipanti) {
                                        tot_partecipanti = 0;
                                        dovuto = 0;
                                    }else{
                                        console.log('DOVUTO');
                                        dovuto = parseFloat((parseFloat(evento.price) / parseFloat(evento.players_number) * tot_partecipanti));
                                         console.log(evento.price);
                                         console.log(evento.players_number);
                                         console.log(dovuto);
                                    }

                                    

                                    sequelize.query('SELECT scuolasci_event_contact_v1(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            event_id + ',' +
                                            contact_id + ',' +
                                            dovuto + ',' +
                                            tot_partecipanti + ',\'' +
                                            JSON.stringify(partecipanti).replace(/'/g, "''''") + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                            })
                                        .then(function(datas) {*/


                                sequelize.query('SELECT scuolasci_event_contact_v2(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',\'' +
                                            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            
                                            
                                               
                                                logs.save_log_model(
                                                    req.user._tenant_id,
                                                    req.headers['host'].split(".")[0],
                                                    'event',
                                                    'update',
                                                    req.user.id,
                                                    JSON.stringify('{}'),
                                                    null,
                                                    'Debito delle lezioni ' + JSON.stringify(events).toString().replace(/'/g, "") + ' splittate'
                                                );

                                              /*  sequelize.query('SELECT scuolasci_event_contact_sistema_prenotazione(' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    event_id + ',' +
                                                    contact_id + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    }).then(function(datas) {
                                                        
                                                        if(ev >= events.length - 1){*/
                                                            return res.status(200).send({});
                                                   /*     }
                                                }).catch(function(err) {
                                                    return res.status(400).render('scuolasci_event_contact_sistema_prenotazione: ' + err);
                                                });*/
                                           
                                        }).catch(function(err) {
                                            return res.status(400).render('scuolasci_event_contact: ' + err);
                                        });


                         /*       


                            }
                        });
                    }
                });

            }

            ev++;
        });

    } else {
        return res.status(422).send(utility.error422('events', 'Parametro non valido', 'Parametro non valido', '422'));
    }*/
};

exports.spezza_eventi = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        event_id = req.body.event_id,
        start_date = moment(req.body.start_date),
        end_date = moment(req.body.end_date),
        interval = req.body.interval / 30;


    var minutes = end_date.diff(start_date, 'minutes');
    minutes = minutes / 30;

    if (minutes == 0 || minutes <= interval) {
        return res.status(200).send({});
    }

    sequelize.query('SELECT scuolasci_spezza_eventi_v3(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            event_id + ',' +
            (minutes - 1) + ',' +
            interval + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].scuolasci_spezza_eventi_v3 === '0') {
                res.status(422).send(utility.error422('event_id', 'Lezione già confermata dal maestro', 'Lezione già confermata dal maestro', '422'));
            } else {
                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'event',
                    'update',
                    req.user.id,
                    JSON.stringify('{}'),
                    event_id,
                    'Lezione spezzata in mezzore ' + event_id
                );
                res.status(200).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('scuolasci_spezza_eventi: ' + err);
        });
};

exports.spezza_prenotazione = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        project_id = req.body.project_id;

    if (project_id) {
        sequelize.query('SELECT scuolasci_spezza_prenotazione(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                project_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                res.status(200).send({});

            }).catch(function(err) {
                return res.status(400).render('scuolasci_spezza_prenotazione: ' + err);
            });
    } else {
        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

};

exports.sanitize_event = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        event_id = req.body.event_id,
        start = req.body.start_date,
        end = req.body.end_date,
        maestro_id = req.body.contact_id,
        durata = 0;

    var day = moment(start);
    var end_date = moment(end);
    /*
    if (day.isoWeekday() === 6 ||
      day.isoWeekday() === 7 ||
      day.isBetween('2016-12-06', '2016-12-12', 'day') ||
      day.isBetween('2016-12-18', '2017-01-09', 'day') ||
      day.isBetween('2017-02-12', '2017-03-11', 'day') ||
      day.isBetween('2017-04-13', '2017-04-20', 'day')) {
       alta = true;
    }*/

    var minutes = end_date.diff(day, 'minutes');
    durata = minutes / 30;

    sequelize.query('SELECT scuolasci_sanitize_eventi_after_update_v4(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            event_id + ',' +
            maestro_id + ',' +
            durata + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            /* sequelize.query('SELECT scuolasci_sanitize_residuo_payment('+
              req.user._tenant_id+',\''+
              req.headers['host'].split(".")[0]+'\','+
              payment_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
              })
              .then(function(datas) {
                   res.status(200).send({});
              }).catch(function(err) {
                return res.status(400).render('Sanitize residuo payment: ' + err);
              });*/
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('scuolasci_sanitize_eventi_after_update: ' + err);
        });
};

exports.elimina_da_calendario = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var util = require('util');
    var starts = [],
        ends = [],
        contact_id = null,
        comprensorio = null;

    if (req.body.starts !== undefined && req.body.starts != '') {
        if (util.isArray(req.body.starts)) {
            req.body.starts.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.ends !== undefined && req.body.ends != '') {
        if (util.isArray(req.body.ends)) {
            req.body.ends.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        if (!utility.check_type_variable(req.body.comp, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (comprensorio) {
        sequelize.query('SELECT scuolasci_elimina_da_calendario_v1(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                contact_id + ',' +
                (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',' +
                comprensorio + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(data) {
                if (data && data[0] && data[0].scuolasci_elimina_da_calendario_v1) {


                    var result = JSON.stringify(data[0].scuolasci_elimina_da_calendario_v1);

                    if (result.indexOf('422') == -1) {

                        var str_dates = '';
                        for (var i = 0; i < starts.length; i++) {
                            str_dates += starts[i].replace(/'/g, " ") + '-' + ends[i].replace(/'/g, " ") + ' ';
                        };

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'event',
                            'destroy',
                            req.user.id,
                            JSON.stringify('{}'),
                            null,
                            'Eliminati eventi da calendario maestro ' + contact_id + ' comprensorio ' + req.body.comp.replace(/'/g, " ") + ' date ' + str_dates
                        );

                        res.status(200).send({});
                    } else {
                        res.status(422).send({});
                    }
                } else {
                    console.log('SELECT scuolasci_elimina_da_calendario_v1(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        contact_id + ',' +
                        (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                        (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',\'' +
                        comprensorio + '\');');
                    res.status(422).send({});
                }

            }).catch(function(err) {
                return res.status(400).render('Elimina eventi da calendario: ' + err);
            });
    } else {
        return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));

    }
};

exports.destroy_event = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        event_id = req.params.id;

    if (event_id && event_id !== undefined && event_id !== 'undefined' && !utility.check_id(event_id)) {
        sequelize.query('SELECT scuolasci_elimina_evento_v4(' +
                event_id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                req.user.role_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].scuolasci_elimina_evento_v4 === '0') {
                    res.status(422).send(utility.error422('event_id', 'Lezione già confermata dal maestro', 'Lezione già confermata dal maestro', '422'));
                } else {
                    res.status(200).send({});
                }
            }).catch(function(err) {
                return res.status(400).render('scuolasci_elimina_evento: ' + err);
            });
    } else {
        res.status(200).send({});
    }
};

exports.destroy_project = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var Sequelize = require('sequelize'),
        project_id = req.params.id,
        payment_delete = (req.query.delete_all_payment ? req.query.delete_all_payment : false),
        workhour_delete = (req.query.delete_all_workhour ? req.query.delete_all_workhour : false),
        event_delete = (req.query.delete_all_event ? req.query.delete_all_event : false);



    if (project_id && project_id !== undefined && project_id !== 'undefined') {
        sequelize.query('SELECT scuolasci_elimina_prenotazione_v7(' +
                project_id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                req.user.role_id + ',' +
                workhour_delete + ',' +
                payment_delete + ','+
                event_delete+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                if (datas[0].scuolasci_elimina_prenotazione_v7 === '0') {
                    res.status(422).send(utility.error422('project_id', 'La prenotazione contiene lezione già confermate dal maestro', 'La prenotazione contiene lezione già confermate dal maestro', '422'));
                } else if (datas[0].scuolasci_elimina_prenotazione_v7 === '-1') {
                    res.status(422).send(utility.error422('project_id', 'Nessuna prenotazione corrispondente', 'Nessuna prenotazione corrispondente', '422'));
                } else {
                    res.status(200).send({});
                }

            }).catch(function(err) {
                return res.status(400).render('scuolasci_elimina_prenotazione: ' + err);
            });
    } else {
        res.status(200).send({});
    }
};