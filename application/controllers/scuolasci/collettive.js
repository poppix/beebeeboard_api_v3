var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');

/*
 * @apiUse IdParamEntryError
 */
exports.get_corsi_attivi = function(req, res){
   
    

    sequelize.query('SELECT scuolasci_get_corsi_attivi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            req.query.status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
                
            })
        .then(function(datas) {
           
           if(datas && datas[0] && datas[0].scuolasci_get_corsi_attivi){
                res.status(200).send(datas[0].scuolasci_get_corsi_attivi);
            }
            else{
                res.status(200).send({});
            }
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_get_corsi_attivi: ' + err);
        });
}

exports.scuolasci_date_collettiva = function(req, res){
    var product_id = null,
        giorni = null,
        comprensorio = null,
        selected_date = null;

    if ((req.query.product_id !== undefined && utility.check_id(req.query.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.query.product_id !== undefined && req.query.product_id !== '') {
        product_id = req.query.product_id;
    }

    if (req.query.comprensorio !== undefined && req.query.comprensorio !== '') {
        if (!utility.check_type_variable(req.query.comprensorio, 'string')) {
            return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        comprensorio = '\''+req.query.comprensorio.replace(/'/g, "''''")+'\'';
    } else {
        return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    
    if (req.query.selected_date !== undefined && req.query.selected_date !== '') {
        selected_date = '\''+req.query.selected_date+'\'';
    }

    sequelize.query('SELECT scuolasci_date_collettiva(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            product_id  + ',' +
            comprensorio + ','+
            selected_date+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
           if(data && data[0] && data[0]['scuolasci_date_collettiva'] && data[0]['scuolasci_date_collettiva'][0]){
                 res.status(200).send(data[0]['scuolasci_date_collettiva'][0]);
           }else
           {
                res.status(200).send({});
           }
           
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_date_collettiva: ' + err);
        });
};

exports.scuolasci_get_info_collettiva = function(req, res){
    var product_id = null,
        data_inizio = null,
        comprensorio = null;

    if ((req.query.product_id !== undefined && utility.check_id(req.query.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.query.product_id !== undefined && req.query.product_id !== '') {
        product_id = req.query.product_id;
    }

    if ((req.query.data_inizio !== undefined && utility.check_id(req.query.data_inizio))) {
        return res.status(422).send(utility.error422('data_inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.query.data_inizio !== undefined && req.query.data_inizio !== '') {
        data_inizio = '\''+req.query.data_inizio+'\'';
    }

    if (req.query.comprensorio !== undefined && req.query.comprensorio !== '') {
        if (!utility.check_type_variable(req.query.comprensorio, 'string')) {
            return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        comprensorio = '\''+req.query.comprensorio.replace(/'/g, "''''")+'\'';
    } else {
        return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_get_info_collettiva_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            product_id  + ',' +
            data_inizio + ',' +
            comprensorio + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
           
            res.status(200).send(data[0]['scuolasci_get_info_collettiva_new']);
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_get_info_collettiva_new: ' + err);
        });
};

exports.get_maestro_collettive = function(req, res) {
    var start_array = [],
        end_array = [],
        product_id = null,
        contact_id = null,
        data_inizio = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.prod_name !== undefined && req.body.prod_name !== '') {
        if (!utility.check_type_variable(req.body.prod_name, 'string')) {
            return res.status(422).send(utility.error422('prod_name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.custom !== undefined && req.body.custom !== '') {
        if (!utility.check_type_variable(req.body.custom, 'string')) {
            return res.status(422).send(utility.error422('custom', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        var weekArray = [];
        var tot_ore = 0;

        var splitItem = req.body.custom.toString().split('\n');

        if (!utility.check_id(splitItem[6])) {
            var dayElement = {};
            var elementi = splitItem[6].split(',');
            dayElement.i = 0;
            dayElement.name = moment().day(6 + 1).format('dddd');
            dayElement.start_h = elementi[2];
            dayElement.start_m = elementi[3];
            dayElement.end_h = elementi[4];
            dayElement.end_m = elementi[5];
        }

        weekArray.push(dayElement);

        for (var i = 0; i < 6; i++) {
            var dayElement = {};

            dayElement.i = i == 6 ? 0 : i + 1;
            dayElement.name = moment().day(i + 1).format('dddd');


            if (!utility.check_id(splitItem[i])) {
                var elementi = splitItem[i].split(',');

                dayElement.start_h = elementi[2];
                dayElement.start_m = elementi[3];
                dayElement.end_h = elementi[4];
                dayElement.end_m = elementi[5];
            }

            weekArray.push(dayElement);
        }

        for (var i = (moment(data_inizio).weekday()); i < weekArray.length; i++) {
            if (!utility.check_id(weekArray[i].start_h)) {
                var start_date = null,
                    end_date = null,
                    giorni_to_add = 0;

                giorni_to_add = i - (moment(data_inizio).weekday());

                start_date = moment(data_inizio).hour(weekArray[i].start_h).minute(weekArray[i].start_m).add(giorni_to_add, 'days');
                end_date = moment(data_inizio).hour(weekArray[i].end_h).minute(weekArray[i].end_m).add(giorni_to_add, 'days');
                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                start_array.push('\'' + moment.utc(start_date).format().toString() + '\'');
                end_array.push('\'' + moment.utc(end_date).format().toString() + '\'');
            }
        }
        for (var i = 0; i < (moment(data_inizio).weekday()); i++) {
            if (!utility.check_id(weekArray[i].start_h)) {
                var start_date = null,
                    end_date = null,
                    giorni_to_add = 0;

                giorni_to_add = i - (moment(data_inizio).weekday()) + 7;

                start_date = moment(data_inizio).hour(weekArray[i].start_h).minute(weekArray[i].start_m).add(giorni_to_add, 'days');
                end_date = moment(data_inizio).hour(weekArray[i].end_h).minute(weekArray[i].end_m).add(giorni_to_add, 'days');
                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                start_array.push('\'' + moment.utc(start_date).format().toString() + '\'');
                end_array.push('\'' + moment.utc(end_date).format().toString() + '\'');

            }
        }

    }

    sequelize.query('SELECT scuolasci_maestri_collettive(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            'ARRAY[' + start_array + ']::character varying[],' +
            'ARRAY[' + end_array + ']::character varying[],' +
            contact_id + ',' +
            product_id +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {


            if (data[0].scuolasci_maestri_collettive && data[0].scuolasci_maestri_collettive != null) {
                if (data[0].scuolasci_maestri_collettive[0] && data[0].scuolasci_maestri_collettive[0].attributes.total >= tot_ore) {
                    console.log('TUTTE LIBERE');



                    var cnt = 0;
                    var str_dates = '';
                    for (var i = 0; i < start_array.length; i++) {



                        str_dates += start_array[i].replace(/'/g, " ") + '-' + end_array[i].replace(/'/g, " ") + ' ';

                        var event = {};
                        event['owner_id'] = req.user.id;
                        event['title'] = req.body.prod_name ? req.body.prod_name.replace(/'/g, '') : null;
                        event['start_date'] = start_array[i].replace(/'/g, '');
                        event['end_date'] = end_array[i].replace(/'/g, '');
                        event['all_day'] = false;
                        event['color'] = '';
                        event['notes'] = '';
                        event['_tenant_id'] = req.user._tenant_id;
                        event['organization'] = req.headers['host'].split(".")[0];
                        event['mongo_id'] = null;
                        event['event_status_id'] = 183;
                        event['archivied'] = false;
                        event['description'] = null;



                        async.parallel({
                            insert: function(callback) {
                                var cnt = i;
                                sequelize.query('SELECT insert_event_v6(\'' +
                                        JSON.stringify(event) + '\',' +
                                        req.user.id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',' +
                                        req.user._tenant_id + ',null);', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        })
                                    .then(function(datas) {

                                        if (datas[0].insert_event_v6 && datas[0].insert_event_v6 != null) {
                                            var relationships = {
                                                'contacts': {
                                                    'data': [
                                                        { 'id': contact_id }
                                                    ]
                                                },
                                                'products': {
                                                    'data': [
                                                        { 'id': product_id }
                                                    ]
                                                }
                                            };
                                            utility.sanitizeRelations({
                                                'organization': req.headers['host'].split(".")[0],
                                                '_tenant': req.user._tenant_id,
                                                'new_id': datas[0].insert_event_v6,
                                                'body': relationships,
                                                'model': 'event',
                                                'user_id': req.user.id,
                                                'array': ['product', 'contact']
                                            }, function(err, results) {

                                                sequelize.query('SELECT event_custom_fields_trigger(' +
                                                        datas[0].insert_event_v6 + ',' +
                                                        req.user._tenant_id + ',\'' +
                                                        req.headers['host'].split(".")[0] + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                        })
                                                    .then(function(cus) {
                                                        callback(null, cnt);
                                                        logs.save_log_model(
                                                            req.user._tenant_id,
                                                            req.headers['host'].split(".")[0],
                                                            'event',
                                                            'insert',
                                                            req.user.id,
                                                            JSON.stringify(event),
                                                            datas[0].insert_event_v6,
                                                            'Creato evento da associazione maestro ' + contact_id + ' alla collettiva ' + product_id
                                                        );
                                                    }).catch(function(err) {

                                                        return res.status(400).render('collettive maestri event_create: ' + err);
                                                    });
                                            });
                                        }
                                    });
                            }
                        }, function(err, r) {
                            if (err) {
                                res.status(500).send(err);
                            }
                            if (cnt++ >= start_array.length - 1) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'contact',
                                    'update',
                                    req.user.id,
                                    JSON.stringify('{}'),
                                    contact_id,
                                    'Maestro  ' + contact_id + ' associato alla collettiva ' + product_id + ' date ' + str_dates
                                );
                                res.status(200).send({});
                            }
                        });

                    }


                } else {
                    return res.status(422).send(utility.error422('Maestro non sempre disponibile', 'Maestro non sempre disponibile', 'Maestro non sempre disponibile', '422'));
                }

            } else {
                return res.status(422).send(utility.error422('Maestro non sempre disponibile', 'Maestro non sempre disponibile', 'Maestro non sempre disponibile', '422'));
            }

        }).catch(function(err) {
            return res.status(400).render('maestri collettive: ' + err);
        });
};

exports.disassocia_maestro_collettive = function(req, res) {
    var start_array = [],
        end_array = [],
        product_id = null,
        contact_id = null,
        data_inizio = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.prod_name !== undefined && req.body.prod_name !== '') {
        if (!utility.check_type_variable(req.body.prod_name, 'string')) {
            return res.status(422).send(utility.error422('prod_name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.custom !== undefined && req.body.custom !== '') {
        if (!utility.check_type_variable(req.body.custom, 'string')) {
            return res.status(422).send(utility.error422('custom', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        var weekArray = [];
        var tot_ore = 0;

        var splitItem = req.body.custom.toString().split('\n');

        if (!utility.check_id(splitItem[6])) {
            var dayElement = {};
            var elementi = splitItem[6].split(',');
            dayElement.i = 0;
            dayElement.name = moment().day(6 + 1).format('dddd');
            dayElement.start_h = elementi[2];
            dayElement.start_m = elementi[3];
            dayElement.end_h = elementi[4];
            dayElement.end_m = elementi[5];
        }

        weekArray.push(dayElement);

        for (var i = 0; i < 6; i++) {
            var dayElement = {};

            dayElement.i = i == 6 ? 0 : i + 1;
            dayElement.name = moment().day(i + 1).format('dddd');


            if (!utility.check_id(splitItem[i])) {
                var elementi = splitItem[i].split(',');

                dayElement.start_h = elementi[2];
                dayElement.start_m = elementi[3];
                dayElement.end_h = elementi[4];
                dayElement.end_m = elementi[5];
            }

            weekArray.push(dayElement);
        }

        for (var i = (moment(data_inizio).weekday()); i < weekArray.length; i++) {
            if (!utility.check_id(weekArray[i].start_h)) {
                var start_date = null,
                    end_date = null,
                    giorni_to_add = 0;

                giorni_to_add = i - (moment(data_inizio).weekday());

                start_date = moment(data_inizio).hour(weekArray[i].start_h).minute(weekArray[i].start_m).add(giorni_to_add, 'days');
                end_date = moment(data_inizio).hour(weekArray[i].end_h).minute(weekArray[i].end_m).add(giorni_to_add, 'days');
                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                start_array.push('\'' + moment.utc(start_date).format().toString() + '\'');
                end_array.push('\'' + moment.utc(end_date).format().toString() + '\'');
            }
        }
        for (var i = 0; i < (moment(data_inizio).weekday()); i++) {
            if (!utility.check_id(weekArray[i].start_h)) {
                var start_date = null,
                    end_date = null,
                    giorni_to_add = 0;

                giorni_to_add = i - (moment(data_inizio).weekday()) + 7;

                start_date = moment(data_inizio).hour(weekArray[i].start_h).minute(weekArray[i].start_m).add(giorni_to_add, 'days');
                end_date = moment(data_inizio).hour(weekArray[i].end_h).minute(weekArray[i].end_m).add(giorni_to_add, 'days');
                tot_ore += moment.duration(end_date.diff(start_date)).asHours();
                start_array.push('\'' + moment.utc(start_date).format().toString() + '\'');
                end_array.push('\'' + moment.utc(end_date).format().toString() + '\'');

            }
        }

    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\'' + req.body.inizio + '\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = '\'' + req.body.fine + '\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_disassocia_eventi_collettive_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            data_inizio + ',' +
            data_fine + ',' +
            contact_id + ',' +
            product_id +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {

            if (data[0].scuolasci_disassocia_eventi_collettive_v1 && data[0].scuolasci_disassocia_eventi_collettive_v1 != null) {
                var str_dates = '';
                for (var i = 0; i < start_array.length; i++) {
                    str_dates += start_array[i].replace(/'/g, " ") + '-' + end_array[i].replace(/'/g, " ") + ' ';
                }

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'contact',
                    'update',
                    req.user.id,
                    JSON.stringify('{}'),
                    contact_id,
                    'Maestro  ' + contact_id + ' disassociato dalla collettiva ' + product_id + ' date ' + str_dates
                );
                var cnt = 0;

                data[0].scuolasci_disassocia_eventi_collettive_v1.forEach(function(ev) {
                    sequelize.query('SELECT delete_event_v2(' +
                            ev.id + ',' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user.id + ',' +
                            req.user.role_id + ',null);', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {

                            if (cnt++ >= data[0].scuolasci_disassocia_eventi_collettive_v1.length - 1) {
                                res.json({
                                    'data': {
                                        'type': 'events',
                                        'id': ev.id
                                    }
                                });
                            }


                        }).catch(function(err) {
                            return res.status(400).render('event_destroy: ' + err);
                        });
                });
            } else {
                return res.status(200).sned({});
            }


        }).catch(function(err) {
            return res.status(400).render('maestri collettive: ' + err);
        });
};

exports.collettive_insert_day = function(req, res){
   
    console.log(req.body);

    sequelize.query('SELECT scuolasci_collettive_new_day(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
           
            res.status(200).send({});
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_collettive_new_day: ' + err);
        });
}

exports.collettive_presenza_effettiva = function(req, res){
   var data_inizio = null;
   var data_fine = null;
   var operation = null;

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!utility.check_type_variable(req.body.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\'' + req.body.from_date + '\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!utility.check_type_variable(req.body.to_date, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = '\'' + req.body.to_date + '\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_collettive_conferma_presenze(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\','+
            data_inizio+','+
            data_fine+',\''+
            req.body.operation+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
           
            res.status(200).send({});
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_collettive_conferma_presenze: ' + err);
        });
}

exports.add_lista_attesa = function(req, res){
   
    console.log(req.body);
}

exports.voucher_confirm = function(req, res){
   
    sequelize.query('SELECT voucher_confirm(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',\'' +
        JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['voucher_confirm']) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.json({'id':datas[0]['voucher_confirm']});
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}

exports.check_voucher = function(req, res){
   
    sequelize.query('SELECT check_voucher_new(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        (req.query.voucher_code ? '\''+req.query.voucher_code+'\'' : null) + ','+
        (req.query.voucher_id ? req.query.voucher_id : null) + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['check_voucher_new']) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.json(datas[0]['check_voucher_new']);
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}

exports.use_voucher = function(req, res){
   
    sequelize.query('SELECT use_voucher_new(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        req.body.voucher_id + ', ' +
        req.body.project_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['use_voucher_new']) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.status(200).send({});
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}

exports.create_voucher = function(req, res){
   
    sequelize.query('SELECT create_voucher(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',\'' +
        JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['create_voucher'] &&  datas[0]['create_voucher'] != -1) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.status(200).send(datas[0]['create_voucher']);
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}

exports.delete_voucher = function(req, res){
   
    sequelize.query('SELECT delete_voucher(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        req.params.id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['delete_voucher'] && datas[0]['delete_voucher'] != -1) {
            //res.status(200).send({'data':datas[0]['update_contact_all']});
           
            res.status(200).send({});
                
        } else {
            res.status(422).send({});
        }
     }).catch(function(err) {
            return res.status(500).send({});
    });
}

exports.associa_maestro_bambino = function(req, res) {


    sequelize.query('SELECT scuolasci_collettive_assegna_bambino(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            
                res.status(200).send({});
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_collettive_assegna_bambino: ' + err);
        });
};

exports.scuolasci_maestro_sposta = function(req, res) {


    sequelize.query('SELECT scuolasci_maestro_sposta(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            
                res.status(200).send({'data':1});
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_maestro_sposta: ' + err);
        });
};

exports.disassocia_maestro_bambino = function(req, res) {
    sequelize.query('SELECT scuolasci_collettive_disassegna_bambino(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            
                res.status(200).send({});
            

        }).catch(function(err) {
            return res.status(400).render('scuolasci_collettive_disassegna_bambino: ' + err);
        });
};

exports.associa_maestro_bambino_old = function(req, res) {
    var
        /*start_array = [],
           	end_array = [],*/
        product_id = null,
        contact_id = null,
        client_id = null,
        data_inizio = null,
        data_fine = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if ((req.body.client_id !== undefined && utility.check_id(req.body.client_id))) {
        return res.status(422).send(utility.error422('client_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.client_id !== undefined && req.body.client_id !== '') {
        client_id = req.body.client_id;
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\'' + req.body.inizio + '\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = '\'' + req.body.fine + '\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_disassocia_eventi_collettive_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            data_inizio + ',' +
            data_fine + ',' +
            contact_id + ',' +
            product_id +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            console.log('lungo: ' + data[0].scuolasci_disassocia_eventi_collettive_v1.length);
            var cnt = 0;
            if (data && data[0] && 
                data[0].scuolasci_disassocia_eventi_collettive_v1 && 
                data[0].scuolasci_disassocia_eventi_collettive_v1.length > 0) {

                data[0].scuolasci_disassocia_eventi_collettive_v1.forEach(function(ev) {
                    sequelize.query('SELECT scuolasci_collettiva_associa_maestro_bambino(' +
                            client_id + ',' +
                            ev.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {

                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'contact',
                                'update',
                                req.user.id,
                                JSON.stringify('{}'),
                                contact_id,
                                'Maestro  ' + contact_id + ' associato al bambino ' + client_id + ' nella collettiva ' + product_id + ' date ' + data_inizio.replace(/'/g, " ") + ' - ' + data_fine.replace(/'/g, " ")
                            );
                            console.log('cnt: ' + cnt);
                            if (cnt++ >= data[0].scuolasci_disassocia_eventi_collettive_v1.length - 1) {

                                res.json({
                                    'data': {
                                        'type': 'events',
                                        'id': ev.id
                                    }
                                });

                            }

                        }).catch(function(err) {
                            return res.status(400).render('insert_event_contact_collettive: ' + err);
                        });
                });
            } else {
                res.status(200).send({});
            }

        }).catch(function(err) {
            return res.status(400).render('maestri-bambini associa collettive: ' + err);
        });
};

exports.disassocia_maestro_bambino_old = function(req, res) {
    var
        /*start_array = [],
            end_array = [],*/
        product_id = null,
        contact_id = null,
        client_id = null,
        data_inizio = null,
        data_fine = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if ((req.body.client_id !== undefined && utility.check_id(req.body.client_id))) {
        return res.status(422).send(utility.error422('client_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.client_id !== undefined && req.body.client_id !== '') {
        client_id = req.body.client_id;
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = '\'' + req.body.inizio + '\'';
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_fine = '\'' + req.body.fine + '\'';
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT scuolasci_disassocia_eventi_collettive_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            /*'ARRAY[' + start_array + ']::character varying[],'+
            'ARRAY[' + end_array + ']::character varying[],'+*/
            data_inizio + ',' +
            data_fine + ',' +
            contact_id + ',' +
            product_id +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {

            var cnt = 0;
            logs.save_log_model(
                req.user._tenant_id,
                req.headers['host'].split(".")[0],
                'contact',
                'update',
                req.user.id,
                JSON.stringify('{}'),
                contact_id,
                'Maestro  ' + contact_id + ' disassociato dal bambino ' + client_id + ' nella collettiva ' + product_id + ' date ' + data_inizio.replace(/'/g, " ") + ' - ' + data_fine.replace(/'/g, " ")
            );
            if (data[0].scuolasci_disassocia_eventi_collettive_v1) {
                data[0].scuolasci_disassocia_eventi_collettive_v1.forEach(function(ev) {
                    sequelize.query('SELECT scuolasci_collettiva_disassocia_maestro_bambino(' +
                            client_id + ',' +
                            ev.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {

                            if (cnt++ >= data[0].scuolasci_disassocia_eventi_collettive_v1.length - 1) {
                                res.json({
                                    'data': {
                                        'type': 'events',
                                        'id': ev.id
                                    }
                                });
                            }

                        }).catch(function(err) {
                            return res.status(400).render('delete_event_contact_collettive: ' + err);
                        });
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('maestri-bambini disassocia collettive: ' + err);
        });
};

exports.find_collettiva_old = function(req, res) {
    var product_id = [],
        data_inizio = null,
        end_date = null,
        giorno_esatto = null;
    var util = require('util');


    if (req.body.product_id !== undefined && req.body.product_id != '') {
        if (util.isArray(req.body.product_id)) {
            product_id = req.body.product_id;
        } else {
            product_id.push(req.body.product_id);
        }
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.giorno !== undefined && req.body.giorno !== '') {
        if (!utility.check_type_variable(req.body.giorno, 'string')) {
            return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        giorno_esatto = req.body.giorno;
    } else {
        return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_find_collettiva_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            (product_id.length > 0 ? 'ARRAY[' + product_id + ']::bigint[]' : null) + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\',\'' +
            giorno_esatto + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.json({
                'data': data[0].scuolasci_find_collettiva_v2[0]
            });
        }).catch(function(err) {
            return res.status(400).render('find_collettiva: ' + err);
        });
};

exports.find_collettiva = function(req, res) {
    var product_id = [],
        data_inizio = null,
        end_date = null,
        giorno_esatto = null,
        color = null;
    var util = require('util');


    if (req.body.product_id !== undefined && req.body.product_id != '') {
        if (util.isArray(req.body.product_id)) {
            product_id = req.body.product_id;
        } else {
            product_id.push(req.body.product_id);
        }
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.color !== undefined && req.body.color !== '') {
        if (!utility.check_type_variable(req.body.color, 'string')) {
            return res.status(422).send(utility.error422('color', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        color = '\''+req.body.color.replace(/'/g, "''''")+'\'';
    }


    sequelize.query('SELECT scuolasci_dashboard_collettiva_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            (product_id.length > 0 ? 'ARRAY[' + product_id + ']::bigint[]' : null) + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\','+
            color+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            utility.removeEmpty(data[0]['scuolasci_dashboard_collettiva_v1'][0]);
            res.json(data[0]['scuolasci_dashboard_collettiva_v1'][0]);
        }).catch(function(err) {
            return res.status(400).render('find_collettiva: ' + err);
        });
};

exports.csv_collettiva = function(req, res) {
    var json2csv = require('json2csv');
    var product_id = null,
        data_inizio = null,
        end_date = null,
        giorno_esatto = null,
        color = null;
    var util = require('util');


    if (req.query.product_id !== undefined && req.query.product_id != '') {
        
            product_id = req.query.product_id;
        
    }

    if (req.query.inizio !== undefined && req.query.inizio !== '') {
        if (!utility.check_type_variable(req.query.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.query.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.fine !== undefined && req.query.fine !== '') {
        if (!utility.check_type_variable(req.query.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.query.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.color !== undefined && req.query.color !== '') {
        if (!utility.check_type_variable(req.query.color, 'string')) {
            return res.status(422).send(utility.error422('color', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        color = '\''+req.query.color.replace(/'/g, "''''")+'\'';
    }


    sequelize.query('SELECT scuolascimontebianco_collettive_stats_csv(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\','+
            color+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] &&  datas[0].scuolascimontebianco_collettive_stats_csv &&  datas[0].scuolascimontebianco_collettive_stats_csv[0]){
                    var fields_array = datas[0].scuolascimontebianco_collettive_stats_csv[0]['fields'];
                    var fieldNames_array = datas[0].scuolascimontebianco_collettive_stats_csv[0]['field_names'];
                    console.log(fields_array.length);
                    console.log(fieldNames_array.length);
                    json2csv({
                        data: datas[0].scuolascimontebianco_collettive_stats_csv,
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=collettiva.csv'
                        });
                        res.end(csv);
                    });
                }
        }).catch(function(err) {
            return res.status(400).render('csv_collettiva: ' + err);
        });
};

exports.mail_collettiva = function(req, res) {
    var product_id = [],
        data_inizio = null,
        end_date = null,
        giorno_esatto = null;
    var util = require('util');


    if (req.body.product_id !== undefined && req.body.product_id != '') {
        if (util.isArray(req.body.product_id)) {
            product_id = req.body.product_id;
        } else {
            product_id.push(req.body.product_id);
        }
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT scuolasci_collettiva_mail(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            (product_id.length > 0 ? 'ARRAY[' + product_id + ']::bigint[]' : null) + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            utility.removeEmpty(data[0]['scuolasci_collettiva_mail'][0]);
            res.json(data[0]['scuolasci_collettiva_mail'][0]);
        }).catch(function(err) {
            return res.status(400).render('find_collettiva: ' + err);
        });
};

exports.find_maestri_per_product_period = function(req, res) {
    var product_id = null,
        data_inizio = null,
        end_date = null;
    var util = require('util');


    if (req.body.product_id !== undefined && req.body.product_id != '') {

        product_id = req.body.product_id;

    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_find_maestri_product_period(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            if (data && data[0] && data[0].scuolasci_find_maestri_product_period) {
                res.json({
                    'data': data[0].scuolasci_find_maestri_product_period[0]
                });
            } else {
                res.json({ 'data': null });
            }

        }).catch(function(err) {
            return res.status(400).render('scuolasci_find_maestri_product_period: ' + err);
        });
};

exports.set_color_maestro = function(req, res) {
    var product_id = [],
        data_inizio = null,
        end_date = null,
        giorno_esatto = null,
        maestro_id = null;

    var util = require('util');

    if (req.body.product_id !== undefined && req.body.product_id != '') {
        if (util.isArray(req.body.product_id)) {
            product_id = req.body.product_id;
        } else {
            product_id.push(req.body.product_id);
        }
    }

    if (req.body.contact_id !== undefined && req.body.contact_id != '') {
        if (utility.check_id(req.body.contact_id)) {
            return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        maestro_id = req.body.contact_id;
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.giorno !== undefined && req.body.giorno !== '') {
        if (!utility.check_type_variable(req.body.giorno, 'string')) {
            return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        giorno_esatto = req.body.giorno;
    } else {
        return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_collettiva_set_color_maestro(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            (product_id.length > 0 ? 'ARRAY[' + product_id + ']::bigint[]' : null) + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\',\'' +
            giorno_esatto + '\',' +
            maestro_id + ',\'' +
            req.body.color + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.json({
                'data': data[0].scuolasci_collettiva_set_color_maestro
            });
        }).catch(function(err) {
            return res.status(400).render('collettiva_set_color_maestro: ' + err);
        });
};

exports.prenotazione_collettiva = function(req, res) {
    var product_id = [],
        starts = [],
        ends = [],
        event_state = null,
        ids = [];
    var maestro_id = null;
    var util = require('util');

    if (req.body.product_id !== undefined && req.body.product_id != '') {
        if (utility.check_id(req.body.product_id)) {
            return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        product_id = req.body.product_id;
    }

    if (req.body.event_state_id !== undefined && req.body.event_state_id !== '') {
        if (utility.check_id(req.body.event_state_id)) {
            return res.status(422).send(utility.error422('event_state_id', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        event_state = req.body.event_state_id;
    }

    if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.maestro_ids !== undefined && req.body.maestro_ids != '') {
        if (util.isArray(req.body.maestro_ids)) {
            req.body.maestro_ids.forEach(function(end) {
                ids.push(end);
            });
        }
    }

    if ((req.body.maestro_id !== undefined && utility.check_id(req.body.maestro_id))) {
        return res.status(422).send(utility.error422('maestro_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.maestro_id !== undefined && req.body.maestro_id !== '') {
        maestro_id = req.body.maestro_id;
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        if (!utility.check_type_variable(req.body.comp, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.name !== undefined && req.body.name !== '') {
        if (!utility.check_type_variable(req.body.name, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    sequelize.query('SELECT scuolasci_prenotazione_collettiva_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
            (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',\'' +
            req.body.name.replace(/'/g, "''''") + '\',' +
            maestro_id + ',\'' +
            req.body.comp.replace(/'/g, "''''") + '\',' +
            event_state + ',' +
            product_id + ','+
            (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            if (data[0].scuolasci_prenotazione_collettiva_new != 1) {
                var str_dates = '';
                for (var i = 0; i < starts.length; i++) {
                    str_dates += starts[i].replace(/'/g, " ") + '-' + ends[i].replace(/'/g, " ") + ' ';
                }

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'contact',
                    'update',
                    req.user.id,
                    JSON.stringify('{}'),
                    maestro_id,
                    'Maestro  ' + maestro_id + ' associato alla collettiva ' + product_id + ' date ' + str_dates
                );

                res.status(200).send({});
            } else
                res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('scuolasci_prenotazione_collettiva: ' + err);
        });
};