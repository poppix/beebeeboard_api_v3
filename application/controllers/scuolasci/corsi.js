var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js');

/*
 * @apiUse IdParamEntryError
 */
exports.get_maestro_corsi = function(req, res) {
    var start_date = null,
        end_date = null,
        product_id = null,
        contact_id = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

    if (req.body.product_name !== undefined && req.body.product_name !== '') {
        if (!utility.check_type_variable(req.body.product_name, 'string')) {
            return res.status(422).send(utility.error422('product_name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    async.parallel({
        control: function(callback) {
            sequelize.query('SELECT scuolasci_maestri_corsi(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.role_id + ',' +
                    req.user.id + ',' +
                    contact_id + ',' +
                    product_id + ',' +
                    start_date + ',' +
                    end_date +
                    ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(data) {
                    callback(null, data[0].scuolasci_maestri_corsi);
                });
        },
        start: function(callback) {
            sequelize.query('SELECT scuolasci_corso_annuale_starts(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.role_id + ',' +
                    req.user.id + ',' +
                    product_id + ',' +
                    start_date + ',' +
                    end_date +
                    ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(start_res) {
                    callback(null, start_res[0].scuolasci_corso_annuale_starts);
                });
        },
        end: function(callback) {
            sequelize.query('SELECT scuolasci_corso_annuale_ends(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.role_id + ',' +
                    req.user.id + ',' +
                    product_id + ',' +
                    start_date + ',' +
                    end_date +
                    ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(end_res) {
                    callback(null, end_res[0].scuolasci_corso_annuale_ends);
                });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('corsi maestri first async: ' + err);
        }

        var tot_ore = 0;
        //tot_ore
        for (var i = 0; i < results.start.length; i++) {
            tot_ore += moment.duration(moment(results.end[i]).diff(moment(results.start[i]))).asHours();
        }

        if (results.control && results.control != null) {
            if (results.control[0] && results.control[0].attributes.total >= tot_ore) {
                console.log('TUTTE LIBERE');
                var cnt = 0;
                for (var i = 0; i < results.start.length; i++) {
                    var event = {};
                    event['owner_id'] = req.user.id;
                    event['title'] = req.body.product_name ? req.body.product_name.replace(/'/g, '') : null;
                    event['start_date'] = results.start[i].replace(/'/g, '');
                    event['end_date'] = results.end[i].replace(/'/g, '');
                    event['all_day'] = false;
                    event['color'] = '';
                    event['notes'] = '';
                    event['_tenant_id'] = req.user._tenant_id;
                    event['organization'] = req.headers['host'].split(".")[0];
                    event['mongo_id'] = null;
                    event['event_status_id'] = 181;
                    event['archivied'] = false;
                    event['description'] = results.control[0].attributes.comprensorio;

                    async.parallel({
                        insert: function(callback) {
                            var cnt = i;
                            sequelize.query('SELECT insert_event_v6(\'' +
                                    JSON.stringify(event) + '\',' +
                                    req.user.id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    req.user._tenant_id + ',null);', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {

                                    if (datas[0].insert_event_v6 && datas[0].insert_event_v6 != null) {
                                        var relationships = {
                                            'contacts': {
                                                'data': [
                                                    { 'id': contact_id }
                                                ]
                                            },
                                            'products': {
                                                'data': [
                                                    { 'id': product_id }
                                                ]
                                            }
                                        };
                                        utility.sanitizeRelations({
                                            'organization': req.headers['host'].split(".")[0],
                                            '_tenant': req.user._tenant_id,
                                            'new_id': datas[0].insert_event_v6,
                                            'body': relationships,
                                            'model': 'event',
                                            'user_id': req.user.id,
                                            'array': ['product', 'contact']
                                        }, function(err, results) {

                                            sequelize.query('SELECT event_custom_fields_trigger(' +
                                                    datas[0].insert_event_v6 + ',' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                         useMaster: true
                                                    })
                                                .then(function(cus) {
                                                    callback(null, cnt);

                                                }).catch(function(err) {

                                                    return res.status(400).render('corsi annuali maestri event_create: ' + err);
                                                });
                                        });
                                    }
                                });
                        }
                    }, function(err, r) {
                        if (err) {
                            res.status(500).send(err);
                        }
                        if (cnt++ >= results.start.length - 1) {
                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'contact',
                                'update',
                                req.user.id,
                                JSON.stringify('{}'),
                                contact_id,
                                'Maestro  ' + contact_id + ' associato al corso annuale ' + product_id
                            );
                            res.status(200).send({});
                        }
                    });

                }
            } else {


                sequelize.query('SELECT scuolasci_corso_dispo(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        contact_id + ',' +
                        product_id + ',' +
                        start_date + ',' +
                        end_date +
                        ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(confr) {
                        res.status(422).send({ 'data': confr[0].scuolasci_corso_dispo });
                    });

            }

        } else {
            sequelize.query('SELECT scuolasci_corso_dispo(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.role_id + ',' +
                    req.user.id + ',' +
                    contact_id + ',' +
                    product_id + ',' +
                    start_date + ',' +
                    end_date +
                    ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(confr) {
                    res.status(422).send({ 'data': confr[0].scuolasci_corso_dispo });
                });
        }


    });
};

exports.disassocia_maestro_corsi = function(req, res) {
    var start_date = null,
        end_date = null,
        product_id = null,
        contact_id = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

    sequelize.query('SELECT scuolasci_disassocia_eventi_corsi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            contact_id + ',' +
            product_id + ',' +
            start_date + ',' +
            end_date +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {

            var cnt = 0;
            if (data[0].scuolasci_disassocia_eventi_corsi &&
                data[0].scuolasci_disassocia_eventi_corsi.length) {
                data[0].scuolasci_disassocia_eventi_corsi.forEach(function(ev) {
                    sequelize.query('SELECT delete_event_v2(' +
                            ev.id + ',' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user.id + ',' +
                            req.user.role_id + ',null);', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {

                            if (cnt++ >= data[0].scuolasci_disassocia_eventi_corsi.length - 1) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'contact',
                                    'update',
                                    req.user.id,
                                    JSON.stringify('{}'),
                                    contact_id,
                                    'Maestro  ' + contact_id + ' disassociato dal corso annuale ' + product_id
                                );
                                res.json({
                                    'data': {
                                        'type': 'events',
                                        'id': ev.id
                                    }
                                });
                            }


                        }).catch(function(err) {
                            return res.status(400).render('event_destroy: ' + err);
                        });
                });
            } else {
                res.status(422).send({});
            }

        }).catch(function(err) {
            return res.status(400).render('maestri corsi: ' + err);
        });
};

exports.find_corso = function(req, res) {
    var product_id = null,
        start_date = null,
        end_date = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_find_corso_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            product_id + ',\'' +
            start_date + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            if(data && data[0] && data[0].scuolasci_find_corso_v1 && data[0].scuolasci_find_corso_v1[0]){

                res.json({
                    'data': data[0].scuolasci_find_corso_v1[0]
                });
            }else{
                res.status(200).send({});
            }

            
        }).catch(function(err) {
            return res.status(400).render('find_corso: ' + err);
        });
};

exports.mail_corso = function(req, res) {
    var product_id = null,
        start_date = null,
        end_date = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start_date = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_corso_mail(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            product_id + ',\'' +
            start_date + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.json({
                'data': data[0].scuolasci_corso_mail[0]
            });
        }).catch(function(err) {
            return res.status(400).render('find_corso: ' + err);
        });
};

exports.associa_maestro_bambino_corso_annuale = function(req, res) {
    var start_date = null,
        end_date = null,
        product_id = null,
        contact_id = null,
        client_id = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if ((req.body.client_id !== undefined && utility.check_id(req.body.client_id))) {
        return res.status(422).send(utility.error422('client_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.client_id !== undefined && req.body.client_id !== '') {
        client_id = req.body.client_id;
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

    sequelize.query('SELECT scuolasci_disassocia_eventi_corsi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            contact_id + ',' +
            product_id + ',' +
            start_date + ',' +
            end_date +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {

            var cnt = 0;
            if (data[0].scuolasci_disassocia_eventi_corsi) {
                data[0].scuolasci_disassocia_eventi_corsi.forEach(function(ev) {
                    sequelize.query('SELECT scuolesci_insert_contact_event(' +
                            client_id + ',' +
                            ev.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {

                            if (cnt++ >= data[0].scuolasci_disassocia_eventi_corsi.length - 1) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'contact',
                                    'update',
                                    req.user.id,
                                    JSON.stringify('{}'),
                                    contact_id,
                                    'Maestro  ' + contact_id + ' associato al bambino ' + client_id + ' per corso annuale ' + product_id
                                );
                                res.json({
                                    'data': {
                                        'type': 'events',
                                        'id': ev.id
                                    }
                                });
                            }

                        }).catch(function(err) {
                            return res.status(400).render('insert_event_contact_corsi: ' + err);
                        });
                });
            } else {
                return res.status(422).send(utility.error422('Lezioni', 'Nessuna lezione associata', 'Nessuna lezione associata', '422'));
            }

        }).catch(function(err) {
            return res.status(400).render('maestri-bambini associa corsi: ' + err);
        });
};

exports.disassocia_maestro_bambino_corso_annuale = function(req, res) {
    var start_array = null,
        end_array = null,
        product_id = null,
        contact_id = null,
        client_id = null;

    if ((req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if ((req.body.client_id !== undefined && utility.check_id(req.body.client_id))) {
        return res.status(422).send(utility.error422('client_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.client_id !== undefined && req.body.client_id !== '') {
        client_id = req.body.client_id;
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

    sequelize.query('SELECT scuolasci_disassocia_eventi_corsi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            contact_id + ',' +
            product_id + ',' +
            start_date + ',' +
            end_date +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {

            var cnt = 0;
            if(data && data[0] && data[0].scuolasci_disassocia_eventi_corsi && 
                data[0].scuolasci_disassocia_eventi_corsi.length > 0){

                    data[0].scuolasci_disassocia_eventi_corsi.forEach(function(ev) {
                    sequelize.query('SELECT scuolesci_delete_contact_event(' +
                            client_id + ',' +
                            ev.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {

                            if (cnt++ >= data[0].scuolasci_disassocia_eventi_corsi.length - 1) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'contact',
                                    'update',
                                    req.user.id,
                                    JSON.stringify('{}'),
                                    contact_id,
                                    'Maestro  ' + contact_id + ' disassociato dal bambino ' + client_id + ' per corso annuale ' + product_id
                                );
                                res.json({
                                    'data': {
                                        'type': 'events',
                                        'id': ev.id
                                    }
                                });
                            }

                        }).catch(function(err) {
                            return res.status(400).render('delete_event_contact_corsi: ' + err);
                        });
            });

            }

            


        }).catch(function(err) {
            return res.status(400).render('maestri-bambini disassocia corsi: ' + err);
        });
};