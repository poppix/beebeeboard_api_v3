var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js');

exports.registro_onorari = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null;
    var util = require('util');

    var sortField = (req.query.sort === undefined) ? '\'date\'' : '\'' + req.query.sort + '\'',
        all_any = (req.query.all_any === undefined) ? 'any' : req.query.all_any,
        sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + (req.query.direction) + '\'';

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT scuolasci_registro_onorari(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            sortDirection + ',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (req.query.filtro_fattura ? '\''+req.query.filtro_fattura+'\'' : '\'all\'')+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolasci_registro_onorari']);
            } else {
                res.json({});
            }

        });
};

exports.incassi_prodotto = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null,
        product = null;

    var util = require('util');

    var sortField = (req.query.sort === undefined) ? '\'date\'' : '\'' + req.query.sort + '\'',
        all_any = (req.query.all_any === undefined) ? 'any' : req.query.all_any,
        sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + (req.query.direction) + '\'';

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.product !== undefined && req.query.product !== '') {
        if (!utility.check_type_variable(req.query.product, 'string')) {
            return res.status(422).send(utility.error422('product', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        product = '\'' + req.query.product + '\'';
    }
    sequelize.query('SELECT scuolascimontebianco_incassi_prodotto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            sortDirection + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            product + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_incassi_prodotto']);
            } else {
                res.json({});
            }

        });
};

exports.statistiche_maestri = function(req, res) {

    
    var from_date = null,
        to_date = null;

    var util = require('util');

    
    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT scuolasci_statistiche_globali_maestri(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (req.query.maestro_id ? req.query.maestro_id : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolasci_statistiche_globali_maestri']);
            } else {
                res.json({});
            }

        });
};

exports.ore_prodotto = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null,
        product = null;

    var util = require('util');

    var sortField = (req.query.sort === undefined) ? '\'date\'' : '\'' + req.query.sort + '\'',
        all_any = (req.query.all_any === undefined) ? 'any' : req.query.all_any,
        sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + (req.query.direction) + '\'';

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.product !== undefined && req.query.product !== '') {
        if (!utility.check_type_variable(req.query.product, 'string')) {
            return res.status(422).send(utility.error422('product', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        product = '\'' + req.query.product + '\'';
    }
    sequelize.query('SELECT scuolascimontebianco_ore_prodotto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            sortDirection + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            product + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_ore_prodotto']);
            } else {
                res.json({});
            }

        });
};

exports.vendite_prodotto = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null,
        product = null;

    var util = require('util');

    var sortField = (req.query.sort === undefined) ? '\'date\'' : '\'' + req.query.sort + '\'',
        all_any = (req.query.all_any === undefined) ? 'any' : req.query.all_any,
        sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + (req.query.direction) + '\'';

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.product !== undefined && req.query.product !== '') {
        if (!utility.check_type_variable(req.query.product, 'string')) {
            return res.status(422).send(utility.error422('product', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        product = '\'' + req.query.product + '\'';
    }
    sequelize.query('SELECT scuolascimontebianco_vendite_prodotto(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            sortDirection + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            product + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_vendite_prodotto']);
            } else {
                res.json({});
            }

        });
};

exports.ore_prenotate_da_maestri = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null,
        maestro_id = null;

    var util = require('util');

    var sortField = (req.query.sort === undefined) ? '\'date\'' : '\'' + req.query.sort + '\'',
        all_any = (req.query.all_any === undefined) ? 'any' : req.query.all_any,
        sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + (req.query.direction) + '\'';

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        
        maestro_id = req.query.contact_id;
    }
    sequelize.query('SELECT scuolascimontebianco_ore_prenotate_maestro(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            sortDirection + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            maestro_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_ore_prenotate_maestro']);
            } else {
                res.json({});
            }

        });
};

exports.economic_situation = function(req, res) {

    var from_date = null,
        to_date = null,
        contact_id = null;

    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        contact_id = req.query.contact_id;
    }

    sequelize.query('SELECT scuolascimontebianco_debiti_crediti_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',\'asc\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            contact_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_debiti_crediti_v1']);
            } else {
                res.json({});
            }

        });
};

exports.events_not_payed = function(req, res) {

    var is_pagato = null,
        senza_maestro = null,
        senza_cliente = null,
        type_ = req.query.tipo;

    var response = {};

    utility.get_parameters_v1(req, 'event', function(err, parameters) {
        if (err) {
            return res.status(500).send(utility.error403('get parameters', 'Non hai i diritti', '500'));

        }

        if (req.query.sent !== undefined && req.query.sent !== '') {
            switch (parseInt(req.query.sent)) {
                case 0:
                    is_pagato = null;
                    break;

                case 1:
                    is_pagato = true;
                    break;

                case 2:
                    is_pagato = false;
                    break;

                default:
                    is_pagato = null;
                    break;
            }

        }

        if (req.query.gdpr !== undefined && req.query.gdpr !== '') {
            switch (parseInt(req.query.gdpr)) {
                case 0:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;

                case 1:
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                case 2:
                    senza_maestro = null;
                    senza_cliente = true;
                    break;

                default:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;
            }
        }


        async.parallel({
            event: function(callback) {
                if(type_ == 'events'){
                    sequelize.query('SELECT scuolascimontebianco_events_not_payed_v4(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        req.user.id + ',' +
                        req.user.role_id + ',null,' +
                        parameters.all_any + ',' +
                        (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                        (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                        is_pagato + ',' +
                        senza_maestro + ',' +
                        senza_cliente + ',' +
                        parameters.page_count + ',' +
                        (parameters.page_num - 1) * parameters.page_count + ',' +
                        parameters.sortDirection + ',' +
                        parameters.sortField + ','+
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT
                        })
                    .then(function(datas) {

                        sequelize.query('SELECT scuolascimontebianco_all_events_meta_v4(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                parameters.start_date + ',' +
                                parameters.end_date + ',' +
                                req.user.id + ',' +
                                req.user.role_id + ',null,' +
                                parameters.all_any + ',' +
                                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                is_pagato + ',' +
                                senza_maestro + ',' +
                                senza_cliente + ','+
                                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');')
                            .then(function(counter) {
                                if (datas && datas[0] && datas[0].scuolascimontebianco_events_not_payed_v4 && datas[0].scuolascimontebianco_events_not_payed_v4 != null) {
                                    response = {
                                        'data': datas[0].scuolascimontebianco_events_not_payed_v4,
                                        'meta': {
                                            'total_items': parseInt(counter[0][0].scuolascimontebianco_all_events_meta_v4.tot_items),
                                            'actual_page': parseInt(parameters.page_num),
                                            'total_pages': parseInt(counter[0][0].scuolascimontebianco_all_events_meta_v4.tot_items / parameters.page_count) + 1,
                                            'item_per_page': parseInt(parameters.page_count),
                                            'tot_all_number': parseInt(counter[0][0].scuolascimontebianco_all_events_meta_v4.tot_items_tutti)
                                        }
                                    };

                                    callback(null, response);
                                } else {

                                    response = {
                                        'data': [],
                                        'meta': {
                                            'total_items': parseInt(counter[0][0].scuolascimontebianco_all_events_meta_v4.tot_items),
                                            'actual_page': parseInt(parameters.page_num),
                                            'total_pages': parseInt(counter[0][0].scuolascimontebianco_all_events_meta_v4.tot_items / parameters.page_count) + 1,
                                            'item_per_page': parseInt(parameters.page_count),
                                            'tot_all_number': parseInt(counter[0][0].scuolascimontebianco_all_events_meta_v4.tot_items_tutti)
                                        }
                                    };
                                    callback(null, response);
                                }
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });



                    }).catch(function(err) {
                        callback(err, null);
                    });
                }
                else{
                    callback(null, {});
                }
                

            },
            collettive: function(callback) {
                if(type_ == 'projects'){
                    sequelize.query('SELECT scuolascimontebianco_projects_not_payed_v4(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            req.user.id + ',' +
                            req.user.role_id + ',null,' +
                            parameters.all_any + ',' +
                            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                            is_pagato + ',' +
                            senza_maestro + ',' +
                            senza_cliente + ',' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            parameters.sortDirection + ',' +
                            parameters.sortField + ','+
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(datas) {
                            sequelize.query('SELECT scuolascimontebianco_all_projects_meta_v4(' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    parameters.start_date + ',' +
                                    parameters.end_date + ',' +
                                    req.user.id + ',' +
                                    req.user.role_id + ',null,' +
                                    parameters.all_any + ',' +
                                    (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                    (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                    is_pagato + ',' +
                                    senza_maestro + ',' +
                                    senza_cliente + ','+
                                    (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');')
                                .then(function(counter) {
                                    if (datas && datas[0] && datas[0].scuolascimontebianco_projects_not_payed_v4 && datas[0].scuolascimontebianco_projects_not_payed_v4 != null) {
                                        response = {
                                            'data': datas[0].scuolascimontebianco_projects_not_payed_v4,
                                            'meta': {
                                                'total_items': parseInt(counter[0][0].scuolascimontebianco_all_projects_meta_v4.tot_items),
                                                'actual_page': parseInt(parameters.page_num),
                                                'total_pages': parseInt(counter[0][0].scuolascimontebianco_all_projects_meta_v4.tot_items / parameters.page_count) + 1,
                                                'item_per_page': parseInt(parameters.page_count),
                                                'tot_all_number': parseInt(counter[0][0].scuolascimontebianco_all_projects_meta_v4.tot_items_tutti)
                                            }
                                        };

                                        callback(null, response);
                                    } else {

                                        response = {
                                            'data': [],
                                            'meta': {
                                                'total_items': parseInt(counter[0][0].scuolascimontebianco_all_projects_meta.tot_items),
                                                'actual_page': parseInt(parameters.page_num),
                                                'total_pages': parseInt(counter[0][0].scuolascimontebianco_all_projects_meta.tot_items / parameters.page_count) + 1,
                                                'item_per_page': parseInt(parameters.page_count),
                                                'tot_all_number': parseInt(counter[0][0].scuolascimontebianco_all_projects_meta.tot_items_tutti)
                                            }
                                        };
                                        callback(null, response);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });

                        }).catch(function(err) {
                            callback(err, null);
                        });
                    }
                    else{
                        callback(null, {});
                    }
            }

        }, function(err, results) {
            if (err) {
                console.log(err);
                return res.status(500).send('Events_not_payed ' + err);
            }

            res.status(200).json({
                'events': results.event,
                'projects': results.collettive
            });
        });

    });
};



exports.events_not_payed_export = function(req, res) {
    var json2csv = require('json2csv');
    var is_pagato = null,
        senza_maestro = null,
        senza_cliente = null,
        type_ = req.query.tipo;

    var response = {};

    

    utility.get_parameters_v1(req, 'event', function(err, parameters) {
        if (err) {
            return res.status(500).send(utility.error403('get parameters', 'Non hai i diritti', '500'));

        }

        if (req.query.sent !== undefined && req.query.sent !== '') {
            switch (parseInt(req.query.sent)) {
                case 0:
                    is_pagato = null;
                    break;

                case 1:
                    is_pagato = true;
                    break;

                case 2:
                    is_pagato = false;
                    break;

                default:
                    is_pagato = null;
                    break;
            }

        }

        if (req.query.gdpr !== undefined && req.query.gdpr !== '') {
            switch (parseInt(req.query.gdpr)) {
                case 0:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;

                case 1:
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                case 2:
                    senza_maestro = null;
                    senza_cliente = true;
                    break;

                default:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;
            }
        }


        async.parallel({
            event: function(callback) {
                if(type_ == 'events'){
                    sequelize.query('SELECT scuolascimontebianco_events_not_payed_export(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        req.user.id + ',' +
                        req.user.role_id + ',null,' +
                        parameters.all_any + ',' +
                        (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                        (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                        is_pagato + ',' +
                        senza_maestro + ',' +
                        senza_cliente + ',' +
                        parameters.page_count + ',' +
                        (parameters.page_num - 1) * parameters.page_count + ',' +
                        parameters.sortDirection + ',' +
                        parameters.sortField + ','+
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT
                        })
                    .then(function(datas) {
                       
                        if(datas && datas[0] && datas[0].scuolascimontebianco_events_not_payed_export){
                            var fields_array = datas[0].scuolascimontebianco_events_not_payed_export[0]['attributes'][0]['fields'];
                            var fieldNames_array = datas[0].scuolascimontebianco_events_not_payed_export[0]['attributes'][0]['field_names'];
                           
                            
                            
                            json2csv({
                                data: datas[0].scuolascimontebianco_events_not_payed_export[0]['attributes'],
                                fields: fields_array,
                                fieldNames: fieldNames_array
                            }, function(err, csv) {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).send(err);
                                }
                                res.writeHead(200, {
                                    'Content-Type': 'text/csv',
                                    'Content-Disposition': 'attachment; filename=ore_lezione.csv'
                                });
                                res.end(csv);
                            });
                        }else
                        {
                            res.status(404).json({});
                        }



                    }).catch(function(err) {
                        callback(err, null);
                    });
                }
                else{
                    callback(null, {});
                }
                

            },
            collettive: function(callback) {
                if(type_ == 'projects'){
                    sequelize.query('SELECT scuolascimontebianco_projects_not_payed_export(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            req.user.id + ',' +
                            req.user.role_id + ',null,' +
                            parameters.all_any + ',' +
                            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                            is_pagato + ',' +
                            senza_maestro + ',' +
                            senza_cliente + ',' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            parameters.sortDirection + ',' +
                            parameters.sortField + ','+
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(datas) {
                            if(datas && datas[0] && datas[0].scuolascimontebianco_projects_not_payed_export){
                                var fields_array = datas[0].scuolascimontebianco_projects_not_payed_export[0]['attributes'][0]['fields'];
                                var fieldNames_array = datas[0].scuolascimontebianco_projects_not_payed_export[0]['attributes'][0]['field_names'];
                               
                              

                                json2csv({
                                    data: datas[0].scuolascimontebianco_projects_not_payed_export[0]['attributes'],
                                    fields: fields_array,
                                    fieldNames: fieldNames_array
                                }, function(err, csv) {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).send(err);
                                    }
                                    res.writeHead(200, {
                                        'Content-Type': 'text/csv',
                                        'Content-Disposition': 'attachment; filename=ore_lezione.csv'
                                    });
                                    res.end(csv);
                                });
                            }else
                            {
                                res.status(404).json({});
                            }
                

                        }).catch(function(err) {
                            callback(err, null);
                        });
                    }
                    else{
                        callback(null, {});
                    }
            }

        }, function(err, results) {
            if (err) {
                console.log(err);
                return res.status(500).send('Events_not_payed ' + err);
            }

           
        });

    });
};

exports.sci_club = function(req, res) {

    var from_date = null,
        to_date = null;


    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }


    sequelize.query('SELECT scuolascimontebianco_sci_club(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.status(200).json({
                    'ore': datas[0]['scuolascimontebianco_sci_club']
                });
            }
        }).catch(function(err) {
            return res.status(500).send('Ore sci club ' + err);
        });
};

exports.collettiva_stat = function(req, res) {
    var product_id = null,
        data_inizio = null,
        end_date = null,
        solo_start = null,
        color = null,
        totali = null;


    if (req.body.product_id !== undefined && req.body.product_id != '') {
        product_id = req.body.product_id;

    }

    if (req.body.totali !== undefined && req.body.totali != '') {
        totali = req.body.totali;

    }

    if (req.body.solo_start !== undefined && req.body.solo_start != '') {
        solo_start = req.body.solo_start;

    }

    if (req.body.color !== undefined && req.body.color != '') {
        color = '\''+req.body.color+'\'';
    }



    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolascimontebianco_collettive_stats_new(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\','+
            color+','+
            totali+','+
            solo_start+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.json({
                'data': data[0].scuolascimontebianco_collettive_stats_new
            });
        }).catch(function(err) {
            return res.status(400).render('collettiva_stat: ' + err);
        });
};

exports.cassa_method_day = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null;
    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT scuolascimontebianco_cassa_method_day(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_cassa_method_day']);
            } else {
                res.json({});
            }

        });
};

exports.cervino_cassa = function(req, res) {

    
    var from_date = null,
        to_date = null,
        contact_id = null;
    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        contact_id = req.query.contact_id;
    }

   sequelize.query('SELECT scuolascicervino_cassa(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',\'' +
            req.query.sort +'\','+
            req.user.id + ',' +
            req.user.role_id + ','+
            contact_id+','+
            req.query.acconto
            +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascicervino_cassa']);
            } else {
                res.json({});
            }

        });
};

exports.update_group_sms = function(req, res) {

   sequelize.query('SELECT group_sms(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',\'' +
        JSON.stringify(req.body) +'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
        if (datas) {
            res.json(datas[0]['group_sms']);
        } else {
            res.json({});
        }

    });
};

exports.assicurazione = function(req, res) {
    var from_date = null,
        to_date = null;
    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT scuolascimontebianco_assicurazione(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_assicurazione']);
            } else {
                res.json({});
            }

        });
};


exports.assicurazione_incassi = function(req, res) {
    var from_date = null,
        to_date = null;
    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT scuolascimontebianco_assicurazione_incassi(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',\'date\',' +
            req.user.id + ',' +
            req.user.role_id + ',null);', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_assicurazione_incassi']);
            } else {
                res.json({});
            }

        });
};

exports.maestri_bloccati = function(req, res) {
    var from_date = null,
        to_date = null;
    var util = require('util');

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT scuolascimontebianco_maestri_bloccati(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_maestri_bloccati']);
            } else {
                res.json({});
            }

        });
};

exports.calendario_gara_collettiva = function(req, res) {
    var data_inizio = null,
        end_date = null,
        giorno_esatto = null;
    var util = require('util');


    if (req.body.inizio !== undefined && req.body.inizio !== '') {
        if (!utility.check_type_variable(req.body.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.body.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.fine !== undefined && req.body.fine !== '') {
        if (!utility.check_type_variable(req.body.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.body.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.giorno !== undefined && req.body.giorno !== '') {
        if (!utility.check_type_variable(req.body.giorno, 'string')) {
            return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        giorno_esatto = req.body.giorno;
    } else {
        return res.status(422).send(utility.error422('giorno', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_gara_find_collettiva_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\',\'' +
            giorno_esatto + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(data) {
            res.json({
                'data': data[0].scuolasci_gara_find_collettiva_v1[0]
            });
        }).catch(function(err) {
            return res.status(400).render('find_collettiva: ' + err);
        });
};

exports.conto_stagione = function(req, res) {

    sequelize.query('SELECT scuolascimontebianco_debiti_crediti_totali(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.json(datas[0]['scuolascimontebianco_debiti_crediti_totali']);
            } else {
                res.json({});
            }

        });
};


exports.conto_stagione_csv = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    var is_pagato = null,
        senza_maestro = null,
        senza_cliente = null,
        type_ = req.query.tipo;

    var response = {};


    sequelize.query('SELECT scuolascimontebianco_debiti_crediti_totali_csv(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
             if (datas[0].scuolascimontebianco_debiti_crediti_totali_csv) {
                    var fields_array = datas[0].scuolascimontebianco_debiti_crediti_totali_csv[0]['fields'];
                    var fieldNames_array = datas[0].scuolascimontebianco_debiti_crediti_totali_csv[0]['field_names'];
                
                    json2csv({
                        data: datas[0].scuolascimontebianco_debiti_crediti_totali_csv,
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=conto_stagione.csv'
                        });
                        res.end(csv);
                    });
                } else
                    return res.status(422).send({});



            }).catch(function(err) {
                return res.status(500).send(err);
            });
};