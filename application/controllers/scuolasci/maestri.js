var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    utils = require('../../../utils'),
    config = require('../../../config');

/*
 * @apiUse IdParamEntryError
 */
exports.stats_maestri_workhours = function(req, res) {
    var contact_id = [],
        start_date = null;
    var util = require('util');

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        if (util.isArray(req.query.contact_id)) {
            contact_id = req.query.contact_id;
        } else {
            contact_id.push(req.query.contact_id);
        }
    }

    if (req.query.from_date != null && !moment(req.query.from_date).isValid()) {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    start_date = '\'' + req.query.from_date + '\'';

    if (req.query.to_date != null && !moment(req.query.to_date).isValid()) {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    end_date = '\'' + req.query.to_date + '\'';

    sequelize.query('SELECT scuolasci_maestri_stats_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',null,' +
            start_date + ',' +
            end_date + ',' +
            (contact_id.length > 0 ? 'ARRAY[' + contact_id + ']::bigint[]' : null) + ',' +
            req.user.role_id +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(counter) {
            res.json({
                'data': counter[0].scuolasci_maestri_stats_new[0],
                'start_date': req.query.from_date,
                'end_date': req.query.to_date
            });

        }).catch(function(err) {
            console.log(err);
            callback(err, null);
        });
};

exports.spunte_mancanti = function(req, res) {
    var start = null,
        end = null;

    if (req.query.start_date != null && req.query.start_date !== undefined){
        if(!moment(req.query.start_date).isValid()) {
         return res.status(422).send(utility.error422('start_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        else{
            start = '\'' + req.query.start_date + '\'';
        }
    }

    if (req.query.end_date != null && !moment(req.query.end_date).isValid()) {
        if(!moment(req.query.end_date).isValid()) {
         return res.status(422).send(utility.error422('end_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        else{
            end = '\'' + req.query.end_date + '\'';
        }
    }

    sequelize.query('SELECT scuolascimontebianco_spunte_mancanti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolascimontebianco_spunte_mancanti);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci spunte_mancanti: ' + err);
        });
};

exports.change_level = function(req, res) {


    sequelize.query('SELECT scuolasci_change_level(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+','+
            req.user.role_id+',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci Maestro Modifica Livello : ' + err);
        });
};

exports.ore_da_confermare = function(req, res) {


    sequelize.query('SELECT scuolasci_ore_da_confermare(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+','+
            req.user.role_id+',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].scuolasci_ore_da_confermare && datas[0].scuolasci_ore_da_confermare[0]){
                 res.status(200).send(datas[0].scuolasci_ore_da_confermare[0].events);
            }else
                res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci Ore da confermare : ' + err);
        });
};

exports.lezione_barcode = function(req, res) {
   
    sequelize.query('SELECT scuolasci_lezione_barcode(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+','+
            req.user.role_id+',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_lezione_barcode[0]);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolasci_lezione_barcode: ' + err);
        });
};


exports.togli_lezione_barcode = function(req, res) {
   
    sequelize.query('SELECT scuolasci_togli_lezione_barcode(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+','+
            req.user.role_id+',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_togli_lezione_barcode[0]);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolasci_togli_lezione_barcode: ' + err);
        });
};

exports.associa_barcode = function(req, res) {
   
    sequelize.query('SELECT scuolasci_associa_barcode(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+','+
            req.user.role_id+',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolasci_associa_barcode: ' + err);
        });
};

exports.ingresso_maestro = function(req, res) {
    var maestro_id = (req.body.maestro_id ? req.body.maestro_id : req.user.id);

    sequelize.query('SELECT scuolasci_maestro_in_new(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            maestro_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_maestro_in_new);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci Maestro Ingresso: ' + err);
        });
};

exports.uscita_maestro = function(req, res) {
    var maestro_id = (req.body.maestro_id ? req.body.maestro_id : req.user.id);

    sequelize.query('SELECT scuolasci_maestro_out_new(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            maestro_id + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_maestro_out_new);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci Maestro Uscita: ' + err);
        });
};

exports.find_maestri = function(req, res) {
    sequelize.query('SELECT scuolasci_find_maestri(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_find_maestri);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolasci_find_maestri: ' + err);
        });
};

exports.excel_bonifici = function(req, res) {
    var json2csv = require('json2csv');
    var start_date = null,
        end_date = null;

    if (!req.user.role[0].json_build_object.attributes.contact_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
    }

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = req.query.from_date;
    }


    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = req.query.to_date;
    }

        sequelize.query('SELECT scuolasci_excel_bonifici_maestri_new(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',\'' +
            start_date + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if(datas && datas[0] && datas[0].scuolasci_excel_bonifici_maestri_new){
                var fields_array = datas[0].scuolasci_excel_bonifici_maestri_new[0]['fields'];
                var fieldNames_array = datas[0].scuolasci_excel_bonifici_maestri_new[0]['field_names'];
                console.log(fields_array.length);
                console.log(fieldNames_array.length);
                json2csv({
                    data: datas[0].scuolasci_excel_bonifici_maestri_new,
                    fields: fields_array,
                    fieldNames: fieldNames_array
                }, function(err, csv) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=ore_maestri.csv'
                    });
                    res.end(csv);
                });
            }
            else
            {
                return res.status(422).send(utility.error422('Nessuna Ora Confermata nel periodo selezionato', 'Parametro non valido', 'Parametro non valido', '422'));
      
            }
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolasci_export_bonifici: ' + err);
        });      
};

exports.consultazione_ranking = function(req, res) {
    
    var start_date = null,
        end_date = null;

    if (!req.user.role[0].json_build_object.attributes.contact_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
    }

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = req.query.from_date;
    }


    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = req.query.to_date;
    }

        sequelize.query('SELECT scuolasci_excel_bonifici_maestri_new(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',\'' +
            start_date + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_excel_bonifici_maestri_new);
                
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolasci_export_bonifici: ' + err);
        });      
};


exports.corsi_annuali_csv = function(req, res) {
    var json2csv = require('json2csv');
     var product_id = null,
        data_inizio = null,
        end_date = null;


    if (req.query.id_corso !== undefined && req.query.id_corso != '') {
        product_id = req.query.id_corso;

    }

    if (req.query.inizio !== undefined && req.query.inizio !== '') {
        if (!utility.check_type_variable(req.query.inizio, 'string')) {
            return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        data_inizio = req.query.inizio;
    } else {
        return res.status(422).send(utility.error422('inizio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.fine !== undefined && req.query.fine !== '') {
        if (!utility.check_type_variable(req.query.fine, 'string')) {
            return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end_date = req.query.fine;
    } else {
        return res.status(422).send(utility.error422('fine', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolascimontebianco_collettive_stats_csv(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            product_id + ',\'' +
            data_inizio + '\',\'' +
            end_date + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
                if(datas && datas[0] &&  datas[0].scuolascimontebianco_collettive_stats_csv &&  datas[0].scuolascimontebianco_collettive_stats_csv[0]){
                    var fields_array = datas[0].scuolascimontebianco_collettive_stats_csv[0]['fields'];
                    var fieldNames_array = datas[0].scuolascimontebianco_collettive_stats_csv[0]['field_names'];
                    console.log(fields_array.length);
                    console.log(fieldNames_array.length);
                    json2csv({
                        data: datas[0].scuolascimontebianco_collettive_stats_csv,
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=corso_annuale.csv'
                        });
                        res.end(csv);
                    });
                }
                
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci scuolascimontebianco_collettive_stats_csv: ' + err);
        });      
};

exports.spunta_ore = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.workhour_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT scuolasci_spunta_ore_v1(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({'res':datas[0].scuolasci_spunta_ore_v1});
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci spunta_ore: ' + err);
        });
};

exports.spunta_ore_bulk = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.workhour_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    
    var item_ids = [];
    var util = require('util');

    if (req.body.ids !== undefined && req.body.ids != '' && req.body.ids != 'undefined') {
        if (util.isArray(req.body.ids)) {
            item_ids = req.body.ids;
        } else {
            item_ids.push(req.body.ids);
        }
    }

    sequelize.query('SELECT scuolasci_spunta_ore_bulk(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',' +
            req.user.id + ',' +
            (item_ids.length > 0 ? 'ARRAY[' + item_ids + ']::bigint[]' : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_spunta_ore_bulk);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci spunta_ore_bulk: ' + err);
        });
};

exports.ultimo_uscito = function(req, res) {

    sequelize.query('SELECT scuolasci_ultimo_uscito(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',' +
            req.user.id + ',' +
            req.body.maestro_id + ','+
            (req.body.snowboard ? req.body.snowboard : false)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_ultimo_uscito);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci ultimo_uscito: ' + err);
        });
};

exports.assegna_penality = function(req, res) {
    var maestro_id = [];
    var util = require('util');

     if (req.body.maestro_id !== undefined && req.body.maestro_id !== '') {
        if (util.isArray(req.body.maestro_id)) {
            maestro_id = req.body.maestro_id;
        } else {
            maestro_id.push(req.body.maestro_id);
        }
    }

    sequelize.query('SELECT scuolasci_assegna_penality(' +
            req.user._tenant_id + ',\'' +
            req.user.organization + '\',' +
            req.user.id + ',' +
            (maestro_id.length > 0 ? 'ARRAY[' + maestro_id + ']::bigint[]' : null) + ',' +
            (req.body.num_ore ? req.body.num_ore : 0) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send(datas[0].scuolasci_assegna_penality);
        }).catch(function(err) {
            return res.status(400).render('ScuolaSci assegna_penality: ' + err);
        });
};

exports.maestri_giorno = function(req, res) {

    var from_date = null,
        to_date = null,
        sort = '\'name\'',
        comprensorio = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.sort !== undefined && req.query.sort !== '') {

        sort = '\'' + req.query.sort + '\'';

    }

    if (req.query.comprensorio !== undefined && req.query.comprensorio !== '') {

        comprensorio = '\'' + req.query.comprensorio.replace(/'/g, "''''"); + '\'';

    }

    sequelize.query('SELECT scuolascimontebianco_maestri_giorno_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            sort + ',' +
            comprensorio + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas &&  datas[0] && datas[0]['scuolascimontebianco_maestri_giorno_new']) {
                res.status(200).json({
                    'ore': datas[0]['scuolascimontebianco_maestri_giorno_new']
                });
            }else{
                 res.status(200).json({
                    'ore': []
                });
            }
        }).catch(function(err) {
            return res.status(500).send('scuolascimontebianco_maestri_giorno ' + err);
        });
};

exports.simple_dashboard = function(req, res) {

    var from_date = null,
        to_date = null,
        sort = '\'name\'',
        comprensorio = null,
        contact_id = null,
        offset = 0,
        lingua_char = [],
        spec_char = [],
        sesso = null,
        only_dispo = null,
        product_id = null,
        dispo_ecommerce = null;

    if (req.query.gen !== undefined && req.query.gen !== '') {
        if (req.query.gen == 'maschio') {
            sesso = true;
        } else {
            sesso = false;
        }
    }

    if (req.query.lang !== undefined && req.query.lang !== '') {
        
        if (!utility.check_type_variable(req.query.lang, 'string')) {
            return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        

        var lang_array = req.query.lang.split(',');
        lang_array.forEach(function(s) {
            lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });
        
    }


    if (req.query.spec !== undefined && req.query.spec !== '') {
        
        if (!utility.check_type_variable(req.query.spec, 'string')) {
            return (null, utility.error422('spec', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        

        var spec_array = req.query.spec.split(',');
        spec_array.forEach(function(s) {
            spec_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });
        
    }

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.sort !== undefined && req.query.sort !== '') {

        sort = '\'' + req.query.sort + '\'';

    }

    if (req.query.comp !== undefined && req.query.comp !== '') {

        comprensorio = '\'' + req.query.comp.replace(/'/g, "''''") + '\'';

    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {

        contact_id = req.query.contact_id;

    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {

        product_id = req.query.product_id;

    }

    if (req.query.offset !== undefined && req.query.offset !== '') {

        offset = req.query.offset;

    }

    if (req.query.only_dispo !== undefined && req.query.only_dispo !== '') {

        only_dispo = req.query.only_dispo;

    }

    if (req.query.dispo_ecommerce !== undefined && req.query.dispo_ecommerce !== '') {

        dispo_ecommerce = req.query.dispo_ecommerce;

    }

    sequelize.query('SELECT scuolasci_simple_dashboard_calendar_new_v4(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        from_date + ',' +
        to_date + ',' +
        req.user.id + ',' +
        req.user.role_id + ',' +
        sort + ',' +
        comprensorio + ','+
        contact_id+','+
        offset +','+
        sesso + ',' +
        (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
        (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
        only_dispo +','+
        product_id+','+
        dispo_ecommerce+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
        if (datas &&  datas[0] && datas[0]['scuolasci_simple_dashboard_calendar_new_v4']) {

                utility.removeEmpty(datas[0]['scuolasci_simple_dashboard_calendar_new_v4'][0]);
                res.status(200).json(datas[0]['scuolasci_simple_dashboard_calendar_new_v4'][0]);
            }else{
                 res.status(200).json({
                    'contacts': []
                });
            }
    }).catch(function(err) {
        console.log(err);
        return res.status(400).render('ScuolaSci scuolasci_simple_dashboard_calendar: ' + err); 
    });

    
    
};


exports.maestro_report = function(req, res) {

    var from_date = null,
        to_date = null,
        comprensorio = null,
        maestro_id = null;



    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }


    if (req.query.comprensorio !== undefined && req.query.comprensorio !== '') {

        comprensorio = '\'' + req.query.comprensorio.replace(/'/g, "''''"); + '\'';

    }

    if (req.query.maestro_id !== undefined && req.query.maestro_id !== '') {

        maestro_id = req.query.maestro_id;

    }

    sequelize.query('SELECT scuolascimontebianco_maestro_report_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            comprensorio + ','+
            maestro_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas) {
                res.status(200).json({
                    'ore': datas[0]['scuolascimontebianco_maestro_report_new']
                });
            }
        }).catch(function(err) {
            return res.status(500).send('scuolascimontebianco_maestro_report_new ' + err);
        });
};

exports.timbratore = function(req, res) {

    var tag_maestro = req.params.id;

    if (tag_maestro !== undefined && tag_maestro != '') {
        sequelize.query('SELECT scuolasci_find_maestro_by_tag(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                tag_maestro.toUpperCase() + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if (datas && datas[0] && datas[0].scuolasci_find_maestro_by_tag && datas[0].scuolasci_find_maestro_by_tag[0]) {

                    var token = utils.uid(config.token.accessTokenLength);

                    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                        moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                        datas[0].scuolasci_find_maestro_by_tag[0].id + ',' +
                        5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(tok) {

                        sequelize.query('SELECT insert_login(' + datas[0].scuolasci_find_maestro_by_tag[0].id + ',\'' + req.headers['host'].split(".")[0] + '\',\'' + req.headers['user-agent'] + '\',\'' + req.connection.remoteAddress + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(logins) {
                            res.status(200).send({
                                'access_token': token,
                                'url': null
                            });
                        }).catch(function(err) {
                            return res.status(400).render('timbratore: ' + err);
                        });


                    }).catch(function(err) {
                        return res.status(400).render('timbratore: ' + err);
                    });
                } else {
                    res.status(422).send('Nessun maestro associato al tag ' + tag_maestro);
                }

            }).catch(function(err) {
                return res.status(500).send('scuolasci_find_maestro_by_tag ' + err);
            });
    }
};



exports.dispo_modificabile_da_maestro = function(req, res) {
    var util = require('util');
    var start_array = [],
        end_array = [],
        comprensorio = [],
        contact_id = [];


       if (req.body.from_date !== undefined && req.body.from_date != '') {
            start_array.push('\'' + req.body.from_date + '\'');
        }

        if (req.body.to_date !== undefined && req.body.to_date != '') {
            
                
                    end_array.push('\'' + req.body.to_date + '\'');
               
        };

        if (req.body.maestri_ids !== undefined && req.body.maestri_ids != '') {
            if (util.isArray(req.body.maestri_ids)) {
                req.body.maestri_ids.forEach(function(mae) {
                    contact_id.push(mae);
                });
            }
        };

        if (req.body.comprensori !== undefined && req.body.comprensori != '') {
            if (util.isArray(req.body.comprensori)) {
                req.body.comprensori.forEach(function(comp) {
                    comprensorio.push('\'' + comp + '\'');
                });
            }
        };


        sequelize.query('SELECT scuolasci_dispo_modificabile_da_maestro('+
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\','+
            req.user.id+',ARRAY[' +
            start_array + ']::character varying[],ARRAY[' + end_array + ']::character varying[],' +
            (comprensorio && comprensorio.length > 0 ? 'ARRAY[' + comprensorio + ']::text[]' : null) + ',' +
            (contact_id && contact_id.length > 0 ? 'ARRAY[' + contact_id + ']::bigint[]' : null) + ',\'' +
            req.body.operation+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].scuolasci_dispo_modificabile_da_maestro && datas[0].scuolasci_dispo_modificabile_da_maestro != null) {
                res.status(200).send({
                    'data': datas[0].scuolasci_dispo_modificabile_da_maestro
                });

            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('scuolasci_dispo_modificabile_da_maestro: ' + err);
        });
     
    
};


exports.fatt_riequilibrio = function(req, res) {

    var maestro_id = req.body.id_maestro;

    if (maestro_id !== undefined && maestro_id != '') {
        sequelize.query('SELECT scuolasci_calcolo_fattore_riequilibrio(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                maestro_id+ ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0]['scuolasci_calcolo_fattore_riequilibrio']) {
                    res.json(datas[0]['scuolasci_calcolo_fattore_riequilibrio']);
                } else {
                    res.status(422).send({});
                }

            }).catch(function(err) {
                return res.status(500).send('scuolasci_calcolo_fattore_riequilibrio ' + err);
            });
    }
};
