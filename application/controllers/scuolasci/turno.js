var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    finds = require('../../utility/finds.js'),
    logs = require('../../utility/logs.js'),
    skebby = require('../skebby.js'),
    config = require('../../../config');
    var mandrill = require('mandrill-api/mandrill');

exports.prenotazione = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    var contact_id = null,
        ritrovo = null,
        starts = [],
        ends = [],
        da_turno = null,
        n_persone = 0,
        maestro_id = null,
        comprensorio = null,
        notes = '',
        is_maestro = false,
        senza_maestro = false,
        product_id = null,
        lingua = [],
        spec = [],
        sesso = null;


    if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.data_prenotazione !== undefined && req.body.data_prenotazione !== '') {
        if (!utility.check_type_variable(req.body.data_prenotazione, 'string')) {
            return res.status(422).send(utility.error422('data_prenotazione', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.name !== undefined && req.body.name !== '') {
        if (!utility.check_type_variable(req.body.name, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        if (!utility.check_type_variable(req.body.comp, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if ((req.body.operatore_id !== undefined && utility.check_id(req.body.operatore_id))) {

        return res.status(422).send(utility.error422('operatore_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.operatore_id !== undefined && req.body.operatore_id !== '') {
        contact_id = req.body.operatore_id;
    }

    if ((req.body.product_id && req.body.product_id !== undefined && utility.check_id(req.body.product_id))) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }


    if (req.body.note !== undefined && req.body.note !== '') {
        notes = req.body.note.replace(/'/g, "''''");
    }

    if (req.body.filter_gen !== undefined && req.body.filter_gen !== '') {
        if (req.body.filter_gen == 'maschio') {
            sesso = true;
        } else {
            sesso = false;
        }
    }

  

   if (req.body.filter_lang !== undefined && req.body.filter_lang !== '') {
        lingua = utility.get_lang(req.body.filter_lang);
        
        
    }

    if (req.body.filter_spec !== undefined && req.body.filter_spec !== '') {
        
        spec = utility.get_specialita(req.body.filter_spec);
        
    }


    if (req.body.is_maestro !== undefined && req.body.is_maestro !== '') {
        is_maestro = req.body.is_maestro;
    }

    if (req.body.senza_maestro !== undefined && req.body.senza_maestro !== '') {
        senza_maestro = req.body.senza_maestro;
    } 
 
    if ((req.body.maestro_id !== undefined && utility.check_id(req.body.maestro_id))) {
        return res.status(422).send(utility.error422('maestro_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.maestro_id !== undefined && req.body.maestro_id !== '') {
        maestro_id = req.body.maestro_id;
    }

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.body.puntoRitrovo !== undefined && req.body.puntoRitrovo !== '') {

        ritrovo = '\'' + req.body.puntoRitrovo.replace(/'/g, "''''") + '\'';
    }

    if (comprensorio) {
        sequelize.query('SELECT scuolasci_crea_prenotazione_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',' +
                contact_id + ',\'' +
                req.body.name.replace(/'/g, "''''") + '\',\'' +
                req.body.data_prenotazione + '\',' +
                (req.body.da_turno && req.body.da_turno != '' ? req.body.da_turno : null )+ ',' +
                n_persone + ',' +
                req.body.ore + ',' +
                maestro_id + ',\'' +
                req.body.comp.replace(/'/g, "''''") + '\',' +
                product_id + ',\'' +
                notes + '\',' +
                is_maestro + ',' +
                senza_maestro + ',' +
                sesso + ',' +
                (spec.length > 0 ? 'ARRAY[' + spec + ']::bigint[]' : null) + ',' +
                (lingua.length > 0 ? 'ARRAY[' + lingua + ']::bigint[]' : null) + ','+
                ritrovo+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                console.log(datas);
                if (datas[0].scuolasci_crea_prenotazione_new != 1) {
                    return res.status(200).send({ 'data': { 'id': datas[0].scuolasci_crea_prenotazione_new } });
                } else
                    return res.status(422).send({});
            }).catch(function(err) {
                return res.status(400).render('crea prenotazione: ' + err);
            });
    } else {
        return res.status(422).send({});
    }
};

exports.preno_products = function(req, res) {
   
    sequelize.query('SELECT scuolasci_preno_products(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
           
            if (datas[0].scuolasci_preno_products && datas[0].scuolasci_preno_products.data) {
                return res.status(200).send(datas[0].scuolasci_preno_products.data);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('preno_products: ' + err);
        });
   
};

exports.parametri_turno = function(req, res) {
   
    sequelize.query('SELECT scuolasci_parametri_turno(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id +','+
            (req.query.contact_id ? req.query.contact_id : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
           
            if (datas[0].scuolasci_parametri_turno && datas[0].scuolasci_parametri_turno) {
                return res.status(200).send(datas[0].scuolasci_parametri_turno);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('scuolasci_parametri_turno: ' + err);
        });
   
};

exports.modifica_parametri_turno = function(req, res) {
   
    sequelize.query('SELECT scuolasci_modifica_parametri_turno(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id +',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
           
            if (datas[0].scuolasci_modifica_parametri_turno ) {
                res.status(200).send({ 'data': datas[0]['scuolasci_modifica_parametri_turno'] });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('scuolasci_modifica_parametri_turno: ' + err);
        });
   
};


exports.bonus_malus = function(req, res) {
   
    sequelize.query('SELECT scuolasci_bonus_malus(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id +',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
           
            if (datas[0].scuolasci_bonus_malus ) {
                res.status(200).send({ 'data': datas[0]['scuolasci_bonus_malus'] });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('scuolasci_bonus_malus: ' + err);
        });
   
};

exports.parameter = function(req, res) {
   
    sequelize.query('SELECT scuolasci_parameter(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id +',\''+
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
           
            if (datas[0].scuolasci_parameter ) {
                res.status(200).send({ 'data': datas[0]['scuolasci_parameter'] });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('scuolasci_parameter: ' + err);
        });
   
};

exports.conferma_prenotazione = function(req, res) {
    sequelize.query('SELECT scuolasci_conferma_prenotazione_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas && datas[0]['scuolasci_conferma_prenotazione_new']) {
                res.status(200).send({ 'data': datas[0]['scuolasci_conferma_prenotazione_new'] });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
};

exports.insert_bulk_family = function(req, res) {
    req.setTimeout(500000);

    sequelize.query('SELECT scuolasci_insert_bulk_family_for_preno_v1(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\', ' +
        req.user.id + ',' +
        req.body.project_id+','+
        req.body.persone+','+
        req.body.contact_id+','+
        req.body.product_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(datas) {
        if (datas && datas[0]['scuolasci_insert_bulk_family_for_preno_v1']) {
            res.status(200).send({ 'data': datas[0]['scuolasci_insert_bulk_family_for_preno_v1'] });
        } else {
            res.status(422).send({});
        }
    }).catch(function(err) {
        return res.status(500).send({});
    });
    
};

exports.prenotazione_gruppo = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    var contact_id = null,
        starts = [],
        ends = [],
        da_turno = null,
        n_persone = 0,
        maestro_id = null,
        lingua = null,
        comprensorio = null,
        product_id = null,
        ids = [];

    if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.maestro_ids !== undefined && req.body.maestro_ids != '') {
        if (util.isArray(req.body.maestro_ids)) {
            req.body.maestro_ids.forEach(function(start) {
                ids.push(start);
            });
        }
    }


    if (req.body.data_prenotazione !== undefined && req.body.data_prenotazione !== '') {
        if (!utility.check_type_variable(req.body.data_prenotazione, 'string')) {
            return res.status(422).send(utility.error422('data_prenotazione', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.name !== undefined && req.body.name !== '') {
        if (!utility.check_type_variable(req.body.name, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        if (!utility.check_type_variable(req.body.comp, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if ((req.body.operatore_id !== undefined && utility.check_id(req.body.operatore_id))) {
        return res.status(422).send(utility.error422('operatore_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.operatore_id !== undefined && req.body.operatore_id !== '') {
        contact_id = req.body.operatore_id;
    }

    if ((req.body.maestro_id !== undefined && utility.check_id(req.body.maestro_id))) {
        return res.status(422).send(utility.error422('maestro_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.maestro_id !== undefined && req.body.maestro_id !== '') {
        maestro_id = req.body.maestro_id;
    }

    if (req.body.product_id !== undefined && req.body.product_id !== '') {
        product_id = req.body.product_id;
    }

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (comprensorio) {
        sequelize.query('SELECT scuolasci_crea_prenotazione_gruppo_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',' +
                contact_id + ',\'' +
                (req.body.name && req.body.name !== null && utility.check_type_variable(req.body.name, 'string') ? req.body.name.replace(/'/g, "''''") : '') + '\',\'' +
                req.body.data_prenotazione + '\',' +
                req.body.da_turno + ',' +
                n_persone + ',' +
                req.body.ore + ',' +
                maestro_id + ',\'' +
                req.body.comp + '\',' +
                product_id + ','+
                (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null) +');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].scuolasci_crea_prenotazione_gruppo_new != 1) {
                    return res.status(200).send({ 'data': { 'id': datas[0].scuolasci_crea_prenotazione_gruppo_new } });
                } else
                    return res.status(422).send({});
            }).catch(function(err) {
                return res.status(400).render('crea prenotazione gruppo: ' + err);
            });
    } else {
        return res.status(422).send({});
    }
};

exports.get_turno = function(req, res) {

    var start_array = [],
        end_array = [],
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua = [],
        lingua_char = [],
        spec_char = [],
        product_id = null;


    if (req.body.timeslots) {

        (JSON.parse(req.body.timeslots)).forEach(function(timeslot) {
            start_array.push('\'' + timeslot.start_date.toString() + '\'');
            end_array.push('\'' + timeslot.end_date.toString() + '\'');
        });

        if (req.body.gen !== undefined && req.body.gen !== '') {
            if (req.body.gen == 'maschio') {
                sesso = true;
            } else {
                sesso = false;
            }
        }

        if (req.body.lang !== undefined && req.body.lang !== '') {
            
            if (!utility.check_type_variable(req.body.lang, 'string')) {
                return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
            }
            

            var lang_array = req.body.lang.split(',');
            lang_array.forEach(function(s) {
                lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
        
        }


        if (req.body.spec !== undefined && req.body.spec !== '') {
            
            if (!utility.check_type_variable(req.body.spec, 'string')) {
                return (null, utility.error422('spec', 'Parametro non valido', 'Parametro non valido', '422'));
            }
            

            var spec_array = req.body.spec.split(',');
            spec_array.forEach(function(s) {
                spec_char.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
            
        }


        if (req.body.product_id !== undefined && req.body.product_id !== '') {
            product_id = req.body.product_id;
        }

        sequelize.query('SELECT scuolasci_maestro_da_turno_new_v1(ARRAY[' +
                start_array + ']::character varying[],ARRAY[' + end_array + ']::character varying[],' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                sesso + ',' +
                (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
                req.body.comp.replace(/'/g, "''''") + '\',' +
                (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
                product_id+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuolasci_maestro_da_turno_new_v1 && datas[0].scuolasci_maestro_da_turno_new_v1 != null) {
                    res.status(200).send({
                        'data': datas[0].scuolasci_maestro_da_turno_new_v1
                    });

                } else {
                    res.status(422).send({});
                }
            }).catch(function(err) {
                return res.status(400).render('update turno: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

exports.cerca_maestro = function(req, res) {

    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
        if (!utility.check_type_variable(req.query.q, 'string')) {
            callback_(null, utility.error422('q', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        if (req.query.q !== undefined && req.query.q !== '') {

            let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
            var q_array = new_str.split(' ');
            q_array.forEach(function(s) {

                q1.push('\'%' + s.replace(/'/g, "''''") + '%\'');
            });
        }

        sequelize.query('SELECT scuolasci_cerca_maestro(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']' : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuolasci_cerca_maestro && datas[0].scuolasci_cerca_maestro != null) {
                    res.status(200).send({
                        'data': datas[0].scuolasci_cerca_maestro
                    });

                } else {
                    res.status(422).send({});
                }
            }).catch(function(err) {
                return res.status(400).render('cerca_maestro : ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

exports.update = function(req, res) {
    var riequilibrio = null,
        ore = null;

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.riequilibrio !== undefined && req.query.riequilibrio !== '') {
        riequilibrio = req.query.riequilibrio;
    } else {
        return res.status(422).send(utility.error422('riequilibrio', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.ore !== undefined && req.query.ore !== '') {
        ore = req.query.ore;
    } else {
        return res.status(422).send(utility.error422('ore', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT scuolasci_update_turno(' +
            req.params.id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user._tenant_id + ',' +
            riequilibrio + ',' +
            ore + ',' +
            req.user.role_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].scuolasci_update_turno && datas[0].scuolasci_update_turno != null) {
                return res.json({
                    'id': datas[0].scuolasci_update_turno
                });

            } else {
                return res.json({});
            }
        }).catch(function(err) {
            return res.status(400).render('update turno: ' + err);
        });
};

exports.dispo_sanitize = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.event_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var starts = [],
        ends = [],
        start_date = null,
        end_date = null,
        contact_id = null,
        add = null,
        comprensorio = null,
        motivo = null,
        status = null,
        notes = null,
        ids = [];


    var util = require('util');

    if (req.body.starts !== undefined && req.body.starts != '') {
        if (util.isArray(req.body.starts)) {
            req.body.starts.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.ends !== undefined && req.body.ends != '') {
        if (util.isArray(req.body.ends)) {
            req.body.ends.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    }

    if (req.body.contact_ids !== undefined && req.body.contact_ids != '') {
        if (util.isArray(req.body.contact_ids)) {
            req.body.contact_ids.forEach(function(end) {
                ids.push(end);
            });
        }
    }

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

    if ((req.body.add === undefined)) {
        return res.status(422).send(utility.error422('add', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.add !== undefined && req.body.add !== '') {
        add = req.body.add;
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        if (!utility.check_type_variable(req.body.comp, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.body.status !== undefined && req.body.status !== '') {

        status = req.body.status;
    }

    if (req.body.motivo !== undefined && req.body.motivo !== '') {

        motivo = '\'' + req.body.motivo.replace(/'/g, "''''") + '\'';
    }

    if (req.body.notes !== undefined && req.body.notes !== '') {

        notes = '\'' + req.body.notes.replace(/'/g, "''''") + '\'';
    }

    if (comprensorio) {
        sequelize.query('SELECT scuolasci_sanitize_dispo_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                contact_id + ',' +
                start_date + ',' +
                end_date + ',' +
                add + ',' +
                (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ',\'' +
                req.body.comp.replace(/'/g, "''''") + '\',' +
                (status ? status : null) + ' ,' +
                (motivo ? motivo : '\'\'') + ' ,' +
                (notes ? notes : '\'\'') + ',' +
                (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(data) {
                if (data && data[0] && data[0].scuolasci_sanitize_dispo_new) {
                    var result = JSON.stringify(data[0].scuolasci_sanitize_dispo_new);

                    console.log(result);
                    if (result.indexOf('"422"') == -1) {

                        var str_dates = '';
                        for (var i = 0; i < starts.length; i++) {
                            str_dates += starts[i].replace(/'/g, " ") + '-' + ends[i].replace(/'/g, " ") + ' ';
                        };

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'contact',
                            'update',
                            req.user.id,
                            JSON.stringify('{}'),
                            null,
                            (add === 'true' ? 'Aggiunta' : 'Tolta') + ' disponibilità a maestro ' + contact_id + ' '+ids+' date ' + str_dates
                        );

                        res.status(200).send({});
                    } else {
                        res.status(422).send({});
                    }
                } else {
                    res.status(422).send({});
                }

            }).catch(function(err) {
                return res.status(400).render('Sanitize Disponibility: ' + err);
            });
    } else {
        return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));

    }
};

exports.modifica_bulk = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.event_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var 
        start_date = null,
        end_date = null,
        contact_id = null,
        add = null,
        comprensorio = null;


    var util = require('util');

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

    if ((req.body.add === undefined)) {
        return res.status(422).send(utility.error422('add', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.add !== undefined && req.body.add !== '') {
        add = req.body.add;
    }

    if (req.body.comp !== undefined && req.body.comp !== '') {
        if (!utility.check_type_variable(req.body.comp, 'string')) {
            return res.status(422).send(utility.error422('name', 'Parametro non valido', 'Parametro non valido', '422'));
        }
    }

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (comprensorio) {
        sequelize.query('SELECT scuolasci_sanitize_dispo_bulk_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.role_id + ',' +
                req.user.id + ',' +
                contact_id + ',' +
                start_date + ',' +
                end_date + ',' +
                add + ',\'' +
                req.body.comp.replace(/'/g, "''''") + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(data) {
                if (data && data[0] && data[0].scuolasci_sanitize_dispo_bulk_new) {
                    var result = JSON.stringify(data[0].scuolasci_sanitize_dispo_bulk_new);
                    console.log(result);
                    if (result.indexOf('422') == -1) {


                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'contact',
                            'update',
                            req.user.id,
                            JSON.stringify('{}'),
                            null,
                            (add === 'true' ? 'Aggiunta' : 'Tolta') + ' disponibilità a maestro ' + contact_id + ' da ' + start_date.replace(/'/g, "") +' a '+end_date.replace(/'/g, "")
                        );

                        res.status(200).send({});
                    } else {
                        res.status(422).send({});
                    }
                } else {
                    res.status(422).send({});
                }

            }).catch(function(err) {
                return res.status(400).render('Sanitize Disponibility Bulk: ' + err);
            });
    } else {
        return res.status(422).send(utility.error422('comprensorio', 'Parametro non valido', 'Parametro non valido', '422'));

    }
};

exports.scuole_sci_cancella_dispo = function(req, res) {
    var starts = null,
        ends = null;

    if (!req.user.role[0].json_build_object.attributes.event_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.body.from_date + '\'';
    }

    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));

        }
        end_date = '\'' + req.body.to_date + '\'';
    }

   
    sequelize.query('SELECT scuolasci_elimina_dispo_bulk_new(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.role_id + ',' +
        req.user.id + ',' +
        start_date + ',' +
        end_date + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(data) {
        if (data && data[0] && data[0].scuolasci_elimina_dispo_bulk_new) {
             res.status(200).send({});
        } else {
            res.status(422).send({});
        }

    }).catch(function(err) {
        return res.status(400).render('Sanitize Dispo Bulk Destroy: ' + err);
    });
};

exports.scuole_sci_cancella_clienti = function(req, res) {
   
    if (!req.user.role[0].json_build_object.attributes.contact_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT scuolasci_elimina_clienti_bulk(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.role_id + ',' +
        req.user.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        })
    .then(function(data) {
        if (data && data[0] && data[0].scuolasci_elimina_clienti_bulk) {
             res.status(200).send({});
        } else {
            res.status(422).send({});
        }

    }).catch(function(err) {
        return res.status(400).render('Sanitize Clienti Bulk Destroy: ' + err);
    });
};

exports.ore_maestro = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.payment_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var from_date = null,
        to_date = null,
        contact_id = null;

    var util = require('util');

    var sortField = (req.query.sort === undefined) ? '\'date\'' : '\'' + req.query.sort + '\'',
        all_any = (req.query.all_any === undefined) ? 'any' : req.query.all_any,
        sortDirection = (req.query.direction === undefined) ? '\'desc\'' : '\'' + (req.query.direction) + '\'';

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        from_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        to_date = '\'' + req.query.to_date + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {
        contact_id = (req.query.contact_id);
    }

    sequelize.query('SELECT scuolascimontebianco_ore_maestri(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            from_date + ',' +
            to_date + ',' +
            sortDirection + ',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            contact_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas.length > 0) {
                res.json(datas[0]['scuolascimontebianco_ore_maestri']);
            } else {
                res.json({});
            }

        });
};

exports.situazione_maestri = function(req, res) {

    var start_date = null,
        end_date = null,
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua_char = [],
        spec_char = [],
        lingua = [];

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    if (req.body.gen !== undefined && req.body.gen !== '') {
        if (req.body.gen == 'maschio') {
            sesso = true;
        } else {
            sesso = false;
        }
    }

    if (req.body.from_date !== undefined && req.body.from_date !== '') {
        if (!moment(req.body.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.body.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = moment_timezone.tz(req.body.from_date, 'YYYY-MM-DDThh:mm:ssZ', 'Europe/Rome').format();
    }


    if (req.body.to_date !== undefined && req.body.to_date !== '') {
        if (!moment(req.body.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.body.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = moment_timezone.tz(req.body.to_date, 'YYYY-MM-DDThh:mm:ssZ', 'Europe/Rome').format();
    }

    if (req.body.comp !== undefined && req.body.comp !== '' && req.body.comp !== 'tutti') {

        comprensorio = '\'' + req.body.comp.replace(/'/g, "''''") + '\'';
    }

    if (req.body.lang !== undefined && req.body.lang !== '') {
        
        if (!utility.check_type_variable(req.body.lang, 'string')) {
            return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        

        var lang_array = req.body.lang.split(',');
        lang_array.forEach(function(s) {
            lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });
        
    }


    if (req.body.spec !== undefined && req.body.spec !== '') {
        
        if (!utility.check_type_variable(req.body.spec, 'string')) {
            return (null, utility.error422('spec', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        

        var spec_array = req.body.spec.split(',');
        spec_array.forEach(function(s) {
            spec_char.push('\'' + s.replace(/'/g, "''''") + '\'');
        });
        
    }

    console.log(start_date);
    console.log(end_date);

    var start = moment(req.body.from_date, 'YYYY-MM-DDThh:mm:ssZ');
    var end = moment(req.body.to_date, 'YYYY-MM-DDThh:mm:ssZ');

    var days = end.diff(start, 'days');



    sequelize.query('SELECT scuolasci_situazione_maestri_v1(\'' +
            start_date + '\',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            sesso + ',' +
            (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
            comprensorio + ',' +
            (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
            days + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].scuolasci_situazione_maestri_v1 && datas[0].scuolasci_situazione_maestri_v1 != null) {
                res.status(200).json(datas[0].scuolasci_situazione_maestri_v1[0]);

            } else {
                res.status(200).send({
                    'data': []
                });
            }
        }).catch(function(err) {
            return res.status(400).render('situazione_maestri: ' + err);
        });
};

exports.cambia_maestro = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var Sequelize = require('sequelize'),
        event_id = req.body.event_id,
        contact_id = req.body.contact_id,
        operation = req.body.operation;

    if (operation == 'add' && event_id && contact_id) {
        sequelize.query('SELECT scuolasci_cambio_maestro_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                event_id + ',' +
                contact_id + ',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                console.log(datas[0].scuolasci_cambio_maestro_new);
                if (datas[0].scuolasci_cambio_maestro_new === '1') {
                    res.status(200).send({});
                } else {
                    res.status(422).send({});
                }

            }).catch(function(err) {
                return res.status(400).render('scuolasci_cambio_maestro_new: ' + err);
            });
    } else {
        res.status(422).send({});
    }
};

exports.sposta_evento = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    var Sequelize = require('sequelize'),
        event_id = req.body.event_id,
        contact_id = req.body.contact_id,
        from_date = '\'' + req.body.start_date + '\'',
        to_date = '\'' + req.body.end_date + '\'',
        comp = '\'' + req.body.comp.replace(/'/g, "''''") + '\'',
        partecipanti = [],
        fixed_price = req.body.fixed_price ? req.body.fixed_price : false,
        price = req.body.price ? req.body.price : null,
        punto_ritrovo = req.body.punto_ritrovo ? '\'' + req.body.punto_ritrovo + '\'' : null,
        title = null,
        da_turno = null;





    if (req.body.partecipanti !== undefined && req.body.partecipanti != '') {
        if (util.isArray(req.body.partecipanti)) {
            var filtered = req.body.partecipanti.filter(function (el) {
              return el != null && el!= undefined && el!= '';
            });

            partecipanti = filtered;
        } else {
            partecipanti.push(req.body.partecipanti);
        }
    }

    if (req.body.title !== undefined && req.body.title != '') {
        var splitted = req.body.title.split(' - ')[0];
        
        title = '\'' + partecipanti.length +'P' + req.body.title.replace(/'/g, "''''").substring(splitted.length) + '\'';
        
    }


    if (event_id) {
        sequelize.query('SELECT scuolasci_sposta_evento_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                event_id + ',' +
                from_date + ',' +
                to_date + ',' +
                (req.body.contact_id ? req.body.contact_id : null) + ',' +
                req.user.id + ',' +
                comp + ',' +
                (partecipanti.length > 0 ? 'ARRAY[' + partecipanti + ']::bigint[]' : null) + ',' +
                fixed_price + ',' +
                price + ',' +
                title + ',' +
                punto_ritrovo + ',' +
                (req.body.da_turno ? req.body.da_turno : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                console.log(datas[0].scuolasci_sposta_evento_new);
                if (datas[0].scuolasci_sposta_evento_new === '1') {
                    res.status(200).send({});
                } else {
                    if (datas[0].scuolasci_sposta_evento_new === '2') {
                        res.status(422).send(utility.error422('event_id', 'Lezione già confermata dal maestro', 'Lezione già confermata dal maestro', '422'));
                    } else {
                        res.status(422).send(utility.error422('event_id', 'Non c\'è disponibilità per la modifica dell\'evento', 'Non c\'è disponibilità per la modifica dell\'evento', '422'));
                    }
                }

            }).catch(function(err) {
                return res.status(500).send('scuolasci_sposta_evento: ' + err);
            });
    } else {
        res.status(422).send({});
    }
};

exports.copia_evento = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    var Sequelize = require('sequelize'),
        event_id = req.body.event_id,
        starts = [],
        ends = [],
        maestro_id = null;

    if(req.body.maestro_id !== undefined && req.body.maestro_id != ''){
        maestro_id = req.body.maestro_id;
    }

   if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    };


    if (event_id) {
        sequelize.query('SELECT scuolasci_copia_evento_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                event_id + ',' +
                (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ','+
                maestro_id+',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].scuolasci_copia_evento_new === '1') {
                    res.status(200).send({});
                } else {       
                    res.status(422).send(utility.error422('event_id', 'Non c\'è disponibilità per la modifica dell\'evento', 'Non c\'è disponibilità per la modifica dell\'evento', '422'));
                }

            }).catch(function(err) {
                return res.status(400).render('scuolasci_copia_evento: ' + err);
            });
    } else {
        res.status(422).send({});
    }
};

exports.events_calendar = function(req, res) {
    var start_date = null,
        end_date = null,
        comprensorio = null,
        contact_id = null,
        product_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');


    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.from_date == 'Invalid date') {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start_date = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date, 'YYYY-MM-DDThh:mm:ssZ').isValid() || req.query.to_date == 'Invalid date') {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end_date = '\'' + req.query.to_date + '\'';
    }


    if (req.query.q !== undefined && req.query.q !== '') {

        comprensorio = '\'' + req.query.q + '\'';
    }

    if (req.query.contact_id !== undefined && req.query.contact_id !== '') {

        contact_id = req.query.contact_id;
    }

    if (req.query.product_id !== undefined && req.query.product_id !== '') {

        product_id = req.query.product_id;
    }





    sequelize.query('SELECT scuolasci_events_calendar_new_v1(' +
            req.user._tenant_id + ',' +
            comprensorio + ',' +
            start_date + ',' +
            end_date + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            contact_id + ',' +
            product_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].scuolasci_events_calendar_new_v1 && datas[0].scuolasci_events_calendar_new_v1 != null) {
                res.status(200).send({
                    'data': datas[0].scuolasci_events_calendar_new_v1
                });

            } else {
                res.status(200).send({
                    'data': []
                });
            }
        }).catch(function(err) {
            return res.status(400).render('events_calendar: ' + err);
        });
};

exports.penality = function(req, res) {
    var moment = require('moment');

    /*sequelize.query('SELECT scuolasci_all_penality_for_day(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(7, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(7, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(7, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(8, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(8, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(8, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(8, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(9, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(9, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(9, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(9, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(10, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(10, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(10, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(10, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(11, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(11, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(11, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(11, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(12, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(12, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(12, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(12, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(13, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(13, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(13, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(13, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(14, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(14, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(14, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(14, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(15, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(15, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(15, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(15, 'hours').add(30, 'minutes').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(16, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' - Scuola Sci Penalita del ' + moment(new Date()).add(-1, 'days').format('YYYY-MM-DD'));
            res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('scuolasci_penalita: ' + err);
        });*/


        sequelize.query('SELECT scuolasci_penality_new(\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(7, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\',\'' +
            moment(new Date()).startOf('day').add(-1, 'days').add(17, 'hours').format('YYYY-MM-DD HH:mm:ss') + '\','+
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',null);', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' - Scuola Sci Penalita del ' + moment(new Date()).add(-1, 'days').format('YYYY-MM-DD'));
            res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('scuolasci_penalita: ' + err);
        });
};

exports.promemoria_private = function(req, res) {
    var moment = require('moment');
     sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {

                sequelize.query('SELECT scuolasci_promemoria_private(\'' +
                    moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + '\','+
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {
                    console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss') + ' - Scuola Sci Promemoria del ' + moment(new Date()).format('YYYY-MM-DD'));
                    
                    if(datas && datas[0] && datas[0].scuolasci_promemoria_private && datas[0].scuolasci_promemoria_private[0] && datas[0].scuolasci_promemoria_private[0].mails){
                        var globals = datas[0].scuolasci_promemoria_private[0].info;
                        var mails = datas[0].scuolasci_promemoria_private[0].mails;
                        var merge_vars = datas[0].scuolasci_promemoria_private[0].data;
                        var contact_ids = datas[0].scuolasci_promemoria_private[0].ids;

                        var template_content = [{
                            "name": "LANG",
                            "content": globals.lang
                        }, {
                            "name": "ORGANIZATION",
                            "content": globals.org_name
                        },{
                            "name": "COLOR",
                            "content": globals.color
                        },{
                            "name": "TITLE",
                            "content": globals.title
                        },{
                            "name": "LOGO",
                            "content": globals.thumbnail_url ? 'https://data.beebeeboard.com/' + globals.thumbnail_url + '-small.jpeg' : null
                        }],
                        message = {
                            "merge_vars": merge_vars,
                            "global_merge_vars": template_content,
                            "merge_language": "handlebars",
                            "preserve_recipients": false,
                            "subject": globals.subject,
                            "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                            "from_name": globals.org_name,
                            "to": mails,
                            "headers": {
                                "Reply-To": globals.reply ? globals.reply : 'no-reply@beebeemailer.com'
                            },
                            "tags": ["beebeeboard-promemoria-privata", req.headers['host'].split(".")[0]],
                            "track_opens": true,
                            "tracks_clicks":true,
                            "metadata": {
                                "website": 'beebeeboard.com'
                            }
                        };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": globals.template_private ? globals.template_private : "scuolesci-mail-private",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Mail sended correctly");

                                 sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',ARRAY['+
                                        contact_ids+']::bigint[],' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (globals.subject ? globals.subject.replace(/'/g, "''''") : 'Promemoria Private ') + '\',' +
                                        '\'' + (globals.title ? globals.title.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                             useMaster: true
                                        })
                                    .then(function(datas) {


                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                        res.status(200).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });

                            } else if(result && result[0]) {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                sequelize.query('SELECT insert_mandrill_send(' +
                                        req.user._tenant_id + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',ARRAY['+
                                        contact_ids+']::bigint[],' +
                                        req.user.id + ',\'' +
                                        result[0].status + '\',\'' +
                                        result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + (globals.subject ? globals.subject.replace(/'/g, "''''") : 'Promemoria private')  + '\',' +
                                        '\'' + (globals.title ? globals.title.replace(/'/g, "''''") : '') + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                              useMaster: true
                                        })
                                    .then(function(datas) {
                                         console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail sended saved');


                                        res.status(422).send({});


                                    }).catch(function(err) {
                                        return res.status(400).render('mandrill send mail: ' + err);
                                    });
                            }
                         }).catch(function(err) {
                            return res.status(400).render('mandrill send mail: ' + err);
                        });

                    }
                    else{
                         res.status(422).send({});
                    }

                   

                }).catch(function(err) {
                    return res.status(400).render('scuolasci_promemoria_private: ' + err);
                });
         }).catch(function(err) {
                    return res.status(400).render('search_organization: ' + err);
                });
};

exports.stampa_tagliando = function(req, res) {
    var util = require('util');
    var https = require('https');

    var mandrill = require('mandrill-api/mandrill');
    var wrap = req.body.wrap ? req.body.wrap : null;
    var pdf = req.body.pdf ? req.body.pdf : null;
    var token = req.body.token ? req.body.token : null;
    var project_id = req.body.project_id ? req.body.project_id : null;
    var partecipante_id = req.body.partecipante_id ? req.body.partecipante_id : null;
    var completo  = req.body.completo ? req.body.completo : null;
    var event_ids = [];
    var isHotel = req.body.isHotel ? req.body.isHotel : null;
    var hotelName = req.body.hotelName ? req.body.hotelName : null;
    var collettiva_all = req.body.collettiva_all ? req.body.collettiva_all : null;
    var type_tagliando = req.body.type_tagliando ? '\''+req.body.type_tagliando+'\'' : null;

    var sendMailTo = req.body.send_mail_to ? '\''+req.body.send_mail_to+'\'' : null;
   

     sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {
               
                if (util.isArray(req.body.event_ids)) {
                    req.body.event_ids.forEach(function(start) {
                        event_ids.push('\'' + start + '\'');
                    });
                }
                
                sequelize.query('SELECT scuolasci_tagliandi(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\','+
                    project_id+','+
                    partecipante_id+','+
                    req.user.id+','+
                    (event_ids.length > 0 ? 'ARRAY[' + event_ids + ']::bigint[]' : null) +','+
                    type_tagliando+','+
                    collettiva_all+');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                .then(function(datas) {
                     //console.log('https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?wrap='+wrap+'&pdf='+pdf+'&hotelName='+hotelName+'&isHotel='+isHotel+'&event_id='+event_ids+'&token='+token+'&organization='+req.headers['host'].split(".")[0]+'&project_id='+project_id+'&partecipante_id='+partecipante_id+'&completo='+completo);
                   // res.redirect('https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?wrap='+wrap+'&pdf='+pdf+'&hotelName='+hotelName+'&isHotel='+isHotel+'&event_id='+event_ids+'&token='+token+'&organization='+req.headers['host'].split(".")[0]+'&project_id='+project_id+'&partecipante_id='+partecipante_id+'&completo='+completo);
                    if(pdf){
                      var url = 'https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?wrap='+wrap+'&pdf='+pdf+'&hotelName='+hotelName+'&isHotel='+isHotel+'&event_id='+event_ids+'&token='+token+'&organization='+req.headers['host'].split(".")[0]+'&project_id='+project_id+'&partecipante_id='+partecipante_id+'&completo='+completo;
                     
                    }else
                    {
                        var url = 'https://asset.beebeeboard.com/asset_scuole_sci/tagliando.php?wrap='+wrap+'&hotelName='+hotelName+'&isHotel='+isHotel+'&event_id='+event_ids+'&token='+token+'&organization='+req.headers['host'].split(".")[0]+'&project_id='+project_id+'&partecipante_id='+partecipante_id+'&completo='+completo;
                    
                    }
                    console.log(url);
                    var request = https.get(url, function(result) {

                        var chunks = [];

                        result.on('data', function(chunk) {
                            chunks.push(chunk);
                        });

                        result.on('end', function() {
                            var pdfData = new Buffer.concat(chunks);

                            if (sendMailTo) {
                                var addresses = req.body.send_mail_to.split(',');
                                var address_string = '';
                                var mails = [];
                                addresses.forEach(function(address) {
                                    address_string += ' ' + address;
                                    var mail = {
                                        "email": address,
                                        "name": null,
                                        "type": "to"
                                    };
                                    mails.push(mail);
                                });

                                if (req.body.sendMailCc) {
                                    addresses = req.body.sendMailCc.split(',');
                                    addresses.forEach(function(address) {
                                        address_string += ' ' + address;
                                        var mail = {
                                            "email": address,
                                            "name": null,
                                            "type": "bcc"
                                        };
                                        mails.push(mail);
                                    });
                                }


                                var contact_name = req.body.contact_name ? req.body.contact_name : null;

                                var template_content = [{
                                        "name": "LANG",
                                        "content": req.body.lang
                                    }, {
                                        "name": "CLIENTE",
                                        "content": contact_name
                                    }, {
                                        "name": "ORGANIZATION",
                                        "content": req.body.organization_name
                                    }, {
                                        "name": "TEXT",
                                        "content": conferma_prenotazione_scuola(req.headers['host'].split(".")[0])
                                    }, {
                                        "name": "ENGLISHTEXT",
                                        "content": conferma_prenotazione_scuola_english(req.headers['host'].split(".")[0])
                                    },{
                                        "name": "COLOR",
                                        "content": req.body.color ? req.body.color : '#00587D'
                                    }, {
                                        "name": "LOGO",
                                        "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                                    }],
                                    message = {
                                        "global_merge_vars": template_content,
                                        "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                                        "from_name": req.body.organization_name,
                                        "to": mails,
                                        "headers": {
                                            "Reply-To": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                                        },
                                        "tags": ["beebeeboard-tagliando", req.headers['host'].split(".")[0]],
                                        "subaccount": "beebeeboard",
                                        "tracking_domain": "mandrillapp.com",
                                        "google_analytics_domains": ["poppix.it"],
                                        "metadata": {
                                            "organization": req.headers['host'].split(".")[0]
                                        },
                                         "attachments": [{
                                            'type': 'application/pdf',
                                            'name': 'Tagliando Prenotazione.pdf',
                                            'content': pdfData.toString('base64')
                                        }]
                                    };

                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                mandrill_client.messages.sendTemplate({
                                    "template_name":  "tagliando-send-personalizzato",
                                    "template_content": template_content,
                                    "message": message,
                                    "async": true,
                                    "send_at": false
                                }, function(result) {

                                    if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                        console.log("Mail sended correctly");

                                         sequelize.query('SELECT insert_mandrill_send(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',ARRAY['+
                                                datas[0].scuolasci_tagliandi+']::bigint[],' +
                                                req.user.id + ',\'' +
                                                result[0].status + '\',\'\',\'' + result[0]._id + '\',\'' + (req.body.object ? req.body.object.replace(/'/g, "''''") : 'Invio Tagliando Prenotazione '+project_id) + '\',' +
                                                '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                     useMaster: true
                                                })
                                            .then(function(datas) {


                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                                res.status(200).send({});


                                            }).catch(function(err) {
                                                return res.status(400).render('mandrill send mail: ' + err);
                                            });

                                    } else if(result && result[0]) {
                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                        sequelize.query('SELECT insert_mandrill_send(' +
                                                req.user._tenant_id + ',\'' +
                                                req.headers['host'].split(".")[0] + '\',ARRAY['+
                                                datas[0].scuolasci_tagliandi+']::bigint[],' +
                                                req.user.id + ',\'' +
                                                result[0].status + '\',\'' +
                                                result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + (req.body.object ? req.body.object.replace(/'/g, "''''") : 'Invio Tagliando Prenotazione '+project_id)  + '\',' +
                                                '\'' + (req.body.text ? req.body.text.replace(/'/g, "''''") : '') + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                      useMaster: true
                                                })
                                            .then(function(datas) {
                                                 console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Error Mandrill mail sended saved');


                                                res.status(422).send({});


                                            }).catch(function(err) {
                                                return res.status(400).render('mandrill send mail: ' + err);
                                            });
                                    }
                                 });
                            }else{
                                
                                res.writeHead(200, {
                                    'Content-Length': pdfData.length
                                });

                                res.end(pdfData);
                            }
                  
                        });
                    }).on('error', function(err) {

                        return false;
                    });

                    request.end();

                }).catch(function(err) {
                    return res.status(400).render('scuolasci_tagliandi: ' + err);
                });
    }).catch(function(err) {
        return res.status(400).render('search_organization: ' + err);
    });
};


exports.tagliandi = function(req, res) {

    var project_id = req.body.project_id ? req.body.project_id : null;
    var partecipante_id = req.body.partecipante_id ? req.body.partecipante_id : null;
   
    
    sequelize.query('SELECT scuolasci_get_tagliandi(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\','+
        project_id+','+
        partecipante_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(datas) {
         
        res.status(200).send(datas[0].scuolasci_get_tagliandi);
    }).catch(function(err) {
        return res.status(400).render('scuolasci_get_tagliandi: ' + err);
    });
};


exports.get_turno = function(req, res) {

    var start_array = [],
        end_array = [],
        sesso = null,
        spec = [],
        comprensorio = null,
        lingua = [],
        lingua_char = [],
        spec_char = [],
        product_id = null;

     

    if (req.body.timeslots) {



        (JSON.parse(req.body.timeslots)).forEach(function(timeslot) {

            start_array.push('\'' + timeslot.start_date.toString() + '\'');
            end_array.push('\'' + timeslot.end_date.toString() + '\'');
        });

        if (req.body.gen !== undefined && req.body.gen !== '') {
            if (req.body.gen == 'maschio') {
                sesso = true;
            } else {
                sesso = false;
            }
        }

        if (req.body.product_id !== undefined && req.body.product_id !== '') {
            product_id = req.body.product_id;
        }


        if (req.body.lang !== undefined && req.body.lang !== '') {
            
            if (!utility.check_type_variable(req.body.lang, 'string')) {
                return (null, utility.error422('lang', 'Parametro non valido', 'Parametro non valido', '422'));
            }
            

            var lang_array = req.body.lang.split(',');
            lang_array.forEach(function(s) {
                lingua_char.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
            
        }

        if (req.body.spec !== undefined && req.body.spec !== '') {
            
            if (!utility.check_type_variable(req.body.spec, 'string')) {
                return (null, utility.error422('spec', 'Parametro non valido', 'Parametro non valido', '422'));
            }
            

            var spec_array = req.body.spec.split(',');
            spec_array.forEach(function(s) {
                spec_char.push('\'' + s.replace(/'/g, "''''") + '\'');
            });
            
        }

        if(req.user && req.user.role_id == 100){
            /* TURNO RICHIESTO DAL MAESTRO */
            sequelize.query('SELECT scuolasci_maestro_da_turno_maestro(ARRAY[' +
                start_array + ']::character varying[],ARRAY[' + end_array + ']::character varying[],' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                sesso + ',' +
                (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
                (req.body.comp ? '\'' +req.body.comp.replace(/'/g, "''''") + '\'' : null)+',' +
                (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
                product_id+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuolasci_maestro_da_turno_maestro && datas[0].scuolasci_maestro_da_turno_maestro != null) {
                    res.status(200).send({
                        'data': datas[0].scuolasci_maestro_da_turno_maestro
                    });

                } else {
                    res.status(422).send({});
                }
            }).catch(function(err) {
                return res.status(400).render('maestro da turno maestro: ' + err);
            });

        }else
        {
            sequelize.query('SELECT scuolasci_maestro_da_turno_new_v1(ARRAY[' +
                start_array + ']::character varying[],ARRAY[' + end_array + ']::character varying[],' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                sesso + ',' +
                (spec_char && spec_char.length > 0 ? 'ARRAY[' + spec_char + ']::text[]' : null) + ',' +
                (req.body.comp ? '\'' +req.body.comp.replace(/'/g, "''''") + '\'' : null)+',' +
                (lingua_char && lingua_char.length > 0 ? 'ARRAY[' + lingua_char + ']::text[]' : null) + ',' +
                product_id+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {
                if (datas && datas[0] && datas[0].scuolasci_maestro_da_turno_new_v1 && datas[0].scuolasci_maestro_da_turno_new_v1 != null) {
                    res.status(200).send({
                        'data': datas[0].scuolasci_maestro_da_turno_new_v1
                    });

                } else {
                    res.status(422).send({});
                }
            }).catch(function(err) {
                return res.status(400).render('maestro da turno:  ' + err);
            });

        }
        
    } else {
        res.json({
            'data': []
        });
    }
};

exports.get_prenotazione = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.project_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT scuolasci_get_prenotazione_data(' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',' +
                    req.params.id + ',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {

                    if (datas[0].scuolasci_get_prenotazione_data && datas[0].scuolasci_get_prenotazione_data != null) {
                        callback(null, datas[0].scuolasci_get_prenotazione_data[0]);

                    } else {

                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });

        },
        included: function(callback) {
            sequelize.query('SELECT scuolasci_get_prenotazione_included(' +
                    req.params.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.id + ',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(included) {

                    if (included[0].scuolasci_get_prenotazione_included && included[0].scuolasci_get_prenotazione_included != null) {
                        callback(null, included[0].scuolasci_get_prenotazione_included);

                    } else {

                        callback(null, []);
                    }
                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('project_find: ' + err);
        }else{
            if (results.data.length == 0 || !results.included) {
                res.status(404).render(utility.error404('id', 'Nessun progetto trovato', 'Nessun progetto trovato', '404'), null);
            } else {
                var response = {
                    'data': utility.check_find_datas(results.data, 'projects', ['payments', 'contacts', 'products', 'custom_fields', 'events']),
                    'included': utility.check_find_included(results.included, 'projects', ['products', 'contacts', 'payments', 'custom_fields', 'organization_custom_fields', 'addresses',
                        'project_states', 'contact_states', 'events', 'related_projects'
                    ])
                };
                res.status(200).send(response);
            }
        }

    });
};

exports.get_giorni_collettiva = function(req, res) {

    sequelize.query('SELECT scuolasci_get_giorni_collettiva(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.params.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].scuolasci_get_giorni_collettiva && datas[0].scuolasci_get_giorni_collettiva != null) {
                res.status(200).send(datas[0].scuolasci_get_giorni_collettiva);

            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('scuolasci_get_giorni_collettiva: ' + err);
        });
};


exports.get_orario_collettiva = function(req, res) {

    sequelize.query('SELECT scuolasci_get_orario_partenza_collettiva(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.params.id + ',\''+
            req.query.start_date+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].scuolasci_get_orario_partenza_collettiva && datas[0].scuolasci_get_orario_partenza_collettiva != null) {
                res.status(200).send(datas[0].scuolasci_get_orario_partenza_collettiva);

            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('scuolasci_get_orario_partenza_collettiva: ' + err);
        });
};

exports.merge_prenotazione = function(req, res) {

    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var da = req.body.da,
        a = req.body.a;


    sequelize.query('SELECT scuolasci_merge_prenotazione(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            da + ',' +
            a + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas && datas[0] && datas[0].scuolasci_merge_prenotazione && datas[0].scuolasci_merge_prenotazione != null) {
                res.status(200).send({
                    'id': da
                });

            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(400).render('merge_prenotazione: ' + err);
        });
};

exports.prenotazione_modifica_turno = function(req, res) {
    console.log(req.user.role[0].json_build_object.attributes);
    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    var project_id = null,
        turno = null;



    if ((req.body.project_id !== undefined && utility.check_id(req.body.project_id))) {

        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.project_id !== undefined && req.body.project_id !== '') {
        project_id = req.body.project_id;
    }

   
    if (req.body.turno !== undefined && req.body.turno !== '') {
        turno = req.body.turno;
    }



        sequelize.query('SELECT scuolasci_prenotazione_modifica_turno(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                project_id + ',' +
                turno + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                console.log(datas);
                if (datas[0].scuolasci_prenotazione_modifica_turno == 1) {
                    return res.status(200).send({ 'data': { 'id': datas[0].scuolasci_prenotazione_modifica_turno } });
                } else
                    return res.status(422).send({});
            }).catch(function(err) {
                return res.status(400).render('scuolasci_prenotazione_modifica_turno: ' + err);
            });
   
};


function conferma_prenotazione_scuola(org){
    var testo = '';

    if(org == 'maestriscicristallo' || org == 'demo_scuole_di_sci'){
        testo += '<br/>In allegato potrà trovare la conferma della prenotazione valida per la lezione. <br/>Le ricordiamo di presentarsi con almeno 1 ora di anticipo presso la Segreteria Cristallo per le ultime informazioni e il saldo della prenotazione.<br/>PRENOTA QUI L\'ATTREZZATURA AL NOLEGGIO CRISTALLO: <a href="http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/">http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/</a>.<br/>Paghi solo la caparra del 20% e poi usufruisci degli sconti previsti sul saldo finale grazie alla prenotazione delle lezioni / corsi con i nostri Maestri.<br/>';
    }
    else if(org == 'snowacademy'){
           testo += '<br/>In allegato potrà trovare la conferma della prenotazione valida per la lezione. <br/>Le ricordiamo di mostrare al maestro tale conferma sul telefono al momento della lezione.<br/>';
    }
    else if(org == 'scuolascipila'){
           testo += '<br/>In allegato potrà trovare la conferma della prenotazione valida per la lezione. <br/>all\'arrivo trovera dei pannelli con tutte le lettere dell\'alfabeto e dovra presentarsi sotto l\'iniziale del cognome del maestro munito di attrezzatura completa e skipass. '+
                   '<br/> Esempio: maestro Rossi Alberto si aspetta sotto la lettera R.<br/>';
    }
    else if(org == 'scuolascipaganella'){
           testo += '<br/>in allegato può trovare il PDF con il riepilogo della prenotazione richiesta.<br/>Le ricordiamo che, salvo diversi accordi con la segreteria, qualora le lezioni non fossero ancora saldate, per essere confermate dovranno essere saldate con due giorni di anticipo rispetto all’inizio delle stesse.<br/><br/>Bonifico bancario: <br/><br />Intestato a: Scuola Italiana Sci Paganella Pradel Atp<br/>IBAN: IT80U0604559310000005002784<br/> filiale di Bussolengo (VR) <br/>';
    }
    else if(org == 'promescaiol'){
           testo += '<br/>In allegato potrà trovare la conferma della prenotazione valida per la lezione. <br/><br/>Grazie per averci scelto per la tua vacanza sulla neve!<br/>In allegato troverai la conferma della prenotazione. Verifica che date ed orari siano corretti e conserva il tagliando sul tuo telefono. Il primo giorno di lezione il tuo maestro ti chiederà di mostrarglielo.<br/>Ti ricordiamo che se non hai saldato l’intera quota dovrai passare in segreteria prima dell’inizio del corso / lezione. Troverai i nostri uffici a valle presso il noleggio Promescaiol, sotto la partenza della telecabina di Daolasa, oppure direttamente sulle piste all’Alpe Daolasa a 2045mt, all’interno del deposito sci Promescaiol.<br/>Il punto di ritrovo per la lezione / corso sarà, all’orario indicato sul tagliando, direttamente all’interno del deposito riscaldato.<br/>Una volta arrivati in quota con la telecabina di Daolasa:<br/>• Entra nello stabile a vetri che trovi sulla tua sinistra appena sceso dalla telecabina<br/>• Prendi le scale mobili all’interno dello stabile di vetro e sali al primo piano<br/>• Troverai sulla tua destra il nostro deposito e al suo interno la scuola sci.<br/><br/>Assicurati di acquistare lo skipass corretto richiesto per il corso/lezione! Troverai il riquadro “comprensorio” nel riepilogo della prenotazione qui sotto. Il nome del comprensorio è uguale al nome dello skipass che dovrai acquistare.<br/>Qui trovi i nostri consigli per prepararti al meglio per la tua lezione: <a href="https://promescaiol.com/consigli-dai-tuoi-maestri/" target="_blank">Consigli dai tuoi maestri</a><br/>Ti aspettiamo sulle piste!<br/><br/>Promescaiol Ski & Snow Academy';
    }
    else{
           testo += '<br/>In allegato potrà trovare la conferma della prenotazione valida per la lezione. <br/>In allegato il tagliando delle lezioni.<br/>';
    }

    return testo + '<br/>Grazie per averci scelto!<br/><br/>';
};

function conferma_prenotazione_scuola_english(org){
    var testo = '';

    if(org == 'maestriscicristallo' || org == 'demo_scuole_di_sci'){
        testo += ' <br/>Please find attached the confirmation of your lesson. <br/>We remind you to arrive at the Secretariat at least 1 hour before the lesson for the latest information and the balance of the reservation.<br/>BOOK HERE THE EQUIPMENT FOR CRISTALLO RENTAL: <a href="http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/">http://www.maestriscicristallo.it/noleggio-inverno/sci-discesa/</a><br/>You only pay the 20% deposit and then take advantage of the discounts provided on the final balance thanks to booking lessons / courses with our instructors. <br/>';
    }
    else if(org == 'snowacademy'){
           testo += ' <br/>Please find attached the confirmation of your lesson. <br/>We kindly remind you to show it on your phone to the instructor on the day of the lesson.<br/>';
    }
    else if(org == 'scuolascipila'){
           testo += ' <br/>Please find attached the confirmation of your lesson. <br/><br/>  Upon arrival you will find panels with all the letters of the alphabet and must be presented under the initial of the surname of the teacher with complete equipment and ski pass.'+
                      '<br/>example: Maestro Rossi Alberto expects under the letter R.';
    }
    else if(org == 'scuolascipaganella'){
           testo += '<br/>in attachment you can find the PDF with the summary of the requested booking.<br/>We remind you that, unless otherwise agreed with the secretariat, in order to be confirmed, the lessons must be paid for two days in advance of the start of the same.';
    }
    else if(org == 'promescaiol'){
        testo += ' <br/>Please find attached the confirmation of your lesson. <br/><br/>Thank you for choosing us for your ski holiday!<br/>Attached you will find the booking confirmation. Check that the dates and times are correct and keep the coupon on your phone. On the first day of class, your teacher will ask you to show it to him.<br/>We remind you that if you have not paid the full class you will have to go to our office before the start of the course / lesson. You will visit our offices in the valley at the Promescaiol rental, under the departure of the Daolasa cable car, or directly on the slopes at Alpe Daolasa at 2045m, inside the Promescaiol ski depot.<br/>The meeting point for the lesson / course will be, at the time indicated on the coupon, directly inside the heated warehouse.<br/>Once you reach the top with the Daolasa cable car:<br/>• Enter the glass building on your left as soon as you get off the cable car<br/>• Take the escalators inside the glass building and go up to the first floor<br/>• You will find our depot on your right and the ski school inside.<br/><br/>Make sure you purchase the correct ski pass required for the course / lesson! You will find the "area" box in the booking summary below. The name of the area is the same as the name of the ski pass that you will have to buy.<br/>Here you will find our tips to better prepare you for your lesson:: <a href="https://promescaiol.com/consigli-dai-tuoi-maestri/" target="_blank">Click here</a><br/>We are waiting for you on the slopes!<br/><br/>Promescaiol Ski & Snow Academy';
    }
    else{
           testo += '';
    }

    return testo + '<br/>Kind regards<br/><br/>';
};