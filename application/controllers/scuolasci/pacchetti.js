var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');


exports.associa_maestro_prenotazione = function(req, res) {
    var
        /*start_array = [],
            end_array = [],*/
        contact_id = null,
        project_id = null;

    if ((req.body.contact_id !== undefined && utility.check_id(req.body.contact_id))) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.contact_id !== undefined && req.body.contact_id !== '') {
        contact_id = req.body.contact_id;
    }

    if ((req.body.project_id !== undefined && utility.check_id(req.body.project_id))) {
        return res.status(422).send(utility.error422('project_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.project_id !== undefined && req.body.project_id !== '') {
        project_id = req.body.project_id;
    }

    sequelize.query('SELECT scuolasci_associa_maestro_preno_pacchetto_new(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ',' +
            req.user.id + ',' +
            project_id + ',' +
            contact_id +
            ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {

            if (data[0].scuolasci_associa_maestro_preno_pacchetto_new) {
                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'contact',
                    'update',
                    req.user.id,
                    JSON.stringify('{}'),
                    contact_id,
                    'Maestro  ' + contact_id + ' associato alla prenotazione ' + project_id
                );
                res.status(200).send({ 'id': data[0].scuolasci_associa_maestro_preno_pacchetto_new });
            } else {
                res.status(200).send({});
            }

        }).catch(function(err) {
            return res.status(400).render('Maestro associato a prenotazione pacchetto: ' + err);
        });
};

exports.aggiungi_ore_a_pacchetto = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var util = require('util');
    var Sequelize = require('sequelize'),
        project_id = req.body.project_id,
        starts = [],
        ends = [],
        maestro_id = null,
        ore = null,
        da_turno = null;

    if(req.body.maestro_id !== undefined && req.body.maestro_id != ''){
        maestro_id = req.body.maestro_id;
    }

     if(req.body.ore !== undefined && req.body.ore != ''){
        ore = req.body.ore;
    }

   if (req.body.start_events !== undefined && req.body.start_events != '') {
        if (util.isArray(req.body.start_events)) {
            req.body.start_events.forEach(function(start) {
                starts.push('\'' + start + '\'');
            });
        }
    }

    if (req.body.end_events !== undefined && req.body.end_events != '') {
        if (util.isArray(req.body.end_events)) {
            req.body.end_events.forEach(function(end) {
                ends.push('\'' + end + '\'');
            });
        }
    };

    if(req.body.da_turno !== undefined && req.body.da_turno != ''){
        da_turno = req.body.da_turno;
    }



    if (project_id) {
        sequelize.query('SELECT scuolasci_aggiungi_lezioni_a_pacchetto_new(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                project_id + ',' +
                (starts.length > 0 ? 'ARRAY[' + starts + ']::character varying[]' : null) + ',' +
                (ends.length > 0 ? 'ARRAY[' + ends + ']::character varying[]' : null) + ','+
                maestro_id+',' +
                req.user.id + ','+
                ore+','+
                da_turno+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                if (datas[0].scuolasci_aggiungi_lezioni_a_pacchetto_new === '1') {
                    res.status(200).send({});
                } else {       
                    res.status(422).send(utility.error422('project_id', 'Non c\'è disponibilità per la modifica della prenotazione', 'Non c\'è disponibilità per la modifica della prenotazione', '422'));
                }

            }).catch(function(err) {
                return res.status(400).render('scuolasci_aggiungi_lezioni_a_pacchetto_new: ' + err);
            });
    } else {
        res.status(422).send({});
    }
};