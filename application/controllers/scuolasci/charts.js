var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');

/*
 * @apiUse IdParamEntryError
 */
exports.scuolasci_ore_charts = function(req, res) {

   
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }



    sequelize.query('SELECT scuolasci_ore_charts(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].scuolasci_ore_charts[0]);

        }).catch(function(err) {
            return res.status(400).render('scuolasci_ore_charts: ' + err);
        });

};

exports.scuolasci_level_charts = function(req, res) {

   
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }



    sequelize.query('SELECT scuolasci_level_charts(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',null);', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].scuolasci_level_charts[0]);

        }).catch(function(err) {
            return res.status(400).render('scuolasci_level_charts: ' + err);
        });

};


exports.scuolasci_age_charts = function(req, res) {

   
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }



    sequelize.query('SELECT scuolasci_age_charts(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',null);', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].scuolasci_age_charts[0]);

        }).catch(function(err) {
            return res.status(400).render('scuolasci_age_charts: ' + err);
        });

};

exports.scuolasci_lingue_charts = function(req, res) {

   
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }



    sequelize.query('SELECT scuolasci_lingue_charts(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].scuolasci_lingue_charts[0]);

        }).catch(function(err) {
            return res.status(400).render('scuolasci_lingue_charts: ' + err);
        });

};

exports.scuolasci_persone_charts = function(req, res) {

   
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(422).send(utility.error422('year', 'Parametro non valido', 'Parametro non valido', '401'));
    }



    sequelize.query('SELECT scuolasci_persone_charts(' +
            req.query.year + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            //callback(null,datas[0].contacts_data);

            res.json(datas[0].scuolasci_persone_charts[0]);

        }).catch(function(err) {
            return res.status(400).render('scuolasci_persone_charts: ' + err);
        });

};
