var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');


exports.find = function(req, res) {

    finds.find_invoicegroup(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var invoicegroup = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        invoicegroup['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        invoicegroup['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        invoicegroup['price'] = req.body.data.attributes.price;
        invoicegroup['qta'] = req.body.data.attributes.qta;
        invoicegroup['importo'] = req.body.data.attributes.importo;
        invoicegroup['unit'] = (req.body.data.attributes.unit && utility.check_type_variable(req.body.data.attributes.unit, 'string') && req.body.data.attributes.unit !== null ? req.body.data.attributes.unit.replace(/'/g, "''''") : '');
        invoicegroup['rank'] = req.body.data.attributes.rank;
        invoicegroup['product_id'] = ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null);
        invoicegroup['contact_id'] = ((req.body.data.relationships && req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id && !utility.check_id(req.body.data.relationships.contact.data.id)) ? req.body.data.relationships.contact.data.id : null);


        sequelize.query('SELECT insert_invoicegroup(\'' + JSON.stringify(invoicegroup) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_invoicegroup && datas[0].insert_invoicegroup != null) {
                    req.body.data.id = datas[0].insert_invoicegroup;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_invoicegroup,
                        'body': req.body.data.relationships,
                        'model': 'invoicegroup',
                        'user_id': req.user.id,
                        'array': ['invoiceproduct']
                    }, function(err, results) {

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'invoicegroup',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_invoicegroup,
                            'Aggiunto invoiceproductgroup in fattura ' + datas[0].insert_invoicegroup
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });
                    });

                }

            }).catch(function(err) {
                //next(new Error(err));
                return res.status(400).render('invoicegroup_insert: ' + err);

            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case

        sequelize.query('SELECT update_invoicegroup(' +
                req.params.id +
                ',\'' + (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') +
                '\',\'' + (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') +
                '\',\'' + (req.body.data.attributes.unit && utility.check_type_variable(req.body.data.attributes.unit, 'string') && req.body.data.attributes.unit !== null ? req.body.data.attributes.unit.replace(/'/g, "''''") : '') +
                '\',' + req.body.data.attributes.price +
                ',' + req.body.data.attributes.qta +
                ',' + req.body.data.attributes.importo +
                ',' + req.body.data.attributes.rank + ',' +
                ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null) + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                ((req.body.data.relationships && req.body.data.relationships.contact && req.body.data.relationships.event.data && req.body.data.relationships.contact.data.id && !utility.check_id(req.body.data.relationships.contact.data.id)) ? req.body.data.relationships.contact.data.id : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                utility.sanitizeRelations({
                    'organization': req.headers['host'].split(".")[0],
                    '_tenant': req.user._tenant_id,
                    'new_id': req.params.id,
                    'body': req.body.data.relationships,
                    'model': 'invoicegroup',
                    'user_id': req.user.id,
                    'array': ['invoiceproduct']
                }, function(err, results) {
                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'invoicegroup',
                        'update',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        req.params.id,
                        'Modificato invoicegroup in fattura ' + req.params.id
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });
                });

            }).catch(function(err) {
                return res.status(400).render('invoicegroup_update: ' + err);
            });


    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_invoicegroup_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'invoicegroups',
                    'id': datas[0].delete_invoicegroup_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('invoicegroup_destroy: ' + err);
        });
};