var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

    var user_sezionale = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        user_sezionale['prefix'] = (req.body.data.attributes.prefix !== null ? req.body.data.attributes.prefix.replace(/'/g, "''''") : '');
        user_sezionale['suffix'] = (req.body.data.attributes.suffix !== null ? req.body.data.attributes.suffix.replace(/'/g, "''''") : '');
        user_sezionale['is_default'] = req.body.data.attributes.is_default;
        user_sezionale['user_id'] = ((req.body.data.relationships.contact && req.body.data.relationships.contact.data && req.body.data.relationships.contact.data.id) ? req.body.data.relationships.contact.data.id : null);
        user_sezionale['_tenant_id'] = req.user._tenant_id;
        user_sezionale['organization'] = req.headers['host'].split(".")[0];

        sequelize.query('SELECT insert_user_sezionale_json(\'' + JSON.stringify(user_sezionale) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_user_sezionale_json && datas[0].insert_user_sezionale_json != null) {
                    req.body.data.id = datas[0].insert_user_sezionale_json;

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'user_sezionale',
                        'insert',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        datas[0].insert_user_sezionale_json,
                        'Aggiunto user_sezionale ' + datas[0].insert_user_sezionale_json
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('user_sezionale_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        
            sequelize.query('SELECT update_user_sezionale(' +
                    req.params.id +
                    ',' + (req.body.data.attributes.prefix && utility.check_type_variable(req.body.data.attributes.prefix, 'string') && req.body.data.attributes.prefix != null ? '\'' + req.body.data.attributes.prefix.replace(/'/g, "''''") + '\'' : null) +
                    ',' + (req.body.data.attributes.suffix && utility.check_type_variable(req.body.data.attributes.suffix, 'string') && req.body.data.attributes.suffix != null ? '\'' + req.body.data.attributes.suffix.replace(/'/g, "''''") + '\'' : null) +
                    ',' + (req.body.data.attributes.is_default !== undefined && utility.check_type_variable(req.body.data.attributes.is_default, 'boolean') ? req.body.data.attributes.is_default : false) +
                    ',' + req.user._tenant_id +
                    ',\'' + req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {
                   

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'user_sezionale',
                            'update',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            req.params.id,
                            'Modificato user_sezionale ' + req.params.id
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    }).catch(function(err) {
                        return res.status(400).render('user_sezionale_update: ' + err);
                    });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_user_sezionale_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'user_sezionales',
                    'id': datas[0].delete_user_sezionale_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('user_sezionale_destroy: ' + err);
        });
};