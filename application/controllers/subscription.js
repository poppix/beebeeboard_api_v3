var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT find_subscription(' +
                req.params.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(old) {
                /* Se ha cambiato il piano o Aggiunto users allora deve prima pagare */
                if (
                    (req.body.data.relationships.plan && req.body.data.relationships.plan.data && req.body.data.relationships.plan.data.id &&
                        old[0].find_subscription.plan_id != req.body.data.relationships.plan.data.id) ||
                    (
                        (req.body.data.attributes.paied_agents && !utility.check_id(req.body.data.attributes.paied_agents) &&
                            old[0].find_subscription.paied_agents != req.body.data.attributes.paied_agents)
                    ) ||
                    (
                        (req.body.data.attributes.month_billing_cycle != old[0].find_subscription.month_billing_cycle)
                    )
                ) {

                    var amount = 0.00;
                    sequelize.query('SELECT find_plan(' +
                            req.body.data.relationships.plan.data.id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user.id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(plan) {
                             var fixed_year_price = plan[0].find_plan.fixed_year ? plan[0].find_plan.fixed_year : 0.00;
                             var fixed_month_price = plan[0].find_plan.fixed_month ? plan[0].find_plan.fixed_month : 0.00;

                            var discount_fix = 0.00;
                            var discount_perc = 0.00;

                            var fixed_year_price = plan[0].find_plan.fixed_year ? plan[0].find_plan.fixed_year : 0.00;
                            var fixed_month_price = plan[0].find_plan.fixed_month ? plan[0].find_plan.fixed_month : 0.00;
                            var expire = moment(old[0].find_subscription.expire_date);
                            var created = moment(old[0].find_subscription.created_at);
                            var today = moment(new Date());
                            var days_from_expire = parseInt(expire.diff(today, 'days'));
                            var days_from_create = parseInt(today.diff(created,'days'));

                            if (old[0].find_subscription.plan_id != req.body.data.relationships.plan.data.id &&
                                old[0].find_subscription.plan_id < req.body.data.relationships.plan.data.id) {
                                if (req.body.data.attributes.month_billing_cycle == true) {
                                    amount = (plan[0].find_plan.per_agent_cost_with_monthly_billing * req.body.data.attributes.paied_agents)+fixed_month_price;
                                } else {
                                    amount = (plan[0].find_plan.per_agent_cost_with_annual_billing * req.body.data.attributes.paied_agents)+fixed_year_price;
                                }
                            } else if (plan[0].find_plan.max_agents) {
                                if (req.body.data.attributes.month_billing_cycle == true)
                                    amount = plan[0].find_plan.monthly_billing;
                                else
                                    amount = plan[0].find_plan.annual_billing;
                            } else {
                                if (req.body.data.attributes.month_billing_cycle == true) {

                                    if (old[0].find_subscription.paied_agents != req.body.data.attributes.paied_agents) {
                                        var diff_agents = (req.body.data.attributes.paied_agents - old[0].find_subscription.paied_agents > 0) ? (req.body.data.attributes.paied_agents - old[0].find_subscription.paied_agents) : 0;

                                        if (days_from_expire > 0) {
                                            amount = (plan[0].find_plan.per_agent_cost_with_monthly_billing / 365 * days_from_expire) * (diff_agents);
                                        } else {
                                            amount = (plan[0].find_plan.per_agent_cost_with_monthly_billing * req.body.data.attributes.paied_agents)+fixed_month_price;
                                        }
                                    } else {
                                        amount = (plan[0].find_plan.per_agent_cost_with_monthly_billing * req.body.data.attributes.paied_agents)+fixed_month_price;
                                    }

                                } else {
                                    if (old[0].find_subscription.paied_agents != req.body.data.attributes.paied_agents) {
                                        var diff_agents = (req.body.data.attributes.paied_agents - old[0].find_subscription.paied_agents > 0) ? (req.body.data.attributes.paied_agents - old[0].find_subscription.paied_agents) : 0;

                                        if (days_from_expire > 0) {
                                            amount = (plan[0].find_plan.per_agent_cost_with_monthly_billing / 365 * days_from_expire) * (diff_agents);
                                        } else {
                                            amount = (plan[0].find_plan.per_agent_cost_with_annual_billing * req.body.data.attributes.paied_agents)+fixed_year_price;
                                        }
                                    } else {
                                        amount = (plan[0].find_plan.per_agent_cost_with_annual_billing * req.body.data.attributes.paied_agents)+fixed_year_price;
                                    }


                                }
                            }

                            if(old[0].find_subscription.promo_code && old[0].find_subscription.duration_day && old[0].find_subscription.plans && old[0].find_subscription.plans.indexOf(parseInt(req.body.plan_id)) != -1){
                                if(old[0].find_subscription.perc && days_from_create < (old[0].find_subscription.duration_day -1)){
                                    discount_perc = old[0].find_subscription.perc / 100;
                                }
                                else if(old[0].find_subscription.fix && days_from_create < (old[0].find_subscription.duration_day -1)){
                                    discount_fix = old[0].find_subscription.fix;
                                }

                                amount = amount - (amount*discount_perc) - discount_fix;
                            }

                            async.parallel({
                                customer: function(callback) {
                                    sequelize.query('SELECT stripe_get_customer_id_from_contact_id(\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.user._tenant_id + ',' +
                                            req.user.id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                            })
                                        .then(function(datas) {
                                            callback(null, datas[0].stripe_get_customer_id_from_contact_id);
                                        }).catch(function(err) {
                                            callback(err, null);
                                        });
                                },
                                amount: function(callback) {
                                    callback(null, parseFloat(utility.formatta_euro(amount * 1.22)));
                                },
                                control: function(callback) {

                                    sequelize.query('SELECT beebeeboard_control_limit(' +
                                            req.params.id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            req.user.id + ',' +
                                            req.body.data.relationships.plan.data.id + ',' +
                                            req.body.data.attributes.paied_agents + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                            })
                                        .then(function(con) {

                                            callback(null, con[0].beebeeboard_control_limit);
                                        }).catch(function(err) {
                                            callback(err, false);
                                        });


                                }
                            }, function(err, final) {
                                if (err) {
                                    console.log(err);
                                    return res.status(400).render('payment from stripe: ' + err);

                                } else if (final.control) {
                                    if (final.customer) {

                                        if (final.amount == 0) {
                                            sequelize.query('SELECT update_subscription(' +
                                                    req.params.id + ',' +
                                                    ((req.body.data.relationships.plan && req.body.data.relationships.plan.data && req.body.data.relationships.plan.data.id) ? req.body.data.relationships.plan.data.id : null) + ',' +
                                                    (req.body.data.attributes.paied_agents && !utility.check_id(req.body.data.attributes.paied_agents) ? req.body.data.attributes.paied_agents : null) + ',' +
                                                    (req.body.data.attributes.active && utility.check_type_variable(req.body.data.attributes.active, 'boolean') ? req.body.data.attributes.active : false) + ',' +
                                                    (req.body.data.attributes.month_billing_cycle && utility.check_type_variable(req.body.data.attributes.month_billing_cycle, 'boolean') ? req.body.data.attributes.month_billing_cycle : false) + ',' +
                                                    (req.body.data.attributes.year_billing_cycle && utility.check_type_variable(req.body.data.attributes.year_billing_cycle, 'boolean') ? req.body.data.attributes.year_billing_cycle : false) + ',' +
                                                    req.user._tenant_id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',' +
                                                    req.user.id + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    if (datas[0].update_subscription == 1) {
                                                        logs.save_log_model(
                                                            req.user._tenant_id,
                                                            req.headers['host'].split(".")[0],
                                                            'subscription',
                                                            'udpate',
                                                            req.user.id,
                                                            JSON.stringify(req.body.data.attributes),
                                                            req.params.id,
                                                            'Modificato piano tariffario ' + req.params.id
                                                        );
                                                    }
                                                    req.body.data.attributes.trial = false;
                                                    res.json({
                                                        'data': req.body.data,
                                                        'relationships': req.body.data.relationships
                                                    });
                                                }).catch(function(err) {
                                                    return res.status(400).render('subscription_update: ' + err);
                                                });
                                        } else {
                                            var charge = stripe.charges.create({
                                                amount: final.amount * 100,
                                                currency: "eur",
                                                customer: final.customer
                                            }, function(err, charge) {

                                                if (charge.status == 'succeeded' && charge.paid == true) {
                                                    sequelize.query('SELECT update_subscription(' +
                                                            req.params.id + ',' +
                                                            ((req.body.data.relationships.plan && req.body.data.relationships.plan.data && req.body.data.relationships.plan.data.id) ? req.body.data.relationships.plan.data.id : null) + ',' +
                                                            (req.body.data.attributes.paied_agents && !utility.check_id(req.body.data.attributes.paied_agents) ? req.body.data.attributes.paied_agents : null) + ',' +
                                                            (req.body.data.attributes.active && utility.check_type_variable(req.body.data.attributes.active, 'boolean') ? req.body.data.attributes.active : false) + ',' +
                                                            (req.body.data.attributes.month_billing_cycle && utility.check_type_variable(req.body.data.attributes.month_billing_cycle, 'boolean') ? req.body.data.attributes.month_billing_cycle : false) + ',' +
                                                            (req.body.data.attributes.year_billing_cycle && utility.check_type_variable(req.body.data.attributes.year_billing_cycle, 'boolean') ? req.body.data.attributes.year_billing_cycle : false) + ',' +
                                                            req.user._tenant_id + ',\'' +
                                                            req.headers['host'].split(".")[0] + '\',' +
                                                            req.user.id + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                            })
                                                        .then(function(datas) {
                                                            if (datas[0].update_subscription == 1) {
                                                                logs.save_log_model(
                                                                    req.user._tenant_id,
                                                                    req.headers['host'].split(".")[0],
                                                                    'subscription',
                                                                    'udpate',
                                                                    req.user.id,
                                                                    JSON.stringify(req.body.data.attributes),
                                                                    req.params.id,
                                                                    'Modificato piano tariffario ' + req.params.id
                                                                );

                                                                sequelize.query('SELECT beebeeboard_crea_fattura_poppix_srl(' +
                                                                        req.user._tenant_id + ',\'' +
                                                                        req.headers['host'].split(".")[0] + '\',' +
                                                                        req.user.id + ',' +
                                                                        req.params.id + ',' +
                                                                        moment().year() + ',' +
                                                                        final.amount + ');', {
                                                                            raw: true,
                                                                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                                                        })
                                                                    .then(function(fattura) {}).catch(function(err) {
                                                                        return res.status(400).render('beebeeboard_crea_fattura_poppix_srl: ' + err);
                                                                    });

                                                                req.body.data.attributes.trial = false;
                                                                res.json({
                                                                    'data': req.body.data,
                                                                    'relationships': req.body.data.relationships
                                                                });
                                                            } else {
                                                                return res.status(422).send(utility.error422('id', 'Non si hanno i diritti per eseguire un cambio di piano', 'Non si hanno i diritti per eseguire un cambio di piano', '422'));
                                                            }


                                                        }).catch(function(err) {
                                                            return res.status(400).render('subscription_update: ' + err);
                                                        });
                                                } else {
                                                    res.status(422).send({});
                                                }
                                            });
                                        }

                                    } else {
                                        res.status(422).send(utility.error422('Piano', 'Nessun metodo di pagamento inserito', 'Parametro non valido', '422'));

                                    }
                                } else {
                                    res.status(422).send(utility.error422('Piano', 'Il tuo piano attuale supera i limiti permessi per il nuovo piano. Verifica il numero di utenti, il numero di campi personalizzati e l\'occupazione in Gb', 'Parametro non valido', '422'));
                                }
                            });
                            
                        }).catch(function(err) {
                            return res.status(400).render('find_plan: ' + err);
                        });
                } /* altrimenti modifica subito la subscription */
                else {
                    sequelize.query('SELECT update_subscription(' +
                            req.params.id + ',' +
                            ((req.body.data.relationships.plan && req.body.data.relationships.plan.data && req.body.data.relationships.plan.data.id) ? req.body.data.relationships.plan.data.id : null) + ',' +
                            (req.body.data.attributes.paied_agents && !utility.check_id(req.body.data.attributes.paied_agents) ? req.body.data.attributes.paied_agents : null) + ',' +
                            (req.body.data.attributes.active && utility.check_type_variable(req.body.data.attributes.active, 'boolean') ? req.body.data.attributes.active : false) + ',' +
                            (req.body.data.attributes.month_billing_cycle && utility.check_type_variable(req.body.data.attributes.month_billing_cycle, 'boolean') ? req.body.data.attributes.month_billing_cycle : false) + ',' +
                            (req.body.data.attributes.year_billing_cycle && utility.check_type_variable(req.body.data.attributes.year_billing_cycle, 'boolean') ? req.body.data.attributes.year_billing_cycle : false) + ',' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user.id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                        .then(function(datas) {
                            if (datas[0].update_subscription == 1) {
                                logs.save_log_model(
                                    req.user._tenant_id,
                                    req.headers['host'].split(".")[0],
                                    'subscription',
                                    'udpate',
                                    req.user.id,
                                    JSON.stringify(req.body.data.attributes),
                                    req.params.id,
                                    'Modificato piano tariffario ' + req.params.id
                                );
                                req.body.data.attributes.trial = false;
                                res.json({
                                    'data': req.body.data,
                                    'relationships': req.body.data.relationships
                                });
                            } else {
                                return res.status(422).send(utility.error422('id', 'Non si hanno i diritti per eseguire un cambio di piano', 'Non si hanno i diritti per eseguire un cambio di piano', '422'));
                            }


                        }).catch(function(err) {
                            return res.status(400).render('subscription_update: ' + err);
                        });
                }

            }).catch(function(err) {
                return res.status(400).render('subscription_select: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

exports.get_price = function(req, res) {
    if (utility.check_id(req.body.subscription_id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.subscription_id !== undefined) {

       

        sequelize.query('SELECT find_subscription(' +
                req.body.subscription_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(old) {

                sequelize.query('SELECT find_plan(' +
                        req.body.plan_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(plan) {

                        sequelize.query('SELECT beebeeboard_to_pay_v2(\'' +
                                JSON.stringify(req.body).replace(/'/g, "''''") + '\',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                req.user.id + ');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT                        
                                })
                            .then(function(to_pay) {
                                //console.log(to_pay[0].beebeeboard_to_pay_v2);
                                res.status(200).send(JSON.parse(to_pay[0].beebeeboard_to_pay_v2));
                        }).catch(function(err) {
                            return res.status(400).render('beebeeboard_to_pay_v2: ' + err);
                        });

                       /* var amount = 0.00;
                        var cycle = 0.00;

                        var discount_fix = 0.00;
                        var discount_perc = 0.00;

                        var fixed_year_price = plan[0].find_plan.fixed_year ? plan[0].find_plan.fixed_year : 0.00;
                        var fixed_month_price = plan[0].find_plan.fixed_month ? plan[0].find_plan.fixed_month : 0.00;
                        var expire = moment(old[0].find_subscription.expire_date);
                        var created = moment(old[0].find_subscription.created_at);

                        var today = moment(new Date());
                        var days_from_expire = parseInt(expire.diff(today, 'days'));
                        var days_from_create = parseInt(today.diff(created,'days'));
                        var booking_online_price = 0;

                        
                        if (plan[0].find_plan.max_agents) {
                            if (req.body.month_billing_cycle == 'true') {
                                cycle = plan[0].find_plan.monthly_billing;
                                amount = plan[0].find_plan.monthly_billing;
                            } else {
                                cycle = plan[0].find_plan.annual_billing;
                                amount = plan[0].find_plan.annual_billing;
                            }
                        } else {
                            if (req.body.month_billing_cycle == 'true') {

                                if (old[0].find_subscription.paied_agents != req.body.paied_agents) {
                                    
                                    var diff_agents = (req.body.paied_agents - old[0].find_subscription.paied_agents > 0) ? (req.body.paied_agents - old[0].find_subscription.paied_agents) : 0;

                                    if (days_from_expire > 0) {
                                        amount = (plan[0].find_plan.per_agent_cost_with_monthly_billing / 365 * days_from_expire) * (diff_agents);
                                    } else {
                                        amount = plan[0].find_plan.per_agent_cost_with_monthly_billing * req.body.paied_agents;
                                    }

                                    amount = amount + fixed_month_price;

                                } else {
                                    amount = plan[0].find_plan.per_agent_cost_with_monthly_billing * req.body.paied_agents;
                                    amount = amount + fixed_month_price;
                                }
                                cycle = (plan[0].find_plan.per_agent_cost_with_monthly_billing * req.body.paied_agents)+fixed_month_price;
                            } else {
                                if (old[0].find_subscription.paied_agents != req.body.paied_agents) {
                                    var diff_agents = (req.body.paied_agents - old[0].find_subscription.paied_agents > 0) ? (req.body.paied_agents - old[0].find_subscription.paied_agents) : 0;

                                    if (days_from_expire > 0) {
                                        amount = (plan[0].find_plan.per_agent_cost_with_annual_billing / 365 * days_from_expire) * (diff_agents);
                                    } else {
                                        amount = plan[0].find_plan.per_agent_cost_with_annual_billing * req.body.paied_agents;
                                    }

                                    amount = amount + fixed_year_price;

                                } else {
                                    amount = plan[0].find_plan.per_agent_cost_with_annual_billing * req.body.paied_agents;
                                    amount = amount + fixed_year_price;
                                }
                                cycle = (plan[0].find_plan.per_agent_cost_with_annual_billing * req.body.paied_agents)+fixed_year_price;
                            }
                        }

                        if(old[0].find_subscription.promo_code && old[0].find_subscription.duration_day && 
                            old[0].find_subscription.plans && old[0].find_subscription.plans.indexOf(parseInt(req.body.plan_id)) != -1){
                            
                            if(old[0].find_subscription.perc && days_from_create < (old[0].find_subscription.duration_day -1)){
                                discount_perc = old[0].find_subscription.perc / 100;
                            }
                            else if(old[0].find_subscription.fix && days_from_create < (old[0].find_subscription.duration_day -1)){
                                discount_fix = old[0].find_subscription.fix;
                            }

                            amount = amount - (amount*discount_perc) - discount_fix;

                            if(days_from_create < (old[0].find_subscription.duration_day -1)){
                                cycle = cycle - (cycle*discount_perc) - discount_fix;
                            }
                        }

                        if(req.body.booking_online && 
                            old[0].find_subscription.booking_online > req.body.booking_online){
                            if (req.body.month_billing_cycle == 'true') {
                                booking_online_price = (15 / 365 * days_from_expire) * 1.22;
                            }
                            else{
                                booking_online_price = (150 / 365 * days_from_expire) * 1.22;
                            }
                        }
                        
                       
                        if (
                            (req.body.plan_id &&
                                old[0].find_subscription.plan_id < parseInt(req.body.plan_id)) || (
                                (req.body.month_billing_cycle == 'true' && old[0].find_subscription.month_billing_cycle == false) ||
                                (req.body.month_billing_cycle == 'false' && old[0].find_subscription.month_billing_cycle == true))
                        ) {


                            res.json({
                                'cycle': cycle + booking_online_price,
                                'amount': amount  + booking_online_price
                            });

                        } 
                        else if(req.body.plan_id &&
                                old[0].find_subscription.plan_id >= parseInt(req.body.plan_id))
                            if (days_from_expire < 1) {
                                res.json({
                                    'cycle': cycle  + booking_online_price,
                                    'amount': amount  + booking_online_price
                                });
                            }
                            else
                            { 
                                res.json({
                                    'cycle': cycle + booking_online_price,
                                    'amount': 0.00 + booking_online_price
                                });

                            }
                        else if (

                            (req.body.paied_agents &&
                                old[0].find_subscription.paied_agents != parseInt(req.body.paied_agents) &&
                                (parseInt(req.body.paied_agents) - old[0].find_subscription.paied_agents > 0))) {

                            res.json({
                                'cycle': cycle + booking_online_price,
                                'amount': amount + booking_online_price
                            });

                        } else if (days_from_expire < 1) {
                            res.json({
                                'cycle': cycle + booking_online_price,
                                'amount': amount + booking_online_price
                            });
                        } else {
                            res.json({
                                'cycle': cycle + booking_online_price,
                                'amount': 0.00 + booking_online_price
                            });

                        }*/

                    }).catch(function(err) {
                        return res.status(400).render('find_plan: ' + err);
                    });

            }).catch(function(err) {
                return res.status(400).render('subscription_select: ' + err);
            });
    } else {
        res.json({
            'amount': 0.00
        });
    }
};

exports.poppixsrl_bill_info = function(req, res) {
    sequelize.query('SELECT poppixsrl_bill_info(' +
            req.user.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(old) {
            if (old[0].poppixsrl_bill_info) {
                res.json({ 'data': old[0].poppixsrl_bill_info });
            }else
            {
                res.json({ });
            }
        }).catch(function(err) {
            return res.status(400).render('poppixsrl_bill_info: ' + err);
        });
};

exports.scadenze = function(req, res) {
    sequelize.query('SELECT get_scadenze_servizi(' +
            req.user.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(old) {
            if (old[0].get_scadenze_servizi) {
                res.json({ 'data': old[0].get_scadenze_servizi });
            }else
            {
                res.json({ });
            }
        }).catch(function(err) {
            return res.status(400).render('get_scadenze_servizi: ' + err);
        });
};

exports.save_poppixsrl_bill_info = function(req, res) {

    sequelize.query('SELECT save_poppixsrl_bill_info_v1(' +
            req.user.id + ',\'' +
            (req.body.contact && req.body.contact !== null && utility.check_type_variable(req.body.contact, 'string') ? req.body.contact.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.piva && req.body.piva !== null && utility.check_type_variable(req.body.piva, 'string') ? req.body.piva.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.location_address_street && req.body.location_address_street !== null && utility.check_type_variable(req.body.location_address_street, 'string') ? req.body.location_address_street.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.location_address_postal_code && req.body.location_address_postal_code !== null && utility.check_type_variable(req.body.location_address_postal_code, 'string') ? req.body.location_address_postal_code.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.location_address_house && req.body.location_address_house !== null && utility.check_type_variable(req.body.location_address_house, 'string') ? req.body.location_address_house.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.location_address_city && req.body.location_address_city !== null && utility.check_type_variable(req.body.location_address_city, 'string') ? req.body.location_address_city.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.location_address_country && req.body.location_address_country !== null && utility.check_type_variable(req.body.location_address_country, 'string') ? req.body.location_address_country.replace(/'/g, "''''") : '') + '\',\'' +
            req.headers['host'].split(".")[0] + '\', \'' +
            (req.body.cod_fiscale && req.body.cod_fiscale !== null && utility.check_type_variable(req.body.cod_fiscale, 'string') ? req.body.cod_fiscale.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.codice_destinatario && req.body.codice_destinatario !== null && utility.check_type_variable(req.body.codice_destinatario, 'string') ? req.body.codice_destinatario.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.pec && req.body.pec !== null && utility.check_type_variable(req.body.pec, 'string') ? req.body.pec.replace(/'/g, "''''") : '') + '\',\'' + 
            (req.body.first_name && req.body.first_name !== null && utility.check_type_variable(req.body.first_name, 'string') ? req.body.first_name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.last_name && req.body.last_name !== null && utility.check_type_variable(req.body.last_name, 'string') ? req.body.last_name.replace(/'/g, "''''") : '') + '\',' +
            (req.body.fisica_giuridica && req.body.fisica_giuridica !== null && utility.check_type_variable(req.body.fisica_giuridica, 'string') ? req.body.fisica_giuridica.replace(/'/g, "''''") : '') + ',\'' +
            (req.body.ragione_sociale && req.body.ragione_sociale !== null && utility.check_type_variable(req.body.ragione_sociale, 'string') ? req.body.ragione_sociale.replace(/'/g, "''''") : '') + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(old) {
            if (old[0].save_poppixsrl_bill_info_v1) {
                res.json({ 'data': old[0].save_poppixsrl_bill_info_v1 });
            }
        }).catch(function(err) {
            return res.status(400).render('save_poppixsrl_bill_info_v1: ' + err);
        });
};