var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');


exports.find = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_todo(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

exports.create = function(req, res) {

    var todo = {};

    if (req.body.data.attributes !== undefined) {
        if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
            return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        todo['date'] = (req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? req.body.data.attributes.date : null);
        todo['text'] = (req.body.data.attributes.text && utility.check_type_variable(req.body.data.attributes.text, 'string') && req.body.data.attributes.text !== null ? req.body.data.attributes.text.replace(/'/g, "''''") : '');
        todo['is_completed'] = req.body.data.attributes.is_completed;
        todo['completed_by_id'] = ((req.body.data.relationships && req.body.data.relationships.completed_by && req.body.data.relationships.completed_by.data && req.body.data.relationships.completed_by.data.id && !utility.check_id(req.body.data.relationships.completed_by.data.id)) ? req.body.data.relationships.completed_by.data.id : null);
        todo['completed_at'] = (req.body.data.attributes.completed_at && moment(req.body.data.attributes.completed_at).isValid() ? req.body.data.attributes.completed_at : null);
        todo['todolist_id'] = ((req.body.data.relationships && req.body.data.relationships.todolist && req.body.data.relationships.todolist.data && req.body.data.relationships.todolist.data.id && !utility.check_id(req.body.data.relationships.todolist.data.id)) ? req.body.data.relationships.todolist.data.id : null);
        todo['organization'] = req.headers['host'].split(".")[0];
        todo['_tenant_id'] = req.user._tenant_id;
        todo['mongo_id'] = null;
        todo['text_field'] = (req.body.data.attributes.text_field && utility.check_type_variable(req.body.data.attributes.text_field, 'string') && req.body.data.attributes.text_field !== null ? req.body.data.attributes.text_field.replace(/'/g, "''''") : '');
        

        sequelize.query('SELECT insert_todo(\'' +
            JSON.stringify(todo) + '\',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

            if (datas[0].insert_todo && datas[0].insert_todo != null) {
                req.body.data.id = parseInt(datas[0].insert_todo);

                utility.sanitizeRelations({
                    'organization': req.headers['host'].split(".")[0],
                    '_tenant': req.user._tenant_id,
                    'new_id': datas[0].insert_todo,
                    'body': req.body.data.relationships,
                    'model': 'todo',
                    'user_id': req.user.id,
                    'array': ['reminder', 'contact']
                }, function(err, results) {

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'todo',
                        'insert',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        datas[0].insert_todo,
                        'Aggiunto todo ' + datas[0].insert_todo
                    );

                    if (todo['todolist_id']) {
                        logs.save_log_relation(
                            'todo',
                            'todolist',
                            datas[0].insert_todo,
                            todo['todolist_id'],
                            'insert',
                            req.user.id,
                            req.headers['host'].split(".")[0],
                            req.user._tenant_id
                        );
                    }

                    if (todo['completed_by_id']) {
                        logs.save_log_relation(
                            'todo',
                            'contact',
                            datas[0].insert_todo,
                            todo['completed_by_id'],
                            'insert',
                            req.user.id,
                            req.headers['host'].split(".")[0],
                            req.user._tenant_id
                        );
                    }

                    utility.save_socket(req.headers['host'].split(".")[0], 'todo', 'insert', req.user.id, datas[0].insert_todo, req.body.data);
        

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });

                });

            } else {
                res.json({
                    'data': [],
                    'relationships': []
                });
            }


        }).catch(function(err) {
            return res.status(400).render('todo_insert: ' + err);
        });
    } else {
        res.json({
            'data': []
        });
    }
};

exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.date != null && !moment(req.body.data.attributes.date).isValid()) {
        return res.status(422).send(utility.error422('date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined && req.body.data.relationships && req.body.data.relationships.todolist && req.body.data.relationships.todolist.data && req.body.data.relationships.todolist.data.id) {
        var completed_at = req.body.data.attributes.completed_at != null ? '\'' + req.body.data.attributes.completed_at + '\'' : null;
        sequelize.query('SELECT update_todo_v1(' +
            req.params.id + ',' +
            (req.body.data.attributes.date != null && req.body.data.attributes.date && moment(req.body.data.attributes.date).isValid() ? '\'' + req.body.data.attributes.date + '\'' : null) + ',' +
            req.body.data.relationships.todolist.data.id + ',\'' +
            (req.body.data.attributes.text && utility.check_type_variable(req.body.data.attributes.text, 'string') && req.body.data.attributes.text !== null ? req.body.data.attributes.text.replace(/'/g, "''''") : '') + '\',' +
            req.body.data.attributes.is_completed + ',' +
            ((req.body.data.relationships && req.body.data.relationships.completed_by && req.body.data.relationships.completed_by.data && req.body.data.relationships.completed_by.data.id && !utility.check_id(req.body.data.relationships.completed_by.data.id)) ? req.body.data.relationships.completed_by.data.id : null) + ',' +
            (req.body.data.attributes.completed_at && moment(req.body.data.attributes.completed_at).isValid() ? '\'' + req.body.data.attributes.completed_at + '\'' : null) + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',\''+
            (req.body.data.attributes.text_field && utility.check_type_variable(req.body.data.attributes.text_field, 'string') && req.body.data.attributes.text_field !== null ? req.body.data.attributes.text_field.replace(/'/g, "''''") : '') + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

            utility.sanitizeRelations({
                'organization': req.headers['host'].split(".")[0],
                '_tenant': req.user._tenant_id,
                'new_id': req.params.id,
                'body': req.body.data.relationships,
                'model': 'todo',
                'user_id': req.user.id,
                'array': ['reminder']
            }, function(err, results) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'todo',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato todo ' + req.params.id
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'todo', 'update', req.user.id, req.params.id, req.body.data);
        

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            });

        }).catch(function(err) {
            return res.status(400).render('todo_update: ' + err);
        });


    } else {

        res.json({
            'data': [],
            'relationships': []
        });
    }
};

exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }


    sequelize.query('SELECT delete_todo_v1(' +
        req.params.id + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

            utility.save_socket(req.headers['host'].split(".")[0], 'todo', 'delete', req.user.id, req.params.id, null);
            
            res.json({
                'data': {
                    'id': datas[0].delete_todo_v1,
                    'type': 'todos',

                }
            });

    }).catch(function(err) {
        return res.status(400).render('todo_destroy: ' + err);
    });
};