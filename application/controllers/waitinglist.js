var moment = require('moment'),
    Sequelize = require('sequelize');

exports.index = function(req, res) {
  

    sequelize.query('SELECT waitinglist(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            (req.query.giorno ? '\''+req.query.giorno+'\''  : null )+','+
            (req.query.contact_id ? req.query.contact_id  : null )+','+
            (req.query.product_id ? req.query.product_id  : null) +');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            if(data && data[0] && data[0].waitinglist){
                    res.status(200).send(data[0].waitinglist );
            }else
            {
                res.status(200).send({});
            }
            
        }).catch(function(err) {
            return res.status(400).render('waitinglist: ' + err);
        });
};

exports.insert_waitinglist = function(req, res) {
    sequelize.query('SELECT waitinglist_insert(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',\''+
            JSON.stringify(req.body).replace(/'/g, "''''")+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.status(200).send(data[0].waitinglist_insert[0]);
        }).catch(function(err) {
            return res.status(400).render('waitinglist_insert: ' + err);
        });
};

exports.delete_waitinglist = function(req, res) {
    sequelize.query('SELECT delete_waitinglist(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            req.params.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('delete_waitinglist: ' + err);
        });
};

exports.update_waitinglist = function(req, res) {
    sequelize.query('SELECT waitinglist_update(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            req.params.id +',\''+
            JSON.stringify(req.body).replace(/'/g, "''''")+'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('waitinglist_update: ' + err);
        });
};