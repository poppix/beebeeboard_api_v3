var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js'),
    util = require('util');


exports.gantt = function(req, res) {

    sequelize.query('SELECT astaldi_gantt(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.project_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.json({
                'data': data[0].astaldi_gantt
            });

        }).catch(function(err) {
            return res.status(400).render('astaldi_gantt: ' + err);
        });
};