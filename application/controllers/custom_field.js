var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');


exports.find = function(req, res) {

    finds.find_custom_field(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT insert_custom_field(\'' +
                (req.body.data.attributes.value && utility.check_type_variable(req.body.data.attributes.value, 'string') && req.body.data.attributes.value !== null ? req.body.data.attributes.value.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.number_value !== null && !utility.check_id(req.body.data.attributes.number_value) ? req.body.data.attributes.number_value : null) + ',' +
                (req.body.data.attributes.boolean_value !== null && utility.check_type_variable(req.body.data.attributes.boolean_value, 'boolean') ? req.body.data.attributes.boolean_value : null) + ',' +
                (req.body.data.relationships && req.body.data.relationships.organization_custom_field && req.body.data.relationships.organization_custom_field.data && req.body.data.relationships.organization_custom_field.data.id && !utility.check_id(req.body.data.relationships.organization_custom_field.data.id) ? req.body.data.relationships.organization_custom_field.data.id : null) + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_custom_field && datas[0].insert_custom_field != null) {
                    req.body.data.id = datas[0].insert_custom_field;

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'custom_field',
                        'insert',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        datas[0].insert_custom_field,
                        'Aggiunto campo personalizzato ' + datas[0].insert_custom_field
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });

                } else {
                    res.json({
                        'data': [],
                        'relationships': []
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('custom_field_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT delete_custom_fields_relations(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {

                sequelize.query('SELECT update_custom_field(' +
                        req.params.id + ',\'' +
                        (req.body.data.attributes.value && utility.check_type_variable(req.body.data.attributes.value, 'string') && req.body.data.attributes.value !== null ? req.body.data.attributes.value.replace(/'/g, "''''") : '') + '\',' +
                        (req.body.data.attributes.number_value !== null && !utility.check_id(req.body.data.attributes.number_value) ? req.body.data.attributes.number_value : null) + ',' +
                        (req.body.data.attributes.boolean_value !== null && utility.check_type_variable(req.body.data.attributes.boolean_value, 'boolean') ? req.body.data.attributes.boolean_value : null) + ',' +
                        (req.body.data.relationships && req.body.data.relationships.organization_custom_field && req.body.data.relationships.organization_custom_field.data && req.body.data.relationships.organization_custom_field.data.id && !utility.check_id(req.body.data.relationships.organization_custom_field.data.id) ? req.body.data.relationships.organization_custom_field.data.id : null) + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {
                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'custom_field',
                            'udpate',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            req.params.id,
                            'Modificato campo personalizzato ' + req.params.id
                        );
                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    }).catch(function(err) {
                        return res.status(400).render('custom_field_update: ' + err);
                    });
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_custom_field_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'custom_fields',
                    'id': datas[0].delete_custom_field_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('custom_field_destroy: ' + err);
        });
};

exports.get_calc_custom_fields = function(req, res) {

    sequelize.query('SELECT calc_custom_fields(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.user.role_id +',\''+
        JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        })
    .then(function(datas) {
       
        if (datas[0].calc_custom_fields ) {
            res.status(200).send({ 'data': datas[0]['calc_custom_fields'] });
        } else
            return res.status(422).send({});
    }).catch(function(err) {
        return res.status(400).render('calc_custom_fields: ' + err);
    });
   
};