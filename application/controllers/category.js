
/**
 * @apiDefine IdParamEntryError
 *
 * @apiError (Error 422) {422} IdParamEntryError The parameter is not a valid id
 * @apiErrorExample {json} Error-Response:
 * {
 * "errors": [{
 *     "status": '422',
 *     "source": {
 *       "pointer": "/data/attributes/id"
 *     },
 *     "title": "Parametro non valido",
 *     "detail": "Parametro non valido"
 *   }] 
 * }
 */

var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @api{post}/categories 1 - Insert new category
 * @apiName PostCategory
 * @apiGroup Categories
 * @apiVersion 1.0.0
 * 
 * @apiParam{Object} data Category attributes in JSON Api Standard (all attributes can be null)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "data":
 *  {
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New category name",
 *    "rank":2
 *   }
 *  }
 * }
 *
 * @apiSuccess {json} data Category in JSON Api Standard with id
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "data":
 *  {
 *   "id": "12",
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New category name",
 *    "rank":2
 *   }
 *  }
 * }
 *
 */
exports.create = function(req, res) {
    var category = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        category['name'] = (req.body.data.attributes.name && req.body.data.attributes.name !== null && utility.check_type_variable(req.body.data.attributes.name, 'string') ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        category['rank'] = (req.body.data.attributes.rank !== null && utility.check_id(req.body.data.attributes.rank)) ? req.body.data.attributes.rank : null;
        category['_tenant_id'] = req.user._tenant_id;
        category['organization'] = req.headers['host'].split(".")[0];

        sequelize.query('SELECT insert_category(\'' + JSON.stringify(category) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_category && datas[0].insert_category != null) {
                    req.body.data.id = datas[0].insert_category;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_category,
                        'body': req.body.data.relationships,
                        'model': 'category',
                        'user_id': req.user.id,
                        'array': ['sub_category']
                    }, function(err, results) {

                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'category',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_category,
                            'Aggiunta categoria ' + datas[0].insert_category
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });

                    });
                }
            }).catch(function(err) {
                return res.status(400).render('category_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @api{patch}/categories/:id 2 - Update specific category
 * @apiName PatchCategory
 * @apiGroup Categories
 * @apiVersion 1.0.0
 * 
 * @apiParam{Object} data Category attributes in JSON Api Standard (all attributes can be null)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "data":
 *  {
 *   "id": "12",
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New Name for the category",
 *    "rank":4
 *   }
 *  }
 * }
 *
 * @apiSuccess {json} data Category in JSON Api Standard with new fields value
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "data":
 *  {
 *   "id": "12",
 *   "type":"categories",
 *   "attributes":
 *   {
 *    "name":"New Name for the category",
 *    "rank":4
 *   }
 *  }
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT update_category(' +
                req.params.id + ',\'' +
                (req.body.data.attributes.name && req.body.data.attributes.name !== null && utility.check_type_variable(req.body.data.attributes.name, 'string') ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.rank !== null && !utility.check_id(req.body.data.attributes.rank) ? req.body.data.attributes.rank : null) + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                utility.sanitizeRelations({
                    'organization': req.headers['host'].split(".")[0],
                    '_tenant': req.user._tenant_id,
                    'new_id': req.params.id,
                    'body': req.body.data.relationships,
                    'model': 'category',
                    'user_id': req.user.id,
                    'array': ['sub_category']
                }, function(err, results) {

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'category',
                        'update',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes),
                        req.params.id,
                        'Modificato categoria ' + req.params.id
                    );

                    res.json({
                        'data': req.body.data,
                        'relationships': req.body.data.relationships
                    });

                });

            }).catch(function(err) {
                return res.status(400).render('category_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @api{delete}/categories/:id 3 - Delete a specific category
 * @apiName DeleteCategory
 * @apiGroup Categories
 * @apiVersion 1.0.0
 *
 * @apiParam{Number} id The category id
 *
 * @apiSuccess{Obect[]} data 
 * @apiSuccess{Number} data.id Category id
 * @apiSuccess{String} data.type Model name
 * @apiSuccessExample Example data on success
 * {
 *  "data":
 *  {
 *    "type":"categories",
 *    "id":"12"
 *  }
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.destroy = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT expense_article_categories(' +
            req.params.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(cat) {
            if (cat[0].expense_article_categories == 0) {
                sequelize.query('SELECT delete_category_v1(' +
                        req.params.id + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        res.json({
                            'data': {
                                'type': 'categories',
                                'id': datas[0].delete_category_v1
                            }
                        });

                    }).catch(function(err) {
                        return res.status(400).render('category_destroy: ' + err);
                    });
            } else {
                return res.status(422).send(utility.error422('id', 'Ci sono degli acquisti associati a questa categoria', 'Ci sono degli acquisti associati a questa categoria'));
            }
        }).catch(function(err) {
            return res.status(400).render('category_destroy: ' + err);
        });
};