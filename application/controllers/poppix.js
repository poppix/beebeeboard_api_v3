var moment = require('moment'),
    Sequelize = require('sequelize');

exports.grant_access = function(req, res) {
    sequelize.query('SELECT poppix_grant_access(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.status(200).send({ token: data[0].poppix_grant_access });
        }).catch(function(err) {
            return res.status(400).render('poppix_grant_access: ' + err);
        });
};

exports.grant_access_list = function(req, res) {
    sequelize.query('SELECT poppix_grant_access_list(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(data) {
            res.status(200).send(data[0].poppix_grant_access_list[0]);
        }).catch(function(err) {
            return res.status(400).render('poppix_grant_access_list: ' + err);
        });
};

exports.revoke_access = function(req, res) {
    if (!req.body.id) {
        res.status(200).send({});
    }

    sequelize.query('SELECT poppix_revoke_access(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ', ' +
            req.body.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function() {
            res.status(200).send({});
        }).catch(function(err) {
            return res.status(400).render('poppix_revoke_access: ' + err);
        });
};