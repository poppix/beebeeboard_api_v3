var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
        utility = require('../utility/utility.js'),
        logs = require('../utility/logs.js');

exports.request_permission = function(req, res) {
    var request = require('request');
    var redirect_uri = 'http://test.beebeeboard.com:3000/api/v1/paypal/connect?state=' + req.headers['host'].split(".")[0] + '-' + req.user.id;

    console.log(redirect_uri);
    var postData = JSON.stringify({
        'requestEnvelope': {
            'errorLanguage': 'it_IT'
        },
        'scope': 'EXPRESS_CHECKOUT',
        'callback': redirect_uri
    });

    request({
        headers: {
            'Content-Length': postData.length,
            'Content-Type': 'application/json',
            'X-PAYPAL-SECURITY-USERID': 'info-facilitator_api1.poppix.it',
            'X-PAYPAL-SECURITY-PASSWORD': '3E7CEA3MNXHTUYZ3',
            'X-PAYPAL-SECURITY-SIGNATURE': 'Av.9i2azchfYhjyEYnXe202q94pfAoav6oSeAtLcpIOvpFkpDaQ8NFw1',
            'X-PAYPAL-REQUEST-DATA-FORMAT': 'JSON',
            'X-PAYPAL-RESPONSE-DATA-FORMAT': 'JSON',
            'X-PAYPAL-APPLICATION-ID': 'APP-80W284485P519543T'
        },
        uri: 'https://svcs.sandbox.paypal.com/Permissions/RequestPermissions',
        body: postData,
        method: 'POST'
    }, function(err, result, body) {

        if (!err) {
            var json_body = JSON.parse(body);

            if (!json_body.token) {
                console.log(err);
                return res.status(400).render('errore nel connettere paypal: ' + err);
            } else {
                res.redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_grant-permission&request_token=' + json_body.token);
            }
        }
    });
};

exports.connect = function(req, res) {
    var request = require('request');

    if (!req.query.request_token && !req.query.verification_code) {
        return false;
    }

    var postData = JSON.stringify({
        'requestEnvelope': {
            'errorLanguage': 'it_IT'
        },
        'token': req.query.request_token,
        'verifier': req.query.verification_code
    });

    request({
        headers: {
            'Content-Length': postData.length,
            'Content-Type': 'application/json',
            'X-PAYPAL-SECURITY-USERID': 'info-facilitator_api1.poppix.it',
            'X-PAYPAL-SECURITY-PASSWORD': '3E7CEA3MNXHTUYZ3',
            'X-PAYPAL-SECURITY-SIGNATURE': 'Av.9i2azchfYhjyEYnXe202q94pfAoav6oSeAtLcpIOvpFkpDaQ8NFw1',
            'X-PAYPAL-REQUEST-DATA-FORMAT': 'JSON',
            'X-PAYPAL-RESPONSE-DATA-FORMAT': 'JSON',
            'X-PAYPAL-APPLICATION-ID': 'APP-80W284485P519543T'
        },
        uri: 'https://svcs.sandbox.paypal.com/Permissions/GetAccessToken',
        body: postData,
        method: 'POST'
    }, function(err, result, body) {
        console.log(err);
        console.log(body);

        if (!err) {
            var json_body = JSON.parse(body);
            console.log(json_body);
            if (!json_body.token) {
                console.log(err);
                return res.status(400).render('errore nel connettere paypal: ' + err);
            } else {
                let state = req.query.state.split('-');

                sequelize.query('SELECT insert_user_payments_pos(\'' +
                    state[0] + '\',' +
                    state[1] + ',\'paypal\',\'' +
                    json_body.token + '\', \'' +
                    json_body.tokenSecret + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    res.redirect('https://' + state[0] + '.beebeeboard.com');
                });
            }
        }
    });
};

exports.save_preference = function(req, res) {
    sequelize.query('SELECT insert_user_paypal_pos(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.body.enable + ',\'' +
        req.body.mail + '\',\'' +
        req.body.merchant_id + '\',\'' +
        req.body.service + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function() {
        res.status(200).send({});
    });
};