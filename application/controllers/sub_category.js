var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async');
utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.create = function(req, res) {
    var sub_category = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        sub_category['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        sub_category['rank'] = req.body.data.attributes.rank;
        sub_category['_tenant_id'] = req.user._tenant_id;
        sub_category['organization'] = req.headers['host'].split(".")[0];
        sub_category['category_id'] = ((req.body.data.relationships && req.body.data.relationships.category && req.body.data.relationships.category.data && req.body.data.relationships.category.data.id && !utility.check_id(req.body.data.relationships.category.data.id)) ? req.body.data.relationships.category.data.id : null);

        sequelize.query('SELECT insert_sub_category(\'' +
                JSON.stringify(sub_category) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_sub_category && datas[0].insert_sub_category != null) {
                    req.body.data.id = datas[0].insert_sub_category;
                }

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'sub_category',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_sub_category,
                    'Aggiunta sottocategoria ' + datas[0].insert_sub_category
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('sub_category_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_sub_category(' +
                req.params.id + ',\'' +
                (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',' +
                req.body.data.attributes.rank + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                ((req.body.data.relationships && req.body.data.relationships.category && req.body.data.relationships.category.data && req.body.data.relationships.category.data.id && !utility.check_id(req.body.data.relationships.category.data.id)) ? req.body.data.relationships.category.data.id : null) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'sub_category',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificata sottocategoria ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('sub_category_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT expense_article_sub_categories(' +
            req.params.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(cat) {
            if (cat[0].expense_article_sub_categories == 0) {
                sequelize.query('SELECT delete_sub_category_v1(' +
                        req.params.id + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.id + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        res.json({
                            'data': {
                                'type': 'sub_categories',
                                'id': datas[0].delete_sub_category_v1
                            }
                        });
                    }).catch(function(err) {
                        return res.status(400).render('sub_category_destroy: ' + err);
                    });
            } else {
                return res.status(422).send(utility.error422('id', 'Ci sono degli acquisti associati a questa sottocategoria', 'Ci sono degli acquisti associati a questa sottocategoria', '422'));
            }
        }).catch(function(err) {
            return res.status(400).render('sub_category_destroy: ' + err);
        });
};