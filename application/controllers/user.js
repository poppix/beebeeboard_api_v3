const { Route53Client, ChangeResourceRecordSetsCommand } = require('@aws-sdk/client-route-53');

var moment = require('moment'),
    passport = require('passport'),
    Sequelize = require('sequelize'),
    bcrypt = require('bcryptjs'),
    async = require('async'),

    config = require('../../config'),
    utils = require('../../utils'),
    SALT_WORK_FACTOR = 10;
 var https = require('https');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New user, control if username and mail not yet exist in users collection
 * The new user it will be administrator of the company indicated during registration
 * It will be the only that can invite other collaborators
 * API create a new domain for the new company like [company].beebeeboard.com in AWS Route53
 * Send mail to user for confirm the registration
 */
exports.create = function(req, res) {
    var  
        crypto = require('crypto'),
        now = moment(),
        promo_code = null,
        org = {},
        user = {},
        re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!req.body.data || !req.body.data.attributes) {
        return res.status(422).send(utility.error422('organization', 'Richiesta non valida', 'Richiesta non valida'));
    }

    if (!req.body.data.attributes.organization ||
        req.body.data.attributes.organization == undefined ||
        req.body.data.attributes.organization == '' ||
        !utility.check_type_variable(req.body.data.attributes.organization, 'string')) {
        return res.status(422).send(utility.error422('organization', 'Invalid attribute', 'Il campo company non può essere vuoto', '422'));

    } else {
        req.body.data.attributes.organization = req.body.data.attributes.organization.toLowerCase().replace('.', '_').replace(/[^A-Z0-9]+/ig, '');
    }

    if (req.body.data.attributes.promo != '' && req.body.data.attributes.promo != undefined) {
        promo_code = '\'' + req.body.data.attributes.promo + '\'';
    }

    if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
        if (re.test(req.body.data.attributes.mail) != true || !utility.check_type_variable(req.body.data.attributes.mail, 'string')) {
            return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
        }
    } else {
        return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
    }


    sequelize.query('SELECT search_organization(\'' +
        req.body.data.attributes.organization + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (datas[0].search_organization && datas[0].search_organization.id) {
            return res.status(422).send(utility.error422('organization', 'Invalid attribute', 'Company già esistente', '422'));

        } else {

            crypto.randomBytes(40, function(err, buf) {
                var token = buf.toString('hex');

                req.body.data.attributes.organization = req.body.data.attributes.organization.replace(/[^a-zA-Z0-9]/g, '_');
                req.body.data.attributes.create_user_token = token;
                req.body.data.attributes.create_user_expires = moment(new Date(now)).add(7, 'days').format('YYYY/MM/DD HH:mm:ss');
                req.body.data.attributes.organization_admin = true;

                user['create_user_token'] = req.body.data.attributes.create_user_token;
                user['create_user_expires'] = req.body.data.attributes.create_user_expires;
                user['organization_admin'] = req.body.data.attributes.organization_admin;
                user['organization'] = req.body.data.attributes.organization;
                user['mail'] = req.body.data.attributes.mail;
                user['mobile'] = req.body.data.attributes.mobile;
                user['socket_room'] = req.body.data.attributes.organization;

                org['organization'] = req.body.data.attributes.organization;
                org['contact_name'] = 'contatto';
                org['product_name'] = 'prodotto';
                org['estimate_name'] = 'preventivo';
                org['invoice_name'] = 'fattura';
                org['dashboard_name'] = 'dashboard';
                org['expense_name'] = 'acquisto';
                org['event_name'] = 'evento';
                org['tracker_name'] = 'ora';
                org['cost_name'] = 'pagamento';
                org['contacts_name'] = 'contatti';
                org['products_name'] = 'prodotti';
                org['estimates_name'] = 'preventivi';
                org['invoices_name'] = 'fatture';
                org['expenses_name'] = 'acquisti';
                org['events_name'] = 'eventi';
                org['trackers_name'] = 'ore';
                org['project_name'] = 'progetto';
                org['projects_name'] = 'progetti';
                org['receipt_name'] = 'ricevuta';
                org['receipts_name'] = 'ricevute';
                org['costs_name'] = 'pagamenti';
                org['new_registration'] = false;
                org['sms_remained'] = 0;
                org['name'] = req.body.data.attributes.organization;
                org['redirect_url'] = 'https://user_' + req.body.data.attributes.organization + '.beebeeboard.com';

                sequelize.query('SELECT insert_organization(\'' + JSON.stringify(org) + '\',\'' + req.body.data.attributes.organization + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                    .then(function(datas) {

                        if (datas[0].insert_organization && datas[0].insert_organization != null) {
                            sequelize.query('SELECT search_organization(\'' +
                                req.body.data.attributes.organization + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(org) {

                                req.body.data.attributes.organization_id = org[0].search_organization.id;
                                user['_tenant_id'] = org[0].search_organization._tenant_id;
                                user['organization_id'] = org[0].search_organization.id;
                                user['role_id'] = org[0].search_organization.role_id;
                                async.parallel({
                                    user: function(callback) {
                                        sequelize.query('SELECT insert_user_v1(\'' +
                                                JSON.stringify(user) + '\',\'' +
                                                req.body.data.attributes.organization + '\',' +
                                                promo_code + ');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {


                                                if (datas[0].insert_user_v1 && datas[0].insert_user_v1 != null) {
                                                    sequelize.query('SELECT insert_poppix_client(' +
                                                            datas[0].insert_user_v1 + ',\'' +
                                                            req.body.data.attributes.organization + '\',\'' +
                                                            req.body.data.attributes.mail + '\','+
                                                            (req.body.data.attributes.mobile ? '\''+req.body.data.attributes.mobile+'\'' : null)+');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(new_poppix_user) {});
                                                    req.body.data.id = datas[0].insert_user_v1;

                                                    /**
                                                     * Create RecordSet in AWS Amazon S53 for subdomain: ex: poppix.beebeeboard.com
                                                     */
                                                    const route53 = new Route53Client({
                                                        accessKeyId: config.aws_route53.accessKeyId,
                                                        secretAccessKey: config.aws_route53.secretAccessKey,
                                                        region: config.aws_route53.region
                                                    });

                                                    
                                                    

                                                    //var route53 = new AWS.Route53();
                                                    var params = {
                                                        "HostedZoneId": config.aws_route53.hostedZoneId, // our Id from the first call
                                                        "ChangeBatch": {
                                                            "Changes": [{
                                                                "Action": "CREATE",
                                                                "ResourceRecordSet": {
                                                                    "Name": req.body.data.attributes.organization + ".beebeeboard.com",
                                                                    "Type": "CNAME",
                                                                    "TTL": 300,
                                                                    "ResourceRecords": [{
                                                                        "Value": 'appv5.beebeeboard.com'
                                                                    }],
                                                                    /*
                                                                        "AliasTarget" : {
                                                                            "DNSName": "app.beebeeboard.com",
                                                                            "HostedZoneId" : config.aws_route53.hostedZoneId,
                                                                            "EvaluateTargetHealth": false
                                                                        }*/
                                                                }
                                                            }]
                                                        }
                                                    };
                                                    const command = new ChangeResourceRecordSetsCommand(params);
                                                    route53.send(command, function(err, data) {
                                                    //route53.changeResourceRecordSets(params, function(err, data) {
                                                        if (err) {
                                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Create Account -> Route53 Amazon create RecordSet Error:  ' + err);
                                                            return res.status(400).render('user_insert_route53_fail: ' + err);
                                                        }

                                                        // Save in database a new organization created with the user_mail and the registration_token
                                                        sequelize.query('SELECT insert_registration(\'' +
                                                                req.body.data.attributes.organization + '\',\'https://' +
                                                                req.body.data.attributes.organization + '.beebeeboard.com\',\'' +
                                                                req.body.data.attributes.mail + '\',\'' +
                                                                req.body.data.attributes.create_user_token + '\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {

                                                                callback(null, 1);
                                                            }).catch(function(err) {
                                                                console.log(err);
                                                                callback(err, null);
                                                            });


                                                    });
                                                } else {
                                                    callback(null, null);
                                                }

                                            }).catch(function(err) {
                                                console.log(err);
                                                callback(err, null);
                                            });
                                    }
                                }, function(err, results) {
                                    if (err) {
                                        return res.status(400).render('user_insert: ' + err);
                                    }

                                    res.json({
                                        'data': req.body.data,
                                        'relationships': req.body.data.relationships
                                    });
                                });
                            }).catch(function(err) {
                                return res.status(400).render('user_insert: ' + err);
                            });

                        }
                    }).catch(function(err) {
                        return res.status(400).render('user_insert: ' + err);
                    });
            });

        }
    }).catch(function(err) {
        return res.status(400).render('user_insert: ' + err);
    });
};

exports.create_clinic = function(req, res) {
    var 
        crypto = require('crypto'),
        now = moment(),
        org = {},
        promo_code = null,
        user = {},
        re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!req.body.data || !req.body.data.attributes) {
        return res.status(422).send(utility.error422('organization', 'Richiesta non valida', 'Richiesta non valida'));
    }

    if (!req.body.data.attributes.organization ||
        req.body.data.attributes.organization == undefined ||
        req.body.data.attributes.organization == '' ||
        !utility.check_type_variable(req.body.data.attributes.organization, 'string')) {
        return res.status(422).send(utility.error422('organization', 'Invalid attribute', 'Il campo company non può essere vuoto', '422'));

    } else {
        req.body.data.attributes.organization = req.body.data.attributes.organization.toLowerCase().replace('.', '_').replace(/[^A-Z0-9]+/ig, '');
    }

    if (req.body.data.attributes.promo != '' && req.body.data.attributes.promo != undefined) {
        promo_code = '\'' + req.body.data.attributes.promo + '\'';
    }

    if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
        if (re.test(req.body.data.attributes.mail) != true || !utility.check_type_variable(req.body.data.attributes.mail, 'string')) {
            return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
        }
    } else {
        return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
    }

    sequelize.query('SELECT search_organization(\'' +
        req.body.data.attributes.organization + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (datas[0].search_organization && datas[0].search_organization.id) {
            return res.status(422).send(utility.error422('organization', 'Invalid attribute', 'Company già esistente', '422'));

        } else {

            crypto.randomBytes(40, function(err, buf) {
                var token = buf.toString('hex');

                req.body.data.attributes.organization = req.body.data.attributes.organization.replace(/[^a-zA-Z0-9]/g, '_');
                req.body.data.attributes.create_user_token = token;
                req.body.data.attributes.create_user_expires = moment(new Date(now)).add(7, 'days').format('YYYY/MM/DD HH:mm:ss');
                req.body.data.attributes.organization_admin = true;

                user['create_user_token'] = req.body.data.attributes.create_user_token;
                user['create_user_expires'] = req.body.data.attributes.create_user_expires;
                user['organization_admin'] = req.body.data.attributes.organization_admin;
                user['organization'] = req.body.data.attributes.organization;
                user['mail'] = req.body.data.attributes.mail;
                user['mobile'] = req.body.data.attributes.mobile;
                user['socket_room'] = req.body.data.attributes.organization;

                org['organization'] = req.body.data.attributes.organization;
                org['contact_name'] = 'contatto';
                org['product_name'] = 'prodotto';
                org['estimate_name'] = 'preventivo';
                org['invoice_name'] = 'fattura';
                org['dashboard_name'] = 'dashboard';
                org['expense_name'] = 'acquisto';
                org['event_name'] = 'appuntamento';
                org['tracker_name'] = 'outcome';
                org['cost_name'] = 'pagamento';
                org['contacts_name'] = 'contatti';
                org['products_name'] = 'prodotti';
                org['estimates_name'] = 'preventivi';
                org['invoices_name'] = 'fatture';
                org['expenses_name'] = 'acquisti';
                org['events_name'] = 'appuntamenti';
                org['trackers_name'] = 'outcomes';
                org['project_name'] = 'valutazione';
                org['projects_name'] = 'valutazioni';
                org['costs_name'] = 'pagamenti';
                org['receipt_name'] = 'ricevuta';
                org['receipts_name'] = 'ricevute';
                org['new_registration'] = false;
                org['sms_remained'] = 0;
                org['name'] = req.body.data.attributes.organization;
                org['redirect_url'] = 'https://user_' + req.body.data.attributes.organization + '.beebeeboard.com';

                sequelize.query('SELECT insert_fisio_organization(\'' + JSON.stringify(org) + '\',\'' + req.body.data.attributes.organization + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                    .then(function(datas) {

                        if (datas[0].insert_fisio_organization && datas[0].insert_fisio_organization != null) {
                            sequelize.query('SELECT search_organization(\'' +
                                req.body.data.attributes.organization + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(org) {

                                req.body.data.attributes.organization_id = org[0].search_organization.id;
                                user['_tenant_id'] = org[0].search_organization._tenant_id;
                                user['organization_id'] = org[0].search_organization.id;
                                user['contact_status_id'] = 120;
                                user['role_id'] = org[0].search_organization.role_id;
                                async.parallel({
                                    user: function(callback) {
                                        sequelize.query('SELECT insert_fisio_user_v1(\'' +
                                                JSON.stringify(user) + '\',\'' +
                                                req.body.data.attributes.organization + '\',' +
                                                promo_code + ');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {


                                                if (datas[0].insert_fisio_user_v1 && datas[0].insert_fisio_user_v1 != null) {
                                                    sequelize.query('SELECT insert_poppix_client(' +
                                                            datas[0].insert_fisio_user_v1 + ',\'' +
                                                            req.body.data.attributes.organization + '\',\'' +
                                                            req.body.data.attributes.mail + '\','+
                                                            (req.body.data.attributes.mobile ? '\''+req.body.data.attributes.mobile+'\'' : null)+');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(new_poppix_user) {});
                                                    req.body.data.id = datas[0].insert_fisio_user_v1;

                                                    /**
                                                     * Create RecordSet in AWS Amazon S53 for subdomain: ex: poppix.beebeeboard.com
                                                     */
                                                    const route53 = new Route53Client({
                                                        accessKeyId: config.aws_route53.accessKeyId,
                                                        secretAccessKey: config.aws_route53.secretAccessKey,
                                                        region: config.aws_route53.region
                                                    });

                                                    var params = {
                                                        "HostedZoneId": config.aws_route53.hostedZoneId, // our Id from the first call
                                                        "ChangeBatch": {
                                                            "Changes": [{
                                                                "Action": "CREATE",
                                                                "ResourceRecordSet": {
                                                                    "Name": req.body.data.attributes.organization + ".beebeeboard.com",
                                                                    "Type": "CNAME",
                                                                    "TTL": 300,
                                                                    "ResourceRecords": [{
                                                                        "Value": 'appv5.beebeeboard.com'
                                                                    }],
                                                                    /*
                                                                        "AliasTarget" : {
                                                                            "DNSName": "app.beebeeboard.com",
                                                                            "HostedZoneId" : config.aws_route53.hostedZoneId,
                                                                            "EvaluateTargetHealth": false
                                                                        }*/
                                                                }
                                                            }]
                                                        }
                                                    };

                                                    const command = new ChangeResourceRecordSetsCommand(params);
                                                    route53.send(command, function(err, data) {
                                                   // route53.changeResourceRecordSets(params, function(err, data) {
                                                        if (err) {
                                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Create Account -> Route53 Amazon create RecordSet Error:  ' + err);
                                                            return res.status(400).render('user_insert_route53_fail: ' + err);
                                                        }

                                                        // Save in database a new organization created with the user_mail and the registration_token
                                                        sequelize.query('SELECT insert_registration(\'' +
                                                                req.body.data.attributes.organization + '\',\'https://' +
                                                                req.body.data.attributes.organization + '.beebeeboard.com\',\'' +
                                                                req.body.data.attributes.mail + '\',\'' +
                                                                req.body.data.attributes.create_user_token + '\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {

                                                                callback(null, 1);
                                                            }).catch(function(err) {
                                                                console.log(err);
                                                                callback(err, null);
                                                            });


                                                    });
                                                } else {
                                                    callback(null, null);
                                                }

                                            }).catch(function(err) {
                                                console.log(err);
                                                callback(err, null);
                                            });
                                    }
                                }, function(err, results) {
                                    if (err) {
                                        return res.status(400).render('user_insert: ' + err);
                                    }

                                    res.json({
                                        'data': req.body.data,
                                        'relationships': req.body.data.relationships
                                    });
                                });
                            }).catch(function(err) {
                                return res.status(400).render('user_insert: ' + err);
                            });

                        }
                    }).catch(function(err) {
                        return res.status(400).render('user_insert: ' + err);
                    });
            });

        }
    }).catch(function(err) {
        return res.status(400).render('user_insert: ' + err);
    });
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * API search in users collection if exist a user with the token passed by argument, if exist return the user
 * 
 */
exports.registration_confirm = function(req, res) {
    if (!utility.check_type_variable(req.params.token, 'string')) {
        return res.status(422).send(utility.error422('token', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT registration_confirm_v1(\'' +
        req.headers['host'].split(".")[0] + '\',\'' +
        req.params.token + '\',\'' +
        moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].registration_confirm_v1) {
            return res.status(422).send(utility.error422('organization', 'Invalid Attribute', 'Nessun utente trovato per questa company con un token valido', '422'));

        } else {
            res.json({
                'data': {
                    'user': {
                        'id': datas[0].registration_confirm_v1.id,
                        'mail': datas[0].registration_confirm_v1.mail,
                        'fresh_restore_id': datas[0].registration_confirm_v1.fresh_restore_id,
                        'redirect': datas[0].registration_confirm_v1.redirect_page,
                        'name': datas[0].registration_confirm_v1.name,
                        'first_name': datas[0].registration_confirm_v1.first_name,
                        'last_name': datas[0].registration_confirm_v1.last_name,
                        'organization_admin': datas[0].registration_confirm_v1.organization_admin,
                        'organization': req.headers['host'].split(".")[0],
                        'username': datas[0].registration_confirm_v1.username
                    }
                }
            });
        }
    }).catch(function(err) {
        return res.status(400).render('user_registration_confirm: ' + err);
    });
};

exports.verify_mail_confirm = function(req, res) {
    if (!utility.check_type_variable(req.params.token, 'string')) {
        res.redirect('https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/#/verify_error');
    }

    sequelize.query('SELECT registration_confirm_v1(\'' +
        req.headers['host'].split(".")[0] + '\',\'' +
        req.params.token + '\',\'' +
        moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].registration_confirm_v1) {
            return res.status(422).send(utility.error422('organization', 'Invalid Attribute', 'Nessun utente trovato per questa company con un token valido', '422'));

        } else {
            sequelize.query('SELECT verify_mail(\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                req.params.token + '\',\'' +
                moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(datas) {
                res.redirect('https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/#/verify_success');

            }).catch(function(err) {
                res.redirect('https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/#/verify_error');
            });
        }
    }).catch(function(err) {
        return res.status(400).render('verify_mail: ' + err);
    });
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * Refresh Token expiration for registration user
 * 
 */
exports.refresh = function(req, res) {
    var now = moment();
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!utility.check_type_variable(req.params.token, 'string')) {
        return res.status(422).send(utility.error422('token', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT refresh_token_expire_v1(\'' +
        req.headers['host'].split(".")[0] + '\',\'' +
        req.params.token + '\',\'' +
        (now + 600000) + '\',' +
        req.user._tenant_id + ',' +
        req.params.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].refresh_token_expire_v1) {
            return res.status(422).send(utility.error422('organization', 'Invalid Attribute', 'Nessun utente trovato per questa company con un token valido', '422'));
        }

        res.json({
            'data': req.body.data,
            'relationships': req.body.data.relationships
        });

    }).catch(function(err) {
        return res.status(400).render('user_refresh: ' + err);
    });
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * API save the new user with password crypted and send a mail to user and to organization admin for the correct operation
 * 
 */
exports.registration = function(req, res) {
    var mandrill = require('mandrill-api/mandrill');

    if (!req.body.data || !req.body.data.attributes) {
        return res.status(422).send(utility.error422('organization', 'Richiesta non valida', 'Richiesta non valida'));
    }

    if (!utility.check_type_variable(req.params.token, 'string')) {
        return res.status(422).send(utility.error422('token', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!req.body.data.attributes ||
        !utility.check_type_variable(req.body.data.attributes.username, 'string')) {
        return res.status(422).send(utility.error422('username', 'Parametro non valido', 'Parametro non valido', '422'));
    }
   
    if (!req.body.data.attributes ||
        !utility.check_type_variable(req.body.data.attributes.password, 'string') ||
        req.body.data.attributes.password == null ||
        req.body.data.attributes.password == '') {
        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!req.body.data.attributes ||
        !utility.check_type_variable(req.body.data.attributes.first_name, 'string')) {
        return res.status(422).send(utility.error422('first_name', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!req.body.data.attributes ||
        !utility.check_type_variable(req.body.data.attributes.last_name, 'string')) {
        return res.status(422).send(utility.error422('last_name', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT registration_user_confirm_v1(\'' +
        req.headers['host'].split(".")[0] + '\',\'' +
        req.params.token + '\',\'' +
        moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].registration_user_confirm_v1) {
            return res.status(422).send(utility.error422('organization', 'Invalid Attribute', 'Nessun utente trovato per questa company con un token valido', '422'));

        }

        mail = datas[0].registration_user_confirm_v1.mail;

        sequelize.query('SELECT check_username_v1(\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            req.body.data.attributes.username + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(mails) {
            if (mails[0].check_username_v1 && !req.body.data.attributes.fixed_username) {
                return res.status(422).send(utility.error422('username', 'Username già esistente', 'Username già esistente', '422'));

            } else if (req.body.data.attributes.username == null || req.body.data.attributes.username == undefined || req.body.data.attributes.username == '') {
                return res.status(422).send(utility.error422('username', 'Invalid Attribute', 'Il campo username non può essere vuoto', '422'));
            } else {

                new_password = req.body.data.attributes.password;

                if (new_password !== null && new_password !== '' && new_password !== undefined) {
                    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                        if (err) {
                            return res.status(400).render('user_registration: ' + err);
                        }
                        // hash the password using our new salt

                        bcrypt.hash(new_password, salt, function(err, hash) {
                            if (err) {
                                return res.status(400).render('user_registration: ' + err);
                            }

                            async.parallel({
                                user_update: function(callback) {
                                    sequelize.query('SELECT update_user_registration_v3(' +
                                        datas[0].registration_user_confirm_v1.id + ',' +
                                        datas[0].registration_user_confirm_v1._tenant + ',\'' +
                                        req.headers['host'].split(".")[0] + '\',\'' +
                                        (req.body.data.attributes.username !== null ? req.body.data.attributes.username.replace("'", "") : '') + '\',\'' +
                                        (hash !== null ? hash : '') + '\',null,null,' +
                                        1 + ',' + 30 + ',\'unit\',' + '\'Europe/Rome\',\'' +
                                        req.body.data.attributes.first_name.replace(/'/g, "''''") + '\',\'' +
                                        req.body.data.attributes.last_name.replace(/'/g, "''''") + '\',\'' +
                                        JSON.stringify(req.body.data.attributes.gdpr).replace(/'/g, "''''") + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        }).then(function(user_update) {
                                        callback(null, datas[0].registration_user_confirm_v1.id);
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback(err, null);
                                    });
                                }
                            }, function(err, results) {
                                if (err) {
                                    return res.status(400).render('user_registration: ' + err);
                                }

                                var template_content = [{
                                        "name": "LANG",
                                        "content": datas[0].registration_user_confirm_v1.lang
                                    }, {
                                        "name": "FIRST_NAME",
                                        "content": req.body.data.attributes.first_name
                                    }, {
                                        "name": "LAST_NAME",
                                        "content": req.body.data.attributes.last_name
                                    }, {
                                        "name": "NAME",
                                        "content": req.body.data.attributes.username
                                    }, {
                                        "name": "ORGANIZATION",
                                        "content": uc_first(datas[0].registration_user_confirm_v1.name)
                                    }, {
                                        "name": "URL",
                                        "content": 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com'
                                    }, {
                                        "name": "COLOR",
                                        "content": datas[0].registration_user_confirm_v1.primary_color ? datas[0].registration_user_confirm_v1.primary_color : '#00587D'
                                    }, {
                                        "name": "LOGO",
                                        "content": datas[0].registration_user_confirm_v1.thumbnail_url ? 'https://data.beebeeboard.com/' + datas[0].registration_user_confirm_v1.thumbnail_url + '-small.jpeg' : null
                                    }],
                                    message = {
                                        "global_merge_vars": template_content,
                                        "metadata": {
                                            "organization": req.headers['host'].split(".")[0]
                                        },
                                        "subject": datas[0].registration_user_confirm_v1.organization_admin ? 'Benvenuto in Beebeeboard!' : (uc_first(datas[0].registration_user_confirm_v1.name) + ' Registrazione Completata'),
                                        "from_email": "no-reply@beebeemailer.com",
                                        "from_name": datas[0].registration_user_confirm_v1.organization_admin ? 'Beebeeboard' : (uc_first(datas[0].registration_user_confirm_v1.name)),
                                        "to": [{
                                            "email": datas[0].registration_user_confirm_v1.mail,
                                            "name": req.body.data.attributes.first_name,
                                            "type": "to"
                                        }],
                                        "headers": {
                                            "Reply-To": "no-reply@beebeemailer.com"
                                        },
                                        "tags": ["registration-completed", req.headers['host'].split(".")[0]],
                                        "subaccount": "beebeeboard",
                                        "tracking_domain": "mandrillapp.com",
                                        "google_analytics_domains": ["poppix.it"]
                                    };

                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                mandrill_client.messages.sendTemplate({
                                    "template_name": datas[0].registration_user_confirm_v1.organization_admin ? "bbh-onboarding-welcome" : "registration-completed",
                                    "template_content": template_content,
                                    "message": message,
                                    "async": true,
                                    "send_at": false
                                }, function(result) {
                                    /*
                                     [{
                                     "email": "recipient.email@example.com",
                                     "status": "sent",
                                     "reject_reason": "hard-bounce",
                                     "_id": "abc123abc123abc123abc123abc123"
                                     }]
                                     */
                                    if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                        console.log("Registration Completed mail sended correctly");


                                    } else {
                                        console.log("Mail rejected to " + datas[0].registration_user_confirm_v1.mail + " for reason: " + result[0].reject_reason);

                                    }




                                }, function(e) {
                                    // Mandrill returns the error as an object with name and message keys
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'

                                });

                                var token = utils.uid(config.token.accessTokenLength);
                                var redirect_page = datas[0].registration_user_confirm_v1.redirect_page;
                                console.log(redirect_page)

                                if (redirect_page === undefined || redirect_page === '' || redirect_page === null)
                                    redirect_page = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/start';
                                else if (redirect_page && redirect_page.indexOf('http') == -1)
                                    redirect_page = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/' + redirect_page;


                                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                    moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                    datas[0].registration_user_confirm_v1.id + ',' +
                                    5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(tok) {

                                    sequelize.query('SELECT insert_login(' + datas[0].registration_user_confirm_v1.id + ',\'' + req.headers['host'].split(".")[0] + '\',\'' + req.headers['user-agent'] + '\',\'' + req.connection.remoteAddress + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(logins) {
                                        res.status(200).send({
                                            'access_token': token,
                                            'url': redirect_page,
                                            'redirect': datas[0].registration_user_confirm_v1.redirect_page,
                                            'organization': req.headers['host'].split(".")[0],
                                            'organization_admin': datas[0].registration_user_confirm_v1.organization_admin,
                                            'name': req.body.data.attributes.first_name + ' ' + req.body.data.attributes.last_name,
                                            'mail': datas[0].registration_user_confirm_v1.mail,
                                            'id': datas[0].registration_user_confirm_v1.id,
                                            'first_name': datas[0].registration_user_confirm_v1.first_name,
                                            'last_name': datas[0].registration_user_confirm_v1.last_name
                                        });
                                    }).catch(function(err) {
                                        return res.status(400).render('user_registration: ' + err);
                                    });


                                }).catch(function(err) {
                                    return res.status(400).render('user_registration: ' + err);
                                });
                            });
                        });
                    });
                } else {
                    return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Password non valida', '422'));
                }
            }
        }).catch(function(err) {
            return res.status(400).render('user_registration: ' + err);
        });
    }).catch(function(err) {
        return res.status(400).render('user_registration: ' + err);
    });
};

exports.user_registration_request = function(req, res) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        mandrill = require('mandrill-api/mandrill'),
        crypto = require('crypto'),
        now = moment(),
        user = {};

    if (!req.body.data || !req.body.data.attributes) {
        return res.status(422).send(utility.error422('organization', 'Richiesta non valida', 'Richiesta non valida'));
    }

    if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
        if (re.test(req.body.data.attributes.mail) != true) {
            return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
        }
    } else {
        return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
    }

    sequelize.query('SELECT search_organization_client(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].search_organization_client && !datas[0].search_organization_client.id) {
            return res.status(422).send(utility.error422('organization', 'Parametro non valido', 'Company non trovata', '422'));
        } else {
            sequelize.query('SELECT check_mail_registration(\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                req.body.data.attributes.mail + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(mails) {
                if (mails[0].check_mail_registration) {
                    
                    req.body.data.attributes.organization_id = datas[0].search_organization_client.id;
                    var _tenant = datas[0].search_organization_client._tenant_id;
                    sequelize.query('SELECT registration_username(\'' +
                        req.headers['host'].split(".")[0] + '\',\'' +
                        req.body.data.attributes.mail + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(datas) {
                        console.log(datas[0].registration_username);
                        if (datas[0].registration_username[0] && !datas[0].registration_username[0].username) {
                            crypto.randomBytes(40, function(err, buf) {
                                var token = buf.toString('hex');

                                req.body.data.attributes.create_user_token = token;
                                req.body.data.attributes.create_user_expires = moment(new Date(now)).add(7, 'days').format('YYYY/MM/DD HH:mm:ss'); // 1 day
                                req.body.data.attributes.organization_admin = false;
                                req.body.data.attributes.organization = req.headers['host'].split(".")[0];


                                sequelize.query('SELECT update_contact_request_registration(' +
                                        datas[0].registration_username[0].id + ',\'' +
                                        req.body.data.attributes.create_user_token + '\',\'' +
                                        req.body.data.attributes.create_user_expires + '\',' +
                                        req.body.data.attributes.organization_id + ',\'' +
                                        req.body.data.attributes.organization + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(upd_cont) {

                                        var template = '';
                                        var subject = '';
                                        req.body.data.id = req.body.data.attributes.contact_id;

                                        // Create registration record
                                        if (req.body.data.attributes.contact_id) {
                                            template = "invitation";
                                            subject = " ti ha mandato un invito";
                                        } else {
                                            template = "create-account";
                                            subject = " Creazione account";
                                        }
                                        var template_content = [{
                                                "name": "LANG",
                                                "content": datas[0].registration_username[0].lang
                                            }, {
                                                "name": "NAME",
                                                "content": req.body.data.attributes.mail
                                            }, {
                                                "name": "URL",
                                                "content": 'https://' + req.body.data.attributes.organization + '.beebeeboard.com/#/confirm/' + req.body.data.attributes.create_user_token
                                            }, {
                                                "name": "ORGANIZATION",
                                                "content": uc_first(datas[0].registration_username[0].organization_name)
                                            }, {
                                                "name": "LOGO",
                                                "content": datas[0].registration_username[0].thumbnail_url ? 'http://data.beebeeboard.com/' + datas[0].registration_username[0].thumbnail_url + '-small.jpeg' : null
                                            }, {
                                                "name": "COLOR",
                                                "content": datas[0].registration_username[0].primary_color ? datas[0].registration_username[0].primary_color : '#00587D'
                                            }],
                                            message = {
                                                "global_merge_vars": template_content,
                                                "metadata": {
                                                    "organization": req.headers['host'].split(".")[0]
                                                },
                                                "subject": uc_first(datas[0].registration_username[0].organization_name) + subject,
                                                "from_email": "no-reply@beebeemailer.com",
                                                "from_name": uc_first(datas[0].registration_username[0].organization_name),
                                                "to": [{
                                                    "email": req.body.data.attributes.mail,
                                                    "type": "to"
                                                }],
                                                "headers": {
                                                    "Reply-To": "no-reply@beebeemailer.com"
                                                },
                                                "tags": [template, req.headers['host'].split(".")[0]],
                                                "subaccount": "beebeeboard",
                                                "tracking_domain": "mandrillapp.com",
                                                "google_analytics_domains": ["poppix.it"]
                                            };

                                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                        mandrill_client.messages.sendTemplate({
                                            "template_name": template,
                                            "template_content": template_content,
                                            "message": message,
                                            "async": true,
                                            "send_at": false
                                        }, function(result) {

                                            //result[0]._tenant_id = data.tenant_id;

                                            if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                                console.log("New account mail sended correctly");
                                                return res.status(200).send();

                                            }
                                        }, function(e) {
                                            // Mandrill returns the error as an object with name and message keys
                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ');
                                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                            return res.status(400).render('user_registration_request: ' + e);
                                        });

                                    }).catch(function(err) {
                                        return res.status(400).render('user_registration_request: ' + err);
                                    });
                            });
                        } else if (datas[0].registration_username[0] && datas[0].registration_username[0].username) {
                            // REIMPOSTA PASSWORD
                            sequelize.query('SELECT user_forgot_password_v1(\'' +
                                    req.body.data.attributes.mail + '\',\'' +
                                    req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {

                                    if (datas[0].user_forgot_password_v1 && datas[0].user_forgot_password_v1.id) {
                                        crypto.randomBytes(40, function(err, buf) {
                                            var token = buf.toString('hex');
                                            sequelize.query('SELECT update_user_forgot_password_v1(' +
                                                    datas[0].user_forgot_password_v1.id + ',\'' +
                                                    req.headers['host'].split(".")[0] + '\',\'' +
                                                    token + '\',\'' +
                                                    moment(new Date(now)).add(2, 'days').format('YYYY/MM/DD HH:mm:ss') + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas_update) {

                                                    var template_content = [{
                                                            "name": "LANG",
                                                            "content": datas[0].user_forgot_password_v1.lang
                                                        }, {
                                                            "name": "NAME",
                                                            "content": datas[0].user_forgot_password_v1.name ? datas[0].user_forgot_password_v1.name : '',
                                                        }, {
                                                            "name": "URL",
                                                            "content": 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/#/reset/' + token
                                                        }, {
                                                            "name": "ORGANIZATION",
                                                            "content": uc_first(datas[0].user_forgot_password_v1.organization_name)
                                                        }, {
                                                            "name": "USERNAME",
                                                            "content": datas[0].user_forgot_password_v1.username
                                                        }, {
                                                            "name": "COLOR",
                                                            "content": datas[0].user_forgot_password_v1.primary_color ? datas[0].user_forgot_password_v1.primary_color : '#00587D'
                                                        }, {
                                                            "name": "LOGO",
                                                            "content": datas[0].user_forgot_password_v1.org_thumb ? 'https://data.beebeeboard.com/' + datas[0].user_forgot_password_v1.org_thumb + '-small.jpeg' : null
                                                        }],
                                                        message = {
                                                            "global_merge_vars": template_content,
                                                            "metadata": {
                                                                "organization": req.headers['host'].split(".")[0]
                                                            },
                                                            "subject": uc_first(datas[0].user_forgot_password_v1.organization_name) + ' Reimposta Password',
                                                            "from_email": "no-reply@beebeemailer.com",
                                                            "from_name": uc_first(datas[0].user_forgot_password_v1.organization_name),
                                                            "to": [{
                                                                "email": datas[0].user_forgot_password_v1.mail,
                                                                "name": datas[0].user_forgot_password_v1.name ? datas[0].user_forgot_password_v1.name : '',
                                                                "type": "to"
                                                            }],
                                                            "headers": {
                                                                "Reply-To": "no-reply@beebeemailer.com"
                                                            },
                                                            "tags": ["password-recovery", req.headers['host'].split(".")[0]],
                                                            "subaccount": "beebeeboard",
                                                            "tracking_domain": "mandrillapp.com",
                                                            "google_analytics_domains": ["poppix.it"]
                                                        };

                                                    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                                    mandrill_client.messages.sendTemplate({
                                                        "template_name": "password-recovery",
                                                        "template_content": template_content,
                                                        "message": message,
                                                        "async": true,
                                                        "send_at": false
                                                    }, function(result) {
                                                        /*
                                                         [{
                                                         "email": "recipient.email@example.com",
                                                         "status": "sent",
                                                         "reject_reason": "hard-bounce",
                                                         "_id": "abc123abc123abc123abc123abc123"
                                                         }]
                                                         */

                                                        if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                                            console.log("Recovery mail sended correctly");

                                                            res.status(200).send({});

                                                        }
                                                    }, function(e) {
                                                        // Mandrill returns the error as an object with name and message keys
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                        return res.status(400).render('user_forgot_password: ' + e);
                                                    });

                                                }).catch(function(err) {
                                                    return res.status(400).render('user_forgot_password: ' + err);
                                                });

                                        });
                                    } else {
                                        return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Mail non presente oppure account già presente per questa mail', '422'));

                                    }


                                }).catch(function(err) {
                                    return res.status(400).render('user_forgot_password: ' + err);
                                });
                        } else {
                            return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Mail già esistente', '422'));
                        }
                    }).catch(function(err) {
                        return res.status(400).render('user_registration_request: ' + err);
                    });
                } else {
                    crypto.randomBytes(40, function(err, buf) {
                        var token = buf.toString('hex');

                        req.body.data.attributes.create_user_token = token;
                        req.body.data.attributes.create_user_expires = moment(new Date(now)).add(7, 'days').format('YYYY/MM/DD HH:mm:ss'); // 1 day
                        req.body.data.attributes.organization_admin = false;
                        req.body.data.attributes.organization = req.headers['host'].split(".")[0];
                        req.body.data.attributes.organization_id = datas[0].search_organization_client.id;
                        req.body.data.attributes.organization_name = datas[0].search_organization_client.name;
                        req.body.data.attributes.primary_color = datas[0].search_organization_client.primary_color;
                        req.body.data.attributes.thumbnail_url = datas[0].search_organization_client.thumbnail_url;

                        var _tenant = datas[0].search_organization_client._tenant_id;

                        if (!req.body.data.attributes.contact_id) {
                            user['create_user_token'] = req.body.data.attributes.create_user_token;
                            user['create_user_expires'] = req.body.data.attributes.create_user_expires;
                            user['organization_admin'] = req.body.data.attributes.organization_admin;
                            user['organization'] = req.body.data.attributes.organization;
                            user['organization_id'] = req.body.data.attributes.organization_id;
                            user['mail'] = req.body.data.attributes.mail;
                            user['redirect_url'] = (datas[0].search_organization_client.redirect_url ? datas[0].search_organization_client.redirect_url : null);
                            user['_tenant_id'] = datas[0].search_organization_client._tenant_id;
                            user['socket_room'] = req.body.data.attributes.organization;
                            //user['role_id'] = datas[0].search_organization_client.role_id;
                            user['contact_status_id'] = datas[0].search_organization_client.contact_status_default;
                            sequelize.query('SELECT insert_user_v1(\'' +
                                    JSON.stringify(user) + '\',\'' +
                                    req.headers['host'].split(".")[0] + '\',null);', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {
                                    sequelize.query('SELECT contact_custom_fields_trigger(' +
                                            datas[0].insert_user_v1 + ',' +
                                            user['_tenant_id'] + ',\'' +
                                            req.body.data.attributes.organization + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(cus) {
                                            var template = '';
                                            var subject = '';
                                            req.body.data.id = datas[0].insert_user_v1;
                                            // Create registration record
                                            if (req.body.data.attributes.contact_id) {
                                                template = "invitation";
                                                subject = " ti ha mandato un invito";
                                            } else {
                                                template = "create-account";
                                                subject = " Creazione account";
                                            }
                                            var template_content = [{
                                                    "name": "LANG",
                                                    "content": req.body.data.attributes.lang
                                                }, {
                                                    "name": "NAME",
                                                    "content": req.body.data.attributes.mail
                                                }, {
                                                    "name": "URL",
                                                    "content": 'https://' + req.body.data.attributes.organization + '.beebeeboard.com/#/confirm/' + req.body.data.attributes.create_user_token
                                                }, {
                                                    "name": "ORGANIZATION",
                                                    "content": uc_first(req.body.data.attributes.organization_name)
                                                }, {
                                                    "name": "LOGO",
                                                    "content": req.body.data.attributes.thumbnail_url ? 'http://data.beebeeboard.com/' + req.body.data.attributes.thumbnail_url + '-small.jpeg' : null
                                                }, {
                                                    "name": "COLOR",
                                                    "content": req.body.data.attributes.primary_color ? req.body.data.attributes.primary_color : '#00587D'
                                                }],
                                                message = {
                                                    "metadata": {
                                                        "organization": req.headers['host'].split(".")[0]
                                                    },
                                                    "global_merge_vars": template_content,
                                                    "subject": uc_first(req.body.data.attributes.organization_name) + subject,
                                                    "from_email": "no-reply@beebeemailer.com",
                                                    "from_name": uc_first(req.body.data.attributes.organization_name),
                                                    "to": [{
                                                        "email": req.body.data.attributes.mail,
                                                        "type": "to"
                                                    }],
                                                    "headers": {
                                                        "Reply-To": "no-reply@beebeemailer.com"
                                                    },
                                                    "tags": [template, req.headers['host'].split(".")[0]],
                                                    "subaccount": "beebeeboard",
                                                    "tracking_domain": "mandrillapp.com",
                                                    "google_analytics_domains": ["poppix.it"]
                                                };

                                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                            mandrill_client.messages.sendTemplate({
                                                "template_name": template,
                                                "template_content": template_content,
                                                "message": message,
                                                "async": true,
                                                "send_at": false
                                            }, function(result) {

                                                //result[0]._tenant_id = data.tenant_id;

                                                if (result && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                                    console.log("New account mail sended correctly");

                                                }
                                                res.status(200).send();
                                            }, function(e) {
                                                // Mandrill returns the error as an object with name and message keys
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                return res.status(400).render('user_registration_request: ' + e);
                                            });
                                        }).catch(function(err) {
                                            return res.status(400).render('user_registration_request: ' + err);
                                        });

                                }).catch(function(err) {
                                    return res.status(400).render('user_registration_request: ' + err);
                                });
                        } else {
                            sequelize.query('SELECT update_contact_request_registration(' +
                                    req.body.data.attributes.contact_id + ',\'' +
                                    req.body.data.attributes.create_user_token + '\',\'' +
                                    req.body.data.attributes.create_user_expires + '\',' +
                                    req.body.data.attributes.organization_id + ',\'' +
                                    req.body.data.attributes.organization + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {
                                    sequelize.query('SELECT contact_custom_fields_trigger(' +
                                            req.body.data.attributes.contact_id + ',' +
                                            _tenant + ',\'' +
                                            req.body.data.attributes.organization + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(cus) {
                                            var template = '';
                                            var subject = '';
                                            req.body.data.id = req.body.data.attributes.contact_id;
                                            // Create registration record
                                            if (req.body.data.attributes.contact_id) {
                                                template = "invitation";
                                                subject = " ti ha mandato un invito";
                                            } else {
                                                template = "create-account";
                                                subject = " Creazione account";
                                            }
                                            var template_content = [{
                                                    "name": "LANG",
                                                    "content": req.body.data.attributes.lang
                                                }, {
                                                    "name": "NAME",
                                                    "content": req.body.data.attributes.mail
                                                }, {
                                                    "name": "URL",
                                                    "content": 'https://' + req.body.data.attributes.organization + '.beebeeboard.com/#/confirm/' + req.body.data.attributes.create_user_token
                                                }, {
                                                    "name": "ORGANIZATION",
                                                    "content": uc_first(req.body.data.attributes.organization_name)
                                                }, {
                                                    "name": "LOGO",
                                                    "content": req.body.data.attributes.thumbnail_url ? 'http://data.beebeeboard.com/' + req.body.data.attributes.thumbnail_url + '-small.jpeg' : null
                                                }, {
                                                    "name": "COLOR",
                                                    "content": req.body.data.attributes.primary_color ? req.body.data.attributes.primary_color : '#00587D'
                                                }],
                                                message = {
                                                    "global_merge_vars": template_content,
                                                    "metadata": {
                                                        "organization": req.headers['host'].split(".")[0]
                                                    },
                                                    "subject": uc_first(req.body.data.attributes.organization_name) + subject,
                                                    "from_email": "no-reply@beebeemailer.com",
                                                    "from_name": uc_first(req.body.data.attributes.organization_name),
                                                    "to": [{
                                                        "email": req.body.data.attributes.mail,
                                                        "type": "to"
                                                    }],
                                                    "headers": {
                                                        "Reply-To": "no-reply@beebeemailer.com"
                                                    },
                                                    "tags": [template, req.headers['host'].split(".")[0]],
                                                    "subaccount": "beebeeboard",
                                                    "tracking_domain": "mandrillapp.com",
                                                    "google_analytics_domains": ["poppix.it"]
                                                };

                                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                            mandrill_client.messages.sendTemplate({
                                                "template_name": template,
                                                "template_content": template_content,
                                                "message": message,
                                                "async": true,
                                                "send_at": false
                                            }, function(result) {

                                                //result[0]._tenant_id = data.tenant_id;

                                                if (result && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                                    console.log("New account mail sended correctly");

                                                }
                                                res.status(200).send();
                                            }, function(e) {
                                                // Mandrill returns the error as an object with name and message keys
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                return res.status(400).render('user_registration_request: ' + e);
                                            });
                                        }).catch(function(err) {
                                            return res.status(400).render('user_registration_request: ' + err);
                                        });

                                }).catch(function(err) {
                                    return res.status(400).render('user_registration_request: ' + err);
                                });
                        }

                    });
                }

            }).catch(function(err) {
                return res.status(400).render('user_registration_request: ' + err);
            });
        }
    }).catch(function(err) {
        return res.status(400).render('user_registration_request: ' + err);
    });
};

exports.user_registration_complete = function(req, res) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        mandrill = require('mandrill-api/mandrill'),
        crypto = require('crypto'),
        now = moment(),
        user = {};


    if (!req.body.data || !req.body.data.attributes) {
        return res.status(422).send(utility.error422('organization', 'Richiesta non valida', 'Richiesta non valida'));
    }

    if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
        if (re.test(req.body.data.attributes.mail) != true) {
            return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
        }
    } else {
        return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
    }

    if (req.body.data.attributes.first_name == '' || req.body.data.attributes.first_name == undefined) {

        return res.status(422).send(utility.error422('first_name', 'Parametro non valido', 'Inserire un nome valido', '422'));
    }

    if (req.body.data.attributes.username == '' || req.body.data.attributes.username == undefined) {

        return res.status(422).send(utility.error422('username', 'Parametro non valido', 'Inserire uno username valido', '422'));
    }

    if (req.body.data.attributes.password == '' || req.body.data.attributes.password == undefined) {

        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'Inserire una password valido', '422'));
    }

    sequelize.query('SELECT search_organization_client(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].search_organization_client && !datas[0].search_organization_client.id) {
            return res.status(422).send(utility.error422('organization', 'Parametro non valido', 'Company non trovata', '422'));
        } else {
            sequelize.query('SELECT check_mail_registration(\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                req.body.data.attributes.mail + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(mails) {
                if (!mails[0].check_mail_registration) {

                    sequelize.query('SELECT check_username_v1(\'' +
                        req.headers['host'].split(".")[0] + '\',\'' +
                        req.body.data.attributes.username + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(username) {
                        if (username[0].check_username_v1 && !req.body.data.attributes.fixed_username) {
                            return res.status(422).send(utility.error422('username', 'Username già esistente', 'Username già esistente', '422'));

                        } else if (req.body.data.attributes.username == null || req.body.data.attributes.username == undefined || req.body.data.attributes.username == '') {
                            return res.status(422).send(utility.error422('username', 'Invalid Attribute', 'Il campo username non può essere vuoto', '422'));
                        } else {

                            req.body.data.attributes.organization_id = datas[0].search_organization_client.id;
                            var _tenant = datas[0].search_organization_client._tenant_id;
                            crypto.randomBytes(40, function(err, buf) {
                                var token = buf.toString('hex');
                                req.body.data.attributes.create_user_token = token;
                                req.body.data.attributes.create_user_expires = moment(new Date(now)).add(7, 'days').format('YYYY/MM/DD HH:mm:ss'); // 3 day
                                req.body.data.attributes.organization_admin = false;
                                req.body.data.attributes.organization = req.headers['host'].split(".")[0];
                                req.body.data.attributes.organization_id = datas[0].search_organization_client.id;
                                req.body.data.attributes.organization_name = datas[0].search_organization_client.name;
                                req.body.data.attributes.primary_color = datas[0].search_organization_client.primary_color;
                                req.body.data.attributes.thumbnail_url = datas[0].search_organization_client.thumbnail_url;

                                user['create_user_token'] = req.body.data.attributes.create_user_token;
                                user['create_user_expires'] = req.body.data.attributes.create_user_expires;
                                user['organization_admin'] = req.body.data.attributes.organization_admin;
                                user['organization'] = req.body.data.attributes.organization;
                                user['organization_id'] = req.body.data.attributes.organization_id;
                                user['mail'] = req.body.data.attributes.mail;
                                user['name'] = req.body.data.attributes.first_name.replace(/'/g, "''''") + ' ' + req.body.data.attributes.last_name.replace(/'/g, "''''");
                                user['username'] = req.body.data.attributes.username;
                                user['redirect_url'] = (datas[0].search_organization_client.redirect_url ? datas[0].search_organization_client.redirect_url : null);
                                user['_tenant_id'] = datas[0].search_organization_client._tenant_id;
                                user['socket_room'] = req.body.data.attributes.organization;
                                user['role_id'] = datas[0].search_organization_client.role_id;
                                user['first_name'] = req.body.data.attributes.first_name.replace(/'/g, "''''");
                                user['last_name'] = req.body.data.attributes.last_name.replace(/'/g, "''''");
                                var redirect_page = datas[0].search_organization_client.redirect_page;

                                if (redirect_page === undefined || redirect_page === '' || redirect_page === null)
                                    redirect_page = 'https://' + user['organization'] + '.beebeeboard.com/start';
                                else if (redirect_page && redirect_page.indexOf('http') == -1)
                                    redirect_page = 'https://' + user['organization'] + '.beebeeboard.com/' + redirect_page;


                                user['contact_status_id'] = datas[0].search_organization_client.contact_status_default;

                                bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                                    if (err) {
                                        return res.status(400).render('user_registration: ' + err);
                                    }
                                    // hash the password using our new salt

                                    bcrypt.hash(req.body.data.attributes.password, salt, function(err, hash) {
                                        if (err) {
                                            return res.status(400).render('user_registration_complete: ' + err);
                                        }
                                        user['password'] = hash;
                                        sequelize.query('SELECT insert_user_complete_v1(\'' +
                                                JSON.stringify(user) + '\',\'' +
                                                req.headers['host'].split(".")[0] + '\',\'' +
                                                JSON.stringify(req.body.data.attributes.gdpr).replace(/'/g, "''''") + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                sequelize.query('SELECT contact_custom_fields_trigger(' +
                                                        datas[0].insert_user_complete_v1 + ',' +
                                                        user['_tenant_id'] + ',\'' +
                                                        req.body.data.attributes.organization + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(cus) {}).catch(function(err) {
                                                        return res.status(400).render('user_registration_complete: ' + err);
                                                    });

                                                var template = '';
                                                var subject = '';
                                                req.body.data.id = datas[0].insert_user_complete_v1;
                                                // Create registration record

                                                template = "verify-mail";
                                                subject = " Verifica il tuo indirizzo mail";

                                                var template_content = [{
                                                        "name": "LANG",
                                                        "content": req.body.data.attributes.lang
                                                    }, {
                                                        "name": "NAME",
                                                        "content": req.body.data.attributes.mail
                                                    }, {
                                                        "name": "URL",
                                                        "content": 'https://' + req.body.data.attributes.organization + '.beebeeboard.com/api/v1/registration/verify_mail/' + user['create_user_token']
                                                    }, {
                                                        "name": "ORGANIZATION",
                                                        "content": uc_first(req.body.data.attributes.organization_name)
                                                    }, {
                                                        "name": "LOGO",
                                                        "content": req.body.data.attributes.thumbnail_url ? 'http://data.beebeeboard.com/' + req.body.data.attributes.thumbnail_url + '-small.jpeg' : null
                                                    }, {
                                                        "name": "COLOR",
                                                        "content": req.body.data.attributes.primary_color ? req.body.data.attributes.primary_color : '#00587D'
                                                    }],
                                                    message = {
                                                        "metadata": {
                                                            "organization": req.headers['host'].split(".")[0]
                                                        },
                                                        "global_merge_vars": template_content,
                                                        "subject": uc_first(req.body.data.attributes.organization_name) + subject,
                                                        "from_email": "no-reply@beebeemailer.com",
                                                        "from_name": uc_first(req.body.data.attributes.organization_name),
                                                        "to": [{
                                                            "email": req.body.data.attributes.mail,
                                                            "type": "to"
                                                        }],
                                                        "headers": {
                                                            "Reply-To": "no-reply@beebeemailer.com"
                                                        },
                                                        "tags": [template, req.headers['host'].split(".")[0]],
                                                        "subaccount": "beebeeboard",
                                                        "tracking_domain": "mandrillapp.com",
                                                        "google_analytics_domains": ["poppix.it"]
                                                    };

                                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                                mandrill_client.messages.sendTemplate({
                                                    "template_name": template,
                                                    "template_content": template_content,
                                                    "message": message,
                                                    "async": true,
                                                    "send_at": false
                                                }, function(result) {

                                                    //result[0]._tenant_id = data.tenant_id;

                                                    if (result && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                                        console.log("New verify mail sended correctly");

                                                    }
                                                    var token = utils.uid(config.token.accessTokenLength);

                                                    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                                        moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                                        datas[0].insert_user_complete_v1 + ',' +
                                                        5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(tok) {
                                                        /*  Token.create({
                                                                token : token,
                                                                expiration_date : config.token.calculateExpirationDate(),
                                                                user_id : user.id,
                                                                client_id : client.id,
                                                                scope : scope
                                                            }).then(function(tok) {*/
                                                        sequelize.query('SELECT insert_login(' + datas[0].insert_user_complete_v1 + ',\'' + req.headers['host'].split(".")[0] + '\',\'' + req.headers['user-agent'] + '\',\'' + req.connection.remoteAddress + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(logins) {
                                                            res.status(200).send({
                                                                'access_token': token,
                                                                'url': redirect_page
                                                            });
                                                        }).catch(function(err) {
                                                            return res.status(400).render('user_registration_complete: ' + err);
                                                        });


                                                    }).catch(function(err) {
                                                        return res.status(400).render('user_registration_complete: ' + err);
                                                    });

                                                }, function(e) {
                                                    // Mandrill returns the error as an object with name and message keys
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                    return res.status(400).render('user_registration_complete: ' + e);
                                                });
                                            }).catch(function(err) {
                                                return res.status(400).render('user_registration_complete: ' + err);
                                            });


                                    });
                                });
                            });
                        }
                    }).catch(function(err) {
                        return res.status(400).render('user_check_username: ' + err);
                    });

                } else {
                    return res.status(422).send(utility.error422('mail', 'Mail già esistente', 'Mail già esistente', '422'));
                }

            }).catch(function(err) {
                return res.status(400).render('user_registration_complete: ' + err);
            });
        }
    }).catch(function(err) {
        return res.status(400).render('user_registration_complete: ' + err);
    });
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * API save the invited user mail and organization and send mail to the new user for the confirm
 * 
 */
exports.invitation = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        mandrill = require('mandrill-api/mandrill'),
        crypto = require('crypto'),
        new_user = {},
        now = moment();

    if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
        if (re.test(req.body.data.attributes.mail) != true) {
            return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
        }
    }

    sequelize.query('SELECT find_user_admin_v1(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (!datas[0].find_user_admin_v1) {
                return res.status(422).send(utility.error422('organization', 'Parametro non valido', 'Company non trovata', '422'));
            } else {
                sequelize.query('SELECT check_mail_registration(\'' +
                    req.headers['host'].split(".")[0] + '\',\'' +
                    req.body.data.attributes.mail + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(mails) {
                    if (mails[0].check_mail_registration) {
                        return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Mail già esistente', '422'));
                    } else {
                        crypto.randomBytes(40, function(err, buf) {
                            var token = buf.toString('hex');

                            new_user.create_user_token = token;
                            new_user.create_user_expires = moment(new Date(now)).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss'); // 1 week
                            new_user.organization_admin = false;
                            new_user.admin_id = req.user.id;
                            new_user.admin_name = req.user.name;

                            new_user.organization = req.headers['host'].split(".")[0];
                            new_user.mail = req.body.data.attributes.mail;
                            new_user.socket_room = req.headers['host'].split(".")[0];

                            sequelize.query('SELECT search_organization(\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(datas_o) {

                                if (datas_o[0].search_organization && !datas_o[0].search_organization.id) {
                                    return res.status(422).send(utility.error422('organization', 'Parametro non valido', 'Company non trovata', '422'));
                                } else {
                                    new_user._tenant_id = datas_o[0].search_organization._tenant_id;
                                    new_user.organization_id = datas_o[0].search_organization.id;

                                    sequelize.query('SELECT insert_user_invitation_v1(\'' +
                                            JSON.stringify(new_user) + '\',\'' +
                                            req.headers['host'].split(".")[0] + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {

                                            if (datas[0].insert_user_invitation_v1 && datas[0].insert_user_invitation_v1 != null) {
                                                req.body.data.id = datas[0].insert_user_invitation_v1;
                                            }
                                            sequelize.query('SELECT contact_custom_fields_trigger(' +
                                                    datas[0].insert_user_invitation_v1 + ',' +
                                                    new_user._tenant_id + ',\'' +
                                                    new_user.organization + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(cus) {

                                                    var template_content = [{
                                                            "name": "LANG",
                                                            "content": datas_o[0].search_organization.lang
                                                        }, {
                                                            "name": "ORGANIZATION",
                                                            "content": uc_first(datas_o[0].search_organization.name)
                                                        }, {
                                                            "name": "NAME",
                                                            "content": req.body.data.attributes.mail
                                                        }, {
                                                            "name": "URL",
                                                            "content": 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/#/confirm/' + token
                                                        }, {
                                                            "name": "LOGO",
                                                            "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                                                        }, , {
                                                            "name": "COLOR",
                                                            "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                                                        }],
                                                        message = {
                                                            "global_merge_vars": template_content,
                                                            "metadata": {
                                                                "organization": req.headers['host'].split(".")[0]
                                                            },
                                                            "subject": uc_first(datas_o[0].search_organization.name) + ' ti ha mandato un invito',
                                                            "from_email": "no-reply@beebeemailer.com",
                                                            "from_name": uc_first(datas_o[0].search_organization.name),
                                                            "to": [{
                                                                "email": req.body.data.attributes.mail,
                                                                "type": "to"
                                                            }],
                                                            "headers": {
                                                                "Reply-To": "no-reply@beebeemailer.com"
                                                            },
                                                            "tags": ["invitation", req.headers['host'].split(".")[0]],
                                                            "subaccount": "beebeeboard",
                                                            "tracking_domain": "mandrillapp.com",
                                                            "google_analytics_domains": ["poppix.it"]
                                                        };

                                                    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                                    mandrill_client.messages.sendTemplate({
                                                        "template_name": "invitation",
                                                        "template_content": template_content,
                                                        "message": message,
                                                        "async": true,
                                                        "send_at": false
                                                    }, function(result) {
                                                        /*
                                                         [{
                                                         "email": "recipient.email@example.com",
                                                         "status": "sent",
                                                         "reject_reason": "hard-bounce",
                                                         "_id": "abc123abc123abc123abc123abc123"
                                                         }]
                                                         */

                                                        if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                                            console.log("Invitation mail sended correctly");

                                                            res.json({
                                                                'data': req.body.data,
                                                                'relationships': req.body.data.relationships
                                                            });

                                                        } else {
                                                            console.log("Mail rejected to " + req.body.data.attributes.mail + " for reason: " + result[0].reject_reason);

                                                            res.json({
                                                                'data': req.body.data,
                                                                'relationships': req.body.data.relationships
                                                            });
                                                        }


                                                    }, function(e) {
                                                        // Mandrill returns the error as an object with name and message keys
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                        res.json({
                                                            'data': req.body.data,
                                                            'relationships': req.body.data.relationships
                                                        });
                                                    });
                                                }).catch(function(err) {
                                                    return res.status(400).render('user_invitation: ' + err);
                                                });
                                        }).catch(function(err) {
                                            return res.status(400).render('user_invitation: ' + err);
                                        });
                                }

                            }).catch(function(err) {
                                return res.status(400).render('user_invitation: ' + err);
                            });

                        });
                    }
                });
            }
        }).catch(function(err) {
            return res.status(400).render('user_invitation: ' + err);
        });
};

exports.modify_password = function(req, res) {
    var mandrill = require('mandrill-api/mandrill');

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (!req.body.new_password || req.body.new_password === undefined ||
        req.body.new_password === '' ||
        !utility.check_type_variable(req.body.new_password, 'string')) {
        return res.status(422).send(utility.error422('nuova password', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT select_user_v1(' +
        req.params.id + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {
        bcrypt.compare(req.body.old_password, datas[0].select_user_v1.password, function(err, ris) {
            if (err) {
                return res.status(400).render('user_modify_password: ' + err);
            }

            if (ris === false) {
                return res.status(422).send(utility.error422('password', 'Parametro non valido', 'La vecchia password non corrisponde', '422'));
            } else {
                console.log('password corretta');
                bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                    if (err) {
                        return res.status(400).render('user_modify_password: ' + err);
                    }

                    // hash the password using our new salt
                    bcrypt.hash(req.body.new_password, salt, function(err, hash) {
                        if (err) {
                            return res.status(400).render('user_modify_password: ' + err);
                        }
                        sequelize.query('SELECT update_password_v1(' +
                            req.params.id + ',' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',\'' +
                            hash + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(datas1) {
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Password changed');


                            var template_content = [{
                                    "name": "LANG",
                                    "content": datas[0].select_user_v1.lang
                                }, {
                                    "name": "NAME",
                                    "content": req.body.name ? req.body.name : ''
                                }, {
                                    "name": "ORGANIZATION",
                                    "content": uc_first(datas[0].select_user_v1.organization_name)
                                }, {
                                    "name": "COLOR",
                                    "content": datas[0].select_user_v1.primary_color ? datas[0].select_user_v1.primary_color : '#00587D'
                                }, {
                                    "name": "LOGO",
                                    "content": datas[0].select_user_v1.org_thumb ? 'https://data.beebeeboard.com/' + datas[0].select_user_v1.org_thumb + '-small.jpeg' : null
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": req.headers['host'].split(".")[0]
                                    },
                                    "subject": uc_first(datas[0].select_user_v1.organization_name) + ' password reset',
                                    "from_email": "no-reply@beebeemailer.com",
                                    "from_name": uc_first(datas[0].select_user_v1.organization_name),
                                    "to": [{
                                        "email": req.body.mail,
                                        "name": req.body.name ? req.body.name : '',
                                        "type": "to"
                                    }],
                                    "headers": {
                                        "Reply-To": "no-reply@beebeemailer.com"
                                    },
                                    "tags": ["password-reset", req.headers['host'].split(".")[0]],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "password-reset",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {
                                /*
                                 [{
                                 "email": "recipient.email@example.com",
                                 "status": "sent",
                                 "reject_reason": "hard-bounce",
                                 "_id": "abc123abc123abc123abc123abc123"
                                 }]
                                 */

                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Modify Password -> Mandrill user' + err);
                                if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                    console.log("Password Modification mail sended correctly");

                                    res.status(200).send({});

                                }


                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                res.status(200).send({});
                            });

                        }).catch(function(err) {
                            return res.status(400).render('user_modify_password: ' + err);
                        });
                    });
                });
            }
        });
    }).catch(function(err) {
        return res.status(400).render('user_modify_password: ' + err);
    });
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * Forgot password by a user, API control that a submissed mail exist in users collection
 * if exist it create a temporary token in the selected user document and send a mail to this
 * user with the correct link for the recovery password 
 * 
 */
exports.forgot = function(req, res) {

    var crypto = require('crypto'),
        mandrill = require('mandrill-api/mandrill'),
        re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        mail = req.query.mail,
        organization = req.headers['host'].split('.')[0],
        now = moment();

    if (!mail ||
        mail == '' ||
        mail == undefined ||
        re.test(mail) != true ||
        !utility.check_type_variable(mail, 'string')) {
        return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
    }

    sequelize.query('SELECT user_forgot_password_v1(\'' +
            mail + '\',\'' +
            organization + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].user_forgot_password_v1 && datas[0].user_forgot_password_v1.id) {
                crypto.randomBytes(40, function(err, buf) {
                    var token = buf.toString('hex');
                    sequelize.query('SELECT update_user_forgot_password_v1(' +
                            datas[0].user_forgot_password_v1.id + ',\'' +
                            organization + '\',\'' +
                            token + '\',\'' +
                            moment(new Date(now)).add(2, 'days').format('YYYY/MM/DD HH:mm:ss') + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas_update) {


                            var template_content = [{
                                    "name": "LANG",
                                    "content": datas[0].user_forgot_password_v1.lang
                                }, {
                                    "name": "NAME",
                                    "content": datas[0].user_forgot_password_v1.name ? datas[0].user_forgot_password_v1.name : '',
                                }, {
                                    "name": "URL",
                                    "content": 'https://' + organization + '.beebeeboard.com/#/reset/' + token
                                }, {
                                    "name": "ORGANIZATION",
                                    "content": uc_first(datas[0].user_forgot_password_v1.organization_name)
                                }, {
                                    "name": "USERNAME",
                                    "content": datas[0].user_forgot_password_v1.username
                                }, {
                                    "name": "COLOR",
                                    "content": datas[0].user_forgot_password_v1.primary_color ? datas[0].user_forgot_password_v1.primary_color : '#00587D'
                                }, {
                                    "name": "LOGO",
                                    "content": datas[0].user_forgot_password_v1.org_thumb ? 'https://data.beebeeboard.com/' + datas[0].user_forgot_password_v1.org_thumb + '-small.jpeg' : null
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": req.headers['host'].split(".")[0]
                                    },
                                    "subject": uc_first(datas[0].user_forgot_password_v1.organization_name) + ' Reimposta Password',
                                    "from_email": "no-reply@beebeemailer.com",
                                    "from_name": uc_first(datas[0].user_forgot_password_v1.organization_name),
                                    "to": [{
                                        "email": datas[0].user_forgot_password_v1.mail,
                                        "name": datas[0].user_forgot_password_v1.name ? datas[0].user_forgot_password_v1.name : '',
                                        "type": "to"
                                    }],
                                    "headers": {
                                        "Reply-To": "no-reply@beebeemailer.com"
                                    },
                                    "tags": ["password-recovery", req.headers['host'].split(".")[0]],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "password-recovery",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {
                                /*
                                 [{
                                 "email": "recipient.email@example.com",
                                 "status": "sent",
                                 "reject_reason": "hard-bounce",
                                 "_id": "abc123abc123abc123abc123abc123"
                                 }]
                                 */

                                if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                    console.log("Recovery mail sended correctly");

                                    res.status(200).send({});

                                }
                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                return res.status(400).render('user_forgot_password: ' + e);
                            });

                        }).catch(function(err) {
                            return res.status(400).render('user_forgot_password: ' + err);
                        });

                });
            } else {
                return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Mail non presente', '422'));

            }


        }).catch(function(err) {
            return res.status(400).render('user_forgot_password: ' + err);
        });
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * API search in users collection if exist a user with the token passed by argument, if exist return the user
 * 
 */
exports.recovery_password = function(req, res) {
    var now = moment();

    if (!utility.check_type_variable(req.params.token, 'string')) {
        return res.status(422).send(utility.error422('token', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT recovery_password_v1(\'' +
        req.params.token + '\',\'' +
        moment(new Date(now)).format('YYYY/MM/DD HH:mm:ss') + '\',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {
        if (!datas[0].recovery_password_v1) {
            return res.status(422).send(utility.error422('organization', 'Parametro non valido', 'Nessun utente trovato per la company ' + req.headers['host'].split(".")[0] + ' con un token valido', '422'));

        } else {
            res.json({
                'data': {
                    'user': {
                        'id': datas[0].recovery_password_v1.id,
                        'mail': datas[0].recovery_password_v1.mail
                    }
                }
            });
        }
    }).catch(function(err) {
        return res.status(400).render('user_recovery_password: ' + err);
    });
};

exports.check_mail = function(req, res) {

    if (req.body.mail) {
        sequelize.query('SELECT check_mail_for_registration(\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            req.body.mail.replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(utente) {
            res.status(200).send(utente[0].check_mail_for_registration)
        }).catch(function(err) {
            return res.status(400).render('check_mail_for_registration: ' + err);
        });
    } else {
        return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail', '422'));

    }
};

/**
 * @param {Object} req
 * @param {Object} res
 * 
 * API save the new password crypted and send a mail to user for the correct operation
 * 
 */
exports.reset_password = function(req, res) {
    var mandrill = require('mandrill-api/mandrill'),
        now = moment();

    if (!utility.check_type_variable(req.params.token, 'string')) {
        return res.status(422).send(utility.error422('token', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT recovery_password_v1(\'' +
        req.params.token + '\',\'' +
        moment(new Date(now)).format('YYYY/MM/DD HH:mm:ss') + '\',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas) {

        if (!datas[0].recovery_password_v1) {
            return res.status(422).send(utility.error422('organization', 'Parametro non valido', 'Nessuno user trovato per ' + req.headers['host'].split(".")[0] + ' con un token valido', '422'));
        }

        last_password = datas[0].recovery_password_v1.password;
        delete datas[0].recovery_password_v1.password;
        new_password = req.body.data.attributes.password;

        if (new_password !== null && new_password !== '') {

            bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                if (err) {
                    return res.status(400).render('user_reset_password: ' + err);
                }
                // hash the password using our new salt
                bcrypt.hash(new_password, salt, function(err, hash) {
                    if (err) {
                        return res.status(400).render('user_reset_password: ' + err);
                    }

                    if (last_password != null && last_password.toString('utf-8') === hash.toString('utf-8')) {
                        return res.status(422).send(utility.error422('password', 'Parametro non valido', 'La password deve essere diversa dall\'ultima utilizzata', '422'));

                    }
                    sequelize.query('SELECT update_user_recovery_password_v1(\'' +
                        hash + '\',' +
                        datas[0].recovery_password_v1.id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(datas_updated) {

                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Password changed');

                        var template_content = [{
                                "name": "LANG",
                                "content": datas[0].recovery_password_v1.lang
                            }, {
                                "name": "NAME",
                                "content": datas[0].recovery_password_v1.name ? datas[0].recovery_password_v1.name : ''
                            }, {
                                "name": "ORGANIZATION",
                                "content": uc_first(datas[0].recovery_password_v1.organization_name)
                            }, {
                                "name": "COLOR",
                                "content": datas[0].recovery_password_v1.primary_color ? datas[0].recovery_password_v1.primary_color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": datas[0].recovery_password_v1.org_thumb ? 'https://data.beebeeboard.com/' + datas[0].recovery_password_v1.org_thumb + '-small.jpeg' : null
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": uc_first(datas[0].recovery_password_v1.organization_name) + ' password reset',
                                "from_email": "no-reply@beebeemailer.com",
                                "from_name": uc_first(datas[0].recovery_password_v1.organization_name),
                                "to": [{
                                    "email": datas[0].recovery_password_v1.mail,
                                    "name": datas[0].recovery_password_v1.name ? datas[0].recovery_password_v1.name : '',
                                    "type": "to"
                                }],
                                "headers": {
                                    "Reply-To": "no-reply@beebeemailer.com"
                                },
                                "tags": ["password-reset", req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "password-reset",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {
                            /*
                             [{
                             "email": "recipient.email@example.com",
                             "status": "sent",
                             "reject_reason": "hard-bounce",
                             "_id": "abc123abc123abc123abc123abc123"
                             }]
                             */
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Recovery Password -> Mandrill user' + err);
                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                console.log("Reset password mail sended correctly");

                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                            /*res.json({
                                'data': {
                                    'user': {
                                        'id': datas[0].recovery_password_v1.id,
                                        'redirect': datas[0].recovery_password_v1.redirect_page
                                    }
                                }
                            });*/
                        });

                        var token = utils.uid(config.token.accessTokenLength);

                        sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                            moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                            datas[0].recovery_password_v1.id + ',' +
                            5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(tok) {
                            /*  Token.create({
                                    token : token,
                                    expiration_date : config.token.calculateExpirationDate(),
                                    user_id : user.id,
                                    client_id : client.id,
                                    scope : scope
                                }).then(function(tok) {*/
                            sequelize.query('SELECT insert_login(' + datas[0].recovery_password_v1.id + ',\'' + req.headers['host'].split(".")[0] + '\',\'' + req.headers['user-agent'] + '\',\'' + req.connection.remoteAddress + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(logins) {

                                var redirect_page = datas[0].recovery_password_v1.redirect_page;

                                if (redirect_page === undefined || redirect_page === '' || redirect_page === null)
                                    redirect_page = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/start';
                                else if (redirect_page && redirect_page.indexOf('http') == -1)
                                    redirect_page = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/' + redirect_page;


                                res.status(200).send({
                                    'access_token': token,
                                    'url': redirect_page
                                });
                            }).catch(function(err) {
                                return res.status(400).render('user_reset_password: ' + err);
                            });


                        }).catch(function(err) {
                            return res.status(400).render('user_reset_password: ' + err);
                        });


                    }).catch(function(err) {
                        return res.status(400).render('user_reset_password: ' + err);
                    });
                });

            });
        }
    }).catch(function(err) {
        return res.status(400).render('user_reset_password: ' + err);
    });
};

exports.free_organization = function(req, res) {
    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT organizationfree_data(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(datas) {

                if (datas[0].organizationfree_data && datas[0].organizationfree_data != null) {
                    callback(null, datas[0].organizationfree_data[0]);

                } else {

                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });

        },
        included: function(callback) {
            sequelize.query('SELECT organizationfree_included(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(included) {

                if (included[0].organizationfree_included && included[0].organizationfree_included != null) {
                    callback(null, included[0].organizationfree_included);

                } else {

                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('organization_free: ' + err);
        }

        res.json({
            'data': utility.check_find_datas(results.data, 'organizations', ['organization_term_options']),
            'included': utility.check_find_included(results.included, 'organizations', ['event_states', 'contact_states',
                'product_states', 'project_states', 'workhour_states'
            ])
        });
    });
};

exports.free_organization_advanced = function(req, res) {
    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT organizationfree_advanced_data(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(datas) {

                if (datas[0].organizationfree_advanced_data && datas[0].organizationfree_advanced_data != null) {
                    callback(null, datas[0].organizationfree_advanced_data[0]);

                } else {

                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });

        },
        included: function(callback) {
            sequelize.query('SELECT organizationfree_included(\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(included) {

                if (included[0].organizationfree_included && included[0].organizationfree_included != null) {
                    callback(null, included[0].organizationfree_included);

                } else {

                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('organization_free_advanced : ' + err);
        }

        res.json({
            'data': utility.check_find_datas(results.data, 'organizations', []),
            'included': utility.check_find_included(results.included, 'organizations', ['event_states', 'contact_states',
                'product_states', 'project_states', 'workhour_states'
            ])
        });
    });
};

exports.resend_registration = function(req, res) {
    var now = moment(),
        crypto = require('crypto');

    crypto.randomBytes(40, function(err, buf) {
        var token = buf.toString('hex');

        var create_user_token = token;
        var create_user_expires = moment(new Date(now)).add(7, 'days').format('YYYY/MM/DD HH:mm:ss'); // 1 day

        sequelize.query('SELECT update_admin_request_registration(\'' +
                create_user_token + '\',\'' +
                create_user_expires + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(upd_cont) {
                if (upd_cont[0].update_admin_request_registration && upd_cont[0].update_admin_request_registration.mail) {
                    sequelize.query('SELECT insert_registration(\'' +
                            req.headers['host'].split(".")[0] + '\',\'https://' +
                            req.headers['host'].split(".")[0] + '.beebeeboard.com\',\'' +
                            upd_cont[0].update_admin_request_registration.mail + '\',\'' +
                            create_user_token + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                        .then(function(datas) {
                            res.status(200).json({});
                        }).catch(function(err) {
                            console.log(err);
                            return res.status(400).render('resend_registration: ' + err);
                        });
                } else {
                    res.status(404).json({});
                }


            }).catch(function(err) {
                return res.status(400).render('resend_registration: ' + err);
            });
    });
};

exports.booking_destroy_cron = function(req, res) {

    sequelize.query('SELECT booking_destroy_cron();', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        useMaster: true
    }).then(function(datas) {

        res.status(200).send({});

    }).catch(function(err) {
        return res.status(400).render('booking_destroy_cron: ' + err);
    });
};

exports.registration_cron = function(req, res) {

    var mandrill = require('mandrill-api/mandrill'),
        request = require('request');
    const { S3Client} = require('@aws-sdk/client-s3');

    sequelize.query('SELECT elenco_registrations();', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
    }).then(function(datas) {

        if (datas[0].elenco_registrations && datas[0].elenco_registrations.length > 0) {
            datas[0].elenco_registrations.forEach(function(registration) {
                var url = 'https://' + registration.organization + '.beebeeboard.com';
                request.get(url, {
                    timeout: 30000,
                    json: false
                }, function(error, result) {
                    if (error) {
                        console.log(error);
                    } else if (result) {
                        console.log('Dominio creato - Invio mail di registrazione in corso');
                        var template_content = [{
                                "name": "NAME",
                                "content": registration.user_mail
                            }, {
                                "name": "ORGANIZATION",
                                "content": uc_first(registration.organization)
                            }, {
                                "name": "URL",
                                "content": 'https://' + registration.organization + '.beebeeboard.com/#/confirm/' + registration.token
                            }, {
                                "name": "LOGO",
                                "content": null
                            }, {
                                "name": "COLOR",
                                "content": '#00587D'
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": uc_first(registration.organization) + ' Creazione Dominio',
                                "from_email": "no-reply@beebeemailer.com",
                                "from_name": registration.organization,
                                "to": [{
                                    "email": registration.user_mail,
                                    "type": "to"
                                }, {
                                    "email": "info@poppix.it",
                                    "type": "bcc"
                                }],
                                "headers": {
                                    "Reply-To": "no-reply@beebeemailer.com"
                                },
                                "tags": ["create-account", req.headers['host'].split(".")[0]],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "create-account",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {
                            if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("New account mail sended correctly");
                                sequelize.query('SELECT delete_registration(' + registration.id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                    .then(function(del) {


                                    }).catch(function(err) {
                                        return res.status(400).render('user_delete_registration: ' + err);
                                    });
                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'

                        });
                    } else {
                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Dominio in attesa di creazione');

                    }

                });
            });

        }

        sequelize.query('SELECT sanitize_export_invoice_expense();', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
            
            if(datas && datas[0] && datas[0].sanitize_export_invoice_expense){

                const s3 = new S3Client({
                    region: config.aws_s3.region,
                    credentials:{
                        accessKeyId: config.aws_s3.accessKeyId,
                        secretAccessKey: config.aws_s3.secretAccessKey,
                    }
                });

                datas[0].sanitize_export_invoice_expense.forEach(function(exp){
                    console.log('Elimina Exports');
                    emptyBucket(exp.url_export, s3);
                });
                
            }
        }).catch(function(err) {
            return res.status(400).render('sanitize_export_invoice_expense: ' + err);
        });
                


        res.status(200).send({});

    }).catch(function(err) {
        return res.status(400).render('user_elenco_registrations: ' + err);
    });
};

function emptyBucket(url_key, s3){
  const { S3Client, DeleteObjectsCommand, ListObjectsV2Command } = require('@aws-sdk/client-s3');
  console.log(url_key);

  const command1 = new ListObjectsV2Command({
      Bucket: 'exports.beebeeboard.com',
        Prefix: url_key
    });

    
    s3.send(command1, function(err, blob){
        
        if (err) {console.log(err);return 0;}
        if (blob && blob.Contents && blob.Contents.length == 0) return 0;
        
        params = {Bucket: 'exports.beebeeboard.com'};
        params.Delete = {Objects:[]};

        var cnt = 0;
        blob.Contents.forEach(function(content){
             params.Delete.Objects.push({Key: content.Key});
         });

  
    const command2 = new DeleteObjectsCommand(params);
    s3.send(command2, function(err, data) {
    //s3.deleteObjects(params, function(err, data) {
      if (err) return 0;
      if(data && data.Contents && data.Contents.length >= 1000)emptyBucket(url_key,s3);
      else {
        

        return 1;
      }
    });
  });

  sequelize.query('SELECT delete_export_invoice_expense(\''+url_key+'\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        useMaster:true
    }).then(function(abd) {

    });

}

exports.subscriptions_cron = function(req, res) {

    var mandrill = require('mandrill-api/mandrill'),
        request = require('request');

    console.log('Subscriptions Abbonamenti Scaduti');

    sequelize.query('SELECT elenco_subscriptions();', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
    }).then(function(datas) {

        if (datas[0].elenco_subscriptions && datas[0].elenco_subscriptions[0] && datas[0].elenco_subscriptions[0].scadenze && datas[0].elenco_subscriptions[0].scadenze.length > 0) {
            datas[0].elenco_subscriptions[0].scadenze.forEach(function(registration) {
                var url = 'https://' + registration.organization + '.beebeeboard.com';
                request.get(url, {
                    timeout: 30000,
                    json: false
                }, function(error, result) {
                    if (error) {
                        console.log(error);
                    } else if (result) {
                        var tags = [];

                        tags.push(registration.organization);
                        tags.push(registration.template_name);

                        if (registration.template_name != 'scadenza-abbonamento') {
                            tags.push('onboarding');
                        }

                        var template_content = [{
                                "name": "NAME",
                                "content": registration.name
                            }, {
                                "name": "ORGANIZATION",
                                "content": uc_first(registration.organization)
                            }, {
                                "name": "URL",
                                "content": url
                            }, {
                                "name": "COLOR",
                                "content": '#00587D'
                            }, {
                                "name": "DATA",
                                "content": moment(registration.expire_date).format('DD-MM-YYYY')
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": registration.organization
                                },
                                "subject": registration.title,
                                "from_email": "info@beebeeboard.com",
                                "from_name": "Beebeeboard",
                                "to": [{
                                    "email": registration.mail,
                                    "type": "to"
                                }],
                                "headers": {
                                    "Reply-To": "info@beebeeboard.com"
                                },
                                "tags": tags,
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"]
                            };


                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": registration.template_name,
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {
                            if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                console.log("Scadenze abbonamenti mail sended correctly");

                            }
                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'

                        });
                    } else {
                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Scadenze abbonamenti');

                    }

                });
            });

        }
        res.status(200).send({});

    }).catch(function(err) {
        return res.status(400).render('elenco_subscriptions: ' + err);
    });
};

exports.google_calendar_watch_cron = function(req, res) {

    var expiration = moment().add(1, 'days').valueOf();
   

    
    var request = require('request');
    req.setTimeout(500000);
    var cnt = 0;
    sequelize.query('SELECT elenco_google_calendar_watch_cron();', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        useMaster: true
    }).then(function(datas) {
        if (datas && datas[0] && datas[0].elenco_google_calendar_watch_cron) {

            datas[0].elenco_google_calendar_watch_cron.forEach(function(calendar) {
                async.parallel({
                    token: function(callback) {
                        sequelize.query('SELECT get_google_calendar_info(\'' + calendar.organization + '\',\'' + calendar.calendar_id + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            })
                            .then(function(datas) {


                                if (datas && datas[0] && datas[0].get_google_calendar_info) {
                                    var tok = JSON.parse(datas[0].get_google_calendar_info);
                                    if (tok.access_token) {
                                        callback(null, tok.access_token);
                                    } else if (tok.refresh_token) {

                                        request.post('https://oauth2.googleapis.com/token', {
                                            form: {
                                                grant_type: 'refresh_token',
                                                refresh_token: tok.refresh_token,
                                                client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                                client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                            }
                                        }, function(err, res, body) {
                                            body = JSON.parse(body);
                                            if(body.error){
                                                callback(body.error, null);
                                            }
                                            else{
                                                sequelize.query('SELECT update_google_calendar_token(\'' + calendar.organization +
                                                    '\', \'' + body.access_token + '\',' + tok.contact_id + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    }).then(function(datas) {
                                                    callback(null, body.access_token);

                                                });
                                            }
                                            
                                        });
                                    }
                                } else {

                                    callback(null, null);
                                }
                            });
                    },
                    calendar: function(callback) {
                        callback(null, calendar.calendar_id);
                    },
                    organization: function(callback) {
                        callback(null, calendar.organization);
                    }
                }, function(err, risultato) {
                    if (err) {
                        console.log('ERRORE async TOKEN');
                        console.log(err);
                        console.log(risultato);
                        if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {
                            res.status(200).send();
                        }
                    }
                    else{
                        console.log('RISULTATO');
                        console.log(risultato);
                        var access_token_ok = risultato.token;

                        if(access_token_ok){

                            var headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + access_token_ok
                            };

                            console.log(headers);
                            // Configure the request
                            var options = {
                                url: 'https://www.googleapis.com/calendar/v3/channels/stop',
                                method: 'POST',
                                headers: headers,
                                json: JSON.parse('{"id": "gc-beebeeboard-notification-' + calendar.organization + '","resourceId": "' + calendar.resource_id + '" }')
                            };
                            // Start the request
                            request(options, function(error, response, body) {
                               

                                if (!error) {
                                    


                                    options = {
                                        url: 'https://www.googleapis.com/calendar/v3/calendars/' + calendar.calendar_id + '/events/watch?alt=json&showdeleted=true',
                                        method: 'POST',
                                        headers: headers,
                                        json: JSON.parse('{"id": "gc-beebeeboard-notification-' + calendar.organization + '","type": "web_hook","address": "https://google-calendar.beebeeboard.com/api/v1/calendar_notification", "expiration": "'+expiration+'" }')
                                    };
                                    request(options, function(error, response2, body2) {
                                        
                                        if (!error && response2.statusCode == 200 && body2) {
                                            if (body2) {
                                                console.log('BODY: ');
                                                console.log(JSON.stringify(body2));
                                                sequelize.query('SELECT update_google_calendar_watch(\'' + calendar.organization +
                                                    '\', ' + calendar.contact_id + ',\'' + calendar.calendar_id + '\',\'' +
                                                    body2.resourceId + '\',\'' +
                                                    body2.resourceUri + '\',\'' +
                                                    body2.expiration + '\');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    }).then(function(datas1) {

                                                   
                                                    if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {


                                                        res.status(200).send();
                                                    }

                                                 }).catch(function(err) {
                                                    return res.status(400).render('update_google_calendar_watch: ' + err);
                                                });
                                            }

                                        } else {
                                            console.log('ERRORE CHIAMATA START WATCH GOOGLE');
                                            console.log(JSON.stringify(JSON.parse(error)));
                                            console.log(JSON.stringify(body2));

                                            if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {
                                                res.status(200).send();
                                            }
                                        }
                                    });
                                } else {
                                    console.log('ERRORE CHIAMATA STOP GOOGLE');
                                    console.log(JSON.stringify(JSON.parse(error)));

                                    if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {
                                        res.status(200).send();
                                    }
                                }
                            });
                        }else{
                            console.log('TOKEN NON PRESENTE' + calendar.calendar_id + ' ' + calendar.organization);
                            if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {
                                res.status(200).send();
                            }
                        }
                    }
                    
                    
                });

            });
        } else {
            res.status(200).send();
        }

    }).catch(function(err) {
        return res.status(400).render('google_calendar_watch_cron: ' + err);
    });
};

exports.reminder_cron = function(req, res) {
    var mandrill = require('mandrill-api/mandrill'),
        timezone = require('moment-timezone'),
        time_zone = null,
        qs = require('querystring');
    
    var base58 = require('../utility/base58.js');
    

    sequelize.query('SELECT elenco_reminders_ghost();', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        useMaster: true
    }).then(function(datas) {
        if (datas && datas[0] && datas[0].elenco_reminders_ghost && datas[0].elenco_reminders_ghost.length > 0) {
            console.log('INIZIA CICLO REMINDER: TROVATI: '+datas[0].elenco_reminders_ghost.length);
            var cnt = 0;
            datas[0].elenco_reminders_ghost.forEach(function(reminder) {
                reminder.organization_name = reminder.organization_name.replace(/'/g, "''''");

                if (reminder.todo_id) {
                    if (reminder.send_type == 'mail' || 
                        (reminder.send_type == 'custom' && (reminder.pref_contact == 'mail' || !reminder.pref_contact))) {

                        sequelize.query('SELECT find_todo(' +
                            reminder.todo_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_todo) {
                                var contacts = [];
                                var contact_string = '';
                                var product_string = '';
                                var project_string = '';

                                if (todo[0].find_todo.json_build_object.contacts &&
                                    todo[0].find_todo.json_build_object.contacts.length) {
                                    var cnt = 0;
                                    todo[0].find_todo.json_build_object.contacts.forEach(function(us) {
                                        if (cnt != 0) {
                                            contact_string += ', ';
                                        }
                                        contact_string += us.name;
                                        cnt++;
                                        time_zone = us.time_zone;
                                    });
                                }

                                product_string += todo[0].find_todo.prod_description ? todo[0].find_todo.prod_description : '-';
                                project_string += todo[0].find_todo.proj_description ? todo[0].find_todo.proj_description : '-';

                                contacts.push(reminder.send_to_id);

                                var template_content = [{
                                        "name": "ORGANIZATION",
                                        "content": uc_first(reminder.organization_name)
                                    }, {
                                        "name": "TEXT",
                                        "content": decode_text_mail(reminder, 'todo', todo[0].find_todo)
                                    }, {
                                        "name": "CONTACT_NAME",
                                        "content": contact_string != '' ? contact_string : '-',
                                    }, {
                                        "name": "PRODUCT_NAME",
                                        "content": product_string != '' ? product_string : '-',
                                    }, {
                                        "name": "PROJECT_NAME",
                                        "content": project_string != '' ? project_string : '-',
                                    }, {
                                        "name": "PRODUCT_LABEL",
                                        "content": reminder.products_name
                                    }, {
                                        "name": "PROJECT_LABEL",
                                        "content": reminder.projects_name
                                    }, {
                                        "name": "CONTACT_LABEL",
                                        "content": reminder.contacts_name
                                    }, {
                                        "name": "LOGO",
                                        "content": reminder.thumbnail_url ? 'https://data.beebeeboard.com/' + reminder.thumbnail_url + '-small.jpeg' : null
                                    }, {
                                        "name": "COLOR",
                                        "content": reminder.primary_color ? reminder.primary_color : '#00587D'
                                    }],
                                    message = {
                                        "global_merge_vars": template_content,
                                        "metadata": {
                                            "organization": reminder.organization.replace(/'/g, "''''")
                                        },
                                        "subject": uc_first(reminder.organization_name) + ' - Promemoria',
                                        "from_email": todo[0].find_todo.domain_from_mail ? todo[0].find_todo.domain_from_mail : "no-reply@beebeemailer.com",
                                        "from_name": uc_first(reminder.organization_name),
                                        "to": [{
                                            "email": reminder.send_to_mail,
                                            "name": reminder.send_to,
                                            "type": "to"
                                        }],
                                        "headers": {
                                            "Reply-To": reminder.send_from_mail
                                        },
                                        "tags": ["todo-reminder", reminder.organization.replace(/'/g, "''''")],
                                        "subaccount": "beebeeboard",
                                        "tracking_domain": "mandrillapp.com",
                                        "google_analytics_domains": ["poppix.it"]
                                    };

                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                mandrill_client.messages.sendTemplate({
                                    "template_name": "event-reminder",
                                    "template_content": template_content,
                                    "message": message,
                                    "async": true,
                                    "send_at": false
                                }, function(result) {
                                    /*
                                     [{
                                     "email": "recipient.email@example.com",
                                     "status": "sent",
                                     "reject_reason": "hard-bounce",
                                     "_id": "abc123abc123abc123abc123abc123"
                                     }]
                                     */


                                    if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                        console.log('Reminder for todo sended correctly');
                                        sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_todo.organization + '\',\'' +
                                                    decode_text_mail(reminder, 'todo', todo[0].find_todo).replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                            .then(function(rem) {
                                                sequelize.query('SELECT insert_mandrill_send_todo(' +
                                                        todo[0].find_todo._tenant_id + ',\'' +
                                                        todo[0].find_todo.organization + '\',' +
                                                        (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
                                                        'null,\'' +
                                                        result[0].status + '\',null,\'' +
                                                        result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                                        decode_text_mail(reminder, 'todo', todo[0].find_todo).replace(/'/g, "''''") + '\',' +
                                                        reminder.todo_id + ');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(datas) {
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                                    }).catch(function(err) {
                                                       console.log(err);
                                                        //return res.status(400).render('mandrill send mail: ' + err);
                                                    });

                                            }).catch(function(err) {
                                               console.log(err);
                                                //return res.status(400).render('reminder_destroy: ' + err);
                                            });


                                    } else {
                                        sequelize.query('SELECT insert_mandrill_send_todo(' +
                                                todo[0].find_todo._tenant_id + ',\'' +
                                                todo[0].find_todo.organization + '\',' +
                                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
                                                'null,\'' +
                                                result[0].status + '\',\'' +
                                                result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                                decode_text_mail(reminder, 'todo', todo[0].find_todo).replace(/'/g, "''''") + '\',' +
                                                reminder.todo_id + ');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                            .then(function(datas) {
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                            }).catch(function(err) {
                                               console.log(err);
                                                //return res.status(400).render('mandrill send mail: ' + err);
                                            });
                                    }
                                }, function(e) {
                                    // Mandrill returns the error as an object with name and message keys
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                })
                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_todo_reminder: ' + err);
                        });

                    } else if (reminder.send_type == 'sms' || 
                        (reminder.send_type == 'custom' && reminder.pref_contact == 'SMS')) {
                        sequelize.query('SELECT find_todo(' +
                            reminder.todo_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_todo) {

                                var https = require('https');

                                var sender_string = reminder.mittente ? reminder.mittente : 'Beebeeboard';
                                var method = todo[0].find_todo._tenant_id == 170 ? 'send_sms_classic_report' : 'send_sms_classic_report';
                                var lrecipients = [];
                                var mob_text = [];
                                var contact_ids = [];
                                var testo_sms = decode_text_sms(reminder, 'todo', todo[0].find_todo);

                                if (reminder.send_to_mobile) {
                                    if (reminder.send_to_mobile.indexOf('+') == -1) {
                                        reminder.send_to_mobile = '+39' + reminder.send_to_mobile.replace(/\D/g, '');
                                    } else {
                                        reminder.send_to_mobile = '+' + reminder.send_to_mobile.replace(/\D/g, '');
                                    }
                                    lrecipients.push(reminder.send_to_mobile);
                                    mob_text.push('\''+reminder.send_to_mobile+'\'');
                                    contact_ids.push(reminder.send_to_id);

                                    utility.login_skebby(function(err, auth) {
                                        if (!err) {
                                            var smsData = {
                                                "returnCredits": true,
                                                "recipient": lrecipients,
                                                "message": testo_sms ? testo_sms : ' Promemoria Todo ',
                                                "message_type": "GP",
                                                "sender" : sender_string
                                            };

                                            utility.sendSMS(auth, smsData,
                                                function(error, data) {
                                                    if (error) {
                                                        var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                todo[0].find_todo._tenant_id + ',\'' +
                                                                todo[0].find_todo.organization + '\',' +
                                                                (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ',null' +
                                                                ',\'' +
                                                                err.replace(/'/g, "''''") + '\',\'' +
                                                                err.replace(/'/g, "''''") + '\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ','+
                                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                 (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'todo\','+
                                                                 reminder.todo_id+','+
                                                                (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {

                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('skebby send sms: ' + err);
                                                            });
                                                     }
                                                    else {
                                                        console.log('SMS: send correctly ' + JSON.stringify(data));

                                                        sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_todo.organization + '\',\'' +
                                                            testo_sms.replace(/'/g, "''''") + '\',\''+
                                                            JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(rem) {


                                                        }).catch(function(err) {
                                                           console.log(err);
                                                            //return res.status(400).render('reminder_destroy: ' + err);
                                                        });

                                                        var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                todo[0].find_todo._tenant_id + ',\'' +
                                                                todo[0].find_todo.organization + '\',' +
                                                                (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ',null' +
                                                                ',\'' +
                                                                data.result.replace(/'/g, "''''") + '\',\'\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ','+
                                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                 (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'todo\','+
                                                                 reminder.todo_id+','+
                                                                (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {

                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('skebby send sms: ' + err);
                                                            });
                                                    }
                                            });
                                        }
                                        else {
                                         console.log(err);
                                        //return res.status(400).render('skebby send sms: ' + err);
                                        }
                                    });

                                }

                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    } else if (reminder.send_type == 'push') {
                        sequelize.query('SELECT find_todo(' +
                            reminder.todo_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_todo) {

                                var https = require('https');
                                var testo_sms = decode_text_sms(reminder, 'todo', todo[0].find_todo);

                                sequelize.query('SELECT get_push_player_id(' +
                                        reminder.send_to_id + ',\'' +
                                        reminder.organization.replace(/'/g, "''''") + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0] && datas[0].get_push_player_id && datas[0].get_push_player_id.length > 0) {
                                            var cnt = 0;
                                            var ids = [];
                                            datas[0].get_push_player_id.forEach(function(player) {
                                                ids.push(player);
                                                if (++cnt > datas[0].get_push_player_id.length - 1) {
                                                    var message = {
                                                        app_id: config.onesignal.app_id,
                                                        contents: {
                                                            "en": testo_sms
                                                        },
                                                        url: 'https://' + todo[0].find_todo.organization + '.beebeeboard.com',
                                                        include_player_ids: datas[0].get_push_player_id
                                                    };

                                                    var headers = {
                                                        "Content-Type": "application/json; charset=utf-8",
                                                        "Authorization": "Basic " + config.onesignal.app_key
                                                    };

                                                    var options = {
                                                        host: "onesignal.com",
                                                        port: 443,
                                                        path: "/api/v1/notifications",
                                                        method: "POST",
                                                        headers: headers
                                                    };

                                                    var https = require('https');
                                                    var req = https.request(options, function(res) {
                                                        res.on('data', function(data) {
                                                            console.log("Response:");
                                                            console.log(JSON.parse(data));
                                                        });
                                                    });

                                                    req.on('error', function(e) {
                                                        console.log("ERROR:");
                                                        console.log(e);
                                                    });

                                                    req.write(JSON.stringify(message));
                                                    req.end();

                                                    console.log('Reminder for todo sended correctly');

                                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_todo.organization + '\',\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                        .then(function(rem) {


                                                        }).catch(function(err) {
                                                           console.log(err);
                                                            //return res.status(400).render('reminder_destroy: ' + err);
                                                        });
                                                }
                                            });



                                        } else {
                                            console.log('Reminder for todo sended correctly');

                                            sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_todo.organization + '\',\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                                .then(function(rem) {


                                                }).catch(function(err) {
                                                   console.log(err);
                                                    //return res.status(400).render('reminder_destroy: ' + err);
                                                });
                                        }
                                    }).catch(function(err) {
                                       console.log(err);
                                        //return res.status(400).render('get_push_player_id: ' + err);
                                    });
                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    }

                } else if (reminder.event_id) {
                     console.log('REMINDER DI : '+reminder.organization_name + ' PER EVENTO: '+reminder.event_id);
                      if (reminder.send_type == 'mail' || 
                        (reminder.send_type == 'custom' && (reminder.pref_contact == 'MAIL' || !reminder.pref_contact))) {

                        sequelize.query('SELECT find_event(' +
                            reminder.event_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(event) {

                            if(event && event[0] && event[0].find_event){

                                var contact_string = '',
                                    product_string = '',
                                    project_string = '';
                                var contacts = [];


                                contacts.push(reminder.send_to_id);

                                if (event[0].find_event.json_build_object.contacts && event[0].find_event.json_build_object.contacts.length) {
                                    var cnt = 0;
                                    event[0].find_event.json_build_object.contacts.forEach(function(us) {
                                        if (cnt != 0) {
                                            contact_string += ', ';
                                        }
                                        contact_string += us.name;
                                        cnt++;
                                        time_zone = us.time_zone;
                                    });
                                }

                                if (event[0].find_event.json_build_object.products && event[0].find_event.json_build_object.products.length) {
                                    var cnt = 0;
                                    event[0].find_event.json_build_object.products.forEach(function(us) {
                                        if (cnt != 0) {
                                            product_string += ', ';
                                        }
                                        product_string += us.name;
                                        cnt++;
                                    });
                                }

                                if (event[0].find_event.json_build_object.projects && event[0].find_event.json_build_object.projects.length) {
                                    var cnt = 0;
                                    event[0].find_event.json_build_object.projects.forEach(function(us) {
                                        if (cnt != 0) {
                                            project_string += ', ';
                                        }
                                        project_string += us.name;
                                        cnt++;
                                    });
                                }

                                if(!time_zone){
                                    time_zone = 'Europe/Rome';
                                }

                                var testo_mail = decode_text_mail(reminder, 'event', event[0].find_event);
                                
                                async.parallel({
                                        'calendar_links': function(callback){
                                                async.parallel({
                                                        google: function(callback1) {
                                                            
                                                            callback1(null,'https://calendar.google.com/calendar/u/0/r/eventedit?dates='+moment(event[0].find_event.start_date).tz(time_zone).format('YYYYMMDDTHHmmss')+'/'+moment(event[0].find_event.end_date).tz(time_zone).format('YYYYMMDDTHHmmss')+'&details='+encodeURIComponent(testo_mail)+'&text='+uc_first(reminder.organization_name) + ' - Promemoria');   
                                                        },
                                                        
                                                        outlook: function(callback1) {
                                                            
                                                            callback1(null,'https://outlook.live.com/calendar/0/deeplink/compose?allday=false&body='+encodeURIComponent(testo_mail)+'&enddt='+moment(event[0].find_event.end_date).tz(time_zone).format('YYYY-MM-DDTHH:mm:ss')+'&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt='+moment(event[0].find_event.start_date).tz(time_zone).format('YYYY-MM-DDTHH:mm:ss')+'&subject='+uc_first(reminder.organization_name) + ' - Promemoria');   
                                                        
                                                        },
                                                        ical:  function(callback1) {
                                                            callback1(null, 'https://asset.beebeeboard.com/utility/ical.php?title='+uc_first(reminder.organization_name) + ' - Promemoria&sd='+moment(event[0].find_event.start_date).tz(time_zone).format('YYYY-MM-DDTHH:mm:ss')+'&ed='+moment(event[0].find_event.end_date).tz(time_zone).format('YYYY-MM-DDTHH:mm:ss')+'&description='+encodeURIComponent(testo_mail));
                                                        }/*,
                                                        file: function(callback1){
                                                            var request = https.get('https://asset.beebeeboard.com/utility/ical.php?title='+uc_first(reminder.organization_name) + ' - Promemoria&sd='+moment(event[0].find_event.start_date).tz(time_zone).format('YYYY-MM-DDTHH:mm:ss')+'&ed='+moment(event[0].find_event.end_date).tz(time_zone).format('YYYY-MM-DDTHH:mm:ss')+'&description='+encodeURIComponent(testo_mail), function(result) {

                                                            var chunks = [];

                                                            result.on('data', function(chunk) {
                                                                chunks.push(chunk);
                                                            });

                                                            result.on('end', function() {
                                                                var pdfData = new Buffer.concat(chunks);

                                                                callback1(null, pdfData);
                                                            });
                                                        }).on('error', function(err) {

                                                            return false;
                                                        });

                                                            request.end();  
                                                        }*/
                                                    }, function(err1, finalissimo) {
                                                        if(err1){
                                                            console.log('ERROR ' + err1);
                                                        }

                                                        var risp = {
                                                            'google_url':finalissimo.google.toString(),
                                                            'outlook_url':finalissimo.outlook.toString(),
                                                            'ical_url':finalissimo.ical.toString()/*,
                                                            'ical_file':finalissimo.file.toString()*/
                                                        };

                                                        console.log('CALENDAR_LINKS');
                                                        //console.log(risp);

                                                        callback(null, risp);
                                                     });
                                                
                                        },
                                        'confirm': function(callback) {
                                            
                                            let str = testo_mail;
                                            if (str.indexOf('{{confirm_sms}}') > -1) {

                                                var token = utils.uid(config.token.accessTokenLength);

                                                    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                                        moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                                        reminder.send_to_id + ',' +
                                                        5 + ',\'\',\'' + reminder.organization + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(tok) {
                                                            var longUrl = 'https://'+reminder.organization+'.beebeeboard.com/booking/#/confirm?access_token='+token+'&event_id='+reminder.event_id;

                                                            sequelize.query('SELECT insert_shortener(\'' +
                                                                longUrl + '\',\'' +
                                                                reminder.organization + '\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(shortner) {
                                                                shortUrl = 'https://' + reminder.organization + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                                                                str = str.replace('{{confirm_sms}}', shortUrl);
                                                                callback(null, str);
                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('find_todo_reminder: ' + err);
                                                               callback(err, null);
                                                            });
                                                        });
                                            }else{
                                                 callback(null, str);
                                            }
                                            
                                        }

                                }, function(err, resul) {
                                       // console.log(resul);
                                            var template_content = [{
                                                    "name": "ORGANIZATION",
                                                    "content": uc_first(reminder.organization_name)
                                                }, {
                                                    "name": "TEXT",
                                                    "content": resul.confirm ? resul.confirm : ' Promemoria Event',
                                                }, {
                                                    "name": "CONTACT_NAME",
                                                    "content": contact_string != '' ? contact_string : '-',
                                                }, {
                                                    "name": "PRODUCT_NAME",
                                                    "content": product_string != '' ? product_string : '-',
                                                }, {
                                                    "name": "PROJECT_NAME",
                                                    "content": project_string != '' ? project_string : '-',
                                                }, {
                                                    "name": "PRODUCT_LABEL",
                                                    "content": reminder.products_name
                                                }, {
                                                    "name": "PROJECT_LABEL",
                                                    "content": reminder.projects_name
                                                }, {
                                                    "name": "CONTACT_LABEL",
                                                    "content": reminder.contacts_name
                                                }, {
                                                    "name": "LOGO",
                                                    "content": reminder.thumbnail_url ? 'https://data.beebeeboard.com/' + reminder.thumbnail_url + '-small.jpeg' : null
                                                }, {
                                                    "name": "COLOR",
                                                    "content": reminder.primary_color ? reminder.primary_color : '#00587D'
                                                }, {
                                                    "name": "GOOGLE_URL",
                                                    "content": resul.calendar_links.google_url
                                                }, {
                                                    "name": "OUTLOOK_URL",
                                                    "content": resul.calendar_links.outlook_url
                                                }/*, {
                                                    "name": "ICAL_URL",
                                                    "content": resul.calendar_links.ical_url
                                                }*/
                                                ],
                                                message = {
                                                    "global_merge_vars": template_content,
                                                    "metadata": {
                                                        "organization": reminder.organization.replace(/'/g, "''''")
                                                    },
                                                    "subject": uc_first(reminder.organization_name) + ' - Promemoria',
                                                    "from_email": event[0].find_event.domain_from_mail ? event[0].find_event.domain_from_mail : "no-reply@beebeemailer.com",
                                                    "from_name": uc_first(reminder.organization_name),
                                                    "to": [{
                                                        "email": reminder.send_to_mail,
                                                        "name": reminder.send_to,
                                                        "type": "to"
                                                    }],
                                                    "headers": {
                                                        "Reply-To": reminder.send_from_mail
                                                    }/*,
                                                    "attachments": [{
                                                        'type': 'text/calendar',
                                                        'name': 'Appuntamento.ics',
                                                        'content': resul.calendar_links.ical_file
                                                    }]*/,
                                                    "tags": ["event-reminder", reminder.organization.replace(/'/g, "''''")],
                                                    "subaccount": "beebeeboard",
                                                    "tracking_domain": "mandrillapp.com",
                                                    "google_analytics_domains": ["poppix.it"]
                                                };

                                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                            mandrill_client.messages.sendTemplate({
                                                "template_name": "event-reminder-new",
                                                "template_content": template_content,
                                                "message": message,
                                                "async": true,
                                                "send_at": false
                                            }, function(result) {

                                                if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                                    console.log('Reminder for event sended correctly');
                                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + event[0].find_event.organization + '\',\'' +
                                                        decode_text_mail(reminder, 'event', event[0].find_event).replace(/'/g, "''''") + '\',\''+
                                                        JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                    })
                                                        .then(function(rem) {
                                                            sequelize.query('SELECT insert_mandrill_send_event(' +
                                                                    event[0].find_event._tenant_id + ',\'' +
                                                                    event[0].find_event.organization + '\',' +
                                                                    (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                                    result[0].status + '\',null,\'' +
                                                                    result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                                                    decode_text_mail(reminder, 'event', event[0].find_event).replace(/'/g, "''''") + '\',' +
                                                                    reminder.event_id + ');', {
                                                                        raw: true,
                                                                        type: Sequelize.QueryTypes.SELECT,
                                                                        useMaster: true
                                                                    })
                                                                .then(function(datas) {
                                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                                                }).catch(function(err) {
                                                                   console.log(err);
                                                                    //return res.status(400).render('mandrill send mail: ' + err);
                                                                });

                                                        }).catch(function(err) {
                                                           console.log(err);
                                                            //return res.status(400).render('reminder_destroy: ' + err);
                                                        });


                                                } else {
                                                    sequelize.query('SELECT insert_mandrill_send_event(' +
                                                            event[0].find_event._tenant_id + ',\'' +
                                                            event[0].find_event.organization + '\',' +
                                                            (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                            result[0].status + '\',\'' +
                                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                                            decode_text_mail(reminder, 'event', event[0].find_event).replace(/'/g, "''''") + '\',' +
                                                            reminder.event_id + ');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                        .then(function(datas) {
                                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                                        }).catch(function(err) {
                                                           console.log(err);
                                                            //return res.status(400).render('mandrill send mail: ' + err);
                                                        });
                                                }
                                            }, function(e) {
                                                // Mandrill returns the error as an object with name and message keys
                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                            });
                                });
                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });


                    } else if (reminder.send_type == 'sms' || 
                        (reminder.send_type == 'custom' && reminder.pref_contact == 'SMS')) {
                        sequelize.query('SELECT find_event(' +
                            reminder.event_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo && todo[0] && todo[0].find_event) {

                                var https = require('https');
                                var sender_string = reminder.mittente ? reminder.mittente : 'Beebeeboard';
                                var method = todo[0].find_event._tenant_id == 170 ? 'send_sms_classic_report' : 'send_sms_classic_report';
                                var lrecipients = [];
                                var mob_text = [];
                                var contact_ids = [];
                                var testo_sms = decode_text_sms(reminder, 'event', todo[0].find_event);

                              
                                if (reminder.send_to_mobile && reminder.send_to_mobile !== null && reminder.send_to_mobile != '') {
                                     console.log('NUMERO XXX: ' +reminder.send_to_mobile);
                                    if (reminder.send_to_mobile.indexOf('+') == -1) {
                                        reminder.send_to_mobile = '+39' + reminder.send_to_mobile.replace(/\D/g, '');
                                    } else {
                                        reminder.send_to_mobile = '+' + reminder.send_to_mobile.replace(/\D/g, '');
                                    }
                                    lrecipients.push(reminder.send_to_mobile);
                                    mob_text.push('\''+reminder.send_to_mobile+'\'');
                                    contact_ids.push(reminder.send_to_id);

                                    async.parallel({
                                        'confirm': function(callback) {
                                            
                                            let str = testo_sms;
                                            if (str.indexOf('{{confirm_sms}}') > -1) {

                                                var token = utils.uid(config.token.accessTokenLength);

                                                    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                                        moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                                        reminder.send_to_id + ',' +
                                                        5 + ',\'\',\'' + reminder.organization + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(tok) {
                                                            var longUrl = 'https://'+reminder.organization+'.beebeeboard.com/booking/#/confirm?access_token='+token+'&event_id='+reminder.event_id;

                                                            sequelize.query('SELECT insert_shortener(\'' +
                                                                longUrl + '\',\'' +
                                                                reminder.organization + '\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(shortner) {
                                                                shortUrl = 'https://' + reminder.organization + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                                                                str = str.replace('{{confirm_sms}}', shortUrl);
                                                                callback(null, str);
                                                            });
                                                         }).catch(function(err) {
                                                           console.log(err);
                                                           callback(err, null);
                                                        });
                                            }else{
                                                 callback(null, str);
                                            }
                                            
                                        }

                                    }, function(err, resul) {


                                        utility.login_skebby(function(err, auth) {
                                            if (!err) {
                                                var smsData = {
                                                    "returnCredits": true,
                                                    "recipient": lrecipients,
                                                    "message": resul.confirm ? resul.confirm : ' Promemoria Event',
                                                    "message_type": "GP",
                                                    "sender" : sender_string
                                                };

                                                utility.sendSMS(auth, smsData,
                                                    function(error, data) {
                                                        if (error) {
                                                            var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);

                                                          
                                                            sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                    todo[0].find_event._tenant_id + ',\'' +
                                                                    todo[0].find_event.organization + '\',' +
                                                                    (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ',null,\'' +
                                                                    error.replace(/'/g, "''''") + '\',\'' +
                                                                    error.replace(/'/g, "''''") + '\',\'' +
                                                                    testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ',' +
                                                                    (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                     (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'event\','+
                                                                    reminder.event_id+','+
                                                                    (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                        raw: true,
                                                                        type: Sequelize.QueryTypes.SELECT,
                                                                        useMaster: true
                                                                    })
                                                                .then(function(datas) {

                                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                                }).catch(function(err) {
                                                                   console.log(err);
                                                                    //return res.status(400).render('skebby send sms: ' + err);
                                                                });
                                                         }
                                                        else {
                                                            console.log('SMS: send correctly ' + JSON.stringify(data));

                                                            sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + 
                                                                todo[0].find_event.organization + '\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',\''+
                                                                JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                                .then(function(rem) {


                                                                }).catch(function(err) {
                                                                   console.log(err);
                                                                    //return res.status(400).render('reminder_destroy: ' + err);
                                                                });
                                                            
                                                           
                                                            var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                            sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                    todo[0].find_event._tenant_id + ',\'' +
                                                                    todo[0].find_event.organization + '\',' +
                                                                    (contact_ids.length > 0 ? 'ARRAY[' + contact_ids + ']::bigint[]' : null) + ',null,\'' +
                                                                    data.result.replace(/'/g, "''''") + '\',\'\',\'' +
                                                                    testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ',' +
                                                                    (data && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                    (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'event\','+
                                                                    reminder.event_id+','+
                                                                     (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                        type: Sequelize.QueryTypes.SELECT,
                                                                        useMaster: true
                                                                    })
                                                                .then(function(datas) {

                                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');


                                                                }).catch(function(err) {
                                                                   console.log(err);
                                                                    //return res.status(400).render('skebby send sms: ' + err);
                                                                });
                                                        }
                                                 });
                                            }
                                            else {
                                             console.log(err);
                                                //return res.status(400).render('skebby send sms: ' + err);
                                            }
                                        });


                                     });

                                }

                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    } else if (reminder.send_type == 'push') {
                        sequelize.query('SELECT find_event(' +
                            reminder.event_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_event) {

                                var https = require('https');
                                var testo_sms = decode_text_sms(reminder, 'event', todo[0].find_event);

                                sequelize.query('SELECT get_push_player_id(' +
                                        reminder.send_to_id + ',\'' +
                                        reminder.organization.replace(/'/g, "''''") + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        var organization = todo[0].find_event.organization;
                                        if (datas && datas[0] && datas[0].get_push_player_id && datas[0].get_push_player_id.length > 0) {
                                            var cnt = 0;
                                            var ids = [];
                                            datas[0].get_push_player_id.forEach(function(player) {
                                                ids.push(player);
                                                if (++cnt > datas[0].get_push_player_id.length - 1) {

                                                    var message = {
                                                        app_id: config.onesignal.app_id,
                                                        contents: {
                                                            "en": testo_sms
                                                        },
                                                        url: 'https://' + organization + '.beebeeboard.com',
                                                        include_player_ids: datas[0].get_push_player_id
                                                    };

                                                    var headers = {
                                                        "Content-Type": "application/json; charset=utf-8",
                                                        "Authorization": "Basic " + config.onesignal.app_key
                                                    };

                                                    var options = {
                                                        host: "onesignal.com",
                                                        port: 443,
                                                        path: "/api/v1/notifications",
                                                        method: "POST",
                                                        headers: headers
                                                    };

                                                    var https = require('https');
                                                    var req = https.request(options, function(res) {
                                                        res.on('data', function(data) {
                                                            console.log("Response:");
                                                            console.log(JSON.parse(data));
                                                        });
                                                    });

                                                    req.on('error', function(e) {
                                                        console.log("ERROR:");
                                                        console.log(e);
                                                    });

                                                    req.write(JSON.stringify(message));
                                                    req.end();

                                                    console.log('Reminder for event sended correctly');

                                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_event.organization + '\',\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                        .then(function(rem) {


                                                        }).catch(function(err) {
                                                           console.log(err);
                                                            //return res.status(400).render('reminder_destroy: ' + err);
                                                        });
                                                }
                                            });

                                        } else {
                                            console.log('Reminder for event sended correctly');

                                            sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_event.organization + '\',\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                                .then(function(rem) {


                                                }).catch(function(err) {
                                                   console.log(err);
                                                    //return res.status(400).render('reminder_destroy: ' + err);
                                                });

                                        }
                                    }).catch(function(err) {
                                       console.log(err);
                                        //return res.status(400).render('get_push_player_id: ' + err);
                                    });
                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    }
                } else if (reminder.invoice_id) {
                      if (reminder.send_type == 'mail' || 
                        (reminder.send_type == 'custom' && (reminder.pref_contact == 'MAIL' || !reminder.pref_contact))) {

                        sequelize.query('SELECT find_invoice(' +
                            reminder.invoice_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(invoice) {

                            var contact_string = '',
                                product_string = '',
                                project_string = '';
                            var contacts = [];

                            contacts.push(reminder.send_to_id);

                            if (invoice[0].find_invoice.json_build_object.contacts &&
                                invoice[0].find_invoice.json_build_object.contacts.length) {
                                var cnt = 0;
                                invoice[0].find_invoice.json_build_object.contacts.forEach(function(us) {
                                    if (cnt != 0) {
                                        contact_string += ', ';
                                    }
                                    contact_string += us.name;
                                    cnt++;
                                });
                            }

                            if (invoice[0].find_invoice.json_build_object.users &&
                                invoice[0].find_invoice.json_build_object.users.length) {
                                var cnt = 0;
                                invoice[0].find_invoice.json_build_object.users.forEach(function(us) {
                                    contact_string = us.name;
                                });
                            }

                            if (invoice[0].find_invoice.json_build_object.products &&
                                invoice[0].find_invoice.json_build_object.products.length) {
                                var cnt = 0;
                                invoice[0].find_invoice.json_build_object.products.forEach(function(us) {
                                    if (cnt != 0) {
                                        product_string += ', ';
                                    }
                                    product_string += us.name;
                                    cnt++;
                                });
                            }

                            if (invoice[0].find_invoice.json_build_object.projects &&
                                invoice[0].find_invoice.json_build_object.projects.length) {
                                var cnt = 0;
                                invoice[0].find_invoice.json_build_object.projects.forEach(function(us) {
                                    if (cnt != 0) {
                                        project_string += ', ';
                                    }
                                    project_string += us.name;
                                    cnt++;
                                });
                            }

                            var template_content = [{
                                    "name": "ORGANIZATION",
                                    "content": uc_first(reminder.organization_name)
                                }, {
                                    "name": "TEXT",
                                    "content": decode_text_mail(reminder, 'invoice', invoice[0].find_invoice)
                                }, {
                                    "name": "CONTACT_NAME",
                                    "content": contact_string != '' ? contact_string : '-',
                                }, {
                                    "name": "PRODUCT_NAME",
                                    "content": product_string != '' ? product_string : '-',
                                }, {
                                    "name": "PROJECT_NAME",
                                    "content": project_string != '' ? project_string : '-',
                                }, {
                                    "name": "PRODUCT_LABEL",
                                    "content": reminder.products_name
                                }, {
                                    "name": "PROJECT_LABEL",
                                    "content": reminder.projects_name
                                }, {
                                    "name": "CONTACT_LABEL",
                                    "content": reminder.contacts_name
                                }, {
                                    "name": "LOGO",
                                    "content": reminder.thumbnail_url ? 'https://data.beebeeboard.com/' + reminder.thumbnail_url + '-small.jpeg' : null
                                }, {
                                    "name": "COLOR",
                                    "content": reminder.primary_color ? reminder.primary_color : '#00587D'
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": reminder.organization.replace(/'/g, "''''")
                                    },
                                    "subject": reminder.organization_name + ' Promemoria Fattura',
                                    "from_email": invoice[0].find_invoice.domain_from_mail ? invoice[0].find_invoice.domain_from_mail : "no-reply@beebeemailer.com",
                                    "from_name": reminder.organization_name + ' Promemoria Fattura',
                                    "to": [{
                                        "email": reminder.send_to_mail,
                                        "name": reminder.send_to,
                                        "type": "to"
                                    }],
                                    "headers": {
                                        "Reply-To": 'no-reply@beebeemailer.com'
                                    },
                                    "tags": ["invoice-reminder", reminder.organization.replace(/'/g, "''''")],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "event-reminder",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {

                                if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                    console.log('Reminder for invoice sended correctly');
                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + invoice[0].find_invoice.organization + '\',\'' +
                                                    decode_text_mail(reminder, 'invoice', invoice[0].find_invoice).replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                        .then(function(rem) {
                                            sequelize.query('SELECT insert_mandrill_send_inv_rem(' +
                                                    invoice[0].find_invoice._tenant_id + ',\'' +
                                                    invoice[0].find_invoice.organization + '\',' +
                                                    (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                    result[0].status + '\',null,\'' +
                                                    result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                                    decode_text_mail(reminder, 'invoice', invoice[0].find_invoice).replace(/'/g, "''''") + '\',' +
                                                    reminder.invoice_id + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                                }).catch(function(err) {
                                                   console.log(err);
                                                    //return res.status(400).render('mandrill send mail: ' + err);
                                                });

                                        }).catch(function(err) {
                                           console.log(err);
                                            //return res.status(400).render('reminder_destroy: ' + err);
                                        });


                                } else {
                                    sequelize.query('SELECT insert_mandrill_send_inv_rem(' +
                                            invoice[0].find_invoice._tenant_id + ',\'' +
                                            invoice[0].find_invoice.organization + '\',' +
                                            (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                            result[0].status + '\',\'' +
                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                            decode_text_mail(reminder, 'invoice', invoice[0].find_invoice).replace(/'/g, "''''") + '\',' +
                                            reminder.invoice_id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                        }).catch(function(err) {
                                           console.log(err);
                                            //return res.status(400).render('mandrill send mail: ' + err);
                                        });
                                }

                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                            });
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });

                    } else if (reminder.send_type == 'sms' || 
                        (reminder.send_type == 'custom' && reminder.pref_contact == 'SMS')) {
                        sequelize.query('SELECT find_invoice(' +
                            reminder.invoice_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_invoice) {

                                var https = require('https');
                                var sender_string = reminder.mittente ? reminder.mittente : 'Beebeeboard';
                                var method = todo[0].find_invoice._tenant_id == 170 ? 'send_sms_classic_report' : 'send_sms_classic_report';
                                var lrecipients = [];
                                 var mob_text = [];
                                var contacts = [];
                                var testo_sms = decode_text_sms(reminder, 'invoice', todo[0].find_invoice);

                                if (reminder.send_to_mobile) {
                                    if (reminder.send_to_mobile.indexOf('+') == -1) {
                                        reminder.send_to_mobile = '+39' + reminder.send_to_mobile.replace(/\D/g, '');
                                    } else {
                                        reminder.send_to_mobile = '+' + reminder.send_to_mobile.replace(/\D/g, '');
                                    }
                                    lrecipients.push(reminder.send_to_mobile);
                                    mob_text.push('\''+reminder.send_to_mobile+'\'');
                                    contacts.push(reminder.send_to_id);

                                    utility.login_skebby(function(err, auth) {
                                        if (!err) {
                                            var smsData = {
                                                "returnCredits": true,
                                                "recipient": lrecipients,
                                                "message": testo_sms ? testo_sms : ' Promemoria Fattura',
                                                "message_type": "GP",
                                                "sender" : sender_string
                                            };

                                            utility.sendSMS(auth, smsData,
                                                function(error, data) {
                                                    if (error) {
                                                       var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                todo[0].find_invoice._tenant_id + ',\'' +
                                                                todo[0].find_invoice.organization + '\',' +
                                                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                                err.replace(/'/g, "''''") + '\',\'' +
                                                                err.replace(/'/g, "''''") + '\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ','+
                                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                 (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'invoice\','+
                                                                reminder.invoice_id+','+
                                                                (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {

                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('skebby send sms: ' + err);
                                                            });
                                                     }
                                                    else {
                                                        console.log('SMS: send correctly ' + JSON.stringify(data));

                                                        sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_invoice.organization + '\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',\''+
                                                            JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                            .then(function(rem) {


                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('reminder_destroy: ' + err);
                                                            });
                                                        var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                todo[0].find_invoice._tenant_id + ',\'' +
                                                                todo[0].find_invoice.organization + '\',' +
                                                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                                data.result.replace(/'/g, "''''") + '\',\'\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ','+
                                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                 (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'invoice\','+
                                                                reminder.invoice_id+','+
                                                                (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {

                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('skebby send sms: ' + err);
                                                            });
                                                    }
                                            });
                                        }
                                        else {
                                         console.log(err);
                                            //return res.status(400).render('skebby send sms: ' + err);
                                        }
                                    });

                                }

                            }
                        }).catch(function(err) {
                           console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    } else if (reminder.send_type == 'push') {
                        sequelize.query('SELECT find_invoice(' +
                            reminder.invoice_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_invoice) {

                                var https = require('https');
                                var testo_sms = decode_text_sms(reminder, 'invoice', todo[0].find_invoice);

                                sequelize.query('SELECT get_push_player_id(' +
                                        reminder.send_to_id + ',\'' +
                                        reminder.organization.replace(/'/g, "''''") + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0] && datas[0].get_push_player_id && datas[0].get_push_player_id.length > 0) {
                                            var cnt = 0;
                                            var ids = [];
                                            datas[0].get_push_player_id.forEach(function(player) {
                                                ids.push(player);
                                                if (++cnt > datas[0].get_push_player_id.length - 1) {
                                                    var message = {
                                                        app_id: config.onesignal.app_id,
                                                        contents: {
                                                            "en": testo_sms
                                                        },
                                                        url: 'https://' + todo[0].find_invoice.organization + '.beebeeboard.com',
                                                        include_player_ids: datas[0].get_push_player_id
                                                    };

                                                    var headers = {
                                                        "Content-Type": "application/json; charset=utf-8",
                                                        "Authorization": "Basic " + config.onesignal.app_key
                                                    };

                                                    var options = {
                                                        host: "onesignal.com",
                                                        port: 443,
                                                        path: "/api/v1/notifications",
                                                        method: "POST",
                                                        headers: headers
                                                    };

                                                    var https = require('https');
                                                    var req = https.request(options, function(res) {
                                                        res.on('data', function(data) {
                                                            console.log("Response:");
                                                            console.log(JSON.parse(data));
                                                        });
                                                    });

                                                    req.on('error', function(e) {
                                                        console.log("ERROR:");
                                                        console.log(e);
                                                    });

                                                    req.write(JSON.stringify(message));
                                                    req.end();

                                                    console.log('Reminder for invoice sended correctly');

                                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_invoice.organization + '\'\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                        .then(function(rem) {


                                                        }).catch(function(err) {
                                                           console.log(err);
                                                           //return res.status(400).render('reminder_destroy: ' + err);
                                                        });
                                                }
                                            });

                                        } else {
                                            console.log('Reminder for todo sended correctly');

                                            sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_invoice.organization + '\'\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                                .then(function(rem) {


                                                }).catch(function(err) {
                                                   console.log(err);
                                                   //return res.status(400).render('reminder_destroy: ' + err);
                                                });
                                        }
                                    }).catch(function(err) {
                                        console.log(err);
                                        //return res.status(400).render('get_push_player_id: ' + err);
                                    });
                            }
                        }).catch(function(err) {
                           console.log(err);
                        //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    }
                } else if (reminder.expense_id) {
                     if (reminder.send_type == 'mail' || 
                        (reminder.send_type == 'custom' && (reminder.pref_contact == 'MAIL' || !reminder.pref_contact))) {

                        sequelize.query('SELECT find_expense(' +
                            reminder.expense_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(expense) {
                            var contact_string = '',
                                product_string = '',
                                project_string = '';
                            var contacts = [];

                            contacts.push(reminder.send_to_id);

                            if (expense[0].find_expense.json_build_object.contacts && expense[0].find_expense.json_build_object.contacts.length) {
                                var cnt = 0;
                                expense[0].find_expense.json_build_object.contacts.forEach(function(us) {
                                    if (cnt != 0) {
                                        contact_string += ', ';
                                    }
                                    contact_string += us.name;
                                    cnt++;
                                });
                            }

                            if (expense[0].find_expense.json_build_object.users && expense[0].find_expense.json_build_object.users.length) {
                                var cnt = 0;
                                expense[0].find_expense.json_build_object.users.forEach(function(us) {
                                    if (cnt != 0) {
                                        contact_string += ', ';
                                    }
                                    contact_string += us.name;
                                    cnt++;
                                });
                            }

                            if (expense[0].find_expense.json_build_object.products && expense[0].find_expense.json_build_object.products.length) {
                                var cnt = 0;
                                expense[0].find_expense.json_build_object.products.forEach(function(us) {
                                    if (cnt != 0) {
                                        product_string += ', ';
                                    }
                                    product_string += us.name;
                                    cnt++;
                                });
                            }

                            if (expense[0].find_expense.json_build_object.projects && expense[0].find_expense.json_build_object.projects.length) {
                                var cnt = 0;
                                expense[0].find_expense.json_build_object.projects.forEach(function(us) {
                                    if (cnt != 0) {
                                        project_string += ', ';
                                    }
                                    project_string += us.name;
                                    cnt++;
                                });
                            }

                            var template_content = [{
                                    "name": "ORGANIZATION",
                                    "content": uc_first(reminder.organization_name)
                                }, {
                                    "name": "TEXT",
                                    "content": decode_text_mail(reminder, 'expense', expense[0].find_expense)
                                }, {
                                    "name": "CONTACT_NAME",
                                    "content": contact_string != '' ? contact_string : '-',
                                }, {
                                    "name": "PRODUCT_NAME",
                                    "content": product_string != '' ? product_string : '-',
                                }, {
                                    "name": "PROJECT_NAME",
                                    "content": project_string != '' ? project_string : '-',
                                }, {
                                    "name": "PRODUCT_LABEL",
                                    "content": reminder.products_name
                                }, {
                                    "name": "PROJECT_LABEL",
                                    "content": reminder.projects_name
                                }, {
                                    "name": "CONTACT_LABEL",
                                    "content": reminder.contacts_name
                                }, {
                                    "name": "LOGO",
                                    "content": reminder.thumbnail_url ? 'https://data.beebeeboard.com/' + reminder.thumbnail_url + '-small.jpeg' : null
                                }, {
                                    "name": "COLOR",
                                    "content": reminder.primary_color ? reminder.primary_color : '#00587D'
                                }],
                                message = {
                                    "global_merge_vars": template_content,
                                    "metadata": {
                                        "organization": reminder.organization.replace(/'/g, "''''")
                                    },
                                    "subject": reminder.organization_name + ' Promemoria Acquisto',
                                    "from_email": expense[0].find_expense.domain_from_mail ? expense[0].find_expense.domain_from_mail : "no-reply@beebeemailer.com",
                                    "from_name": reminder.organization_name + ' Promemoria Acquisto',
                                    "to": [{
                                        "email": reminder.send_to_mail,
                                        "name": reminder.send_to,
                                        "type": "to"
                                    }],
                                    "headers": {
                                        "Reply-To": 'no-reply@beebeemailer.com'
                                    },
                                    "tags": ["expense-reminder", reminder.organization.replace(/'/g, "''''")],
                                    "subaccount": "beebeeboard",
                                    "tracking_domain": "mandrillapp.com",
                                    "google_analytics_domains": ["poppix.it"]
                                };

                            var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                            mandrill_client.messages.sendTemplate({
                                "template_name": "event-reminder",
                                "template_content": template_content,
                                "message": message,
                                "async": true,
                                "send_at": false
                            }, function(result) {

                                if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                                    console.log('Reminder for invoice sended correctly');
                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + expense[0].find_expense.organization + '\',\'' +
                                                    decode_text_mail(reminder, 'expense', expense[0].find_expense).replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                        .then(function(rem) {
                                            sequelize.query('SELECT insert_mandrill_send_exp_rem(' +
                                                    expense[0].find_expense._tenant_id + ',\'' +
                                                    expense[0].find_expense.organization + '\',' +
                                                    (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
                                                    'null,\'' +
                                                    result[0].status + '\',null,\'' +
                                                    result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                                    decode_text_mail(reminder, 'expense', expense[0].find_expense).replace(/'/g, "''''") + '\',' +
                                                    reminder.expense_id + ');', {
                                                        raw: true,
                                                        type: Sequelize.QueryTypes.SELECT,
                                                        useMaster: true
                                                    })
                                                .then(function(datas) {
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                                }).catch(function(err) {
                                                    console.log(err);
                                                     //return res.status(400).render('mandrill send mail: ' + err);
                                                });

                                        }).catch(function(err) {
                                            console.log(err);
                                             //return res.status(400).render('reminder_destroy: ' + err);
                                        });


                                } else {
                                    sequelize.query('SELECT insert_mandrill_send_exp_rem(' +
                                            expense[0].find_expense._tenant_id + ',\'' +
                                            expense[0].find_expense.organization + '\',' +
                                            (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
                                            'null,\'' +
                                            result[0].status + '\',\'' +
                                            result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(reminder.organization_name) + ' - Promemoria\',\'' +
                                            decode_text_mail(reminder, 'expense', expense[0].find_expense).replace(/'/g, "''''") + '\',' +
                                                    reminder.expense_id + ');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');


                                        }).catch(function(err) {
                                            console.log(err);
                                            //return res.status(400).render('mandrill send mail: ' + err);
                                        });
                                }
                            }, function(e) {
                                // Mandrill returns the error as an object with name and message keys
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                            });

                        }).catch(function(err) {
                            console.log(err);
                            //return res.status(400).render('find_expense_reminder: ' + err);
                        });

                    } else if (reminder.send_type == 'sms' || 
                        (reminder.send_type == 'custom' && reminder.pref_contact == 'SMS')) {
                        sequelize.query('SELECT find_expense(' +
                            reminder.expense_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_expense) {

                                var https = require('https');
                                var sender_string = reminder.mittente ? reminder.mittente : 'Beebeeboard';
                                var method = todo[0].find_expense._tenant_id == 170 ? 'send_sms_classic_report' : 'send_sms_classic_report';
                                var lrecipients = [];
                                 var mob_text = [];
                                var contacts = [];
                                var testo_sms = decode_text_sms(reminder, 'expense', todo[0].find_expense);

                                if (reminder.send_to_mobile) {
                                    if (reminder.send_to_mobile.indexOf('+') == -1) {
                                        reminder.send_to_mobile = '+39' + reminder.send_to_mobile.replace(/\D/g, '');
                                    } else {
                                        reminder.send_to_mobile = '+' + reminder.send_to_mobile.replace(/\D/g, '');
                                    }
                                    lrecipients.push(reminder.send_to_mobile);
                                    mob_text.push('\''+reminder.send_to_mobile+'\'');
                                    contacts.push(reminder.send_to_id);

                                    utility.login_skebby(function(err, auth) {
                                        if (!err) {
                                            var smsData = {
                                                "returnCredits": true,
                                                "recipient": lrecipients,
                                                "message": testo_sms ? testo_sms : ' Promemoria Acquisto',
                                                "message_type": "GP",
                                                "sender" : sender_string
                                            };

                                            utility.sendSMS(auth, smsData,
                                                function(error, data) {
                                                    if (error) {
                                                       var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                todo[0].find_expense._tenant_id + ',\'' +
                                                                todo[0].find_expense.organization + '\',' +
                                                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                                err.replace(/'/g, "''''") + '\',\'' +
                                                                err.replace(/'/g, "''''") + '\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ','+
                                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                 (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'expense\','+
                                                                reminder.expense_id+','+
                                                                (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {
                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                            }).catch(function(err) {
                                                                console.log(err);
                                                                //return res.status(400).render('skebby send sms: ' + err);
                                                            });
                                                     }
                                                    else {
                                                        console.log('SMS: send correctly ' + JSON.stringify(data));

                                                        sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_expense.organization + '\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',\''+
                                                            JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                                raw: true,
                                                                type: Sequelize.QueryTypes.SELECT,
                                                                useMaster: true
                                                            })
                                                            .then(function(rem) {


                                                            }).catch(function(err) {
                                                               console.log(err);
                                                                //return res.status(400).render('reminder_destroy: ' + err);
                                                            });
                                                        var num_sms = parseInt(parseInt(testo_sms.length / 160) + 1);
                                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                                                todo[0].find_expense._tenant_id + ',\'' +
                                                                todo[0].find_expense.organization + '\',' +
                                                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',null,\'' +
                                                                data.result.replace(/'/g, "''''") + '\',\'\',\'' +
                                                                testo_sms.replace(/'/g, "''''") + '\',' + num_sms + ','+
                                                                (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                                                 (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',\'expense\','+
                                                                reminder.expense_id+','+
                                                                (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {
                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                                            }).catch(function(err) {
                                                                console.log(err);
                                                                //return res.status(400).render('skebby send sms: ' + err);
                                                            });
                                                    }
                                            });
                                        }
                                        else {
                                          console.log(err);
                                          //return res.status(400).render('skebby send sms: ' + err);
                                        }
                                    });

                                    
                                }
                            }
                        }).catch(function(err) {
                            console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                        });
                    } else if (reminder.send_type == 'push') {
                        sequelize.query('SELECT find_expense(' +
                            reminder.expense_id + ',\'' +
                            reminder.organization.replace(/'/g, "''''") + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            }).then(function(todo) {
                            if (todo[0].find_invoice) {

                                var https = require('https');
                                var testo_sms = decode_text_sms(reminder, 'expense', todo[0].find_expense);

                                sequelize.query('SELECT get_push_player_id(' +
                                        reminder.send_to_id + ',\'' +
                                        reminder.organization.replace(/'/g, "''''") + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                    .then(function(datas) {
                                        if (datas && datas[0] && datas[0].get_push_player_id && datas[0].get_push_player_id.length > 0) {
                                            var cnt = 0;
                                            var ids = [];
                                            datas[0].get_push_player_id.forEach(function(player) {
                                                ids.push(player);
                                                if (++cnt > datas[0].get_push_player_id.length - 1) {
                                                    var message = {
                                                        app_id: config.onesignal.app_id,
                                                        contents: {
                                                            "en": testo_sms
                                                        },
                                                        url: 'https://' + todo[0].find_expense.organization + '.beebeeboard.com',
                                                        include_player_ids: datas[0].get_push_player_id
                                                    };

                                                    var headers = {
                                                        "Content-Type": "application/json; charset=utf-8",
                                                        "Authorization": "Basic " + config.onesignal.app_key
                                                    };

                                                    var options = {
                                                        host: "onesignal.com",
                                                        port: 443,
                                                        path: "/api/v1/notifications",
                                                        method: "POST",
                                                        headers: headers
                                                    };

                                                    var https = require('https');
                                                    var req = https.request(options, function(res) {
                                                        res.on('data', function(data) {
                                                            console.log("Response:");
                                                            console.log(JSON.parse(data));
                                                        });
                                                    });

                                                    req.on('error', function(e) {
                                                        console.log("ERROR:");
                                                        console.log(e);
                                                    });

                                                    req.write(JSON.stringify(message));
                                                    req.end();

                                                    console.log('Reminder for expense sended correctly');

                                                    sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_expense.organization + '\',\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                        .then(function(rem) {


                                                        }).catch(function(err) {
                                                            console.log(err);
                                                            //return res.status(400).render('reminder_destroy: ' + err);
                                                        });
                                                }
                                            });

                                        } else {
                                            console.log('Reminder for todo sended correctly');

                                            sequelize.query('SELECT delete_reminder_from_cron_ghost(' + reminder.reminder_id + ',\'' + todo[0].find_expense.organization + '\',\'' +
                                                        testo_sms.replace(/'/g, "''''") + '\',\''+
                                                    JSON.stringify(reminder).replace(/'/g, "''''") +'\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                })
                                                .then(function(rem) {


                                                }).catch(function(err) {
                                                    console.log(err);
                                                     //return res.status(400).render('reminder_destroy: ' + err);
                                                });
                                        }
                                    }).catch(function(err) {
                                        console.log(err);
                                        //return res.status(400).render('get_push_player_id: ' + err);
                                    });
                            }
                        }).catch(function(err) {
                            console.log(err);
                            //return res.status(400).render('find_event_reminder: ' + err);
                           
                        });
                    }
                }

                if (cnt++ >= datas[0].elenco_reminders_ghost.length - 1) {
                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + 'N°: ' + cnt + ' reminder schedulati');
                    return res.status(200).send({});
                }

            });
        } else {
            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Nessun reminder da inviare');
            return res.status(200).send({});
        }

    }).catch(function(err) {
       return res.status(400).render('user_elenco_registrations: ' + err);
    });
};


exports.reminder_data = function (req, res){
     var base58 = require('../utility/base58.js');
     sequelize.query('SELECT get_reminder_settings(\'' +
                 req.headers['host'].split(".")[0] + '\');', {
                     raw: true,
                     type: Sequelize.QueryTypes.SELECT
                 })
             .then(function(datas) {
                if(datas && datas[0] && datas[0].get_reminder_settings){
                  var reminder = datas[0].get_reminder_settings;
                  sequelize.query('SELECT find_event(' +
                        req.query.event_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT
                        }).then(function(event) {


                        var testo_mail = decode_text_mail(reminder, 'event', event[0].find_event);
                        var testo_sms = decode_text_sms(reminder, 'event', event[0].find_event);

                        console.log(testo_sms);

                         async.parallel(
                         {
                                'confirm_sms': function(callback) {
                                    
                                    let str = testo_sms;
                                    if (str.indexOf('{{confirm_sms}}') > -1) {

                                        var token = utils.uid(config.token.accessTokenLength);

                                            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                                moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                                req.query.contact_id + ',' +
                                                5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                }).then(function(tok) {
                                                    var longUrl = 'https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/booking/#/confirm?access_token='+token+'&event_id='+req.query.event_id;

                                                    sequelize.query('SELECT insert_shortener(\'' +
                                                        longUrl + '\',\'' +
                                                        req.headers['host'].split(".")[0] + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(shortner) {
                                                        shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                                                        str = str.replace('{{confirm_sms}}', shortUrl);
                                                        callback(null, str);
                                                    });
                                                });
                                    }else{
                                         callback(null, str);
                                    }
                                    
                                },
                                'confirm_mail': function(callback) {
                                    
                                    let str = testo_mail;
                                    if (str.indexOf('{{confirm_sms}}') > -1) {

                                        var token = utils.uid(config.token.accessTokenLength);

                                            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                                                moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                                                req.query.contact_id + ',' +
                                                5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                                                    raw: true,
                                                    type: Sequelize.QueryTypes.SELECT,
                                                    useMaster: true
                                                }).then(function(tok) {
                                                    var longUrl = 'https://'+req.headers['host'].split(".")[0]+'.beebeeboard.com/booking/#/confirm?access_token='+token+'&event_id='+req.query.event_id;

                                                    sequelize.query('SELECT insert_shortener(\'' +
                                                        longUrl + '\',\'' +
                                                        req.headers['host'].split(".")[0] + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(shortner) {
                                                        shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                                                        str = str.replace('{{confirm_sms}}', shortUrl);
                                                        callback(null, str);
                                                    });
                                                });
                                    }else{
                                         callback(null, str);
                                    }
                                    
                                },
                                'evento': function(callback) {
                                    
                                     callback(null, event[0].find_event);
                                    
                                }

                        }, function(err, resul) {
                                  
                              res.status(200).send({
                                    'testo_mail':resul.confirm_mail,
                                    'testo_sms':resul.confirm_sms,
                                    'event':resul.evento
                              });
                        });
                      
                  });
                }else
                {
                    res.status(200).send();
                }
         }).catch(function(err) {
             console.log(err);
              return res.status(400).render('error paramenter: ' + err);
         });
   
}

/**
 * 
 * req.authInfo is set using the `info` argument supplied by
 * `BearerStrategy`. It is typically used to indicate scope of the token,
 * and used in access control checks. For illustrative purposes, this
 * example simply returns the scope in the response.
 * 
 */
exports.info = function(req, res) {
    res.json({
        contact_id: req.user.id,
        name: req.user.name,
        scope: req.authInfo.scope
    });
};

function formatta_euro(number) {
    if (number) {
        var numberStr = parseFloat(number).toFixed(2).toString();
        var numFormatDec = numberStr.slice(-2);
        numberStr = numberStr.substring(0, numberStr.length - 3);
        var numFormat = [];
        while (numberStr.length > 3) {
            numFormat.unshift(numberStr.slice(-3));
            numberStr = numberStr.substring(0, numberStr.length - 3);
        }
        numFormat.unshift(numberStr);
        return numFormat.join('.') + ',' + numFormatDec;
    }
    return '0,00';
};

exports.paypal_ipn = function(req, res) {

    var request = require('request'),
        https = require('https'),
        mandrill = require('mandrill-api/mandrill');

    req.body = req.body || {};

     sequelize.query('SELECT save_paypal_ipn(\'' +
        req.headers['host'].split(".")[0] + '\',\'' + JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(datas_o) {
    });

    sequelize.query('SELECT search_organization(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas_o) {


        var postreq = 'cmd=_notify-validate';
    
        for (var key in req.body) {
            if (req.body.hasOwnProperty(key)) {
                postreq = `${postreq}&${key}=${encodeURIComponent(req.body[key])}`;
            }
        }

        console.log('postreq', postreq);

        const options = {
            url: 'https://ipnpb.paypal.com/cgi-bin/webscr', // LIVE
            //url: 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr', // SANDBOX
            method: 'POST',
            headers: {
                'Content-Length': postreq.length,
            },
            encoding: 'utf-8',
            body: postreq
        };
        

        /*request({
            url: 'https://asset.beebeeboard.com/paypal/verify.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: postreq
        }, function callback(error, response, body) {*/
        request(options, (error, response, body) => {

            console.log('error', error);
            console.log('body', body);

            if (!error && response.statusCode === 200 && req.body['custom']) {
                var item_name = req.body['item_name'];
                var item_number = req.body['item_number'];
                var payment_status = req.body['payment_status'];
                var payment_amount = parseFloat(req.body['mc_gross']);
                var payment_currency = req.body['mc_currency'];
                var txn_id = req.body['txn_id'];
                var receiver_email = req.body['receiver_email'];
                var payer_email = req.body['payer_email'];
                var custom = parseInt(req.body['custom']);
                var pay_date = moment(new Date(req.body['payment_date'])).format('DD-MM-YYYY HH:mm:ss');
                var contact_name = req.body['first_name'] + ' ' + req.body['last_name'];
                /*var mails = [{
                    "email": payer_email,
                    "type": "to"
                }];

                var mail = datas_o[0].search_organization.ecommerce_mail;

                if (mail !== '' && mail !== null && mail !== undefined) {
                    mails.push({
                        "email": mail,
                        "type": "to"
                    });
                }*/

                // inspect IPN validation result and act accordingly
                if (body.substring(0, 8) === 'VERIFIED') {

                    console.log('Verified IPN!');
                    console.log("payment_status:", payment_status);

                    // IPN message values depend upon the type of notification sent.
                    // To loop through the &_POST array and print the NV pairs to the screen:

                    if (payment_status.toLowerCase() == 'completed' || payment_status.toLowerCase() == 'refunded') {
                        sequelize.query('SELECT scuole_ecommerce_paypal_success(' +
                            datas_o[0].search_organization._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            custom + ',\'' +
                            payer_email + '\',\'' +
                            payment_currency + '\',\'' +
                            txn_id + '\',\'' +
                            payment_status + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                                useMaster: true
                            }).then(function(datas) {

                            var url = '';
                            if (datas_o[0].search_organization.ecommerce_mail &&
                                datas_o[0].search_organization.ecommerce_mail !== null &&
                                datas_o[0].search_organization.ecommerce_mail != '' &&
                                datas_o[0].search_organization.ecommerce_mail !== undefined) {
                                url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?allegati=true&mail=${datas_o[0].search_organization.ecommerce_mail}&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0]}&payment_id=${custom}&completo=false&esito=OK`;
                            } else {
                                url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?allegati=true&token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0]}&payment_id=${custom}&completo=false&esito=OK`;
                            }

                            var request = https.get(url, function(result) {

                                result.on('end', function() {

                                });
                            }).on('error', function(err) {

                                return res.status(400).render('IPN PAypal: Mail Not Sended: ' + err);
                            });

                            request.end();

                            res.status(200).send({});
                            /*
                            sequelize.query('SELECT scuolasci_ecommerce_find_products_from_payment(\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                datas_o[0].search_organization._tenant_id + ',' +
                                custom + ');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                }).then(function(product) {

                               
                                var product_string = '';
                                if (product && product[0] && product[0].scuolasci_ecommerce_find_products_from_payment && product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products && product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products.length > 0 &&
                                    product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products.toString() != 'Nessun prodotto') {
                                    var cnt = 0;
                                    product[0].scuolasci_ecommerce_find_products_from_payment.json_build_object.products.forEach(function(us) {
                                        if (cnt != 0) {
                                            product_string += ', ';
                                        }
                                        product_string += us.name;
                                        cnt++;
                                    });
                                } else {
                                    product_string = 'Pagamento richiesto ';
                                }


                                var template_content = [{
                                        "name": "ORGANIZATION",
                                        "content": uc_first(datas_o[0].search_organization.name)
                                    }, {
                                        "name": "COLOR",
                                        "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                                    }, {
                                        "name": "LOGO",
                                        "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                                    }, {
                                        "name": "CLIENTE",
                                        "content": contact_name
                                    }, {
                                        "name": "DATA_PAGAMENTO",
                                        "content": pay_date
                                    }, {
                                        "name": "PRODOTTI",
                                        "content": product_string
                                    }, {
                                        "name": "IMPORTO",
                                        "content": formatta_euro(payment_amount)
                                    }],
                                    message = {
                                        "global_merge_vars": template_content,
                                        "metadata": {
                                            "organization": req.headers['host'].split(".")[0]
                                        },
                                        "subject": 'Conferma pagamento tramite PayPal',
                                        "from_email": "no-reply@beebeemailer.com",
                                        "from_name": uc_first(datas_o[0].search_organization.name),
                                        "to": mails,
                                        "headers": {
                                            "Reply-To": "no-reply@beebeemailer.com"
                                        },
                                        "tags": ["conferma-pagamento"],
                                        "subaccount": "beebeeboard",
                                        "tracking_domain": "mandrillapp.com",
                                        "google_analytics_domains": ["poppix.it"]
                                    };

                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                mandrill_client.messages.sendTemplate({
                                    "template_name": "conferma-pagamento",
                                    "template_content": template_content,
                                    "message": message,
                                    "async": true,
                                    "send_at": false
                                }, function(result) {

                                }, function(e) {
                                    // Mandrill returns the error as an object with name and message keys
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                    return res.status(422).send({});
                                });
                            }).catch(function(err) {
                                return res.status(400).render('scuole_ecommerce_find_products_from_payment: ' + err);
                            });*/
                        }).catch(function(err) {
                            return res.status(400).render('scuole_ecommerce_paypal_success: ' + err);
                        });

                    }

                } else if (body.substring(0, 7) === 'INVALID') {
                    // IPN invalid, log for manual investigation
                    console.log('Invalid IPN!');
                    var custom = req.body['custom'];

                    sequelize.query('SELECT scuole_ecommerce_paypal_error(' +
                        datas_o[0].search_organization._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        custom + ',\'' +
                        payer_email + '\',\'' +
                        payment_currency + '\',\'' +
                        txn_id + '\',\'' +
                        payment_status + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        }).then(function(datas) {

                        var url = `https://asset.beebeeboard.com/asset_scuole_sci/esito_pagamento_mail.php?token=${datas_o[0].search_organization.ecommerce_tok}&organization=${req.headers['host'].split(".")[0]}&payment_id=${custom}&completo=true&esito=KO`;

                        var request = https.get(url, function(result) {

                            result.on('end', function() {

                            });
                        }).on('error', function(err) {

                            return res.status(400).render('IPN PAypal: Mail Not Sended: ' + err);
                        });

                        request.end();

                        res.status(200).send({});

                        /*var template_content = [{
                                "name": "ORGANIZATION",
                                "content": uc_first(datas_o[0].search_organization.name)
                            }, {
                                "name": "COLOR",
                                "content": datas_o[0].search_organization.primary_color ? datas_o[0].search_organization.primary_color : '#00587D'
                            }, {
                                "name": "LOGO",
                                "content": datas_o[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + datas_o[0].search_organization.thumbnail_url + '-small.jpeg' : null
                            }, {
                                "name": "CLIENTE",
                                "content": contact_name
                            }, {
                                "name": "DATA_PAGAMENTO",
                                "content": pay_date
                            }, {
                                "name": "IMPORTO",
                                "content": formatta_euro(payment_amount)
                            }],
                            message = {
                                "global_merge_vars": template_content,
                                "metadata": {
                                    "organization": req.headers['host'].split(".")[0]
                                },
                                "subject": 'Pagamento fallito tramite PayPal',
                                "from_email": "no-reply@beebeemailer.com",
                                "from_name": uc_first(datas_o[0].search_organization.name),
                                "to": [{
                                    "email": payer_email,
                                    "type": "to"
                                }],
                                "headers": {
                                    "Reply-To": "no-reply@beebeemailer.com"
                                },
                                "tags": ["fallito-payment"],
                                "subaccount": "beebeeboard",
                                "tracking_domain": "mandrillapp.com",
                                "google_analytics_domains": ["poppix.it"]
                            };

                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                        mandrill_client.messages.sendTemplate({
                            "template_name": "fallito-pagamento",
                            "template_content": template_content,
                            "message": message,
                            "async": true,
                            "send_at": false
                        }, function(result) {

                        }, function(e) {
                            // Mandrill returns the error as an object with name and message keys
                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                            return res.status(422).send({});
                        });*/

                    }).catch(function(err) {
                        return res.status(400).render('scuole_ecommerce_paypal_error: ' + err);
                    });
                } else {
                    res.status(404).send({});
                }
            } else {
                res.status(404).send({});
            }
        });

    });
};

exports.unicredit_request_payment = function(req, res) {

    if (req.user.role[0].json_build_object.attributes.is_client) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var mails = [];
    var contacts = [];
    var emails = [];

     sequelize.query('SELECT search_organization(\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas_o) {

                contacts.push(req.body.contact_id);

                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    mandrill = require('mandrill-api/mandrill');

                if (req.body.mail != '' && req.body.mail != undefined) {
                    mails = req.body.mail.replace(' ', '').split(',');
                    mails.forEach(function(mail) {
                        if (re.test(mail) != true) {
                            return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
                        }
                        var email = {
                            "email": mail,
                            "name": null,
                            "type": "to"
                        };
                        emails.push(email);
                    });

                }

                 if (req.body.sendMailCc) {
                    mails = req.body.sendMailCc.replace(' ', '').split(',');
                    mails.forEach(function(address) {
                        
                        var mail = {
                            "email": address,
                            "name": null,
                            "type": "bcc"
                        };
                        emails.push(mail);
                    });
                }

                var template_content = [{
                        "name": "MESSAGE",
                        "content": req.body.message
                    }, {
                        "name": "ORGANIZATION",
                        "content": uc_first(req.body.organization_name)
                    }, {
                        "name": "COLOR",
                        "content": req.body.primary_color ? req.body.primary_color : '#00587D'
                    }, {
                        "name": "LOGO",
                        "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                    }, {
                        "name": "NAME",
                        "content": uc_first(req.body.contact_name)
                    }, {
                        "name": "URL",
                        "content": req.body.url
                    }, {
                        "name": "AMOUNT",
                        "content": formatta_euro(req.body.amount)
                    }],
                    message = {
                        "global_merge_vars": template_content,
                        "metadata": {
                            "organization": req.headers['host'].split(".")[0]
                        },
                        "subject": req.body.object,
                        "from_email": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com",
                        "from_name": uc_first(req.body.organization_name),
                        "to": emails,
                        "headers": {
                            "Reply-To": datas_o[0].search_organization.domain_from_mail ? datas_o[0].search_organization.domain_from_mail : "no-reply@beebeemailer.com"
                        },
                        "tags": ["payment-require", req.headers['host'].split(".")[0]],
                        "subaccount": "beebeeboard",
                        "tracking_domain": "mandrillapp.com",
                        "google_analytics_domains": ["poppix.it"]
                    };

                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                mandrill_client.messages.sendTemplate({
                    "template_name": "payment-require",
                    "template_content": template_content,
                    "message": message,
                    "async": true,
                    "send_at": false
                }, function(result) {

                    if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                        console.log("Request payment mail sended correctly");
                        sequelize.query('SELECT insert_mandrill_send(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
                                req.user.id + ',\'' +
                                result[0].status + '\',null,\'' +
                                result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pagamento di euro ' + formatta_euro(req.body.amount) + '\',\'' +
                                req.body.message.replace(/'/g, "''''") + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(datas) {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                return res.status(200).send({});

                            }).catch(function(err) {
                                return res.status(400).render('mandrill send mail: ' + err);
                            });
                    } else {
                        console.log("Mail rejected to " + req.body.mail + " for reason: " + result[0].reject_reason);
                        sequelize.query('SELECT insert_mandrill_send(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                (contacts.length > 0 ? 'ARRAY[' + contacts + ']::bigint[]' : null) + ',' +
                                req.user.id + ',\'' +
                                result[0].status + '\',\'' +
                                result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pagamento di ' + formatta_euro(req.body.amount) + '\'\'' +
                                req.body.message.replace(/'/g, "''''") + '\');', {
                                    raw: true,
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(datas) {
                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                return res.status(422).send({});
                            }).catch(function(err) {
                                return res.status(400).render('mandrill send mail: ' + err);
                            });

                    }


                }, function(e) {
                    // Mandrill returns the error as an object with name and message keys
                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                    return res.status(422).send({});
                });
          }).catch(function(err) {
            return res.status(400).render('search_organization: ' + err);
        });
};

exports.request_payment = function(req, res) {
    return wrap_request_payment(req, res, true);
};

exports.request_payment_base = function(req, res) {
    return wrap_request_payment(req, res, false);
};

function wrap_request_payment(req, res, is_scuolesci = false) {
    var base58 = require('../utility/base58.js');
    var util = require('util');
    var mails = [];
    var emails = [];
    
    if (req.user.role[0].json_build_object.attributes.is_client) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        mandrill = require('mandrill-api/mandrill');

    if (req.body.mail != '' && req.body.mail != undefined) {
        mails = req.body.mail.split(',');
        mails.forEach(function(mail) {

            if (re.test(mail.replaceAll(' ', '')) != true) {
                return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
            } else {
                var email = {
                    "email": mail.replaceAll(' ', ''),
                    "name": null,
                    "type": "to"
                };
                emails.push(email);
            }

        });

    }

     if (req.body.sendMailCc) {
            mails = req.body.sendMailCc.split(',');
            mails.forEach(function(address) {
                
                var mail = {
                    "email": address.replaceAll(' ', ''),
                    "name": null,
                    "type": "bcc"
                };
                emails.push(mail);
            });
        }

    var payment = {};

    payment['owner_id'] = req.user.id;
    payment['date'] = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    payment['status'] = ''
    payment['total'] = req.body.amount;
    payment['payment_method'] = '';
    payment['_tenant_id'] = req.user._tenant_id;
    payment['organization'] = req.headers['host'].split(".")[0];
    //payment['invoice_id'] = ((req.body.data.relationships && req.body.data.relationships.invoice && req.body.data.relationships.invoice.data && req.body.data.relationships.invoice.data.id && !utility.check_id(req.body.data.relationships.invoice.data.id)) ? req.body.data.relationships.invoice.data.id : null);
    //payment['expense_id'] = ((req.body.data.relationships && req.body.data.relationships.expense && req.body.data.relationships.expense.data && req.body.data.relationships.expense.data.id) && !utility.check_id(req.body.data.relationships.expense.data.id) ? req.body.data.relationships.expense.data.id : null);
    payment['error'] = '';
    payment['success'] = false;
    payment['transaction_type'] = '';
    payment['transaction_id'] = '';
    payment['transaction_ref'] = '';
    payment['currency'] = '';
    payment['payment_id'] = '';
    payment['enr_status'] = '';
    payment['auth_status'] = '';
    payment['tran_bank_id'] = '';
    payment['sent'] = false;
    payment['mongo_id'] = null;
    payment['residuo'] = req.body.amount;
    payment['archivied'] = false;

    sequelize.query('SELECT insert_payment_v4(\'' + JSON.stringify(payment) + '\',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (is_scuolesci) {
                sequelize.query('SELECT insert_payment_contact_ecommerce(' + datas[0].insert_payment_v4 + ',' + req.user.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(ins) {});
            }
            sequelize.query('SELECT insert_payment_contact(' + datas[0].insert_payment_v4 + ',' + req.body.contact_id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(ins) {});

            if (req.body.invoice_id) {
                sequelize.query('SELECT insert_invoice_payment(' + req.body.invoice_id + ', ' + datas[0].insert_payment_v4 + ', \'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(ins) {});
            }

            if (req.body.invoices && req.body.invoices.length > 0) {
                for (var i = 0; i < req.body.invoices.length; i++) {
                    sequelize.query('SELECT insert_invoice_payment(' + req.body.invoices[i] + ', ' + datas[0].insert_payment_v4 + ', \'' + req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(ins) {});
                }
            }

            if (req.body.events && req.body.events.length > 0) {
                sequelize.query('SELECT scuolasci_insert_payment_events(' + datas[0].insert_payment_v4 + ',' + (req.body.events.length > 0 ? 'ARRAY[' + req.body.events + ']::bigint[]' : null) + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(ins) {});
            }

            if (is_scuolesci && req.body.projects && req.body.projects.length > 0) {
                sequelize.query('SELECT scuolasci_insert_payment_projects(' + datas[0].insert_payment_v4 + ',' + (req.body.projects.length > 0 ? 'ARRAY[' + req.body.projects + ']::bigint[]' : null) + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(ins) {});
            }

            if (is_scuolesci && req.body.projects_related && req.body.projects_related.length > 0) {
                sequelize.query('SELECT scuolasci_insert_payment_projects(' + datas[0].insert_payment_v4 + ',' + (req.body.projects_related.length > 0 ? 'ARRAY[' + req.body.projects_related + ']::bigint[]' : null) + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(ins) {});
            }


            var token = utils.uid(config.token.accessTokenLength);

            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                moment(new Date()).add(1, 'months').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                req.body.contact_id + ',' +
                5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                }).then(function(tok) {
                /*  Token.create({
                        token : token,
                        expiration_date : config.token.calculateExpirationDate(),
                        user_id : user.id,
                        client_id : client.id,
                        scope : scope
                    }).then(function(tok) {*/
                var longUrl = req.body.url + '&access_token=' + token + '&payment_id=' + datas[0].insert_payment_v4;

                if (req.body.request_by) {
                    longUrl += '&request_by=' + req.body.request_by;

                    sequelize.query('SELECT insert_payment_contact(' + datas[0].insert_payment_v4 + ',' + req.body.request_by + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                        .then(function(ins) {

                        });
                }

                sequelize.query('SELECT insert_shortener(\'' +
                        longUrl + '\',\'' +
                        req.headers['host'].split(".")[0] + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                        })
                    .then(function(shortner) {
                        var shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                        if (req.body.invoice_id) {
                            return res.status(200).send({
                                'url': shortUrl
                            });
                        } else {

                            sequelize.query('SELECT search_organization_reply_to_new(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                            .then(function(reply_mail) {

                                    var reply_to = 'no-reply@beebeemailer.com';
                                    var domain_from_mail = 'no-reply@beebeemailer.com';

                                   if (reply_mail && reply_mail[0] && reply_mail[0].search_organization_reply_to_new) {
                                        reply_mail = JSON.parse(reply_mail[0].search_organization_reply_to_new);
                                        reply_to = reply_mail.default_mail && reply_mail.default_mail != '' ? reply_mail.default_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                        domain_from_mail  = reply_mail.domain_from_mail  && reply_mail.domain_from_mail != '' ? reply_mail.domain_from_mail.replace(/"/g, '') : 'no-reply@beebeemailer.com';
                                        if(domain_from_mail != 'no-reply@beebeemailer.com'){
                                            reply_to = domain_from_mail;
                                        }
                                    }

                                    if(!is_scuolesci){
                                            var template_content = [{
                                                "name": "MESSAGE",
                                                "content": req.body.message
                                            }, {
                                                "name": "ORGANIZATION",
                                                "content": uc_first(req.body.organization_name)
                                            }, {
                                                "name": "COLOR",
                                                "content": req.body.primary_color ? req.body.primary_color : '#00587D'
                                            }, {
                                                "name": "LOGO",
                                                "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                                            }, {
                                                "name": "NAME",
                                                "content": uc_first(req.body.contact_name)
                                            }, {
                                                "name": "URL",
                                                "content": shortUrl
                                            }, {
                                                "name": "AMOUNT",
                                                "content": formatta_euro(req.body.amount)
                                            }],
                                            message = {
                                                "global_merge_vars": template_content,
                                                "merge_language": "handlebars",
                                                "metadata": {
                                                    "organization": req.headers['host'].split(".")[0]
                                                },
                                                "subject": req.body.object,
                                                "from_email": domain_from_mail && domain_from_mail != '' ? domain_from_mail : 'no-reply@beebeemailer.com',
                                                "from_name": uc_first(req.body.organization_name),
                                                "to": emails,
                                                "headers": {
                                                    "Reply-To": reply_to && reply_to != '' ? reply_to : 'no-reply@beebeemailer.com'
                                                },
                                                "tags": ["payment-require", req.headers['host'].split(".")[0]],
                                                "subaccount": "beebeeboard",
                                                "tracking_domain": "mandrillapp.com",
                                                "google_analytics_domains": ["poppix.it"],
                                                "preserve_recipients": false
                                                
                                            };

                                        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                        mandrill_client.messages.sendTemplate({
                                            "template_name": "payment-require",
                                            "template_content": template_content,
                                            "message": message,
                                            "async": true,
                                            "send_at": false
                                        }, function(result) {



                                            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                                console.log("Request payment mail sended correctly");
                                                sequelize.query('SELECT insert_mandrill_send(' +
                                                        req.user._tenant_id + ',\'' +
                                                        req.headers['host'].split(".")[0] + '\',' +
                                                        'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                                        req.user.id + ',\'' +
                                                        result[0].status + '\',null,\'' +
                                                        result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pagamento di euro ' + formatta_euro(req.body.amount) + '\',' +
                                                        (req.body && req.body.message ? '\'' + req.body.message.replace(/'/g, "''''") + '\'' : null) + ');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(datas) {
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                                        return res.status(200).send({});

                                                    }).catch(function(err) {
                                                        return res.status(400).render('mandrill send mail: ' + err);
                                                    });
                                            } else {
                                                console.log("Mail rejected to " + req.body.mail + " for reason: " + result[0].reject_reason);
                                                sequelize.query('SELECT insert_mandrill_send(' +
                                                        req.user._tenant_id + ',\'' +
                                                        req.headers['host'].split(".")[0] + '\',' +
                                                        'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                                        req.user.id + ',\'' +
                                                        result[0].status + '\',\'' +
                                                        result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pagamento di ' + formatta_euro(req.body.amount) + '\',\'' +
                                                        req.body.message.replace(/'/g, "''''") + '\');', {
                                                            raw: true,
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        })
                                                    .then(function(datas) {
                                                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                                        return res.status(422).send({});
                                                    }).catch(function(err) {
                                                        return res.status(400).render('mandrill send mail: ' + err);
                                                    });

                                            }


                                        }, function(e) {
                                            // Mandrill returns the error as an object with name and message keys
                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                            return res.status(422).send({});
                                        });
                                        
                                    }else{
                                        console.log(req.body);
                                         sequelize.query('SELECT scuolasci_get_product_for_request_payment(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            (req.body.contact_id ? req.body.contact_id : null) +','+
                                            (req.body.projects && req.body.projects.length > 0 ? 'ARRAY[' + req.body.projects + ']::bigint[]' : null)+','+
                                            (req.body.events && req.body.events.length > 0 ? 'ARRAY[' + req.body.events + ']::bigint[]' : null)+');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            }).then(function(datas) {

                                                console.log(datas[0].scuolasci_get_product_for_request_payment.products);
                                                console.log(JSON.stringify(datas[0].scuolasci_get_product_for_request_payment.products));

                                                var template_content = [{
                                                        "name": "MESSAGE",
                                                        "content": req.body.message
                                                    }, {
                                                        "name": "ORGANIZATION",
                                                        "content": uc_first(req.body.organization_name)
                                                    }, {
                                                        "name": "COLOR",
                                                        "content": req.body.primary_color ? req.body.primary_color : '#00587D'
                                                    }, {
                                                        "name": "LOGO",
                                                        "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                                                    }, {
                                                        "name": "NAME",
                                                        "content": uc_first(req.body.contact_name)
                                                    }, {
                                                        "name": "URL",
                                                        "content": shortUrl
                                                    }, {
                                                        "name": "AMOUNT",
                                                        "content": formatta_euro(req.body.amount)
                                                    }, {
                                                        "name": "PRODUCTS",
                                                        "content": datas[0].scuolasci_get_product_for_request_payment.products
                                                    }],
                                                    message = {
                                                        "global_merge_vars": template_content,
                                                        "merge_language": "handlebars",
                                                        "metadata": {
                                                            "organization": req.headers['host'].split(".")[0]
                                                        },
                                                        "subject": req.body.object,
                                                        "from_email": domain_from_mail,
                                                        "from_name": uc_first(req.body.organization_name),
                                                        "to": emails,
                                                        "headers": {
                                                            "Reply-To": reply_to
                                                        },
                                                        "tags": ["payment-require", req.headers['host'].split(".")[0]],
                                                        "subaccount": "beebeeboard",
                                                        "tracking_domain": "mandrillapp.com",
                                                        "google_analytics_domains": ["poppix.it"]
                                                    };

                                                var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                                                mandrill_client.messages.sendTemplate({
                                                    "template_name": "payment-require",
                                                    "template_content": template_content,
                                                    "message": message,
                                                    "async": true,
                                                    "send_at": false
                                                }, function(result) {

                                                    
                                                    if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                                                        console.log("Request payment mail sended correctly");
                                                        sequelize.query('SELECT insert_mandrill_send(' +
                                                                req.user._tenant_id + ',\'' +
                                                                req.headers['host'].split(".")[0] + '\',' +
                                                                'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                                                req.user.id + ',\'' +
                                                                result[0].status + '\',null,\'' +
                                                                result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pagamento di euro ' + formatta_euro(req.body.amount) + '\',' +
                                                                (req.body && req.body.message ? '\'' + req.body.message.replace(/'/g, "''''") + '\'' : null) + ');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {
                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                                                return res.status(200).send({});

                                                            }).catch(function(err) {
                                                                console.log('ENTRO IN ERRORE ' + err);
                                                               // return res.status(400).render('mandrill send mail: ' + err);
                                                            });
                                                    } else {
                                                        console.log("Mail rejected to " + req.body.mail + " for reason: " + (result && result[0] && result[0].reject_reason ? result[0].reject_reason : 'SCONOSCIUTO'));
                                                        sequelize.query('SELECT insert_mandrill_send(' +
                                                                req.user._tenant_id + ',\'' +
                                                                req.headers['host'].split(".")[0] + '\',' +
                                                                'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                                                req.user.id + ',\'' +
                                                                result[0].status + '\',\'' +
                                                                (result && result[0] && result[0].reject_reason ? result[0].reject_reason : 'SCONOSCIUTO') + '\',\'' + result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pagamento di ' + formatta_euro(req.body.amount) + '\',\'' +
                                                                req.body.message.replace(/'/g, "''''") + '\');', {
                                                                    raw: true,
                                                                    type: Sequelize.QueryTypes.SELECT,
                                                                    useMaster: true
                                                                })
                                                            .then(function(datas) {
                                                                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                                                return res.status(422).send({});
                                                            }).catch(function(err) {
                                                                return res.status(400).render('mandrill send mail: ' + err);
                                                            });

                                                    }


                                                }, function(e) {
                                                    // Mandrill returns the error as an object with name and message keys
                                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                                                    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                                                    return res.status(422).send({});
                                                });
                                            }).catch(function(err) {

                                                return res.status(400).render('Elenco prodotti richiesta pagamento: ' + err);
                                            });
                                    }
                            }).catch(function(err) {
                                    return res.status(400).render('search_organization_reply_to: ' + err);
                            });     
                        }

                    }).catch(function(err) {
                        return res.status(400).render('tag_insert: ' + err);
                    });

            }).catch(function(err) {
                return res.status(400).render('user_request_payment: ' + err);
            });



        }).catch(function(err) {
            return res.status(400).render('payment_insert: ' + err);
        });
}

exports.triage_request = function(req, res) {
    var base58 = require('../utility/base58.js');
    var mails = [];
    var emails = [];
    var https = require('https');
    var qs = require('querystring');
    var util = require('util');
    var type_contact = req.body.type ? req.body.type : 'mail';
    var event_id = req.body.event_id ? req.body.event_id : null;

    var token = utils.uid(config.token.accessTokenLength);

    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
        moment(new Date()).add(1, 'weeks').format('YYYY/MM/DD HH:mm:ss') + '\',' +
        req.body.contact_id + ',' +
        5 + ',\'\',\'' + req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        }).then(function(tok) {
        /*  Token.create({
                token : token,
                expiration_date : config.token.calculateExpirationDate(),
                user_id : user.id,
                client_id : client.id,
                scope : scope
            }).then(function(tok) {*/
        var longUrl = 'https://asset.beebeeboard.com/triage/triage.php?op_id='+req.user.id+'&token=' + token +'&organization='+req.headers['host'].split(".")[0]+'&contact_id='+req.body.contact_id+'&event_id='+event_id;

       

        sequelize.query('SELECT insert_shortener(\'' +
                longUrl + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(shortner) {
                shortUrl = 'https://' + req.headers['host'].split(".")[0] + '.beebeeboard.com/bee/' + base58.encode(shortner[0].insert_shortener);

                if (type_contact == 'sms') {
                    var mobile = null;
                    var mobiles = [];
                    var mob_text = [];
                    var lrecipients = [];

                    if (req.body.contact_id !== undefined && req.body.contact_id != '') {
                        if (util.isArray(req.body.contact_id)) {
                            lrecipients.push(req.body.contact_id);
                        } else {
                            lrecipients.push(req.body.contact_id);
                        }
                    }

                    if (req.body.mobile) {
                        if (req.body.mobile.indexOf('+') == -1) {
                            mobile = '+39' + req.body.mobile;
                        } else {
                            mobile = req.body.mobile.replace('', '');
                        }

                    }

                    mobiles.push(mobile.replace(' ', ''));
                    mob_text.push('\''+mobile.replace(' ', '')+'\'');

                    var testo_sms = 'Gentile ' + req.body.contact_name + ' ti chiediamo di compilare il pre-triage al seguente link: ' + shortUrl+'. Grazie da '+req.body.organization_name;


                    utility.login_skebby(function(err, auth) {
                        if (!err) {
                            var smsData = {
                                "returnCredits": true,
                                "recipient": mobiles,
                                "message": testo_sms,
                                "message_type": "GP",
                                "sender" : 'Beebeeboard'
                            };

                            utility.sendSMS(auth, smsData,
                                function(error, data) {
                                    if (error) {
                                       sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                            req.user.id + ',\'' +
                                            (error ? error.replace(/'/g, "''''") : '') + '\',\'' +
                                            (error ? error.replace(/'/g, "''''") : '') + '\',\'' + 'Richiesta Triage' + '\',1,'
                                            (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                             (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',null,null,'+
                                            (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {
                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');
                                            res.status(200).send({});

                                        }).catch(function(err) {
                                            return res.status(400).render('skebby send sms: ' + err);
                                        });
                                     }
                                    else {
                                        console.log('SMS: send correctly ' + JSON.stringify(data));

                                        sequelize.query('SELECT insert_skebby_send_new_v1(' +
                                            req.user._tenant_id + ',\'' +
                                            req.headers['host'].split(".")[0] + '\',' +
                                            'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                            req.user.id + ',\'' +
                                            data.result + '\',\'\',\'' + 'Richiesta Triage' + '\',1,'
                                            (data  && data.order_id ? '\''+data.order_id +'\'' : null)+' ,'+
                                             (data &&  data.internal_order_id ? '\''+data.internal_order_id +'\'' : null)+',null,null,'+
                                            (mob_text.length > 0 ? 'ARRAY[' + mob_text + ']::text[]' : null)+');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                                                useMaster: true
                                            })
                                        .then(function(datas) {

                                            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Skebby sms sended saved');

                                            res.status(200).send({});


                                        }).catch(function(err) {
                                            return res.status(400).render('skebby send sms: ' + err);
                                        });
                                    }
                            });
                        }
                        else {
                          return res.status(400).render('skebby send sms: ' + err);
                        }
                    });

                } else {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        mandrill = require('mandrill-api/mandrill');

                    if (req.body.mail != '' && req.body.mail != undefined && type_contact == 'mail') {
                        mails = req.body.mail.replace(' ', '').split(',');
                        mails.forEach(function(mail) {
                            if (re.test(mail) != true) {
                                return res.status(422).send(utility.error422('mail', 'Parametro non valido', 'Inserire una mail valida', '422'));
                            } else {
                                var email = {
                                    "email": mail,
                                    "name": null,
                                    "type": "to"
                                };
                                emails.push(email);
                            }

                        });

                    }

                    var message = 'ti chiediamo di compilare il seguente questionario pre-triage per garantire al meglio la sicurezza nostra e dei nostri pazienti.<br/> Grazie da '+uc_first(req.body.organization_name);

                    var template_content = [{
                            "name": "MESSAGE",
                            "content": message
                        }, {
                            "name": "ORGANIZATION",
                            "content": uc_first(req.body.organization_name)
                        }, {
                            "name": "COLOR",
                            "content": req.body.primary_color ? req.body.primary_color : '#00587D'
                        }, {
                            "name": "LOGO",
                            "content": req.body.thumbnail_url ? 'https://data.beebeeboard.com/' + req.body.thumbnail_url + '-small.jpeg' : null
                        }, {
                            "name": "NAME",
                            "content": uc_first(req.body.contact_name)
                        }, {
                            "name": "URL",
                            "content": shortUrl
                        }],
                        message = {
                            "global_merge_vars": template_content,
                            "metadata": {
                                "organization": req.headers['host'].split(".")[0]
                            },
                            "subject": req.body.object,
                            "from_email": "no-reply@beebeemailer.com",
                            "from_name": uc_first(req.body.organization_name),
                            "to": emails,
                            "headers": {
                                "Reply-To": "no-reply@beebeemailer.com"
                            },
                            "tags": ["triage-require", req.headers['host'].split(".")[0]],
                            "subaccount": "beebeeboard",
                            "tracking_domain": "mandrillapp.com",
                            "google_analytics_domains": ["poppix.it"]
                        };

                    var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

                    mandrill_client.messages.sendTemplate({
                        "template_name": "triage-require",
                        "template_content": template_content,
                        "message": message,
                        "async": true,
                        "send_at": false
                    }, function(result) {

                        if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {

                            console.log("Request triage mail sended correctly");
                            sequelize.query('SELECT insert_mandrill_send_triage(' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                    req.user.id + ',\'' +
                                    result[0].status + '\',null,\'' +
                                    result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pre-triage\',\'' +
                                    'Richiesta Triage' + '\','+event_id+');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                    return res.status(200).send({});

                                }).catch(function(err) {
                                    return res.status(400).render('mandrill send mail: ' + err);
                                });
                        } else if(result) {
                            console.log("Mail rejected to " + req.body.mail );
                            sequelize.query('SELECT insert_mandrill_send_triage(' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    'ARRAY[' + req.body.contact_id + ']::bigint[]' + ',' +
                                    req.user.id + ',\'' +
                                    result[0].status + '\',\'' +
                                    result[0].reject_reason + '\',\'' + result[0]._id + '\',\'' + uc_first(req.headers['host'].split(".")[0]) + ' - Richiesta pre-triage \',\'' +
                                    'Richiesta Triage' + '\','+event_id+');', {
                                        raw: true,
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(datas) {
                                    console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mandrill mail sended saved');
                                    return res.status(422).send({});
                                }).catch(function(err) {
                                    return res.status(400).render('mandrill send mail: ' + err);
                                });

                        }


                    }, function(e) {
                        // Mandrill returns the error as an object with name and message keys
                        console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);
                        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                        return res.status(422).send({});
                    });
                }

            }).catch(function(err) {
                return res.status(400).render('tag_insert: ' + err);
            });

    }).catch(function(err) {
        return res.status(400).render('user_request_payment: ' + err);
    });
}

function decode_text_mail(reminder, model, datas) {
    var str = '';
    switch (model) {
        case 'event':
            str = reminder.mail_event;

            if (str.indexOf('{{event.title}}') > -1) {
                str = str.replace('{{event.title}}', datas.title);
            }

            if (str.indexOf('{{event.start_date}}') > -1) {
                str = str.replace('{{event.start_date}}', moment(datas.start_date).tz('Europe/Rome').format('DD/MM/YYYY HH:mm'));
            }

            if (str.indexOf('{{event.start_day}}') > -1) {
                str = str.replace('{{event.start_day}}', moment(datas.start_date).tz('Europe/Rome').format('DD/MM/YYYY'));
            }

            if (str.indexOf('{{event.start_hour}}') > -1) {
                str = str.replace('{{event.start_hour}}', moment(datas.start_date).tz('Europe/Rome').format('HH:mm'));
            }

            if (str.indexOf('{{event.end_date}}') > -1) {
                str = str.replace('{{event.end_date}}', moment(datas.end_date).tz('Europe/Rome').format('DD/MM/YYYY HH:mm'));
            }

            if (str.indexOf('{{event.end_day}}') > -1) {
                str = str.replace('{{event.end_day}}', moment(datas.end_date).tz('Europe/Rome').format('DD/MM/YYYY'));
            }

            if (str.indexOf('{{event.end_hour}}') > -1) {
                str = str.replace('{{event.end_hour}}', moment(datas.end_date).tz('Europe/Rome').format('HH:mm'));
            }

            if (str.indexOf('{{event.description}}') > -1) {
                str = str.replace('{{event.description}}', datas.description);
            }

            if (str.indexOf('{{event.players_number}}') > -1) {
                str = str.replace('{{event.players_number}}', datas.players_number);
            }

            if (str.indexOf('{{event.price}}') > -1) {
                str = str.replace('{{event.price}}', formatta_euro(datas.price));
            }

            if (str.indexOf('{{event.to_name}}') > -1) {
                str = str.replace('{{event.to_name}}', reminder.to_name);
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{event.product_name}}') > -1) {
                str = str.replace('{{event.product_name}}', (datas && datas.json_build_object && datas.json_build_object.products && datas.json_build_object.products[0] && datas.json_build_object.products[0].name ? datas.json_build_object.products[0].name : ''));
            }

            if (str.indexOf('{{event.specialist_name}}') > -1) {
                str = str.replace('{{event.specialist_name}}', (datas && datas.json_build_object && datas.json_build_object.specialista && datas.json_build_object.specialista[0] && datas.json_build_object.specialista[0].name ? datas.json_build_object.specialista[0].name : ''));
            }

            if (str.indexOf('{{event.specialist_notes}}') > -1) {
                str = str.replace('{{event.specialist_notes}}', (datas && datas.json_build_object && datas.json_build_object.specialista && datas.json_build_object.specialista[0] && datas.json_build_object.specialista[0].notes ? datas.json_build_object.specialista[0].notes : ''));
            }

            if (str.indexOf('{{event.paziente_name}}') > -1) {
                str = str.replace('{{event.paziente_name}}', (datas && datas.json_build_object && datas.json_build_object.paziente && datas.json_build_object.paziente[0] && datas.json_build_object.paziente[0].name ? datas.json_build_object.paziente[0].name : ''));
            }

             if (str.indexOf('{{event.paziente_nome}}') > -1) {
                str = str.replace('{{event.paziente_nome}}', (datas && datas.json_build_object && datas.json_build_object.paziente && datas.json_build_object.paziente[0] && datas.json_build_object.paziente[0].first_name ? datas.json_build_object.paziente[0].first_name : ''));
            }

             if (str.indexOf('{{event.paziente_cognome}}') > -1) {
                str = str.replace('{{event.paziente_cognome}}', (datas && datas.json_build_object && datas.json_build_object.paziente && datas.json_build_object.paziente[0] && datas.json_build_object.paziente[0].last_name ? datas.json_build_object.paziente[0].last_name : ''));
            }

            if (str.indexOf('{{event.stanza}}') > -1) {
                str = str.replace('{{event.stanza}}', (datas && datas.json_build_object && datas.json_build_object.stanza && datas.json_build_object.stanza[0] && datas.json_build_object.stanza[0].name ? datas.json_build_object.stanza[0].name : ''));
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{client.name}}') > -1) {
                str = str.replace('{{client.name}}', reminder.scuola_fields.name);
            }

            if (str.indexOf('{{client.mobile}}') > -1) {
                str = str.replace('{{client.mobile}}', reminder.scuola_fields.mobile);
            }

            if (str.indexOf('{{partecipanti_level}}') > -1) {
                str = str.replace('{{partecipanti_level}}', reminder.scuola_fields.levels);
            }

            if (str.indexOf('{{comprensorio}}') > -1) {
                str = str.replace('{{comprensorio}}', reminder.scuola_fields.comprensorio);
            }

            if (str.indexOf('{{punto_ritrovo}}') > -1) {
                str = str.replace('{{punto_ritrovo}}', reminder.scuola_fields.punto_ritrovo);
            }

            if (str.indexOf('{{product.name}}') > -1) {
                str = str.replace('{{product.name}}', reminder.scuola_fields.product_name);
            }

            if (str.indexOf('{{lingua}}') > -1) {
                str = str.replace('{{lingua}}', reminder.scuola_fields.lingua);
            }

            if (str.indexOf('{{operatore.name}}') > -1) {
                str = str.replace('{{operatore.name}}', reminder.scuola_fields.operatore_name);
            }
            
            return str;
            break;

        case 'todo':
            str = reminder.mail_todo;

            if (str.indexOf('{{todo.date}}') > -1) {
                str = str.replace('{{todo.date}}', moment(datas.date).tz('Europe/Rome').format('DD-MM-YYYY HH:mm'));
            }

            if (str.indexOf('{{todo.text}}') > -1) {
                str = str.replace('{{todo.text}}', datas.text);
            }

            if (str.indexOf('{{todo.todolist}}') > -1) {
                str = str.replace('{{todo.todolist}}', datas.todolist_title);
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{todo.to_name}}') > -1) {
                str = str.replace('{{todo.to_name}}', reminder.to_name);
            }

            return str;
            break;

        case 'invoice':
            str = reminder.mail_invoice;

            if (str.indexOf('{{document_type}}') > -1) {
                var document_type = (datas.estimate ? 'Preventivo' : (datas.total < 0 ? 'Nota di Credito' : ((datas.number && !datas.estimate) ? 'Fattura' : 'Avviso di Fattura')));
                str = str.replace('{{document_type}}', document_type);
            }

            if (str.indexOf('{{invoice.number}}') > -1) {
                str = str.replace('{{invoice.number}}', (datas.number ? datas.number : '#'));
            }

            if (str.indexOf('{{invoice.date}}') > -1) {
                str = str.replace('{{invoice.date}}', moment(datas.date).format('DD-MM-YYYY HH:mm'));
            }

            if (str.indexOf('{{invoice.to}}') > -1) {
                str = str.replace('{{invoice.to}}', datas.contact_name);
            }

            if (str.indexOf('{{invoice.imponibile}}') > -1) {
                str = str.replace('{{invoice.imponibile}}', datas.imponibile);
            }

            if (str.indexOf('{{invoice.total}}') > -1) {
                str = str.replace('{{invoice.total}}', datas.total);
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{invoice.to_name}}') > -1) {
                str = str.replace('{{invoice.to_name}}', reminder.to_name);
            }

            return str;
            break;

        case 'expense':

            str = reminder.mail_expense;

            if (str.indexOf('{{expense.number}}') > -1) {
                str = str.replace('{{expense.number}}', (datas.number ? datas.number : '#'));
            }

            if (str.indexOf('{{expense.date}}') > -1) {
                str = str.replace('{{expense.date}}', moment(datas.date).format('DD-MM-YYYY HH:mm'));
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{expense.total}}') > -1) {
                str = str.replace('{{expense.total}}', formatta_euro(datas.total));
            }

            if (str.indexOf('{{expense.to_name}}') > -1) {
                str = str.replace('{{expense.to_name}}', reminder.to_name);
            }



            return str;
            break;

        default:
            return '';
            break;
    }
};

function decode_text_sms(reminder, model, datas) {
   var str = '';
    switch (model) {
        case 'event':
            str = reminder.sms_event;

            if (str.indexOf('{{event.title}}') > -1) {
                str = str.replace('{{event.title}}', datas.title);
            }

            if (str.indexOf('{{event.start_date}}') > -1) {
                str = str.replace('{{event.start_date}}', moment(datas.start_date).tz('Europe/Rome').format('DD/MM/YYYY HH:mm'));
            }

            if (str.indexOf('{{event.start_day}}') > -1) {
                str = str.replace('{{event.start_day}}', moment(datas.start_date).tz('Europe/Rome').format('DD/MM/YYYY'));
            }

            if (str.indexOf('{{event.start_hour}}') > -1) {
                str = str.replace('{{event.start_hour}}', moment(datas.start_date).tz('Europe/Rome').format('HH:mm'));
            }

            if (str.indexOf('{{event.end_date}}') > -1) {
                str = str.replace('{{event.end_date}}', moment(datas.end_date).tz('Europe/Rome').format('DD/MM/YYYY HH:mm'));
            }

            if (str.indexOf('{{event.end_day}}') > -1) {
                str = str.replace('{{event.end_day}}', moment(datas.end_date).tz('Europe/Rome').format('DD/MM/YYYY'));
            }

            if (str.indexOf('{{event.end_hour}}') > -1) {
                str = str.replace('{{event.end_hour}}', moment(datas.end_date).tz('Europe/Rome').format('HH:mm'));
            }

            if (str.indexOf('{{event.description}}') > -1) {
                str = str.replace('{{event.description}}', datas.description);
            }

            if (str.indexOf('{{event.players_number}}') > -1) {
                str = str.replace('{{event.players_number}}', datas.players_number);
            }

            if (str.indexOf('{{event.price}}') > -1) {
                str = str.replace('{{event.price}}', formatta_euro(datas.price));
            }

            if (str.indexOf('{{event.to_name}}') > -1) {
                str = str.replace('{{event.to_name}}', reminder.to_name);
            }

            if (str.indexOf('{{event.product_name}}') > -1) {
                str = str.replace('{{event.product_name}}', (datas && datas.json_build_object && datas.json_build_object.products && datas.json_build_object.products[0] && datas.json_build_object.products[0].name ? datas.json_build_object.products[0].name : ''));
            }

            if (str.indexOf('{{event.specialist_name}}') > -1) {
                str = str.replace('{{event.specialist_name}}', (datas && datas.json_build_object && datas.json_build_object.specialista && datas.json_build_object.specialista[0] && datas.json_build_object.specialista[0].name ? datas.json_build_object.specialista[0].name : ''));
            }

            if (str.indexOf('{{event.specialist_notes}}') > -1) {
                str = str.replace('{{event.specialist_notes}}', (datas && datas.json_build_object && datas.json_build_object.specialista && datas.json_build_object.specialista[0] && datas.json_build_object.specialista[0].notes ? datas.json_build_object.specialista[0].notes : ''));
            }

            if (str.indexOf('{{event.paziente_name}}') > -1) {
                str = str.replace('{{event.paziente_name}}', (datas && datas.json_build_object && datas.json_build_object.paziente && datas.json_build_object.paziente[0] && datas.json_build_object.paziente[0].name ? datas.json_build_object.paziente[0].name : ''));
            }

               if (str.indexOf('{{event.paziente_nome}}') > -1) {
                str = str.replace('{{event.paziente_nome}}', (datas && datas.json_build_object && datas.json_build_object.paziente && datas.json_build_object.paziente[0] && datas.json_build_object.paziente[0].first_name ? datas.json_build_object.paziente[0].first_name : ''));
            }

             if (str.indexOf('{{event.paziente_cognome}}') > -1) {
                str = str.replace('{{event.paziente_cognome}}', (datas && datas.json_build_object && datas.json_build_object.paziente && datas.json_build_object.paziente[0] && datas.json_build_object.paziente[0].last_name ? datas.json_build_object.paziente[0].last_name : ''));
            }

            if (str.indexOf('{{event.stanza}}') > -1) {
                str = str.replace('{{event.stanza}}', (datas && datas.json_build_object && datas.json_build_object.stanza && datas.json_build_object.stanza[0] && datas.json_build_object.stanza[0].name ? datas.json_build_object.stanza[0].name : ''));
            }
            
            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{client.name}}') > -1) {
                str = str.replace('{{client.name}}', reminder.scuola_fields.name);
            }

            if (str.indexOf('{{client.mobile}}') > -1) {
                str = str.replace('{{client.mobile}}', reminder.scuola_fields.mobile);
            }

            if (str.indexOf('{{partecipanti_level}}') > -1) {
                str = str.replace('{{partecipanti_level}}', reminder.scuola_fields.levels);
            }

            if (str.indexOf('{{comprensorio}}') > -1) {
                str = str.replace('{{comprensorio}}', reminder.scuola_fields.comprensorio);
            }

            if (str.indexOf('{{punto_ritrovo}}') > -1) {
                str = str.replace('{{punto_ritrovo}}', reminder.scuola_fields.punto_ritrovo);
            }

            if (str.indexOf('{{product.name}}') > -1) {
                str = str.replace('{{product.name}}', reminder.scuola_fields.product_name);
            }

            if (str.indexOf('{{lingua}}') > -1) {
                str = str.replace('{{lingua}}', reminder.scuola_fields.lingua);
            }

            if (str.indexOf('{{operatore.name}}') > -1) {
                str = str.replace('{{operatore.name}}', reminder.scuola_fields.operatore_name);
            }

            return str;

   
            
            break;

        case 'todo':
            str = reminder.sms_todo;

            if (str.indexOf('{{todo.date}}') > -1) {
                str = str.replace('{{todo.date}}', moment(datas.date).tz('Europe/Rome').format('DD/MM/YYYY HH:mm'));
            }

            if (str.indexOf('{{todo.text}}') > -1) {
                str = str.replace('{{todo.text}}', datas.text);
            }

            if (str.indexOf('{{todo.todolist}}') > -1) {
                str = str.replace('{{todo.todolist}}', datas.todolist_title);
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{todo.to_name}}') > -1) {
                str = str.replace('{{todo.to_name}}', reminder.to_name);
            }

            return str;
            break;

        case 'invoice':
            str = reminder.sms_invoice;

            if (str.indexOf('{{document_type}}') > -1) {
                var document_type = (datas.estimate ? 'Preventivo' : (datas.total < 0 ? 'Nota di Credito' : ((datas.number && !datas.estimate) ? 'Fattura' : 'Avviso di Fattura')));
                str = str.replace('{{document_type}}', document_type);
            }

            if (str.indexOf('{{invoice.to_name}}') > -1) {
                str = str.replace('{{invoice.to_name}}', reminder.to_name);
            }

            if (str.indexOf('{{invoice.number}}') > -1) {
                str = str.replace('{{invoice.number}}', (datas.number ? datas.number : ' - '));
            }

            if (str.indexOf('{{invoice.date}}') > -1) {
                str = str.replace('{{invoice.date}}', moment(datas.date).format('DD/MM/YYYY'));
            }

            if (str.indexOf('{{invoice.to}}') > -1) {
                str = str.replace('{{invoice.to}}', datas.contact_name);
            }

            if (str.indexOf('{{invoice.imponibile}}') > -1) {
                str = str.replace('{{invoice.imponibile}}', formatta_euro(datas.imponibile));
            }

            if (str.indexOf('{{invoice.total}}') > -1) {
                str = str.replace('{{invoice.total}}', formatta_euro(datas.total));
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            return str;
            break;

        case 'expense':

            str = reminder.sms_expense;

            if (str.indexOf('{{expense.number}}') > -1) {
                str = str.replace('{{expense.number}}', (datas.number ? datas.number : ' - '));
            }

            if (str.indexOf('{{expense.to_name}}') > -1) {
                str = str.replace('{{expense.to_name}}', reminder.to_name);
            }

            if (str.indexOf('{{expense.date}}') > -1) {
                str = str.replace('{{expense.date}}', moment(datas.date).tz('Europe/Rome').format('DD/MM/YYYY HH:mm'));
            }

            if (str.indexOf('{{organization.name}}') > -1) {
                str = str.replace('{{organization.name}}', reminder.organization_name);
            }

            if (str.indexOf('{{expense.total}}') > -1) {
                str = str.replace('{{expense.total}}', formatta_euro(datas.total));
            }

            return str;
            break;

        default:
            return '';
            break;
    }
};



exports.revoke_google = function(req, res) {
    var request1 = require('request');
    var scope = req.query.scope;
    var async = require('async');
    var organization = req.headers['host'].split(".")[0];
    var contact_id = req.user.id;

    if (scope == 'calendar') {

        async.parallel({
            token: function(callback) {
                
                sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                    .then(function(datas) {


                        if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                            var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                            if(tok.refresh_token) {
                                var request = require('request');
                                request.post('https://accounts.google.com/o/oauth2/token', {
                                    form: {
                                        grant_type: 'refresh_token',
                                        refresh_token: tok.refresh_token,
                                        client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                        client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                    }
                                }, function(err, res, body) {
                                    body = JSON.parse(body);
                                    sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                        '\', \'' + body.access_token + '\',' + contact_id + ');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        }).then(function(datas) {
                                            callback(null, body.access_token);

                                    });
                                });
                            }
                            else {

                                callback(null, null);
                            }
                        } else {

                            callback(null, null);
                        }
                    });     
            },
            old_calendar: function(callback){
                sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + organization + '\', '+ contact_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                    .then(function(datas) {

                        if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                            var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);
                            
                                callback(null, tok);
                           
                        } else {
                            callback(null, null);
                        }
                    });
            },
            org: function(callback) {
                callback(null, organization);
            },
            contact:  function(callback) {
                callback(null, contact_id);
            }
        }, function(err, results) {

            console.log(results);

            if (results.token) {


                sequelize.query('SELECT get_google_calendar_info_no_calendar(\'' + req.headers['host'].split(".")[0] + '\',' + req.user.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    })
                    .then(function(datas) {

                        if (datas && datas[0] && datas[0].get_google_calendar_info_no_calendar) {
                            var tok = JSON.parse(datas[0].get_google_calendar_info_no_calendar);

                            var headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + tok.access_token
                            }

                            var options = {
                                url: 'https://www.googleapis.com/calendar/v3/channels/stop',
                                method: 'POST',
                                headers: headers,
                                json: JSON.parse('{"id": "gc-beebeeboard-notification-'+req.headers['host'].split(".")[0]+'","resourceId":"'+tok.resource+'"}')
                            }
                            // Start the request
                             
                            request1(options, function(error1, response1, body1) {
                                console.log(options);
                                console.log('367');
                                console.log(body1);
                                if (error1) {
                                    console.log(error1);
                                }
                            });

                            if (tok.refresh_token) {
                                var request = require('request');
                                request.post('https://accounts.google.com/o/oauth2/revoke?token='+tok.refresh_token, function(err, res, body) {

                                    console.log(body);
                                });
                            } else if (tok.access_token) {
                                var request = require('request');
                                request.post('https://accounts.google.com/o/oauth2/revoke?token='+tok.access_token, function(err, res, body) {
                                    console.log(body);
                                });
                            }

                            sequelize.query('SELECT delete_google_token(\'' + req.headers['host'].split(".")[0] +
                                '\', ' + req.user.id + ');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(err, datas) {
                                if (err) {
                                    console.log(err);
                                }

                                res.status(200).json({});

                            }).catch(function(err) {
                                console.log(err);
                            });
                        } else {
                            res.status(200).json({});
                        }
                    }).catch(function(err) {
                        console.log(err);
                    });
            }else
            {
                res.status(200).json({});
            }
        });
    } else if (scope == 'login') {
        sequelize.query('SELECT delete_google_account(\'' + req.headers['host'].split(".")[0] + '\',' + req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {

                res.status(200).json({});
            }).catch(function(err) {
                console.log(err);

            });
    }
};

exports.google_calendar_webhooks = function(req, res) {
    var async = require('async');
    console.log(req.headers);

    var organization = req.headers['x-goog-channel-id'];
    var uri = req.headers['x-goog-resource-uri'];
    var request = require('request');

    organization = organization.replace('gc-beebeeboard-notification-', '');
    uri = uri.replace('https://www.googleapis.com/calendar/v3/calendars/', '');

    uri = uri.split('\/events')[0];

    async.parallel({
        token: function(callback) {
            sequelize.query('SELECT get_google_calendar_info(\'' + organization + '\',\'' + uri + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
                .then(function(datas) {


                    if (datas && datas[0] && datas[0].get_google_calendar_info) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info);
                        if (tok.access_token) {
                            callback(null, tok.access_token);
                        } else if (tok.refresh_token) {

                            request.post('https://accounts.google.com/o/oauth2/token', {
                                form: {
                                    grant_type: 'refresh_token',
                                    refresh_token: tok.refresh_token,
                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                }
                            }, function(err, res, body) {
                                body = JSON.parse(body);
                                sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                    '\', \'' + body.access_token + '\',' + tok.contact_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(datas) {
                                    callback(null, body.access_token);

                                });
                            });
                        }
                    } else {

                        callback(null, null);
                    }
                });
        },
        google_url: function(callback) {
            callback(null, uri);
        },
        org: function(callback) {
            callback(null, organization)
        },
        sync_token: function(callback) {
            console.log('SYNC');
            sequelize.query('SELECT get_google_calendar_info(\'' + organization + '\',\'' + uri + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
                .then(function(datas) {
                    if (datas && datas[0] && datas[0].get_google_calendar_info) {
                        var tok = JSON.parse(datas[0].get_google_calendar_info);

                        if (tok.access_token) {
                            var headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + tok.access_token
                            }

                            var options = {
                                url: 'https://www.googleapis.com/calendar/v3/calendars/' + uri + '/events?maxResults=10000&singleEvents=true&syncToken=' + tok.sync_token + '&alt=json',
                                method: 'GET',
                                headers: headers
                            }

                            // Start the request
                            request(options, function(error, response, body) {
                                if (error) {
                                    console.log('error' + error)
                                } else {
                                    body = JSON.parse(body);
                                    console.log(body);
                                    if (body.error) {

                                        var headers = {
                                            'Content-Type': 'application/json',
                                            'Authorization': 'Bearer ' + tok.access_token
                                        }

                                        var options = {
                                            url: 'https://www.googleapis.com/calendar/v3/calendars/' + uri + '/events?maxResults=100000&alt=json',
                                            method: 'GET',
                                            headers: headers
                                        }

                                        request(options, function(error1, response1, body1) {
                                            if (error1) {
                                                console.log('error' + error1)
                                            } else {
                                                body1 = JSON.parse(body1);
                                                console.log(body1);
                                                if (body1.nextSyncToken) {
                                                    sequelize.query('SELECT update_google_calendar_sync_token(\'' + organization +
                                                        '\', \'' + body1.nextSyncToken + '\',' + tok.contact_id + ');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(datas) {
                                                        callback(null, null);

                                                    });
                                                } else {
                                                    callback(null, null);
                                                }

                                            }
                                        });


                                    } else {
                                        callback(null, null);
                                    }
                                }
                            });
                        } else if (tok.refresh_token) {

                            request.post('https://accounts.google.com/o/oauth2/token', {
                                form: {
                                    grant_type: 'refresh_token',
                                    refresh_token: tok.refresh_token,
                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                }
                            }, function(err, res, body) {
                                body = JSON.parse(body);
                                sequelize.query('SELECT update_google_calendar_token(\'' + organization +
                                    '\', \'' + body.access_token + '\',' + tok.contact_id + ');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    }).then(function(datas) {
                                    var headers = {
                                        'Content-Type': 'application/json',
                                        'Authorization': 'Bearer ' + body.access_token
                                    }

                                    var options = {
                                        url: 'https://www.googleapis.com/calendar/v3/calendars/' + uri + '/events?maxResults=10000&singleEvents=true&syncToken=' + tok.sync_token + '&alt=json',
                                        method: 'GET',
                                        headers: headers
                                    }

                                    // Start the request
                                    request(options, function(error, response, body) {
                                        if (error) {
                                            console.log('error' + error)
                                        }

                                        body = JSON.parse(body);

                                        if (body.error) {
                                            var options = {
                                                url: 'https://www.googleapis.com/calendar/v3/calendars/' + uri + '/events?maxResults=10000&alt=json',
                                                method: 'GET',
                                                headers: headers
                                            }

                                            request(options, function(error1, response1, body1) {
                                                if (error1) {
                                                    console.log('error' + error1)
                                                } else {
                                                    body1 = JSON.parse(body1);
                                                    sequelize.query('SELECT update_google_calendar_sync_token(\'' + organization +
                                                        '\', \'' + body1.nextSyncToken + '\',' + tok.contact_id + ');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(datas) {
                                                        callback(null, null);

                                                    });
                                                }
                                            });


                                        } else {
                                            callback(null, null);
                                        }


                                    });


                                });
                            });
                        }

                    } else {

                        callback(null, null);
                    }
                });
        }
    }, function(err, results) {

        if (results.token) {
            sequelize.query('SELECT get_google_calendar_info(\'' + results.org + '\',\'' + results.google_url + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
                .then(function(datas) {
                    if (datas && datas[0] && datas[0].get_google_calendar_info) {
                        datas = JSON.parse(datas[0].get_google_calendar_info);

                        var headers = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + results.token
                        }

                        var options = {
                            url: 'https://www.googleapis.com/calendar/v3/calendars/' + results.google_url + '/events?showDeleted=true&maxResults=10000&singleEvents=true&syncToken=' + datas.sync_token + '&alt=json',
                            method: 'GET',
                            headers: headers
                        }


                        // Start the request
                        request(options, function(error, response, body) {
                            if (error) {
                                console.log('error' + error)
                            }

                            body = JSON.parse(body);

                            console.log(body);

                            if (body.items && body.items.length > 0) {
                                body.items.forEach(function(ev) {
                                    var start_date = '';
                                    var end_date = '';
                                    var title = '';
                                    if (ev.status != 'cancelled') {
                                        start_date = ev.start.dateTime ? ev.start.dateTime : (ev.start.date ? ev.start.date : null);
                                        end_date = ev.end.dateTime ? ev.end.dateTime : (ev.end.date ? ev.end.date : null);
                                        title = ev.summary ? ev.summary.replace('null','') : '';
                                    }

                                    console.log(ev);
                                    sequelize.query('SELECT google_calendar_push_action(\'' + results.org + '\',\'' + ev.id + '\',' + datas.contact_id + ',\'' + results.google_url + '\',\'' + title.replace(/'/g, "''''") + '\',\'' + start_date + '\',\'' + end_date + '\',\'' + ev.status + '\',\'' + (body.nextSyncToken ? body.nextSyncToken : null) + '\');', {
                                            raw: true,
                                            type: Sequelize.QueryTypes.SELECT,
                                            useMaster: true
                                        })
                                        .then(function(evento) {

                                        });

                                });
                            }

                            res.status(200).json({});

                            // CICLO SUGLI EVENTI CHE MI PASSA GOOGLE
                            /*sequelize.query('SELECT insert_event_google(\'' + results.org +
                                '\','+datas.contact_id+',\''+body.id+'\',\''+results.google_url+'\','+
                                event.attributes.id+',true);', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                }).then(function(datas) {
                                     
                                });*/
                        });
                    }
                });

        }

    });
};

exports.status = function(req, res) {
    sequelize.query('SELECT beebee_status();', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
        .then(function(datas) {
            return res.status(200).json(datas[0].beebee_status[0].array_agg);
        }).catch(function(err) {
            return res.status(400).render('status error: ' + err);
        });
};

exports.get_pos_method = function(req, res) {
    sequelize.query('SELECT get_pos_method(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (req.query.user_id ? req.query.user_id : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            return res.status(200).json(datas[0].get_pos_method);
        }).catch(function(err) {
            return res.status(400).render('status error: ' + err);
        });
};

exports.fresh_contact = function(req, res) {
    var contact_id = null,
        fresh_restore_id = null,
        organization = null;

    if (req.body.contact_id !== null && req.body.contact_id !== undefined) {
        contact_id = req.body.contact_id;
    } else {
        return res.status(422).send(utility.error422('contact_id', 'Invalid attribute', 'Il campo contact_id non può essere vuoto', '422'));
    }

    if (req.body.fresh_restore_id !== null && req.body.fresh_restore_id !== undefined) {
        fresh_restore_id = '\'' + req.body.fresh_restore_id + '\'';
    } else {
        return res.status(422).send(utility.error422('fresh_restore_id', 'Invalid attribute', 'Il campo fresh_restore_id non può essere vuoto', '422'));
    }

    if (req.body.organization !== null && req.body.organization !== undefined) {
        organization = '\'' + req.body.organization + '\'';
    } else {
        organization = '\'' + req.headers['host'].split(".")[0] + '\'';
    }

    sequelize.query('SELECT update_fresh_restore(' +
            organization + ',' +
            contact_id + ',' +
            fresh_restore_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            return res.status(200).json(datas[0].update_fresh_restore);
        }).catch(function(err) {
            return res.status(400).render('status error: ' + err);
        });
};

exports.domain_mail = function(req, res){

     sequelize.query('SELECT update_organization_option_field(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify({'data':{'domain_from_mail':req.headers['host'].split(".")[0]+'@beebeemailer.com'},'fields':['domain_from_mail']}).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            res.status(200).send({});
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send({});
        });

  
}

exports.test_route = function(req, res){

     var expiration = moment().add(1, 'days').valueOf();
   

    
    var request = require('request');
    req.setTimeout(500000);
    var cnt = 0;
    sequelize.query('SELECT elenco_google_calendar_watch_cron_test();', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        useMaster: true
    }).then(function(datas) {
        if (datas && datas[0] && datas[0].elenco_google_calendar_watch_cron_test) {

            datas[0].elenco_google_calendar_watch_cron_test.forEach(function(calendar) {
               
                    console.log(calendar.organization);
                    async.parallel({
                        token: function(callback) {
                            sequelize.query('SELECT get_google_calendar_info(\'' + calendar.organization + '\',\'' + calendar.calendar_id + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                                .then(function(datas) {


                                    if (datas && datas[0] && datas[0].get_google_calendar_info) {
                                        var tok = JSON.parse(datas[0].get_google_calendar_info);
                                        if (tok.access_token) {
                                            callback(null, tok.access_token);
                                        } else if (tok.refresh_token) {

                                            request.post('https://oauth2.googleapis.com/token', {
                                                form: {
                                                    grant_type: 'refresh_token',
                                                    refresh_token: tok.refresh_token,
                                                    client_id: '212534379139-9o1463se7g67mbl97krsnmkve53nfkft.apps.googleusercontent.com',
                                                    client_secret: 'xZzP4Y0YbrUv8XFKS1YeCqBT'
                                                }
                                            }, function(err, res, body) {
                                                body = JSON.parse(body);
                                                console.log('REFRESH '+JSON.stringify(body));
                                                if(body.error){
                                                    callback(body.error, null);
                                                }
                                                else{
                                                    sequelize.query('SELECT update_google_calendar_token(\'' + calendar.organization +
                                                        '\', \'' + body.access_token + '\',' + tok.contact_id + ');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(datas) {
                                                        callback(null, body.access_token);

                                                    });
                                                }
                                                
                                            });
                                        }
                                    } else {

                                        callback(null, null);
                                    }
                                });
                        },
                        calendar: function(callback) {
                            callback(null, calendar.calendar_id);
                        },
                        organization: function(callback) {
                            callback(null, calendar.organization);
                        },
                        sync_token: function(callback) {
                            sequelize.query('SELECT get_google_calendar_info(\'' + calendar.organization + '\',\'' + calendar.calendar_id + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                })
                                .then(function(datas) {
                                    

                                    if (datas && datas[0] && datas[0].get_google_calendar_info) {
                                        var tok = JSON.parse(datas[0].get_google_calendar_info);
                                        if (tok.sync_token && tok.sync_token !== 'undefined') {
                                            callback(null, tok.sync_token);
                                        } else {
                                            console.log('ENTRO QUI');
                                           callback(null,google_calendar_sync_token(calendar, tok, null, null));


                                        }
                                    } else {

                                        callback(null, null);
                                    }
                                });
                        },
                    }, function(err, risultato) {
                        if (err) {
                            console.log('ERRORE async TOKEN');
                            console.log(err);
                            console.log(risultato);
                            if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {
                                res.status(200).send();
                            }
                        }
                        else{
                            console.log('RISULTATO');
                            console.log(risultato);
                            var access_token_ok = risultato.token;

                            if(access_token_ok){

                                var headers = {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + access_token_ok
                                };

                                console.log(headers);
                                // Configure the request
                                var options = {
                                    url: 'https://www.googleapis.com/calendar/v3/channels/stop',
                                    method: 'POST',
                                    headers: headers,
                                    json: JSON.parse('{"id": "gc-beebeeboard-notification-' + calendar.organization + '","resourceId": "' + calendar.resource_id + '" }')
                                };
                                // Start the request
                                request(options, function(error, response, body) {
                                   

                                    if (!error) {
                                        


                                        options = {
                                            url: 'https://www.googleapis.com/calendar/v3/calendars/' + calendar.calendar_id + '/events/watch?alt=json&showdeleted=true',
                                            method: 'POST',
                                            headers: headers,
                                            json: JSON.parse('{"id": "gc-beebeeboard-notification-' + calendar.organization + '","type": "web_hook","address": "https://google-calendar.beebeeboard.com/api/v1/calendar_notification", "expiration": "'+expiration+'" }')
                                        };
                                        request(options, function(error, response2, body2) {
                                            
                                            if (!error && response2.statusCode == 200 && body2) {
                                                if (body2) {
                                                    console.log('BODY: ');
                                                    console.log(JSON.stringify(body2));
                                                    sequelize.query('SELECT update_google_calendar_watch(\'' + calendar.organization +
                                                        '\', ' + calendar.contact_id + ',\'' + calendar.calendar_id + '\',\'' +
                                                        body2.resourceId + '\',\'' +
                                                        body2.resourceUri + '\',\'' +
                                                        body2.expiration + '\');', {
                                                            raw: true,
                                                            type: Sequelize.QueryTypes.SELECT,
                                                            useMaster: true
                                                        }).then(function(datas1) {

                                                       
                                                        if (++cnt >= datas[0].elenco_google_calendar_watch_cron_test.length) {


                                                            res.status(200).send();
                                                        }

                                                     }).catch(function(err) {
                                                        return res.status(400).render('update_google_calendar_watch: ' + err);
                                                    });
                                                }

                                            } else {
                                                console.log('ERRORE CHIAMATA START WATCH GOOGLE');
                                                console.log(JSON.stringify(JSON.parse(error)));
                                                console.log(JSON.stringify(body2));

                                                if (++cnt >= datas[0].elenco_google_calendar_watch_cron_test.length) {
                                                    res.status(200).send();
                                                }
                                            }
                                        });
                                    } else {
                                        console.log('ERRORE CHIAMATA STOP GOOGLE');
                                        console.log(JSON.stringify(JSON.parse(error)));

                                        if (++cnt >= datas[0].elenco_google_calendar_watch_cron_test.length) {
                                            res.status(200).send();
                                        }
                                    }
                                });
                            }else{
                                console.log('TOKEN NON PRESENTE' + calendar.calendar_id + ' ' + calendar.organization);
                                if (++cnt >= datas[0].elenco_google_calendar_watch_cron.length) {
                                    res.status(200).send();
                                }
                            }
                        }
                        
                        
                    });
                
            });
        } else {
            res.status(200).send();
        }

    }).catch(function(err) {
        return res.status(400).render('google_calendar_watch_cron: ' + err);
    });


    /*

    var mandrill = require('mandrill-api/mandrill');
    var reply_to = 'no-reply@beebeemailer.com';
    var mails = [];

    sequelize.query('SELECT search_organization(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster: true
        })
    .then(function(reply_mail) {

        console.log(reply_mail[0].search_organization.survey_json.survey);
        

        var mail = {
            "email": 'luca@poppix.it',
            "name": 'Luca',
            "type": "to"
        };

        mails.push(mail);

        var template_content = [{
                "name": "LANG",
                "content": reply_mail[0].search_organization.lang
            }, {
                "name": "ORGANIZATION",
                "content": reply_mail[0].search_organization.name
            }, {
                "name": "TITLE",
                "content": (reply_mail[0].search_organization.survey_json && reply_mail[0].search_organization.survey_json.title? reply_mail[0].search_organization.survey_json.title : '')
            }, {
                "name": "COLOR",
                "content": (reply_mail[0].search_organization.primary_color ? reply_mail[0].search_organization.primary_color : '#00587D') 
            }, {
                "name": "LOGO",
                "content": (reply_mail[0].search_organization.thumbnail_url ? 'https://data.beebeeboard.com/' + reply_mail[0].search_organization.thumbnail_url + '-small.jpeg' : null)
            }, {
                "name": "TITLE_ENG",
                "content": (reply_mail[0].search_organization.survey_json && reply_mail[0].search_organization.survey_json.title_eng ? reply_mail[0].search_organization.survey_json.title_eng : '')
            }, {
                "name": "TESTO_MESSAGGIO_ENG",
                "content": (reply_mail[0].search_organization.survey_json && reply_mail[0].search_organization.survey_json.testo_eng ? reply_mail[0].search_organization.survey_json.testo_eng : '')
            }, {
                "name": "TESTO_MESSAGGIO",
                "content": (reply_mail[0].search_organization.survey_json && reply_mail[0].search_organization.survey_json.testo ? reply_mail[0].search_organization.survey_json.testo : '')
            }, {
                "name": "SURVEY",
                "content": (reply_mail[0].search_organization.survey_json && reply_mail[0].search_organization.survey_json.survey ? reply_mail[0].search_organization.survey_json.survey : '')
            }],
            message = {
                "global_merge_vars": template_content,
                "subject": (reply_mail[0].search_organization.name ? reply_mail[0].search_organization.name +' - Lascia un Feedback' : 'Lascia un Feedback'),
                "from_email": "no-reply@beebeemailer.com",
                "from_name": (reply_mail[0].search_organization.option_field &&  reply_mail[0].search_organization.option_field.default_mail ? reply_mail[0].search_organization.option_field.default_mail : null) ,
                "to": mails,
                "merge_language": "handlebars",
                "headers": {
                    "Reply-To": reply_to
                },
                "tags": ["beebeeboard-survey", req.headers['host'].split(".")[0]],
                "subaccount": "beebeeboard",
                "tracking_domain": "mandrillapp.com",
                "google_analytics_domains": ["poppix.it"],
                "metadata": {
                    "organization": req.headers['host'].split(".")[0]
                }
            };

        var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

        mandrill_client.messages.sendTemplate({
            "template_name": "survey",
            "template_content": template_content,
            "message": message,
            "async": true,
            "send_at": false
        }, function(result) {

            if (result && result[0] && result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
                console.log("Mail sended correctly");

                 res.status(200).send({});

            } else {
                console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred ');
                // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
                
                        res.status(422).send({});
            }
        }, function(e) {
            // Mandrill returns the error as an object with name and message keys
            console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' A mandrill error occurred: ' + e.name + ' - ' + e.message);

        });
    });
     
    */
}

function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};

function google_calendar_sync_token(cal, tok, new_page_tok, syn_tok){
    var request = require('request');
    var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tok.access_token
    }

    if(syn_tok){
         var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal.calendar_id + '/events?maxResults=10000&singleEvents=true&syncToken=' + syn_tok + '&alt=json',
            method: 'GET',
            headers: headers
        }


    }else if(new_page_tok){
         var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal.calendar_id + '/events?maxResults=10000&singleEvents=true&pageToken=' + new_page_tok + '&alt=json',
            method: 'GET',
            headers: headers
        }
    }
    else{
         var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal.calendar_id + '/events?maxResults=10000&singleEvents=true&alt=json',
            method: 'GET',
            headers: headers
        }
    }

    console.log(options);
   
    // Start the request
    request(options, function(error, response, body) {
        if (error) {
            console.log('error' + error);
            return null;
        } else {
            body = JSON.parse(body);
            //console.log(body);
           

                        
            if (body.nextSyncToken) {
                sequelize.query('SELECT update_google_calendar_sync_token(\'' + cal.organization +
                    '\', \'' + body.nextSyncToken + '\',' + tok.contact_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(datas) {
                     return body.nextSyncToken;

                });
            } else if (body.nextPageToken) {

                console.log('ASPETTO IL SYNC TOKEN PASSO IL NEXT PAGE '+body.nextPageToken);
               return google_calendar_sync_token(cal, tok, body.nextPageToken, null);
            }

        }
    });

      
};
