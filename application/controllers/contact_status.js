var moment = require('moment'),
  Sequelize = require('sequelize'),
  async = require('async'),
  utility = require('../utility/utility.js'),
  logs = require('../utility/logs.js');

exports.item_index = function(req, res) {
  utility.get_parameters(req, 'item_status', function(err, parameters){
    if(err){
     return res.status(400).render('item_states_index: ' + err);
    }
    

    async.parallel({
      data: function(callback) {
        var response = {};
        sequelize.query('SELECT item_states_data(' + 
          req.user._tenant_id + ',\'' + 
          req.headers['host'].split(".")[0] + '\',' + 
          req.user.role_id + ',' + 
          req.user.id + ',' +  
          parameters.archivied+','+
          (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' + 
          (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' + 
          (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' + 
          (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' + 
          (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' + 
          (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' + 
          (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ','+
          (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
          })
          .then(function(datas) {

            if (datas && datas[0] && datas[0].item_states_data && datas[0].item_states_data != null) {
              callback(null, datas[0].item_states_data);

            } else {

              callback(null, []);
            }

          }).catch(function(err) {
            console.log(err);
            callback(err, null);
          });
      }
    }, function(err, results) {

      if (err) {
        return res.status(400).render('item_states_index: ' + err);
      }

      res.json({
        'data': results.data
      });
    });
  });
};

exports.index = function(req, res) {
  utility.get_parameters(req, 'contact_status', function(err, parameters){
    if(err){
     return res.status(400).render('contact_states_index: ' + err);
    }
    

    async.parallel({
      data: function(callback) {
        var response = {};
        sequelize.query('SELECT contact_states_data_v4(' + 
          req.user._tenant_id + ',\'' + 
          req.headers['host'].split(".")[0] + '\',' + 
          req.user.role_id + ',' + 
          req.user.id + ',' + 
          (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY['+parameters.q1+']' : null) +','+
          parameters.archivied+','+
          (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' + 
          (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' + 
          (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' + 
          (parameters.workhour_ids && parameters.workhour_ids.length > 0 ? 'ARRAY[' + parameters.workhour_ids + ']::bigint[]' : null) + ',' + 
          (parameters.invoice_ids && parameters.invoice_ids.length > 0 ? 'ARRAY[' + parameters.invoice_ids + ']::bigint[]' : null) + ',' + 
          (parameters.expense_ids && parameters.expense_ids.length > 0 ? 'ARRAY[' + parameters.expense_ids + ']::bigint[]' : null) + ',' + 
          (parameters.payment_ids && parameters.payment_ids.length > 0 ? 'ARRAY[' + parameters.payment_ids + ']::bigint[]' : null) + ','+
          (parameters.event_ids && parameters.event_ids.length > 0 ? 'ARRAY[' + parameters.event_ids + ']::bigint[]' : null) + ','+
          parameters.all_any +');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
          })
          .then(function(datas) {

            if (datas[0].contact_states_data_v4 && datas[0].contact_states_data_v4 != null) {
              callback(null, datas[0].contact_states_data_v4);

            } else {

              callback(null, []);
            }

          }).catch(function(err) {
            console.log(err);
            callback(err, null);
          });
      }
    }, function(err, results) {

      if (err) {
        return res.status(400).render('contact_states_index: ' + err);
      }

      res.json({
        'data': results.data
      });
    });
  });
};

exports.find = function(req, res) {

  if (utility.check_id(req.params.id)) {
    return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
  }

   utility.get_parameters(req, 'contact', function(err, parameters){
    if(err){
      callback_(err, null);
    }

    async.parallel({
      data: function(callback) {
        var response = {};
        sequelize.query('SELECT contact_states_find_data_v3(' + 
          req.user._tenant_id + ',\'' + 
          req.headers['host'].split(".")[0] + '\',' + 
          req.params.id + ',' + 
          req.user.role_id + ',' + 
          req.user.id + ','+
          (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY['+parameters.q1+']' : null) +','+
          parameters.archivied+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
          })
          .then(function(datas) {

            if (datas[0].contact_states_find_data_v3 && datas[0].contact_states_find_data_v3 != null) {
              callback(null, datas[0].contact_states_find_data_v3);

            } else {

              callback(null, []);
            }

          }).catch(function(err) {
            console.log(err);
            callback(err, null);
          });
      }
    }, function(err, results) {

      if (err) {
        return res.status(400).render('contact_states_find: ' + err);
      }

      res.json({
        'data': results.data[0]
      });
    });
  });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
  if (!req.user.role[0].json_build_object.attributes.is_admin) {
    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
  }
  var contact_status = {};

  if (req.body.data.attributes !== undefined) {
    // Import case

    contact_status['rank'] = (!utility.check_id(req.body.data.attributes.rank && req.body.data.attributes.rank) ? req.body.data.attributes.rank : null);
    contact_status['organization_id'] = req.user.organization_id;
    contact_status['status_name'] = (req.body.data.attributes.status_name && req.body.data.attributes.status_name !== null && utility.check_type_variable(req.body.data.attributes.status_name,'string') ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '');
    contact_status['status_color'] = (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color,'string') ? req.body.data.attributes.status_color : null);
    contact_status['status_default'] = (req.body.data.attributes.status_default !== null && utility.check_type_variable(req.body.data.attributes.status_default,'boolean') ? req.body.data.attributes.status_default : null);
    contact_status['_tenant_id'] = req.user._tenant_id;
    contact_status['organization'] = req.headers['host'].split(".")[0];
    contact_status['mongo_id'] = null;

    sequelize.query('SELECT insert_contact_status(\'' + JSON.stringify(contact_status) + '\',\''+
      req.headers['host'].split(".")[0]+'\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
      })
      .then(function(datas) {
        req.body.data.id = datas[0].insert_contact_status;

        logs.save_log_model(
          req.user._tenant_id, 
          req.headers['host'].split(".")[0],
          'contact_status',
          'insert',
          req.user.id,
          JSON.stringify(req.body.data.attributes),
          datas[0].insert_contact_status,
          'Aggiunto contact_status '+datas[0].insert_contact_status
        );

        utility.save_socket(req.headers['host'].split(".")[0], 'contact_state', 'insert', req.user.id, datas[0].insert_contact_status, req.body.data);
                        

        res.json({
          'data': req.body.data,
          'relationships': req.body.data.relationships
        });

      }).catch(function(err) {
        return res.status(400).render('contact_status_insert: ' + err);
      });
  }
  else
  {
    res.json({
      'data':[]
    });
  }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
  if (!req.user.role[0].json_build_object.attributes.is_admin) {
    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
  }

  if (utility.check_id(req.params.id)) {
    return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
  }

  if (req.body.data.attributes !== undefined) {
    // Import case
    sequelize.query('SELECT update_contact_status(' + 
        req.params.id + ',' + 
        ((!utility.check_id(req.body.data.attributes.rank) && req.body.data.attributes.rank !== null) ? req.body.data.attributes.rank : null) + ',\'' + 
        (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name,'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '') + '\',\'' + 
        (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color,'string') ? req.body.data.attributes.status_color : '') + '\',' + 
        (req.body.data.attributes.status_default !== null && utility.check_type_variable(req.body.data.attributes.status_default,'boolean') ? req.body.data.attributes.status_default : null) + ',' + 
        req.user._tenant_id + ',\'' + 
        req.headers['host'].split(".")[0] + '\');', {
          raw: true,
          type: Sequelize.QueryTypes.SELECT,
                useMaster: true
      })
      .then(function(datas) {

        logs.save_log_model(
          req.user._tenant_id, 
          req.headers['host'].split(".")[0],
          'contact_status',
          'update',
          req.user.id,
          JSON.stringify(req.body.data.attributes),
          req.params.id,
          'Modificato contact_status '+req.params.id
        );

        utility.save_socket(req.headers['host'].split(".")[0], 'contact_state', 'update', req.user.id, req.params.id, req.body.data);
        
        res.json({
          'data': req.body.data
        });

      }).catch(function(err) {
        return res.status(400).render('contact_status_update: ' + err);
      });
  }
  else
  {
    res.json({
      'data':[]
    });
  }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
  if (!req.user.role[0].json_build_object.attributes.is_admin) {
    return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
  }

  if (utility.check_id(req.params.id)) {
    return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
  }

  sequelize.query('SELECT delete_contact_status_v1(' + 
    req.params.id + ',' + 
    req.user._tenant_id + ',\'' + 
    req.headers['host'].split(".")[0] + '\','+
    req.user.id+');', {
      raw: true,
      type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    })
    .then(function(datas) {

      utility.save_socket(req.headers['host'].split(".")[0], 'contact_state', 'delete', req.user.id, req.params.id, null);
        
      res.json({
        'data': {
          'type': 'contact_states',
          'id': datas[0].delete_contact_status_v1
        }
      });

    }).catch(function(err) {
      return res.status(400).render('contact_status_destroy: ' + err);
    });
};