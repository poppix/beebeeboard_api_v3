var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var tag = {};

    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT insert_tag_v1(\'' +
                (req.body.data.attributes.value && utility.check_type_variable(req.body.data.attributes.value, 'string') && req.body.data.attributes.value !== null ? req.body.data.attributes.value.replace(/'/g, "''''") : '') + '\',' +
                ((req.body.data.relationships && req.body.data.relationships.file && req.body.data.relationships.file.data && req.body.data.relationships.file.data.id && !utility.check_id(req.body.data.relationships.file.data.id)) ? req.body.data.relationships.file.data.id : null) +
                ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_tag_v1;

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });


            }).catch(function(err) {
                return res.status(400).render('tag_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_tag(' +
                req.params.id + ',\'' +
                (req.body.data.attributes.value && utility.check_type_variable(req.body.data.attributes.value, 'string') && req.body.data.attributes.value !== null ? req.body.data.attributes.value.replace(/'/g, "''''") : '') + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('tag_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_tag(' +
            req.params.id + ',\'' +
            req.headers['host'].split(".")[0] +
            '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'tags',
                    'id': datas[0].delete_tag
                }
            });

        }).catch(function(err) {
            return res.status(400).render('tag_destroy: ' + err);
        });
};