//var Trello = require("node-trello");

exports.boards_id = function(req, res) {
    var t = new Trello(req.user.trello_key, req.user.trello_token);
    var param = {};

    Object.keys(req.query).forEach(function(key) {
        if (key.toString('utf-8') !== 'access_token' && key.toString('utf-8') !== '_') {
            param[key] = req.query[key];
        }
    });

    t.get("/1/boards/" + req.params.id, param, function(err, data) {
        if (err)
            return res.status(500).send(err);

        res.send(data);
    });
};

exports.boards = function(req, res) {
    var t = new Trello(req.user.trello_key, req.user.trello_token);
    t.get("/1/members/me/boards", function(err, data) {
        if (err)
            return res.status(500).send(err);

        res.send(data);
    });
};