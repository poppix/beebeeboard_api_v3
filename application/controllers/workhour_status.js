var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.index = function(req, res) {
    utility.get_parameters(req, 'workhour_status', function(err, parameters) {
        if (err) {
            return res.status(400).render('workhour_status: ' + err);
        }

        async.parallel({
            data: function(callback) {
                sequelize.query('SELECT workhour_states_data_v3(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.role_id + ',' +
                        req.user.id + ',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                        (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                        (parameters.project_ids && parameters.project_ids.length > 0 ? 'ARRAY[' + parameters.project_ids + ']::bigint[]' : null) + ',' +
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
                        parameters.all_any + ',' +
                        parameters.archivied + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        if (datas[0].workhour_states_data_v3 && datas[0].workhour_states_data_v3 != null) {
                            callback(null, datas[0].workhour_states_data_v3);

                        } else {

                            callback(null, []);
                        }

                    }).catch(function(err) {
                        callback(err, null);
                    });
            }
        }, function(err, results) {

            if (err) {
                return res.status(400).render('wokrhour_states_index: ' + err);
            }
            if (results.data.length > 0) {
                results.data.forEach(function(dat) {
                    if (!dat.attributes.duration) {
                        dat.attributes.duration = 0;
                    }
                });
            }

            res.json({
                'data': results.data
            });
        });
    });
};

exports.find = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT workhour_states_find_data_v3(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.params.id + ',' +
                    req.user.role_id + ',' +
                    req.user.id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {

                    if (datas[0].workhour_states_find_data_v3 && datas[0].workhour_states_find_data_v3 != null) {
                        callback(null, datas[0].workhour_states_find_data_v3);

                    } else {

                        callback(null, []);
                    }

                }).catch(function(err) {
                    console.log(err);
                    callback(err, null);
                });
        }
    }, function(err, results) {

        if (err) {
            return res.status(400).render('workhour_states_find: ' + err);
        }

        res.json({
            'data': results.data[0]
        });
    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var workhour_status = {};

    if (req.body.data.attributes !== undefined) {

        workhour_status['rank'] = req.body.data.attributes.rank;
        workhour_status['organization_id'] = req.user.organization_id;
        workhour_status['status_name'] = (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name, 'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '');
        workhour_status['status_color'] = (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color, 'string') && req.body.data.attributes.status_color !== null ? req.body.data.attributes.status_color.replace(/'/g, "''''") : '');
        workhour_status['status_default'] = (req.body.data.attributes.status_default !== undefined && utility.check_type_variable(req.body.data.attributes.status_default, 'boolean') && req.body.data.attributes.status_default !== null ? req.body.data.attributes.status_default : false);
        workhour_status['_tenant_id'] = req.user._tenant_id;
        workhour_status['organization'] = req.headers['host'].split(".")[0];
        workhour_status['mongo_id'] = null;

        sequelize.query('SELECT insert_workhour_status(\'' +
                JSON.stringify(workhour_status) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_workhour_status;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'workhour_status',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_workhour_status,
                    'Aggiunto workhour_status ' + datas[0].insert_workhour_status
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'workhour_state', 'insert', req.user.id, datas[0].insert_workhour_status, req.body.data);
        

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('workhour_status_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_workhour_status(' +
                req.params.id + ',' +
                req.body.data.attributes.rank + ',\'' +
                (req.body.data.attributes.status_name && utility.check_type_variable(req.body.data.attributes.status_name, 'string') && req.body.data.attributes.status_name !== null ? req.body.data.attributes.status_name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.status_color && utility.check_type_variable(req.body.data.attributes.status_color, 'string') && req.body.data.attributes.status_color !== null ? req.body.data.attributes.status_color.replace(/'/g, "''''") : '') + '\',' +
                (req.body.data.attributes.status_default !== undefined && utility.check_type_variable(req.body.data.attributes.status_default, 'boolean') && req.body.data.attributes.status_default !== null ? req.body.data.attributes.status_default : false) + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'workhour_status',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato workhour_status ' + req.params.id
                );

                utility.save_socket(req.headers['host'].split(".")[0], 'workhour_state', 'update', req.user.id, req.params.id, req.body.data);
        

                res.json({
                    'data': req.body.data
                });

            }).catch(function(err) {
                return res.status(400).render('workhour_status_update: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT delete_workhour_status_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            utility.save_socket(req.headers['host'].split(".")[0], 'workhour_state', 'delete', req.user.id, req.params.id, null);
        

            res.json({
                'data': {
                    'type': 'workhour_states',
                    'id': datas[0].delete_workhour_status_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('workhour_status_destroy: ' + err);
        });
};