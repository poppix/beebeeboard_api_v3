var moment = require('moment'),
    Sequelize = require('sequelize'),
    utility = require('../utility/utility.js'),
    async = require('async'),
    logs = require('../utility/logs.js');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {

    var sortField = (req.query.sort === undefined) ? 'name' : req.query.sort,
        sortDirection = (req.query.direction === undefined) ? 'DESC' : req.query.direction,
        sortObj = {},
        page_num = 1,
        page_count = 50;


    if (req.query.pag !== undefined && req.query.pag !== '') {
        if (utility.check_id(req.query.pag)) {
            return res.status(422).send(utility.error422('pag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_num = req.query.pag;
    }

    if (req.query.xpag !== undefined && req.query.xpag !== '') {
        if (utility.check_id(req.query.xpag)) {
            return res.status(422).send(utility.error422('xpag', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        page_count = req.query.xpag;
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT organization_custom_fields_data(' +
                page_count + ',' +
                (page_num - 1) * page_count + ',' +
                req.user._tenant_id + ',\'' +
                sortDirection + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                //callback(null,datas[0].contacts_data);
                if (datas[0].organization_custom_fields_data && datas[0].organization_custom_fields_data != null) {
                    callback(null, datas[0].organization_custom_fields_data);

                } else {

                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        },
        incuded: function(callback) {
            sequelize.query('SELECT organization_custom_fields_included(' +
                page_count + ',' +
                (page_num - 1) * page_count + ',' +
                req.user._tenant_id + ',\'' +
                sortDirection + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                if (datas[0].organization_custom_fields_included && datas[0].organization_custom_fields_included != null) {
                    callback(null, included[0].organization_custom_fields_included);
                } else {
                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {
        if (err) {
            return res.status(400).render('organization_custom_field_index: ' + err);
        }
        if (results.data.length == 0 || !results.included) {
            return res.status(404).send(utility.error404('id', 'Nessun organizazzione trovato', 'Nessun organizzazione trovato', '404'));

        }

        res.json({
            'data': utility.check_find_datas(results.data, 'organization_custom_fields', ['products', 'projects', 'custom_fields', 'contacts', 'custom_field_definitions']),
            'included': utility.check_find_included(results.included, 'organization_custom_fields', ['products', 'projects', 'custom_fields', 'contacts', 'custom_field_definitions',
                'contact_states', 'product_states', 'project_states', 'event_states', 'workhour_states'
            ])
        });

    });
};

exports.find = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT organization_custom_field_find_data(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                if (datas[0].organization_custom_field_find_data && datas[0].organization_custom_field_find_data != null) {
                    callback(null, datas[0].organization_custom_field_find_data[0]);
                } else {
                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        },
        included: function(callback) {
            sequelize.query('SELECT organization_custom_field_find_included(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                if (datas[0].organization_custom_field_find_included && datas[0].organization_custom_field_find_included != null) {
                    callback(null, datas[0].organization_custom_field_find_included);
                } else {
                    callback(null, []);
                }
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        },
    }, function(err, results) {
        if (err) {
            return res.status(400).render('organization_custom_field_find: ' + err);
        }
        if (results.data.length == 0 || !results.included) {
            return res.status(404).send(utility.error404('id', 'Nessun campo personalizzato trovato', 'Nessun campo personalizzato trovato', '404'));
        }
        res.json({
            'data': utility.check_find_datas(results.data, 'organization_custom_fields', ['products', 'projects', 'custom_fields', 'contacts', 'custom_field_definitions']),
            'included': utility.check_find_included(results.included, 'organization_custom_fields', ['products', 'projects', 'custom_fields', 'contacts', 'custom_field_definitions',
                'contact_states', 'product_states', 'project_states', 'event_states', 'workhour_states'
            ])
        });
    });
};

exports.create = function(req, res) {

    var ocf = {};
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT control_custom_field_for_plan(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(custom_ok) {

            if (custom_ok[0].control_custom_field_for_plan != 0) {

                ocf['organization_id'] = req.user.organization_id;
                ocf['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
                ocf['data_type'] = (req.body.data.attributes.data_type && utility.check_type_variable(req.body.data.attributes.data_type, 'string') && req.body.data.attributes.data_type !== null ? req.body.data.attributes.data_type.replace(/'/g, "''''") : '');
                ocf['model_related'] = (req.body.data.attributes.model_related && utility.check_type_variable(req.body.data.attributes.model_related, 'string') && req.body.data.attributes.model_related !== null ? req.body.data.attributes.model_related.replace(/'/g, "''''") : '');
                ocf['rank'] = req.body.data.attributes.rank;
                ocf['disabled'] = (req.body.data.attributes.disabled !== undefined && utility.check_type_variable(req.body.data.attributes.disabled, 'boolean') && req.body.data.attributes.disabled !== null ? req.body.data.attributes.disabled : false);
                ocf['in_contact'] = (req.body.data.attributes.in_contact !== undefined && utility.check_type_variable(req.body.data.attributes.in_contact, 'boolean') && req.body.data.attributes.in_contact !== null ? req.body.data.attributes.in_contact : false);
                ocf['in_product'] = (req.body.data.attributes.in_product !== undefined && utility.check_type_variable(req.body.data.attributes.in_product, 'boolean') && req.body.data.attributes.in_product !== null ? req.body.data.attributes.in_product : false);
                ocf['in_event'] = (req.body.data.attributes.in_event !== undefined && utility.check_type_variable(req.body.data.attributes.in_event, 'boolean') && req.body.data.attributes.in_event !== null ? req.body.data.attributes.in_event : false);
                ocf['in_project'] = (req.body.data.attributes.in_project !== undefined && utility.check_type_variable(req.body.data.attributes.in_project, 'boolean') && req.body.data.attributes.in_project !== null ? req.body.data.attributes.in_project : false);
                ocf['in_workhour'] = (req.body.data.attributes.in_workhour !== undefined && utility.check_type_variable(req.body.data.attributes.in_workhour, 'boolean') && req.body.data.attributes.in_workhour !== null ? req.body.data.attributes.in_workhour : false);
                ocf['_tenant_id'] = req.user._tenant_id;
                ocf['organization'] = req.headers['host'].split(".")[0];
                ocf['mongo_id'] = null;
                ocf['contact_status_id'] = ((req.body.data.relationships && req.body.data.relationships.contact_state && req.body.data.relationships.contact_state.data && req.body.data.relationships.contact_state.data.id && !utility.check_id(req.body.data.relationships.contact_state.data.id)) ? req.body.data.relationships.contact_state.data.id : null);
                ocf['product_status_id'] = ((req.body.data.relationships && req.body.data.relationships.product_state && req.body.data.relationships.product_state.data && req.body.data.relationships.product_state.data.id && !utility.check_id(req.body.data.relationships.product_state.data.id)) ? req.body.data.relationships.product_state.data.id : null);
                ocf['project_status_id'] = ((req.body.data.relationships && req.body.data.relationships.project_state && req.body.data.relationships.project_state.data && req.body.data.relationships.project_state.data.id && !utility.check_id(req.body.data.relationships.project_state.data.id)) ? req.body.data.relationships.project_state.data.id : null);
                ocf['event_status_id'] = ((req.body.data.relationships && req.body.data.relationships.event_state && req.body.data.relationships.event_state.data && req.body.data.relationships.event_state.data.id && !utility.check_id(req.body.data.relationships.event_state.data.id)) ? req.body.data.relationships.event_state.data.id : null);
                ocf['workhour_status_id'] = ((req.body.data.relationships && req.body.data.relationships.workhour_state && req.body.data.relationships.workhour_state.data && req.body.data.relationships.workhour_state.data.id && !utility.check_id(req.body.data.relationships.workhour_state.data.id)) ? req.body.data.relationships.workhour_state.data.id : null);

                sequelize.query('SELECT insert_organization_custom_field(\'' + JSON.stringify(ocf) + '\',\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].insert_organization_custom_field && datas[0].insert_organization_custom_field != null) {
                        req.body.data.id = datas[0].insert_organization_custom_field;

                        sequelize.query('SELECT sanitize_custom_fields(' + datas[0].insert_organization_custom_field + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            req.user._tenant_id + ');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            }).then(function(datas) {}).catch(function(err) {
                            return res.status(400).render('sanitize_custom_fields: ' + err);
                        });

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': datas[0].insert_organization_custom_field,
                            'body': req.body.data.relationships,
                            'user_id': req.user.id,
                            'model': 'organization_custom_field',
                            'array': ['custom_field_definition']
                        }, function(err, results) {

                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'organization_custom_field',
                                'insert',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                datas[0].insert_organization_custom_field,
                                'Aggiunto campo personalizzato in organizzazione  ' + datas[0].insert_organization_custom_field
                            );

                            res.json({
                                'data': req.body.data,
                                'relationships': req.body.data.relationships
                            });
                        });
                    } else {
                        res.json({
                            'data': [],
                            'relationships': []
                        });
                    }

                }).catch(function(err) {
                    return res.status(400).render('organization_custom_field_insert: ' + err);
                });
            } else {
                return res.status(422).send(utility.error422('id', 'Raggiunto il numero massimo di Campi personalizzati per il piano a cui si è abbonati', 'Raggiunto il numero massimo di Campi personalizzati per il piano a cui si è abbonati', '422'));
            }
        }).catch(function(err) {
            return res.status(400).render('organization_custom_field_insert: ' + err);
        });

    } else {
        res.json({
            'data': []
        });
    }
};

exports.update = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined) {
        sequelize.query('SELECT update_organization_custom_field(' +
            req.params.id + ',' +
            (req.body.data.relationships && req.body.data.relationships.organization && req.body.data.relationships.organization.data && req.body.data.relationships.organization.data.id != null && !utility.check_id(req.body.data.relationships.organization.data.id) ? req.body.data.relationships.organization.data.id : null) + ',\'' +
            (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',\'' +
            (req.body.data.attributes.data_type && utility.check_type_variable(req.body.data.attributes.data_type, 'string') && req.body.data.attributes.data_type !== null ? req.body.data.attributes.data_type : '') + '\',\'' +
            (req.body.data.attributes.model_related && utility.check_type_variable(req.body.data.attributes.model_related, 'string') && req.body.data.attributes.model_related !== null ? req.body.data.attributes.model_related : '') + '\',' +
            req.body.data.attributes.rank + ',' +
            (req.body.data.attributes.disabled !== undefined && utility.check_type_variable(req.body.data.attributes.disabled, 'boolean') && req.body.data.attributes.disabled !== null ? req.body.data.attributes.disabled : false) + ',' +
            (req.body.data.attributes.in_contact !== undefined && utility.check_type_variable(req.body.data.attributes.in_contact, 'boolean') && req.body.data.attributes.in_contact !== null ? req.body.data.attributes.in_contact : false) + ',' +
            (req.body.data.attributes.in_product !== undefined && utility.check_type_variable(req.body.data.attributes.in_product, 'boolean') && req.body.data.attributes.in_product !== null ? req.body.data.attributes.in_product : false) + ',' +
            (req.body.data.attributes.in_event !== undefined && utility.check_type_variable(req.body.data.attributes.in_event, 'boolean') && req.body.data.attributes.in_event !== null ? req.body.data.attributes.in_event : false) + ',' +
            (req.body.data.attributes.in_project !== undefined && utility.check_type_variable(req.body.data.attributes.in_project, 'boolean') && req.body.data.attributes.in_project !== null ? req.body.data.attributes.in_project : false) + ',' +
            (req.body.data.attributes.in_workhour !== undefined && utility.check_type_variable(req.body.data.attributes.in_workhour, 'boolean') && req.body.data.attributes.in_workhour !== null ? req.body.data.attributes.in_workhour : false) + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            ((req.body.data.relationships && req.body.data.relationships.contact_state && req.body.data.relationships.contact_state.data && req.body.data.relationships.contact_state.data.id && !utility.check_id(req.body.data.relationships.contact_state.data.id)) ? req.body.data.relationships.contact_state.data.id : null) + ',' +
            ((req.body.data.relationships && req.body.data.relationships.product_state && req.body.data.relationships.product_state.data && req.body.data.relationships.product_state.data.id && !utility.check_id(req.body.data.relationships.product_state.data.id)) ? req.body.data.relationships.product_state.data.id : null) + ',' +
            ((req.body.data.relationships && req.body.data.relationships.project_state && req.body.data.relationships.project_state.data && req.body.data.relationships.project_state.data.id && !utility.check_id(req.body.data.relationships.project_state.data.id)) ? req.body.data.relationships.project_state.data.id : null) + ',' +
            ((req.body.data.relationships && req.body.data.relationships.event_state && req.body.data.relationships.event_state.data && req.body.data.relationships.event_state.data.id && !utility.check_id(req.body.data.relationships.event_state.data.id)) ? req.body.data.relationships.event_state.data.id : null) + ',' +
            ((req.body.data.relationships && req.body.data.relationships.workhour_state && req.body.data.relationships.workhour_state.data && req.body.data.relationships.workhour_state.data.id && !utility.check_id(req.body.data.relationships.workhour_state.data.id)) ? req.body.data.relationships.workhour_state.data.id : null) + ',' +
            (req.body.data.attributes.always_visible !== undefined && utility.check_type_variable(req.body.data.attributes.always_visible, 'boolean') && req.body.data.attributes.always_visible !== null ? req.body.data.attributes.always_visible : false) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

            utility.sanitizeRelations({
                'organization': req.headers['host'].split(".")[0],
                '_tenant': req.user._tenant_id,
                'new_id': req.params.id,
                'body': req.body.data.relationships,
                'model': 'organization_custom_field',
                'user_id': req.user.id,
                'array': ['custom_field_definition']
            }, function(err, results) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'organization_custom_field',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato campo personalizzato in organizzazione  ' + req.params.id
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });
            });

        }).catch(function(err) {
            return res.status(400).render('organization_custom_field_update: ' + err);
        });

    } else {
        res.json({
            'data': []
        });
    }
};

exports.destroy = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_organization_custom_field_v1(' +
        req.params.id + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

        res.json({
            'data': {
                'type': 'organization_custom_fields',
                'id': datas[0].delete_organization_custom_field_v1
            }
        });

    }).catch(function(err) {
        return res.status(400).render('organization_custom_field_destroy: ' + err);
    });
};