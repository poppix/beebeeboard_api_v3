var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.assurance_index = function(req, res) {
  
    sequelize.query('SELECT assurance_data(' + 
      req.user._tenant_id + ',\'' + 
      req.headers['host'].split(".")[0] + '\',' + 
      req.user.role_id + ',' + 
      req.user.id + ');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
      })
      .then(function(datas) {

        if (datas && datas[0] && datas[0].assurance_data && datas[0].assurance_data != null) {
          return res.status(200).send({
            'data':datas[0].assurance_data
          });

        } else {

             return res.status(200).send({
                'data':[]
              });
        }

      }).catch(function(err) {
        console.log(err);
        return res.status(500).render('assurance_insert: ' + err);
      });
      
    
};


exports.get_assurances = function(req, res) {
  
    sequelize.query('SELECT assurance_all_data(' + 
      req.user._tenant_id + ',\'' + 
      req.headers['host'].split(".")[0] + '\',' + 
      req.user.role_id + ',' + 
      req.user.id + ');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
      })
      .then(function(datas) {

        if (datas && datas[0] && datas[0].assurance_all_data && datas[0].assurance_all_data != null) {
          return res.status(200).send(datas[0].assurance_all_data);

        } else {

            return res.status(200).send({});
        }

      }).catch(function(err) {
        console.log(err);
        return res.status(500).render('assurance_insert: ' + err);
      });
      
    
};

exports.assurance_get= function(req, res) {
  
    sequelize.query('SELECT assurance_find_data(' + 
      req.user._tenant_id + ',\'' + 
      req.headers['host'].split(".")[0] + '\',' + 
      req.user.role_id + ',' + 
      req.user.id + ','+
      req.params.id+');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
      })
      .then(function(datas) {

        if (datas && datas[0] && datas[0].assurance_find_data && datas[0].assurance_find_data != null) {
          return res.status(200).send({
            'data':datas[0].assurance_find_data[0]
          });

        } else {

            return res.status(200).send({});
        }

      }).catch(function(err) {
        console.log(err);
        return res.status(500).render('assurance_find_data: ' + err);
      });
      
    
};

exports.assurance_fields = function(req, res) {
  
    sequelize.query('SELECT assurance_fields_new(' + 
      req.user._tenant_id + ',\'' + 
      req.headers['host'].split(".")[0] + '\',' + 
      req.user.role_id + ',' + 
      req.user.id + ',\'' + 
      JSON.stringify(req.body).replace(/'/g, "''''") + '\','+
      req.body.assurance_id+');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
      })
      .then(function(datas) {

        if (datas && datas[0] && datas[0].assurance_fields_new && datas[0].assurance_fields_new != null) {
          return res.status(200).send(datas[0].assurance_fields_new);

        } else {

            return res.status(200).send({});
        }

      }).catch(function(err) {
        console.log(err);
        return res.status(500).render('assurance_fields: ' + err);
      });
      
    
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {

   
    if (req.body.data.attributes !== undefined) {
        // Import case


        sequelize.query('SELECT insert_assurance(\'' + JSON.stringify(req.body.data.attributes).replace(/'/g, "''''") + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_assurance && datas[0].insert_assurance != null) {
                    req.body.data.id = datas[0].insert_assurance;

                   
                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'assurance',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes).replace(/'/g, "''''"),
                            datas[0].insert_assurance,
                            'Aggiunto Assicurazione ' + datas[0].insert_assurance
                        );

                        res.json({
                            'data': req.body.data
                        });
                   
                }

            }).catch(function(err) {
                return res.status(400).render('assurance_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_assurance(' +
                req.params.id +
                ',' + req.user._tenant_id +
                ',\'' + req.headers['host'].split(".")[0] + '\',\''
                 + JSON.stringify(req.body.data).replace(/'/g, "''''") + '\');', {
         
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                     useMaster: true
                })
            .then(function(datas) {
               

                    logs.save_log_model(
                        req.user._tenant_id,
                        req.headers['host'].split(".")[0],
                        'assurance',
                        'update',
                        req.user.id,
                        JSON.stringify(req.body.data.attributes).replace(/'/g, "''''"),
                        req.params.id,
                        'Modificato assicurazione ' + req.params.id
                    );

                    res.json({
                        'data': req.body.data
                    });

                
            }).catch(function(err) {
                return res.status(400).render('assurance_update: ' + err);
            });
            
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_assurance(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'assurances',
                    'id': datas[0].delete_assurance
                }
            });

        }).catch(function(err) {
            return res.status(400).render('delete_assurance: ' + err);
        });
};

exports.assurance_default = function(req, res) {


    sequelize.query('SELECT assurance_default(\'' + JSON.stringify(req.body) + '\',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            

              res.status(200).send({});
        
        }).catch(function(err) {
            return res.status(400).render('assurance_default: ' + err);
        });
           
 
};

exports.clinic_chart_all_assurance = function(req, res) {
    utility.get_parameters(req, 'invoice', function(err, parameters) {
       
            sequelize.query('SELECT clinic_assurance_all_chart(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',null,'+
                req.user.role_id+','+
                req.user.id+',\''+
                req.query.type+'\',null);', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                async.parallel({
                  prezzi: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_all_chart && datas[0].clinic_assurance_all_chart[0] && datas[0].clinic_assurance_all_chart[0].Prezzo) {
                            callback(null, datas[0].clinic_assurance_all_chart[0].Prezzo);
                        } else {
                            callback(null, null);
                        }
                  },
                  num: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_all_chart && datas[0].clinic_assurance_all_chart[0] && datas[0].clinic_assurance_all_chart[0].Num) {
                            callback(null, datas[0].clinic_assurance_all_chart[0].Num);
                        } else {
                             callback(null, null);
                        }
                  },
                  ore: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_all_chart && datas[0].clinic_assurance_all_chart[0] && datas[0].clinic_assurance_all_chart[0].Ore) {
                            
                           
                            callback(null, datas[0].clinic_assurance_all_chart[0].Ore);
                               
                        } else {
                            callback(null, null);
                        }
                  },
                  trattamenti: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_all_chart && datas[0].clinic_assurance_all_chart[0] && datas[0].clinic_assurance_all_chart[0].Trattamenti) {
                            
                           
                            callback(null, datas[0].clinic_assurance_all_chart[0].Trattamenti);
                               
                        } else {
                            callback(null, null);
                        }
                  },
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Report Chart  Assurance' + err);
                    }

                    res.status(200).json({
                        'prezzi': results.prezzi,
                        'ore': results.ore,
                        'num': results.num,
                        'trattamenti': results.trattamenti
                    });
                });


            }).catch(function(err) {
                return res.status(400).render('clinic_assurance_all_chart: ' + err);
            });  
       
          
    });
};

exports.clinic_chart_assurance = function(req, res) {
    utility.get_parameters(req, 'project', function(err, parameters) {
       
            sequelize.query('SELECT clinic_assurance_chart(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',null,'+
                req.user.role_id+','+
                req.user.id+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                async.parallel({
                  prezzi: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_chart && datas[0].clinic_assurance_chart[0] && datas[0].clinic_assurance_chart[0].Prezzo) {
                            var cnt = 0;
                            var prezzi = {};
                            datas[0].clinic_assurance_chart[0].Prezzo.forEach(function(cat) {
                                

                                prezzi[cat.name] = [];
                                prezzi[cat.name].push(cat.tot);
                                if (cnt++ >= datas[0].clinic_assurance_chart[0].Prezzo.length - 1) {
                                    callback(null, prezzi);
                                }
                            });
                        } else {
                            callback(null, null);
                        }
                  },
                  num: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_chart && datas[0].clinic_assurance_chart[0] && datas[0].clinic_assurance_chart[0].Num) {
                            var cnt1 = 0;
                            var num = {};
                            datas[0].clinic_assurance_chart[0].Num.forEach(function(cat) {
                              
                                num[cat.name] = [];
                                num[cat.name].push(cat.num);
                                if (cnt1++ >= datas[0].clinic_assurance_chart[0].Num.length - 1) {
                                    callback(null, num);
                                }
                            });
                        } else {
                             callback(null, null);
                        }
                  },
                  ore: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_assurance_chart && datas[0].clinic_assurance_chart[0] && datas[0].clinic_assurance_chart[0].Ore) {
                            var cnt2 = 0;
                            var ore = {};
                            datas[0].clinic_assurance_chart[0].Ore.forEach(function(cat) {
                                

                                ore[cat.name] = [];
                                ore[cat.name].push(cat.ore);
                                if (cnt2++ >= datas[0].clinic_assurance_chart[0].Ore.length - 1) {
                                    callback(null, ore);
                                }
                            });
                        } else {
                            callback(null, null);
                        }
                  },
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Report Chart clinic assurance' + err);
                    }

                    res.status(200).json({
                        'prezzi': results.prezzi,
                        'ore': results.ore,
                        'num': results.num
                    });
                });


            }).catch(function(err) {
                return res.status(400).render('clinic_assurance_chart: ' + err);
            });  
       
          
    });
};


exports.clinic_report_assurance_export = function(req, res){
    var json2csv = require('json2csv');

    utility.get_parameters_v1(req, 'event', function(err, parameters) {
        sequelize.query('SELECT clinic_report_assurance_export(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ',\''+
            req.query.type+'\','+
            (req.query.with_paz ? req.query.with_paz : null) +','+
            (req.query.paied ? req.query.paied : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
           
                if (err) {
                    console.log(err);
                    return res.status(500).send('Report Assicurazioni' + err);
                }

                if(datas && datas[0] && datas[0].clinic_report_assurance_export){
                    var fields_array = datas[0].clinic_report_assurance_export[0]['fields'];
                    var fieldNames_array = datas[0].clinic_report_assurance_export[0]['field_names'];
                

                    json2csv({
                        data: datas[0].clinic_report_assurance_export[0]['contacts'],
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=report_assicurazioni.csv'
                        });
                        res.end(csv);
                    });
                }else
                {
                    res.status(404).json({});
                }
                
            
        }).catch(function(err) {
            return res.status(400).render('clinic_report_assurance_export: ' + err);
        }); 
    });
};

exports.clinic_report_assurance = function(req, res) {
    
    if (!req.user.role[0].json_build_object.attributes.invoice_list ) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters_v1(req, 'invoice', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_assurance(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ',\''+
            req.query.type+'\','+
            (req.query.with_paz ? req.query.with_paz : null) +','+
            (req.query.paied ? req.query.paied : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {

             if (datas[0].clinic_report_assurance) {
                res.json(datas[0].clinic_report_assurance);
            } else
                return res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_assurance: ' + err);
        });
    });   
};
