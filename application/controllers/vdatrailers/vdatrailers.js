var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    util = require('util'),
    utility = require('../../utility/utility.js'),
    logs = require('../../utility/logs.js');

exports.check_gare_attive = function(req, res) {
    var request = require('request');
    request({
        headers: {
          'Content-Type': 'application/json'
        },
        uri: 'https://www.100x100trail.com/it/json_gare/all',
        method: 'GET'
    },  function (err, result, body) {
        if(err)
        {   
            console.log(err);
            return res.status(400).render('Drupal Gare attive: ' + err);
        }
        else
        {
            //console.log(body);
            if(utility.isJsonValid(body)){
                var json_body = JSON.parse(body); 

                sequelize.query('SELECT vdatrailers_check_gare(\'' +
                    JSON.stringify(json_body.nodes).replace(/'/g, "''''") + '\',\'' +
                   req.headers['host'].split(".")[0]+'\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas1) {
                    res.status(200).send({});
                }).catch(function(err1) {
                    console.log('ERRORE CHECK GARE VDATRAILERS'+err1);
                    return res.status(400).render('Drupal Gare attive: ' + err);
                });
                
            }else{
                return res.status(404).send({});
            }
        }
    });   
};

exports.check_gara_import_time = function(req, res) {
    var request = require('request');

    sequelize.query('SELECT vdatrailers_check_gara_import_time(\'' +
       req.query.tempo + '\',\'' +
       req.headers['host'].split(".")[0]+'\','+
       req.query.product_id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    }).then(function(datas1) {
        console.log(datas1);
        res.status(200).send(datas1[0].vdatrailers_check_gara_import_time);
    }).catch(function(err1) {
        console.log('ERRORE CHECK GARE IMPORT RUNNER VDATRAILERS'+err1);
        return res.status(400).render('VDATRAILERS IMPORT RUNNER CHECK: ' + err);
    });
          
};

exports.zappa_runners = function(req, res) {
    var request = require('request');

    sequelize.query('SELECT vdatrailers_zappa_runner(\'' +
       req.headers['host'].split(".")[0]+'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    }).then(function(datas1) {
        res.status(200).send(datas1[0].vdatrailers_zappa_runner);
    }).catch(function(err1) {
        console.log('ERRORE ZAPPA RUNNERS'+err1);
        return res.status(400).render('ERRORE ZAPPA RUNNERS: ' + err);
    });
          
};

exports.zappa_tempi = function(req, res) {
    var request = require('request');

    sequelize.query('SELECT vdatrailers_zappa_tempi(\'' +
       req.headers['host'].split(".")[0]+'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    }).then(function(datas1) {
        res.status(200).send(datas1[0].vdatrailers_zappa_tempi);
    }).catch(function(err1) {
        console.log('ERRORE ZAPPA TEMPI'+err1);
        return res.status(400).render('ERRORE ZAPPA TEMPI: ' + err);
    });
          
};

exports.sync_runners = function(req, res) {
    var request = require('request');
    request({
        headers: {
          'Content-Type': 'application/json'
        },
        uri: 'https://www.100x100trail.com/race-server/rest_views/rapporti.json?filters[field_pettorale_value]=&display_id=elenco_iscritti_full&api-key=eBtW5ICWOmPdJP0WH8fxhONheOxlJyxnqcDS6z0kcnTs3mMnoR&format_output=0&items_per_page=All',
        method: 'GET'
    },  function (err, result, body) {
        if(err)
        {   
            console.log(err);
            return res.status(400).render('Drupal Import runner: ' + err);
        }
        else
        {
            //console.log(body);
            if(utility.isJsonValid(body)){
                var json_body = JSON.parse(body); 

                sequelize.query('SELECT vdatrailers_import_runner(\'' +
                JSON.stringify(json_body).replace(/'/g, "''''") + '\',\'' +
                req.headers['host'].split(".")[0]+'\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas1) {
                    res.status(200).send({});
                }).catch(function(err1) {
                    console.log('ERRORE Drupal Import runner VDATRAILERS'+err1);
                    return res.status(400).render('Drupal Import runner: ' + err);
                });
                
            }else{
                return res.status(404).send({});
            }
        }
    });   
};

exports.save_crono = function(req, res) {

    var pettorali = [],
        product_id = null,
        postazione = null,
        tempo = null,
        status = null;

    if (utility.check_id(req.body.gara)) {
        return res.status(422).send(utility.error422('gara', 'Parametro non valido', 'Product id non valido', '422'));
    }else{
        product_id = req.body.gara;
    }

    if (req.body.postazione && req.body.postazione !== undefined) {
        postazione = '\'' + req.body.postazione + '\'';
    }

    if (req.body.status && req.body.status !== undefined) {
        status = req.body.status;
    }

    if (req.body.tempo && req.body.tempo !== undefined) {
        tempo = '\'' + req.body.tempo + '\'';
    }

    if (req.body.pettorali !== undefined && req.body.pettorali != '') {
        if (util.isArray(req.body.pettorali)) {
            req.body.pettorali.forEach(function(end) {
                pettorali.push('\'' + end + '\'');
            });
        }
    }

    
    sequelize.query('SELECT vdatrailers_crono(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        product_id + ',' +
        postazione + ',' +
        (pettorali.length > 0 ? 'ARRAY[' + pettorali + ']::character varying[]' : null) + ',' +
        tempo + ','+
        status+','+
        req.user.id+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

        res.json(datas[0].vdatrailers_crono);

    }).catch(function(err) {
        return res.status(400).render('vdatrailers_crono: ' + err);
    });
    
};

exports.consegna_pettorale = function(req, res) {
    var request = require('request');
    var contact_id = null;

    if (utility.check_id(req.body.contact_id)) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Contact id non valido', '422'));
    }else{
        contact_id = req.body.contact_id;
    }

    sequelize.query('SELECT vdatrailers_pettorale_consegnato(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        contact_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

            if(datas && datas[0].vdatrailers_pettorale_consegnato){
                console.log(datas[0].vdatrailers_pettorale_consegnato);
                 /* CHIAMATA DRUPAL */
                var formData = {
                    'field_consegna_pettorale': {'und':[{'value':'1'}]},
                    'field_data_iscrizione': {'und': [{
                        'value': {
                            'date': '',
                            'time': ''
                        },
                        'timezone': 'Europe/Berlin',
                        'timezone_db': 'UTC',
                        'date_type': 'datetime'
                    }]},
                    'type':'race_link'
                };

                request({
                        headers: {
                          'Content-Type': 'application/json'
                        },
                        uri: 'https://www.100x100trail.com/race-server/race-link/'+datas[0].vdatrailers_pettorale_consegnato+'?api-key=eBtW5ICWOmPdJP0WH8fxhONheOxlJyxnqcDS6z0kcnTs3mMnoR',
                        body: JSON.stringify(formData),
                        method: 'PUT'
                    },  function (err, result, body) {
                        if(err){
                            console.log(err);
                        }
                        else{
                            console.log(body);
                        }
                });

                res.json(datas[0].vdatrailers_pettorale_consegnato);
            }

    }).catch(function(err) {
        return res.status(400).render('vdatrailers_pettorale_consegnato: ' + err);
    });
    
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing user searched by id
 */
exports.elenco_iscritti_gara = function(req, res) {

    if (utility.check_id(req.query.product_id)) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Product id non valido', '422'));
    }

    if (req.query.product_id) {
        sequelize.query('SELECT elenco_iscritti_gara(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.product_id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

            res.json(datas[0].elenco_iscritti_gara);

        }).catch(function(err) {
            return res.status(400).render('user_elenco_iscritti_gara: ' + err);
        });
    } else {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Product id non valido', '422'));
    }
};

exports.classifica_gara = function(req, res) {

    var sesso = null,
        partenza = null,
        categoria = null,
        ris = null;

    if (utility.check_id(req.query.product_id)) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Product id non valido', '422'));
    }

    if (req.query.sesso && req.query.sesso !== undefined) {
        sesso = '\'' + req.query.sesso + '\'';
    }

    if (req.query.categoria && req.query.categoria !== undefined) {
        categoria = '\'' + req.query.categoria + '\'';
    }

    if (req.query.partenza && req.query.partenza !== undefined) {
        partenza = '\'' + req.query.partenza + '\'';
    }

    if(req.query.ris && req.query.ris !== undefined){
        ris = req.query.ris;
    }

    if (req.query.product_id) {
        sequelize.query('SELECT classifica_gara_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.product_id + ',' +
            sesso + ',' +
            categoria + ',' +
            partenza + ','+
            ris+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            }).then(function(datas) {

            res.json(datas[0].classifica_gara_v1);

        }).catch(function(err) {
            return res.status(400).render('user_classifica_gara: ' + err);
        });
    } else {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Product id non valido', '422'));

    }
};

exports.classifica_lite = function(req, res) {

    var sesso = null,
        partenza = null,
        categoria = null,
        ris = null;

    if (utility.check_id(req.query.product_id)) {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Product id non valido', '422'));
    }

    if (req.query.sesso && req.query.sesso !== undefined) {
        sesso = '\'' + req.query.sesso + '\'';
    }

    if (req.query.categoria && req.query.categoria !== undefined) {
        categoria = '\'' + req.query.categoria + '\'';
    }

    if (req.query.partenza && req.query.partenza !== undefined) {
        partenza = '\'' + req.query.partenza + '\'';
    }

    if(req.query.ris && req.query.ris !== undefined){
        ris = req.query.ris;
    }

    if (req.query.product_id) {
        sequelize.query('SELECT classifica_lite(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.product_id + ',' +
            sesso + ',' +
            categoria + ',' +
            partenza + ','+
            ris+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            }).then(function(datas) {

            res.json(datas[0].classifica_lite);

        }).catch(function(err) {
            return res.status(400).render('classifica_lite: ' + err);
        });
    } else {
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Product id non valido', '422'));

    }
};

exports.medtrack_get_trattamenti = function(req, res) {

    if (utility.check_id(req.query.contact_id)) {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Product id non valido', '422'));
    }

    if (req.query.contact_id) {
        sequelize.query('SELECT medtrack_get_trattamenti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.contact_id + ','+
            req.user.id+','+
            req.user.role_id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            }).then(function(datas) {

            res.json(datas[0].medtrack_get_trattamenti[0]);

        }).catch(function(err) {
            return res.status(400).render('medtrack_get_trattamenti: ' + err);
        });
    } else {
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Product id non valido', '422'));
    }
};


