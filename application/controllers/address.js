/** 
 * @apiDefine IdParamEntryError
 *
 * @apiError (Error 422) {422} IdParamEntryError The parameter is not a valid id
 * @apiErrorExample {json} Error-Response:
 * {
 * "errors": [{
 *     "status": '422',
 *     "source": {
 *       "pointer": "/data/attributes/id"
 *     },
 *     "title": "Parametro non valido",
 *     "detail": "Parametro non valido"
 *   }] 
 * }
 */

var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @api{get}/addresses/:id 1 - Request a specific address
 * @apiName FindAddress
 * @apiGroup Addresses
 * @apiVersion 1.0.0
 *
 * @apiParam{Number} id The address id
 *
 * @apiSuccess{Number} id Address id
 * @apiSuccess{String} type Model name
 * @apiSuccess{Object[]} attributes List of address fields
 * @apiSuccess{Number} attributes.id Address id
 * @apiSuccess{String} attributes.organization Company name
 * @apiSuccess{String} attributes.name Address name
 * @apiSuccess{String} attributes.icon Icon for a specific place like airport, restaurants, ecc
 * @apiSuccess{String} attributes.view View field for Here maps
 * @apiSuccess{String} attributes.place_id Place_id field for Here maps
 * @apiSuccess{Number} attributes.location_position_lat Latitude
 * @apiSuccess{Number} attributes.location_position_lng Longitude
 * @apiSuccess{String} attributes.location_address_text Complete address in String format
 * @apiSuccess{String} attributes.location_address_house House number
 * @apiSuccess{String} attributes.location_address_street Street name
 * @apiSuccess{String} attributes.location_address_postal_code Postal Code
 * @apiSuccess{String} attributes.location_address_city City
 * @apiSuccess{String} attributes.location_address_county County
 * @apiSuccess{String} attributes.location_address_state State
 * @apiSuccess{String} attributes.location_address_country Country
 * @apiSuccess{String} attributes.location_address_country_code Country code (for example USA, ITA)
 * @apiSuccess{String} attributes.categories_id Categories id for Here maps
 * @apiSuccess{String} attributes.categories_title Categories title for Here maps
 * @apiSuccess{String} attributes.type Type od Address (home, work)
 * @apiSuccess{Boolean} attributes.is_invoice True if is the invoice address, false otherwise
 *
 * @apiSuccessExample Example data on success
 * {
 *  "data": {
 *    "id": 1,
 *    "type": "addresses",
 *    "attributes": {
 *      "id": 1,
 *      "organization": "test",
 *      "name": "Colosseo",
 *      "icon": "https://download.vcdn.nokia.com/p/d/places2/icons/categories/10.icon",
 *      "view": "https://share.here.com/p/s-Yz1zaWdodHMtbXVzZXVtcztpZD0zODBzcjJ5ay0xYWQ2ZTAxZjRkY2Y0OTUwODBjZGRmNDg3MDYyNDgyYjtsYXQ9NDEuODkwMTM7bG9uPTEyLjQ5MzQ5O249Q29sb3NzZW87bmxhdD00MS44OTAxMztubG9uPTEyLjQ5MzQ5O3BoPSUyQjM5MDYzOTk2NzcwMDtoPTE0Mzc2",
 *      "place_id": "loc-dmVyc2lvbj0xO3RpdGxlPVZpYStSb21hO2xhdD00NS43NDA0ODtsb249Ny4zMzMzOTtzdHJlZXQ9VmlhK1JvbWE7Y2l0eT1Bb3N0YTtwb3N0YWxDb2RlPTExMTAwO2NvdW50cnk9SVRBO3N0YXRlPVZhbGxlK2QlMjdBb3N0YTtjb3VudHk9QW9zdGE7Y2F0ZWdvcnlJZD1zdHJlZXQtc3F1YXJlO3NvdXJjZVN5c3RlbT1pbnRlcm5hbA",
 *      "location_position_lat": 41.89013,
 *      "location_position_lng": 12.49349,
 *      "location_address_text": "Piazza del Colosseo",
 *      "location_address_house": "12/A",
 *      "location_address_street": "Piazza del Colosseo",
 *      "location_address_postal_code": "00184",
 *      "location_address_city": "Roma",
 *      "location_address_county": null,
 *      "location_address_state": "Lazio",
 *      "location_address_country": "Italia",
 *      "location_address_country_code": "ITA",
 *      "categories_id": "sights-museums",
 *      "categories_title": "Luoghi turistici e musei",
 *      "type": "Home",
 *      "is_invoice": true
 *   }
 * },
 *  "included": []
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.find = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_address(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 * 
 * @api{post}/addresses 2 - Insert new address
 * @apiName PostAddress
 * @apiGroup Addresses
 * @apiVersion 1.0.0
 * 
 * @apiParam{Object} data Address attributes in JSON Api Standard (all attributes can be null)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "data":
 *  {
 *    "attributes":
 *    {
 *      "name":"Via Roma",
 *      "icon":"https://download.vcdn.nokia.com/p/d/places2/icons/categories/06.icon",
 *      "view":"https://share.here.com/p/s-YmI9Ny42ODAwMDk4NDE5MTg5NDUlMkM0NS4wNjQxODk5MTA4ODg2NyUyQzcuNjg1MDgwMDUxNDIyMTE5JTJDNDUuMDcwNzI4MzAyMDAxOTU7Yz1zdHJlZXQtc3F1YXJlO2xhdD00NS4wNjc0Njtsb249Ny42ODI1NDtuPVZpYStSb21hO3o9MTc7aD0xMzZmMmE",
 *      "place_id":"loc-dmVyc2lvbj0xO3RpdGxlPVZpYStSb21hO2xhdD00NS4wNjczMztsb249Ny42ODIzO3N0cmVldD1WaWErUm9tYTtjaXR5PVRvcmlubztwb3N0YWxDb2RlPTEwMTI0O2NvdW50cnk9SVRBO3N0YXRlPVBpZW1vbnRlO2NvdW50eT1Ub3Jpbm87Y2F0ZWdvcnlJZD1zdHJlZXQtc3F1YXJlO3NvdXJjZVN5c3RlbT1pbnRlcm5hbA",
 *      "location_position_lat":45.06733,
 *      "location_position_lng":7.6823,
 *      "location_address_text":"Via Roma<br/>10124 Torino<br/>Italia",
 *      "location_address_house":"121",
 *      "location_address_street":"Via Roma",
 *      "location_address_postal_code":"10124",
 *      "location_address_city":"Torino",
 *      "location_address_county":"Torino",
 *      "location_address_state":"Piemonte",
 *      "location_address_country":"Italia",
 *      "location_address_country_code":"ITA",
 *      "categories_id":"street-square",
 *      "categories_title":"Via o piazza",
 *      "type": "Office",
 *      "is_invoice":false
 *    },
 *  "type":"addresses"
 *  }
 *}
 *
 * @apiSuccess {json} data Address in JSON Api Standard with id
 * @apiSuccessExample {json} Success-Response:
 * {
 * "data":
 * {
 *   "attributes":
 *     {
 *       "name":"Via Roma",
 *       "icon":"https://download.vcdn.nokia.com/p/d/places2/icons/categories/06.icon",
 *       "view":"https://share.here.com/p/s-YmI9Ny42ODAwMDk4NDE5MTg5NDUlMkM0NS4wNjQxODk5MTA4ODg2NyUyQzcuNjg1MDgwMDUxNDIyMTE5JTJDNDUuMDcwNzI4MzAyMDAxOTU7Yz1zdHJlZXQtc3F1YXJlO2xhdD00NS4wNjc0Njtsb249Ny42ODI1NDtuPVZpYStSb21hO3o9MTc7aD0xMzZmMmE",
 *       "place_id":"loc-dmVyc2lvbj0xO3RpdGxlPVZpYStSb21hO2xhdD00NS4wNjczMztsb249Ny42ODIzO3N0cmVldD1WaWErUm9tYTtjaXR5PVRvcmlubztwb3N0YWxDb2RlPTEwMTI0O2NvdW50cnk9SVRBO3N0YXRlPVBpZW1vbnRlO2NvdW50eT1Ub3Jpbm87Y2F0ZWdvcnlJZD1zdHJlZXQtc3F1YXJlO3NvdXJjZVN5c3RlbT1pbnRlcm5hbA",
 *       "location_position_lat":45.06733,
 *       "location_position_lng":7.6823,
 *       "location_address_text":"Via Roma<br/>10124 Torino<br/>Italia",
 *       "location_address_house":"121",
 *       "location_address_street":"Via Roma",
 *       "location_address_postal_code":"10124",
 *       "location_address_city":"Torino",
 *       "location_address_county":"Torino",
 *       "location_address_state":"Piemonte",
 *       "location_address_country":"Italia",
 *       "location_address_country_code":"ITA",
 *       "categories_id":"street-square",
 *       "categories_title":"Via o piazza",
 *       "type":"Office",
 *       "is_invoice":false
 *     },
 *   "type":"addresses",
 *   "id": "213"
 *   }
 * }
 *
 */
exports.create = function(req, res) {

    var address = {};

    if (req.body.data.attributes !== undefined) {

        address['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') ? req.body.data.attributes.name.replace(/'/g, "''''") : null);
        address['icon'] = (req.body.data.attributes.icon && utility.check_type_variable(req.body.data.attributes.icon, 'string') ? req.body.data.attributes.icon.replace(/'/g, "''''") : null);
        address['view'] = (req.body.data.attributes.view && utility.check_type_variable(req.body.data.attributes.view, 'string') ? req.body.data.attributes.view.replace(/'/g, "''''") : null);
        address['place_id'] = (req.body.data.attributes.place_id && utility.check_type_variable(req.body.data.attributes.place_id, 'string') ? req.body.data.attributes.place_id.replace(/'/g, "''''") : null);
        address['location_position_lat'] = req.body.data.attributes.location_position_lat;
        address['location_position_lng'] = req.body.data.attributes.location_position_lng;
        address['location_address_text'] = (req.body.data.attributes.location_address_text && utility.check_type_variable(req.body.data.attributes.location_address_text, 'string') ? req.body.data.attributes.location_address_text.replace(/'/g, "''''") : null);
        address['location_address_house'] = (req.body.data.attributes.location_address_house && utility.check_type_variable(req.body.data.attributes.location_address_house, 'string') ? req.body.data.attributes.location_address_house.replace(/'/g, "''''") : null);
        address['location_address_street'] = (req.body.data.attributes.location_address_street && utility.check_type_variable(req.body.data.attributes.location_address_street, 'string') ? req.body.data.attributes.location_address_street.replace(/'/g, "''''") : null);
        address['location_address_postal_code'] = (req.body.data.attributes.location_address_postal_code && utility.check_type_variable(req.body.data.attributes.location_address_postal_code, 'string') ? req.body.data.attributes.location_address_postal_code.replace(/'/g, "''''") : null);
        address['location_address_city'] = (req.body.data.attributes.location_address_city && utility.check_type_variable(req.body.data.attributes.location_address_city, 'string') ? req.body.data.attributes.location_address_city.replace(/'/g, "''''") : null);
        address['location_address_county'] = (req.body.data.attributes.location_address_county && utility.check_type_variable(req.body.data.attributes.location_address_county, 'string') ? req.body.data.attributes.location_address_county.replace(/'/g, "''''") : null);
        address['location_address_state'] = (req.body.data.attributes.location_address_state && utility.check_type_variable(req.body.data.attributes.state, 'string') ? req.body.data.attributes.location_address_state.replace(/'/g, "''''") : null);
        address['location_address_country'] = (req.body.data.attributes.location_address_country && utility.check_type_variable(req.body.data.attributes.location_address_country, 'string') ? req.body.data.attributes.location_address_country.replace(/'/g, "''''") : null);
        address['location_address_country_code'] = (req.body.data.attributes.location_address_country_code && utility.check_type_variable(req.body.data.attributes.location_address_country_code, 'string') ? req.body.data.attributes.location_address_country_code.replace(/'/g, "''''") : null);
        address['categories_id'] = (req.body.data.attributes.categories_id && utility.check_type_variable(req.body.data.attributes.categories_id, 'string') ? req.body.data.attributes.categories_id.replace(/'/g, "''''") : null);
        address['categories_title'] = (req.body.data.attributes.categories_title && utility.check_type_variable(req.body.data.attributes.categories_title, 'string') ? req.body.data.attributes.categories_title.replace(/'/g, "''''") : null);
        address['type'] = (req.body.data.attributes.type && utility.check_type_variable(req.body.data.attributes.type, 'string') ? req.body.data.attributes.type.replace(/'/g, "''''") : null);
        address['is_invoice'] = req.body.data.attributes.is_invoice;
        address['accuracy'] = (req.body.data.attributes.accuracy ? req.body.data.attributes.accuracy : null);
        address['heading'] = (req.body.data.attributes.heading ? req.body.data.attributes.heading : null);
        address['_tenant_id'] = req.user._tenant_id;
        address['organization'] = req.headers['host'].split(".")[0];
        address['mongo_id'] = null;

        sequelize.query('SELECT insert_address(\'' + JSON.stringify(address) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                req.body.data.id = datas[0].insert_address;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'address',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_address,
                    'Aggiunto indirizzo ' + datas[0].insert_address
                );

                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('address_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @api{patch}/addresses/:id 3 - Update specific address
 * @apiName PatchAddress
 * @apiGroup Addresses
 * @apiVersion 1.0.0
 * 
 * @apiParam{Object} data Address attributes in JSON Api Standard (all attributes can be null)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *  "data":
 *  {
 *    "attributes":
 *    {
 *      "name":"Via Roma",
 *      "icon":"https://download.vcdn.nokia.com/p/d/places2/icons/categories/06.icon",
 *      "view":"https://share.here.com/p/s-YmI9Ny42ODAwMDk4NDE5MTg5NDUlMkM0NS4wNjQxODk5MTA4ODg2NyUyQzcuNjg1MDgwMDUxNDIyMTE5JTJDNDUuMDcwNzI4MzAyMDAxOTU7Yz1zdHJlZXQtc3F1YXJlO2xhdD00NS4wNjc0Njtsb249Ny42ODI1NDtuPVZpYStSb21hO3o9MTc7aD0xMzZmMmE",
 *      "place_id":"loc-dmVyc2lvbj0xO3RpdGxlPVZpYStSb21hO2xhdD00NS4wNjczMztsb249Ny42ODIzO3N0cmVldD1WaWErUm9tYTtjaXR5PVRvcmlubztwb3N0YWxDb2RlPTEwMTI0O2NvdW50cnk9SVRBO3N0YXRlPVBpZW1vbnRlO2NvdW50eT1Ub3Jpbm87Y2F0ZWdvcnlJZD1zdHJlZXQtc3F1YXJlO3NvdXJjZVN5c3RlbT1pbnRlcm5hbA",
 *      "location_position_lat":45.06733,
 *      "location_position_lng":7.6823,
 *      "location_address_text":"Via Roma<br/>10124 Torino<br/>Italia",
 *      "location_address_house":"121",
 *      "location_address_street":"Via Roma",
 *      "location_address_postal_code":"10124",
 *      "location_address_city":"Torino",
 *      "location_address_county":"Torino",
 *      "location_address_state":"Piemonte",
 *      "location_address_country":"Italia",
 *      "location_address_country_code":"ITA",
 *      "categories_id":"street-square",
 *      "categories_title":"Via o piazza",
 *      "type": "Office",
 *      "is_invoice":false
 *    },
 *    "type":"addresses",
 *    "id": "213"
 *  }
 *}
 *
 * @apiSuccess {json} data Address in JSON Api Standard with new fields value
 * @apiSuccessExample {json} Success-Response:
 * {
 * "data":
 * {
 *   "attributes":
 *     {
 *       "name":"Via Roma",
 *       "icon":"https://download.vcdn.nokia.com/p/d/places2/icons/categories/06.icon",
 *       "view":"https://share.here.com/p/s-YmI9Ny42ODAwMDk4NDE5MTg5NDUlMkM0NS4wNjQxODk5MTA4ODg2NyUyQzcuNjg1MDgwMDUxNDIyMTE5JTJDNDUuMDcwNzI4MzAyMDAxOTU7Yz1zdHJlZXQtc3F1YXJlO2xhdD00NS4wNjc0Njtsb249Ny42ODI1NDtuPVZpYStSb21hO3o9MTc7aD0xMzZmMmE",
 *       "place_id":"loc-dmVyc2lvbj0xO3RpdGxlPVZpYStSb21hO2xhdD00NS4wNjczMztsb249Ny42ODIzO3N0cmVldD1WaWErUm9tYTtjaXR5PVRvcmlubztwb3N0YWxDb2RlPTEwMTI0O2NvdW50cnk9SVRBO3N0YXRlPVBpZW1vbnRlO2NvdW50eT1Ub3Jpbm87Y2F0ZWdvcnlJZD1zdHJlZXQtc3F1YXJlO3NvdXJjZVN5c3RlbT1pbnRlcm5hbA",
 *       "location_position_lat":45.06733,
 *       "location_position_lng":7.6823,
 *       "location_address_text":"Via Roma<br/>10124 Torino<br/>Italia",
 *       "location_address_house":"121",
 *       "location_address_street":"Via Roma",
 *       "location_address_postal_code":"10124",
 *       "location_address_city":"Torino",
 *       "location_address_county":"Torino",
 *       "location_address_state":"Piemonte",
 *       "location_address_country":"Italia",
 *       "location_address_country_code":"ITA",
 *       "categories_id":"street-square",
 *       "categories_title":"Via o piazza",
 *       "type":"Office",
 *       "is_invoice":false
 *     },
 *     "type":"addresses",
 *     "id": "213"
 *   }
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.update = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes !== undefined) {

        sequelize.query('SELECT update_address_v1(' +
                req.params.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',\'' +
                (req.body.data.attributes.name && req.body.data.attributes.name != null && utility.check_type_variable(req.body.data.attributes.name, 'string') ? req.body.data.attributes.name.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.icon && req.body.data.attributes.icon != null && utility.check_type_variable(req.body.data.attributes.icon, 'string') ? req.body.data.attributes.icon.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.view && req.body.data.attributes.view != null && utility.check_type_variable(req.body.data.attributes.view, 'string') ? req.body.data.attributes.view.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.place_id && req.body.data.attributes.place_id != null && utility.check_type_variable(req.body.data.attributes.place_id, 'string') ? req.body.data.attributes.place_id.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_text && req.body.data.attributes.location_address_text != null && utility.check_type_variable(req.body.data.attributes.location_address_text, 'string') ? req.body.data.attributes.location_address_text.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_house && req.body.data.attributes.location_address_house != null && utility.check_type_variable(req.body.data.attributes.location_address_house, 'string') ? req.body.data.attributes.location_address_house.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_street && req.body.data.attributes.location_address_street != null && utility.check_type_variable(req.body.data.attributes.location_address_street, 'string') ? req.body.data.attributes.location_address_street.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_postal_code && req.body.data.attributes.location_address_postal_code != null && utility.check_type_variable(req.body.data.attributes.location_address_postal_code, 'string') ? req.body.data.attributes.location_address_postal_code.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_city && req.body.data.attributes.location_address_city != null && utility.check_type_variable(req.body.data.attributes.location_address_city, 'string') ? req.body.data.attributes.location_address_city.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_county && req.body.data.attributes.location_address_county != null && utility.check_type_variable(req.body.data.attributes.location_address_county, 'string') ? req.body.data.attributes.location_address_county.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_state && req.body.data.attributes.location_address_state != null && utility.check_type_variable(req.body.data.attributes.location_address_state, 'string') ? req.body.data.attributes.location_address_state.replace(/'/g, "''''") : '') + '\',' +
                req.body.data.attributes.location_position_lat + ',' +
                req.body.data.attributes.location_position_lng + ',\'' +
                (req.body.data.attributes.location_address_country && req.body.data.attributes.location_address_country != null && utility.check_type_variable(req.body.data.attributes.location_address_country, 'string') ? req.body.data.attributes.location_address_country.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.location_address_country_code && req.body.data.attributes.location_address_country_code != null && utility.check_type_variable(req.body.data.attributes.location_address_country_code, 'string') ? req.body.data.attributes.location_address_country_code.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.categories_id && req.body.data.attributes.categories_id != null && utility.check_type_variable(req.body.data.attributes.categories_id, 'string') ? req.body.data.attributes.categories_id.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.categories_title && req.body.data.attributes.categories_title != null && utility.check_type_variable(req.body.data.attributes.categories_title, 'string') ? req.body.data.attributes.categories_title.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.type && req.body.data.attributes.type != null && utility.check_type_variable(req.body.data.attributes.type, 'string') ? req.body.data.attributes.type.replace(/'/g, "''''") : '') + '\',' +
                req.body.data.attributes.is_invoice + ',' +
                (req.body.data.attributes.accuracy ? req.body.data.attributes.accuracy : null) + ',\'' +
                (req.body.data.attributes.heading && req.body.data.attributes.heading != null && utility.check_type_variable(req.body.data.attributes.heading, 'string') ? req.body.data.attributes.heading.replace(/'/g, "''''") : '') + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'address',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato indirizzo ' + req.params.id
                );
                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('address_update: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @api{delete}/addresses/:id 4 - Delete a specific address
 * @apiName DeleteAddress
 * @apiGroup Addresses
 * @apiVersion 1.0.0
 *
 * @apiParam{Number} id The address id
 *
 * @apiSuccess{Obect[]} data 
 * @apiSuccess{Number} data.id Address id
 * @apiSuccess{String} data.type Model name
 * @apiSuccessExample Example data on success
 * {
 *  "data":
 *  {
 *    "type":"addresses",
 *    "id":"213"
 *  }
 * }
 *
 * @apiUse IdParamEntryError
 */
exports.destroy = function(req, res) {

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT delete_address_v2(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'addresses',
                    'id': req.params.id
                }
            });

        }).catch(function(err) {
            return res.status(400).render('address_destroy: ' + err);
        });
};

exports.addresses_cluster = function(req, res) {

    sequelize.query('SELECT addresses_cluster(\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json(datas);

        }).catch(function(err) {
            return res.status(400).render('addresses_cluster: ' + err);
        });
};
