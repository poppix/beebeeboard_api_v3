/**
 * @apiDefine IdParamEntryError
 *
 * @apiError (Error 422) {422} IdParamEntryError The parameter is not a valid id
 * @apiErrorExample {json} Error-Response:
 * {
 * "errors": [{
 *     "status": '422',
 *     "source": {
 *       "pointer": "/data/attributes/id"
 *     },
 *     "title": "Parametro non valido",
 *     "detail": "Parametro non valido"
 *   }] 
 * }
 */
  
var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    export_datas = require('../utility/exports.js'),
    indexes = require('../utility/indexes.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js'),
    bcrypt = require('bcryptjs'),
        config = require('../../config'),
        utils = require('../../utils'),
        SALT_WORK_FACTOR = 10;

exports.stats = function(req, res) {

    sequelize.query('SELECT contacts_stats_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.role_id + ');')
        .then(function(datas) {
            res.json({
                'meta': datas[0][0].contacts_stats_v1
            });
        }).catch(function(err) {
            return res.status(400).render('contact_stats: ' + err);
        });
};

exports.export_data = function(req, res) {
    var json2csv = require('json2csv');

    if (!req.user.role[0].json_build_object.attributes.contact_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('contact', req, function(err, results) {
        if (err) {
            return res.status(400).render('contact_custom: ' + err);
        }
        indexes.contact(req, results, function(err, index) {
            if (err) {
                return res.status(400).render('contact_index: ' + err);
            }

            /*if (index.meta.total_items > 1000) {
                return res.status(422).send('La quantità di dati che si sta cercando di esportare supera i limiti consensiti. Ti chiediamo di contattare l\'amministratore di sistema per ottenere i tuoi dati in formato csv.');
            } else {*/
                export_datas.contact(req, results, function(err, response) {
                    if (err) {
                        return res.status(400).render('contact_index: ' + err);
                    }


                    json2csv({
                        data: response.contacts,
                        fields: response.fields,
                        fieldNames: response.field_names
                    }, function(err, csv) {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=contacts.csv'
                        });
                        res.end(csv);
                    });
                });
           // }
        });

    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
   
    if (!req.user.role[0].json_build_object.attributes.contact_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.customs('contact', req, function(err, results) {
        if (err) {
            return res.status(400).render('contact_custom: ' + err);
        }
        indexes.contact(req, results, function(err, response) {
            if (err) {
                return res.status(400).render('contact_index: ' + err);
            }
            res.json(response);
        });

    });
};


/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Find a specific contact searched by id
 */
exports.find = function(req, res) {
    

    if (!req.user.role[0].json_build_object.attributes.contact_view) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    finds.find_contact(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var re = /^(([^<>()[\]\\.,;':\s@\"]+(\.[^<>()[\]\\.,;':\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var contact = {};

    if (req.body.data.attributes !== undefined) {


        if (req.body.data.attributes.mail && req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {

            if (!utility.check_type_variable(req.body.data.attributes.mail, 'string')) {
                return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }

            if (re.test(req.body.data.attributes.mail) != true) {
                return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
            }
        }
        if ((!req.body.data.attributes.first_name || req.body.data.attributes.first_name == '' || req.body.data.attributes.first_name == undefined || req.body.data.attributes.first_name == null || !utility.check_type_variable(req.body.data.attributes.first_name, 'string')) && 
            (!req.body.data.attributes.last_name || req.body.data.attributes.last_name == '' || req.body.data.attributes.last_name == undefined || req.body.data.attributes.last_name == null || !utility.check_type_variable(req.body.data.attributes.last_name, 'string')) && 
            (!req.body.data.attributes.ragione_sociale || req.body.data.attributes.ragione_sociale == '' || req.body.data.attributes.ragione_sociale == undefined || req.body.data.attributes.ragione_sociale == null || !utility.check_type_variable(req.body.data.attributes.ragione_sociale, 'string'))) {
            return res.status(422).send(utility.error422('first_name', 'Invalid attribute', 'Inserire un nome per il contatto', '422'));
        }

        if (req.body.data.attributes.codice_destinatario && req.body.data.attributes.codice_destinatario !== undefined) {
            if(req.body.data.attributes.codice_destinatario.length != 7 && req.body.data.attributes.codice_destinatario.length != 6){
                return res.status(422).send(utility.error422('codice_destinatario', 'Invalid attribute', 'Inserire un codice destinatario di 7 caratteri', '422'));
            }
        }

        async.parallel({
            'mail': function(callback) {
                if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
                    sequelize.query('SELECT check_mail_v1(\'' +
                        req.headers['host'].split(".")[0] + '\',\'' +
                        req.body.data.attributes.mail + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(mails) {
                        console.log(mails[0].check_mail_v1);
                        if (mails[0].check_mail_v1) {
                            callback(null, 0);
                        } else {
                            callback(null, 1);
                        }
                    }).catch(function(err) {
                        callback(err, null);
                    });
                } else {
                    callback(null, 1);
                }
            }
        }, function(err, resul) {
            if (err) {
                return res.status(400).render('contact_find: ' + err);
            }

            if (resul.mail == 0) {
                return res.status(422).send(utility.error422('mail', 'Mail già esistente', 'Mail già esistente', '422'));
            }

            contact['owner_id'] = req.user.id;
            contact['name'] = ((req.body.data.attributes.first_name && req.body.data.attributes.first_name !== null && utility.check_type_variable(req.body.data.attributes.first_name, 'string')) || (req.body.data.attributes.last_name && req.body.data.attributes.last_name !== null && utility.check_type_variable(req.body.data.attributes.last_name, 'string')) ? (req.body.data.attributes.first_name ? req.body.data.attributes.first_name.replace(/'/g, "''''"):'') + ' ' + (req.body.data.attributes.last_name ? req.body.data.attributes.last_name.replace(/'/g, "''''") : '') : '');
            contact['mail'] = (req.body.data.attributes.mail && req.body.data.attributes.mail !== null && utility.check_type_variable(req.body.data.attributes.mail, 'string') ? req.body.data.attributes.mail.replace(/'/g, "''''") : '');
            contact['contact'] = (req.body.data.attributes.contact && req.body.data.attributes.contact !== null && utility.check_type_variable(req.body.data.attributes.contact, 'string') ? req.body.data.attributes.contact.replace(/'/g, "''''") : '');
            contact['telephone'] = (req.body.data.attributes.telephone && req.body.data.attributes.telephone !== null && utility.check_type_variable(req.body.data.attributes.telephone, 'string') ? req.body.data.attributes.telephone.replace(/'/g, "''''") : '');
            contact['mobile'] = (req.body.data.attributes.mobile && req.body.data.attributes.mobile !== null && utility.check_type_variable(req.body.data.attributes.mobile, 'string') ? req.body.data.attributes.mobile.replace(/'/g, "''''") : '');
            contact['fax'] = (req.body.data.attributes.fax && req.body.data.attributes.fax !== null && utility.check_type_variable(req.body.data.attributes.fax, 'string') ? req.body.data.attributes.fax.replace(/'/g, "''''") : '');
            contact['web_site'] = (req.body.data.attributes.web_site && req.body.data.attributes.web_site !== null && utility.check_type_variable(req.body.data.attributes.web_site, 'string') ? req.body.data.attributes.web_site.replace(/'/g, "''''") : '');
            contact['terms'] = (req.body.data.attributes.terms && req.body.data.attributes.terms !== null && utility.check_type_variable(req.body.data.attributes.terms, 'string') ? req.body.data.attributes.terms.replace(/'/g, "''''") : '');
            contact['piva'] = (req.body.data.attributes.piva && req.body.data.attributes.piva !== null && utility.check_type_variable(req.body.data.attributes.piva, 'string') ? req.body.data.attributes.piva.replace(/'/g, "''''") : '');
            contact['set_event_color'] = ((req.body.data.attributes.set_event_color !== '' && req.body.data.attributes.set_event_color !== null && req.body.data.attributes.set_event_color !== 'null' && utility.check_type_variable(req.body.data.attributes.set_event_color, 'string')) ? req.body.data.attributes.set_event_color : null);
            contact['color'] = utility.check_type_variable(req.body.data.attributes.color, 'string') ? req.body.data.attributes.color : '';
            contact['notes'] = (req.body.data.attributes.notes && req.body.data.attributes.notes !== null && utility.check_type_variable(req.body.data.attributes.notes, 'string') ? req.body.data.attributes.notes.replace(/'/g, "''''") : '');
            contact['thumbnail_url'] = utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') ? req.body.data.attributes.thumbnail_url : '';
            contact['thumbnail_name'] = utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') ? req.body.data.attributes.thumbnail_name : '';
            contact['thumbnail_etag'] = utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') ? req.body.data.attributes.thumbnail_etag : '';
            contact['thumbnail_type'] = utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') ? req.body.data.attributes.thumbnail_type : '';
            contact['_tenant_id'] = req.user._tenant_id;
            contact['organization'] = req.headers['host'].split(".")[0];
            contact['socket_room'] = req.headers['host'].split(".")[0];
            contact['mongo_id'] = null;
            contact['archivied'] = req.body.data.attributes.archivied;
            contact['contact_status_id'] = ((req.body.data.relationships && req.body.data.relationships.contact_state && req.body.data.relationships.contact_state.data && req.body.data.relationships.contact_state.data.id && !utility.check_id(req.body.data.relationships.contact_state.data.id)) ? req.body.data.relationships.contact_state.data.id : null);
            contact['piva_name'] = (req.body.data.attributes.piva && req.body.data.attributes.piva !== null && utility.check_type_variable(req.body.data.attributes.piva, 'string') ? req.body.data.attributes.piva.replace(/'/g, "''''") : '');
            contact['first_name'] = (req.body.data.attributes.first_name && req.body.data.attributes.first_name !== null && utility.check_type_variable(req.body.data.attributes.first_name, 'string') ? req.body.data.attributes.first_name.replace(/'/g, "''''") : '');
            contact['last_name'] = (req.body.data.attributes.first_name && req.body.data.attributes.last_name !== null && utility.check_type_variable(req.body.data.attributes.last_name, 'string') ? req.body.data.attributes.last_name.replace(/'/g, "''''") : '');
            contact['sesso'] = req.body.data.attributes.sesso;
            contact['data_nascita'] = req.body.data.attributes.data_nascita;
            contact['luogo_nascita'] = (req.body.data.attributes.luogo_nascita && req.body.data.attributes.luogo_nascita !== null && utility.check_type_variable(req.body.data.attributes.luogo_nascita, 'string') ? req.body.data.attributes.luogo_nascita.replace(/'/g, "''''") : '');
            contact['ragione_sociale'] = (req.body.data.attributes.ragione_sociale && req.body.data.attributes.ragione_sociale !== null && utility.check_type_variable(req.body.data.attributes.ragione_sociale, 'string') ? req.body.data.attributes.ragione_sociale.replace(/'/g, "''''") : '');
            contact['cod_fiscale'] = (req.body.data.attributes.cod_fiscale && req.body.data.attributes.cod_fiscale !== null && utility.check_type_variable(req.body.data.attributes.cod_fiscale, 'string') ? req.body.data.attributes.cod_fiscale.replace(/'/g, "''''") : '');
            contact['codice_destinatario'] = (req.body.data.attributes.codice_destinatario && req.body.data.attributes.codice_destinatario !== null && utility.check_type_variable(req.body.data.attributes.codice_destinatario, 'string') ? req.body.data.attributes.codice_destinatario.replace(/'/g, "''''") : '');
            contact['fisica_giuridica'] = req.body.data.attributes.fisica_giuridica;
            contact['pec'] = (req.body.data.attributes.pec && req.body.data.attributes.pec !== null && utility.check_type_variable(req.body.data.attributes.pec, 'string') ? req.body.data.attributes.pec.replace(/'/g, "''''") : '');
            contact['ente_pa'] = req.body.data.attributes.ente_pa;
            

            sequelize.query('SELECT insert_contact_v5(\'' +
                    JSON.stringify(contact) + '\',' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {

                    if (datas[0].insert_contact_v5 && datas[0].insert_contact_v5 != null) {
                        req.body.data.id = datas[0].insert_contact_v5;

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            'user_id': req.user.id,
                            'new_id': datas[0].insert_contact_v5,
                            'body': req.body.data.relationships,
                            '_tenant': req.user._tenant_id,
                            'model': 'contact',
                            'array': ['event', 'product', 'project', 'address', 'payment', 'custom_field', 'related_contact']
                        }, function(err, results) {

                            //req.body.data.attributes.total_files_number = results.file;
                            req.body.data.attributes.total_products_number = results.product;
                            req.body.data.attributes.total_projects_number = results.project;
                            req.body.data.attributes.total_payments = results.payment;

                            sequelize.query('SELECT contact_custom_fields_trigger(' +
                                    datas[0].insert_contact_v5 + ',' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\');', {
                                        raw: true,
                                        type: Sequelize.QueryTypes.SELECT,
                                        useMaster: true
                                    })
                                .then(function(cus) {
                                    req.params.id = datas[0].insert_contact_v5;
                                    finds.find_contact(req, function(err, results) {
                                        logs.save_log_model(
                                            req.user._tenant_id,
                                            req.headers['host'].split(".")[0],
                                            'contact',
                                            'insert',
                                            req.user.id,
                                            JSON.stringify(req.body.data.attributes),
                                            datas[0].insert_contact_v5,
                                            'Aggiunto contatto ' + datas[0].insert_contact_v5
                                        );
                                        utility.save_socket(req.headers['host'].split(".")[0], 'contact', 'insert', req.user.id, datas[0].insert_contact_v5, results.data);
                        
                                        res.json(results);
                                    });

                                }).catch(function(err) {
                                    return res.status(400).render('contact_create: ' + err);
                                });
                        });

                    } else {
                        res.json({
                            'data': [],
                            'relationships': []
                        });
                    }
                }).catch(function(err) {
                    return res.status(400).render('contact_find: ' + err);
                });
        });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Update an existing contact searched by id
 * When a contact change we must change also all embedded documents related
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var re = /^(([^<>()[\]\\.,';:\s@\"]+(\.[^<>()[\]\\.,';:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var contact = {};

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.body.data.attributes.mail != '' && req.body.data.attributes.mail != undefined) {
        /*if (re.test(req.body.data.attributes.mail) != true) {
            return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
        }*/
        if (!utility.check_type_variable(req.body.data.attributes.mail, 'string')) {
            return res.status(422).send(utility.error422('mail', 'Invalid attribute', 'Inserire una mail valida', '422'));
        }
    }

    if ((
            req.body.data.attributes.first_name == '' || 
            req.body.data.attributes.first_name == undefined || 
            req.body.data.attributes.first_name == null || 
            !utility.check_type_variable(req.body.data.attributes.first_name, 'string')
        ) &&
        (
            req.body.data.attributes.last_name == '' || 
            req.body.data.attributes.last_name == undefined || 
            req.body.data.attributes.last_name == null || 
            !utility.check_type_variable(req.body.data.attributes.last_name, 'string')
        ) &&
        (
            req.body.data.attributes.ragione_sociale == '' || 
            req.body.data.attributes.ragione_sociale == undefined || 
            req.body.data.attributes.ragione_sociale == null || 
            !utility.check_type_variable(req.body.data.attributes.ragione_sociale, 'string')
        )
    ) {
        return res.status(422).send(utility.error422('first_name last_name ragione_sociale', 'Invalid attribute', 'Inserire un nome per il contatto', '422'));
    }

    if (req.body.data.attributes.codice_destinatario && req.body.data.attributes.codice_destinatario !== undefined) {
        if(req.body.data.attributes.codice_destinatario.length != 7 && req.body.data.attributes.codice_destinatario.length != 6){
            return res.status(422).send(utility.error422('codice_destinatario', 'Invalid attribute', 'Inserire un codice destinatario di 7 caratteri', '422'));
        }
    }

    finds.find_contact(req, function(err, old_contact) {

        sequelize.query('SELECT select_user_v1(' + req.params.id + ',' + req.user._tenant_id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {


            async.parallel({
                'mail': function(callback) {
                    if (req.body.data.attributes.mail) {
                        sequelize.query('SELECT check_mail_contact(\'' +
                            req.headers['host'].split(".")[0] + '\',\'' +
                            req.body.data.attributes.mail + '\','+
                            req.params.id+');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            }).then(function(mails) {
                            if (mails[0].check_mail_v1) {
                                callback(null, mails[0].check_mail_v1);

                            } else {
                                callback(null, 0);
                            }
                        }).catch(function(err) {
                            callback(err, null);
                        });
                    } else {
                        callback(null, 0);
                    }

                }
            }, function(err, resul) {
                if (err) {
                    return res.status(400).render('contact_find: ' + err);
                }

                if (resul.mail != 0 && resul.mail != req.params.id) {
                    return res.status(422).send(utility.error422('mail', 'Mail già esistente', 'Mail già esistente', '422'));
                }

                var old = old_contact;

                sequelize.query('SELECT update_contact_v6(' +
                        req.params.id + ',' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        req.user.id + ',\'' +
                        (
                            (
                                req.body.data.attributes.first_name != '' && 
                                req.body.data.attributes.first_name != undefined && 
                                req.body.data.attributes.first_name != null && 
                                utility.check_type_variable(req.body.data.attributes.first_name, 'string')
                            ) ? req.body.data.attributes.first_name.replace(/'/g, "''''") : ''

                        ) +' ' +
                        (
                            (
                                req.body.data.attributes.last_name != '' && 
                                req.body.data.attributes.last_name != undefined && 
                                req.body.data.attributes.last_name != null && 
                                utility.check_type_variable(req.body.data.attributes.last_name, 'string')
                            ) ? req.body.data.attributes.last_name.replace(/'/g, "''''") : ''

                        ) + '\',\'' +
                        (req.body.data.attributes.mail && req.body.data.attributes.mail !== null && utility.check_type_variable(req.body.data.attributes.mail, 'string') ? req.body.data.attributes.mail.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.contact && req.body.data.attributes.contact !== null && utility.check_type_variable(req.body.data.attributes.contact, 'string') ? req.body.data.attributes.contact.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.telephone && req.body.data.attributes.telephone !== null && utility.check_type_variable(req.body.data.attributes.telephone, 'string') ? req.body.data.attributes.telephone.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.mobile && req.body.data.attributes.mobile !== null && utility.check_type_variable(req.body.data.attributes.mobile, 'string') ? req.body.data.attributes.mobile.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.fax && req.body.data.attributes.fax !== null && utility.check_type_variable(req.body.data.attributes.fax, 'string') ? req.body.data.attributes.fax.replace(/'/g, "''''").replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.web_site && req.body.data.attributes.web_site !== null && utility.check_type_variable(req.body.data.attributes.web_site, 'string') ? req.body.data.attributes.web_site.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.terms && req.body.data.attributes.terms !== null && utility.check_type_variable(req.body.data.attributes.terms, 'string') ? req.body.data.attributes.terms.replace(/'/g, "''''").replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.piva && req.body.data.attributes.piva !== null && utility.check_type_variable(req.body.data.attributes.piva, 'string') ? req.body.data.attributes.piva.replace(/'/g, "''''").replace(/'/g, "''''") : '') + '\',' +
                        (req.body.data.attributes.set_event_color !== undefined && utility.check_type_variable(req.body.data.attributes.set_event_color, 'boolean') ? req.body.data.attributes.set_event_color : null) + ',\'' +
                        (req.body.data.attributes.color && req.body.data.attributes.color !== null && utility.check_type_variable(req.body.data.attributes.color, 'string') ? req.body.data.attributes.color.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.notes && req.body.data.attributes.notes !== null && utility.check_type_variable(req.body.data.attributes.notes, 'string') ? req.body.data.attributes.notes.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.thumbnail_url && req.body.data.attributes.thumbnail_url !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_url, 'string') ? req.body.data.attributes.thumbnail_url.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.thumbnail_name && req.body.data.attributes.thumbnail_name !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_name, 'string') ? req.body.data.attributes.thumbnail_name.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.thumbnail_etag && req.body.data.attributes.thumbnail_etag !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_etag, 'string') ? req.body.data.attributes.thumbnail_etag.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.thumbnail_type && req.body.data.attributes.thumbnail_type !== null && utility.check_type_variable(req.body.data.attributes.thumbnail_type, 'string') ? req.body.data.attributes.thumbnail_type.replace(/'/g, "''''") : '') + '\',' +
                        ((req.body.data.relationships.contact_state && req.body.data.relationships.contact_state.data && req.body.data.relationships.contact_state.data.id) ? req.body.data.relationships.contact_state.data.id : null) + ',' +
                        ((req.body.data.relationships.role && req.body.data.relationships.role.data && req.body.data.relationships.role.data.id) ? req.body.data.relationships.role.data.id : null) + ',\'' +
                        (req.body.data.attributes.from_mail_message && req.body.data.attributes.from_mail_message !== null && utility.check_type_variable(req.body.data.attributes.from_mail_message, 'string') ? req.body.data.attributes.from_mail_message.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.mail_message && req.body.data.attributes.mail_message !== null && utility.check_type_variable(req.body.data.attributes.mail_message, 'string') ? req.body.data.attributes.mail_message.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.cc_mail_message && req.body.data.attributes.cc_mail_message !== null && utility.check_type_variable(req.body.data.attributes.cc_mail_message, 'string') ? req.body.data.attributes.cc_mail_message.replace(/'/g, "''''") : '') + '\',' +
                        (req.body.data.attributes.calendar_step_minute ? req.body.data.attributes.calendar_step_minute : 30) + ',\'' +
                        (req.body.data.attributes.calendar_default_view !== null ? req.body.data.attributes.calendar_default_view.replace(/'/g, "''''") : '') + '\',' +
                        req.body.data.attributes.calendar_first_hour + ',' +
                        req.body.data.attributes.calendar_last_hour + ',\'' +
                        (req.body.data.attributes.start_yearmonth && req.body.data.attributes.start_yearmonth !== null && utility.check_type_variable(req.body.data.attributes.start_yearmonth, 'string') ? req.body.data.attributes.start_yearmonth.replace('\'', ' ') : '') + '\',' +
                        req.body.data.attributes.start_yearday + ',\'' +
                        (req.body.data.attributes.invoice_from && req.body.data.attributes.invoice_from !== null && utility.check_type_variable(req.body.data.attributes.invoice_from, 'string') ? req.body.data.attributes.invoice_from.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.invoice_notes && req.body.data.attributes.invoice_notes !== null && utility.check_type_variable(req.body.data.attributes.invoice_notes, 'string') ? req.body.data.attributes.invoice_notes.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.invoice_piva && req.body.data.attributes.invoice_piva !== null && utility.check_type_variable(req.body.data.attributes.invoice_piva, 'string') ? req.body.data.attributes.invoice_piva.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.paypal && req.body.data.attributes.paypal !== null ? req.body.data.attributes.paypal.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.invoice_style && req.body.data.attributes.invoice_style !== null && utility.check_type_variable(req.body.data.attributes.invoice_style, 'string') ? req.body.data.attributes.invoice_style.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.socket_room && req.body.data.attributes.socket_room !== null && utility.check_type_variable(req.body.data.attributes.socket_room, 'string') ? req.body.data.attributes.socket_room.replace(/'/g, "''''") : req.headers['host'].split(".")[0]) + '\',\'' +
                        (req.body.data.attributes.time_zone && req.body.data.attributes.time_zone !== null && utility.check_type_variable(req.body.data.attributes.time_zone, 'string') ? req.body.data.attributes.time_zone.replace(/'/g, "''''") : '') + '\',' +
                        req.user.role_id + ',\'' +
                        (req.body.data.attributes.piva_name && req.body.data.attributes.piva_name !== null && utility.check_type_variable(req.body.data.attributes.piva_name, 'string') ? req.body.data.attributes.piva_name.replace(/'/g, "''''") : '') + '\',' +
                        (req.body.data.attributes.archivied) + ',\''+
                        (req.body.data.attributes.first_name && req.body.data.attributes.first_name !== null && utility.check_type_variable(req.body.data.attributes.first_name, 'string') ? req.body.data.attributes.first_name.replace(/'/g, "''''") : '')+ '\',\'' +
                        (req.body.data.attributes.last_name && req.body.data.attributes.last_name !== null && utility.check_type_variable(req.body.data.attributes.last_name, 'string') ? req.body.data.attributes.last_name.replace(/'/g, "''''") : '')+ '\',' +
                        req.body.data.attributes.sesso+',\''+
                        (req.body.data.attributes.data_nascita && req.body.data.attributes.data_nascita !== null && utility.check_type_variable(req.body.data.attributes.data_nascita, 'string') ? req.body.data.attributes.data_nascita : '') + '\',\'' +
                        (req.body.data.attributes.luogo_nascita && req.body.data.attributes.luogo_nascita !== null && utility.check_type_variable(req.body.data.attributes.luogo_nascita, 'string') ? req.body.data.attributes.luogo_nascita.replace(/'/g, "''''") : '') + '\',\'' +
                        (req.body.data.attributes.ragione_sociale && req.body.data.attributes.ragione_sociale !== null && utility.check_type_variable(req.body.data.attributes.ragione_sociale, 'string') ? req.body.data.attributes.ragione_sociale.replace(/'/g, "''''") : '')+ '\',\'' +
                        (req.body.data.attributes.pec && req.body.data.attributes.pec !== null && utility.check_type_variable(req.body.data.attributes.pec, 'string') ? req.body.data.attributes.pec.replace(/'/g, "''''") : '')+ '\',\'' +
                        (req.body.data.attributes.codice_destinatario && req.body.data.attributes.codice_destinatario !== null && utility.check_type_variable(req.body.data.attributes.codice_destinatario, 'string') ? req.body.data.attributes.codice_destinatario.replace(/'/g, "''''") : '')+ '\',\'' +
                        (req.body.data.attributes.cod_fiscale && req.body.data.attributes.cod_fiscale !== null && utility.check_type_variable(req.body.data.attributes.cod_fiscale, 'string') ? req.body.data.attributes.cod_fiscale.replace(/'/g, "''''") : '')+'\','+
                        (req.body.data.attributes.fisica_giuridica ? req.body.data.attributes.fisica_giuridica : null)+','+
                        (req.body.data.attributes.ente_pa ? req.body.data.attributes.ente_pa : null)+');', {
                        
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(up) {
                        if (!up[0].update_contact_v6) {
                            return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
                        }

                        if (up[0].update_contact_v6 == 0) {
                            return res.status(422).send(utility.error422('id', 'Raggiunto numero massimo di Agenti per il piano a cui si è abbonati', 'Raggiunto numero massimo di Agenti per il piano a cui si è abbonati', '422'));
                        }

                        console.log(up[0].update_contact_v6);
                        sequelize.query('SELECT delete_contact_relations_v1(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            })
                            .then(function(datas) {

                                utility.sanitizeRelations({
                                    'organization': req.headers['host'].split(".")[0],
                                    '_tenant': req.user._tenant_id,
                                    'new_id': req.params.id,
                                    'user_id': req.user.id,
                                    'body': req.body.data.relationships,
                                    'model': 'contact',
                                    'array': ['address', 'user_calendar', 'user_taxe', 'application']
                                }, function(err, results) {

                                    req.body.data.attributes.total_files_number = results.file;
                                    req.body.data.attributes.total_products_number = results.product;
                                    req.body.data.attributes.total_projects_number = results.project;
                                    req.body.data.attributes.total_payments = results.payment;

                                    finds.find_contact(req, function(err, response) {
                                        logs.save_log_model(
                                            req.user._tenant_id,
                                            req.headers['host'].split(".")[0],
                                            'contact',
                                            'update',
                                            req.user.id,
                                            JSON.stringify(req.body.data.attributes),
                                            req.params.id,
                                            'Modificato contatto ' + req.params.id
                                        );
                                        utility.save_socket(req.headers['host'].split(".")[0], 'contact', 'update', req.user.id, req.params.id, response.data);
                        
                                        res.json(response);
                                    });
                                });

                            }).catch(function(err) {
                                return res.status(400).render('contact_update: ' + err);
                            });
                    }).catch(function(err) {
                        return res.status(400).render('contact_update: ' + err);
                    });
            });
        }).catch(function(err) {
            return res.status(400).render('contact_update: ' + err);
        });
    });
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Update an existing contact searched by id
 * When a contact change we must change also all embedded documents related
 */
exports.update_all = function(req, res) {
    var util = require('util');

    if(!util.isArray(req.body))
    {   
        if (req.body.attributes.codice_destinatario && 
            req.body.attributes.codice_destinatario !== undefined && 
            req.body.attributes.codice_destinatario != 'null' &&
            req.body.attributes.codice_destinatario != '') {
            if(req.body.attributes.codice_destinatario.length != 7 && 
                req.body.attributes.codice_destinatario.length != 6){
                return res.status(422).send(utility.error422('codice_destinatario', 'Invalid attribute', 'Inserire un codice destinatario di 7 caratteri', '422'));
            }
        }

        sequelize.query('SELECT update_contact_all_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_contact_all_v1'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_contact_all_v1']) {
                //res.status(200).send({'data':datas[0]['update_contact_all']});
                req.params.id = datas[0]['update_contact_all_v1'];
                finds.find_contact(req, function(err, results) {
                    if (err) {
                        return res.status(err.errors[0].status).send(err);
                    } else {
                        utility.save_socket(req.headers['host'].split(".")[0], 'contact', 'update', req.user.id, req.params.id, results.data);
                        res.json(results);
                    }
                });
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });

    }else{
        sequelize.query('SELECT update_contact_all_array(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\', ' +
            req.user.id + ',\'' +
            JSON.stringify(req.body).replace(/'/g, "''''") + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if(datas && datas[0]['update_contact_all_array'] == 0)
            {
                return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
            }
            else if (datas && datas[0]['update_contact_all_array']) {
                res.status(200).send({});
            } else {
                res.status(422).send({});
            }
        }).catch(function(err) {
            return res.status(500).send({});
        });
    }
};  

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * Delete an existing contact searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_delete) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT is_contact_admin(' +
        req.params.id + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(is_admin) {
        if (is_admin[0].is_contact_admin) {
            return res.status(422).send(utility.error422('id', 'Non è possibile eliminare il contatto proprietario', 'Non è possibile eliminare il contatto proprietario', '422'));
        } else {
            sequelize.query('SELECT delete_contact_v1(' +
                    req.params.id + ',' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user.id + ',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    })
                .then(function(datas) {

                    if(!datas[0].delete_contact_v1){
                        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '403'));
                    }
                    else{
                        utility.save_socket(req.headers['host'].split(".")[0], 'contact', 'delete', req.user.id, req.params.id, null);
                        
                        res.json({
                            'data': {
                                'type': 'contacts',
                                'id': datas[0].delete_contact_v1
                            }
                        });
                    }
                }).catch(function(err) {
                    return res.status(400).render('contact_destroy: ' + err);
                });
        }
    }).catch(function(err) {
        console.log(err);
        return res.status(400).render('contact_destroy: ' + err);
    });
};

exports.me = function(req, res) {

    async.parallel({
        data: function(callback) {
            sequelize.query('SELECT contact_me_data_v3(' +
                req.user.id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user._tenant_id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {
                callback(null, datas[0].contact_me_data_v3);
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });

        },
        included: function(callback) {
            sequelize.query('SELECT contact_me_included_v3(' +
                req.user.id + ',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(included) {
                callback(null, included[0].contact_me_included_v3);
            }).catch(function(err) {
                console.log(err);
                callback(err, null);
            });
        }
    }, function(err, results) {

        if (err) {
            return res.status(400).render('contact_me: ' + err);
        }

        if (!results.data || results.data.length == 0 || !results.included) {

            return res.status(404).send(utility.error404('id', 'Nessun contatto trovato', 'Nessun contatto trovato', '404'));

        }
        if (results.included.organizations) {
            results.included.organizations[0].attributes.alta_bassa = req.user.alta_bassa;
        }

        res.json({
            'data': utility.check_find_datas(results.data, 'contacts', ['addresses', 'user_calendars', 'reminder_settings',
                'user_taxes', 'files', 'custom_fields', 'invoice_styles', 'contact_consensis'
            ]),
            'included': utility.check_find_included(results.included, 'contacts', ['addresses', 'custom_fields', 'user_taxes', 'files',
                'tags', 'organizations', 'event_states', 'contact_states', 'product_states', 'project_states',
                'workhour_states', 'organization_custom_fields', 'product_categories', 'categories', 'sub_categories', 'applications',
                'custom_field_definitions', 'files', 'cumulable_taxes', 'user_calendars',
                'roles', 'contacts', 'reminder_settings', 'payment_methods', 'custom_reminders', 'subscriptions', 'plans', 'invoice_styles',
                'user_sezionales','user_invoice_settings', 'contact_consensis'
            ])

        });
    });
};

exports.user_new_account = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var new_password = req.body.password;

    if (new_password !== null && new_password !== '' && new_password !== undefined) {
        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if (err) {
                return res.status(400).render('user_new_account: ' + err);
            }
            // hash the password using our new salt

            bcrypt.hash(new_password, salt, function(err, hash) {
                if (err) {
                    return res.status(400).render('user_new_account: ' + err);
                }

                req.body.password = hash;

                sequelize.query('SELECT user_new_account(' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',\'' +
                    JSON.stringify(req.body).replace(/'/g, "''''") + '\',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                    }).then(function(datas) {
                    console.log(datas);
                    if(datas && datas[0] && datas[0].user_new_account && datas[0].user_new_account == 1){
                        res.status(200).send({});
                    }else if (datas && datas[0] && datas[0].user_new_account && datas[0].user_new_account == -1) {

                         return res.status(403).send(utility.error403('Numero di account massimo raggiunto', 'Numero di account massimo raggiunto', '403')); 
                    }
                    else
                    {
                       return res.status(403).send(utility.error403('Operazione non completata correttamente', 'Operazione non completata correttamente', '401')); 
                    }
                   
                }).catch(function(err) {
                    return res.status(400).render('user_new_account: ' + err);
                });
            });
        });
    }

    
}

exports.user_change_username = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT user_change_username(' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',\'' +
        JSON.stringify(req.body).replace(/'/g, "''''") + '\',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {
        console.log(datas);
        if(datas && datas[0] && datas[0].user_change_username && datas[0].user_change_username == 1){
            res.status(200).send({});
        }else
        {
           return res.status(403).send(utility.error403('Operazione non completata correttamente', 'Operazione non completata correttamente', '401')); 
        }
       
    }).catch(function(err) {
        return res.status(400).render('user_change_username: ' + err);
    });
}

exports.user_change_password = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var new_password = req.body.password;

    if (new_password !== null && new_password !== '' && new_password !== undefined) {
        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if (err) {
                return res.status(400).render('user_new_account: ' + err);
            }
            // hash the password using our new salt

            bcrypt.hash(new_password, salt, function(err, hash) {
                if (err) {
                    return res.status(400).render('user_new_account: ' + err);
                }

                req.body.password = hash;

                sequelize.query('SELECT user_change_password(' +
                    req.user.id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    req.user._tenant_id + ',\'' +
                    JSON.stringify(req.body).replace(/'/g, "''''") + '\',' +
                    req.user.role_id + ');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                            useMaster: true
                    }).then(function(datas) {
                    console.log(datas);
                    if(datas && datas[0] && datas[0].user_change_password && datas[0].user_change_password == 1){
                        res.status(200).send({});
                    }else
                    {
                       return res.status(403).send(utility.error403('Operazione non completata correttamente', 'Operazione non completata correttamente', '401')); 
                    }
       
                }).catch(function(err) {
                    return res.status(400).render('user_change_password: ' + err);
                });
            });
        });
    }
}

exports.user_change_role = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT user_change_role(' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',\'' +
        JSON.stringify(req.body).replace(/'/g, "''''") + '\',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {
        console.log(datas);
        if(datas && datas[0] && datas[0].user_change_role && datas[0].user_change_role == 1){
            res.status(200).send({});
        }else
        {
           return res.status(403).send(utility.error403('Operazione non completata correttamente', 'Operazione non completata correttamente', '401')); 
        }
       
    }).catch(function(err) {
        return res.status(400).render('user_change_role: ' + err);
    });
}

exports.revoke = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    sequelize.query('SELECT contact_revoca_accesso(' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ',' +
        req.body.data.attributes.id + ',' +
        req.user.role_id + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {
        res.status(200).send({});
    }).catch(function(err) {
        return res.status(400).render('contact_revoke: ' + err);
    });
};


exports.doppi_contatti = function(req, res) {

    var status = req.query.status ? req.query.status : null;

    if (status) {
        sequelize.query('SELECT get_doppi_contatti(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                status + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas && datas.length > 0) {

                    res.json(datas[0]['get_doppi_contatti']);
                } else {
                    res.json({});
                }

            });
    } else {
        return res.status(422).send(utility.error422('status', 'Status errato', 'Non è possibile cercare contatti ', '422'));
    }

};

exports.accoppia_contatti = function(req, res) {
    console.log(req.body.contacts.length);

    if (req.body.contacts.length == 2 && req.body.into !== undefined && req.body.into) {
        sequelize.query('SELECT merge_doppi_contatti(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.body.into + ',' +
                (req.body.contacts[0] == req.body.into ? req.body.contacts[1] : req.body.contacts[0]) + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].merge_doppi_contatti == 1) {

                    res.json(datas[0]['merge_doppi_contatti']);
                } else {
                    return res.status(422).send(utility.error422('contacts', 'Contatto amministratore', 'Non è possibile unire contatti ', '422'));
                }

            });
    } else {
        return res.status(422).send(utility.error422('contacts', 'Array di contatti errato', 'Non è possibile unire contatti ', '422'));
    }

};

exports.ignora_doppi_contatti = function(req, res) {

    if (req.body.contacts.length == 2) {
        sequelize.query('SELECT ignora_doppi_contatti(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                req.user.id + ',' +
                req.body.contacts[0] + ',' +
                req.body.contacts[1] + ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].ignora_doppi_contatti == 1) {

                    res.json(datas[0]['ignora_doppi_contatti']);
                } else {
                    return res.status(422).send(utility.error422('contacts', 'Errore', 'Non è possibile ignorare contatti ', '422'));
                }

            });
    } else {
        return res.status(422).send(utility.error422('contacts', 'Array di contatti errato', 'Non è possibile ignorare i doppi contatti ', '422'));
    }

};

exports.doppi_contatti_ignorati = function(req, res) {


    sequelize.query('SELECT get_doppi_contatti_ignorati(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas) {

                res.json(datas[0]['get_doppi_contatti_ignorati']);
            } else {
                res.json({});
            }

        });


};

exports.fornitori_charts = function(req, res) {
     var start = null,
        end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }



    sequelize.query('SELECT expense_fornitori_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].expense_fornitori_chart[0] && datas[0].expense_fornitori_chart[0].Fornitori) {
                var cnt = 0;
                var categorie = {};
                datas[0].expense_fornitori_chart[0].Fornitori.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name].push(cat.tot);
                    if (cnt++ >= datas[0].expense_fornitori_chart[0].Fornitori.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('expense_fornitori_chart: ' + err);
        });
};

exports.annual_fornitori_charts = function(req, res) {
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT expense_annual_fornitori_chart_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].expense_annual_fornitori_chart_v2[0] && datas[0].expense_annual_fornitori_chart_v2[0].fornitori) {
                var cnt = 0;
                var categorie = {};
                datas[0].expense_annual_fornitori_chart_v2[0].fornitori.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name] = cat.array_agg;
                    if (cnt++ >= datas[0].expense_annual_fornitori_chart_v2[0].fornitori.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('expense_annual_fornitori_chart_v2: ' + err);
        });
};

exports.contratti_fornitori_charts = function(req, res) {
    var start = null,
        end=null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!utility.check_type_variable(req.query.from_date, 'string')) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        start = '\''+req.query.from_date+'\'';
    } else {
        return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!utility.check_type_variable(req.query.to_date, 'string')) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }
        end = '\''+req.query.to_date+'\'';
    } else {
        return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
    }



    sequelize.query('SELECT contratti_fornitori_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            start + ',' +
            end + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].contratti_fornitori_chart[0] && datas[0].contratti_fornitori_chart[0].Fornitori) {
                var cnt = 0;
                var categorie = {};
                datas[0].contratti_fornitori_chart[0].Fornitori.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name].push(cat.tot);
                    if (cnt++ >= datas[0].contratti_fornitori_chart[0].Fornitori.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('contratti_fornitori_chart: ' + err);
        });
};

exports.contratti_annual_fornitori_charts = function(req, res) {
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT contratti_annual_fornitori_chart_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].contratti_annual_fornitori_chart_v2[0] && datas[0].contratti_annual_fornitori_chart_v2[0].fornitori) {
                var cnt = 0;
                var categorie = {};
                datas[0].contratti_annual_fornitori_chart_v2[0].fornitori.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name] = cat.array_agg;
                    if (cnt++ >= datas[0].contratti_annual_fornitori_chart_v2[0].fornitori.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('contratti_annual_fornitori_chart_v2: ' + err);
        });
};

exports.iva_charts = function(req, res) {
    if (req.query.year === undefined && req.query.year === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT iva_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            req.user.id + ','+
            (req.query.contact_id ? req.query.contact_id : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            if (datas[0].iva_chart[0] && datas[0].iva_chart[0].IVA) {
                var cnt = 0;
                var categorie = {};
                datas[0].iva_chart[0].IVA.forEach(function(cat) {
                    if (!cat.name)
                        cat.name = 'Non categorizzato';

                    categorie[cat.name] = [];
                    categorie[cat.name] = cat.array_agg;
                    if (cnt++ >= datas[0].iva_chart[0].IVA.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('iva_chart: ' + err);
        });
};

exports.codice_fiscale = function(req, res) {
    var cf = require('../utility/codice_fiscale.js');


    if (req.body.first_name && req.body.first_name !== null && utility.check_type_variable(req.body.first_name, 'string') &&
        req.body.last_name && req.body.last_name !== null && utility.check_type_variable(req.body.last_name, 'string') &&
        req.body.sex && req.body.sex !== null &&
        req.body.birthDateDay && req.body.birthDateDay !== null && utility.check_type_variable(req.body.birthDateDay, 'string') &&
        req.body.placeOfBirth && req.body.placeOfBirth !== null && utility.check_type_variable(req.body.placeOfBirth, 'string')
    ) {

        let sex = req.body.sex == 'true' ? 'M' : 'F';
        let birthDateDay = req.body.birthDateDay;
        let placeOfBirth = req.body.placeOfBirth;
        let first_name = req.body.first_name;
        let last_name = req.body.last_name;

        birthDateDay = moment(req.body.birthDateDay, 'DD/MM/YYYY').format('MM-DD-YYYY');
        let res_comuni = comune_valido(placeOfBirth);

        if(res_comuni == -1){
            let cf_calcolato = cf(first_name, last_name, sex, new Date(birthDateDay), placeOfBirth);

            if (cf_calcolato) {
                res.status(200).send({ 'cf': cf_calcolato });
            } else {
                res.status(403).send({ 'error': 'errore nel calcolare il codice fiscale' });
            }
        }else{
            res.status(200).send({ 'comuni': res_comuni});
        }

        
    } else {
        res.status(403).send({ 'error': 'dati mancanti' });
    }

}


exports.codfiscale_to_comune = function(req, res) {
    var comuni = require('../utility/comuni.json');

    var parsedJSON = JSON.parse(JSON.stringify(comuni));


    var cityResultObj = parsedJSON.filter(function (el) {
        if(el.codice.toLowerCase() == req.body.sigla.toLowerCase()){
            return el;
        }
        
    });


    if(cityResultObj && cityResultObj[0]){
        res.status(200).send({'comune':cityResultObj[0].comune})
    }
    else{
        res.status(200).send({'comune':null})
    }
};

function comune_valido(comune) {
    var comuni = require('../utility/comuni.json');

    var parsedJSON = JSON.parse(JSON.stringify(comuni));


    var cityResultObj = parsedJSON.filter(function (el) {
        if(el.comune.toLowerCase() == comune.toLowerCase()){
            return el;
        }
        
    });

    
    if(cityResultObj && cityResultObj[0]){
        return -1;
        
    }
    else{ 
        var comuni =  [];
       
        parsedJSON.filter(function (el) {
            if (el.comune.toLowerCase().indexOf(comune.toLowerCase()) !== -1) {  // obj1.key contains "xx"
                //do something
                comuni.push(el.comune);
            }   
        });
         
       return comuni;
    }
};


function uc_first(param) {
    return param.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
};