var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

exports.create = function(req, res) {
    sequelize.query('SELECT insert_push_notifications(\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',\'' +
            req.body.player_id + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});

        }).catch(function(err) {
            return res.status(400).render('insert_push_notifications: ' + err);
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {

    sequelize.query('SELECT delete_push_notifications(' +
            req.user.id + ',\'' +
            req.headers['host'].split(".")[0] + '\',\'' +
            req.body.player_id + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.status(200).json({});

        }).catch(function(err) {
            return res.status(400).render('delete_push_notifications: ' + err);
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.get_player = function(req, res) {
    var request = require('request');
    var config = require('../../config');
    sequelize.query('SELECT get_push_player_id(' +
            req.user.id + ',\'' +
            req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            var response = {};
            response.players = [];

            var cnt = 0;
            if (datas && datas[0] && datas[0].get_push_player_id && datas[0].get_push_player_id.length > 0) {
                datas[0].get_push_player_id.forEach(function(player) {
                    // Configure the request
                    var options = {
                        url: ' https://onesignal.com/api/v1/players/' + player + '?app_id=' + config.onesignal.app_id,
                        method: 'GET'
                    }
                    // Start the request
                    request(options, function(error1, response1, body1) {
                        response.players.push(JSON.parse(body1));
                        if (++cnt > datas[0].get_push_player_id.length - 1) {

                            res.status(200).json(response);
                        }
                    });
                });
            } else {
                res.status(200).json({});
            }


        }).catch(function(err) {
            return res.status(400).render('get_push_player_id: ' + err);
        });
};