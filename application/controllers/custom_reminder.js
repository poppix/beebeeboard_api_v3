var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    logs = require('../utility/logs.js');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    var custom_reminder = {};

    if (req.body.data.attributes !== undefined) {



        custom_reminder['mail_event'] = (req.body.data.attributes.mail_event && utility.check_type_variable(req.body.data.attributes.mail_event, 'string') && req.body.data.attributes.mail_event !== null ? req.body.data.attributes.mail_event.replace(/'/g, "''''") : '');
        custom_reminder['mail_todo'] = (req.body.data.attributes.mail_todo && utility.check_type_variable(req.body.data.attributes.mail_todo, 'string') && req.body.data.attributes.mail_todo !== null ? req.body.data.attributes.mail_todo.replace(/'/g, "''''") : '');
        custom_reminder['mail_invoice'] = (req.body.data.attributes.mail_invoice && utility.check_type_variable(req.body.data.attributes.mail_invoice, 'string') && req.body.data.attributes.mail_invoice !== null ? req.body.data.attributes.mail_invoice.replace(/'/g, "''''") : '');
        custom_reminder['mail_expense'] = (req.body.data.attributes.mail_expense && utility.check_type_variable(req.body.data.attributes.mail_expense, 'string') && req.body.data.attributes.mail_expense !== null ? req.body.data.attributes.mail_expense.replace(/'/g, "''''") : '');
        custom_reminder['sms_event'] = (req.body.data.attributes.sms_event && utility.check_type_variable(req.body.data.attributes.sms_event, 'string') && req.body.data.attributes.sms_event !== null ? req.body.data.attributes.sms_event.replace(/'/g, "''''") : '');
        custom_reminder['sms_todo'] = (req.body.data.attributes.sms_todo && utility.check_type_variable(req.body.data.attributes.sms_todo, 'string') && req.body.data.attributes.sms_todo !== null ? req.body.data.attributes.sms_todo.replace(/'/g, "''''") : '');
        custom_reminder['sms_invoice'] = (req.body.data.attributes.sms_invoice && utility.check_type_variable(req.body.data.attributes.sms_invoice, 'string') && req.body.data.attributes.sms_invoice !== null ? req.body.data.attributes.sms_invoice.replace(/'/g, "''''") : '');
        custom_reminder['sms_expense'] = (req.body.data.attributes.sms_expense && utility.check_type_variable(req.body.data.attributes.sms_expense, 'string') && req.body.data.attributes.sms_expense !== null ? req.body.data.attributes.sms_expense.replace(/'/g, "''''") : '');
         custom_reminder['sms_birthday'] = (req.body.data.attributes.sms_birthday && utility.check_type_variable(req.body.data.attributes.sms_birthday, 'string') && req.body.data.attributes.sms_birthday !== null ? req.body.data.attributes.sms_birthday.replace(/'/g, "''''") : '');
        custom_reminder['mail_birthday'] = (req.body.data.attributes.mail_birthday && utility.check_type_variable(req.body.data.attributes.mail_birthday, 'string') && req.body.data.attributes.mail_birthday !== null ? req.body.data.attributes.mail_birthday.replace(/'/g, "''''") : '');
        custom_reminder['_tenant_id'] = req.user._tenant_id;
        custom_reminder['organization'] = req.headers['host'].split(".")[0];
        custom_reminder['organization_id'] = req.user.organization_id;

        sequelize.query('SELECT insert_custom_reminder(\'' +
                JSON.stringify(custom_reminder) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                req.body.data.id = datas[0].insert_custom_reminder;

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'custom_reminder',
                    'insert',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    datas[0].insert_custom_reminder,
                    'Aggiunto testo promemoria ' + datas[0].insert_custom_reminder
                );
                res.json({
                    'data': req.body.data,
                    'relationships': req.body.data.relationships
                });

            }).catch(function(err) {
                return res.status(400).render('custom_reminder_insert: ' + err);
            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT update_custom_reminders(' +
                req.params.id + ',\'' +
                (req.body.data.attributes.mail_event && utility.check_type_variable(req.body.data.attributes.mail_event, 'string') && req.body.data.attributes.mail_event !== null ? req.body.data.attributes.mail_event.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.mail_todo && utility.check_type_variable(req.body.data.attributes.mail_todo, 'string') && req.body.data.attributes.mail_todo !== null ? req.body.data.attributes.mail_todo.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.mail_invoice && utility.check_type_variable(req.body.data.attributes.mail_invoice, 'string') && req.body.data.attributes.mail_invoice !== null ? req.body.data.attributes.mail_invoice.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.mail_expense && utility.check_type_variable(req.body.data.attributes.mail_expense, 'string') && req.body.data.attributes.mail_expense !== null ? req.body.data.attributes.mail_expense.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.sms_event && utility.check_type_variable(req.body.data.attributes.sms_event, 'string') && req.body.data.attributes.sms_event !== null ? req.body.data.attributes.sms_event.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.sms_todo && utility.check_type_variable(req.body.data.attributes.sms_todo, 'string') && req.body.data.attributes.sms_todo !== null ? req.body.data.attributes.sms_todo.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.sms_invoice && utility.check_type_variable(req.body.data.attributes.sms_invoice, 'string') && req.body.data.attributes.sms_invoice !== null ? req.body.data.attributes.sms_invoice.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.sms_expense && utility.check_type_variable(req.body.data.attributes.sms_expense, 'string') && req.body.data.attributes.sms_expense !== null ? req.body.data.attributes.sms_expense.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.mail_birthday && utility.check_type_variable(req.body.data.attributes.mail_birthday, 'string') && req.body.data.attributes.mail_birthday !== null ? req.body.data.attributes.mail_birthday.replace(/'/g, "''''") : '') + '\',\'' +
                (req.body.data.attributes.sms_birthday && utility.check_type_variable(req.body.data.attributes.sms_birthday, 'string') && req.body.data.attributes.sms_birthday !== null ? req.body.data.attributes.sms_birthday.replace(/'/g, "''''") : '') + '\',' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {

                logs.save_log_model(
                    req.user._tenant_id,
                    req.headers['host'].split(".")[0],
                    'custom_reminder',
                    'update',
                    req.user.id,
                    JSON.stringify(req.body.data.attributes),
                    req.params.id,
                    'Modificato testo del promemoria ' + req.params.id
                );
                res.json({
                    'data': req.body.data
                });

            }).catch(function(err) {
                return res.status(400).render('custom_reminder_update: ' + err);
            });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.is_admin) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_custom_reminder_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'custom_reminder',
                    'id': datas[0].delete_custom_reminder_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('custom_reminder_destroy: ' + err);
        });
};