var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js');

/*
 * @apiUse IdParamEntryError
 */


exports.clinic_report_miss = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters(req, 'project', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_miss(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

             if (datas[0].clinic_report_miss) {
                res.json(datas[0].clinic_report_miss);
            } else
                return res.status(422).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_miss: ' + err);
        });
    });  
};

exports.clinic_report_appuntamenti = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters(req, 'event', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_appuntamenti_v1(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ','+
            (req.query.with_paz ? req.query.with_paz : null) +','+
            (req.query.with_inv ? '\''+req.query.with_inv+'\'' : null) +','+
            (parameters.status && parameters.status.length > 0 ? 'ARRAY[' + parameters.status + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

             if (datas[0].clinic_report_appuntamenti_v1) {
                res.json(datas[0].clinic_report_appuntamenti_v1);
            } else
                return res.status(422).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_appuntamenti: ' + err);
        });
    });   
};

exports.clinic_report_scadenze = function(req, res) {
    if (!req.user.role[0].json_build_object.attributes.event_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters(req, 'project', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_scadenze(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

             if (datas[0].clinic_report_scadenze) {
                res.json(datas[0].clinic_report_scadenze);
            } else
                return res.status(422).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_scadenze: ' + err);
        });
    });   
};

exports.clinic_report_specialist = function(req, res) {
    
    if (!req.user.role[0].json_build_object.attributes.event_list || !req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters(req, 'project', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_specialist(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ',\''+
            req.query.type+'\','+
            (req.query.with_paz ? req.query.with_paz : null) +','+
            (req.query.paied ? req.query.paied : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

             if (datas[0].clinic_report_specialist) {
                res.json(datas[0].clinic_report_specialist);
            } else
                return res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_specialisti: ' + err);
        });
    });   
};

exports.clinic_sign_project = function(req, res) {
    var util = require('util');

    if (!req.user.role[0].json_build_object.attributes.project_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var project_id = null;
    var bool_val = null;

    if (req.query.project_id !== undefined && req.query.project_id != '' && req.query.project_id != 'undefined') {
        if (util.isArray(req.query.project_id)) {
            project_id = req.query.project_id;
        } else {
            project_id.push(req.query.project_id);
        }
    }

    if (req.query.firma !== undefined && req.query.firma != '' && req.query.firma != 'undefined') {
        bool_val = req.query.firma;
    }

     sequelize.query('SELECT clinic_sign_project(' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        req.user.role_id + ','+
        (project_id.length > 0 ? 'ARRAY[' + project_id + ']::bigint[]' : null) + ','+
        bool_val+');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {

         if (datas[0].clinic_sign_project) {
            res.json(datas[0].clinic_sign_project);
        } else
            return res.status(422).send({});

    }).catch(function(err) {
        return res.status(400).render('clinic_sign_project: ' + err);
    }); 
};

exports.copy_contact = function(req, res) {
    var util = require('util');

    if (!req.user.role[0].json_build_object.attributes.contact_edit) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    var models = [];
    var model_id = null;


    if(req.query.option && req.query.option.contact && req.query.option.contact == 'true'){
        models.push('\''+'contact'+'\'');
    }   

    if(req.query.option && req.query.option.product && req.query.option.product == 'true'){
        models.push('\''+'product'+'\'');
    }   

     if(req.query.option && req.query.option.event && req.query.option.event == 'true'){
        models.push('\''+'event'+'\'');
    } 
     if(req.query.option && req.query.option.project && req.query.option.project == 'true'){
        models.push('\''+'project'+'\'');
    } 
     if(req.query.option && req.query.option.invoice && req.query.option.invoice == 'true'){
        models.push('\''+'invoice'+'\'');
    }   

      if(req.query.option && req.query.option.expense && req.query.option.expense == 'true'){
        models.push('\''+'expense'+'\'');
    }   
      if(req.query.option && req.query.option.payment && req.query.option.payment == 'true'){
        models.push('\''+'payment'+'\'');
    }   


    if (req.query.id !== undefined && req.query.id != '' && req.query.id != 'undefined') {
        model_id = req.query.id;
    }

     sequelize.query('SELECT clinic_export_contact(' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ','+
        model_id + ','+
        req.user.role_id + ','+
        (models.length > 0 ? 'ARRAY[' + models + ']::text[]' : null) + ');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {

         if (datas[0].clinic_export_contact) {
            res.json(datas[0].clinic_export_contact);
        } else
            return res.status(422).send({});

    }).catch(function(err) {
        return res.status(400).render('clinic_export_contact: ' + err);
    }); 
};

exports.paste_contact = function(req, res) {

    
    if (!req.user.role[0].json_build_object.attributes.contact_insert) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    sequelize.query('SELECT clinic_import_contact(' +
        req.user.id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user._tenant_id + ','+
        req.user.role_id + ',\''+
        JSON.stringify(req.body).replace(/'/g, "''''") +'\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
            useMaster:true
        })
    .then(function(datas) {

         if (datas[0].clinic_import_contact) {
            res.json(datas[0].clinic_import_contact);
        } else
            return res.status(422).send({});

    }).catch(function(err) {
        
        return res.status(400).render('clinic_import_contact: ' + err);
    }); 
};

exports.clinic_chart_appuntamenti = function(req, res) {
    utility.get_parameters(req, 'project', function(err, parameters) {
       
            sequelize.query('SELECT clinic_appuntamenti_chart(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
                (req.query.with_paz ? req.query.with_paz : null)+','+
                req.user.role_id+','+
                req.user.id+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                async.parallel({
                  prezzi: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_appuntamenti_chart && datas[0].clinic_appuntamenti_chart[0] && datas[0].clinic_appuntamenti_chart[0].Prezzo) {
                            var cnt = 0;
                            var prezzi = {};
                            datas[0].clinic_appuntamenti_chart[0].Prezzo.forEach(function(cat) {
                                

                                prezzi[cat.name] = [];
                                prezzi[cat.name].push(cat.tot);
                                if (cnt++ >= datas[0].clinic_appuntamenti_chart[0].Prezzo.length - 1) {
                                    callback(null, prezzi);
                                }
                            });
                        } else {
                            callback(null, null);
                        }
                  },
                  num: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_appuntamenti_chart && datas[0].clinic_appuntamenti_chart[0] && datas[0].clinic_appuntamenti_chart[0].Num) {
                            var cnt1 = 0;
                            var num = {};
                            datas[0].clinic_appuntamenti_chart[0].Num.forEach(function(cat) {
                              
                                num[cat.name] = [];
                                num[cat.name].push(cat.num);
                                if (cnt1++ >= datas[0].clinic_appuntamenti_chart[0].Num.length - 1) {
                                    callback(null, num);
                                }
                            });
                        } else {
                             callback(null, null);
                        }
                  },
                  ore: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_appuntamenti_chart && datas[0].clinic_appuntamenti_chart[0] && datas[0].clinic_appuntamenti_chart[0].Ore) {
                            var cnt2 = 0;
                            var ore = {};
                            datas[0].clinic_appuntamenti_chart[0].Ore.forEach(function(cat) {
                                

                                ore[cat.name] = [];
                                ore[cat.name].push(cat.ore);
                                if (cnt2++ >= datas[0].clinic_appuntamenti_chart[0].Ore.length - 1) {
                                    callback(null, ore);
                                }
                            });
                        } else {
                            callback(null, null);
                        }
                  },
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Report Chart clinic appuntamenti' + err);
                    }

                    res.status(200).json({
                        'prezzi': results.prezzi,
                        'ore': results.ore,
                        'num': results.num
                    });
                });


            }).catch(function(err) {
                return res.status(400).render('clinic_appuntamenti_chart: ' + err);
            });  
       
          
    });
};

exports.clinic_chart_all_appuntamenti = function(req, res) {
    utility.get_parameters(req, 'project', function(err, parameters) {
       
            sequelize.query('SELECT clinic_appuntamenti_all_chart(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
                (req.query.with_paz ? req.query.with_paz : null)+','+
                req.user.role_id+','+
                req.user.id+',\''+
                req.query.type+'\',\''+
                req.query.object+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                async.parallel({
                  prezzi: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_appuntamenti_all_chart && datas[0].clinic_appuntamenti_all_chart[0] && datas[0].clinic_appuntamenti_all_chart[0].Prezzo) {
                            callback(null, datas[0].clinic_appuntamenti_all_chart[0].Prezzo);
                        } else {
                            callback(null, null);
                        }
                  },
                  num: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_appuntamenti_all_chart && datas[0].clinic_appuntamenti_all_chart[0] && datas[0].clinic_appuntamenti_all_chart[0].Num) {
                            callback(null, datas[0].clinic_appuntamenti_all_chart[0].Num);
                        } else {
                             callback(null, null);
                        }
                  },
                  ore: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_appuntamenti_all_chart && datas[0].clinic_appuntamenti_all_chart[0] && datas[0].clinic_appuntamenti_all_chart[0].Ore) {
                            
                           
                            callback(null, datas[0].clinic_appuntamenti_all_chart[0].Ore);
                               
                        } else {
                            callback(null, null);
                        }
                  },
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Report Chart  appuntamenti' + err);
                    }

                    res.status(200).json({
                        'prezzi': results.prezzi,
                        'ore': results.ore,
                        'num': results.num
                    });
                });


            }).catch(function(err) {
                return res.status(400).render('clinic_chart_all_appuntamenti: ' + err);
            });  
       
          
    });
};

exports.clinic_valutazioni_chart = function(req, res) {
    utility.get_parameters(req, 'project', function(err, parameters) {
       
            sequelize.query('SELECT clinic_valutazioni_chart(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
                (req.query.with_paz ? req.query.with_paz : null)+','+
                req.user.role_id+','+
                req.user.id+',\''+
                req.query.type+'\',\''+
                req.query.object+'\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                async.parallel({
                  
                  num: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_valutazioni_chart && datas[0].clinic_valutazioni_chart[0] && datas[0].clinic_valutazioni_chart[0].Num) {
                            callback(null, datas[0].clinic_valutazioni_chart[0].Num);
                        } else {
                             callback(null, null);
                        }
                  }
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Report Chart  Clinic' + err);
                    }

                    res.status(200).json({
                        'num': results.num
                    });
                });


            }).catch(function(err) {
                return res.status(400).render('clinic_valutazioni_chart: ' + err);
            });  
       
          
    });
};

exports.clinic_chart_trattamenti = function(req, res){
    utility.get_parameters(req, 'project', function(err, parameters) {
        sequelize.query('SELECT clinic_trattamenti_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            req.user.role_id+','+
            req.user.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            async.parallel({
              prezzi: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_trattamenti_chart && datas[0].clinic_trattamenti_chart[0] && datas[0].clinic_trattamenti_chart[0].Prezzo) {
                        
                                callback(null, datas[0].clinic_trattamenti_chart[0].Prezzo);
                            
                    } else {
                        callback(null, null);
                    }
              },
              num: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_trattamenti_chart && datas[0].clinic_trattamenti_chart[0] && datas[0].clinic_trattamenti_chart[0].Num) {
                          callback(null, datas[0].clinic_trattamenti_chart[0].Num);
                    } else {
                         callback(null, null);
                    }
              },
              ore: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_trattamenti_chart && datas[0].clinic_trattamenti_chart[0] && datas[0].clinic_trattamenti_chart[0].Ore) {
                          callback(null, datas[0].clinic_trattamenti_chart[0].Ore);
                    } else {
                        callback(null, null);
                    }
              },
              trattamenti: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_trattamenti_chart && datas[0].clinic_trattamenti_chart[0] && datas[0].clinic_trattamenti_chart[0].trattamenti) {
                          callback(null, datas[0].clinic_trattamenti_chart[0].trattamenti);
                    } else {
                        callback(null, null);
                    }
              },
               medici: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_trattamenti_chart && datas[0].clinic_trattamenti_chart[0] && datas[0].clinic_trattamenti_chart[0].medici) {
                          callback(null, datas[0].clinic_trattamenti_chart[0].medici);
                    } else {
                        callback(null, null);
                    }
              }
            }, function(err, results) {
                if (err) {
                    console.log(err);
                    return res.status(500).send('Report Chart clinic trattamenti' + err);
                }

                res.status(200).json({
                    'prezzi': results.prezzi,
                    'ore': results.ore,
                    'num': results.num,
                    'trattamenti': results.trattamenti,
                    'medici': results.medici
                });
            });


        }).catch(function(err) {
            return res.status(400).render('clinic_trattamenti_chart: ' + err);
        }); 
    });
};

exports.clinic_chart_eta = function(req, res){
    utility.get_parameters(req, 'project', function(err, parameters) {
        sequelize.query('SELECT clinic_eta_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            req.user.role_id+','+
            req.user.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            async.parallel({
              prezzi: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_eta_chart && datas[0].clinic_eta_chart[0] && datas[0].clinic_eta_chart[0].Prezzo) {
                        
                                callback(null, datas[0].clinic_eta_chart[0].Prezzo);
                            
                    } else {
                        callback(null, null);
                    }
              },
              num: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_eta_chart && datas[0].clinic_eta_chart[0] && datas[0].clinic_eta_chart[0].Num) {
                          callback(null, datas[0].clinic_eta_chart[0].Num);
                    } else {
                         callback(null, null);
                    }
              },
              fasce: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_eta_chart && datas[0].clinic_eta_chart[0] && datas[0].clinic_eta_chart[0].Fasce) {
                          callback(null, datas[0].clinic_eta_chart[0].Fasce);
                    } else {
                        callback(null, null);
                    }
              },
              totali: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_eta_chart && datas[0].clinic_eta_chart[0] && datas[0].clinic_eta_chart[0].Totali) {
                          callback(null, datas[0].clinic_eta_chart[0].Totali);
                    } else {
                        callback(null, null);
                    }
              }
            }, function(err, results) {
                if (err) {
                    console.log(err);
                    return res.status(500).send('Report Chart clinic Eta' + err);
                }

                res.status(200).json({
                    'prezzi': results.prezzi,
                    'num': results.num,
                    'fasce': results.fasce,
                    'totali': results.totali
                });
            });


        }).catch(function(err) {
            return res.status(400).render('clinic_eta_chart: ' + err);
        }); 
    });
};

exports.clinic_chart_sesso = function(req, res) {
    utility.get_parameters(req, 'project', function(err, parameters) {
       
            sequelize.query('SELECT clinic_sesso_chart(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
                req.user.role_id+','+
                req.user.id+');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                    useMaster: true
                })
            .then(function(datas) {
                async.parallel({
                  prezzi: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_sesso_chart && datas[0].clinic_sesso_chart[0] && datas[0].clinic_sesso_chart[0].Prezzo) {
                            var cnt = 0;
                            var prezzi = {};
                            datas[0].clinic_sesso_chart[0].Prezzo.forEach(function(cat) {
                                

                                prezzi[cat.sesso] = [];
                                prezzi[cat.sesso].push(cat.tot);
                                if (cnt++ >= datas[0].clinic_sesso_chart[0].Prezzo.length - 1) {
                                    callback(null, prezzi);
                                }
                            });
                        } else {
                            callback(null, null);
                        }
                  },
                  num: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_sesso_chart && datas[0].clinic_sesso_chart[0] && datas[0].clinic_sesso_chart[0].Num) {
                            var cnt1 = 0;
                            var num = {};
                            datas[0].clinic_sesso_chart[0].Num.forEach(function(cat) {
                              
                                num[cat.sesso] = [];
                                num[cat.sesso].push(cat.num);
                                if (cnt1++ >= datas[0].clinic_sesso_chart[0].Num.length - 1) {
                                    callback(null, num);
                                }
                            });
                        } else {
                             callback(null, null);
                        }
                  },
                  totali: function(callback) {
                        if (datas && datas[0] && datas[0].clinic_sesso_chart && datas[0].clinic_sesso_chart[0] && datas[0].clinic_sesso_chart[0].Totali) {
                            var cnt1 = 0;
                            var num = {};
                            datas[0].clinic_sesso_chart[0].Totali.forEach(function(cat) {
                              
                                num[cat.sesso] = [];
                                num[cat.sesso].push(cat.num);
                                if (cnt1++ >= datas[0].clinic_sesso_chart[0].Totali.length - 1) {
                                    callback(null, num);
                                }
                            });
                        } else {
                             callback(null, null);
                        }
                  }
                }, function(err, results) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send('Report Chart clinic Sesso' + err);
                    }

                    res.status(200).json({
                        'prezzi': results.prezzi,
                        'num': results.num,
                        'totali': results.totali
                    });
                });


            }).catch(function(err) {
                return res.status(400).render('clinic_sesso_chart: ' + err);
            });  
       
          
    });
};

exports.clinic_chart_occupazione = function(req, res){
    utility.get_parameters(req, 'project', function(err, parameters) {
        sequelize.query('SELECT clinic_occupazione_chart(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            (req.query.with_paz ? req.query.with_paz : null)+','+
            req.user.role_id+','+
            req.user.id+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            async.parallel({
              dispo: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_occupazione_chart && datas[0].clinic_occupazione_chart[0] && datas[0].clinic_occupazione_chart[0].Dispo) {
                        
                                callback(null, datas[0].clinic_occupazione_chart[0].Dispo);
                            
                    } else {
                        callback(null, null);
                    }
              },
              occup: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_occupazione_chart && datas[0].clinic_occupazione_chart[0] && datas[0].clinic_occupazione_chart[0].Occup) {
                          callback(null, datas[0].clinic_occupazione_chart[0].Occup);
                    } else {
                         callback(null, null);
                    }
              },
              mesi: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_occupazione_chart && datas[0].clinic_occupazione_chart[0] && datas[0].clinic_occupazione_chart[0].mesi) {
                          callback(null, datas[0].clinic_occupazione_chart[0].mesi);
                    } else {
                        callback(null, null);
                    }
              },
               medici: function(callback) {
                    if (datas && datas[0] && datas[0].clinic_occupazione_chart && datas[0].clinic_occupazione_chart[0] && datas[0].clinic_occupazione_chart[0].medici) {
                          callback(null, datas[0].clinic_occupazione_chart[0].medici);
                    } else {
                        callback(null, null);
                    }
              }
            }, function(err, results) {
                if (err) {
                    console.log(err);
                    return res.status(500).send('Report Chart clinic Occupazione' + err);
                }

                res.status(200).json({
                    'dispo': results.dispo,
                    'occup': results.occup,
                    'mesi': results.mesi,
                    'medici': results.medici
                });
            });


        }).catch(function(err) {
            return res.status(400).render('clinic_occupazione_chart: ' + err);
        }); 
    });
};

exports.clinic_report_pazienti = function(req, res){
    utility.get_parameters(req, 'event', function(err, parameters) {
        sequelize.query('SELECT clinic_pazienti_report_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (req.query.first_app ? '\''+req.query.first_app+'\'' : null)+','+
            (req.query.last_app ? '\''+req.query.last_app+'\'' : null)+','+
            (req.query.sesso ? req.query.sesso : null)+','+
            (req.query.eta_min ? req.query.eta_min : null)+','+
            (req.query.eta_max ? req.query.eta_max : null)+','+
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ','+
            (req.query.app_status ? req.query.app_status : null)+','+
            (req.query.min_fatt ? req.query.min_fatt : null)+','+
            (req.query.max_fatt ? req.query.max_fatt : null)+','+
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            parameters.sortField + ',' +
            parameters.sortDirection + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
            req.user.role_id+','+
            req.user.id+','+
            (req.query.created ? '\''+req.query.created+'\'' : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
           
                if (err) {
                    console.log(err);
                    return res.status(500).send('Report pazienti' + err);
                }

                if(datas && datas[0] && datas[0].clinic_pazienti_report_v1){
                    res.status(200).json(datas[0].clinic_pazienti_report_v1);
                }else
                {
                    res.status(404).json({});
                }
                
            
        }).catch(function(err) {
            return res.status(400).render('clinic_pazienti_report: ' + err);
        }); 
    });
};

exports.clinic_report_pazienti_export = function(req, res){
    var json2csv = require('json2csv');

    utility.get_parameters(req, 'event', function(err, parameters) {
        sequelize.query('SELECT clinic_pazienti_report_export_v1(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            (req.query.first_app ? '\''+req.query.first_app+'\'' : null)+','+
            (req.query.last_app ? '\''+req.query.last_app+'\'' : null)+','+
            (req.query.sesso ? req.query.sesso : null)+','+
            (req.query.eta_min ? req.query.eta_min : null)+','+
            (req.query.eta_max ? req.query.eta_max : null)+','+
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ','+
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ','+
            (req.query.app_status ? req.query.app_status : null)+','+
            (req.query.min_fatt ? req.query.min_fatt : null)+','+
            (req.query.max_fatt ? req.query.max_fatt : null)+','+
            parameters.sortField + ',' +
            parameters.sortDirection + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
            req.user.role_id+','+
            req.user.id+','+
            (req.query.created ? '\''+req.query.created+'\'' : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {

            if (datas[0].clinic_pazienti_report_export_v1 && 
                datas[0].clinic_pazienti_report_export_v1[0] &&
                datas[0].clinic_pazienti_report_export_v1[0]['contacts'] &&
                datas[0].clinic_pazienti_report_export_v1[0]['contacts'].length > 0) {
                    


                async.parallel({
                    custom: function(callback) {
                        var contacts = [];

                        var cnt_contact = 0;
                        if (datas[0].clinic_pazienti_report_export_v1[0]['contacts'].length > 0) {

                            datas[0].clinic_pazienti_report_export_v1[0]['contacts'].forEach(function(contact) {
                                var single_contact = {};
                                var cnt = 0;

                                for(var i = 0; i< datas[0].clinic_pazienti_report_export_v1[0]['fields'].length -1; i++){
                                    single_contact[datas[0].clinic_pazienti_report_export_v1[0]['fields'][i]] = contact[datas[0].clinic_pazienti_report_export_v1[0]['fields'][i]];
                                }

                                
                                if (contact.custom_fields && contact.custom_fields.length > 0) {
                                    for(var i = 0; i < contact.custom_fields.length -1; i++){
                                    
                                        single_contact[contact.custom_fields[i].name_field] = contact.custom_fields[i].valore;
                                    }
                                } 
                                
                                contacts.push(single_contact);
                                if (cnt_contact++ >= datas[0].clinic_pazienti_report_export_v1[0]['contacts'].length - 1) {

                                    callback(null, contacts);
                                }
                               

                            });
                        } else {
                            callback(null, []);
                        }
                    },
                    fields: function(callback) {
                        var fields = datas[0].clinic_pazienti_report_export_v1[0]['fields'];

                        var cnt_contact = 0;

                        datas[0].clinic_pazienti_report_export_v1[0]['contacts'][0]['custom_fields'].forEach(function(field){
                            fields.push(field.name_field);
                            if(cnt_contact ++ >= datas[0].clinic_pazienti_report_export_v1[0]['contacts'][0]['custom_fields'].length-1){
                                callback(null, fields);
                            }
                        });

                        
                         
                    },
                    field_names: function(callback) {
                        var fields = datas[0].clinic_pazienti_report_export_v1[0]['field_names'];

                        var cnt_contact = 0;

                         datas[0].clinic_pazienti_report_export_v1[0]['contacts'][0]['custom_fields'].forEach(function(field){
                            fields.push(field.name_field);
                            if(cnt_contact ++ >= datas[0].clinic_pazienti_report_export_v1[0]['contacts'][0]['custom_fields'].length-1){
                                callback(null, fields);
                            }
                        });
                       

                    }

                }, function(err, finals) {
                    if (err) {
                        console.log(err);
                    }

                    
                    /*var response = {
                        'data': finals.custom,
                        'fields': finals.fields,
                        'field_names': finals.field_names
                    };*/

                    json2csv({
                        data: finals.custom,
                        fields: finals.fields,
                        fieldNames: finals.field_names
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=report_pazienti.csv'
                        });
                        res.end(csv);
                    });

                });

            }else
            {
                res.status(404).json({});
            }

           
                /*if (err) {
                    console.log(err);
                    return res.status(500).send('Report pazienti' + err);
                }

                if(datas && datas[0] && datas[0].clinic_pazienti_report_export_v1){
                    var fields_array = datas[0].clinic_pazienti_report_export_v1[0]['fields'];
                    var fieldNames_array = datas[0].clinic_pazienti_report_export_v1[0]['field_names'];
                   
                    for(var i = 0; i < datas[0].clinic_pazienti_report_export_v1[0]['event_states'].length; i++){
                        fieldNames_array.push(datas[0].clinic_pazienti_report_export_v1[0]['event_states'][i]['status_name']);
                        fields_array.push(datas[0].clinic_pazienti_report_export_v1[0]['event_states'][i]['status_name']);
                    }

                    json2csv({
                        data: datas[0].clinic_pazienti_report_export_v1[0]['contacts'],
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=report_pazienti.csv'
                        });
                        res.end(csv);
                    });
                }else
                {
                    res.status(404).json({});
                }*/
                
            
        }).catch(function(err) {
            return res.status(400).render('clinic_pazienti_report_export: ' + err);
        }); 
    });
};


exports.clinic_report_pazienti_export_pdf = function(req, res){
    var querystring = require('querystring');

    var https = require('https');

    

    
    var postreq = 'organization='+req.headers['host'].split(".")[0];
    for (var key in req.query) {
        if (req.query.hasOwnProperty(key) && key != 'access_token') {
            postreq = postreq + "&" + key + "=" + req.query[key];
        }
    }

    

    var url = `https://asset.beebeeboard.com/pdf_create/api/clinic_report_pazienti_export_pdf.php?${postreq}&token=${req.query.access_token}&$_t=${moment().format('HHmmss')}`;
    console.log(url);
    var request = https.get(url, function(result) {

        var chunks = [];

        result.on('data', function(chunk) {
            chunks.push(chunk);
        });

        result.on('end', function() {
            var pdfData = new Buffer.concat(chunks);

            
            res.writeHead(200, {
                'Content-Disposition': `inline; filename=pazienti.pdf`,
                'Content-Length': pdfData.length
            });

            res.end(pdfData);
        });
    }).on('error', function(err) {

        return false;
    });

    request.end();
};

exports.clinic_report_specialisti_export = function(req, res){
    var json2csv = require('json2csv');

    utility.get_parameters(req, 'event', function(err, parameters) {
        sequelize.query('SELECT clinic_report_specialist_export(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ',\''+
            req.query.type+'\','+
            (req.query.with_paz ? req.query.with_paz : null) +','+
            (req.query.paied ? req.query.paied : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {
           
                if (err) {
                    console.log(err);
                    return res.status(500).send('Report Specialisti' + err);
                }

                if(datas && datas[0] && datas[0].clinic_report_specialist_export){
                    var fields_array = datas[0].clinic_report_specialist_export[0]['fields'];
                    var fieldNames_array = datas[0].clinic_report_specialist_export[0]['field_names'];
                

                    json2csv({
                        data: datas[0].clinic_report_specialist_export[0]['contacts'],
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=report_specialisti.csv'
                        });
                        res.end(csv);
                    });
                }else
                {
                    res.status(404).json({});
                }
                
            
        }).catch(function(err) {
            return res.status(400).render('clinic_specialisti_report_export: ' + err);
        }); 
    });
};

exports.clinic_report_specialist = function(req, res) {
    
    if (!req.user.role[0].json_build_object.attributes.event_list || !req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters(req, 'project', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_specialist(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ',\''+
            req.query.type+'\','+
            (req.query.with_paz ? req.query.with_paz : null) +','+
            (req.query.paied ? req.query.paied : null)+');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {

             if (datas[0].clinic_report_specialist) {
                res.json(datas[0].clinic_report_specialist);
            } else
                return res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_specialisti: ' + err);
        });
    });   
};

exports.clinic_report_documenti = function(req, res) {
    
    if (!req.user.role[0].json_build_object.attributes.event_list || !req.user.role[0].json_build_object.attributes.project_list) {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }

    utility.get_parameters(req, 'project', function(err, parameters) {

         if (err) {
             callback_(err, null);
         }

         sequelize.query('SELECT clinic_report_documenti(' +
            parameters.page_count + ',' +
            (parameters.page_num - 1) * parameters.page_count + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ','+
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.sortDirection + ',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            parameters.start_date + ',' +
            parameters.end_date + ',' +
            parameters.sortField + ',' +
            parameters.all_any + ',\''+
            req.query.type+'\',\''+
            (req.query.status ? req.query.status : null) +'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            })
        .then(function(datas) {

             if (datas && datas[0] && datas[0].clinic_report_documenti) {
                res.json(datas[0].clinic_report_documenti);
            } else
                return res.status(200).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_report_documenti: ' + err);
        });
    });   
};