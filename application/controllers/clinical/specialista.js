var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    finds = require('../../utility/finds.js'),
    logs = require('../../utility/logs.js'),
    skebby = require('../skebby.js');

/*
 * @apiUse IdParamEntryError
 */

exports.get_listino = function(req,res){
   
    var contact_id = req.query.contact_id;


    if(!contact_id){
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        sequelize.query('SELECT clinic_get_listino(' +
            contact_id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            req.user.role_id+');', {
                        
               raw: true,
               type: Sequelize.QueryTypes.SELECT,
               useMaster: true 
        })
        .then(function(datas) {

            if(datas && datas[0] && datas[0].clinic_get_listino){
                res.status(200).json(datas[0].clinic_get_listino);   
            }else
            {
                res.status(404).send({}); 
            }

        }).catch(function(err) {
            return res.status(400).render('clinic_get_listino: ' + err);
        });
    }
};


exports.post_listino = function(req,res){
   

    sequelize.query('SELECT clinic_post_listino(\''+
        JSON.stringify(req.body).replace(/'/g, "''''") +'\',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        req.user.role_id+');', {
                    
           raw: true,
           type: Sequelize.QueryTypes.SELECT,
           useMaster: true 
    })
    .then(function(datas) {

        res.status(200).json({});
        
    }).catch(function(err) {
        return res.status(400).render('clinic_post_listino: ' + err);
    });
    
};


exports.get_assurance_listino = function(req,res){
   
    var product_id = req.query.product_id;


    if(!product_id){
        return res.status(422).send(utility.error422('product_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        sequelize.query('SELECT clinic_get_assurance_listino(' +
            product_id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ','+
            req.user.role_id+');', {
                        
               raw: true,
               type: Sequelize.QueryTypes.SELECT,
               useMaster: true 
        })
        .then(function(datas) {

            if(datas && datas[0] && datas[0].clinic_get_assurance_listino){
                res.status(200).json(datas[0].clinic_get_assurance_listino);   
            }else
            {
                res.status(404).send({}); 
            }

        }).catch(function(err) {
            return res.status(400).render('clinic_get_assurance_listino: ' + err);
        });
    }
};


exports.post_assurance_listino = function(req,res){
   

    sequelize.query('SELECT clinic_post_assurance_listino(\''+
        JSON.stringify(req.body).replace(/'/g, "''''") +'\',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        req.user.role_id+');', {
                    
           raw: true,
           type: Sequelize.QueryTypes.SELECT,
           useMaster: true 
    })
    .then(function(datas) {

        res.status(200).json({});
        
    }).catch(function(err) {
        return res.status(400).render('clinic_post_assurance_listino: ' + err);
    });
    
};