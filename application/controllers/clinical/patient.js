var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js'),
    finds = require('../../utility/finds.js'),
    logs = require('../../utility/logs.js'),
    skebby = require('../skebby.js');

/*
 * @apiUse IdParamEntryError
 */

 exports.clinic_report_save_draft = function(req,res){
   
    sequelize.query('SELECT clinic_report_save_draft(\''+
        JSON.stringify(req.body).replace(/'/g, "''''") +'\',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ','+
        req.user.role_id+');', {
           raw: true,
           type: Sequelize.QueryTypes.SELECT,
           useMaster: true 
    })
    .then(function(datas) {

        res.status(200).json({});
        
    }).catch(function(err) {
        return res.status(400).render('clinic_report_save_draft: ' + err);
    });
    
};

exports.patch_patient_from_php = function(req,res){
   
    var contact_id = req.body.contact_id,
        nome = req.body.nome ? '\''+req.body.nome.replace(/'/g, "''''")+'\'' : null,
        cognome = req.body.cognome ? '\''+req.body.cognome.replace(/'/g, "''''")+'\'' : null,
        address_street = req.body.location_address_street ? '\''+req.body.location_address_street.replace(/'/g, "''''")+'\'' : null,
        civico = req.body.location_address_house ? '\''+req.body.location_address_house.replace(/'/g, "''''")+'\'' : null,
        cap = req.body.location_address_postal_code ? '\''+req.body.location_address_postal_code.replace(/'/g, "''''")+'\'' : null,
        comune = req.body.comune ? '\''+req.body.comune.replace(/'/g, "''''")+'\'' : null,
        nascita = req.body.nato_a ? '\''+req.body.nato_a.replace(/'/g, "''''")+'\'' : null,
        nascita_luogo = req.body.nato_il ? '\''+req.body.nato_il.replace(/'/g, "''''")+'\'' : null,
        mail = req.body.mail ? '\''+req.body.mail.replace(/'/g, "''''")+'\'' : null,
        mobile = req.body.mobile ? '\''+req.body.mobile.replace(/'/g, "''''")+'\'' : null,
        alert_data = req.body.alert_data ? req.body.alert_data : null,
        fiscale = req.body.fiscale ? '\''+req.body.fiscale.replace(/'/g, "''''")+'\'' : null,
        event_id = req.body.event_id ? req.body.event_id : null;


    if(!contact_id){
        return res.status(422).send(utility.error422('contact_id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    else{
        sequelize.query('SELECT clinic_triage_update_contact_v1(' +
            contact_id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            nascita +','+
            fiscale +','+
            address_street +','+
            civico +','+
            cap +','+
            comune +','+
            nascita_luogo+','+
            mail+','+
            mobile+','+
            nome+','+
            cognome+','+
            alert_data+','+
            event_id+');', {
                        
               raw: true,
               type: Sequelize.QueryTypes.SELECT,
               useMaster: true 
        })
        .then(function(datas) {

            res.status(200).json({});
            
        }).catch(function(err) {
            return res.status(400).render('clinic_triage_update_contact: ' + err);
        });
    }
};

exports.clinic_storico = function(req,res){
   
    var model = req.body.model ? '\''+req.body.model.replace(/'/g, "''''")+'\'' : null,
        name = req.body.name ? '\''+req.body.name.replace(/'/g, "''''")+'\'' : null,
        storico_id = req.body.storico_id ? req.body.storico_id : null,
        list_id = req.body.list_id ? req.body.list_id : null,
        todo_id = req.body.todo_id ? req.body.todo_id : null,
        date = req.body.date ? '\''+req.body.date+'\'' : null,
        is_completed = req.body.is_completed ? req.body.is_completed : null,
        action = req.body.action ? '\''+req.body.action.replace(/'/g, "''''")+'\'' : null;


    sequelize.query('SELECT clinic_storico(' +
        model + ',' +
        req.user._tenant_id + ',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id + ',' +
        storico_id +','+
        list_id +','+
        todo_id +','+
        action +','+
        date+','+
        is_completed+','+
        name+');', {
                    
           raw: true,
           type: Sequelize.QueryTypes.SELECT,
           useMaster: true 
    })
    .then(function(datas) {

        res.status(200).json({});
        
    }).catch(function(err) {
        return res.status(400).render('clinic_storico$9 : ' + err);
    });
    
};

exports.get_consensi = function(req,res){
   
    var contact_id = null,
        project_id = null;


    sequelize.query('SELECT get_consensi(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        (req.query.contact_id ? req.query.contact_id : null) + ','+
        (req.query.project_id ? req.query.project_id : null) +');', {
                    
           raw: true,
           type: Sequelize.QueryTypes.SELECT
    })
    .then(function(datas) {

        if(datas && datas[0] && datas[0].get_consensi && datas[0].get_consensi[0]){
            res.status(200).json(datas[0].get_consensi[0]);
        }else
        {
            res.status(200).json({});
        }
        
        
    }).catch(function(err) {
        return res.status(400).render('get_consensi : ' + err);
    });
    
};

exports.get_pacchetti = function(req,res){
   
    var contact_id = null,
        project_id = null;


    sequelize.query('SELECT get_pacchetti(\'' +
        req.headers['host'].split(".")[0] + '\',' +
        (req.query.contact_id ? req.query.contact_id : null) + ');', {
                    
           raw: true,
           type: Sequelize.QueryTypes.SELECT
    })
    .then(function(datas) {

        if(datas && datas[0] && datas[0].get_pacchetti && datas[0].get_pacchetti[0]){
            res.status(200).json(datas[0].get_pacchetti[0]);
        }else
        {
            res.status(200).json({});
        }
        
        
    }).catch(function(err) {
        return res.status(400).render('get_pacchetti : ' + err);
    });
    
};

exports.save_consensi = function(req,res){
   
   
    
    sequelize.query('SELECT save_consensi('+
        req.user._tenant_id+',\'' +
        req.headers['host'].split(".")[0] + '\',' +
        req.user.id +','+
        (req.body.contact_id ? req.body.contact_id : null) + ','+
        (req.body.project_id ? req.body.project_id : null) +','+
        (req.body.consenso.option_name ? '\''+req.body.consenso.option_name.replace(/'/g, "''''")+'\'' : null)+','+
        (req.body.state ? req.body.state : null)+','+
        (req.body.consenso.in_contact ? req.body.consenso.in_contact : null)+','+
        (req.body.consenso.in_project ? req.body.consenso.in_project : null)+');', {
                    
           raw: true,
           type: Sequelize.QueryTypes.SELECT,
           useMaster: true
    })
    .then(function(datas) {

        res.status(200).json({});
        
    }).catch(function(err) {
        return res.status(400).render('save_consensi : ' + err);
    });
    
};

exports.get_patient_info = function(req, res) {

    var util = require('util');
    var contact_id = req.params.id,
        start = null,
        end = null;

    if(!req.params.id || req.params.id === undefined){
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
        if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start = '\'' + req.query.from_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end = '\'' + req.query.to_date + '\'';
    }

    sequelize.query('SELECT clinical_patient_info(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            req.params.id + ',' +
            start + ',' +
            end + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].clinical_patient_info) {
                res.json(datas[0].clinical_patient_info[0]);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('clinical get patient info: ' + err);
        });
};

exports.get_patient_debiti_crediti = function(req, res) {

    var util = require('util');
    var limit = null,
        start = null,
        end = null,
        status = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
         if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start = '\'' + req.query.from_date + '\'';
    }

    if (req.query.limit_date !== undefined && req.query.limit_date !== '') {
        if (!moment(req.query.limit_date).isValid()) {
            return res.status(422).send(utility.error422('limit_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        limit = '\'' + req.query.limit_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end = '\'' + req.query.to_date + '\'';
    }

    if (req.query.status !== undefined && req.query.status !== '') {
        status = req.query.status;
    }

    sequelize.query('SELECT clinical_patient_debiti_crediti(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start + ',' +
            end + ',' +
            limit + ',' +
            status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].clinical_patient_debiti_crediti) {
                res.json(datas[0].clinical_patient_debiti_crediti);
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('clinical patient_debiti_crediti info: ' + err);
        });
};

exports.get_patient_debiti_crediti_csv = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    var limit = null,
        start = null,
        end = null,
        status = null;

    if (req.query.from_date !== undefined && req.query.from_date !== '') {
         if (!moment(req.query.from_date).isValid()) {
            return res.status(422).send(utility.error422('from_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        start = '\'' + req.query.from_date + '\'';
    }

    if (req.query.limit_date !== undefined && req.query.limit_date !== '') {
        if (!moment(req.query.limit_date).isValid()) {
            return res.status(422).send(utility.error422('limit_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        limit = '\'' + req.query.limit_date + '\'';
    }

    if (req.query.to_date !== undefined && req.query.to_date !== '') {
        if (!moment(req.query.to_date).isValid()) {
            return res.status(422).send(utility.error422('to_date', 'Parametro non valido', 'Parametro non valido', '422'));
        }

        end = '\'' + req.query.to_date + '\'';
    }

    if (req.query.status !== undefined && req.query.status !== '') {
        status = req.query.status;
    }

    sequelize.query('SELECT clinical_patient_debiti_crediti_export(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.user.role_id + ',' +
            start + ',' +
            end + ',' +
            limit + ',' +
            status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {
            if (datas[0].clinical_patient_debiti_crediti_export) {
                var fields_array = datas[0].clinical_patient_debiti_crediti_export[0]['fields'];
                var fieldNames_array = datas[0].clinical_patient_debiti_crediti_export[0]['field_names'];
                console.log(fields_array.length);
                console.log(fieldNames_array.length);
                json2csv({
                    data: datas[0].clinical_patient_debiti_crediti_export,
                    fields: fields_array,
                    fieldNames: fieldNames_array
                }, function(err, csv) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    }
                    res.writeHead(200, {
                        'Content-Type': 'text/csv',
                        'Content-Disposition': 'attachment; filename=debiti_crediti.csv'
                    });
                    res.end(csv);
                });
            } else
                return res.status(422).send({});
        }).catch(function(err) {
            return res.status(400).render('clinical patient_debiti_crediti export: ' + err);
        });
};

exports.clinic_conto = function(req, res) {

    var is_pagato = null,
        senza_maestro = null,
        senza_cliente = null,
        type_ = req.query.tipo;

    var response = {};

    utility.get_parameters_v1(req, 'event', function(err, parameters) {
        if (err) {
            return res.status(500).send(utility.error403('get parameters', 'Non hai i diritti', '500'));

        }

        if (req.query.sent !== undefined && req.query.sent !== '') {
            switch (parseInt(req.query.sent)) {
                case 0:
                    is_pagato = null;
                    break;

                case 1:
                    is_pagato = true;
                    break;

                case 2:
                    is_pagato = false;
                    break;

                default:
                    is_pagato = null;
                    break;
            }

        }

        if (req.query.gdpr !== undefined && req.query.gdpr !== '') {
            switch (parseInt(req.query.gdpr)) {
                case 0: //INVOICE
                    senza_maestro = null; 
                    senza_cliente = null;
                    break;

                case 1: //RECEIPT
                    senza_maestro = null;
                    senza_cliente = null;
                    break;

                case 2: //NOTA CREDITO
                    senza_maestro = null;
                    senza_cliente = true;
                    break;

                case 3: //NOTA CREDITO
                    senza_maestro = null;
                    senza_cliente = false;
                    break;

                case 4: //RECEIPT
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                default:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;
            }
        }

        async.parallel({
            event: function(callback) {
                if(type_ == 'events'){
                    sequelize.query('SELECT clinic_events_not_payed_v4(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        req.user.id + ',' +
                        req.user.role_id + ',null,' +
                        parameters.all_any + ',' +
                        (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                        (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                        is_pagato + ',' +
                        senza_maestro + ',' +
                        senza_cliente + ',' +
                        parameters.page_count + ',' +
                        (parameters.page_num - 1) * parameters.page_count + ',' +
                        parameters.sortDirection + ',' +
                        parameters.sortField + ','+
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                        parameters.archivied +');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT
                        })
                    .then(function(datas) {

                        sequelize.query('SELECT clinic_all_events_meta_v4(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                parameters.start_date + ',' +
                                parameters.end_date + ',' +
                                req.user.id + ',' +
                                req.user.role_id + ',null,' +
                                parameters.all_any + ',' +
                                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                is_pagato + ',' +
                                senza_maestro + ',' +
                                senza_cliente + ','+
                                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                                parameters.archivied +');')
                            .then(function(counter) {
                                if (datas && datas[0] && datas[0].clinic_events_not_payed_v4 && datas[0].clinic_events_not_payed_v4 != null) {
                                    response = {
                                        'data': datas[0].clinic_events_not_payed_v4,
                                        'meta': {
                                            'total_items': parseInt(counter[0][0].clinic_all_events_meta_v4.tot_items),
                                            'actual_page': parseInt(parameters.page_num),
                                            'total_pages': parseInt(counter[0][0].clinic_all_events_meta_v4.tot_items / parameters.page_count) + 1,
                                            'item_per_page': parseInt(parameters.page_count),
                                            'tot_all_number': parseInt(counter[0][0].clinic_all_events_meta_v4.tot_items_tutti)
                                        }
                                    };

                                    callback(null, response);
                                } else {

                                    response = {
                                        'data': [],
                                        'meta': {
                                            'total_items': parseInt(counter[0][0].clinic_all_events_meta_v4.tot_items),
                                            'actual_page': parseInt(parameters.page_num),
                                            'total_pages': parseInt(counter[0][0].clinic_all_events_meta_v4.tot_items / parameters.page_count) + 1,
                                            'item_per_page': parseInt(parameters.page_count),
                                            'tot_all_number': parseInt(counter[0][0].clinic_all_events_meta_v4.tot_items_tutti)
                                        }
                                    };
                                    callback(null, response);
                                }
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });



                    }).catch(function(err) {
                        callback(err, null);
                    });
                }
                else{
                    callback(null, {});
                }
                

            },
            pack: function(callback) {
                if(type_ == 'pacchetti'){
                    sequelize.query('SELECT clinic_packs_not_payed_v4(' +
                        req.user._tenant_id + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        parameters.start_date + ',' +
                        parameters.end_date + ',' +
                        req.user.id + ',' +
                        req.user.role_id + ',null,' +
                        parameters.all_any + ',' +
                        (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                        (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                        is_pagato + ',' +
                        senza_maestro + ',' +
                        senza_cliente + ',' +
                        parameters.page_count + ',' +
                        (parameters.page_num - 1) * parameters.page_count + ',' +
                        parameters.sortDirection + ',' +
                        parameters.sortField + ','+
                        (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                        parameters.archivied +');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT
                        })
                    .then(function(datas) {

                        sequelize.query('SELECT clinic_all_packs_meta_v4(' +
                                req.user._tenant_id + ',\'' +
                                req.headers['host'].split(".")[0] + '\',' +
                                parameters.start_date + ',' +
                                parameters.end_date + ',' +
                                req.user.id + ',' +
                                req.user.role_id + ',null,' +
                                parameters.all_any + ',' +
                                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                is_pagato + ',' +
                                senza_maestro + ',' +
                                senza_cliente + ','+
                                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                                parameters.archivied +');')
                            .then(function(counter) {
                                if (datas && datas[0] && datas[0].clinic_packs_not_payed_v4 && datas[0].clinic_packs_not_payed_v4 != null) {
                                    response = {
                                        'data': datas[0].clinic_packs_not_payed_v4,
                                        'meta': {
                                            'total_items': parseInt(counter[0][0].clinic_all_packs_meta_v4.tot_items),
                                            'actual_page': parseInt(parameters.page_num),
                                            'total_pages': parseInt(counter[0][0].clinic_all_packs_meta_v4.tot_items / parameters.page_count) + 1,
                                            'item_per_page': parseInt(parameters.page_count),
                                            'tot_all_number': parseInt(counter[0][0].clinic_all_packs_meta_v4.tot_items_tutti)
                                        }
                                    };

                                    callback(null, response);
                                } else {

                                    response = {
                                        'data': [],
                                        'meta': {
                                            'total_items': parseInt(counter[0][0].clinic_all_packs_meta_v4.tot_items),
                                            'actual_page': parseInt(parameters.page_num),
                                            'total_pages': parseInt(counter[0][0].clinic_all_packs_meta_v4.tot_items / parameters.page_count) + 1,
                                            'item_per_page': parseInt(parameters.page_count),
                                            'tot_all_number': parseInt(counter[0][0].clinic_all_packs_meta_v4.tot_items_tutti)
                                        }
                                    };
                                    callback(null, response);
                                }
                            }).catch(function(err) {
                                console.log(err);
                                callback(err, null);
                            });



                    }).catch(function(err) {
                        callback(err, null);
                    });
                }
                else{
                    callback(null, {});
                }
                

            },
            invoice: function(callback) {
                if(type_ == 'invoices'){
                    sequelize.query('SELECT clinic_invoices_not_payed_v4(' +
                            req.user._tenant_id + ',\'' +
                            req.headers['host'].split(".")[0] + '\',' +
                            parameters.start_date + ',' +
                            parameters.end_date + ',' +
                            req.user.id + ',' +
                            req.user.role_id + ',null,' +
                            parameters.all_any + ',' +
                            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                            is_pagato + ',' +
                            senza_maestro + ',' +
                            senza_cliente + ',' +
                            parameters.page_count + ',' +
                            (parameters.page_num - 1) * parameters.page_count + ',' +
                            parameters.sortDirection + ',' +
                            parameters.sortField + ','+
                            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT
                            })
                        .then(function(datas) {
                            sequelize.query('SELECT clinic_all_invoices_meta_v4(' +
                                    req.user._tenant_id + ',\'' +
                                    req.headers['host'].split(".")[0] + '\',' +
                                    parameters.start_date + ',' +
                                    parameters.end_date + ',' +
                                    req.user.id + ',' +
                                    req.user.role_id + ',null,' +
                                    parameters.all_any + ',' +
                                    (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                                    (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                                    is_pagato + ',' +
                                    senza_maestro + ',' +
                                    senza_cliente + ','+
                                    (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');')
                                .then(function(counter) {
                                    if (datas && datas[0] && datas[0].clinic_invoices_not_payed_v4 && datas[0].clinic_invoices_not_payed_v4 != null) {
                                        response = {
                                            'data': datas[0].clinic_invoices_not_payed_v4,
                                            'meta': {
                                                'total_items': parseInt(counter[0][0].clinic_all_invoices_meta_v4.tot_items),
                                                'actual_page': parseInt(parameters.page_num),
                                                'total_pages': parseInt(counter[0][0].clinic_all_invoices_meta_v4.tot_items / parameters.page_count) + 1,
                                                'item_per_page': parseInt(parameters.page_count),
                                                'tot_all_number': parseInt(counter[0][0].clinic_all_invoices_meta_v4.tot_items_tutti)
                                            }
                                        };

                                        callback(null, response);
                                    } else {

                                        response = {
                                            'data': [],
                                            'meta': {
                                                'total_items': parseInt(counter[0][0].clinic_all_invoices_meta_v4.tot_items),
                                                'actual_page': parseInt(parameters.page_num),
                                                'total_pages': parseInt(counter[0][0].clinic_all_invoices_meta_v4.tot_items / parameters.page_count) + 1,
                                                'item_per_page': parseInt(parameters.page_count),
                                                'tot_all_number': parseInt(counter[0][0].clinic_all_invoices_meta_v4.tot_items_tutti)
                                            }
                                        };
                                        callback(null, response);
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                    callback(err, null);
                                });

                        }).catch(function(err) {
                            callback(err, null);
                        });
                    }
                    else{
                        callback(null, {});
                    }
            }

        }, function(err, results) {
            if (err) {
                console.log(err);
                return res.status(500).send('Clinic_not_payed ' + err);
            }

            res.status(200).json({
                'events': results.event,
                'invoices': results.invoice,
                'pacchetti': results.pack
            });
        });

    });
};

exports.clinic_conto_csv = function(req, res) {
    var json2csv = require('json2csv');
    var util = require('util');
    var is_pagato = null,
        senza_maestro = null,
        senza_cliente = null,
        type_ = req.query.tipo;

    var response = {};

    utility.get_parameters_v1(req, 'event', function(err, parameters) {
        if (err) {
            return res.status(500).send(utility.error403('get parameters', 'Non hai i diritti', '500'));

        }

        if (req.query.sent !== undefined && req.query.sent !== '') {
            switch (parseInt(req.query.sent)) {
                case 0:
                    is_pagato = null;
                    break;

                case 1:
                    is_pagato = true;
                    break;

                case 2:
                    is_pagato = false;
                    break;

                default:
                    is_pagato = null;
                    break;
            }

        }

        if (req.query.gdpr !== undefined && req.query.gdpr !== '') {
            switch (parseInt(req.query.gdpr)) {
                case 0: //INVOICE
                    senza_maestro = null; 
                    senza_cliente = null;
                    break;

                case 1: //RECEIPT
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                case 2: //NOTA CREDITO
                    senza_maestro = null;
                    senza_cliente = true;
                    break;

                case 3: //NOTA CREDITO
                    senza_maestro = null;
                    senza_cliente = false;
                    break;

                case 4: //RECEIPT
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                default:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;
            }
        }

       
        if(type_ == 'events'){
            sequelize.query('SELECT clinic_events_not_payed_export(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.id + ',' +
                req.user.role_id + ',null,' +
                parameters.all_any + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                is_pagato + ',' +
                senza_maestro + ',' +
                senza_cliente + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                parameters.sortDirection + ',' +
                parameters.sortField + ','+
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                parameters.archivied +');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if (datas[0].clinic_events_not_payed_export) {
                    var fields_array = datas[0].clinic_events_not_payed_export[0]['fields'];
                    var fieldNames_array = datas[0].clinic_events_not_payed_export[0]['field_names'];
                    console.log(fields_array.length);
                    console.log(fieldNames_array.length);
                    json2csv({
                        data: datas[0].clinic_events_not_payed_export,
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=appuntamenti.csv'
                        });
                        res.end(csv);
                    });
                } else
                    return res.status(422).send({});



            }).catch(function(err) {
                return res.status(500).send(err);
            });
        } else if(type_ == 'pacchetti'){
            sequelize.query('SELECT clinic_packs_not_payed_export(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.id + ',' +
                req.user.role_id + ',null,' +
                parameters.all_any + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                is_pagato + ',' +
                senza_maestro + ',' +
                senza_cliente + ',' +
                parameters.page_count + ',' +
                (parameters.page_num - 1) * parameters.page_count + ',' +
                parameters.sortDirection + ',' +
                parameters.sortField + ','+
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                parameters.archivied +');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
                })
            .then(function(datas) {

                if (datas[0].clinic_packs_not_payed_export) {
                    var fields_array = datas[0].clinic_packs_not_payed_export[0]['fields'];
                    var fieldNames_array = datas[0].clinic_packs_not_payed_export[0]['field_names'];
                    console.log(fields_array.length);
                    console.log(fieldNames_array.length);
                    json2csv({
                        data: datas[0].clinic_packs_not_payed_export,
                        fields: fields_array,
                        fieldNames: fieldNames_array
                    }, function(err, csv) {
                        if (err) {
                            console.log(err);
                            return res.status(500).send(err);
                        }
                        res.writeHead(200, {
                            'Content-Type': 'text/csv',
                            'Content-Disposition': 'attachment; filename=appuntamenti.csv'
                        });
                        res.end(csv);
                    });
                } else
                    return res.status(422).send({});




            }).catch(function(err) {
                 return res.status(500).send(err);
            });
        } else if(type_ == 'invoices'){
            sequelize.query('SELECT clinic_invoices_not_payed_export(' +
                    req.user._tenant_id + ',\'' +
                    req.headers['host'].split(".")[0] + '\',' +
                    parameters.start_date + ',' +
                    parameters.end_date + ',' +
                    req.user.id + ',' +
                    req.user.role_id + ',null,' +
                    parameters.all_any + ',' +
                    (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                    (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                    is_pagato + ',' +
                    senza_maestro + ',' +
                    senza_cliente + ',' +
                    parameters.page_count + ',' +
                    (parameters.page_num - 1) * parameters.page_count + ',' +
                    parameters.sortDirection + ',' +
                    parameters.sortField + ','+
                    (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT
                    })
                .then(function(datas) {
                    if (datas[0].clinic_invoices_not_payed_export) {
                        var fields_array = datas[0].clinic_invoices_not_payed_export[0]['fields'];
                        var fieldNames_array = datas[0].clinic_invoices_not_payed_export[0]['field_names'];
                        console.log(fields_array.length);
                        console.log(fieldNames_array.length);
                        json2csv({
                            data: datas[0].clinic_invoices_not_payed_export,
                            fields: fields_array,
                            fieldNames: fieldNames_array
                        }, function(err, csv) {
                            if (err) {
                                console.log(err);
                                return res.status(500).send(err);
                            }
                            res.writeHead(200, {
                                'Content-Type': 'text/csv',
                                'Content-Disposition': 'attachment; filename=appuntamenti.csv'
                            });
                            res.end(csv);
                        });
                    } else
                        return res.status(422).send({});

                }).catch(function(err) {
                     return res.status(500).send(err);
                });
            }else
            {
                 return res.status(404).send({});
            }
                    

    });
};

exports.clinic_stats = function(req, res) {

    var is_pagato = null,
        senza_maestro = null,
        senza_cliente = null;

    var response = {};

    utility.get_parameters_v1(req, 'event', function(err, parameters) {
       
        if (err) {
            return res.status(500).send(utility.error403('get parameters', 'Non hai i diritti', '500'));

        }

        if (req.query.sent !== undefined && req.query.sent !== '') {
            switch (parseInt(req.query.sent)) {
                case 0:
                    is_pagato = null;
                    break;

                case 1:
                    is_pagato = true;
                    break;

                case 2:
                    is_pagato = false;
                    break;

                default:
                    is_pagato = null;
                    break;
            }

        }

        if (req.query.gdpr !== undefined && req.query.gdpr !== '') {
            switch (parseInt(req.query.gdpr)) {
                case 0: //INVOICE
                    senza_maestro = null; 
                    senza_cliente = null;
                    break;

                case 1: //RECEIPT
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                case 2: //NOTA CREDITO
                    senza_maestro = null;
                    senza_cliente = true;
                    break;

                case 3: //NOTA CREDITO
                    senza_maestro = null;
                    senza_cliente = false;
                    break;

                 case 4: //RECEIPT
                    senza_maestro = true;
                    senza_cliente = null;
                    break;

                default:
                    senza_maestro = null;
                    senza_cliente = null;
                    break;
            }
        }

        sequelize.query('SELECT clinic_stats(' +
                req.user._tenant_id + ',\'' +
                req.headers['host'].split(".")[0] + '\',' +
                parameters.start_date + ',' +
                parameters.end_date + ',' +
                req.user.id + ',' +
                req.user.role_id + ',null,' +
                parameters.all_any + ',' +
                (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
                (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ',' +
                is_pagato + ',' +
                senza_maestro + ',' +
                senza_cliente + ','+
                (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) +','+
                parameters.archivied +');')
            .then(function(datas) {
                res.json({
                    'meta': datas[0][0].clinic_stats
                });
            }).catch(function(err) {
                return res.status(400).render('clinic_stats: ' + err);
            });
    });
};

exports.sesso_chart = function(req, res) {
    if (req.query.year === undefined || req.query.year === '' || req.query.status === undefined || req.query.status === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    sequelize.query('SELECT clinic_sesso_chart_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            (req.query.month ? req.query.month : null) + ',' +
            req.user.id + ',' +
            req.query.status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if (datas && datas[0] && datas[0].clinic_sesso_chart_v2 && datas[0].clinic_sesso_chart_v2[0] && datas[0].clinic_sesso_chart_v2[0].Sesso) {
                var cnt = 0;
                var categorie = {};
                datas[0].clinic_sesso_chart_v2[0].Sesso.forEach(function(cat) {
                    if (!cat.value)
                        cat.value = 'Non categorizzato';

                    categorie[cat.value] = [];
                    categorie[cat.value].push(cat.tot);
                    if (cnt++ >= datas[0].clinic_sesso_chart_v2[0].Sesso.length - 1) {
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('clinic_sesso_chart_v2: ' + err);
        });
};

exports.corpo_chart = function(req, res) {
    if (req.query.year === undefined || req.query.year === '' || req.query.status === undefined || req.query.status === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    sequelize.query('SELECT clinic_corpo_chart_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            (req.query.month ? req.query.month : null) + ',' +
            req.user.id + ',' +
            req.query.status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if (datas && datas[0] && datas[0].clinic_corpo_chart_v2 && datas[0].clinic_corpo_chart_v2[0] && datas[0].clinic_corpo_chart_v2[0].Corpo) {
                var cnt = 0;
                var categorie = {};
                categorie.data = {};
                categorie.data.columns = [];
                var columns = [];
                columns[0] = (req.query.status_name ? req.query.status_name : null);
                categorie.axis = {};
                categorie.axis.x = {
                    type: 'category',
                    categories: []
                };
                datas[0].clinic_corpo_chart_v2[0].Corpo.forEach(function(cat) {
                    if (!cat.title)
                        cat.title = 'Non categorizzato';

                    columns.push(cat.tot);
                    categorie.axis.x.categories.push(cat.title);
                    if (cnt++ >= datas[0].clinic_corpo_chart_v2[0].Corpo.length - 1) {
                        categorie.data.columns.push(columns);
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('clinic_corpo_chart_v2: ' + err);
        });
};

exports.sintomi_chart = function(req, res) {
    if (req.query.year === undefined || req.query.year === '' || req.query.status === undefined || req.query.status === '') {
        return res.status(403).send(utility.error403('Non hai i diritti', 'Non hai i diritti', '401'));
    }


    sequelize.query('SELECT clinic_sintomi_chart_v2(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.query.year + ',' +
            (req.query.month ? req.query.month : null) + ',' +
            req.user.id + ',' +
            req.query.status + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

            if (datas && datas[0] && datas[0].clinic_sintomi_chart_v2 && datas[0].clinic_sintomi_chart_v2[0] && datas[0].clinic_sintomi_chart_v2[0].Sintomi) {
                var cnt = 0;
                var categorie = {};
                categorie.data = {};
                categorie.data.columns = [];
                var columns = [];
                columns[0] = (req.query.status_name ? req.query.status_name : null);
                categorie.axis = {};
                categorie.axis.x = {
                    type: 'category',
                    categories: []
                };
                datas[0].clinic_sintomi_chart_v2[0].Sintomi.forEach(function(cat) {
                    if (!cat.title)
                        cat.title = 'Non categorizzato';

                    columns.push(cat.tot);
                    categorie.axis.x.categories.push(cat.name);
                    if (cnt++ >= datas[0].clinic_sintomi_chart_v2[0].Sintomi.length - 1) {
                        categorie.data.columns.push(columns);
                        res.status(200).send(
                            categorie
                        );
                    }
                });
            } else {
                res.status(200).send({});
            }


        }).catch(function(err) {
            return res.status(400).render('clinic_sintomi_chart_v2: ' + err);
        });
};

exports.clinic_files = function(req, res) {
    if (req.query.project_id === undefined || req.query.project_id === '' ) {
        return res.status(422).send(utility.error422('Parametro non valido', 'Parametro non valido', '422'));
    }

    sequelize.query('SELECT clinic_files(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            req.query.project_id  + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

             if (datas[0].clinic_files) {
                res.json(datas[0].clinic_files);
            } else
                return res.status(422).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_files: ' + err);
        });
};


exports.copy_clinic = function(req, res) {
    var util = require('util');
    var ids = [];

    if (req.body.ids !== undefined && req.body.ids != '') {
        if (util.isArray(req.body.ids)) {
            ids = req.body.ids;
        } else {
            ids.push(req.body.ids);
        }
    }

    sequelize.query('SELECT clinic_copy(' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ',' +
            (ids.length > 0 ? 'ARRAY[' + ids + ']::bigint[]' : null)  + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

             if (datas[0].clinic_copy) {
                res.json(datas[0].clinic_copy);
            } else
                return res.status(422).send({});

        }).catch(function(err) {
            return res.status(400).render('clinic_copy: ' + err);
        });
};

exports.farmaci = function(req, res) {

    var q1 = [];

    if (req.query.q !== undefined && req.query.q !== '') {
        let new_str = req.query.q.replace(/[^a-z0-9]/gi, ' ');
        var q_array = new_str.split(' ');
        
        q_array.forEach(function(s) {
            
            q1.push('\'%' + s.replace(/'/g, "''''") + '%\'');
        });
        
    }

    sequelize.query('SELECT get_farmaci(' +
            
            (q1 && q1.length > 0 ? 'ARRAY[' + q1 + ']::bigint[]' : null)  + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            })
        .then(function(datas) {

             if (datas[0].get_farmaci) {
                res.json(datas[0].get_farmaci);
            } else
                return res.status(422).send({});

        }).catch(function(err) {
            return res.status(400).render('get_farmaci: ' + err);
        });
};