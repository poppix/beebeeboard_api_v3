var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../../utility/utility.js');



exports.beebeehealth_events_slot_calendar = function(req, res) {
    var start_date = null,
        end_date = null,
        q = null,
        contact_id = null,
        product_id = null;

    var moment = require('moment');
    var moment_timezone = require('moment-timezone');

    utility.get_parameters_v1(req, 'project', function(err, parameters) {
         if (err) {
             callback_(err, null);
         }

        sequelize.query('SELECT beebeehealth_events_slot_calendar(' +
            req.user._tenant_id + ',' +
            (parameters.q1 && parameters.q1.length > 0 ? 'ARRAY[' + parameters.q1 + ']' : null) + ',' +
            parameters.start_date+ ',' +
            parameters.end_date+ ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            (parameters.contact_ids && parameters.contact_ids.length > 0 ? 'ARRAY[' + parameters.contact_ids + ']::bigint[]' : null) + ',' +
            (parameters.product_ids && parameters.product_ids.length > 0 ? 'ARRAY[' + parameters.product_ids + ']::bigint[]' : null) + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {
            if (datas[0].beebeehealth_events_slot_calendar && datas[0].beebeehealth_events_slot_calendar != null) {
                res.status(200).send(datas[0].beebeehealth_events_slot_calendar);

            } else {
                console.log('ciao');
                res.status(200).send('{}');
            }
        }).catch(function(err) {
            return res.status(400).render('datas[0].beebeehealth_events_slot_calendar: ' + err);
        });
     });  


};