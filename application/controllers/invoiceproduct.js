var moment = require('moment'),
    Sequelize = require('sequelize'),
    async = require('async'),
    utility = require('../utility/utility.js'),
    finds = require('../utility/finds.js'),
    logs = require('../utility/logs.js');


exports.find = function(req, res) {

    finds.find_invoiceproduct(req, function(err, results) {
        if (err) {
            return res.status(err.errors[0].status).send(err);
        } else {
            res.json(results);
        }
    });
};
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New contact, 2 different cases: single contact created or multiple contacts imported
 */
exports.create = function(req, res) {
    var invoiceproduct = {};

    if (req.body.data.attributes !== undefined) {
        // Import case

        invoiceproduct['name'] = (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '');
        invoiceproduct['description'] = (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '');
        invoiceproduct['price'] = req.body.data.attributes.price;
        invoiceproduct['qta'] = req.body.data.attributes.qta;
        invoiceproduct['importo'] = req.body.data.attributes.importo;
        invoiceproduct['unit'] = (req.body.data.attributes.unit && utility.check_type_variable(req.body.data.attributes.unit, 'string') && req.body.data.attributes.unit !== null ? req.body.data.attributes.unit.replace(/'/g, "''''") : '');
        invoiceproduct['rank'] = req.body.data.attributes.rank;
        invoiceproduct['product_id'] = ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null);
        invoiceproduct['event_id'] = ((req.body.data.relationships && req.body.data.relationships.event && req.body.data.relationships.event.data && req.body.data.relationships.event.data.id && !utility.check_id(req.body.data.relationships.event.data.id)) ? req.body.data.relationships.event.data.id : null);
        invoiceproduct['project_id'] = ((req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data && req.body.data.relationships.project.data.id && !utility.check_id(req.body.data.relationships.project.data.id)) ? req.body.data.relationships.project.data.id : null);
        invoiceproduct['iva_natura'] = (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '');
        invoiceproduct['iva_rate'] = req.body.data.attributes.iva_rate;
        invoiceproduct['ritenuta'] = req.body.data.attributes.ritenuta;
        invoiceproduct['sdi_field'] = (req.body.data.attributes.sdi_field && utility.check_type_variable(req.body.data.attributes.sdi_field, 'string') && req.body.data.attributes.sdi_field !== null ? req.body.data.attributes.sdi_field.replace(/'/g, "''''") : null);
        

        sequelize.query('SELECT insert_invoiceproduct_v2(\'' + JSON.stringify(invoiceproduct) + '\',\'' +
                req.headers['host'].split(".")[0] + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                })
            .then(function(datas) {
                if (datas[0].insert_invoiceproduct_v2 && datas[0].insert_invoiceproduct_v2 != null) {
                    req.body.data.id = datas[0].insert_invoiceproduct_v2;

                    utility.sanitizeRelations({
                        'organization': req.headers['host'].split(".")[0],
                        '_tenant': req.user._tenant_id,
                        'new_id': datas[0].insert_invoiceproduct_v2,
                        'body': req.body.data.relationships,
                        'model': 'invoiceproduct',
                        'user_id': req.user.id,
                        'array': ['invoiceproduct_taxe']
                    }, function(err, results) {
                        logs.save_log_model(
                            req.user._tenant_id,
                            req.headers['host'].split(".")[0],
                            'invoiceproduct',
                            'insert',
                            req.user.id,
                            JSON.stringify(req.body.data.attributes),
                            datas[0].insert_invoiceproduct_v2,
                            'Aggiunto elemento in fattura ' + datas[0].insert_invoiceproduct_v2
                        );

                        res.json({
                            'data': req.body.data,
                            'relationships': req.body.data.relationships
                        });
                    });
                }

            }).catch(function(err) {
                //next(new Error(err));
                return res.status(400).render('invoiceproduct_insert: ' + err);

            });
    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing eventstatus searched by id
 */
exports.update = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    if (req.body.data.attributes !== undefined) {
        // Import case
        sequelize.query('SELECT delete_invoiceproduct_relations_v2(' + req.params.id + ',\'' + req.headers['host'].split(".")[0] + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
            .then(function(datas) {
                sequelize.query('SELECT update_invoiceproduct_v2(' +
                        req.params.id +
                        ',\'' + (req.body.data.attributes.name && utility.check_type_variable(req.body.data.attributes.name, 'string') && req.body.data.attributes.name !== null ? req.body.data.attributes.name.replace(/'/g, "''''") : '') +
                        '\',\'' + (req.body.data.attributes.description && utility.check_type_variable(req.body.data.attributes.description, 'string') && req.body.data.attributes.description !== null ? req.body.data.attributes.description.replace(/'/g, "''''") : '') +
                        '\',\'' + (req.body.data.attributes.unit && utility.check_type_variable(req.body.data.attributes.unit, 'string') && req.body.data.attributes.unit !== null ? req.body.data.attributes.unit.replace(/'/g, "''''") : '') +
                        '\',' + req.body.data.attributes.price +
                        ',' + req.body.data.attributes.qta +
                        ',' + req.body.data.attributes.importo +
                        ',' + req.body.data.attributes.rank + ',' +
                        ((req.body.data.relationships && req.body.data.relationships.product && req.body.data.relationships.product.data && req.body.data.relationships.product.data.id && !utility.check_id(req.body.data.relationships.product.data.id)) ? req.body.data.relationships.product.data.id : null) + ',\'' +
                        req.headers['host'].split(".")[0] + '\',' +
                        ((req.body.data.relationships && req.body.data.relationships.event && req.body.data.relationships.event.data && req.body.data.relationships.event.data.id && !utility.check_id(req.body.data.relationships.event.data.id)) ? req.body.data.relationships.event.data.id : null) + ','+
                        ((req.body.data.relationships && req.body.data.relationships.project && req.body.data.relationships.project.data && req.body.data.relationships.project.data.id && !utility.check_id(req.body.data.relationships.project.data.id)) ? req.body.data.relationships.project.data.id : null) +',\'' + 
                        (req.body.data.attributes.iva_natura && utility.check_type_variable(req.body.data.attributes.iva_natura, 'string') && req.body.data.attributes.iva_natura !== null ? req.body.data.attributes.iva_natura.replace(/'/g, "''''") : '') + '\','+
                        req.body.data.attributes.iva_rate + ','+
                        req.body.data.attributes.ritenuta + ',\'' +
                        (req.body.data.attributes.sdi_field && utility.check_type_variable(req.body.data.attributes.sdi_field, 'string') && req.body.data.attributes.sdi_field !== null ? req.body.data.attributes.sdi_field.replace(/'/g, "''''") : '')+'\','+
                        ((req.body.data.relationships && req.body.data.relationships.category && req.body.data.relationships.category.data && req.body.data.relationships.category.data.id && !utility.check_id(req.body.data.relationships.category.data.id)) ? req.body.data.relationships.category.data.id : null) + ','+
                        ((req.body.data.relationships && req.body.data.relationships.sub_category && req.body.data.relationships.sub_category.data && req.body.data.relationships.sub_category.data.id && !utility.check_id(req.body.data.relationships.sub_category.data.id)) ? req.body.data.relationships.sub_category.data.id : null) + ');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        })
                    .then(function(datas) {

                        utility.sanitizeRelations({
                            'organization': req.headers['host'].split(".")[0],
                            '_tenant': req.user._tenant_id,
                            'new_id': req.params.id,
                            'body': req.body.data.relationships,
                            'model': 'invoiceproduct',
                            'user_id': req.user.id,
                            'array': ['invoiceproduct_taxe']
                        }, function(err, results) {

                            logs.save_log_model(
                                req.user._tenant_id,
                                req.headers['host'].split(".")[0],
                                'invoiceproduct',
                                'update',
                                req.user.id,
                                JSON.stringify(req.body.data.attributes),
                                req.params.id,
                                'Modificato elemento in fattura ' + req.params.id
                            );

                            res.json({
                                'data': req.body.data,
                                'relationships': req.body.data.relationships
                            });
                        });

                    }).catch(function(err) {
                        return res.status(400).render('invoiceproduct_update: ' + err);
                    });
            }).catch(function(err) {
                return res.status(400).render('invoiceproduct_update: ' + err);
            });

    } else {
        res.json({
            'data': []
        });
    }
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing eventstatus searched by id
 */
exports.destroy = function(req, res) {
    if (utility.check_id(req.params.id)) {
        return res.status(422).send(utility.error422('id', 'Parametro non valido', 'Parametro non valido', '422'));
    }
    sequelize.query('SELECT delete_invoiceproduct_v1(' +
            req.params.id + ',' +
            req.user._tenant_id + ',\'' +
            req.headers['host'].split(".")[0] + '\',' +
            req.user.id + ');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            })
        .then(function(datas) {

            res.json({
                'data': {
                    'type': 'invoiceproducts',
                    'id': datas[0].delete_invoiceproduct_v1
                }
            });

        }).catch(function(err) {
            return res.status(400).render('invoiceproduct_destroy: ' + err);
        });
};