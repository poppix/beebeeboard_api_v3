var dbox = require("dbox");
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Dropbox communication API
 */
exports.read = function(req, res) {

    var drop_app = dbox.app({
        app_key: config.dropbox.app_key,
        app_secret: config.dropbox.app_secret
    });

    drop_app.requesttoken(function(status, request_token) {
        console.log(request_token);
        res.send({
            'token': request_token
        });
    });
    /*
    	var request_token = { oauth_token_secret: 'VYP0Os1IXmwMAtBg',
      oauth_token: 'ldSJ2lM91wqMKBOJ',
      authorize_url: 'https://www.dropbox.com/1/oauth/authorize?oauth_token=ldSJ2lM91wqMKBOJ' }
    	
    	drop_app.accesstoken(request_token, function(status, access_token){
    			  console.log(access_token);
    			  var client = drop_app.client(access_token);
    			  client.account(function(status, reply){
    			 	 console.log(reply);
    			 	
    			 });
    			});*/


};