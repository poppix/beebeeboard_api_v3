/**
 * Module dependencies.
 */
var oauth2orize = require('./oauth2_domain'),
    passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth20').Strategy,
    login = require('connect-ensure-login'),
    config = require('./config'),
    utils = require('./utils'),
    Sequelize = require('sequelize');
var bcrypt = require('bcryptjs');
// create OAuth 2.0 server
var server = oauth2orize.createServer();

// Register supported grant types.
//
// OAuth 2.0 specifies a framework that allows users to grant client
// applications limited access to their protected resources.  It does this
// through a process of the user granting access, and the client exchanging
// the grant for an access token.

/**
 * Grant authorization codes
 *
 * The callback takes the `client` requesting authorization, the `redirectURI`
 * (which is used as a verifier in the subsequent exchange), the authenticated
 * `user` granting access, and their response, which contains approved scope,
 * duration, etc. as parsed by the application.  The application issues a code,
 * which is bound to these values, and will be exchanged for an access token.
 */
server.grant(oauth2orize.grant.code(function(client, redirectURI, user, ares, done) {
    var code = utils.uid(config.token.authorizationCodeLength);

    Token.create({
        token: code,
        client_id: client.id,
        redirecturi: redirectURI,
        user_id: user.id,
        scope: client.scope
    }).then(function(t) {
        return done(null, code);
    }).catch(function(err) {
        return done(err);
    });
}));

/**
 * Grant implicit authorization.
 *
 * The callback takes the `client` requesting authorization, the authenticated
 * `user` granting access, and their response, which contains approved scope,
 * duration, etc. as parsed by the application.  The application issues a token,
 * which is bound to these values.
 */
server.grant(oauth2orize.grant.token(function(client, user, ares, done) {
    var token = utils.uid(config.token.accessTokenLength);

    Token.create({
        token: code,
        client_id: client.id,
        expiration_date: config.token.calculateExpirationDate(),
        user_id: user.id,
        scope: client.scope
    }).then(function(t) {
        return done(null, token, {
            expires_in: config.token.expiresIn
        });
    }).catch(function(err) {
        return done(err);
    });
}));

/**
 * Exchange authorization codes for access tokens.
 *
 * The callback accepts the `client`, which is exchanging `code` and any
 * `redirectURI` from the authorization request for verification.  If these values
 * are validated, the application issues an access token on behalf of the user who
 * authorized the code.
 */
server.exchange(oauth2orize.exchange.code(function(client, code, redirectURI, done) {

    Token.findOne({
        where: {
            token: code
        }
    }).then(function(authCode) {

        if (!authCode) {
            return done(null, false);
        }
        if (client.id !== authCode.client_id) {
            return done(null, false);
        }
        if (redirectURI !== authCode.redirecturi) {
            return done(null, false);
        }
        Token.destroy({
            where: {
                token: code
            }
        }).then(function(result) {

            if (result != undefined && result === 0) {
                //This condition can result because of a "race condition" that can occur naturally when you're making
                //two very fast calls to the authorization server to exchange authorization codes.  So, we check for
                // the result and if it's not undefined and the result is zero, then we have already deleted the
                // authorization code
                return done(null, false);
            }
            var token = utils.uid(config.token.accessTokenLength);

            Token.create({
                token: token,
                client_id: authCode.client_id,
                expiration_date: config.token.calculateExpirationDate(),
                user_id: authCode.user_id,
                scope: authCode.scope
            }).then(function(t) {

                var refreshToken = null;
                //I mimic openid connect's offline scope to determine if we send
                //a refresh token or not
                if (authCode.scope && authCode.scope.indexOf("offline_access") === 0) {
                    refreshToken = utils.uid(config.token.refreshTokenLength);

                    Token.create({
                        token: refreshToken,
                        client_id: authCode.client_id,
                        user_id: authCode.user_id,
                        scope: authCode.scope
                    }).then(function(t) {

                        return done(null, token, refreshToken, {
                            expires_in: config.token.expiresIn
                        });
                    }).catch(function(err) {
                        return done(err);
                    });
                } else {
                    return done(null, token, refreshToken, {
                        expires_in: config.token.expiresIn
                    });
                }
            }).catch(function(err) {
                return done(err);
            });
        }).catch(function(err) {
            return done(err);
        });
    }).catch(function(err) {
        return done(err);
    });
}));

/**
 * Exchange user id and password for access tokens.
 *
 * The callback accepts the `client`, which is exchanging the user's name and password
 * from the token request for verification. If these values are validated, the
 * application issues an access token on behalf of the user who authorized the code.
 */
server.exchange(oauth2orize.exchange_password(function(client, username, password, organization, scope, done, req) {
    //Validate the user

    sequelize.query('SELECT login(\'' + username + '\',\'' + organization + '\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    }).then(function(datas) {

        if (!datas || !datas[0] || !datas[0].login || !datas[0].login[0]) {
            return done(null, false);
        }

        var user = datas[0].login[0];
        bcrypt.compare(password, user.password, function(err, res) {
            if (err) {
                return done(err);
            }

            if (res === false) {
                console.log('password errata');
                return done(null, false);
            } else {
                console.log('password corretta');
                var token = utils.uid(config.token.accessTokenLength);
                var moment = require('moment');
                sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                    moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                    user.id + ',' +
                    client.id + ',\'' +
                    scope + '\',\'' + organization + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(res) {
                    /*	Token.create({
                    		token : token,
                    		expiration_date : config.token.calculateExpirationDate(),
                    		user_id : user.id,
                    		client_id : client.id,
                    		scope : scope
                    	}).then(function(tok) {*/
                    sequelize.query('SELECT insert_login(' + user.id + ',\'' + organization + '\',\'' + req.headers['user-agent'] + '\',\'' + req.connection.remoteAddress + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(login) {
                        var refreshToken = null;
                        //I mimic openid connect's offline scope to determine if we send
                        //a refresh token or not
                        if (scope && scope.indexOf("offline_access") === 0) {
                            refreshToken = utils.uid(config.token.refreshTokenLength);
                            sequelize.query('SELECT insert_token(\'' + refreshToken + '\',null,' +
                                user.id + ',' +
                                client.id + ',\'' +
                                scope + '\',\'' + organization + '\');', {
                                    raw: true,
                                    type: Sequelize.QueryTypes.SELECT,
                                    useMaster: true
                                }).then(function(res) {
                                /*Token.create({
                                	token : refreshToken,
                                	user_id : user.id,
                                	client_id : client.id,
                                	scope : scope
                                }).then(function(t) {
                                */
                                return done(null, token, refreshToken, {
                                    expires_in: config.token.expiresIn,
                                    redirect: user.redirect_page ? user.redirect_page : null
                                });
                            }).catch(function(err) {
                                return done(err);
                            });
                        } else {
                            return done(null, token, refreshToken, {
                                expires_in: config.token.expiresIn,
                                redirect: user.redirect_page ? user.redirect_page : null
                            });
                        }
                    }).catch(function(err) {
                        return done(err);
                    });


                }).catch(function(err) {
                    return done(err);
                });

            }

        });

    }).catch(function(err) {
        return done(err);
    });

}));

/**
 * Exchange the client id and password/secret for an access token.
 *
 * The callback accepts the `client`, which is exchanging the client's id and
 * password/secret from the token request for verification. If these values are validated, the
 * application issues an access token on behalf of the client who authorized the code.
 */
server.exchange(oauth2orize.exchange.clientCredentials(function(client, scope, done) {
    var token = utils.uid(config.token.accessTokenLength);
    //Pass in a null for user id since there is no user when using this grant type
    Token.create({
        token: token,
        expiration_date: config.token.calculateExpirationDate(),
        client_id: client.id,
        scope: scope
    }).then(function(t) {

        return done(null, token, null, {
            expires_in: config.token.expiresIn
        });
    }).catch(function(err) {
        return done(err);
    });
}));

/**
 * Exchange the refresh token for an access token.
 *
 * The callback accepts the `client`, which is exchanging the client's id from the token
 * request for verification.  If this value is validated, the application issues an access
 * token on behalf of the client who authorized the code
 */
server.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {
    Token.findOne({
        where: {
            token: refreshToken
        }
    }).then(function(authCode) {

        if (!authCode) {
            return done(null, false);
        }
        if (client.id !== authCode.client_id) {
            return done(null, false);
        }
        var token = utils.uid(config.token.accessTokenLength);
        Token.create({
            token: token,
            expiration_date: config.token.calculateExpirationDate(),
            user_id: authCode.user_id,
            client_id: authCode.client_id,
            scope: authCode.scope
        }).then(function(t) {
            return done(null, token, null, {
                expires_in: config.token.expiresIn
            });
        }).catch(function(err) {
            return done(err);
        });
    }).catch(function(err) {
        return done(err);
    });
}));

/**
 * User authorization endpoint
 *
 * `authorization` middleware accepts a `validate` callback which is
 * responsible for validating the client making the authorization request.  In
 * doing so, is recommended that the `redirectURI` be checked against a
 * registered value, although security requirements may vary accross
 * implementations.  Once validated, the `done` callback must be invoked with
 * a `client` instance, as well as the `redirectURI` to which the user will be
 * redirected after an authorization decision is obtained.
 *
 * This middleware simply initializes a new authorization transaction.  It is
 * the application's responsibility to authenticate the user and render a dialog
 * to obtain their approval (displaying details about the client requesting
 * authorization).  We accomplish that here by routing through `ensureLoggedIn()`
 * first, and rendering the `dialog` view.
 */
exports.authorization = [login.ensureLoggedIn(), server.authorization(function(clientID, redirectURI, scope, done) {
        Client.findOne(({
            where: {
                clientid: clientID
            }
        }).then(function(client) {

            if (client) {
                client.scope = scope;
            }
            // WARNING: For security purposes, it is highly advisable to check that
            //          redirectURI provided by the client matches one registered with
            //          the server.  For simplicity, this example does not.  You have
            //          been warned.
            return done(null, client, redirectURI);
        }).catch(function(err) {
            return done(err);
        }));
    }),
    function(req, res, next) {
        //Render the decision dialog if the client isn't a trusted client
        //TODO Make a mechanism so that if this isn't a trusted client, the user can recorded that they have consented
        //but also make a mechanism so that if the user revokes access to any of the clients then they will have to
        //re-consent.
        Client.findOne(({
            where: {
                clientid: req.query.client_id
            }
        }).then(function(client) {
            if (client && client.trustedClient && client.trustedClient === true) {
                //This is how we short call the decision like the dialog below does
                server.decision({
                    loadTransaction: false
                }, function(req, callback) {
                    callback(null, {
                        allow: true
                    });
                })(req, res, next);
            } else {
                res.render('dialog', {
                    transactionID: req.oauth2.transactionID,
                    user: req.user,
                    client: req.oauth2.client
                });
            }
        }).catch(function(err) {
            return done(err);
        }));
    }
];

/**
 * User decision endpoint
 *
 * `decision` middleware processes a user's decision to allow or deny access
 * requested by a client application.  Based on the grant type requested by the
 * client, the above grant middleware configured above will be invoked to send
 * a response.
 */
exports.decision = [login.ensureLoggedIn(), server.decision()];

/**
 * Token endpoint
 *
 * `token` middleware handles client requests to exchange authorization grants
 * for access tokens.  Based on the grant type being exchanged, the above
 * exchange middleware will be invoked to handle the request.  Clients must
 * authenticate when making requests to this endpoint.
 */
exports.token = [passport.authenticate(['basic', 'oauth2-client-password'], {

    session: false
}), server.token(), server.errorHandler()];

// Register serialialization and deserialization functions.
//
// When a client redirects a user to user authorization endpoint, an
// authorization transaction is initiated.  To complete the transaction, the
// user must authenticate and approve the authorization request.  Because this
// may involve multiple HTTPS request/response exchanges, the transaction is
// stored in the session.
//
// An application must supply serialization functions, which determine how the
// client object is serialized into the session.  Typically this will be a
// simple matter of serializing the client's ID, and deserializing by finding
// the client by ID from the database.

server.serializeClient(function(client, done) {
    return done(null, client.id);
});

server.deserializeClient(function(id, done) {
    Client.findById(id).then(function(client) {
        return done(null, client);
    }).catch(function(err) {
        return done(err);
    });
});

exports.logout = function(req, res) {
    var Sequelize = require('sequelize');

    sequelize.query('SELECT delete_token(\'' + req.query.access_token + '\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    }).then(function(datas) {
        req.logout();

        res.send({
            'authenticate': false,
            'message': "logout successful"
        });
    }).catch(function(err) {
        return res.status(500).send(err);
    });

};

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete Google profile is serialized
//   and deserialized.
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    done(null, obj);
});

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete Google profile is serialized
//   and deserialized.
passport.use('google', new GoogleStrategy({
    clientID: config.google_api.client_id,
    clientSecret: config.google_api.client_secret,
    callbackURL: "https://google.beebeeboard.com/api/auth/google/return",
    passReqToCallback: true
}, function(req, accessToken, refreshToken, profile, done) {

    var state = JSON.parse(req.query.state);
    var organization = state.domain,
        useragent = state.useragent,
        remoteAddress = state.remoteAddress,
        redirect_page = state.redirect_page;

    if (typeof profile.emails[0].value === 'undefined' || profile.emails[0].value === '') {
        return done(null, null, organization);
    }

    sequelize.query('SELECT login_google(\'' + profile.id + '\',\'' + organization + '\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
    }).then(function(datas) {

        if (!datas || !datas[0] || !datas[0].login_google || !datas[0].login_google[0]) {
            return done(null, null, organization);
        }

        var user = datas[0].login_google[0];

        var token = utils.uid(config.token.accessTokenLength);
        var moment = require('moment');
        sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
            moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
            user.id + ',' +
            null + ',' +
            null + ',\'' +
            organization + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(res) {

            sequelize.query('SELECT insert_login(' + user.id + ',\'' + organization + '\',\'' + useragent + '\',\'' + remoteAddress + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(login) {

                var refreshToken = null;
                //I mimic openid connect's offline scope to determine if we send
                //a refresh token or not

                return done(null, token, organization);
            }).catch(function(err) {
                return done(null, null, organization);
            });

        }).catch(function(err) {
            return done(null, null, organization);
        });

    }).catch(function(err) {
        return done(null, null, organization);
    });

}));

passport.use('set_google', new GoogleStrategy({
    clientID: config.google_api.client_id,
    clientSecret: config.google_api.client_secret,
    callbackURL: "https://google.beebeeboard.com/api/auth/set_google/return",
    passReqToCallback: true
}, function(req, accessToken, refreshToken, profile, done) {

    var state = JSON.parse(req.query.state);
    console.log(JSON.stringify(state));
    var organization = state.domain,
        contact_id = state.contact_id,
        useragent = state.useragent,
        remoteAddress = state.remoteAddress,
        redirect_page = state.redirect_page;

    if (typeof profile.id === 'undefined' || profile.id === '') {
        return done(null, null, organization);
    }

    var mail = '';
    if (typeof profile.emails[0].value !== 'undefined') {
        mail = profile.emails[0].value;
    }

    sequelize.query('SELECT set_google_account_v1(\'' + profile.displayName.replace(/'/g, "''''") +
        '\', ' + contact_id + ', \'' + organization + '\', \'' + profile.id + '\', \'' + mail + '\',\'' + (profile.name && profile.name.familyName ? profile.name.familyName.replace(/'/g, "''''") : mail) +
                '\',\'' + (profile.name && profile.name.givenName ? profile.name.givenName.replace(/'/g, "''''") : mail) +
                '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

        sequelize.query('SELECT login_google(\'' + profile.id + '\',\'' + organization + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(datas) {

            if (!datas || !datas[0] || !datas[0].login_google || !datas[0].login_google[0]) {
                return done(null, null, organization);
            }

            var user = datas[0].login_google[0];

            var token = utils.uid(config.token.accessTokenLength);
            var moment = require('moment');
            sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                user.id + ',' +
                null + ',' +
                null + ',\'' + organization + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(res) {

                sequelize.query('SELECT insert_login(' + user.id + ',\'' + organization + '\',\'' + useragent + '\',\'' + remoteAddress + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(login) {

                    var refreshToken = null;
                    //I mimic openid connect's offline scope to determine if we send
                    //a refresh token or not
                    return done(null, token, organization);
                }).catch(function(err) {
                    return done(null, null, organization);
                });

            }).catch(function(err) {
                return done(null, null, organization);
            });

        }).catch(function(err) {
            return done(null, null, organization);
        });


    }).catch(function(err) {
        return done(null, null, organization);
    });

}));

passport.use('create_google', new GoogleStrategy({
    clientID: config.google_api.client_id,
    clientSecret: config.google_api.client_secret,
    callbackURL: "https://google.beebeeboard.com/api/auth/create_google/return",
    passReqToCallback: true
}, function(req, accessToken, refreshToken, profile, done) {

    var state = JSON.parse(req.query.state);
    var organization = state.domain,
        useragent = state.useragent,
        remoteAddress = state.remoteAddress,
        redirect_page = state.redirect_page;

    if (typeof profile.id === 'undefined' || profile.id === '') {
        return done(null, null, organization);
    }

    var mail = '';
    if (typeof profile.emails[0].value !== 'undefined') {
        mail = profile.emails[0].value;
    }

    console.log(profile);
    /* check mail */
    sequelize.query('SELECT check_mail_registration(\'' +
        organization + '\',\'' +
        mail + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
        }).then(function(mailers) {
        if (!mailers[0].check_mail_registration) {
            sequelize.query('SELECT create_google_account(\'' + profile.displayName.replace(/'/g, "''''") +
                '\', \'' + mail + '\', \'' + organization + '\', \'' + profile.id + '\',\'' +(profile.name && profile.name.familyName ? profile.name.familyName.replace(/'/g, "''''") : mail) +
                '\',\'' + (profile.name && profile.name.givenName ? profile.name.givenName.replace(/'/g, "''''") : mail) +
                '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {

                sequelize.query('SELECT login_google(\'' + profile.id + '\',\'' + organization + '\');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                }).then(function(datas) {

                    if (!datas || !datas[0] || !datas[0].login_google || !datas[0].login_google[0]) {
                        return done(null, null, organization);
                    }

                    var user = datas[0].login_google[0];

                    var token = utils.uid(config.token.accessTokenLength);
                    var moment = require('moment');
                    sequelize.query('SELECT insert_token(\'' + token + '\',\'' +
                        moment(new Date()).add(1, 'days').format('YYYY/MM/DD HH:mm:ss') + '\',' +
                        user.id + ',' +
                        null + ',' +
                        null + ',\'' + organization + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(res) {

                        sequelize.query('SELECT insert_login(' + user.id + ',\'' + organization + '\',\'' + useragent + '\',\'' + remoteAddress + '\');', {
                            raw: true,
                            type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                        }).then(function(login) {

                            var refreshToken = null;
                            //I mimic openid connect's offline scope to determine if we send
                            //a refresh token or not
                            return done(null, token, organization);
                        }).catch(function(err) {
                            return done(null, null, organization);
                        });

                    }).catch(function(err) {
                        console.log(err);
                        return done(null, null, organization);
                    });

                }).catch(function(err) {
                    console.log(err);
                    return done(null, null, organization);
                });


            }).catch(function(err) {
                console.log(err);
                return done(null, null, organization);
            });
        } else {
            console.log(mailers[0]);
            return done(null, null, organization);
        }
    }).catch(function(err) {
        console.log(err);
        return done(null, null, organization);
    });

}));

/*
passport.use('google_calendar', new GoogleStrategy({
    clientID: config.google_api.client_id,
    clientSecret: config.google_api.client_secret,
    callbackURL: "https://google.beebeeboard.com/api/auth/google_calendar/return",
    scope: ['email', 'https://www.googleapis.com/auth/calendar'],
    passReqToCallback: true
}, function(req, accessToken, refreshToken, params, profile, done) {
    var request = require('request');
    var state = JSON.parse(req.query.state);
    var organization = state.domain,
        useragent = state.useragent,
        remoteAddress = state.remoteAddress,
        contact_id = state.contact_id;

    var mail = '';

    if (typeof profile.emails[0].value !== 'undefined') {
        mail = profile.emails[0].value;
    }

    if (accessToken && refreshToken && organization) {
        // Set the headers
        var headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + accessToken
        }

        // Configure the request
        var options = {
            url: 'https://www.googleapis.com/calendar/v3/calendars?alt=json',
            method: 'POST',
            headers: headers,
            json: JSON.parse('{"summary": "'+organization+'"}')
        }

        // Start the request
        request(options, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + accessToken
                }

                // Configure the request
                var options = {
                    url: 'https://www.googleapis.com/calendar/v3/calendars/' + body.id + '/events?alt=json',
                    method: 'GET',
                    headers: headers
                }
                // Start the request
                request(options, function(error1, response1, body1) {
                    if (!error1 && response1.statusCode == 200) {
                        // Print out the response body
                        console.log(JSON.parse(body1).nextSyncToken);
                        sequelize.query('SELECT insert_google_calendar_token(\'' + organization +
                            '\', \'' + accessToken + '\', \'' + refreshToken + '\',' + contact_id + ',\'' +
                            mail + '\',\'' + body.id + '\',\'' + JSON.parse(body1).nextSyncToken + '\');', {
                                raw: true,
                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                            }).then(function(err, datas) {
                            if (err) {
                                console.log(err);
                            }

                            var calendar_id = body.id;
                            var headers = {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + accessToken
                            }

                            // Configure the request
                            var options = {
                                url: 'https://www.googleapis.com/calendar/v3/calendars/' + body.id + '/events/watch?alt=json&showdeleted=true',
                                method: 'POST',
                                headers: headers,
                                json: JSON.parse('{"id": "gc-beebeeboard-notification-' + organization + '","type": "web_hook","address": "https://google-calendar.beebeeboard.com/api/v1/calendar_notification" }')
                            }
                            // Start the request
                            request(options, function(error, response2, body2) {
                                if (!error && response2.statusCode == 200) {
                                    if(body2){
                                        console.log(body2);
                                        sequelize.query('SELECT update_google_calendar_watch(\'' + organization +
                                            '\', ' + contact_id + ',\'' + calendar_id + '\',\'' + 
                                            body2.resourceId + '\',\'' + 
                                            body2.resourceUri + '\',\'' + 
                                            body2.expiration + '\');', {
                                                raw: true,
                                                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                                        }).then(function(err, datas) {

                                        });
                                    }
                                    
                                } else {
                                    console.log(error);
                                }
                            });

                            return done(null, refreshToken, accessToken, organization);

                        });
                    }
                });

            } else {
                console.log(error);
                return done(null, null, null, organization);
            }
        });


    } else {
        return done(null, null, null, organization);
    }
*/

passport.use('google_calendar', new GoogleStrategy({
    clientID: config.google_api.client_id,
    clientSecret: config.google_api.client_secret,
    callbackURL: "https://google.beebeeboard.com/api/auth/google_calendar/return",
    scope: ['email', 'https://www.googleapis.com/auth/calendar'],
    passReqToCallback: true
}, function(req, accessToken, refreshToken, params, profile, done) {
    var request = require('request');
    var state = JSON.parse(req.query.state);
    var organization = state.domain,
        useragent = state.useragent,
        remoteAddress = state.remoteAddress,
        contact_id = state.contact_id;

    var mail = '';

    if (typeof profile.emails[0].value !== 'undefined') {
        mail = profile.emails[0].value;
    }

    if (accessToken && refreshToken && organization) {
        sequelize.query('SELECT insert_google_calendar_token(\'' + organization +
            '\', \'' + accessToken + '\', \'' + refreshToken + '\',' + contact_id + ',\'' +
            mail + '\',null,null);', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster:true
            }).then(function(err, datas) {
            if (err) {
                console.log(err);
            }

            console.log('FINE');
            return done(null, refreshToken, accessToken, organization);
        });
    } else {
        return done(null, null, null, organization);
    }

}));