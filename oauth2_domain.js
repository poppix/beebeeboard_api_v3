var oauth2orize = require('oauth2orize');
var utils = require('oauth2orize/lib/utils');
var AuthorizationError = require('oauth2orize/lib/errors/authorizationerror');
oauth2orize.exchange_password = function(options, issue) {

    if (typeof options == 'function') {
        issue = options;
        options = null;
    }
    options = options || {};

    if (!issue) throw new Error('OAuth 2.0 password exchange middleware requires an issue function.');

    var userProperty = options.userProperty || 'user';

    // For maximum flexibility, multiple scope spearators can optionally be
    // allowed.  This allows the server to accept clients that separate scope
    // with either space or comma (' ', ',').  This violates the specification,
    // but achieves compatibility with existing client libraries that are already
    // deployed.
    var separators = options.scopeSeparator || ' ';
    if (!Array.isArray(separators)) {
        separators = [separators];
    }

    return function password(req, res, next) {

        if (!req.body) { return next(new Error('Request body not parsed. Use bodyParser middleware.')); }

        // The 'user' property of `req` holds the authenticated user.  In the case
        // of the token endpoint, the property will contain the OAuth 2.0 client.
        var client = req[userProperty],
            username = req.body['username'],
            password = req.body['password'],
            organization = req.headers['host'].split(".")[0],
            scope = req.body['scope'];

        if (!username) { return next(new AuthorizationError('missing username parameter', 'invalid_request')); }
        if (!password) { return next(new AuthorizationError('missing password parameter', 'invalid_request')); }
        if (!organization) { return next(new AuthorizationError('missing organization parameter', 'invalid_request')); }


        if (scope) {
            for (var i = 0, len = separators.length; i < len; i++) {
                var separated = scope.split(separators[i]);
                // only separate on the first matching separator.  this allows for a sort
                // of separator "priority" (ie, favor spaces then fallback to commas)
                if (separated.length > 1) {
                    scope = separated;
                    break;
                }
            }
            if (!Array.isArray(scope)) { scope = [scope]; }
        }

        function issued(err, accessToken, refreshToken, params) {
            if (err) { return next(err); }
            if (!accessToken) { return next(new AuthorizationError('invalid resource owner credentials', 'invalid_grant')); }

            var tok = {};
            tok['access_token'] = accessToken;
            if (refreshToken) { tok['refresh_token'] = refreshToken; }
            if (params) { utils.merge(tok, params); }
            tok['token_type'] = tok['token_type'] || 'bearer';


            var json = JSON.stringify(tok);
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Cache-Control', 'no-store');
            res.setHeader('Pragma', 'no-cache');
            res.end(json);
        }

        var arity = issue.length;
        if (arity == 7) {
            issue(client, username, password, organization, scope, issued, req);
        } else { // arity == 4
            issue(client, username, password, organization, issued);
        }
    }
};

module.exports = oauth2orize;