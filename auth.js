var passport = require('passport'),
LocalStrategy = require('passport-local').Strategy,
BasicStrategy = require('passport-http').BasicStrategy,
ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy,
BearerStrategy = require('passport-http-bearer').Strategy,
Sequelize = require('sequelize'),
	/*mongoose = require('mongoose'),
	Token = mongoose.model('Token'),
	User = mongoose.model('User'),
	Client = mongoose.model('clientt'),*/
	bcrypt = require('bcryptjs');
/**
 * LocalStrategy
 *
 * This strategy is used to authenticate users based on a username and password.
 * Anytime a request is made to authorize an application, we must ensure that
 * a user is logged in before asking them to approve the request.
 */
 passport.use(new LocalStrategy(function(username, password, done) {
 	Contact.findOne(({where:{username: username}})).then(function(user) {
 		
 		if (!user) {
 			return done(null, false);
 		}
 		bcrypt.compare(password, user.password, function(err, res){
 			if(err) {
 				return done(err);
 			}
 			if(res === false){ 
 				return done(null,false);
 			}
 			else
 			{
 				
 				return done(null, user);
 				
 			}
 		});	
 	}).catch(function(err){
 		return done(err);
 	});
 }));

/**
 * BasicStrategy & ClientPasswordStrategy
 *
 * These strategies are used to authenticate registered OAuth clients.  They are
 * employed to protect the `token` endpoint, which consumers use to obtain
 * access tokens.  The OAuth 2.0 specification suggests that clients use the
 * HTTP Basic scheme to authenticate.  Use of the client password strategy
 * allows clients to send the same credentials in the request body (as opposed
 * to the `Authorization` header).  While this approach is not recommended by
 * the specification, in practice it is quite common.
 */
 passport.use(new BasicStrategy(function(username, password, done) {
 	Client.findOne({where:{clientid:username}}).then(function (client) {
 		if (!client) {
 			return done(null, false);
 		}
 		if (client.clientsecret != password) {
 			return done(null, false);
 		}
 		return done(null, client);
 	}).catch(function(error){
 		return error;
 	});
 	
 }));

/**
 * Client Password strategy
 *
 * The OAuth 2.0 client password authentication strategy authenticates clients
 * using a client ID and client secret. The strategy requires a verify callback,
 * which accepts those credentials and calls done providing a client.
 */
 passport.use(new ClientPasswordStrategy(function(clientId, clientSecret, done) {
 	Client.findOne(({where:{clientid:clientId}})).then(function(client) {
 		if (!client) {
 			return done(null, false);
 		}
 		if (client.clientsecret != clientSecret) {
 			return done(null, false);
 		}
 		return done(null, client);
 	}).catch(function(err){
 		return done(err);
 	});
 }));

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate either users or clients based on an access token
 * (aka a bearer token).  If a user, they must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */
 passport.use(new BearerStrategy({ // not required
    passReqToCallback: true // default false
}, function(req, accessToken, done) {

 	sequelize.query('SELECT find_token_v1(\''+accessToken+'\', \''+req.headers['host'].split(".")[0]+'\');', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
      }).then(function (tokens) {

      	if(!tokens || !tokens[0] || !tokens[0].find_token_v1 || !tokens[0].find_token_v1[0] || !tokens[0].find_token_v1[0].token){
			return done(null, false);
      	}

      	var token = tokens[0].find_token_v1[0].token;
 	//Token.findOne({where:{token: accessToken}}).then(function(token) {
 		//var token = datas[0].authorize[0];
 		if (!token) {
 			return done(null, false);
 		}
 		if (new Date() > tokens[0].find_token_v1[0].expiration_date) {
 			sequelize.query('SELECT delete_token(\''+accessToken+'\');',{
 				raw:true, 
 				type: Sequelize.QueryTypes.SELECT,
                		useMaster: true})
 			.then(function(datas){
				req.logout();

				res.send({
					'authenticate' : false,
					'message' : "logout successful"
				});
			}).catch(function(err){
				return done();
			});
 		} else {
 			if (tokens[0].find_token_v1[0].user_id != null) {
 				var info = {
					scope : '*'
				};
    	 			var user = tokens[0].find_token_v1[0];
				
				
		 		return done(null, user, info);
			    
 			}	
 			else
 			{
 				return done(null, false);
 			}
			/*sequelize.query('SELECT find_contact_from_token(\''+accessToken+'\',\''+token.organization+'\','+token.user_id+');', {
			        raw: true,
			        type: Sequelize.QueryTypes.SELECT
                
			      }).then(function (users) {
 				//Contact.findById(token.user_id).then(function(user) {
 					if (!users[0].find_contact_from_token[0]) {
 						return done(null, false);
 					}
 					var user = users[0].find_contact_from_token[0];
					// to keep this example simple, restricted scopes are not implemented,
					// and this is just for illustrative purposes
					var info = {
						scope : '*'
					};

					sequelize.query('SELECT roles_find_data_v2('+user._tenant_id+',\''+user.organization+'\','+user.role_id+');', {
				        raw: true,
				        type: Sequelize.QueryTypes.SELECT
				      }).then(function(datas){
				      	 if (datas[0].roles_find_data_v2 && datas[0].roles_find_data_v2 != null) {
				      	 		user.role = datas[0].roles_find_data_v2;
        						user.alta_bassa = datas[0].roles_find_data_v2[0].alta_bassa;
        				 }
        				 		return done(null, user, info);
				      });
					
			
				}).catch(function(err){
					return done(err);
				});
			} else {
				//The request came from a client only since userID is null
				//therefore the client is passed back instead of a user
				//Client.findById(token.client_id).then(function(err, client) {
				sequelize.query('SELECT find_client_from_token(\''+accessToken+'\',\''+token.organization+'\','+token.client_id+');', {
			        raw: true,
			        type: Sequelize.QueryTypes.SELECT
			      }).then(function (clients) {	
					if (!clients[0].find_client_from_token[0]) {
						return done(null, false);
					}
					var client = clients[0].find_client_from_token[0];
					// to keep this example simple, restricted scopes are not implemented,
					// and this is just for illustrative purposes
					var info = {
						scope : '*'
					};
					
					return done(null, client, info);
				}).catch(function(err){
					return done(err);
				});
			}*/
		}
	}).catch(function(err){
		return done(err);
	});
}));

// Register serialialization and deserialization functions.
//
// When a client redirects a user to user authorization endpoint, an
// authorization transaction is initiated.  To complete the transaction, the
// user must authenticate and approve the authorization request.  Because this
// may involve multiple HTTPS request/response exchanges, the transaction is
// stored in the session.
//
// An application must supply serialization functions, which determine how the
// client object is serialized into the session.  Typically this will be a
// simple matter of serializing the client's ID, and deserializing by finding
// the client by ID from the database.

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	Contact.findById(id).then(function(user) {
		done(null, user);
	}).catch(function(err){
		return done(err,null);
	});
});
