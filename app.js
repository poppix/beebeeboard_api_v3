var 
    express = require('express'),
    Sequelize = require('sequelize'),
    fs = require('fs'),
    models_path = __dirname + '/application/models';
var multer = require('multer');
var https = require('https');

//var raven = require('raven');
var Sentry = require("@sentry/node"),
    Tracing = require("@sentry/tracing");

var methodOverride = require('method-override');
var session = require('express-session');
var morgan = require('morgan');
var compression = require('compression');
var stripe_file = require(__dirname + '/application/controllers/stripe');


//Pull in the memory store if we're configured to use it
//else pull in MemoryStore for the session configuration
console.log('Using MemoryStore for the Session');

var MemoryStore = session.MemoryStore;
sessionStorage = new MemoryStore();

// Express configuration
var app = express();

// Raven package for Sentry 
//raven.config('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3').install();
//raven.config('https://2fb836d820aa466291b1dc29bebcc37a:7a5f8eec115649278efa808074cad047@sentry.io/66389').install();
//client = new raven.Client('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3');

Sentry.init({
    dsn: "https://2fb836d820aa466291b1dc29bebcc37a:7a5f8eec115649278efa808074cad047@sentry.io/66389",
    integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({
            tracing: true
        }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({
            app
        }),
        new Tracing.Integrations.Postgres({
            usePgNative: true // Default: false
        })
    ],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 0.2,
});


// compress all requests
app.use(compression());

// Cross Origins configuration
var cors = require('cors'),
    corsOptions = {
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'],
        origin: true,
        credentials: true
    },
    config = require('./config'),
    passport = require('passport'),
    site = require('./site'),
    oauth2 = require('./oauth2'),
    token = require('./token'),
    https = require('https'),
    path = require('path'),
    dbToConnect = process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || (config.db.dbUrl + config.db.dbName),
    port = Number(process.env.PORT || 3000);

// Sequelize for Postgres Functions
var sequelize = new Sequelize(config.db.dbName, null, null, {
    //host: process.env.MONGOHQ_URL || config.db.dbUrl,
    dialect: 'postgres',
    ssl: true,
    dialectOptions: {
        ssl: {
            rejectUnauthorized: true,
            ca: fs.readFileSync('./eu-central-1-bundle.pem')
        }
    },
    //operatorsAliases: false,
    replication: {
        read: [{
            host: config.dbRead.dbUrl,
            username: config.dbRead.user,
            password: config.dbRead.dbpwd
        }],
        write: {
            host: config.db.dbUrl,
            username: config.db.user,
            password: config.db.dbpwd
        }
    },
    pool: {
        max: 5,
        min: 0,
        maxIdleTime: 180000
    }
});


// Sequelize for Postgres Functions
var sequelize_socket = new Sequelize(config.dbSocket.dbName, null, null, {
    //host: process.env.MONGOHQ_URL || config.db.dbUrl,
    dialect: 'postgres',
    ssl: true,
    dialectOptions: {
        ssl: {
            rejectUnauthorized: true,
            ca: fs.readFileSync('./ca-certificate.crt')
        }
    },
    host: config.dbSocket.dbUrl,
    username: config.dbSocket.user,
    password: config.dbSocket.dbpwd,  
    port: 25060,      
    pool: {
        max: 22,
        min: 0,
        maxIdleTime: 180000
    }
});


var stripe = require("stripe")(
    //config.stripe_keys.test_secret_key
    config.stripe_keys.live_secret_key
);

global.stripe = stripe;

global.sequelize = sequelize;
global.sequelize_socket = sequelize_socket;


var walk = function(path) {
    fs.readdirSync(path).forEach(function(file) {
        var newPath = path + '/' + file;
        var stat = fs.statSync(newPath);
        if (stat.isFile()) {
            if (/(.*)\.(js$|coffee$)/.test(file)) {
                require(newPath);
            }
        } else if (stat.isDirectory()) {
            walk(newPath);
        }
    });
};
walk(models_path);

// Middleware for Sentry segnalations
function onError(err, req, res, next) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;

    sequelize.query('SELECT all_reset_sequence(\'' +
        req.headers['host'].split(".")[0] + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT,
        useMaster: true
        })
    .then(function(datas) {
    }).catch(function(err) {
        
    });

    res.end(res.sentry + '\n');
}

app.use(cors(corsOptions));

app.set('view engine', 'ejs');
app.use(morgan('dev'));

var storage = multer.memoryStorage({
    destination: function(req, file, cb) {
        callback(null, '');
    },
    filename: function(req, file, cb) {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
        cb(null, Date.now() + ext)
    }
});
var upload = multer({
    storage: storage
});
global.upload = upload;



// Body Parser configuration
var bodyParser = require('body-parser');


app.post('/api/v1/stripe/webhooks', bodyParser.raw({
    type: 'application/json'
}), stripe_file.webhooks);
app.post('/api/v1/stripe/webhooks_connect', bodyParser.raw({
    type: 'application/json'
}), stripe_file.webhooks_connect);

app.post('/api/v1/stripe/webhooks_test', bodyParser.raw({
    type: 'application/json'
}), stripe_file.webhooks_test);
app.post('/api/v1/stripe/webhooks_connect_test', bodyParser.raw({
    type: 'application/json'
}), stripe_file.webhooks_connect_test);


app.use(bodyParser.urlencoded({
    extended: true,
    limit: '20mb'
}));

app.use(bodyParser.json({
    type: 'application/*+json',
    limit: '500mb'
}));

app.use(bodyParser.json({
    type: 'application/json',
    limit: '500mb'
}));

app.use(methodOverride());

//Session Configuration
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.session.secret,
    store: sessionStorage,
    key: "authorization.sid",
    cookie: {
        maxAge: config.session.maxAge
    }
}));

app.use(passport.initialize());
app.use(passport.session());

// Catch all for error messages.  Instead of a stack
// trace, this will log the json of the error message
// to the browser and pass along the status with it
app.use(function(err, req, res, next) {
    if (err) {
        res.status(err.status);
        res.json(err);
    } else {
        next();
    }
});


// Passport configuration
require('./auth');
require('./config/routes')(app, site, oauth2, token);

//static resources for stylesheets, images, javascript files
app.use(express.static(path.join(__dirname, 'public')));

// The request handler must be the first item
//app.use(raven.requestHandler('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3'));
//app.use(raven.requestHandler('https://2fb836d820aa466291b1dc29bebcc37a:7a5f8eec115649278efa808074cad047@sentry.io/66389'));
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());

// The error handler must be before any other error middleware
//app.use(raven.errorHandler('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3'));
//app.use(raven.errorHandler('https://2fb836d820aa466291b1dc29bebcc37a:7a5f8eec115649278efa808074cad047@sentry.io/66389'));
app.use(Sentry.Handlers.errorHandler());

// Optional fallthrough error handler
app.use(onError);


var server = app.listen(port, function() {
    console.log('Beebeeboard API listen on port %d', 3000);
});