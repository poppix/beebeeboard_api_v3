	//
	// The configuration options of the server
	//

	/**
	 * Configuration of access tokens.
	 *
	 * expiresIn - The time in seconds before the access token expires
	 * calculateExpirationDate - A simple function to calculate the absolute
	 * time that th token is going to expire in.
	 * authorizationCodeLength - The length of the authorization code
	 * accessTokenLength - The length of the access token
	 * refreshTokenLength - The length of the refresh token
	 * 
	 */

	const fs = require('fs');
	exports.token = {
	    expiresIn: 43200,
	    calculateExpirationDate: function() {
	        return new Date(new Date().getTime() + (this.expiresIn * 1000));
	    },
	    authorizationCodeLength: 16,
	    accessTokenLength: 256,
	    refreshTokenLength: 256
	};

	/**
	 * Database configuration for access and refresh tokens.
	 *
	 * timeToCheckExpiredTokens - The time in seconds to check the database
	 * for expired access tokens.  For example, if it's set to 3600, then that's
	 * one hour to check for expired access tokens.
	 * type - The type of database to use.  "db" for "in-memory", or
	 * "mongodb" for the mongo database store.
	 * dbName - The database name to use.
	 */

	exports.db = {
	    timeToCheckExpiredTokens: 3600,
	    type: "postgres",
	    //dbName : "beebeeboard",
	    dbName: "beebee_core",
	    dbpwd: process.env.DB_POSTGRES_PWD ,
	    user: process.env.DB_POSTGRES_USR ,
	    dbUrl: process.env.DBW_POSTGRES_HOST ,
	};

	exports.dbRead = {
	    timeToCheckExpiredTokens: 3600,
	    type: "postgres",
	    //dbName : "beebeeboard",
	    dbName: "beebee_core",
	    dbpwd: process.env.DB_POSTGRES_PWD ,
	    user: process.env.DB_POSTGRES_USR,
	    dbUrl: process.env.DBR_POSTGRES_HOST
	};

	exports.dbSocket = {
	    timeToCheckExpiredTokens: 3600,
	    type: "postgres",
	    //dbName : "beebeeboard",
	    dbName: "defaultdb",
	    dbpwd: process.env.SOCKET_POSTGRES_PWD ,
	    user: process.env.SOCKET_POSTGRES_USR ,
	    dbUrl: process.env.SOCKET_POSTGRES_HOST 
	};


	/**
	 * Session configuration
	 *
	 * type - The type of session to use.  MemoryStore for "in-memory",
	 * or MongoStore for the mongo database store
	 * maxAge - The maximum age in milliseconds of the session.  Use null for
	 * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
	 * for a year.
	 * secret - The session secret that you should change to what you want
	 * dbName - The database name if you're using Mongo
	 */
	exports.session = {
	    type: "MongoStore",
	    maxAge: 3600000 * 24 * 7 * 52,
	    secret: "Fj029ID9jwjwkdOI8hdsf",
	    dbName: "Session"
	};



	/**
	 * AWS S3 connection
	 */
	exports.aws_s3 = {
	    accessKeyId: process.env.AWS_S3_KEY,
	    secretAccessKey: process.env.AWS_S3_SECRET,
	    region: process.env.AWS_S3_REGION
	};

	/**
	 * AWS Route53 connection
	 */
	exports.aws_route53 = {
	    accessKeyId: process.env.AWS_ROUTE53_KEY,
	    secretAccessKey: process.env.AWS_ROUTE53_SECRET,
	    region: process.env.AWS_ROUTE53_REGION,
	    hostedZoneId: process.env.AWS_ROUTE53_ZONEID

	};

	/**
	 * Stripe connection
	 */
	exports.stripe_keys = {
	    test_secret_key: process.env.STRIPE_TEST_SECRET || 'sk_test_xTpn9Yn2jDOsc4AY5tsE3MLW',
	    test_publ_key: process.env.STRIPE_TEST_PUBL_KEY || 'pk_test_yUTYzS9lsKbvTnlPMsQBUArR',
	    live_secret_key: process.env.STRIPE_LIVE_SECRET,
	    live_publ_key: process.env.STRIPE_LIVE_PUBL_KEY
	};

	/**
	 * Mandrill API
	 * test API 'A0hnSomu9GcqaCO1gnM71g' real 6f-e0qenhJDqDgQ9D-bpDg
	 */
	exports.mandrill = {
	    api: process.env.MANDRILL_API
	};

	/**
	 * Skebby API
	 */
	exports.skebby = {
	    username: process.env.SKEBBY_USR,
	    password: process.env.SKEBBY_PWD
	};


	/**
	 * Trust Technologies NEW Version
	 */
	exports.tt_fe_new = {
		auth_code: process.env.TTFE_NEW_AUTH,
	    host: process.env.TTFE_NEW_URL || 'https://tt-invoice.trusttechnologies.it/FatturaDigitale/app/fattura/uploadfattureProd',
	    host_notifiche: process.env.TTFE_NEW_NOT_ACT || 'https://tt-invoice.trusttechnologies.it/proxyTrustDoc/exportDocuments',
		host_update_documents: process.env.TTFE_NEW_UPDATE || 'https://tt-invoice.trusttechnologies.it/proxyTrustDoc/updateDocuments'
	};

	/**
	 * TS System
	 */
	exports.ts_certificate = {
		cert: process.env.TS_CERT || '-----BEGIN CERTIFICATE-----\n'+
'MIIDyzCCArOgAwIBAgIIUoaQWHvvfpAwDQYJKoZIhvcNAQELBQAwbTELMAkGA1UE\n'+
'BhMCSVQxHjAcBgNVBAoTFUFnZW56aWEgZGVsbGUgRW50cmF0ZTEbMBkGA1UECxMS\n'+
'U2Vydml6aSBUZWxlbWF0aWNpMSEwHwYDVQQDExhDQSBBZ2VuemlhIGRlbGxlIEVu\n'+
'dHJhdGUwHhcNMTgwMzE1MTMxNzAzWhcNMjEwMzE1MTMxNzAzWjBeMQswCQYDVQQG\n'+
'EwJJVDEeMBwGA1UECgwVQWdlbnppYSBkZWxsZSBFbnRyYXRlMRswGQYDVQQLDBJT\n'+
'ZXJ2aXppIFRlbGVtYXRpY2kxEjAQBgNVBAMMCVNhbml0ZWxDRjCBnzANBgkqhkiG\n'+
'9w0BAQEFAAOBjQAwgYkCgYEA1B+Xx0nrlCxQYAhG+I5vJA8DYBgGnGBWNOKauf4H\n'+
'0L1hRLPpttm/zdlfl0xhvfrwbrOwmG9m+ipVlAah/Pmmxz0fNc0C1n0Gppl8VDUk\n'+
'mZ16dqtFKxN09Ze4muSS0AxDXSESjUHLZ8bajLV/3fiJ2mvE69yjGw7q2pIXxb0y\n'+
'0e8CAwEAAaOCAQAwgf0wHwYDVR0jBBgwFoAU6kQ/HxnjNz6rqpSCpZ/r/Ba6f7Uw\n'+
'gaoGA1UdHwSBojCBnzCBnKCBmaCBloaBk2xkYXA6Ly9jYWRzLmVudHJhdGUuZmlu\n'+
'YW56ZS5pdC9DTj1DQSUyMEFnZW56aWElMjBkZWxsZSUyMEVudHJhdGUsT1U9U2Vy\n'+
'dml6aSUyMFRlbGVtYXRpY2ksTz1BZ2VuemlhJTIwZGVsbGUlMjBFbnRyYXRlLEM9\n'+
'aXQ/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdDAdBgNVHQ4EFgQUk40paPskEoq8\n'+
'te6R8PK19/Bb02AwDgYDVR0PAQH/BAQDAgQwMA0GCSqGSIb3DQEBCwUAA4IBAQCG\n'+
'yAvWzRWYVSFFa3/jbGA6rzQS4EDXV+XUMjOnnWshmZDFoXwNreAZ93aIqkoomQzN\n'+
'CpQZ6CduuWhyuHkGpwu0JcqdgRLC2SNO+1L6FZsKTCRNPnx5bvtuHZEzuBoF1BSi\n'+
'trI6nr26dlYc0U5y+skZzUPBDqvsKDU4DwPTa3dk1Z7pBfEE5752mnYg2V7mvzr6\n'+
'UXYi1CE9MdBH8xiUrtmD2YIBuRlQet9hE5QDApjpfVwqSaVX3G17hRHeo0+LfYwi\n'+
'G7AAWBkfR/kTDqf2a4b9ARJrA9edIv3x5hLtc+qnU5tT1xJZKZTIhrzqvOr0oXQv\n'+
'Zj7OVHL00QlH0/Kx471W\n'+
'-----END CERTIFICATE-----'
	};


	exports.satispay = {
		pk: fs.readFileSync("./public.pem", { encoding: "utf8" }),
		private_pk: fs.readFileSync("./private.pem", { encoding: "utf8" }),
		//sandbox_url: 'https://staging.authservices.satispay.com/',
		//sandbox_host: 'staging.authservices.satispay.com',
		sandbox_host: 'authservices.satispay.com',
		sandbox_url: 'https://authservices.satispay.com/'

	}
	/**
	 * iPA governements
	 */
	exports.ipa = {
		auth_code: process.env.IPA_AUTH || 'VVVRXQVI',
	    host_search: process.env.IPA_SEARCH || 'https://www.indicepa.gov.it/public-ws/WS16_DES_AMM.php',
	    host_amm: process.env.IPA_AMM || 'https://www.indicepa.gov.it/public-ws/WS05_AMM.php',
	    host_fse: process.env.IPA_FSE || 'https://www.indicepa.gov.it/public-ws/WS04_SFE.php'

	};

	/**
	 * OneSignal API
	 */
	exports.onesignal = {
	    app_id: process.env.ONESIGNAL_APP_ID,
	    app_key: process.env.ONESIGNAL_KEY 
	};

	exports.yousign = {
		api_key: process.env.YOUSIGN_API_KEY,
		api_test_key: '6LAbGi7wRY3zUR0iEkhcmYa26ybi4sX6',
		url_test_base: 'https://api-sandbox.yousign.app/v3',
		url_base: process.env.YOUSIGN_URL
	};

	/**
	 * Dropbox Connection
	 */
	exports.dropbox = {
	    app_key: process.env.DROPBOX_KEY,
	    app_secret: process.env.DROPBOX_SECRET
	};

	exports.physitrack = {
		token_key: process.env.PHYSITRACK_TOKEN
	};

	/**
	 * Google API login
	 */
	exports.google_api = {
	    client_id: process.env.GOOGLE_CLIENT_ID,
	    client_secret: process.env.GOOGLE_CLIENT_SECRET
	};