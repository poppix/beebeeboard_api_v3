var passport = require('passport');
var async = require('async');
var Sequelize = require('sequelize');

//var redis = require('redis');

module.exports = function(app, site, oauth2, token) {
    var user = require('../application/controllers/user');
    var log = require('../application/controllers/log');
    var mandrill = require('../application/controllers/mandrill');
    var skebby = require('../application/controllers/skebby');
    var utility = require('../application/utility/utility.js');
   
    var organization = require('../application/controllers/organization');
    var report_clinic = require('../application/controllers/clinical/clinic_report');
    var physitrack = require('../application/controllers/physitrack/physitrack');
    var yousign  = require('../application/controllers/yousign/yousign');
    var guide_monterosa = require('../application/controllers/guide/guidemonterosa');
    var peakshunter = require('../application/controllers/guide/peakshunter');


    app.get('/favicon.ico', function(req, res) {
        var https = require('https');

        var externalReq = https.request({
            hostname: "beebeeassets.beebeeboard.com",
            path: "/favicon.ico"
        }, function(externalRes) {
            externalRes.pipe(res);
        });
        externalReq.end();
    });

    app.get('/', function(req, res) {

        sequelize.query('SELECT boot_login();', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(boot) {
            var body = boot[0].boot_login || '';
            res.writeHeader(200, {
                "Content-Type": "text/html"
            });
            res.write(body);
            res.end();

        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
        });


        
    });

    app.get('/get_organization_by_mail', function(req, res) {
        var mail = req.query.mail ? req.query.mail : null;

        if(!mail) {
            return res.status(200).send();
        }

        sequelize.query('SELECT get_organization_by_email(\'' + mail + '\');', {
            raw: true,
            type: Sequelize.QueryTypes.SELECT
        }).then(function(datas) {
        
        if (datas && datas[0] && datas[0].get_organization_by_email && datas[0].get_organization_by_email != null) {
            return res.status(200).send({
                'data':datas[0].get_organization_by_email
            });

        } else {
            return res.status(200).send({
                'data':[]
            });
        }


        }).catch(function(err) {
            console.log(err);
            return res.status(200).send({
                'data':[]
            });
        });

        
    });

    function loadIndex(req, res, current_name) {
        var queryName = req.query.index_key ? req.query.index_key : null;
        
        sequelize.query('SELECT boot_current_v3(\'' + 
                req.headers['host'].split(".")[0] + '\''+
                ',' + (current_name && utility.check_type_variable(current_name, 'string') && current_name != null ? '\'' + current_name.replace(/'/g, "''''") + '\'' : null)+
                ',' + (queryName && utility.check_type_variable(queryName, 'string') && queryName != null ? '\'' + queryName.replace(/'/g, "''''") + '\'' : null) +
                ');', {
                    raw: true,
                    type: Sequelize.QueryTypes.SELECT
               
        }).then(function(boot) {
            var body = boot[0].boot_current_v3 || '';
            res.writeHeader(200, {
                "Content-Type": "text/html"
            });
            
            res.write(body);
            res.end();

        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
        });
    }

    /* MANIFEST */
    app.get('/manifest.webmanifest', function(req, res) {
        var json_res = '{"name":"Beebeeboard","short_name":"Beebeeboard","description":"Il Gestionale per la tua piccola azienda","start_url":"/","display":"standalone","background_color":"#fff","theme_color":"#1b2a39","icons":[{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-72x72-a2cd5a5492172d5c5138def064eb34a1.png","sizes":"72x72","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-96x96-371b575e9ea0c53f316a1b7c5ffbe93c.png","sizes":"96x96","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-128x128-21cb0ba6a0e2cff593c6a06943678e05.png","sizes":"128x128","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-144x144-8cd453dfb6641f2ac158ad5031f9831a.png","sizes":"144x144","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-152x152-a0029df5f00a0d86f52348a9aacaf6d5.png","sizes":"152x152","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-192x192-9bb3fe136860babde9c3391602037392.png","sizes":"192x192","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-384x384-74dcc0762bf68b714fe6c9d3fb99bd7e.png","sizes":"384x384","type":"image/png"},{"src":"https://beebeeassets.beebeeboard.com/manifest/icons/icon-512x512-f407cf7132c7137b5ba733776876be38.png","sizes":"512x512","type":"image/png"}]}';
        json_res = JSON.parse(json_res);
        res.send(json_res);
    });


    app.get('/start', function(req, res) {
        return loadIndex(req, res, 'start');
    });

    app.get('/rototracer', function(req, res) {
        return loadIndex(req, res, 'rototracer');
    });

    app.get('/trail', function(req, res) {
        return loadIndex(req, res, 'trail');
    });

    app.get('/astaldi', function(req, res) {
        return loadIndex(req, res, 'astaldi');
    });

    app.get('/new', function(req, res) {
        return loadIndex(req, res, 'new');
    });

    app.get('/albolive_cliente', function(req, res) {
        return loadIndex(req, res, 'albolive');
    });

    app.get('/clinic', function(req, res) {
        return loadIndex(req, res, 'clinic');
    });

    app.get('/infovda', function(req, res) {
        return loadIndex(req, res, 'infovda');
    });

    app.get('/scuolesci_segreteria', function(req, res) {
        return loadIndex(req, res, 'scuolesci_segreteria');
    });

    app.get('/scuolesci_maestro', function(req, res) {
        return loadIndex(req, res, 'scuolesci_maestro');
    });

    app.get('/scuolesci_ecommerce', function(req, res) {
        return loadIndex(req, res, 'scuolesci_ecommerce');
    });

    app.get('/scuolesci_timbratore', function(req, res) {
        return loadIndex(req, res, 'scuolesci_timbratore');
    });

    app.get('/sciclub', function(req, res) {
        return loadIndex(req, res, 'sciclub');
    });

    app.get('/guide_segreteria', function(req, res) {
        return loadIndex(req, res, 'guide_segreteria');
    });

    app.get('/guide_ecommerce', function(req, res) {
        return loadIndex(req, res, 'guide_ecommerce');
    });

    app.get('/medtrack', function(req, res) {
        return loadIndex(req, res, 'medtrack');
    });

    app.get('/maisonvive', function(req, res) {
        return loadIndex(req, res, 'maisonvive');
    });

    app.get('/bike_segreteria', function(req, res) {
        return loadIndex(req, res, 'bike_segreteria');
    });

    app.get('/bike_ecommerce', function(req, res) {
        return loadIndex(req, res, 'bike_ecommerce');
    });

    app.get('/guidemonterosa_segreteria', function(req, res) {
        return loadIndex(req, res, 'guidemonterosa_segreteria');
    });

    app.get('/booking', function(req, res) {
        sequelize.query('SELECT booking_online_is_active(\'' +
            req.headers['host'].split(".")[0] +'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT
            }).then(function(boot) {
                if (boot[0].booking_online_is_active && boot[0].booking_online_is_active === true) {
                    return loadIndex(req, res, 'booking');
                }
                else
                {
                    res.redirect('https://beebeeboard.com');
                }
            }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
        });

        
    });

    app.get('/booking/confirm_sms', function(req, res) {

        

    });

    app.get('/poppix', function(req, res) {
        return loadIndex(req, res, 'poppix');
    });

    /**
     * INTERCOM MESSAGER
     */
    var intercom = require('../application/controllers/intercom');
    app.post('/api/v1/intercom_initialize', intercom.intercom_initialize);
    app.post('/api/v1/intercom_submit', intercom.intercom_submit);

  


    app.get('/api/v1/check', function(req, res) {
        var redirect = '';

        if (req.query.redirect && req.query.redirect !== undefined &&
            req.query.redirect !== 'http://albolive-client.beebeeboard.com.s3-website.eu-central-1.amazonaws.com/') {
            redirect = '\'' + req.query.redirect + '\'';
        }else{
            redirect = null;
        }

        sequelize.query('SELECT current_version_v3(' +
            redirect + ',\''+
            req.headers['host'].split(".")[0] +'\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(boot) {
            res.status(200).send({
                'current': boot[0].current_version_v3
            });
        }).catch(function(err) {
            console.log(err);
            return res.status(500).send(err);
        });
    });


    app.get('/api/v1/device_association', organization.device_association);

    /**
     * STRIPE
     */
    var stripe_file = require('../application/controllers/stripe');
    app.get('/api/v1/stripe/connect', stripe_file.connect);

    /**
     * PAYPAL
     */
    var paypal_file = require('../application/controllers/paypal');
    //app.get('/api/v1/paypal/connect', paypal_file.connect);

    /**
     * Shortener
     */
    var shortener = require('../application/controllers/shortener');
    app.get('/bee/:id', shortener.find);
    app.post('/api/v1/bee', shortener.create);

    app.get('/api/v1/organizations/open', user.free_organization);
    app.get('/api/v1/organizations/open_advanced', user.free_organization_advanced);

   
    /**
     * Recovery Password
     */
    app.get('/api/v1/users/forgot', user.forgot);
    app.get('/api/v1/reset/:token', user.recovery_password);
    app.post('/api/v1/reset/:token', user.reset_password);

    /** 
     * Registration to Beebeeboard
     */
    app.post('/api/v1/organizations', user.create);
    app.post('/api/v1/clinic', user.create_clinic);

    app.get('/api/v1/registration/confirm/:token', user.registration_confirm);
    app.get('/api/v1/registration/verify_mail/:token', user.verify_mail_confirm);
    app.post('/api/v1/user_registration_request', user.user_registration_request);
    app.post('/api/v1/registration_complete', user.user_registration_complete);
    app.put('/api/v1/registration/:token', user.registration);
    app.get('/api/v1/registration/refresh/:token', user.refresh);
    app.get('/api/v1/registration_cron', user.registration_cron);
    app.get('/api/v1/reminder_cron', user.reminder_cron);
    app.get('/api/v1/subscription_cron', user.subscriptions_cron);
    app.get('/api/v1/google_event_cron', user.google_calendar_watch_cron);
    app.post('/api/v1/resend_registration', user.resend_registration);
    app.post('/api/v1/fresh_contact', user.fresh_contact);
    app.post('/api/v1/check_mail', user.check_mail);

    app.post('/api/v1/paypal/ipn', user.paypal_ipn);

    /**
     * Invitation Confirm to Beebeeboard
     */

    var ecommerce = require('../application/controllers/scuolasci/ecommerce');
    app.get('/api/v1/ecommerce_product/:id', ecommerce.product_status);
    app.post('/api/v1/dispo_individuali', ecommerce.find_maestro);
    app.get('/api/v1/individuali_maestro', ecommerce.event_individuali);
    app.post('/api/v1/ecommerce_private_dashboard', ecommerce.ecommerce_private_dashboard);
    app.post('/api/v1/ecommerce_private_dashboard_week', ecommerce.ecommerce_private_dashboard_week);
    app.get('/api/v1/ecommerce_scuolesci', ecommerce.ecommerce_products);
    app.get('/api/v1/ecommerce_dashboard', ecommerce.ecommerce_scuolesci_dashboard);
    app.get('/api/v1/ecommerce_cron_delete', ecommerce.ecommerce_cron_delete);
    app.get('/api/v1/events_pacchetto', ecommerce.ecommerce_dispo_pacchetti);
    app.post('/api/v1/scuolasci_freeprice', ecommerce.calcola_prezzo);
    app.post('/api/v1/ecommerce_freeprice', ecommerce.ecommerce_freeprice);
    app.post('/api/v1/ecommerce_check_product', ecommerce.ecommerce_check_product);
    app.get('/api/v1/sistema_prenotazioni_pendenti', ecommerce.sistema_prenotazioni_pendenti);
    //app.post('/api/v1/scuolasci_freeprice_v1', ecommerce.calcola_prezzo_sertorelli);
    app.post('/api/v1/ecommerce_turno_maestro', ecommerce.ecommerce_turno_maestro);

    /*
     * Mandrill webhook
     */
    app.post('/api/v1/mandrill/webhooks', mandrill.webhooks);
    app.post('/api/v1/send_admin_mail', skebby.send_organization_mail);
    app.post('/api/v1/skebby/webhooks', skebby.webhooks);


    /*
     * Beebeeboars status
     */
    app.get('/api/v1/status', user.status);

    /*
     * Google Calendar
     */
    app.post('/api/v1/calendar_notification', user.google_calendar_webhooks);

    /*
     * Test Route
     */
    app.get('/api/v1/test_route', user.test_route);


  
    
    var event = require('../application/controllers/event');

    /* BOOKING */
    app.get('/api/v1/dispos_data', event.dispos_data);
    app.get('/api/v1/dispos', event.dispos);
    app.get('/api/v1/dispos_groups', event.dispos_groups);
    app.post('/api/v1/confirm_booking', event.confirm_booking);
    app.post('/api/v1/booking_use_voucher', event.booking_use_voucher);
    app.post('/api/v1/booking_calcola_prezzo', event.booking_calcola_prezzo);
    app.get('/api/v1/active_booking', event.active_booking);
    app.post('/api/v1/active_booking', event.save_active_booking);
    app.get('/api/v1/booking_destroy_cron', user.booking_destroy_cron);
    app.post('/api/v1/booking_get_voucher',  event.booking_get_voucher);
    


    app.post('/api/v1/physitrack_webhook', physitrack.physitrack_webhook);

    app.post('/api/v1/yousign_webhooks', yousign.yousign_webhook);
    app.get('/api/v1/yousign_webhook_cron', yousign.yousign_webhook_cron);

    var sciclub = require('../application/controllers/sciclub/sciclub');
    app.post('/api/v1/sciclub_iscrizione_form', sciclub.sciclub_iscrizione_form);

    app.get('/api/v1/guidemonterosa_products_voucher',  guide_monterosa.guidemonterosa_products_voucher); 
    app.get('/api/v1/peakshunter_wp', peakshunter.peakshunter_wp);

    /**
     * Authorization Necessary for all models
     */
    app.all('/api/v1/*', passport.authenticate('bearer', {
        session: false
    }));

    app.get('/api/v1/generate_token_client', sciclub.generate_token_client);
    /**
     *  Request POS payments
     *
     */
    app.post('/api/v1/uncrt_request_payment', user.unicredit_request_payment);
    app.post('/api/v1/request_payment', user.request_payment);
    app.post('/api/v1/request_payment_base', user.request_payment_base);
    app.get('/api/v1/get_pos_method', user.get_pos_method);

    /**
     *  PARTNERS
     *  
     */
    var partner = require('../application/controllers/partner');
    app.post('/api/v1/partner', partner.start_collaboration);
    app.get('/api/v1/partner', partner.partner_info);

    /**
     * Default Authentication
     */
    app.get('/api/logout', oauth2.logout);

    app.get('/api/dialog/authorize', oauth2.authorization);
    app.post('/api/dialog/authorize/decision', oauth2.decision);
    app.post('/api/oauth/token', oauth2.token);

    /**
     * Mimicking google's token info endpoint from
     * https://developers.google.com/accounts/docs/OAuth2UserAgent#validatetoken
     */
    app.get('/api/tokeninfo', token.info);

    /**
     * Google login with google
     */
    app.get('/api/auth/google', function(req, res, next) {
        var domain = req.headers['host'].split(".")[0];
        console.log(req.query.redirect_page);
        passport.authenticate('google', {
            scope: ['email'],
            prompt: 'select_account',
            state: JSON.stringify({
                'domain': domain,
                'useragent': req.headers['user-agent'],
                'remoteAddress': req.connection.remoteAddress,
                'redirect_page': req.query.redirect_page
            })
        })(req, res, next);
    });

    app.get('/api/auth/google/return', function(req, res, next) {
        var state1 = req.query.state;

        passport.authenticate('google', function(err, token, org) {


            var state = JSON.parse(state1);

            if (token) {
                sequelize.query('SELECT find_redirect(\'' + token + '\',\'' +
                    org + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect && datas[0].find_redirect.redirect_page && datas[0].find_redirect.redirect_page !== undefined) {
                        if (datas[0].find_redirect.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect.redirect_page + '?result=true&access_token=' + token);
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/' + datas[0].find_redirect.redirect_page + '/#/token_login/success/' + token);
                        }
                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/start/#/token_login/success/' + token);
                    }
                }).catch(function(err) {
                    return res.status(500).render('google return: ' + err);
                });
            } else {
                sequelize.query('SELECT find_redirect(\'' + token + '\',\'' +
                    org + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect && datas[0].find_redirect.redirect_page && datas[0].find_redirect.redirect_page !== undefined) {
                        if (datas[0].find_redirect.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect.redirect_page + '?result=false');
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/');
                        }
                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/');
                    }
                }).catch(function(err) {
                    return res.status(500).render('google return: ' + err);
                });
            }

        })(req, res, next);
    });

    /**
     * Google set login with google for an existing account
     */

    app.get('/api/auth/set_google', function(req, res, next) {
        var domain = req.headers['host'].split(".")[0],
            token = req.query.contact_id;

        sequelize.query('SELECT find_token_for_google_reg(\'' +
            token + '\', \'' + domain + '\');', {
                raw: true,
                type: Sequelize.QueryTypes.SELECT,
                useMaster: true
            }).then(function(datas) {

            if (datas[0].find_token_for_google_reg && datas[0].find_token_for_google_reg[0] && datas[0].find_token_for_google_reg[0].user_id) {
                console.log(datas[0].find_token_for_google_reg);
                console.log(datas[0].find_token_for_google_reg[0]);
                passport.authenticate('set_google', {
                    scope: ['email', 'https://www.googleapis.com/auth/userinfo.profile'],
                    prompt: 'select_account',
                    state: JSON.stringify({
                        'contact_id': datas[0].find_token_for_google_reg[0].user_id,
                        'domain': domain,
                        'useragent': req.headers['user-agent'],
                        'remoteAddress': req.connection.remoteAddress,
                        'redirect_page': req.query.redirect_page
                    })
                })(req, res, next);
            } else {
                if (req.query.redirect_page) {
                    if (req.query.redirect_page.substring(0, 4) === 'http') {
                        res.redirect(req.query.redirect_page + '?result=false');
                    } else {
                        res.redirect('https://' + domain + '.beebeeboard.com/start/#/token_login/error/error');
                    }
                } else {
                    res.redirect('https://' + domain + '.beebeeboard.com/start/#/token_login/error/error');
                }

            }
        }).catch(function(err) {
            res.redirect('https://' + domain + '.beebeeboard.com/start/#/token_login/error/error');
        });

    });

    app.get('/api/auth/set_google/return', function(req, res, next) {
        var state1 = req.query.state;

        passport.authenticate('set_google', function(err, token, org) {

            var state = JSON.parse(state1);

            if (token) {
                sequelize.query('SELECT find_redirect(\'' + token + '\',\'' +
                    org + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect && datas[0].find_redirect.redirect_page && datas[0].find_redirect.redirect_page !== undefined) {
                        if (datas[0].find_redirect.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect.redirect_page + '?result=true&access_token=' + token);
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/' + datas[0].find_redirect.redirect_page + '/#/token_login/create/' + token);
                        }
                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/start/#/token_login/create/' + token);
                    }
                }).catch(function(err) {
                    return res.status(500).render('set_google return: ' + err);
                });
            } else {
                sequelize.query('SELECT find_redirect(\'' + token + '\',\'' +
                    org + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect && datas[0].find_redirect.redirect_page && datas[0].find_redirect.redirect_page !== undefined) {
                        if (datas[0].find_redirect.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect.redirect_page + '?result=false');
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/' + datas[0].find_redirect.redirect_page + '/#/token_login/error/exists');
                        }

                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/start/#/token_login/error/exists');
                    }
                }).catch(function(err) {
                    return res.status(500).render('set_google return: ' + err);
                });
            }

        })(req, res, next);
    });

    /**
     * Google create account with google
     */

    app.get('/api/auth/create_google', function(req, res, next) {
        var domain = req.headers['host'].split(".")[0];

        passport.authenticate('create_google', {
            scope: ['email',
                'https://www.googleapis.com/auth/userinfo.profile'
            ],
            prompt: 'select_account',
            state: JSON.stringify({
                'domain': domain,
                'useragent': req.headers['user-agent'],
                'remoteAddress': req.connection.remoteAddress,
                'redirect_page': req.query.redirect_page
            })
        })(req, res, next);
    });

    app.get('/api/auth/create_google/return', function(req, res, next) {
        var state1 = req.query.state;
        passport.authenticate('create_google', function(err, token, org) {
            var state = JSON.parse(state1);
            if (token) {
                sequelize.query('SELECT find_redirect(\'' + token + '\',\'' +
                    org + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect && datas[0].find_redirect.redirect_page && datas[0].find_redirect.redirect_page !== undefined) {
                        if (datas[0].find_redirect.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect.redirect_page + '?result=true&access_token=' + token);
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/' + datas[0].find_redirect.redirect_page + '/#/token_login/create/' + token);
                        }

                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/start/#/token_login/create/' + token);
                    }
                }).catch(function(err) {
                    return res.status(500).render('set_google return: ' + err);
                });
            } else {
                sequelize.query('SELECT find_redirect(\'' + token + '\',\'' +
                    org + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect && datas[0].find_redirect.redirect_page && datas[0].find_redirect.redirect_page !== undefined) {
                        if (datas[0].find_redirect.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect.redirect_page + '?result=false');
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/' + datas[0].find_redirect.redirect_page + '/#/token_login/error/exists');
                        }

                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/start/#/token_login/error/exists');
                    }
                }).catch(function(err) {
                    return res.status(500).render('set_google return: ' + err);
                });
            }

        })(req, res, next);
    });

    /** 
     * Google Calendar
     */
    /**
     * Google login with google
     */
    app.get('/api/v1/google_calendar', function(req, res, next) {
        var domain = req.headers['host'].split(".")[0];
        passport.authenticate('google_calendar', {
            scope: ['email', 'https://www.googleapis.com/auth/calendar'],
            prompt: 'consent',
            accessType: 'offline',
            state: JSON.stringify({
                'domain': domain,
                'useragent': req.headers['user-agent'],
                'remoteAddress': req.connection.remoteAddress,
                'contact_id': req.user.id,
                'access_token': req.query.contact_id
            })
        })(req, res, next);
    });

    app.get('/api/auth/google_calendar/return', function(req, res, next) {
        var state1 = req.query.state;
        passport.authenticate('google_calendar', function(err, ref_token, acc_token, org) {

            var state = JSON.parse(state1);
            if (state.contact_id) {

                sequelize.query('SELECT find_redirect_contact(' + 
                    state.contact_id + ',\'' +
                    state.domain + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                        useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect_contact && datas[0].find_redirect_contact.redirect_page && datas[0].find_redirect_contact.redirect_page !== undefined) {
                        if (datas[0].find_redirect_contact.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect_contact.redirect_page + '?result=true&access_token=' + state.contact_id);
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/' + datas[0].find_redirect_contact.redirect_page + '/#/settings/integrations');
                        }
                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/start/');
                    }
                }).catch(function(err) {
                    return res.status(500).render('google return: ' + err);
                });
            } else {
                sequelize.query('SELECT find_redirect_contact(' + state.contact_id + ',\'' +
                    state.domain + '\');', {
                        raw: true,
                        type: Sequelize.QueryTypes.SELECT,
                useMaster: true
                    }).then(function(datas) {
                    if (datas[0].find_redirect_contact && datas[0].find_redirect_contact.redirect_page && datas[0].find_redirect_contact.redirect_page !== undefined) {
                        if (datas[0].find_redirect_contact.redirect_page.substring(0, 4) === 'http') {
                            res.redirect(datas[0].find_redirect_contact.redirect_page + '?result=false');
                        } else {
                            res.redirect('https://' + state.domain + '.beebeeboard.com/');
                        }
                    } else {
                        res.redirect('https://' + state.domain + '.beebeeboard.com/');
                    }
                }).catch(function(err) {
                    return res.status(500).render('google return: ' + err);
                });
            }

        })(req, res, next);
    });

    app.get('/api/v1/revoke_google', utility.check_subscription_expire, user.revoke_google);

    app.post('/api/v1/mandrill', mandrill.write);
    /**
     * Contact
     */
    var contact = require('../application/controllers/contact');
    app.get('/api/v1/contacts', utility.check_subscription_expire, contact.index);
    app.get('/api/v1/contacts/me', contact.me);
    app.get('/api/v1/contacts/:id', utility.check_subscription_expire, contact.find);
    app.get('/api/v1/contacts_csv', utility.check_subscription_expire, contact.export_data);
    app.post('/api/v1/contacts', utility.check_subscription_expire, contact.create);
    app.post('/api/v1/contacts_all', utility.check_subscription_expire, contact.update_all);
    app.patch('/api/v1/contacts/:id', contact.update);
    app.put('/api/v1/contacts/:id', utility.check_subscription_expire, contact.update);
    app.delete('/api/v1/contacts/:id', utility.check_subscription_expire, contact.destroy);
    app.get('/api/v1/contacts_stats', utility.check_subscription_expire, contact.stats);
    app.post('/api/v1/contacts/revoke', utility.check_subscription_expire, contact.revoke);
    app.get('/api/v1/merge_contact', utility.check_subscription_expire, contact.doppi_contatti);
    app.post('/api/v1/no_merge_contact', utility.check_subscription_expire, contact.ignora_doppi_contatti);
    app.get('/api/v1/no_merge_contact', utility.check_subscription_expire, contact.doppi_contatti_ignorati);
    app.post('/api/v1/merge_contact', utility.check_subscription_expire, contact.accoppia_contatti);
    app.get('/api/v1/fornitori_charts', utility.check_subscription_expire, contact.fornitori_charts);
    app.get('/api/v1/iva_charts', utility.check_subscription_expire, contact.iva_charts);
    app.get('/api/v1/annual_fornitori_charts', utility.check_subscription_expire, contact.annual_fornitori_charts);
    app.get('/api/v1/contratti_charts', utility.check_subscription_expire, contact.contratti_fornitori_charts);
    app.get('/api/v1/annual_contratti_charts', utility.check_subscription_expire, contact.contratti_annual_fornitori_charts);
    app.post('/api/v1/contacts/codice_fiscale', utility.check_subscription_expire, contact.codice_fiscale);
    app.post('/api/v1/codfiscale_to_comune', utility.check_subscription_expire, contact.codfiscale_to_comune);
    app.post('/api/v1/user_new_account', utility.check_subscription_expire, contact.user_new_account);
    app.post('/api/v1/user_change_username', utility.check_subscription_expire, contact.user_change_username);  
    app.post('/api/v1/user_change_password', utility.check_subscription_expire, contact.user_change_password);
    app.post('/api/v1/user_change_role', utility.check_subscription_expire, contact.user_change_role);


    /* Specialisti */
    var specialista = require('../application/controllers/clinical/specialista');
    app.get('/api/v1/listino_spec', utility.check_subscription_expire, specialista.get_listino);
    app.post('/api/v1/listino_spec', utility.check_subscription_expire, specialista.post_listino);
    app.get('/api/v1/assurance_listino', utility.check_subscription_expire, specialista.get_assurance_listino);
    app.post('/api/v1/assurance_listino', utility.check_subscription_expire, specialista.post_assurance_listino);

    /**
     * PaYment
     */
    var payment = require('../application/controllers/payment');
    app.get('/api/v1/payments', utility.check_subscription_expire, payment.index);
    app.get('/api/v1/payments_csv', utility.check_subscription_expire, payment.export_data);
    app.get('/api/v1/payments/:id', utility.check_subscription_expire, payment.find);
    app.post('/api/v1/payments', utility.check_subscription_expire, payment.create);
    app.patch('/api/v1/payments/:id', utility.check_subscription_expire, payment.update);
    app.post('/api/v1/payments_all', utility.check_subscription_expire, payment.update_all);
    app.delete('/api/v1/payments/:id', utility.check_subscription_expire, payment.destroy);
    app.get('/api/v1/payments_stats', utility.check_subscription_expire, payment.stats);
    app.post('/api/v1/pay_event', utility.check_subscription_expire, payment.sanitize_event_payment);
    app.post('/api/v1/use_credit', utility.check_subscription_expire, payment.use_contact_credit);
    app.post('/api/v1/free_payment', utility.check_subscription_expire, payment.sblocca_pagamento);
    app.get('/api/v1/prima_nota', utility.check_subscription_expire, payment.prima_nota);
    app.get('/api/v1/prima_nota_csv', utility.check_subscription_expire, payment.export_prima_nota);
    app.get('/api/v1/payment_request_success', utility.check_subscription_expire, payment.payment_request_success);
    app.get('/api/v1/get_ritenute', utility.check_subscription_expire, payment.get_ritenute);
    app.get('/api/v1/get_bolli', utility.check_subscription_expire, payment.get_bolli);
    app.get('/api/v1/export_ritenute', utility.check_subscription_expire, payment.export_ritenute);
    app.post('/api/v1/salda_ritenute', utility.check_subscription_expire, payment.salda_ritenute);
    app.post('/api/v1/save_blocco_pagamenti', utility.check_subscription_expire, payment.save_blocco_pagamenti);
    

    /**
     * Address
     */
    var address = require('../application/controllers/address');
    app.post('/api/v1/addresses', address.create);
    app.get('/api/v1/addresses/:id', utility.check_subscription_expire, address.find);
    app.patch('/api/v1/addresses/:id', address.update);
    app.delete('/api/v1/addresses/:id', utility.check_subscription_expire, address.destroy);
    app.get('/api/v1/addresses_cluster', utility.check_subscription_expire, address.addresses_cluster);

    /**
     * Custom_Fields
     */
    var custom_field = require('../application/controllers/custom_field');
    app.get('/api/v1/custom_fields/:id', utility.check_subscription_expire, custom_field.find);
    app.post('/api/v1/custom_fields', utility.check_subscription_expire, custom_field.create);
    app.patch('/api/v1/custom_fields/:id', utility.check_subscription_expire, custom_field.update);
    app.delete('/api/v1/custom_fields/:id', utility.check_subscription_expire, custom_field.destroy);
    app.post('/api/v1/calc_custom_fields', utility.check_subscription_expire, custom_field.get_calc_custom_fields);

    /**
     * Reminder
     */
    var reminder = require('../application/controllers/reminder');
    app.post('/api/v1/reminders', utility.check_subscription_expire, reminder.create);
    app.post('/api/v1/send_sms_now', utility.check_subscription_expire, reminder.send_sms_now);
    app.patch('/api/v1/reminders/:id', utility.check_subscription_expire, reminder.update);
    app.delete('/api/v1/reminders/:id', utility.check_subscription_expire, reminder.destroy);
    app.get('/api/v1/communications', utility.check_subscription_expire, reminder.get_communications);
    app.get('/api/v1/communications_stats', utility.check_subscription_expire, reminder.communications_stats);
    app.get('/api/v1/communications_filter', utility.check_subscription_expire, reminder.communications_filter_clinic);
    app.get('/api/v1/communications_filter_scuolasci', utility.check_subscription_expire, reminder.communications_filter_scuolasci);

    /**
     * Reminder Settings
     */
    var remindersetting = require('../application/controllers/remindersetting');
    app.post('/api/v1/reminder_settings', utility.check_subscription_expire, remindersetting.create);
    app.patch('/api/v1/reminder_settings/:id', utility.check_subscription_expire, remindersetting.update);
    app.delete('/api/v1/reminder_settings/:id', utility.check_subscription_expire, remindersetting.destroy);

    /**
     * Pdf
     */
    var pdf = require('../application/controllers/pdf');
    app.get('/api/v1/pdf', utility.check_subscription_expire, pdf.index);
    app.get('/api/v1/privacy', utility.check_subscription_expire, pdf.privacy);
    app.post('/api/v1/pdf', utility.check_subscription_expire, pdf.send_mail);
    app.post('/api/v1/send_signed_document', utility.check_subscription_expire, pdf.send_signed_document);
    app.get('/api/v1/project_pdf', utility.check_subscription_expire, pdf.project);
    app.post('/api/v1/project_pdf', utility.check_subscription_expire, pdf.send_project_mail);
    app.post('/api/v1/contact_pdf', utility.check_subscription_expire, pdf.send_contact_mail);
    app.post('/api/v1/send_tagliando', utility.check_subscription_expire, pdf.send_tagliando);
    app.get('/api/v1/pdf_our_invoices', utility.check_subscription_expire, pdf.pdf_our_invoices);

    /**
     * Event
     */
    app.get('/api/v1/events', utility.check_subscription_expire, event.index);
    app.get('/api/v1/events/:id', utility.check_subscription_expire, event.find);
    app.post('/api/v1/events', utility.check_subscription_expire, event.create);
    app.get('/api/v1/events_csv', utility.check_subscription_expire, event.export_data);
    app.patch('/api/v1/events/:id', utility.check_subscription_expire, event.update);
    app.post('/api/v1/events_all', utility.check_subscription_expire, event.update_all);
    app.put('/api/v1/events/:id', utility.check_subscription_expire, event.update);
    app.delete('/api/v1/events/:id', utility.check_subscription_expire, event.destroy);
    app.post('/api/v1/free_event', utility.check_subscription_expire, event.sblocca_evento);
    app.post('/api/v1/add_event_invoice', utility.check_subscription_expire, event.add_event_invoice); 
    app.get('/api/v1/unit_dispo', utility.check_subscription_expire, event.unit_dispo); 
    app.get('/api/v1/groups/:id', utility.check_subscription_expire, event.get_group); 
    app.post('/api/v1/groups_add', utility.check_subscription_expire, event.group_add_people); 
    app.post('/api/v1/groups_del', utility.check_subscription_expire, event.group_del_people); 
    app.post('/api/v1/groups_add_pkg', utility.check_subscription_expire, event.group_add_pack); 
    app.get('/api/v1/promo_codes', utility.check_subscription_expire,event.promo_codes);
    app.post('/api/v1/promo_codes', utility.check_subscription_expire,event.save_promo_codes);
    app.delete('/api/v1/promo_codes/:id', utility.check_subscription_expire,event.delete_promo_codes);
    
    /**
     * Roles
     */
    var role = require('../application/controllers/role');
    app.get('/api/v1/roles/:id', utility.check_subscription_expire, role.find);
    app.post('/api/v1/roles', utility.check_subscription_expire, role.create);
    app.patch('/api/v1/roles/:id', utility.check_subscription_expire, role.update);
    app.delete('/api/v1/roles/:id', utility.check_subscription_expire, role.destroy);
    app.get('/api/v1/roles', utility.check_subscription_expire, role.index);

    /**
     * EventStatus
     */
    var event_status = require('../application/controllers/event_status');
    app.post('/api/v1/event_states', utility.check_subscription_expire, event_status.create);
    app.patch('/api/v1/event_states/:id', utility.check_subscription_expire, event_status.update);
    app.delete('/api/v1/event_states/:id', utility.check_subscription_expire, event_status.destroy);
    app.get('/api/v1/event_states/:id', utility.check_subscription_expire, event_status.find);
    app.get('/api/v1/event_states', utility.check_subscription_expire, event_status.index);

    /**
     * ContactStatus
     */
    var contact_status = require('../application/controllers/contact_status');
    app.get('/api/v1/contact_states/:id', utility.check_subscription_expire, contact_status.find);
    app.get('/api/v1/contact_states', utility.check_subscription_expire, contact_status.index);
    app.post('/api/v1/contact_states', utility.check_subscription_expire, contact_status.create);
    app.patch('/api/v1/contact_states/:id', utility.check_subscription_expire, contact_status.update);
    app.get('/api/v1/item_states', utility.check_subscription_expire, contact_status.item_index);
    app.delete('/api/v1/contact_states/:id', utility.check_subscription_expire, contact_status.destroy);

    /**
     * Push notification
     */
    var push_notify = require('../application/controllers/push_notify');
    app.post('/api/v1/push_notify_ins', utility.check_subscription_expire, push_notify.create);
    app.get('/api/v1/push_notify_players', utility.check_subscription_expire, push_notify.get_player);
    app.post('/api/v1/push_notify_del', utility.check_subscription_expire, push_notify.destroy);

     /**
     * Medtrack
     */
    var medtrack = require('../application/controllers/medtrack/medtrack');
    app.get('/api/v1/medtrack_report_bv', utility.check_subscription_expire, medtrack.medtrack_basivita_charts);
    app.get('/api/v1/medtrack_report_tt', utility.check_subscription_expire, medtrack.medtrack_charts_tot);
    /**
     * ProductStatus
     */
    var product_status = require('../application/controllers/product_status');
    app.get('/api/v1/product_states/:id', utility.check_subscription_expire, product_status.find);
    app.get('/api/v1/product_states', utility.check_subscription_expire, product_status.index);
    app.post('/api/v1/product_states', utility.check_subscription_expire, product_status.create);
    app.patch('/api/v1/product_states/:id', utility.check_subscription_expire, product_status.update);
    app.delete('/api/v1/product_states/:id', utility.check_subscription_expire, product_status.destroy);

    /**
     * ProjectStatus
     */
    var project_status = require('../application/controllers/project_status');
    app.get('/api/v1/project_states/:id', utility.check_subscription_expire, project_status.find);
    app.get('/api/v1/project_states', utility.check_subscription_expire, project_status.index);
    app.post('/api/v1/project_states', utility.check_subscription_expire, project_status.create);
    app.patch('/api/v1/project_states/:id', utility.check_subscription_expire, project_status.update);
    app.delete('/api/v1/project_states/:id', utility.check_subscription_expire, project_status.destroy);

    /**
     * WorkhourStatus
     */
    var workhour_status = require('../application/controllers/workhour_status');
    app.get('/api/v1/workhour_states/:id', utility.check_subscription_expire, workhour_status.find);
    app.post('/api/v1/workhour_states', utility.check_subscription_expire, workhour_status.create);
    app.patch('/api/v1/workhour_states/:id', utility.check_subscription_expire, workhour_status.update);
    app.delete('/api/v1/workhour_states/:id', utility.check_subscription_expire, workhour_status.destroy);
    app.get('/api/v1/workhour_states', utility.check_subscription_expire, workhour_status.index);


    /**
     * Custom_field_Definitions
     */
    var custom_field_definition = require('../application/controllers/custom_field_definition');
    app.get('/api/v1/custom_field_definitions', utility.check_subscription_expire, custom_field_definition.index);
    app.post('/api/v1/custom_field_definitions', utility.check_subscription_expire, custom_field_definition.create);
    app.patch('/api/v1/custom_field_definitions/:id', utility.check_subscription_expire, custom_field_definition.update);
    app.delete('/api/v1/custom_field_definitions/:id', utility.check_subscription_expire, custom_field_definition.destroy);

    /**
     * Invoice
     */
    var invoice = require('../application/controllers/invoice');
    app.get('/api/v1/invoices', utility.check_subscription_expire, invoice.index);
    app.get('/api/v1/invoices_csv', utility.check_subscription_expire, invoice.export_data);
    app.get('/api/v1/invoices/:id', utility.check_subscription_expire, invoice.find);
    app.post('/api/v1/invoices', utility.check_subscription_expire, invoice.create);
    app.post('/api/v1/invoices_all', utility.check_subscription_expire, invoice.update_all);
    app.get('/api/v1/invoices/new/:id', utility.check_subscription_expire, invoice.new_invoice);
    app.get('/api/v1/document_type_number', utility.check_subscription_expire, invoice.document_type_number);
    app.get('/api/v1/invoices/estimate/:id', utility.check_subscription_expire, invoice.create_estimate_credit);
    app.post('/api/v1/cast_document', utility.check_subscription_expire, invoice.cast_document);

    app.post('/api/v1/cast_to_expense', utility.check_subscription_expire, invoice.cast_expense);
    app.post('/api/v1/invoice_change_status', utility.check_subscription_expire, invoice.change_status_invoice);
    app.patch('/api/v1/invoices/:id', utility.check_subscription_expire, invoice.update);
    app.delete('/api/v1/invoices/:id', utility.check_subscription_expire, invoice.destroy);
    app.get('/api/v1/invoices_stats', utility.check_subscription_expire, invoice.stats);
    app.get('/api/v1/autofatture_stats', utility.check_subscription_expire, invoice.autofatture_stats);
    app.get('/api/v1/estimates_stats', utility.check_subscription_expire, invoice.estimate_stats);
    app.get('/api/v1/orders_stats', utility.check_subscription_expire, invoice.orders_stats);
    app.get('/api/v1/ddts_stats', utility.check_subscription_expire, invoice.ddts_stats);
    app.get('/api/v1/receipts_stats', utility.check_subscription_expire, invoice.receipts_stats);
    app.get('/api/v1/contracts_stats', utility.check_subscription_expire, invoice.contracts_stats);
    app.get('/api/v1/invoices_charts', utility.check_subscription_expire, invoice.charts);
    app.get('/api/v1/customer_charts', utility.check_subscription_expire, invoice.customer_charts);
    app.get('/api/v1/inv_categories_charts', utility.check_subscription_expire, invoice.inv_categories_charts);
    app.get('/api/v1/inv_annual_categories_charts', utility.check_subscription_expire, invoice.inv_annual_categories_charts);
    app.get('/api/v1/inv_sub_categories_charts', utility.check_subscription_expire, invoice.inv_sub_categories_charts);
    app.get('/api/v1/competenze_entrate', utility.check_subscription_expire, invoice.competenze_entrate);
    app.post('/api/v1/change_invoice_status', utility.check_subscription_expire, invoice.change_invoice_status);

    /**
     * Electronic Invoice
     */
    var el_invoice = require('../application/controllers/electronic_invoice');
    app.get('/api/v1/invoices/xml/:id', utility.check_subscription_expire, el_invoice.fattura_xml);
    app.get('/api/v1/invoices/verify_xml/:id', utility.check_subscription_expire, el_invoice.verifica_fattura_xml);
    app.post('/api/v1/invoices/xml/:id', utility.check_subscription_expire, el_invoice.invia_fattura_xml);
    app.post('/api/v1/import_xml_invoice', utility.check_subscription_expire, el_invoice.invoices_from_xml);
    app.post('/api/v1/import_xml_expense', utility.check_subscription_expire, el_invoice.expenses_from_xml);
    app.get('/api/v1/invoices/notifications/active', utility.check_subscription_expire, el_invoice.controllo_notifiche_attive);
    app.get('/api/v1/invoices/notifications/passive', utility.check_subscription_expire, el_invoice.controllo_notifiche_passive);
    //app.get('/api/v1/salva_pdf', utility.check_subscription_expire, el_invoice.salva_attachment);
    app.get('/api/v1/search_ente', utility.check_subscription_expire, el_invoice.ipa_search);
    app.get('/api/v1/find_ente', utility.check_subscription_expire, el_invoice.ipa_dati_ente);

    /**
    * Electronic Invoice
    */
    var ts_system = require('../application/controllers/ts');
    app.post('/api/v1/ts_control_enable', utility.check_subscription_expire, ts_system.ts_control_enable);
    app.post('/api/v1/ts_single', utility.check_subscription_expire, ts_system.ts_single_xml);
    app.get('/api/v1/ts_multiple', utility.check_subscription_expire, ts_system.ts_multiple_xml);
    app.get('/api/v1/ts_response/:id', utility.check_subscription_expire, ts_system.ts_response);
    app.get('/api/v1/ts_xml', utility.check_subscription_expire, ts_system.ts_xml);
    app.get('/api/v1/ts_zip', utility.check_subscription_expire, ts_system.ts_zip_xml);
    app.get('/api/v1/sistema_ts', utility.check_subscription_expire, ts_system.sistema_ts);
    app.get('/api/v1/ts_ricevuta_pdf/:id', utility.check_subscription_expire, ts_system.ts_ricevuta_pdf);
    app.get('/api/v1/ts_dettaglio_errori/:id', utility.check_subscription_expire, ts_system.ts_dettaglio_errori);
    app.get('/api/v1/dashboard_ts', utility.check_subscription_expire, ts_system.dashboard_ts);
    app.get('/api/v1/user_ts', utility.check_subscription_expire, ts_system.user_ts);
    app.get('/api/v1/ts_storico/:id', utility.check_subscription_expire, ts_system.ts_storico);
    app.post('/api/v1/ts_corretta', utility.check_subscription_expire, ts_system.ts_corretta);
    /**
     * Archiver
     */
    var archiver = require('../application/controllers/archiver');
    app.get('/api/v1/archivier/invoice', utility.check_subscription_expire, archiver.invoice);
    app.get('/api/v1/archivier/invoice_new', utility.check_subscription_expire, archiver.invoice_to_aws);
    app.get('/api/v1/archivier/expense_new', utility.check_subscription_expire, archiver.expense_to_aws);
    app.get('/api/v1/archivier/expense', utility.check_subscription_expire, archiver.expense);
    app.get('/api/v1/archivier/invoice_info', utility.check_subscription_expire, archiver.invoice_info);
    app.get('/api/v1/archivier/expense_info', utility.check_subscription_expire, archiver.expense_info);
    app.get('/api/v1/archivier/stop', utility.check_subscription_expire, archiver.stop_process);

    /**
     * Expense
     */
    var expense = require('../application/controllers/expense');
    app.get('/api/v1/expenses', utility.check_subscription_expire, expense.index);
    app.get('/api/v1/expenses_csv', utility.check_subscription_expire, expense.export_data);
    app.get('/api/v1/expenses/:id', utility.check_subscription_expire, expense.find);
    app.post('/api/v1/expenses', utility.check_subscription_expire, expense.create);
    app.post('/api/v1/expenses_all', utility.check_subscription_expire, expense.update_all);
    app.post('/api/v1/expense_credit', utility.check_subscription_expire, expense.expense_credit);
    app.patch('/api/v1/expenses/:id', utility.check_subscription_expire, expense.update);
    app.delete('/api/v1/expenses/:id', utility.check_subscription_expire, expense.destroy);
    app.get('/api/v1/expenses_stats', utility.check_subscription_expire, expense.stats);
    app.get('/api/v1/categories_charts', utility.check_subscription_expire, expense.categories_charts);
    app.get('/api/v1/annual_categories_charts', utility.check_subscription_expire, expense.annual_categories_charts);
    app.get('/api/v1/sub_categories_charts', utility.check_subscription_expire, expense.sub_categories_charts);
    app.post('/api/v1/change_expense_status', utility.check_subscription_expire, expense.change_expense_status);
    app.post('/api/v1/autofattura', utility.check_subscription_expire, expense.autofattura);
    app.post('/api/v1/storno_autofattura', utility.check_subscription_expire, expense.storno_autofattura);


    var expensearticle = require('../application/controllers/expensearticle');
    app.post('/api/v1/expensearticles', utility.check_subscription_expire, expensearticle.create);
    app.patch('/api/v1/expensearticles/:id', utility.check_subscription_expire, expensearticle.update);
    app.delete('/api/v1/expensearticles/:id', utility.check_subscription_expire, expensearticle.destroy);

    /**
     * Expire Dates
     */
    var expire_date = require('../application/controllers/expire_date');
    app.post('/api/v1/expire_dates', utility.check_subscription_expire, expire_date.create);
    app.patch('/api/v1/expire_dates/:id', utility.check_subscription_expire, expire_date.update);
    app.delete('/api/v1/expire_dates/:id', utility.check_subscription_expire, expire_date.destroy);

    /**
     * Tags
     */
    var tag = require('../application/controllers/tag');
    app.post('/api/v1/tags', utility.check_subscription_expire, tag.create);
    app.patch('/api/v1/tags/:id', utility.check_subscription_expire, tag.update);
    app.delete('/api/v1/tags/:id', utility.check_subscription_expire, tag.destroy);

    /**
     * Settings and operations
     */
    var setting = require('../application/controllers/setting');
    app.post('/api/v1/bulk_operation', utility.check_subscription_expire, setting.bulk_operation);


    /**
     * Categories
     */
    var categorie = require('../application/controllers/category');
    app.post('/api/v1/categories', utility.check_subscription_expire, categorie.create);
    app.patch('/api/v1/categories/:id', utility.check_subscription_expire, categorie.update);
    app.delete('/api/v1/categories/:id', utility.check_subscription_expire, categorie.destroy);

    /**
     * Sub-Categories
     */
    var sub_categorie = require('../application/controllers/sub_category');
    app.post('/api/v1/sub_categories', utility.check_subscription_expire, sub_categorie.create);
    app.patch('/api/v1/sub_categories/:id', utility.check_subscription_expire, sub_categorie.update);
    app.delete('/api/v1/sub_categories/:id', utility.check_subscription_expire, sub_categorie.destroy);


    /**
     * Product Categories
     */
    var product_categorie = require('../application/controllers/product_category');
    app.get('/api/v1/product_categories', utility.check_subscription_expire, product_categorie.index);
    app.get('/api/v1/product_categories/:id', utility.check_subscription_expire, product_categorie.find);
    app.post('/api/v1/product_categories', utility.check_subscription_expire, product_categorie.create);
    app.patch('/api/v1/product_categories/:id', utility.check_subscription_expire, product_categorie.update);
    app.delete('/api/v1/product_categories/:id', utility.check_subscription_expire, product_categorie.destroy);

    /**
     * User_calendars
     */
    var user_calendar = require('../application/controllers/user_calendar');
    app.post('/api/v1/user_calendars', utility.check_subscription_expire, user_calendar.create);
    app.patch('/api/v1/user_calendars/:id', utility.check_subscription_expire, user_calendar.update);
    app.delete('/api/v1/user_calendars/:id', utility.check_subscription_expire, user_calendar.destroy);
    app.get('/api/v1/google_calendar/list', utility.check_subscription_expire, user_calendar.google_calendar_list);
    app.post('/api/v1/google_calendar/sync', utility.check_subscription_expire, user_calendar.save_google_calendar);
    app.post('/api/v1/google_calendar/import', utility.check_subscription_expire, user_calendar.import_from_google_to_beebee);
    app.post('/api/v1/google_calendar/export', utility.check_subscription_expire, user_calendar.export_from_beebee_to_google);

    /**
     * User_payment_methods
     */
    var user_payment_method = require('../application/controllers/user_payment_method');
    app.post('/api/v1/payment_methods', utility.check_subscription_expire, user_payment_method.create);
    app.patch('/api/v1/payment_methods/:id', utility.check_subscription_expire, user_payment_method.update);
    app.delete('/api/v1/payment_methods/:id', utility.check_subscription_expire, user_payment_method.destroy);

    /**
     * Custom_reminders
     */
    var custom_reminder = require('../application/controllers/custom_reminder');
    app.post('/api/v1/custom_reminders', utility.check_subscription_expire, custom_reminder.create);
    app.patch('/api/v1/custom_reminders/:id', utility.check_subscription_expire, custom_reminder.update);
    app.delete('/api/v1/custom_reminders/:id', utility.check_subscription_expire, custom_reminder.destroy);

    /**
     * User_taxes
     */
    var user_taxe = require('../application/controllers/user_taxe');
    app.post('/api/v1/user_taxes', utility.check_subscription_expire, user_taxe.create);
    app.patch('/api/v1/user_taxes/:id', utility.check_subscription_expire, user_taxe.update);
    app.delete('/api/v1/user_taxes/:id', utility.check_subscription_expire, user_taxe.destroy);

     /**
     * Assurances
     */
    var assurance = require('../application/controllers/assurance');
    app.get('/api/v1/assurances', utility.check_subscription_expire, assurance.assurance_index);
    app.get('/api/v1/assurances/:id', utility.check_subscription_expire, assurance.assurance_get);
    app.post('/api/v1/assurances', utility.check_subscription_expire, assurance.create);
    app.patch('/api/v1/assurances/:id', utility.check_subscription_expire, assurance.update);
    app.delete('/api/v1/assurances/:id', utility.check_subscription_expire, assurance.destroy);
    app.get('/api/v1/get_assurances', utility.check_subscription_expire, assurance.get_assurances);
    app.post('/api/v1/assurance_default', utility.check_subscription_expire, assurance.create);
    app.post('/api/v1/assurance_fields', utility.check_subscription_expire, assurance.assurance_fields);
    app.get('/api/v1/clinic_chart_all_assurance', utility.check_subscription_expire, assurance.clinic_chart_all_assurance);
    app.get('/api/v1/clinic_chart_assurance', utility.check_subscription_expire, assurance.clinic_chart_assurance);
    app.get('/api/v1/clinic_report_assurance', utility.check_subscription_expire, assurance.clinic_report_assurance);
    app.get('/api/v1/clinic_report_assurance_export', utility.check_subscription_expire, assurance.clinic_report_assurance_export);



    /**
     * User_sezionali
     */
    var user_sezionale = require('../application/controllers/user_sezionali');
    app.post('/api/v1/user_sezionales', utility.check_subscription_expire, user_sezionale.create);
    app.patch('/api/v1/user_sezionales/:id', utility.check_subscription_expire, user_sezionale.update);
    app.delete('/api/v1/user_sezionales/:id', utility.check_subscription_expire, user_sezionale.destroy);

    /**
     * User_invoice_settings
     */
    var user_invoice_setting = require('../application/controllers/user_invoice_setting');
    app.post('/api/v1/user_invoice_settings', utility.check_subscription_expire, user_invoice_setting.create);
    app.patch('/api/v1/user_invoice_settings/:id', utility.check_subscription_expire, user_invoice_setting.update);
    app.delete('/api/v1/user_invoice_settings/:id', utility.check_subscription_expire, user_invoice_setting.destroy);

    /**
     * Invoicegroups
     */
    var invoicegroup = require('../application/controllers/invoicegroup');
    app.get('/api/v1/invoicegroups/:id', utility.check_subscription_expire, invoicegroup.find);
    app.post('/api/v1/invoicegroups', utility.check_subscription_expire, invoicegroup.create);
    app.patch('/api/v1/invoicegroups/:id', utility.check_subscription_expire, invoicegroup.update);
    app.delete('/api/v1/invoicegroups/:id', utility.check_subscription_expire, invoicegroup.destroy);

    /**
     * Invoiceproducts
     */
    var invoiceproduct = require('../application/controllers/invoiceproduct');
    app.get('/api/v1/invoiceproducts/:id', utility.check_subscription_expire, invoiceproduct.find);
    app.post('/api/v1/invoiceproducts', utility.check_subscription_expire, invoiceproduct.create);
    app.patch('/api/v1/invoiceproducts/:id', utility.check_subscription_expire, invoiceproduct.update);
    app.delete('/api/v1/invoiceproducts/:id', utility.check_subscription_expire, invoiceproduct.destroy);

    /**
     * Invoiceproduct_taxes
     */
    var invoiceproduct_taxe = require('../application/controllers/invoiceproduct_taxe');
    app.post('/api/v1/invoiceproduct_taxes', utility.check_subscription_expire, invoiceproduct_taxe.create);
    app.patch('/api/v1/invoiceproduct_taxes/:id', utility.check_subscription_expire, invoiceproduct_taxe.update);
    app.delete('/api/v1/invoiceproduct_taxes/:id', utility.check_subscription_expire, invoiceproduct_taxe.destroy);

    /**
     * Invoicetaxe
     */
    var invoicetaxe = require('../application/controllers/invoicetaxe');
    app.get('/api/v1/invoicetaxes/:id', utility.check_subscription_expire, invoicetaxe.find);
    app.post('/api/v1/invoicetaxes', utility.check_subscription_expire, invoicetaxe.create);
    app.patch('/api/v1/invoicetaxes/:id', utility.check_subscription_expire, invoicetaxe.update);
    app.delete('/api/v1/invoicetaxes/:id', utility.check_subscription_expire, invoicetaxe.destroy);

    /**
     * Invoicestyles
     */
    var invoice_style = require('../application/controllers/invoice_style');
    app.get('/api/v1/invoice_styles/default', utility.check_subscription_expire, invoice_style.default);
    app.get('/api/v1/invoice_styles/contact_default', utility.check_subscription_expire, invoice_style.contact_default);
    app.get('/api/v1/invoice_styles/:id', utility.check_subscription_expire, invoice_style.find);
    app.post('/api/v1/invoice_styles', utility.check_subscription_expire, invoice_style.create);
    app.patch('/api/v1/invoice_styles/:id', utility.check_subscription_expire, invoice_style.update);
    app.delete('/api/v1/invoice_styles/:id', utility.check_subscription_expire, invoice_style.destroy);


    /**
     * Product
     */
    var product = require('../application/controllers/product');
    app.get('/api/v1/products', utility.check_subscription_expire, product.index);
    app.post('/api/v1/products', utility.check_subscription_expire, product.create);
    app.get('/api/v1/products_csv', utility.check_subscription_expire, product.export_data);
    app.get('/api/v1/products/:id', utility.check_subscription_expire, product.find);
    app.patch('/api/v1/products/:id', utility.check_subscription_expire, product.update);
    app.post('/api/v1/products_all', utility.check_subscription_expire, product.update_all);
    app.put('/api/v1/products/:id', utility.check_subscription_expire, product.update);
    app.delete('/api/v1/products/:id', utility.check_subscription_expire, product.destroy);
    app.get('/api/v1/products_stats', utility.check_subscription_expire, product.stats);
    app.get('/api/v1/prodotti_venduti', utility.check_subscription_expire, product.product_sales_charts);

    /**
     * Project
     */
    var project = require('../application/controllers/project');
    app.get('/api/v1/projects', utility.check_subscription_expire, project.index);
    app.post('/api/v1/projects', utility.check_subscription_expire, project.create);
    app.get('/api/v1/projects/:id', utility.check_subscription_expire, project.find);
    app.patch('/api/v1/projects/:id', utility.check_subscription_expire, project.update);
    app.post('/api/v1/projects_all', utility.check_subscription_expire, project.update_all);
    app.get('/api/v1/projects_csv', utility.check_subscription_expire, project.export_data);
    app.put('/api/v1/projects/:id', utility.check_subscription_expire, project.update);
    app.delete('/api/v1/projects/:id', utility.check_subscription_expire, project.destroy);
    app.get('/api/v1/projects_stats', utility.check_subscription_expire, project.stats);
    app.post('/api/v1/free_project', utility.check_subscription_expire, project.sblocca_progetto);

    /**
     * Totolist
     */
    var todolist = require('../application/controllers/todolist');
    app.get('/api/v1/todolists/:id', utility.check_subscription_expire, todolist.find);
    //app.get('/api/v1/todolists', todolist.index);
    app.post('/api/v1/todolists', utility.check_subscription_expire, todolist.create);
    //app.get('/api/v1/todolists/:id', todolist.find);
    app.patch('/api/v1/todolists/:id', utility.check_subscription_expire, todolist.update);
    app.put('/api/v1/todolists/:id', utility.check_subscription_expire, todolist.update);
    app.delete('/api/v1/todolists/:id', utility.check_subscription_expire, todolist.destroy);

    var todo = require('../application/controllers/todo');
    app.get('/api/v1/todos/:id', utility.check_subscription_expire, todo.find);
    app.post('/api/v1/todos', utility.check_subscription_expire, todo.create);
    app.patch('/api/v1/todos/:id', utility.check_subscription_expire, todo.update);
    app.delete('/api/v1/todos/:id', utility.check_subscription_expire, todo.destroy);

    /**
     * User
     */
    app.get('/api/v1/userinfo', utility.check_subscription_expire, user.info);
    app.post('/api/v1/users/invitation', utility.check_subscription_expire, user.invitation);
    app.post('/api/v1/password/:id', utility.check_subscription_expire, user.modify_password);
    app.get('/api/v1/reminders_data', utility.check_subscription_expire, user.reminder_data);
    app.get('/api/v1/domain_mail', utility.check_subscription_expire, user.domain_mail);

    /**
     * Organization
     */

    app.get('/api/v1/organizations', utility.check_subscription_expire, organization.index);
    app.patch('/api/v1/organizations/:id', utility.check_subscription_expire, organization.update);
    app.put('/api/v1/organizations/:id', utility.check_subscription_expire, organization.update);
    app.get('/api/v1/organizations/:id', utility.check_subscription_expire, organization.find);
    app.post('/api/v1/organizations_consensi', utility.check_subscription_expire, organization.insert_organization_consensi);
    app.delete('/api/v1/delete_privacy/:id', utility.check_subscription_expire, organization.delete_organization_consensi);
    app.post('/api/v1/gdpr2018', utility.check_subscription_expire, organization.update_gdpr);
    app.get('/api/v1/create_backup', utility.check_subscription_expire, organization.create_backup);
    app.get('/api/v1/backups', utility.check_subscription_expire, organization.index_backup);
    app.delete('/api/v1/delete_backup/:id', utility.check_subscription_expire, organization.delete_backup);
    app.post('/api/v1/restore_backup', organization.restore_backup);
    app.get('/api/v1/dashboard', utility.check_subscription_expire, organization.dashboard);
    app.get('/api/v1/onboarding-checklist', utility.check_subscription_expire, organization.onboarding_checklist);
    app.get('/api/v1/clinic/crea-dati-demo', utility.check_subscription_expire, organization.clinic_crea_dati_demo);
    app.get('/api/v1/cron_personalizzato', utility.check_subscription_expire, organization.cron_personalizzato);

    app.get('/api/v1/get_code_association', utility.check_subscription_expire, organization.get_code_association);
    app.get('/api/v1/beebeesign_dashboard', utility.check_subscription_expire, organization.beebeesign_dashboard);
    app.get('/api/v1/get_certificate', utility.check_subscription_expire, organization.get_certificate);
    app.post('/api/v1/license_device', utility.check_subscription_expire, organization.license_device);
    app.delete('/api/v1/device/:id', utility.check_subscription_expire, organization.delete_device);
    app.post('/api/v1/device_identifier', utility.check_subscription_expire, organization.update_identifier_device);
    app.post('/api/v1/beebeesign_errors', utility.check_subscription_expire, organization.beebeesign_errors);
    app.post('/api/v1/update_option_field', utility.check_subscription_expire, organization.update_option_field);


    /* LIVI CONNECT */
    app.post('/api/v1/livi_connect', utility.check_subscription_expire, organization.livi_connect);
    app.post('/api/v1/livi_disconnect', utility.check_subscription_expire, organization.livi_disconnect);

    /* PHYSITRACK CONNECT */
    
    app.post('/api/v1/physitrack_connect', utility.check_subscription_expire, physitrack.physitrack_connect);
    app.post('/api/v1/physitrack_disconnect', utility.check_subscription_expire, physitrack.physitrack_disconnect);
    app.get('/api/v1/physitrack_patient', utility.check_subscription_expire, physitrack.physitrack_patient);
    

    /**
     * Organization_custom_field
     */
    var organization_custom_field = require('../application/controllers/organization_custom_field');
    app.get('/api/v1/organization_custom_fields', utility.check_subscription_expire, organization_custom_field.index);
    app.patch('/api/v1/organization_custom_fields/:id', utility.check_subscription_expire, organization_custom_field.update);
    app.delete('/api/v1/organization_custom_fields/:id', utility.check_subscription_expire, organization_custom_field.destroy);
    app.get('/api/v1/organization_custom_fields/:id', utility.check_subscription_expire, organization_custom_field.find);
    app.post('/api/v1/organization_custom_fields', utility.check_subscription_expire, organization_custom_field.create);

    /**
     * Client
     */
    var client = require('../application/controllers/client');
    app.get('/api/v1/clients', client.index);
    app.get('/api/v1/clients', client.find);
    app.post('/api/v1/clients', client.create);
    app.put('/api/v1/clients/:id', client.update);
    app.delete('/api/v1/clients/:id', client.destroy);

    /**
     * Workhour
     */
    var workhour = require('../application/controllers/workhour');
    app.get('/api/v1/workhours', utility.check_subscription_expire, workhour.index);
    app.get('/api/v1/workhours/:id', utility.check_subscription_expire, workhour.find);
    app.get('/api/v1/workhours_csv', utility.check_subscription_expire, workhour.export_data);
    app.post('/api/v1/workhours', utility.check_subscription_expire, workhour.create);
    app.patch('/api/v1/workhours/:id', utility.check_subscription_expire, workhour.update);
    app.post('/api/v1/workhours_all', utility.check_subscription_expire, workhour.update_all);
    app.delete('/api/v1/workhours/:id', utility.check_subscription_expire, workhour.destroy);

    /**
     * Plan
     */
    var plan = require('../application/controllers/plan');
    app.get('/api/v1/plans', plan.index);
    app.get('/api/v1/plans/:id', plan.find);
    app.get('/api/v1/additional_packages', plan.additional_packages);
    app.post('/api/v1/additional_packages', plan.buy_additional_packages);

    /**
     * Subscription
     */
    var subscription = require('../application/controllers/subscription');
    app.patch('/api/v1/subscriptions/:id', subscription.update);
    app.post('/api/v1/subscriptions/get_price', subscription.get_price);
    app.get('/api/v1/get_bill_info', subscription.poppixsrl_bill_info);
    app.post('/api/v1/get_bill_info', subscription.save_poppixsrl_bill_info);
    app.get('/api/v1/scadenze', subscription.scadenze);

    /**
     * Log
     */
    app.get('/api/v1/logs/contacts/:id', utility.check_subscription_expire, log.contacts);
    app.get('/api/v1/logs/products/:id', utility.check_subscription_expire, log.products);
    app.get('/api/v1/logs/projects/:id', utility.check_subscription_expire, log.projects);
    app.get('/api/v1/logs/events/:id', utility.check_subscription_expire, log.events);
    app.get('/api/v1/logs/workhours/:id', utility.check_subscription_expire, log.workhours);
    app.get('/api/v1/logs/invoices/:id', utility.check_subscription_expire, log.invoices);
    app.get('/api/v1/logs/expenses/:id', utility.check_subscription_expire, log.expenses);
    app.get('/api/v1/logs/payments/:id', utility.check_subscription_expire, log.payments);
    app.get('/api/v1/logs/todolists/:id', utility.check_subscription_expire, log.todolists);
    app.get('/api/v1/logs/users/:id', utility.check_subscription_expire, log.users);
    app.get('/api/v1/logs/logins', utility.check_subscription_expire, log.logins);
    app.post('/api/v1/log', utility.check_subscription_expire, log.save_log);

    /**
     * Notification
     */
    var notification = require('../application/controllers/notification');
    app.get('/api/v1/notifications', notification.index);
    app.get('/api/v1/notifications/:id', notification.find);
    app.put('/api/v1/notifications/:id', notification.update);
    app.delete('/api/v1/notifications/:id', notification.destroy);

    /**
     * WaitingList
     */
    var waitinglist = require('../application/controllers/waitinglist');
    app.get('/api/v1/waitinglist', utility.check_subscription_expire, waitinglist.index);
    app.post('/api/v1/waitinglist', utility.check_subscription_expire, waitinglist.insert_waitinglist);
    app.delete('/api/v1/waitinglist/:id', utility.check_subscription_expire, waitinglist.delete_waitinglist);
    app.patch('/api/v1/waitinglist/:id', utility.check_subscription_expire, waitinglist.update_waitinglist);

    /**
     *  Upload files
     */
    var file = require('../application/controllers/file');
    app.post('/api/v1/app/file', utility.check_subscription_expire, upload.any(), file.insert);
    app.post('/api/v1/app/file_url', utility.check_subscription_expire, file.insert_from_url);
    app.get('/api/v1/app/file', utility.check_subscription_expire, file.read);
    app.get('/api/v1/files', utility.check_subscription_expire, file.index);
    app.delete('/api/v1/app/file', utility.check_subscription_expire, file.destroy);
    app.patch('/api/v1/files/:id', utility.check_subscription_expire, file.update);
    app.get('/api/v1/files/:id', utility.check_subscription_expire, file.find);
    app.get('/api/v1/app/app', file.app);
    app.post('/api/v1/zip', utility.check_subscription_expire, file.zip_files);
    app.post('/api/v1/app/change_permission_file', utility.check_subscription_expire, file.change_permission_file);
    app.get('/api/v1/signed_files', utility.check_subscription_expire, file.signed_files);   
    app.get('/api/v1/signed_files_platform', utility.check_subscription_expire, file.signed_files_platform);   
    app.post('/api/v1/request_sign_file', utility.check_subscription_expire, file.request_sign_file);
    app.post('/api/v1/files/signed', utility.check_subscription_expire, file.insert_signed_file);
    app.post('/api/v1/document_sign/:id', utility.check_subscription_expire, file.del_document_sign);

    /**
     *  Mandrill Mail
     */

    app.get('/api/v1/mandrill', utility.check_subscription_expire, mandrill.index);
    app.get('/api/v1/mandrill_by_id', utility.check_subscription_expire, mandrill.search_by_id);
    app.post('/api/v1/invoice_mandrill', utility.check_subscription_expire, mandrill.invoice_mandrill);
    app.post('/api/v1/project_mandrill', utility.check_subscription_expire, mandrill.project_mandrill);

    /**
    APPLICAZIONI 
   */
    app.post('/api/v1/sms', utility.check_subscription_expire, skebby.send_sms);
    app.post('/api/v1/mail', utility.check_subscription_expire, skebby.send_mail);
    app.post('/api/v1/push', utility.check_subscription_expire, skebby.send_push_notification);
    app.get('/api/v1/sms', utility.check_subscription_expire, skebby.skebby_get_sms);
    var trello = require('../application/controllers/trello');
    app.get('/api/v1/app/trello/boards', utility.check_subscription_expire, trello.boards);
    app.get('/api/v1/app/trello/boards/:id', utility.check_subscription_expire, trello.boards_id);

    var dropbox = require('../application/controllers/dropbox');
    app.get('/api/v1/app/dropbox/read', utility.check_subscription_expire, dropbox.read);

    /** 
       VDATRAILERS
     */
    var trailers = require('../application/controllers/vdatrailers/vdatrailers');
    app.get('/api/v1/check_gare_attive', utility.check_subscription_expire, trailers.check_gare_attive);
    app.get('/api/v1/sync_runners', utility.check_subscription_expire, trailers.sync_runners);
    app.get('/api/v1/elenco_iscritti_gara', utility.check_subscription_expire, trailers.elenco_iscritti_gara);
    app.get('/api/v1/classifica_gara', utility.check_subscription_expire, trailers.classifica_gara);
    app.get('/api/v1/classifica_lite', utility.check_subscription_expire, trailers.classifica_lite);
    app.post('/api/v1/crono_insert', utility.check_subscription_expire, trailers.save_crono);
    app.post('/api/v1/consegna_pettorale', utility.check_subscription_expire, trailers.consegna_pettorale);
    app.get('/api/v1/check_gara_import_time', utility.check_subscription_expire, trailers.check_gara_import_time);
    app.get('/api/v1/medtrack_trattamenti', utility.check_subscription_expire, trailers.medtrack_get_trattamenti);
    app.post('/api/v1/zappa_runners', utility.check_subscription_expire, trailers.zappa_runners);
    app.post('/api/v1/zappa_tempi', utility.check_subscription_expire, trailers.zappa_tempi);
    /** 
      4W4W
    */
    var wwww = require('../application/controllers/wwww/wwww');
    app.get('/api/v1/wwww_doc_link', utility.check_subscription_expire, wwww.find_from_link);
    app.get('/api/v1/wwww_find_project', utility.check_subscription_expire, wwww.find_project);
    app.get('/api/v1/wwww_find_product', utility.check_subscription_expire, wwww.find_product);

    /** 
      ALBOLIVE
    */
    var albolive = require('../application/controllers/albolive/albolive');
    app.post('/api/v1/albo', utility.check_subscription_expire, albolive.find_atto);
    app.post('/api/v1/albolive_evento', utility.check_subscription_expire, albolive.albolive_salva_testo);
    app.get('/api/v1/albolive_find_atti', utility.check_subscription_expire, albolive.index_atti);
    app.get('/api/v1/albolive_enti', utility.check_subscription_expire, albolive.index_enti);
    app.get('/api/v1/albolive_dashboard', utility.check_subscription_expire, albolive.dashboard);

    /** 
      ASTALDI
    */
    var astaldi = require('../application/controllers/astaldi/astaldi');
    app.get('/api/v1/astaldi_gantt', utility.check_subscription_expire, astaldi.gantt);
    /**
     */

     /**
        BEEBEEHEALTH
    */
    var slots = require('../application/controllers/clinical/calendar');
    app.get('/api/v1/clinic_slot_calendar', utility.check_subscription_expire, slots.beebeehealth_events_slot_calendar);
   
    var patient = require('../application/controllers/clinical/patient');
    app.get('/api/v1/patient/:id', utility.check_subscription_expire, patient.get_patient_info);
    app.get('/api/v1/client_debts', utility.check_subscription_expire, patient.get_patient_debiti_crediti);
    app.get('/api/v1/client_debts_csv', utility.check_subscription_expire, patient.get_patient_debiti_crediti_csv);
    app.get('/api/v1/report_sex', utility.check_subscription_expire, patient.sesso_chart);
    app.get('/api/v1/report_corpo', utility.check_subscription_expire, patient.corpo_chart);
    app.get('/api/v1/report_sintomi', utility.check_subscription_expire, patient.sintomi_chart);
    app.get('/api/v1/clinic_conto', utility.check_subscription_expire, patient.clinic_conto);
    app.get('/api/v1/clinic_conto_csv', utility.check_subscription_expire, patient.clinic_conto_csv);
    app.get('/api/v1/clinic_stats', utility.check_subscription_expire, patient.clinic_stats);
    app.post('/api/v1/patch_patient_from_php', utility.check_subscription_expire, patient.patch_patient_from_php); 
    app.post('/api/v1/clinic_storico', utility.check_subscription_expire, patient.clinic_storico);
    app.get('/api/v1/clinic_files', utility.check_subscription_expire, patient.clinic_files);
    app.post('/api/v1/copy_clinic', utility.check_subscription_expire, patient.copy_clinic);
    app.get('/api/v1/consensi', utility.check_subscription_expire, patient.get_consensi);
    app.get('/api/v1/pacchetti', utility.check_subscription_expire, patient.get_pacchetti);
    app.post('/api/v1/save_consensi', utility.check_subscription_expire, patient.save_consensi);
    app.post('/api/v1/clinic_report_save_draft', utility.check_subscription_expire, patient.clinic_report_save_draft);

    
    app.get('/api/v1/clinic_report_miss', utility.check_subscription_expire, report_clinic.clinic_report_miss);
    app.get('/api/v1/clinic_report_appuntamenti', utility.check_subscription_expire, report_clinic.clinic_report_appuntamenti);
    app.get('/api/v1/clinic_report_scadenze', utility.check_subscription_expire, report_clinic.clinic_report_scadenze);
    app.get('/api/v1/clinic_report_specialist', utility.check_subscription_expire, report_clinic.clinic_report_specialist);
    app.get('/api/v1/clinic_sign_project', utility.check_subscription_expire, report_clinic.clinic_sign_project);
    app.get('/api/v1/copy_contact', utility.check_subscription_expire, report_clinic.copy_contact);
    app.post('/api/v1/paste_contact', utility.check_subscription_expire, report_clinic.paste_contact);
    app.get('/api/v1/clinic_chart_appuntamenti', utility.check_subscription_expire, report_clinic.clinic_chart_appuntamenti);
    app.get('/api/v1/clinic_chart_trattamenti', utility.check_subscription_expire, report_clinic.clinic_chart_trattamenti);
    app.get('/api/v1/clinic_chart_occupazione', utility.check_subscription_expire, report_clinic.clinic_chart_occupazione);
    app.get('/api/v1/clinic_report_pazienti', utility.check_subscription_expire, report_clinic.clinic_report_pazienti);
    app.get('/api/v1/clinic_report_pazienti_export', utility.check_subscription_expire, report_clinic.clinic_report_pazienti_export);
    app.get('/api/v1/clinic_report_pazienti_export_pdf', utility.check_subscription_expire, report_clinic.clinic_report_pazienti_export_pdf);
    app.get('/api/v1/clinic_report_specialisti_export', utility.check_subscription_expire, report_clinic.clinic_report_specialisti_export);
    app.get('/api/v1/clinic_chart_sesso', utility.check_subscription_expire, report_clinic.clinic_chart_sesso);
    app.get('/api/v1/clinic_chart_eta', utility.check_subscription_expire, report_clinic.clinic_chart_eta);
    app.get('/api/v1/clinic_chart_all_appuntamenti', utility.check_subscription_expire, report_clinic.clinic_chart_all_appuntamenti);
    app.get('/api/v1/clinic_chart_valutazioni', utility.check_subscription_expire, report_clinic.clinic_valutazioni_chart);
    app.get('/api/v1/clinic_report_document', utility.check_subscription_expire, report_clinic.clinic_report_documenti);


    /** 
      SCUOLE DI SCI
     */

    var scuoledisci_chart = require('../application/controllers/scuolasci/charts');
    app.get('/api/v1/scuolasci_ore_charts', utility.check_subscription_expire, scuoledisci_chart.scuolasci_ore_charts);
    app.get('/api/v1/scuolasci_level_charts', utility.check_subscription_expire, scuoledisci_chart.scuolasci_level_charts);
    app.get('/api/v1/scuolasci_age_charts', utility.check_subscription_expire, scuoledisci_chart.scuolasci_age_charts);
    
    app.get('/api/v1/scuolasci_lingue_charts', utility.check_subscription_expire, scuoledisci_chart.scuolasci_lingue_charts);
    app.get('/api/v1/scuolasci_persone_charts', utility.check_subscription_expire, scuoledisci_chart.scuolasci_persone_charts);
    var scuoledisci = require('../application/controllers/scuolasci/turno');
    app.post('/api/v1/stampa_tagliando', utility.check_subscription_expire, scuoledisci.stampa_tagliando);
    app.get('/api/v1/tagliandi', utility.check_subscription_expire, scuoledisci.tagliandi);
    app.get('/api/v1/penality', utility.check_subscription_expire, scuoledisci.penality);
    app.get('/api/v1/promemoria_private', utility.check_subscription_expire, scuoledisci.promemoria_private);
    app.post('/api/v1/turno', utility.check_subscription_expire, scuoledisci.get_turno);
    app.patch('/api/v1/turno/:id', utility.check_subscription_expire, scuoledisci.update);
    app.get('/api/v1/cerca_maestro', utility.check_subscription_expire, scuoledisci.cerca_maestro);
    app.post('/api/v1/dispo/modifica', utility.check_subscription_expire, scuoledisci.dispo_sanitize);
    app.post('/api/v1/dispo/modifica_bulk', utility.check_subscription_expire, scuoledisci.modifica_bulk);
    app.post('/api/v1/scuole_sci_cancella_dispo', utility.check_subscription_expire, scuoledisci.scuole_sci_cancella_dispo);
    app.post('/api/v1/scuole_sci_cancella_clienti', utility.check_subscription_expire, scuoledisci.scuole_sci_cancella_clienti);
    app.post('/api/v1/merge_prenotazione', utility.check_subscription_expire, scuoledisci.merge_prenotazione);
    app.post('/api/v1/prenotazione', utility.check_subscription_expire, scuoledisci.prenotazione);
    app.get('/api/v1/preno_products', utility.check_subscription_expire, scuoledisci.preno_products);
    app.post('/api/v1/insert_bulk_family', utility.check_subscription_expire, scuoledisci.insert_bulk_family);
    app.post('/api/v1/conferma_prenotazione', utility.check_subscription_expire, scuoledisci.conferma_prenotazione);
    app.post('/api/v1/prenotazione_gruppo', utility.check_subscription_expire, scuoledisci.prenotazione_gruppo);
    app.get('/api/v1/prenotazione/:id', utility.check_subscription_expire, scuoledisci.get_prenotazione);
    app.get('/api/v1/get_giorni_collettiva/:id', utility.check_subscription_expire, scuoledisci.get_giorni_collettiva);
    app.get('/api/v1/get_orario_collettiva/:id', utility.check_subscription_expire, scuoledisci.get_orario_collettiva);
    app.post('/api/v1/situazione_maestri', utility.check_subscription_expire, scuoledisci.situazione_maestri);
    app.get('/api/v1/ore_maestro', utility.check_subscription_expire, scuoledisci.ore_maestro);
    app.post('/api/v1/cambia_maestro', utility.check_subscription_expire, scuoledisci.cambia_maestro);
    app.post('/api/v1/sposta_evento', utility.check_subscription_expire, scuoledisci.sposta_evento);
    app.get('/api/v1/scuolasci_events_calendar', utility.check_subscription_expire, scuoledisci.events_calendar);
    app.post('/api/v1/copia_lezione', utility.check_subscription_expire, scuoledisci.copia_evento);
    app.get('/api/v1/parametri_turno', utility.check_subscription_expire, scuoledisci.parametri_turno);
    app.post('/api/v1/parametri_turno', utility.check_subscription_expire, scuoledisci.modifica_parametri_turno);
    app.post('/api/v1/bonus_malus', utility.check_subscription_expire, scuoledisci.bonus_malus);
    app.post('/api/v1/scuolasci_parameter', utility.check_subscription_expire, scuoledisci.parameter);
    app.post('/api/v1/scuolasci_calcolatore', utility.check_subscription_expire, ecommerce.calcolatore_prezzo);
    app.post('/api/v1/prenotazione_modifica_turno', utility.check_subscription_expire, scuoledisci.prenotazione_modifica_turno);


    var collettive = require('../application/controllers/scuolasci/collettive');
    app.post('/api/v1/collettive', utility.check_subscription_expire, collettive.get_maestro_collettive);
    app.post('/api/v1/collettive/presenza', utility.check_subscription_expire, collettive.collettive_insert_day);
    app.post('/api/v1/collettive/conferma_presenza', utility.check_subscription_expire, collettive.collettive_presenza_effettiva);
    app.post('/api/v1/collettive/delete', utility.check_subscription_expire, collettive.disassocia_maestro_collettive);
    app.post('/api/v1/collettive/associa', utility.check_subscription_expire, collettive.associa_maestro_bambino);
    app.post('/api/v1/collettive/disassocia', utility.check_subscription_expire, collettive.disassocia_maestro_bambino);
    app.post('/api/v1/collettive/all', utility.check_subscription_expire, collettive.find_collettiva);
    app.get('/api/v1/collettive/csv', utility.check_subscription_expire, collettive.csv_collettiva);
    app.post('/api/v1/collettive/mail', utility.check_subscription_expire, collettive.mail_collettiva);
    app.post('/api/v1/collettive/find_maestri_product', utility.check_subscription_expire, collettive.find_maestri_per_product_period);
    app.get('/api/v1/scuolasci_date_collettiva', utility.check_subscription_expire, collettive.scuolasci_date_collettiva);
    app.get('/api/v1/scuolasci_get_info_collettiva', utility.check_subscription_expire, collettive.scuolasci_get_info_collettiva);
    app.post('/api/v1/collettive/prenotazione', utility.check_subscription_expire, collettive.prenotazione_collettiva);
    app.post('/api/v1/collettive/colore_maestro', utility.check_subscription_expire, collettive.set_color_maestro);
    app.post('/api/v1/add_lista_attesa', utility.check_subscription_expire, collettive.add_lista_attesa);
    app.post('/api/v1/voucher_confirm', utility.check_subscription_expire, collettive.voucher_confirm);
    app.get('/api/v1/check_voucher', utility.check_subscription_expire, collettive.check_voucher);
    app.post('/api/v1/use_voucher', utility.check_subscription_expire, collettive.use_voucher);
    app.delete('/api/v1/voucher/:id', utility.check_subscription_expire, collettive.delete_voucher);
    app.post('/api/v1/create_voucher', utility.check_subscription_expire, collettive.create_voucher);
    app.get('/api/v1/get_corsi_attivi', utility.check_subscription_expire, collettive.get_corsi_attivi);
    app.post('/api/v1/scuolasci_maestro_sposta', utility.check_subscription_expire, collettive.scuolasci_maestro_sposta);
    
    var maestri = require('../application/controllers/scuolasci/maestri');
    app.get('/api/v1/maestro', utility.check_subscription_expire, maestri.stats_maestri_workhours);
    app.get('/api/v1/spunte_mancanti', utility.check_subscription_expire, maestri.spunte_mancanti);
    app.get('/api/v1/find_maestri', utility.check_subscription_expire, maestri.find_maestri);
    app.get('/api/v1/bonifici', utility.check_subscription_expire, maestri.excel_bonifici);
    app.get('/api/v1/consultazione_ranking', utility.check_subscription_expire, maestri.consultazione_ranking);
    app.get('/api/v1/collettiva_csv', utility.check_subscription_expire, maestri.corsi_annuali_csv);
    app.post('/api/v1/scuola_spunta_ore', utility.check_subscription_expire, maestri.spunta_ore);
    app.post('/api/v1/scuole_sci_spunta_bulk', utility.check_subscription_expire, maestri.spunta_ore_bulk);
    app.get('/api/v1/maestri_giorno', utility.check_subscription_expire, maestri.maestri_giorno);
    app.get('/api/v1/maestro_report', utility.check_subscription_expire, maestri.maestro_report);
    app.post('/api/v1/ingresso_maestro', utility.check_subscription_expire, maestri.ingresso_maestro);
    app.post('/api/v1/uscita_maestro', utility.check_subscription_expire, maestri.uscita_maestro);
    app.post('/api/v1/ultimo_uscito', utility.check_subscription_expire, maestri.ultimo_uscito);
    app.post('/api/v1/assegna_penality', utility.check_subscription_expire, maestri.assegna_penality);
    app.get('/api/v1/timbratore/:id', utility.check_subscription_expire, maestri.timbratore);
    app.get('/api/v1/simple_dashboard', utility.check_subscription_expire, maestri.simple_dashboard);
    app.post('/api/v1/fatt_riequilibrio', utility.check_subscription_expire, maestri.fatt_riequilibrio);
    app.post('/api/v1/lezione_barcode', utility.check_subscription_expire, maestri.lezione_barcode);
    app.post('/api/v1/togli_lezione_barcode', utility.check_subscription_expire, maestri.togli_lezione_barcode);
    app.post('/api/v1/associa_barcode', utility.check_subscription_expire, maestri.associa_barcode);
    app.post('/api/v1/scuolasci_update_level', utility.check_subscription_expire, maestri.change_level);
    app.post('/api/v1/scuolasci_ore_da_confermare', utility.check_subscription_expire, maestri.ore_da_confermare);
    app.post('/api/v1/dispo_modificabile_da_maestro', utility.check_subscription_expire, maestri.dispo_modificabile_da_maestro);
    
    var corsi = require('../application/controllers/scuolasci/corsi');
    app.post('/api/v1/corsi_annuali', utility.check_subscription_expire, corsi.get_maestro_corsi);
    app.post('/api/v1/corsi_annuali/delete', utility.check_subscription_expire, corsi.disassocia_maestro_corsi);
    app.post('/api/v1/corsi_annuali/associa', utility.check_subscription_expire, corsi.associa_maestro_bambino_corso_annuale);
    app.post('/api/v1/corsi_annuali/disassocia', utility.check_subscription_expire, corsi.disassocia_maestro_bambino_corso_annuale);
    app.post('/api/v1/corsi_annuali/all', utility.check_subscription_expire, corsi.find_corso);
    app.post('/api/v1/corsi_annuali/mail', utility.check_subscription_expire, corsi.mail_corso);
    var pacchetti = require('../application/controllers/scuolasci/pacchetti');
    app.post('/api/v1/pacchetti/associa', utility.check_subscription_expire, pacchetti.associa_maestro_prenotazione);
    app.post('/api/v1/aggiungi_pacchetto', utility.check_subscription_expire, pacchetti.aggiungi_ore_a_pacchetto);
    var stampe = require('../application/controllers/scuolasci/stampe');
    app.get('/api/v1/registro_onorari', utility.check_subscription_expire, stampe.registro_onorari);
    app.get('/api/v1/incassi_prodotto', utility.check_subscription_expire, stampe.incassi_prodotto);
    app.get('/api/v1/statistiche_maestri', utility.check_subscription_expire, stampe.statistiche_maestri);
    
    app.get('/api/v1/ore_prodotto', utility.check_subscription_expire, stampe.ore_prodotto);
    app.get('/api/v1/vendite_prodotto', utility.check_subscription_expire, stampe.vendite_prodotto);
    app.get('/api/v1/conto_economico', utility.check_subscription_expire, stampe.economic_situation);
    app.get('/api/v1/conto_lezioni', utility.check_subscription_expire, stampe.events_not_payed);
    app.get('/api/v1/conto_lezioni_export', utility.check_subscription_expire, stampe.events_not_payed_export);
    app.get('/api/v1/conto_stagione', utility.check_subscription_expire, stampe.conto_stagione);
    app.get('/api/v1/conto_stagione_csv', utility.check_subscription_expire, stampe.conto_stagione_csv);
    app.get('/api/v1/cassa_method_date', utility.check_subscription_expire, stampe.cassa_method_day);
    app.get('/api/v1/assicurazione', utility.check_subscription_expire, stampe.assicurazione);
    app.get('/api/v1/assicurazione_incassi', utility.check_subscription_expire, stampe.assicurazione_incassi);
    app.get('/api/v1/maestri_bloccati', utility.check_subscription_expire, stampe.maestri_bloccati);
    app.post('/api/v1/gara_collettive', utility.check_subscription_expire, stampe.calendario_gara_collettiva);
    app.get('/api/v1/ore_sci_club', utility.check_subscription_expire, stampe.sci_club);
    app.post('/api/v1/collettiva_stat', utility.check_subscription_expire, stampe.collettiva_stat);
    app.get('/api/v1/ore_prenotate_da_maestri', utility.check_subscription_expire, stampe.ore_prenotate_da_maestri);
    app.get('/api/v1/cervino_cassa', utility.check_subscription_expire, stampe.cervino_cassa);
    app.post('/api/v1/scuolasci_update_group_sms', utility.check_subscription_expire, stampe.update_group_sms);

    var payment_scuola = require('../application/controllers/scuolasci/payment_scuola');
    /*app.post('/api/v1/pay_events', payment_scuola.payment_event_people);
    app.post('/api/v1/pay_import', payment_scuola.payment_import_for_event);*/
    app.post('/api/v1/event_split', utility.check_subscription_expire, payment_scuola.event_split_contact);
    app.get('/api/v1/load_split/:id', utility.check_subscription_expire, payment_scuola.load_split);
    app.post('/api/v1/sanitize_payment', utility.check_subscription_expire, payment_scuola.payment_project);
    app.post('/api/v1/sanitize_payment_ecommerce', utility.check_subscription_expire, payment_scuola.payment_project_ecommmerce);
    app.post('/api/v1/assegna_residuo', utility.check_subscription_expire, payment_scuola.assegna_residuo_pagamento);
    app.get('/api/v1/elimina_pagamento/:id', utility.check_subscription_expire, payment_scuola.destroy_payment_event);
    app.get('/api/v1/elimina_evento/:id', utility.check_subscription_expire, payment_scuola.destroy_event);
    app.get('/api/v1/elimina_prenotazione/:id', utility.check_subscription_expire, payment_scuola.destroy_project);
    app.post('/api/v1/scollega_pagamento', utility.check_subscription_expire, payment_scuola.destroy_payment_project);
    app.get('/api/v1/salda_residui/:id', utility.check_subscription_expire, payment_scuola.salda_residui_contact);
    app.post('/api/v1/trunk_event', utility.check_subscription_expire, payment_scuola.spezza_eventi);
    app.post('/api/v1/trunk_prenotazione', utility.check_subscription_expire, payment_scuola.spezza_prenotazione);
    app.post('/api/v1/sanitize_event', utility.check_subscription_expire, payment_scuola.sanitize_event);
    app.post('/api/v1/update_payment/:id', utility.check_subscription_expire, payment_scuola.update_payment);
    app.post('/api/v1/update_project/:id', utility.check_subscription_expire, payment_scuola.update_project);
    app.post('/api/v1/elimina_da_calendario', utility.check_subscription_expire, payment_scuola.elimina_da_calendario);
    app.post('/api/v1/annulla_prenotazione', utility.check_subscription_expire, payment_scuola.annulla_prenotazione);
    app.get('/api/v1/fattura_da_ecommerce/:id',utility.check_subscription_expire,payment_scuola.crea_fattura_da_payment_ecommerce);
    app.get('/api/v1/sanitize_conto_cliente/:id', utility.check_subscription_expire, payment_scuola.sanitize_conto_cliente);
    
    app.get('/api/v1/ecommerce_in_vendita', ecommerce.ecommerce_in_vendita);
    /**
     * ECOMMERCE SCUOLA DI SCI
     */
    app.get('/api/v1/next_events/:id', utility.check_subscription_expire, ecommerce.next_events);
    app.post('/api/v1/prenota_individuali', utility.check_subscription_expire, ecommerce.individuali);
    app.post('/api/v1/scuolasci_prezzo', ecommerce.calcola_prezzo_id);
    app.post('/api/v1/scuolasci_prezzo_fisso', ecommerce.calcola_prezzo_id_fisso);
    app.post('/api/v1/ecommerce_vetrina', ecommerce.ecommerce_vetrina);
    app.get('/api/v1/ecommerce_vetrina', ecommerce.get_ecommerce_vetrina);
    //app.post('/api/v1/scuolasci_prezzo_v1', ecommerce.calcola_prezzo_id_sertorelli);

    app.get('/api/v1/ecommerce_da_cancellare', utility.check_subscription_expire, ecommerce.ecommerce_cron_delete);
    app.post('/api/v1/change_dispo_ecommerce', utility.check_subscription_expire, ecommerce.change_dispo_ecommerce);
    app.get('/api/v1/list_dispo_ecommerce', utility.check_subscription_expire, ecommerce.list_dispo_ecommerce);
    
    app.post('/api/v1/ecommerce_confirm', utility.check_subscription_expire, ecommerce.ecommerce_checkout_confirm_new);
    app.post('/api/v1/scuolasci_ecommerce_confirm', utility.check_subscription_expire, ecommerce.ecommerce_checkout_confirm);
    
    //app.post('/api/v1/scuolasci_ecommerce_confirm_v1', utility.check_subscription_expire, ecommerce.ecommerce_checkout_confirm_sertorelli);
    
    app.get('/api/v1/ecommerce_sanitize_dispo/:id', utility.check_subscription_expire, ecommerce.sanitize_dispo_verify);
    app.post('/api/v1/ecommerce_bonifico', utility.check_subscription_expire, ecommerce.ecommerce_bonifico);
    app.post('/api/v1/ecommerce_in_loco', utility.check_subscription_expire, ecommerce.ecommerce_in_loco);
    app.post('/api/v1/ecommerce_zero', utility.check_subscription_expire, ecommerce.ecommerce_zero);
    app.post('/api/v1/ecommerce/set_dispo_pacchetto', utility.check_subscription_expire, ecommerce.set_dispo_pacchetto);
    app.post('/api/v1/booking_in_loco', utility.check_subscription_expire, event.booking_in_loco);
    app.post('/api/v1/booking_bonifico', utility.check_subscription_expire, event.booking_bonifico);
   

    /** 
      SCI CLUBS
     */

    app.get('/api/v1/scadenza_certificati', utility.check_subscription_expire, sciclub.find_scadenza_certificati);
    app.get('/api/v1/iscritti_sciclub', utility.check_subscription_expire, sciclub.iscritti_sciclub);
    app.get('/api/v1/iscritti_sciclub_csv', utility.check_subscription_expire, sciclub.iscritti_sciclub_csv);

    /** 
      GUIDE ALPINE
    */
    var guide_alpine = require('../application/controllers/guide/guide');
    app.get('/api/v1/prenotazioni_guide', utility.check_subscription_expire, guide_alpine.prenotazioni_guide);
    app.get('/api/v1/dispo_guide', utility.check_subscription_expire, guide_alpine.dispo_guide);
    app.get('/api/v1/dispo_gruppi', utility.check_subscription_expire, guide_alpine.dispo_gruppi);
    app.post('/api/v1/dispo_guida', utility.check_subscription_expire, guide_alpine.dispo_guida_per_gruppo);
    app.post('/api/v1/togli_dispo_guida', utility.check_subscription_expire, guide_alpine.togli_dispo_guida_per_gruppo);
    app.post('/api/v1/associa_guide', utility.check_subscription_expire, guide_alpine.associa_guide);
    app.post('/api/v1/associa_gruppi', utility.check_subscription_expire, guide_alpine.associa_gruppi);
    app.post('/api/v1/disassocia_gruppi', utility.check_subscription_expire, guide_alpine.disassocia_gruppi);
    app.get('/api/v1/guide_events_calendar', utility.check_subscription_expire, guide_alpine.guide_events_calendar);
    app.get('/api/v1/guide_resoconto', utility.check_subscription_expire, guide_alpine.guide_resoconto);
    app.post('/api/v1/guide_fatture', utility.check_subscription_expire, guide_alpine.guide_fatture);
    app.get('/api/v1/guide_assicurazione', utility.check_subscription_expire, guide_alpine.guide_assicurazione);
    app.post('/api/v1/guide_da_turno', utility.check_subscription_expire, guide_alpine.guide_da_turno);
    app.get('/api/v1/guide_last_turno', utility.check_subscription_expire, guide_alpine.guide_last_turno);
    app.get('/api/v1/guide_notifica_dispo', utility.check_subscription_expire, guide_alpine.send_dispo_notification);
    app.get('/api/v1/guide_notifica_assegna_gita', utility.check_subscription_expire, guide_alpine.guide_notifica_assegna_gita);
    app.post('/api/v1/guide_disdetta', utility.check_subscription_expire, guide_alpine.guide_disdetta_preno);

    /** 
      GUIDE ALPINE MONTE ROSA
    */
    
    app.get('/api/v1/guidemonterosa_prenotazione/:id', utility.check_subscription_expire, guide_monterosa.guidemonterosa_get_prenotazione);
    app.get('/api/v1/guidemonterosa_dashboard', utility.check_subscription_expire, guide_monterosa.guidemonterosa_dashboard);
    app.get('/api/v1/guidemonterosa_conto_economico', utility.check_subscription_expire, guide_monterosa.guidemonterosa_economic_situation);
    app.get('/api/v1/guidemonterosa_crea_fattura', utility.check_subscription_expire, guide_monterosa.guidemonterosa_crea_fattura);
    app.get('/api/v1/guidemonterosa_genera_fattura', utility.check_subscription_expire, guide_monterosa.guidemonterosa_genera_fattura);
    app.get('/api/v1/guidemonterosa_dashboard_export', utility.check_subscription_expire, guide_monterosa.guidemonterosa_dashboard_export);
    app.get('/api/v1/guidemonterosa_lettura_volo', utility.check_subscription_expire, guide_monterosa.guidemonterosa_lettura_volo);
    app.get('/api/v1/guidemonterosa_elenco_letture', utility.check_subscription_expire, guide_monterosa.guidemonterosa_elenco_letture_volo);
    app.post('/api/v1/guidemonterosa_prenota_volo', utility.check_subscription_expire, guide_monterosa.guidemonterosa_prenota_volo);
    app.post('/api/v1/guidemonterosa_aggiungi_volo_a_pratica', utility.check_subscription_expire, guide_monterosa.guidemonterosa_aggiungi_volo_a_pratica);
    app.get('/api/v1/guidemonterosa_report_servizi', utility.check_subscription_expire, guide_monterosa.guidemonterosa_report_servizi);
    app.get('/api/v1/guidemonterosa_report_pacchetti', utility.check_subscription_expire, guide_monterosa.guidemonterosa_report_pacchetti);
    app.post('/api/v1/guidemonterosa_preventivo', utility.check_subscription_expire, guide_monterosa.guidemonterosa_crea_preventivo);
    app.post('/api/v1/guidemonterosa_change_status_preventivo', utility.check_subscription_expire, guide_monterosa.guidemonterosa_stato_preventivo);
    app.post('/api/v1/guidemonterosa_crea_pratica_preventivo', utility.check_subscription_expire, guide_monterosa.guidemonterosa_crea_pratica_preventivo);
    app.post('/api/v1/guidemonterosa_add_preventivo_to_pratica', utility.check_subscription_expire, guide_monterosa.guidemonterosa_aggiungi_preventivi_pratica);
    app.get('/api/v1/guidemonterosa_dashboard_preventivi', utility.check_subscription_expire, guide_monterosa.guidemonterosa_dashboard_preventivi);   
    app.get('/api/v1/segreteria_use_voucher', utility.check_subscription_expire, guide_monterosa.segreteria_use_voucher);
    app.post('/api/v1/segreteria_update_voucher', utility.check_subscription_expire, guide_monterosa.segreteria_update_voucher);
    app.get('/api/v1/guidemonterosa_dashboard_voucher', utility.check_subscription_expire, guide_monterosa.guidemonterosa_dashboard_voucher);   
    app.post('/api/v1/guidemonterosa_send_request', utility.check_subscription_expire, guide_monterosa.guidemonterosa_send_compila_dati);
    app.post('/api/v1/guidemonterosa_create_session', utility.check_subscription_expire, guide_monterosa.guidemonterosa_create_session);
    
    /** 
      PEAKSHUNTER
    */
    app.get('/api/v1/peakshunter/attivita', utility.check_subscription_expire, peakshunter.peakshunter_attivita);
    app.get('/api/v1/peakshunter/scadenze', utility.check_subscription_expire, peakshunter.peakshunter_scadenze);
    app.post('/api/v1/peakshunter/copia_attivita_prodotto', utility.check_subscription_expire, peakshunter.copia_attivita_prodotto);
    app.get('/api/v1/peakshunter/rifugi', utility.check_subscription_expire, peakshunter.prenotazione_rifugi);
    app.get('/api/v1/scuolasci_events_calendar_ical', utility.check_subscription_expire, peakshunter.scuolasci_events_calendar_ical);
    app.get('/api/v1/peakshunter_timeline/:id', utility.check_subscription_expire, peakshunter.peakshunter_timeline);
    app.post('/api/v1/peakshunter_timeline_update', utility.check_subscription_expire, peakshunter.peakshunter_update_timeline);
    app.post('/api/v1/peakshunter_save_releaseform', utility.check_subscription_expire, peakshunter.peakshunter_save_releaseform);
    app.post('/api/v1/peakshunter_booking_step', utility.check_subscription_expire, peakshunter.peakshunter_booking_step);
    app.post('/api/v1/peakshunter_send_request', utility.check_subscription_expire, peakshunter.peakshunter_send_compila_dati);
    

    /**
     * STRIPE
     */
    //var stripe_file = require('../application/controllers/stripe');
    app.post('/api/v1/stripe/create', stripe_file.create_customer);
    app.get('/api/v1/stripe/get_info', stripe_file.get_customer_info);
    app.get('/api/v1/stripe/pay', stripe_file.you_must_pay);
    app.get('/api/v1/get_next_bill', stripe_file.get_amount);
    app.post('/api/v1/stripe/create_session', stripe_file.create_session);
    app.get('/api/v1/stripe/request_permission', stripe_file.request_permission);
    //app.post('/api/v1/stripe/create_charge', stripe_file.create_charge);


    /**
     * SUMUP
     */
    var sumup = require('../application/controllers/sumup');
    app.get('/api/v1/sumup/create_checkout', sumup.create_checkout);
    app.post('/api/v1/sumup/verify', sumup.verify);

    /**
     * SATISPAY
     */
    var satispay = require('../application/controllers/satispay');
    app.post('/api/v1/satispay/create_key', satispay.create_key_id);
    app.post('/api/v1/satispay/create_checkout', satispay.create_checkout);
    app.get('/api/v1/satispay/check_payment', satispay.check_payment);

    /**
     * PAYPAL
     */
    //var paypal_file = require('../application/controllers/paypal');
    //app.get('/api/v1/paypal/request_permission', paypal_file.request_permission);
    app.post('/api/v1/paypal/save_preference', paypal_file.save_preference);


    /**
     * POPPIX ACCESS
     */
    var poppix = require('../application/controllers/poppix');
    app.post('/api/v1/poppix/grant_access', poppix.grant_access);
    app.get('/api/v1/poppix/grant_access_list', poppix.grant_access_list);
    app.post('/api/v1/poppix/revoke_access', poppix.revoke_access);


    var poppixsrl = require('../application/controllers/poppix/poppix');
    app.get('/api/v1/dashboard_poppix', poppixsrl.dashboard_poppix);
    app.post('/api/v1/poppix/modify_organization', poppixsrl.modify_organization);
    app.post('/api/v1/poppix/update_organization_new', poppixsrl.update_organization_new);
    app.post('/api/v1/poppix/update_organization', poppixsrl.update_organization_single);
    app.post('/api/v1/poppix/upload_certificato_fea', poppixsrl.upload_certificato_fea);
    app.post('/api/v1/poppix/beebeeskischool', poppixsrl.create_school);
    app.get('/api/v1/poppix/todos', poppixsrl.todo_list);
    app.get('/api/v1/poppix/licenses_expire', poppixsrl.licenses_expire);
    app.post('/api/v1/poppix/beebeesign_licenses', poppixsrl.poppix_update_beebeesign_licenses);
    app.get('/api/v1/poppix/trust', poppixsrl.trust);
    app.get('/api/v1/poppix/beebeesign_errors', poppixsrl.poppix_beebeesign_errors);
    app.post('/api/v1/poppix/update_trust', poppixsrl.update_trust);
    app.post('/api/v1/poppix/set_new_trust', poppixsrl.set_new_trust);
    app.post('/api/v1/poppix/mail_activate_sdi', poppixsrl.trust_activation_mail);
    app.post('/api/v1/new_trust', poppixsrl.new_trust);
    app.post('/api/v1/poppix/urgence_token', poppixsrl.urgence_token);
    app.get('/api/v1/poppix/organization_users', poppixsrl.organization_users);
    app.get('/api/v1/our_invoices', poppixsrl.our_invoices);
    app.get('/api/v1/poppix/export_payment', poppixsrl.export_payment_contabilita);

    /**
     * MAISONVIVE 
     */
    var maisonvive = require('../application/controllers/maisonvive/maisonvive');
    app.get('/api/v1/maisonvive_contact/:id', utility.check_subscription_expire,maisonvive.maisonvive_contact);
    app.get('/api/v1/maisonvive_scadenze_passive', utility.check_subscription_expire,maisonvive.maisonvive_scadenze_passive);
    app.get('/api/v1/maisonvive_contract_time/:id', utility.check_subscription_expire,maisonvive.maisonvive_contract_time);
    app.get('/api/v1/maisonvive_dashboard', utility.check_subscription_expire,maisonvive.maisonvive_dashboard);
    app.get('/api/v1/maisonvive_contratti_aperti', utility.check_subscription_expire,maisonvive.maisonvive_contratti_aperti);
    app.get('/api/v1/maisonvive_in_consegna', utility.check_subscription_expire,maisonvive.maisonvive_in_consegna);
    app.get('/api/v1/maisonvive_stats', utility.check_subscription_expire,maisonvive.maisonvive_stats);
    app.get('/api/v1/maisonvive_culletta', utility.check_subscription_expire,maisonvive.maisonvive_culletta);
    app.get('/api/v1/maisonvive_slot_calendario_consegne', utility.check_subscription_expire,maisonvive.maisonvive_slot_calendario_consegne);
    app.get('/api/v1/maisonvive_finalizza_contratto/:id',utility.check_subscription_expire, maisonvive.maisonvive_finalizza_contratto);
    app.get('/api/v1/maisonvive_associa_contratti',utility.check_subscription_expire, maisonvive.maisonvive_associa_contratti);
    app.get('/api/v1/maisonvive_associa_ddt_contratti',utility.check_subscription_expire, maisonvive.maisonvive_associa_ddt_contratti);
    app.post('/api/v1/maisonvive_associa_contratto_expensearticle',utility.check_subscription_expire, maisonvive.maisonvive_associa_contratto_expensearticle);
    app.get('/api/v1/maisonvive_contratti_from_fornitore',utility.check_subscription_expire, maisonvive.maisonvive_contratti_from_fornitore);
    app.get('/api/v1/maisonvive_associa_ordini',utility.check_subscription_expire, maisonvive.maisonvive_associa_ordini); 
    app.post('/api/v1/maisonvive_associa_ordine_expensearticle',utility.check_subscription_expire, maisonvive.maisonvive_associa_ordine_expensearticle);
    app.post('/api/v1/maisonvive_associa_ordine_ddt',utility.check_subscription_expire, maisonvive.maisonvive_associa_ordine_ddt);
    app.post('/api/v1/maisonvive_consegne', utility.check_subscription_expire,maisonvive.maisonvive_consegne);
    app.post('/api/v1/maisonvive_conferma_consegna', utility.check_subscription_expire,maisonvive.maisonvive_conferma_consegna);
    app.post('/api/v1/maisonvive_slot_calendario_consegne', utility.check_subscription_expire,maisonvive.maisonvive_modifica_consegna);
    app.post('/api/v1/maisonvive_crea_ddt_attivo', utility.check_subscription_expire,maisonvive.maisonvive_crea_ddt_attivo);
    app.get('/api/v1/maisonvive_calendario_consegne', utility.check_subscription_expire,maisonvive.maisonvive_calendario_consegne);
    app.get('/api/v1/maisonvive_calendario_scadenze', utility.check_subscription_expire,maisonvive.maisonvive_calendario_scadenze);
    app.post('/api/v1/maisonvive_goals', utility.check_subscription_expire,maisonvive.maisonvive_goals);
    app.post('/api/v1/maisonvive_delete_contract', utility.check_subscription_expire,maisonvive.maisonvive_delete_contract);
    app.post('/api/v1/maisonvive_finish_contract', utility.check_subscription_expire,maisonvive.maisonvive_finish_contract);
    app.post('/api/v1/maisonvive_reopen_contract', utility.check_subscription_expire,maisonvive.maisonvive_reopen_contract);
    app.post('/api/v1/maisonvive_contract_charts', utility.check_subscription_expire,maisonvive.maisonvive_contract_charts);
    app.post('/api/v1/maisonvive_delete_order', utility.check_subscription_expire,maisonvive.maisonvive_delete_order);
    app.post('/api/v1/maisonvive_sign_manual_contract', utility.check_subscription_expire,maisonvive.maisonvive_sign_manual_contract);
    app.post('/api/v1/maisonvive_disassocia_ordine_ddt',utility.check_subscription_expire, maisonvive.maisonvive_disassocia_ordine_ddt);
    app.post('/api/v1/maisonvive_disassocia_ordine_expensearticle',utility.check_subscription_expire, maisonvive.maisonvive_disassocia_ordine_expensearticle);
    app.post('/api/v1/maisonvive_escludi_contract', utility.check_subscription_expire,maisonvive.maisonvive_escludi_contract);
    app.get('/api/v1/maisonvive_associa_ddt_ordini',utility.check_subscription_expire, maisonvive.maisonvive_associa_ddt_ordini);
    app.post('/api/v1/maisonvive_ddt_upload_consegna', utility.check_subscription_expire,maisonvive.maisonvive_ddt_upload_consegna);
    app.post('/api/v1/maisonvive_save_importo_ordine', utility.check_subscription_expire,maisonvive.maisonvive_save_importo_ordine);
    app.post('/api/v1/maisonvive_aggiungi_montatore', utility.check_subscription_expire,maisonvive.maisonvive_aggiungi_montatore);
    app.post('/api/v1/maisonvive_paga_scadenza', utility.check_subscription_expire,maisonvive.maisonvive_paga_scadenza);
    app.post('/api/v1/maisonvive_cancella_consegna', utility.check_subscription_expire,maisonvive.maisonvive_cancella_consegna);
    app.post('/api/v1/maisonvive_cancella_consegna', utility.check_subscription_expire,maisonvive.maisonvive_cancella_consegna);
    app.post('/api/v1/maisonvive_expense_commercialista', utility.check_subscription_expire,maisonvive.maisonvive_expense_commercialista);
    app.post('/api/v1/maisonvive_associa_fattura_expire',utility.check_subscription_expire, maisonvive.maisonvive_associa_fattura_expire);
    
     /* 
     * YOUSIGN
     *
    */
    app.post('/api/v1/yousign_counter', yousign.yousign_counter);
    app.post('/api/v1/yousign_sended', yousign.yousign_sended);
    app.post('/api/v1/test_yousign', yousign.set_test_yousign);

    /* 
     * RUSCONI REHABILITA
     *
    */
    var rusconi = require('../application/controllers/rusconi/rusconi');
    app.get('/api/v1/rcb_get_fatture',utility.check_subscription_expire, rusconi.rcb_get_fatture);
    app.post('/api/v1/rcb_block_fatture',utility.check_subscription_expire, rusconi.rcb_block_fatture);

    /**
     * RELATIONS ADD/DELETE
     */
    var utility = require('../application/utility/utility');
    app.post('/api/v1/relations', utility.relations);
    app.post('/api/v1/sync_relations', utility.sync_relations);

    /**
     * SPECIAL REQUESTS
     */
     app.post('/api/v1/triage_request', utility.check_subscription_expire, user.triage_request);

     app.get('/api/v1/farmaci', utility.check_subscription_expire, patient.farmaci);
     app.get('/api/v1/confirm_event', utility.check_subscription_expire, event.confirm_event);
     app.post('/api/v1/confirm_event', utility.check_subscription_expire, event.confirm_event_post);
    

};