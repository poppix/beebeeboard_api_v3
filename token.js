var passport = require('passport');

/**
 * This endpoint is for verifying a token.  This has the same signature to
 * Google's token verification system from:
 * https://developers.google.com/accounts/docs/OAuth2UserAgent#validatetoken
 *
 * You call it like so
 * https://localhost:3000/api/tokeninfo?access_token=someToken
 *
 * If the token is valid you get returned
 * {
 *    "audience": someClientId
 * }
 *
 * If the token is not valid you get a 400 Status and this returned
 * {
 *     "error": "invalid_token"
 * }
 */
 exports.info = [
 function(req, res) {
 	if (req.query.access_token) {
 		Token.findOne({where:{token:req.query.access_token}}).then(function(token) {
 			if (new Date() > token.expiration_date) {
 				res.status(400);
 				res.json({
 					error : "invalid_token"
 				});
 			} else {
 				Client.findById(token.client_id).then(function(client) {
 					
 					if (token.expiration_date) {
 						var expirationLeft = Math.floor((token.expiration_date.getTime() - new Date().getTime()) / 1000);
 						if (expirationLeft <= 0) {
 							res.json({
 								error : "invalid_token"
 							});
 						} else {
 							res.json({
 								audience : client.clientId,
 								expires_in : expirationLeft
 							});
 						}
 					} else {
 						res.json({
 							audience : client.clientId
 						});
 					}
 					
 				}).catch(function(err){
 					res.status(400);
 					res.json({
 						error : "invalid_token"
 					});
 				});
 			}
 		}).catch(function(err){
 			res.status(400);
 			res.json({
 				error : "invalid_token"
 			});
 		});
 	} else {
 		res.status(400);
 		res.json({
 			error : "invalid_token"
 		});
 	}
 }];
