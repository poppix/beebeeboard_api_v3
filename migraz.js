var express = require('express'), Sequelize = require('sequelize'), fs = require('fs'), models_path = __dirname + '/application/models';
var multer  = require('multer');
var https = require('https');
var raven = require('raven');
var methodOverride = require('method-override');
var session = require('express-session');
var morgan = require('morgan');
var compression = require('compression');
	// Raven package for Sentry 
	client = new raven.Client('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3');

raven.patchGlobal(client);

//Pull in the memory store if we're configured to use it
//else pull in MemoryStore for the session configuration
console.log('Using MemoryStore for the Session');

var MemoryStore = session.MemoryStore;
sessionStorage = new MemoryStore();

// Express configuration
var app = express(); 

// compress all requests
app.use(compression());

// Body Parser configuration
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// Cross Origins configuration
var cors = require('cors'), corsOptions = {
	methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'],
	origin : true,
	credentials : true
}, config = require('./config'), passport = require('passport'), site = require('./site'), oauth2 = require('./oauth2'), token = require('./token'), https = require('https'), path = require('path'), dbToConnect = process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || (config.db.dbUrl + config.db.dbName), port = Number(process.env.PORT || 3000);

// Sequelize for Postgres Functions
var sequelize = new Sequelize(config.db.dbName, config.db.user, config.db.dbpwd, {
	host: process.env.MONGOHQ_URL || config.db.dbUrl,
	dialect: 'postgres',

	pool: {
		max: 5,
		min: 0,
		idle: 10000
	}
});

global.sequelize = sequelize;

var walk = function(path) {
	fs.readdirSync(path).forEach(function(file) {
		var newPath = path + '/' + file;
		var stat = fs.statSync(newPath);
		if (stat.isFile()) {
			if (/(.*)\.(js$|coffee$)/.test(file)) {
				require(newPath);
			}
		} else if (stat.isDirectory()) {
			walk(newPath);
		}
	});
};
walk(models_path);

// Middleware for Sentry segnalations
function onError(err, req, res, next) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    res.end(res.sentry+'\n');
}

app.use(cors(corsOptions));

app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(multer());

app.use(bodyParser.urlencoded({
	extended : false,
	limit: '20mb'
}));

app.use(bodyParser.json({
	limit: '20mb'}));
app.use(methodOverride());

//Session Configuration
app.use(session({
	resave : true,
	saveUninitialized : true,
	secret : config.session.secret,
	store : sessionStorage,
	key : "authorization.sid",
	cookie : {
		maxAge : config.session.maxAge
	}
}));

app.use(passport.initialize());
app.use(passport.session());

// Catch all for error messages.  Instead of a stack
// trace, this will log the json of the error message
// to the browser and pass along the status with it
app.use(function(err, req, res, next) {
	if (err) {
		res.status(err.status);
		res.json(err);
	} else {
		next();
	}
});


// Passport configuration
require('./auth');
require('./config/routes')(app, site, oauth2, token);

//static resources for stylesheets, images, javascript files
app.use(express.static(path.join(__dirname, 'public')));


// The request handler must be the first item
app.use(raven.middleware.express.requestHandler('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3'));

// The error handler must be before any other error middleware
app.use(raven.middleware.express.errorHandler('https://f392edf362e74a93adda8b1bb4003b0a:bac7c4eaee5d4d1d826d98d45e7b34a4@sentry.beebeeboard.com/3'));

// Optional fallthrough error handler
app.use(onError);


function migrazione() {
	 sequelize.query('with preno_no_pay as (select p.id as project_id, price, cf.value as pren_date, extract(year from cf.value::timestamp) as year, extract(month from cf.value::timestamp) as month, '+
				'extract(day from cf.value::timestamp) as day,extract(hour from cf.value::timestamp) as hour,extract(minute from cf.value::timestamp) as minute, ('+
				'select DISTINCT c.name as contact_name '+
				'from contacts as c '+
				'left join contact_projects as cp on cp.contact_id = c.id '+
				'where c._tenant_id = 170 and c.organization = \'scuolascimontebianco\' and '+
				'cp.project_id = p.id and contact_status_id = 104 and name is not null '+
				'limit 1'+
			')'+
			'from projects as p '+
			'left join project_custom_fields as pcf on pcf.project_id = p.id '+
			'left join custom_fields as cf on cf.id = pcf.custom_field_id '+
			'where p._tenant_id = 170 and p.organization = \'scuolascimontebianco\' and '+
			'cf.organization_custom_field_id = 183 and price > 0 '+
			'and NOT EXISTS ('+
				'select * '+
				'from payment_projects '+
				'where project_id = p.id '+
			')'+
			')select * from preno_no_pay where contact_name is not null order by contact_name, pren_date', {
        raw: true,
        type: Sequelize.QueryTypes.SELECT
      })
      .then(function(pren) {
      	var contact_preno = [];
      	var last_contact = pren[0].contact_name;

      	pren.forEach(function(p){
      		if(p.contact_name != last_contact){
      			var arr = contact_preno;
      			
      			ricorsione(arr, last_contact);

      			contact_preno = [];
      			last_contact = p.contact_name;
      		}else
      		{
      			contact_preno.push(p);
      		}
      	});
      	 
      }).catch(function(err){
      	console.log(err);
      });
}

function ricorsione(arr, last_contact){
	 sequelize.query('with preno_no_pay as (select p.id as project_id, price, cf.value as pren_date, extract(year from cf.value::timestamp) as year, extract(month from cf.value::timestamp) as month, '+
				'extract(day from cf.value::timestamp) as day,extract(hour from cf.value::timestamp) as hour,extract(minute from cf.value::timestamp) as minute, '+
				'c.name as contact_name '+
			'from projects as p '+
			'left join project_custom_fields as pcf on pcf.project_id = p.id '+
			'left join custom_fields as cf on cf.id = pcf.custom_field_id '+
			'left join contact_projects as cp on cp.project_id = p.id '+
			'left join contacts as c on c.id = cp.contact_id '+
			'where p._tenant_id = 170 and p.organization = \'scuolascimontebianco\' and '+
			'cf.organization_custom_field_id = 183 and price > 0 and c.name =\'' +last_contact.replace(/'/g, "''''")+'\''+
			'and NOT EXISTS ('+
				'select * '+
				'from payment_projects '+
				'where project_id = p.id '+
			')'+
			')select * from preno_no_pay where contact_name is not null order by contact_name, pren_date', {
	    raw: true,
	    type: Sequelize.QueryTypes.SELECT
	  })
	  .then(function(pren) {
	 	
      	var new_arr = pren;
		sequelize.query('with pays as (select payments.total, c.name, payments.id as payment_id, payments.date as pay_date '+
			', (total - ('+
				'select sum(price) '+
				'from projects as p '+
				'left join payment_projects as pp on pp.project_id = p.id '+
				'where p._tenant_id = 170 and p.organization = \'scuolascimontebianco\' and pp.payment_id = payments.id and p.id is not null '+
			')) as residuo '+
			'from payments '+
			'left join contact_payments as cp on cp.payment_id = payments.id '+
			'left join contacts as c on c.id = cp.contact_id '+
			'where payments._tenant_id = 170 and payments.organization = \'scuolascimontebianco\' and c.name =\'' +last_contact.replace(/'/g, "''''")+'\' '+
			'and total > 0 '+
			'order by pay_date) select * from pays where residuo > 0 or residuo is null' , {
		    raw: true,
		    type: Sequelize.QueryTypes.SELECT
		  })
		  .then(function(pays) {
		  	var residuo = 0;
		  	for(var i= 0; i< pays.length; i++){
		  		if(!pays[i].residuo || pays[i].residuo == null){
		  			residuo = pays[i].total;
		  		}
		  		else
		  		{
		  			residuo = pays[i].residuo;
		  		}
		  		
		  		var min = residuo;
		  		var minindex = -1;

		  		for(var j = 0; j< new_arr.length; j++){

		  			if(residuo == new_arr[j].price){
		  				sequelize.query('Insert into payment_projects (payment_id, project_id) select '+pays[i].payment_id+','+new_arr[j].project_id+' where not exists (select * from payment_projects where payment_id = '+pays[i].payment_id+' and project_id = '+new_arr[j].project_id+')', {
						    raw: true,
						    type: Sequelize.QueryTypes.SELECT
						  })
						  .then(function(ins) {
						  		ricorsione(new_arr, last_contact);
						  		return 1;
						  }).catch(function(err){
						  	console.log(err);
						  });
		  				
		  			}
		  			else{
		  				if(residuo - new_arr[j].price < min){
		  					min = residuo - new_arr[j].price;
		  					minindex = j;
		  				}
		  			}
		  		}

		  		if(minindex != -1){
			  		sequelize.query('Insert into payment_projects (payment_id, project_id) select '+pays[i].payment_id+','+new_arr[minindex].project_id+' where not exists (select * from payment_projects where payment_id = '+pays[i].payment_id+' and project_id = '+new_arr[minindex].project_id+')', {
					    raw: true,
					    type: Sequelize.QueryTypes.SELECT
					  })
					  .then(function(ins) {
							ricorsione(arr, last_contact);
							return 1;
					  }).catch(function(err){
					  	console.log(err);
					  });
		  		}
		  		else
		  		{
		  			return 1;
		  		}

	  		}

		  }).catch(function(err){
		  		console.log(err);
		  });
	}).catch(function(err){
  		console.log(err);
  });

};

migrazione();


